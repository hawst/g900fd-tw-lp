.class public Lorg/icepdf/index/core/io/BitStream;
.super Ljava/lang/Object;
.source "BitStream.java"


# static fields
.field private static final masks:[I


# instance fields
.field bits:I

.field bits_left:I

.field in:Ljava/io/InputStream;

.field out:Ljava/io/OutputStream;

.field readEOF:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x20

    .line 31
    new-array v1, v3, [I

    sput-object v1, Lorg/icepdf/index/core/io/BitStream;->masks:[I

    .line 34
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 35
    sget-object v1, Lorg/icepdf/index/core/io/BitStream;->masks:[I

    const/4 v2, 0x1

    shl-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x1

    aput v2, v1, v0

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "i"    # Ljava/io/InputStream;

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lorg/icepdf/index/core/io/BitStream;->in:Ljava/io/InputStream;

    .line 47
    iput v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    .line 48
    iput v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    .line 49
    iput-boolean v0, p0, Lorg/icepdf/index/core/io/BitStream;->readEOF:Z

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "o"    # Ljava/io/OutputStream;

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lorg/icepdf/index/core/io/BitStream;->out:Ljava/io/OutputStream;

    .line 60
    iput v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    .line 61
    iput v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    .line 62
    iput-boolean v0, p0, Lorg/icepdf/index/core/io/BitStream;->readEOF:Z

    .line 63
    return-void
.end method


# virtual methods
.method public atEndOfFile()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lorg/icepdf/index/core/io/BitStream;->readEOF:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    iget v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/icepdf/index/core/io/BitStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    if-gtz v0, :cond_0

    .line 147
    const/4 v0, 0x0

    .line 148
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lorg/icepdf/index/core/io/BitStream;->in:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lorg/icepdf/index/core/io/BitStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 76
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/io/BitStream;->out:Ljava/io/OutputStream;

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lorg/icepdf/index/core/io/BitStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 78
    iget-object v0, p0, Lorg/icepdf/index/core/io/BitStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 80
    :cond_1
    return-void
.end method

.method public getBits(I)I
    .locals 3
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    :goto_0
    iget v1, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    if-ge v1, p1, :cond_0

    .line 89
    iget-object v1, p0, Lorg/icepdf/index/core/io/BitStream;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 90
    .local v0, "r":I
    if-gez v0, :cond_1

    .line 91
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/icepdf/index/core/io/BitStream;->readEOF:Z

    .line 98
    .end local v0    # "r":I
    :cond_0
    iget v1, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    sub-int/2addr v1, p1

    iput v1, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    .line 99
    iget v1, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    iget v2, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    shr-int/2addr v1, v2

    sget-object v2, Lorg/icepdf/index/core/io/BitStream;->masks:[I

    aget v2, v2, p1

    and-int/2addr v1, v2

    return v1

    .line 94
    .restart local v0    # "r":I
    :cond_1
    iget v1, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    shl-int/lit8 v1, v1, 0x8

    iput v1, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    .line 95
    iget v1, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    and-int/lit16 v2, v0, 0xff

    or-int/2addr v1, v2

    iput v1, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    .line 96
    iget v1, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    add-int/lit8 v1, v1, 0x8

    iput v1, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    goto :goto_0
.end method

.method public putBit(I)V
    .locals 3
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 111
    iget v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    shl-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    .line 112
    iget v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    or-int/2addr v0, p1

    iput v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    .line 113
    iget v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    .line 114
    iget v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 115
    iget-object v0, p0, Lorg/icepdf/index/core/io/BitStream;->out:Ljava/io/OutputStream;

    iget v1, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 116
    iput v2, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    .line 117
    iput v2, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    .line 119
    :cond_0
    return-void
.end method

.method public putRunBits(II)V
    .locals 3
    .param p1, "i"    # I
    .param p2, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    add-int/lit8 v0, p2, -0x1

    .local v0, "j":I
    :goto_0
    if-ltz v0, :cond_3

    .line 128
    iget v1, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    if-nez v1, :cond_0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_1

    .line 129
    :cond_0
    invoke-virtual {p0, p1}, Lorg/icepdf/index/core/io/BitStream;->putBit(I)V

    .line 130
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 132
    :cond_1
    if-nez p1, :cond_2

    .line 133
    iget-object v1, p0, Lorg/icepdf/index/core/io/BitStream;->out:Ljava/io/OutputStream;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write(I)V

    .line 136
    :goto_1
    add-int/lit8 v0, v0, -0x8

    goto :goto_0

    .line 135
    :cond_2
    iget-object v1, p0, Lorg/icepdf/index/core/io/BitStream;->out:Ljava/io/OutputStream;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write(I)V

    goto :goto_1

    .line 139
    :cond_3
    return-void
.end method

.method public skipByte()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 155
    iput v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits_left:I

    .line 156
    iput v0, p0, Lorg/icepdf/index/core/io/BitStream;->bits:I

    .line 157
    return-void
.end method
