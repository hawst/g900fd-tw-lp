.class public Lorg/icepdf/index/core/pobjects/Name;
.super Ljava/lang/Object;
.source "Name.java"


# static fields
.field private static final HEX_CHAR:I = 0x23


# instance fields
.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/Name;->convertHexChars(Ljava/lang/StringBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Name;->name:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/StringBuffer;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/StringBuffer;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-direct {p0, p1}, Lorg/icepdf/index/core/pobjects/Name;->convertHexChars(Ljava/lang/StringBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Name;->name:Ljava/lang/String;

    .line 66
    return-void
.end method

.method private convertHexChars(Ljava/lang/StringBuffer;)Ljava/lang/String;
    .locals 5
    .param p1, "name"    # Ljava/lang/StringBuffer;

    .prologue
    .line 126
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 127
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    const/16 v4, 0x23

    if-ne v3, v4, :cond_0

    .line 129
    add-int/lit8 v3, v2, 0x1

    add-int/lit8 v4, v2, 0x3

    invoke-virtual {p1, v3, v4}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 131
    .local v0, "charDd":I
    add-int/lit8 v3, v2, 0x3

    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 132
    int-to-char v3, v0

    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    .end local v0    # "charDd":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 135
    :catch_0
    move-exception v1

    .line 139
    .local v1, "e":Ljava/lang/Throwable;
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 143
    .end local v1    # "e":Ljava/lang/Throwable;
    :goto_1
    return-object v3

    :cond_1
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 95
    if-nez p1, :cond_1

    .line 100
    .end local p1    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 97
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Lorg/icepdf/index/core/pobjects/Name;

    if-eqz v1, :cond_2

    .line 98
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Name;->name:Ljava/lang/String;

    check-cast p1, Lorg/icepdf/index/core/pobjects/Name;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 100
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_2
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Name;->name:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Name;->name:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Name;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Name;->name:Ljava/lang/String;

    return-object v0
.end method
