.class public Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;
.super Lorg/icepdf/index/core/pobjects/security/SecurityHandler;
.source "StandardSecurityHandler.java"


# instance fields
.field private encryptionKey:[B

.field private initiated:Z

.field private password:Ljava/lang/String;

.field private standardEncryption:Lorg/icepdf/index/core/pobjects/security/StandardEncryption;


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;)V
    .locals 1
    .param p1, "encryptionDictionary"    # Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lorg/icepdf/index/core/pobjects/security/SecurityHandler;-><init>(Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;)V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/index/core/pobjects/security/StandardEncryption;

    .line 116
    const-string/jumbo v0, "Adobe Standard Security"

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->handlerName:Ljava/lang/String;

    .line 117
    return-void
.end method


# virtual methods
.method public decrypt(Lorg/icepdf/index/core/pobjects/Reference;[B[B)[B
    .locals 1
    .param p1, "objectReference"    # Lorg/icepdf/index/core/pobjects/Reference;
    .param p2, "encryptionKey"    # [B
    .param p3, "data"    # [B

    .prologue
    .line 198
    invoke-virtual {p0, p1, p2, p3}, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encrypt(Lorg/icepdf/index/core/pobjects/Reference;[B[B)[B

    move-result-object v0

    return-object v0
.end method

.method public dispose()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 285
    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/index/core/pobjects/security/StandardEncryption;

    .line 286
    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionKey:[B

    .line 287
    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->permissions:Lorg/icepdf/index/core/pobjects/security/Permissions;

    .line 289
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->initiated:Z

    .line 290
    return-void
.end method

.method public encrypt(Lorg/icepdf/index/core/pobjects/Reference;[B[B)[B
    .locals 5
    .param p1, "objectReference"    # Lorg/icepdf/index/core/pobjects/Reference;
    .param p2, "encryptionKey"    # [B
    .param p3, "data"    # [B

    .prologue
    .line 174
    const-string/jumbo v0, "V2"

    .line 175
    .local v0, "algorithmType":Ljava/lang/String;
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getCryptFilter()Lorg/icepdf/index/core/pobjects/security/CryptFilter;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 176
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getCryptFilter()Lorg/icepdf/index/core/pobjects/security/CryptFilter;

    move-result-object v3

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v4}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getStrF()Lorg/icepdf/index/core/pobjects/Name;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/icepdf/index/core/pobjects/security/CryptFilter;->getCryptFilterByName(Lorg/icepdf/index/core/pobjects/Name;)Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;

    move-result-object v1

    .line 180
    .local v1, "cryptFilterEntry":Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;
    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;->getCryptFilterMethod()Lorg/icepdf/index/core/pobjects/Name;

    move-result-object v2

    .line 181
    .local v2, "namefiltermethod":Lorg/icepdf/index/core/pobjects/Name;
    if-eqz v2, :cond_0

    .line 183
    invoke-virtual {v2}, Lorg/icepdf/index/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v0

    .line 190
    .end local v1    # "cryptFilterEntry":Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;
    .end local v2    # "namefiltermethod":Lorg/icepdf/index/core/pobjects/Name;
    :cond_0
    :goto_0
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/index/core/pobjects/security/StandardEncryption;

    invoke-virtual {v3, p1, p2, v0, p3}, Lorg/icepdf/index/core/pobjects/security/StandardEncryption;->generalEncryptionAlgorithm(Lorg/icepdf/index/core/pobjects/Reference;[BLjava/lang/String;[B)[B

    move-result-object v3

    return-object v3

    .line 186
    :cond_1
    const-string/jumbo v0, "V2"

    goto :goto_0
.end method

.method public getDecryptionKey()[B
    .locals 1

    .prologue
    .line 259
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->getEncryptionKey()[B

    move-result-object v0

    return-object v0
.end method

.method public getEncryptionInputStream(Lorg/icepdf/index/core/pobjects/Reference;[BLjava/util/Hashtable;Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 6
    .param p1, "objectReference"    # Lorg/icepdf/index/core/pobjects/Reference;
    .param p2, "encryptionKey"    # [B
    .param p3, "decodeParams"    # Ljava/util/Hashtable;
    .param p4, "input"    # Ljava/io/InputStream;

    .prologue
    .line 208
    const/4 v1, 0x0

    .line 209
    .local v1, "cryptFilter":Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;
    if-eqz p3, :cond_3

    .line 210
    const-string/jumbo v4, "Name"

    invoke-virtual {p3, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/index/core/pobjects/Name;

    .line 213
    .local v2, "filterName":Lorg/icepdf/index/core/pobjects/Name;
    const-string/jumbo v4, "Identity"

    invoke-virtual {v2, v4}, Lorg/icepdf/index/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 240
    .end local v2    # "filterName":Lorg/icepdf/index/core/pobjects/Name;
    .end local p4    # "input":Ljava/io/InputStream;
    :goto_0
    return-object p4

    .line 217
    .restart local v2    # "filterName":Lorg/icepdf/index/core/pobjects/Name;
    .restart local p4    # "input":Ljava/io/InputStream;
    :cond_0
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v4}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getCryptFilter()Lorg/icepdf/index/core/pobjects/security/CryptFilter;

    move-result-object v4

    invoke-virtual {v4, v2}, Lorg/icepdf/index/core/pobjects/security/CryptFilter;->getCryptFilterByName(Lorg/icepdf/index/core/pobjects/Name;)Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;

    move-result-object v1

    .line 229
    .end local v2    # "filterName":Lorg/icepdf/index/core/pobjects/Name;
    :cond_1
    :goto_1
    const-string/jumbo v0, "V2"

    .line 230
    .local v0, "algorithmType":Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 231
    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;->getCryptFilterMethod()Lorg/icepdf/index/core/pobjects/Name;

    move-result-object v3

    .line 232
    .local v3, "namefiltermethod":Lorg/icepdf/index/core/pobjects/Name;
    if-eqz v3, :cond_2

    .line 234
    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v0

    .line 240
    .end local v3    # "namefiltermethod":Lorg/icepdf/index/core/pobjects/Name;
    :cond_2
    :goto_2
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/index/core/pobjects/security/StandardEncryption;

    invoke-virtual {v4, p1, p2, v0, p4}, Lorg/icepdf/index/core/pobjects/security/StandardEncryption;->generalEncryptionInputStream(Lorg/icepdf/index/core/pobjects/Reference;[BLjava/lang/String;Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object p4

    goto :goto_0

    .line 223
    .end local v0    # "algorithmType":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v4}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getCryptFilter()Lorg/icepdf/index/core/pobjects/security/CryptFilter;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 224
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v4}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getCryptFilter()Lorg/icepdf/index/core/pobjects/security/CryptFilter;

    move-result-object v4

    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v5}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getStmF()Lorg/icepdf/index/core/pobjects/Name;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/icepdf/index/core/pobjects/security/CryptFilter;->getCryptFilterByName(Lorg/icepdf/index/core/pobjects/Name;)Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;

    move-result-object v1

    goto :goto_1

    .line 237
    .restart local v0    # "algorithmType":Ljava/lang/String;
    :cond_4
    const-string/jumbo v0, "V2"

    goto :goto_2
.end method

.method public getEncryptionKey()[B
    .locals 3

    .prologue
    .line 246
    iget-boolean v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->initiated:Z

    if-nez v0, :cond_0

    .line 248
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->init()V

    .line 251
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/index/core/pobjects/security/StandardEncryption;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->password:Ljava/lang/String;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v2}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getKeyLength()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/security/StandardEncryption;->encryptionKeyAlgorithm(Ljava/lang/String;I)[B

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionKey:[B

    .line 255
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionKey:[B

    return-object v0
.end method

.method public getHandlerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->handlerName:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissions()Lorg/icepdf/index/core/pobjects/security/Permissions;
    .locals 1

    .prologue
    .line 263
    iget-boolean v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->initiated:Z

    if-nez v0, :cond_0

    .line 265
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->init()V

    .line 267
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->permissions:Lorg/icepdf/index/core/pobjects/security/Permissions;

    return-object v0
.end method

.method public init()V
    .locals 2

    .prologue
    .line 276
    new-instance v0, Lorg/icepdf/index/core/pobjects/security/StandardEncryption;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/pobjects/security/StandardEncryption;-><init>(Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/index/core/pobjects/security/StandardEncryption;

    .line 278
    new-instance v0, Lorg/icepdf/index/core/pobjects/security/Permissions;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/pobjects/security/Permissions;-><init>(Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->permissions:Lorg/icepdf/index/core/pobjects/security/Permissions;

    .line 279
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->permissions:Lorg/icepdf/index/core/pobjects/security/Permissions;

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/security/Permissions;->init()V

    .line 281
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->initiated:Z

    .line 282
    return-void
.end method

.method public isAuthorized(Ljava/lang/String;)Z
    .locals 5
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x5

    const/4 v2, 0x0

    .line 120
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v3

    if-ge v3, v4, :cond_3

    .line 121
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/index/core/pobjects/security/StandardEncryption;

    invoke-virtual {v2, p1}, Lorg/icepdf/index/core/pobjects/security/StandardEncryption;->authenticateUserPassword(Ljava/lang/String;)Z

    move-result v1

    .line 123
    .local v1, "value":Z
    if-nez v1, :cond_2

    .line 125
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/index/core/pobjects/security/StandardEncryption;

    invoke-virtual {v2, p1}, Lorg/icepdf/index/core/pobjects/security/StandardEncryption;->authenticateOwnerPassword(Ljava/lang/String;)Z

    move-result v1

    .line 127
    if-eqz v1, :cond_0

    .line 128
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/index/core/pobjects/security/StandardEncryption;

    invoke-virtual {v2}, Lorg/icepdf/index/core/pobjects/security/StandardEncryption;->getUserPassword()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->password:Ljava/lang/String;

    :cond_0
    :goto_0
    move v2, v1

    .line 143
    .end local v1    # "value":Z
    :cond_1
    :goto_1
    return v2

    .line 132
    .restart local v1    # "value":Z
    :cond_2
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->password:Ljava/lang/String;

    goto :goto_0

    .line 135
    .end local v1    # "value":Z
    :cond_3
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v3

    if-ne v3, v4, :cond_1

    .line 137
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/index/core/pobjects/security/StandardEncryption;

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v4}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getKeyLength()I

    move-result v4

    invoke-virtual {v3, p1, v4}, Lorg/icepdf/index/core/pobjects/security/StandardEncryption;->encryptionKeyAlgorithm(Ljava/lang/String;I)[B

    move-result-object v0

    .line 140
    .local v0, "encryptionKey":[B
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->password:Ljava/lang/String;

    .line 141
    if-eqz v0, :cond_1

    const/4 v2, 0x1

    goto :goto_1
.end method

.method public isOwnerAuthorized(Ljava/lang/String;)Z
    .locals 2
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 149
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 150
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/index/core/pobjects/security/StandardEncryption;

    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/pobjects/security/StandardEncryption;->authenticateOwnerPassword(Ljava/lang/String;)Z

    move-result v0

    .line 152
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->isAuthenticatedOwnerPassword()Z

    move-result v0

    goto :goto_0
.end method

.method public isUserAuthorized(Ljava/lang/String;)Z
    .locals 3
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 158
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v1

    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 159
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/index/core/pobjects/security/StandardEncryption;

    invoke-virtual {v1, p1}, Lorg/icepdf/index/core/pobjects/security/StandardEncryption;->authenticateUserPassword(Ljava/lang/String;)Z

    move-result v0

    .line 160
    .local v0, "value":Z
    if-eqz v0, :cond_0

    .line 161
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->password:Ljava/lang/String;

    .line 165
    .end local v0    # "value":Z
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->isAuthenticatedUserPassword()Z

    move-result v0

    goto :goto_0
.end method
