.class public Lorg/icepdf/index/core/util/Parser;
.super Ljava/lang/Object;
.source "Parser.java"


# static fields
.field public static final PARSE_MODE_NORMAL:I = 0x0

.field public static final PARSE_MODE_OBJECT_STREAM:I = 0x1


# instance fields
.field private parseMode:I

.field private reader:Ljava/io/InputStream;

.field private stack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "r"    # Ljava/io/InputStream;

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/icepdf/index/core/util/Parser;-><init>(Ljava/io/InputStream;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1
    .param p1, "r"    # Ljava/io/InputStream;
    .param p2, "pm"    # I

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    .line 66
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    .line 67
    iput p2, p0, Lorg/icepdf/index/core/util/Parser;->parseMode:I

    .line 68
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/core/io/SeekableInput;)V
    .locals 1
    .param p1, "r"    # Lorg/icepdf/index/core/io/SeekableInput;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/icepdf/index/core/util/Parser;-><init>(Lorg/icepdf/index/core/io/SeekableInput;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/core/io/SeekableInput;I)V
    .locals 1
    .param p1, "r"    # Lorg/icepdf/index/core/io/SeekableInput;
    .param p2, "pm"    # I

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    .line 57
    invoke-interface {p1}, Lorg/icepdf/index/core/io/SeekableInput;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    .line 58
    iput p2, p0, Lorg/icepdf/index/core/util/Parser;->parseMode:I

    .line 59
    return-void
.end method

.method private captureStreamData(Ljava/io/OutputStream;)J
    .locals 7
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x65

    .line 1239
    const-wide/16 v2, 0x0

    .line 1242
    .local v2, "numBytes":J
    :goto_0
    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 1244
    .local v0, "nextByte":I
    if-ne v0, v6, :cond_3

    .line 1245
    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    const/16 v4, 0xa

    invoke-virtual {v1, v4}, Ljava/io/InputStream;->mark(I)V

    .line 1246
    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x6e

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x64

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x73

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x74

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x72

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    if-ne v1, v6, :cond_0

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x61

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x6d

    if-ne v1, v4, :cond_0

    .line 1265
    :goto_1
    return-wide v2

    .line 1256
    :cond_0
    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->reset()V

    .line 1261
    :cond_1
    if-eqz p1, :cond_2

    .line 1262
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 1263
    :cond_2
    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    .line 1264
    goto :goto_0

    .line 1258
    :cond_3
    if-gez v0, :cond_1

    goto :goto_1
.end method

.method private static final isDelimiter(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 1161
    const/16 v0, 0x5b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x5d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x28

    if-eq p0, v0, :cond_0

    const/16 v0, 0x29

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3c

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x25

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isExpectedInContentStream(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 1174
    const/16 v0, 0x61

    if-lt p0, v0, :cond_0

    const/16 v0, 0x7a

    if-le p0, v0, :cond_3

    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x5a

    if-le p0, v0, :cond_3

    :cond_1
    const/16 v0, 0x30

    if-lt p0, v0, :cond_2

    const/16 v0, 0x39

    if-le p0, v0, :cond_3

    :cond_2
    invoke-static {p0}, Lorg/icepdf/index/core/util/Parser;->isWhitespace(C)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, Lorg/icepdf/index/core/util/Parser;->isDelimiter(C)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x5c

    if-eq p0, v0, :cond_3

    const/16 v0, 0x27

    if-eq p0, v0, :cond_3

    const/16 v0, 0x22

    if-eq p0, v0, :cond_3

    const/16 v0, 0x2a

    if-eq p0, v0, :cond_3

    const/16 v0, 0x2e

    if-ne p0, v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isStillInlineImageData(Ljava/io/InputStream;I)Z
    .locals 8
    .param p0, "reader"    # Ljava/io/InputStream;
    .param p1, "numBytesToCheck"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1193
    const/4 v2, 0x0

    .line 1194
    .local v2, "imageDataFound":Z
    const/4 v4, 0x1

    .line 1195
    .local v4, "onlyWhitespaceSoFar":Z
    invoke-virtual {p0, p1}, Ljava/io/InputStream;->mark(I)V

    .line 1196
    new-array v5, p1, [B

    .line 1197
    .local v5, "toCheck":[B
    invoke-virtual {p0, v5}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 1198
    .local v3, "numReadToCheck":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 1199
    aget-byte v7, v5, v1

    and-int/lit16 v7, v7, 0xff

    int-to-char v0, v7

    .line 1202
    .local v0, "charToCheck":C
    const/16 v7, 0x51

    if-eq v0, v7, :cond_0

    const/16 v7, 0x71

    if-eq v0, v7, :cond_0

    const/16 v7, 0x53

    if-eq v0, v7, :cond_0

    const/16 v7, 0x73

    if-ne v0, v7, :cond_2

    :cond_0
    const/4 v6, 0x1

    .line 1205
    .local v6, "typicalTextTokenInContentStream":Z
    :goto_1
    if-eqz v4, :cond_3

    if-eqz v6, :cond_3

    add-int/lit8 v7, v1, 0x1

    if-ge v7, v3, :cond_3

    add-int/lit8 v7, v1, 0x1

    aget-byte v7, v5, v7

    and-int/lit16 v7, v7, 0xff

    int-to-char v7, v7

    invoke-static {v7}, Lorg/icepdf/index/core/util/Parser;->isWhitespace(C)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1220
    .end local v0    # "charToCheck":C
    .end local v6    # "typicalTextTokenInContentStream":Z
    :cond_1
    :goto_2
    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    .line 1221
    return v2

    .line 1202
    .restart local v0    # "charToCheck":C
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 1211
    .restart local v6    # "typicalTextTokenInContentStream":Z
    :cond_3
    invoke-static {v0}, Lorg/icepdf/index/core/util/Parser;->isWhitespace(C)Z

    move-result v7

    if-nez v7, :cond_4

    .line 1212
    const/4 v4, 0x0

    .line 1215
    :cond_4
    invoke-static {v0}, Lorg/icepdf/index/core/util/Parser;->isExpectedInContentStream(C)Z

    move-result v7

    if-nez v7, :cond_5

    .line 1216
    const/4 v2, 0x1

    .line 1217
    goto :goto_2

    .line 1198
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static final isWhitespace(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 1156
    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private skipUntilEndstream(Ljava/io/OutputStream;)J
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x65

    const/16 v6, 0xa

    .line 1269
    const-wide/16 v2, 0x0

    .line 1271
    .local v2, "skipped":J
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1, v6}, Ljava/io/InputStream;->mark(I)V

    .line 1273
    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 1274
    .local v0, "nextByte":I
    if-ne v0, v7, :cond_2

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x6e

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x64

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x73

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x74

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x72

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    if-ne v1, v7, :cond_2

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x61

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x6d

    if-ne v1, v4, :cond_2

    .line 1283
    iget-object v1, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->reset()V

    .line 1295
    :cond_1
    return-wide v2

    .line 1285
    :cond_2
    if-ltz v0, :cond_1

    .line 1288
    if-eq v0, v6, :cond_0

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 1290
    if-eqz p1, :cond_3

    .line 1291
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 1293
    :cond_3
    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    .line 1294
    goto :goto_0
.end method


# virtual methods
.method public addPObject(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/pobjects/Reference;)Lorg/icepdf/index/core/pobjects/PObject;
    .locals 3
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "objectReference"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    .line 522
    iget-object v2, p0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    .line 526
    .local v0, "o":Ljava/lang/Object;
    instance-of v2, v0, Lorg/icepdf/index/core/pobjects/Stream;

    if-eqz v2, :cond_1

    move-object v1, v0

    .line 527
    check-cast v1, Lorg/icepdf/index/core/pobjects/Stream;

    .line 528
    .local v1, "tmp":Lorg/icepdf/index/core/pobjects/Stream;
    invoke-virtual {v1, p2}, Lorg/icepdf/index/core/pobjects/Stream;->setPObjectReference(Lorg/icepdf/index/core/pobjects/Reference;)V

    .line 539
    .end local v1    # "tmp":Lorg/icepdf/index/core/pobjects/Stream;
    :cond_0
    :goto_0
    invoke-virtual {p1, v0, p2}, Lorg/icepdf/index/core/util/Library;->addObject(Ljava/lang/Object;Lorg/icepdf/index/core/pobjects/Reference;)V

    .line 541
    new-instance v2, Lorg/icepdf/index/core/pobjects/PObject;

    invoke-direct {v2, v0, p2}, Lorg/icepdf/index/core/pobjects/PObject;-><init>(Ljava/lang/Object;Lorg/icepdf/index/core/pobjects/Reference;)V

    return-object v2

    .line 533
    :cond_1
    instance-of v2, v0, Lorg/icepdf/index/core/pobjects/Dictionary;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 534
    check-cast v1, Lorg/icepdf/index/core/pobjects/Dictionary;

    .line 535
    .local v1, "tmp":Lorg/icepdf/index/core/pobjects/Dictionary;
    invoke-virtual {v1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;->setPObjectReference(Lorg/icepdf/index/core/pobjects/Reference;)V

    goto :goto_0
.end method

.method public getCharSurroundedByWhitespace()C
    .locals 4

    .prologue
    .line 1025
    const/4 v0, 0x0

    .line 1028
    .local v0, "alpha":C
    :cond_0
    :try_start_0
    iget-object v3, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 1029
    .local v2, "curr":I
    if-gez v2, :cond_1

    .line 1041
    .end local v2    # "curr":I
    :goto_0
    return v0

    .line 1031
    .restart local v2    # "curr":I
    :cond_1
    int-to-char v1, v2

    .line 1032
    .local v1, "c":C
    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_0

    .line 1033
    move v0, v1

    .line 1034
    goto :goto_0

    .line 1038
    .end local v1    # "c":C
    .end local v2    # "curr":I
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public getIntSurroundedByWhitespace()I
    .locals 5

    .prologue
    .line 965
    const/4 v2, 0x0

    .line 966
    .local v2, "num":I
    const/4 v1, 0x0

    .line 967
    .local v1, "makeNegative":Z
    const/4 v3, 0x0

    .line 970
    .local v3, "readNonWhitespace":Z
    :cond_0
    :goto_0
    :try_start_0
    iget-object v4, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 971
    .local v0, "curr":I
    if-gez v0, :cond_2

    .line 989
    .end local v0    # "curr":I
    :goto_1
    if-eqz v1, :cond_1

    .line 990
    mul-int/lit8 v2, v2, -0x1

    .line 991
    :cond_1
    return v2

    .line 973
    .restart local v0    # "curr":I
    :cond_2
    int-to-char v4, v0

    invoke-static {v4}, Ljava/lang/Character;->isWhitespace(C)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_3

    .line 974
    if-eqz v3, :cond_0

    goto :goto_1

    .line 976
    :cond_3
    const/16 v4, 0x2d

    if-ne v0, v4, :cond_4

    .line 977
    const/4 v1, 0x1

    .line 978
    const/4 v3, 0x1

    goto :goto_0

    .line 979
    :cond_4
    const/16 v4, 0x30

    if-lt v0, v4, :cond_0

    const/16 v4, 0x39

    if-gt v0, v4, :cond_0

    .line 980
    mul-int/lit8 v2, v2, 0xa

    .line 981
    add-int/lit8 v4, v0, -0x30

    add-int/2addr v2, v4

    .line 982
    const/4 v3, 0x1

    goto :goto_0

    .line 986
    .end local v0    # "curr":I
    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method public getLongSurroundedByWhitespace()J
    .locals 8

    .prologue
    .line 995
    const-wide/16 v2, 0x0

    .line 996
    .local v2, "num":J
    const/4 v1, 0x0

    .line 997
    .local v1, "makeNegative":Z
    const/4 v4, 0x0

    .line 1000
    .local v4, "readNonWhitespace":Z
    :cond_0
    :goto_0
    :try_start_0
    iget-object v5, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 1001
    .local v0, "curr":I
    if-gez v0, :cond_2

    .line 1019
    .end local v0    # "curr":I
    :goto_1
    if-eqz v1, :cond_1

    .line 1020
    const-wide/16 v6, -0x1

    mul-long/2addr v2, v6

    .line 1021
    :cond_1
    return-wide v2

    .line 1003
    .restart local v0    # "curr":I
    :cond_2
    int-to-char v5, v0

    invoke-static {v5}, Ljava/lang/Character;->isWhitespace(C)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_3

    .line 1004
    if-eqz v4, :cond_0

    goto :goto_1

    .line 1006
    :cond_3
    const/16 v5, 0x2d

    if-ne v0, v5, :cond_4

    .line 1007
    const/4 v1, 0x1

    .line 1008
    const/4 v4, 0x1

    goto :goto_0

    .line 1009
    :cond_4
    const/16 v5, 0x30

    if-lt v0, v5, :cond_0

    const/16 v5, 0x39

    if-gt v0, v5, :cond_0

    .line 1010
    const-wide/16 v6, 0xa

    mul-long/2addr v2, v6

    .line 1011
    add-int/lit8 v5, v0, -0x30

    int-to-long v6, v5

    add-long/2addr v2, v6

    .line 1012
    const/4 v4, 0x1

    goto :goto_0

    .line 1016
    .end local v0    # "curr":I
    :catch_0
    move-exception v5

    goto :goto_1
.end method

.method public getNumberOrStringWithMark(I)Ljava/lang/Object;
    .locals 9
    .param p1, "maxLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 904
    iget-object v8, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v8, p1}, Ljava/io/InputStream;->mark(I)V

    .line 906
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7, p1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 907
    .local v7, "sb":Ljava/lang/StringBuffer;
    const/4 v6, 0x0

    .line 908
    .local v6, "readNonWhitespaceYet":Z
    const/4 v3, 0x0

    .line 909
    .local v3, "foundDigit":Z
    const/4 v2, 0x0

    .line 911
    .local v2, "foundDecimal":Z
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, p1, :cond_0

    .line 912
    iget-object v8, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 913
    .local v0, "curr":I
    if-gez v0, :cond_1

    .line 942
    .end local v0    # "curr":I
    :cond_0
    :goto_1
    if-eqz v3, :cond_8

    .line 944
    if-eqz v2, :cond_7

    .line 945
    :try_start_0
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 957
    :goto_2
    return-object v8

    .line 915
    .restart local v0    # "curr":I
    :cond_1
    int-to-char v1, v0

    .line 916
    .local v1, "currChar":C
    invoke-static {v1}, Lorg/icepdf/index/core/util/Parser;->isWhitespace(C)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 917
    if-nez v6, :cond_0

    .line 911
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 919
    :cond_2
    invoke-static {v1}, Lorg/icepdf/index/core/util/Parser;->isDelimiter(C)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 923
    iget-object v8, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->reset()V

    .line 924
    iget-object v8, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v8, p1}, Ljava/io/InputStream;->mark(I)V

    .line 925
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_4
    if-ge v5, v4, :cond_3

    .line 926
    iget-object v8, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->read()I

    .line 925
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 928
    :cond_3
    const/4 v6, 0x1

    .line 929
    goto :goto_1

    .line 931
    .end local v5    # "j":I
    :cond_4
    const/4 v6, 0x1

    .line 932
    const/16 v8, 0x2e

    if-ne v1, v8, :cond_6

    .line 933
    const/4 v2, 0x1

    .line 936
    :cond_5
    :goto_5
    invoke-virtual {v7, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 934
    :cond_6
    const/16 v8, 0x30

    if-lt v1, v8, :cond_5

    const/16 v8, 0x39

    if-gt v0, v8, :cond_5

    .line 935
    const/4 v3, 0x1

    goto :goto_5

    .line 947
    .end local v0    # "curr":I
    .end local v1    # "currChar":C
    :cond_7
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v8

    goto :goto_2

    .line 950
    :catch_0
    move-exception v8

    .line 955
    :cond_8
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    if-lez v8, :cond_9

    .line 956
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    .line 957
    :cond_9
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public getObject(Lorg/icepdf/index/core/util/Library;)Ljava/lang/Object;
    .locals 46
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/index/core/exceptions/PDFException;
        }
    .end annotation

    .prologue
    .line 80
    const/4 v15, 0x0

    .line 81
    .local v15, "deepnessCount":I
    const/16 v20, 0x0

    .line 82
    .local v20, "inObject":Z
    const/4 v12, 0x0

    .line 84
    .local v12, "complete":Z
    const/16 v26, 0x0

    .local v26, "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    move-object/from16 v27, v26

    .line 92
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .local v27, "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :goto_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/core/util/Parser;->getToken()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v23

    .line 106
    .local v23, "nextToken":Ljava/lang/Object;
    :try_start_1
    move-object/from16 v0, v23

    instance-of v10, v0, Lorg/icepdf/index/core/pobjects/StringObject;

    if-nez v10, :cond_0

    move-object/from16 v0, v23

    instance-of v10, v0, Lorg/icepdf/index/core/pobjects/Name;

    if-nez v10, :cond_0

    move-object/from16 v0, v23

    instance-of v10, v0, Ljava/lang/Number;

    if-eqz v10, :cond_2

    .line 111
    :cond_0
    move-object/from16 v0, v23

    instance-of v10, v0, Lorg/icepdf/index/core/pobjects/StringObject;

    if-eqz v10, :cond_1

    .line 112
    move-object/from16 v0, v23

    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move-object/from16 v34, v0

    .line 113
    .local v34, "tmp":Lorg/icepdf/index/core/pobjects/StringObject;
    move-object/from16 v0, v34

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Lorg/icepdf/index/core/pobjects/StringObject;->setReference(Lorg/icepdf/index/core/pobjects/Reference;)V

    .line 115
    .end local v34    # "tmp":Lorg/icepdf/index/core/pobjects/StringObject;
    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-object/from16 v26, v27

    .line 495
    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :goto_1
    :try_start_2
    move-object/from16 v0, p0

    iget v10, v0, Lorg/icepdf/index/core/util/Parser;->parseMode:I

    const/16 v43, 0x1

    move/from16 v0, v43

    if-ne v10, v0, :cond_2f

    if-nez v15, :cond_2f

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->size()I

    move-result v10

    if-lez v10, :cond_2f

    .line 496
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8

    move-result-object v10

    .line 509
    .end local v23    # "nextToken":Ljava/lang/Object;
    :goto_2
    return-object v10

    .line 97
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :catch_0
    move-exception v16

    .line 102
    .local v16, "e":Ljava/io/IOException;
    const/4 v10, 0x0

    move-object/from16 v26, v27

    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto :goto_2

    .line 118
    .end local v16    # "e":Ljava/io/IOException;
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v23    # "nextToken":Ljava/lang/Object;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_2
    :try_start_3
    const-string/jumbo v10, "obj"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 123
    const/4 v15, 0x1

    .line 124
    const/16 v20, 0x1

    .line 125
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    move-object v0, v10

    check-cast v0, Ljava/lang/Number;

    move-object/from16 v17, v0

    .line 126
    .local v17, "generationNumber":Ljava/lang/Number;
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    move-object v0, v10

    check-cast v0, Ljava/lang/Number;

    move-object/from16 v25, v0

    .line 127
    .local v25, "objectNumber":Ljava/lang/Number;
    new-instance v26, Lorg/icepdf/index/core/pobjects/Reference;

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/Reference;-><init>(Ljava/lang/Number;Ljava/lang/Number;)V

    .line 129
    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto :goto_1

    .line 131
    .end local v17    # "generationNumber":Ljava/lang/Number;
    .end local v25    # "objectNumber":Ljava/lang/Number;
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_3
    const-string/jumbo v10, "endobj"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 132
    add-int/lit8 v15, v15, -0x1

    .line 134
    if-eqz v20, :cond_4

    .line 136
    const/16 v20, 0x0

    .line 138
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Parser;->addPObject(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/pobjects/Reference;)Lorg/icepdf/index/core/pobjects/PObject;

    move-result-object v10

    move-object/from16 v26, v27

    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto :goto_2

    .line 142
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_4
    const/4 v10, 0x0

    move-object/from16 v26, v27

    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto :goto_2

    .line 148
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_5
    const-string/jumbo v10, "endstream"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 149
    add-int/lit8 v15, v15, -0x1

    .line 151
    if-eqz v20, :cond_2e

    .line 152
    const/16 v20, 0x0

    .line 154
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Parser;->addPObject(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/pobjects/Reference;)Lorg/icepdf/index/core/pobjects/PObject;

    move-result-object v10

    move-object/from16 v26, v27

    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto :goto_2

    .line 161
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_6
    const-string/jumbo v10, "stream"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_17

    .line 163
    add-int/lit8 v15, v15, 0x1

    .line 165
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lorg/icepdf/index/core/util/Hashtable;

    .line 168
    .local v32, "streamHash":Lorg/icepdf/index/core/util/Hashtable;
    const-string/jumbo v10, "Length"

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v10}, Lorg/icepdf/index/core/util/Library;->getInt(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    move-result v33

    .line 172
    .local v33, "streamLength":I
    const/16 v28, 0x0

    .line 188
    .local v28, "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    const/16 v43, 0x2

    move/from16 v0, v43

    invoke-virtual {v10, v0}, Ljava/io/InputStream;->mark(I)V

    .line 191
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->read()I

    move-result v13

    .line 192
    .local v13, "curChar":I
    const/16 v10, 0xd

    if-ne v13, v10, :cond_a

    .line 193
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    const/16 v43, 0x1

    move/from16 v0, v43

    invoke-virtual {v10, v0}, Ljava/io/InputStream;->mark(I)V

    .line 194
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->read()I

    move-result v10

    const/16 v43, 0xa

    move/from16 v0, v43

    if-eq v10, v0, :cond_7

    .line 195
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->reset()V

    .line 215
    :cond_7
    :goto_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    instance-of v10, v10, Lorg/icepdf/index/core/io/SeekableInput;

    if-eqz v10, :cond_d

    .line 216
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    check-cast v5, Lorg/icepdf/index/core/io/SeekableInput;

    .line 217
    .local v5, "streamDataInput":Lorg/icepdf/index/core/io/SeekableInput;
    invoke-interface {v5}, Lorg/icepdf/index/core/io/SeekableInput;->getAbsolutePosition()J

    move-result-wide v6

    .line 221
    .local v6, "filePositionOfStreamData":J
    if-lez v33, :cond_c

    .line 222
    move/from16 v0, v33

    int-to-long v8, v0

    .line 223
    .local v8, "lengthOfStreamData":J
    move/from16 v0, v33

    int-to-long v0, v0

    move-wide/from16 v44, v0

    move-wide/from16 v0, v44

    invoke-interface {v5, v0, v1}, Lorg/icepdf/index/core/io/SeekableInput;->seekRelative(J)V

    .line 226
    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lorg/icepdf/index/core/util/Parser;->skipUntilEndstream(Ljava/io/OutputStream;)J

    move-result-wide v44

    add-long v8, v8, v44

    .line 230
    :goto_4
    new-instance v4, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    const/4 v10, 0x0

    invoke-direct/range {v4 .. v10}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;-><init>(Lorg/icepdf/index/core/io/SeekableInput;JJZ)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 283
    .local v4, "streamInputWrapper":Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;
    :goto_5
    if-eqz v28, :cond_8

    .line 285
    :try_start_5
    invoke-virtual/range {v28 .. v28}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 291
    :cond_8
    :goto_6
    const/16 v36, 0x0

    .line 293
    .local v36, "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    const/16 v31, 0x0

    .line 295
    .local v31, "stream":Lorg/icepdf/index/core/pobjects/Stream;
    :try_start_6
    const-string/jumbo v10, "Type"

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v10}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lorg/icepdf/index/core/pobjects/Name;

    .line 296
    .local v39, "type":Lorg/icepdf/index/core/pobjects/Name;
    const-string/jumbo v10, "Subtype"

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v10}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    .line 297
    if-eqz v39, :cond_9

    .line 299
    const-string/jumbo v10, "Pattern"

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Lorg/icepdf/index/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    .line 335
    :cond_9
    :goto_7
    if-eqz v36, :cond_15

    .line 336
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v36

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    :goto_8
    move-object/from16 v26, v27

    .line 345
    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_1

    .line 199
    .end local v4    # "streamInputWrapper":Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;
    .end local v5    # "streamDataInput":Lorg/icepdf/index/core/io/SeekableInput;
    .end local v6    # "filePositionOfStreamData":J
    .end local v8    # "lengthOfStreamData":J
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .end local v31    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    .end local v36    # "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    .end local v39    # "type":Lorg/icepdf/index/core/pobjects/Name;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_a
    const/16 v10, 0xa

    if-eq v13, v10, :cond_7

    .line 204
    :try_start_7
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->reset()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    .line 280
    .end local v13    # "curChar":I
    :catch_1
    move-exception v16

    .line 281
    .restart local v16    # "e":Ljava/io/IOException;
    :goto_9
    const/4 v10, 0x0

    .line 283
    if-eqz v28, :cond_b

    .line 285
    :try_start_8
    invoke-virtual/range {v28 .. v28}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    :cond_b
    :goto_a
    move-object/from16 v26, v27

    .line 288
    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_2

    .line 228
    .end local v16    # "e":Ljava/io/IOException;
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v5    # "streamDataInput":Lorg/icepdf/index/core/io/SeekableInput;
    .restart local v6    # "filePositionOfStreamData":J
    .restart local v13    # "curChar":I
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_c
    const/4 v10, 0x0

    :try_start_9
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lorg/icepdf/index/core/util/Parser;->captureStreamData(Ljava/io/OutputStream;)J

    move-result-wide v8

    .restart local v8    # "lengthOfStreamData":J
    goto :goto_4

    .line 241
    .end local v5    # "streamDataInput":Lorg/icepdf/index/core/io/SeekableInput;
    .end local v6    # "filePositionOfStreamData":J
    .end local v8    # "lengthOfStreamData":J
    :cond_d
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/core/util/Library;->isLinearTraversal()Z

    move-result v10

    if-nez v10, :cond_10

    if-lez v33, :cond_10

    .line 242
    move/from16 v0, v33

    new-array v11, v0, [B

    .line 243
    .local v11, "buffer":[B
    const/16 v35, 0x0

    .line 244
    .local v35, "totalRead":I
    :goto_b
    array-length v10, v11

    move/from16 v0, v35

    if-ge v0, v10, :cond_e

    .line 245
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    array-length v0, v11

    move/from16 v43, v0

    sub-int v43, v43, v35

    move/from16 v0, v35

    move/from16 v1, v43

    invoke-virtual {v10, v11, v0, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v14

    .line 249
    .local v14, "currRead":I
    if-gtz v14, :cond_f

    .line 254
    .end local v14    # "currRead":I
    :cond_e
    new-instance v29, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;

    move-object/from16 v0, p1

    iget-object v10, v0, Lorg/icepdf/index/core/util/Library;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    move-object/from16 v0, v29

    invoke-direct {v0, v11, v10}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;-><init>([BLorg/icepdf/index/core/util/MemoryManager;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 258
    .end local v28    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .local v29, "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    :try_start_a
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/util/Parser;->skipUntilEndstream(Ljava/io/OutputStream;)J
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    move-object/from16 v28, v29

    .line 269
    .end local v11    # "buffer":[B
    .end local v29    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v35    # "totalRead":I
    .restart local v28    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    :goto_c
    :try_start_b
    invoke-virtual/range {v28 .. v28}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->size()I

    move-result v30

    .line 270
    .local v30, "size":I
    invoke-virtual/range {v28 .. v28}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->trim()Z

    .line 271
    invoke-virtual/range {v28 .. v28}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->relinquishByteArray()[B

    move-result-object v11

    .line 273
    .restart local v11    # "buffer":[B
    new-instance v5, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;

    invoke-direct {v5, v11}, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;-><init>([B)V

    .line 274
    .restart local v5    # "streamDataInput":Lorg/icepdf/index/core/io/SeekableInput;
    const-wide/16 v6, 0x0

    .line 275
    .restart local v6    # "filePositionOfStreamData":J
    move/from16 v0, v30

    int-to-long v8, v0

    .line 276
    .restart local v8    # "lengthOfStreamData":J
    new-instance v4, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    const/4 v10, 0x1

    invoke-direct/range {v4 .. v10}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;-><init>(Lorg/icepdf/index/core/io/SeekableInput;JJZ)V

    .restart local v4    # "streamInputWrapper":Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;
    goto/16 :goto_5

    .line 251
    .end local v4    # "streamInputWrapper":Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;
    .end local v5    # "streamDataInput":Lorg/icepdf/index/core/io/SeekableInput;
    .end local v6    # "filePositionOfStreamData":J
    .end local v8    # "lengthOfStreamData":J
    .end local v30    # "size":I
    .restart local v14    # "currRead":I
    .restart local v35    # "totalRead":I
    :cond_f
    add-int v35, v35, v14

    .line 253
    goto :goto_b

    .line 264
    .end local v11    # "buffer":[B
    .end local v14    # "currRead":I
    .end local v35    # "totalRead":I
    :cond_10
    new-instance v29, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;

    const/16 v10, 0x4000

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/icepdf/index/core/util/Library;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    move-object/from16 v43, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v43

    invoke-direct {v0, v10, v1}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;-><init>(ILorg/icepdf/index/core/util/MemoryManager;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 266
    .end local v28    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v29    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    :try_start_c
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/util/Parser;->captureStreamData(Ljava/io/OutputStream;)J
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    move-object/from16 v28, v29

    .end local v29    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v28    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    goto :goto_c

    .line 283
    .end local v13    # "curChar":I
    :catchall_0
    move-exception v10

    :goto_d
    if-eqz v28, :cond_11

    .line 285
    :try_start_d
    invoke-virtual/range {v28 .. v28}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_5
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2

    .line 288
    :cond_11
    :goto_e
    :try_start_e
    throw v10

    .line 504
    .end local v23    # "nextToken":Ljava/lang/Object;
    .end local v28    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v32    # "streamHash":Lorg/icepdf/index/core/util/Hashtable;
    .end local v33    # "streamLength":I
    :catch_2
    move-exception v16

    move-object/from16 v26, v27

    .line 506
    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .local v16, "e":Ljava/lang/Exception;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :goto_f
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 301
    .end local v16    # "e":Ljava/lang/Exception;
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v4    # "streamInputWrapper":Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;
    .restart local v5    # "streamDataInput":Lorg/icepdf/index/core/io/SeekableInput;
    .restart local v6    # "filePositionOfStreamData":J
    .restart local v8    # "lengthOfStreamData":J
    .restart local v13    # "curChar":I
    .restart local v23    # "nextToken":Ljava/lang/Object;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v28    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v31    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    .restart local v32    # "streamHash":Lorg/icepdf/index/core/util/Hashtable;
    .restart local v33    # "streamLength":I
    .restart local v36    # "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    .restart local v39    # "type":Lorg/icepdf/index/core/pobjects/Name;
    :cond_12
    const-string/jumbo v10, "XRef"

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Lorg/icepdf/index/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_14

    .line 302
    new-instance v31, Lorg/icepdf/index/core/pobjects/Stream;

    .end local v31    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    move-object/from16 v0, v31

    move-object/from16 v1, p1

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2, v4}, Lorg/icepdf/index/core/pobjects/Stream;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;)V

    .line 303
    .restart local v31    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    invoke-virtual/range {v31 .. v31}, Lorg/icepdf/index/core/pobjects/Stream;->init()V

    .line 304
    invoke-virtual/range {v31 .. v31}, Lorg/icepdf/index/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v19

    .line 305
    .local v19, "in":Ljava/io/InputStream;
    new-instance v41, Lorg/icepdf/index/core/pobjects/CrossReference;

    invoke-direct/range {v41 .. v41}, Lorg/icepdf/index/core/pobjects/CrossReference;-><init>()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_2

    .line 306
    .local v41, "xrefStream":Lorg/icepdf/index/core/pobjects/CrossReference;
    if-eqz v19, :cond_13

    .line 308
    :try_start_f
    move-object/from16 v0, v41

    move-object/from16 v1, p1

    move-object/from16 v2, v32

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lorg/icepdf/index/core/pobjects/CrossReference;->addXRefStreamEntries(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Ljava/io/InputStream;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 312
    :try_start_10
    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_6
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_2

    .line 319
    :cond_13
    :goto_10
    const/4 v10, 0x0

    :try_start_11
    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Lorg/icepdf/index/core/pobjects/Stream;->dispose(Z)V

    .line 323
    invoke-virtual/range {v32 .. v32}, Lorg/icepdf/index/core/util/Hashtable;->clone()Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Lorg/icepdf/index/core/util/Hashtable;

    .line 324
    .local v38, "trailerHash":Lorg/icepdf/index/core/util/Hashtable;
    new-instance v36, Lorg/icepdf/index/core/pobjects/PTrailer;

    .end local v36    # "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    const/4 v10, 0x0

    move-object/from16 v0, v36

    move-object/from16 v1, p1

    move-object/from16 v2, v38

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v10, v3}, Lorg/icepdf/index/core/pobjects/PTrailer;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Lorg/icepdf/index/core/pobjects/CrossReference;Lorg/icepdf/index/core/pobjects/CrossReference;)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_2

    .line 325
    .restart local v36    # "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    goto/16 :goto_7

    .line 311
    .end local v38    # "trailerHash":Lorg/icepdf/index/core/util/Hashtable;
    :catchall_1
    move-exception v10

    .line 312
    :try_start_12
    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_7
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_2

    .line 316
    :goto_11
    :try_start_13
    throw v10

    .line 325
    .end local v19    # "in":Ljava/io/InputStream;
    .end local v41    # "xrefStream":Lorg/icepdf/index/core/pobjects/CrossReference;
    :cond_14
    const-string/jumbo v10, "ObjStm"

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Lorg/icepdf/index/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 326
    new-instance v31, Lorg/icepdf/index/core/pobjects/ObjectStream;

    .end local v31    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    move-object/from16 v0, v31

    move-object/from16 v1, p1

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2, v4}, Lorg/icepdf/index/core/pobjects/ObjectStream;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;)V

    .restart local v31    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    goto/16 :goto_7

    .line 340
    :cond_15
    if-nez v31, :cond_16

    .line 341
    new-instance v31, Lorg/icepdf/index/core/pobjects/Stream;

    .end local v31    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    move-object/from16 v0, v31

    move-object/from16 v1, p1

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2, v4}, Lorg/icepdf/index/core/pobjects/Stream;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;)V

    .line 343
    .restart local v31    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    :cond_16
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v31

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_8

    .line 349
    .end local v4    # "streamInputWrapper":Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;
    .end local v5    # "streamDataInput":Lorg/icepdf/index/core/io/SeekableInput;
    .end local v6    # "filePositionOfStreamData":J
    .end local v8    # "lengthOfStreamData":J
    .end local v13    # "curChar":I
    .end local v28    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v31    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    .end local v32    # "streamHash":Lorg/icepdf/index/core/util/Hashtable;
    .end local v33    # "streamLength":I
    .end local v36    # "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    .end local v39    # "type":Lorg/icepdf/index/core/pobjects/Name;
    :cond_17
    const-string/jumbo v10, "true"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_18

    .line 350
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    const/16 v43, 0x1

    invoke-static/range {v43 .. v43}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v26, v27

    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_1

    .line 351
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_18
    const-string/jumbo v10, "false"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_19

    .line 352
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    const/16 v43, 0x0

    invoke-static/range {v43 .. v43}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v26, v27

    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_1

    .line 355
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_19
    const-string/jumbo v10, "R"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1a

    .line 357
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    move-object v0, v10

    check-cast v0, Ljava/lang/Number;

    move-object/from16 v17, v0

    .line 358
    .restart local v17    # "generationNumber":Ljava/lang/Number;
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    move-object v0, v10

    check-cast v0, Ljava/lang/Number;

    move-object/from16 v25, v0

    .line 359
    .restart local v25    # "objectNumber":Ljava/lang/Number;
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    new-instance v43, Lorg/icepdf/index/core/pobjects/Reference;

    move-object/from16 v0, v43

    move-object/from16 v1, v25

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/Reference;-><init>(Ljava/lang/Number;Ljava/lang/Number;)V

    move-object/from16 v0, v43

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v26, v27

    .line 361
    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_1

    .end local v17    # "generationNumber":Ljava/lang/Number;
    .end local v25    # "objectNumber":Ljava/lang/Number;
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_1a
    const-string/jumbo v10, "["

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1b

    .line 362
    add-int/lit8 v15, v15, 0x1

    .line 363
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v26, v27

    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_1

    .line 366
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_1b
    const-string/jumbo v10, "]"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1e

    .line 367
    add-int/lit8 v15, v15, -0x1

    .line 368
    new-instance v40, Ljava/util/Vector;

    invoke-direct/range {v40 .. v40}, Ljava/util/Vector;-><init>()V

    .line 369
    .local v40, "v":Ljava/util/Vector;
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v24

    .line 370
    .local v24, "obj":Ljava/lang/Object;
    :goto_12
    move-object/from16 v0, v24

    instance-of v10, v0, Ljava/lang/String;

    if-eqz v10, :cond_1c

    const-string/jumbo v10, "["

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1d

    .line 372
    :cond_1c
    const/4 v10, 0x0

    move-object/from16 v0, v40

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v10}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 373
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v24

    goto :goto_12

    .line 375
    :cond_1d
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v40

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v26, v27

    .line 376
    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_1

    .end local v24    # "obj":Ljava/lang/Object;
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .end local v40    # "v":Ljava/util/Vector;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_1e
    const-string/jumbo v10, "<<"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1f

    .line 378
    add-int/lit8 v15, v15, 0x1

    .line 379
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v26, v27

    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_1

    .line 382
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_1f
    const-string/jumbo v10, ">>"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_29

    .line 384
    add-int/lit8 v15, v15, -0x1

    .line 385
    new-instance v18, Lorg/icepdf/index/core/util/Hashtable;

    invoke-direct/range {v18 .. v18}, Lorg/icepdf/index/core/util/Hashtable;-><init>()V

    .line 387
    .local v18, "hashTable":Lorg/icepdf/index/core/util/Hashtable;
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_28

    .line 388
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v24

    .line 392
    .restart local v24    # "obj":Ljava/lang/Object;
    :goto_13
    move-object/from16 v0, v24

    instance-of v10, v0, Ljava/lang/String;

    if-eqz v10, :cond_20

    const-string/jumbo v10, "<<"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_21

    :cond_20
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_21

    .line 393
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v21

    .line 396
    .local v21, "key":Ljava/lang/Object;
    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_21

    .line 398
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v24

    .line 402
    goto :goto_13

    .line 403
    .end local v21    # "key":Ljava/lang/Object;
    :cond_21
    const-string/jumbo v10, "Type"

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    .line 406
    if-eqz v24, :cond_27

    move-object/from16 v0, v24

    instance-of v10, v0, Lorg/icepdf/index/core/pobjects/Name;

    if-eqz v10, :cond_27

    .line 407
    move-object/from16 v0, v24

    check-cast v0, Lorg/icepdf/index/core/pobjects/Name;

    move-object/from16 v22, v0

    .line 409
    .local v22, "n":Lorg/icepdf/index/core/pobjects/Name;
    const-string/jumbo v10, "Catalog"

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Lorg/icepdf/index/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_22

    .line 410
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    new-instance v43, Lorg/icepdf/index/core/pobjects/Catalog;

    move-object/from16 v0, v43

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/Catalog;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    move-object/from16 v0, v43

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    .end local v22    # "n":Lorg/icepdf/index/core/pobjects/Name;
    :goto_14
    if-nez v15, :cond_28

    .line 436
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    move-object/from16 v26, v27

    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_2

    .line 411
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v22    # "n":Lorg/icepdf/index/core/pobjects/Name;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_22
    const-string/jumbo v10, "Pages"

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Lorg/icepdf/index/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_23

    .line 412
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    new-instance v43, Lorg/icepdf/index/core/pobjects/PageTree;

    move-object/from16 v0, v43

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/PageTree;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    move-object/from16 v0, v43

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_14

    .line 413
    :cond_23
    const-string/jumbo v10, "Page"

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Lorg/icepdf/index/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_24

    .line 414
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    new-instance v43, Lorg/icepdf/index/core/pobjects/Page;

    move-object/from16 v0, v43

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/Page;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    move-object/from16 v0, v43

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_14

    .line 415
    :cond_24
    const-string/jumbo v10, "Font"

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Lorg/icepdf/index/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_25

    .line 416
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-static {}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->getInstance()Lorg/icepdf/index/core/pobjects/fonts/FontFactory;

    move-result-object v43

    move-object/from16 v0, v43

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->getFont(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_14

    .line 418
    :cond_25
    const-string/jumbo v10, "FontDescriptor"

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Lorg/icepdf/index/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_26

    .line 419
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    new-instance v43, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v0, v43

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    move-object/from16 v0, v43

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_14

    .line 426
    :cond_26
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_14

    .line 431
    .end local v22    # "n":Lorg/icepdf/index/core/pobjects/Name;
    :cond_27
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_14

    .end local v24    # "obj":Ljava/lang/Object;
    :cond_28
    move-object/from16 v26, v27

    .line 438
    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_1

    .line 470
    .end local v18    # "hashTable":Lorg/icepdf/index/core/util/Hashtable;
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_29
    const-string/jumbo v10, "xref"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2a

    .line 472
    new-instance v42, Lorg/icepdf/index/core/pobjects/CrossReference;

    invoke-direct/range {v42 .. v42}, Lorg/icepdf/index/core/pobjects/CrossReference;-><init>()V

    .line 473
    .local v42, "xrefTable":Lorg/icepdf/index/core/pobjects/CrossReference;
    move-object/from16 v0, v42

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/pobjects/CrossReference;->addXRefTableEntries(Lorg/icepdf/index/core/util/Parser;)V

    .line 474
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v42

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v26, v27

    .line 475
    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_1

    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .end local v42    # "xrefTable":Lorg/icepdf/index/core/pobjects/CrossReference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_2a
    const-string/jumbo v10, "trailer"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2c

    .line 476
    const/16 v42, 0x0

    .line 477
    .restart local v42    # "xrefTable":Lorg/icepdf/index/core/pobjects/CrossReference;
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v10

    instance-of v10, v10, Lorg/icepdf/index/core/pobjects/CrossReference;

    if-eqz v10, :cond_2b

    .line 478
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v42

    .end local v42    # "xrefTable":Lorg/icepdf/index/core/pobjects/CrossReference;
    check-cast v42, Lorg/icepdf/index/core/pobjects/CrossReference;

    .line 479
    .restart local v42    # "xrefTable":Lorg/icepdf/index/core/pobjects/CrossReference;
    :cond_2b
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->clear()V

    .line 480
    invoke-virtual/range {p0 .. p1}, Lorg/icepdf/index/core/util/Parser;->getObject(Lorg/icepdf/index/core/util/Library;)Ljava/lang/Object;

    move-result-object v37

    check-cast v37, Lorg/icepdf/index/core/util/Hashtable;

    .line 484
    .local v37, "trailerDictionary":Lorg/icepdf/index/core/util/Hashtable;
    new-instance v10, Lorg/icepdf/index/core/pobjects/PTrailer;

    const/16 v43, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    move-object/from16 v2, v42

    move-object/from16 v3, v43

    invoke-direct {v10, v0, v1, v2, v3}, Lorg/icepdf/index/core/pobjects/PTrailer;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Lorg/icepdf/index/core/pobjects/CrossReference;Lorg/icepdf/index/core/pobjects/CrossReference;)V

    move-object/from16 v26, v27

    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_2

    .line 487
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .end local v37    # "trailerDictionary":Lorg/icepdf/index/core/util/Hashtable;
    .end local v42    # "xrefTable":Lorg/icepdf/index/core/pobjects/CrossReference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_2c
    move-object/from16 v0, v23

    instance-of v10, v0, Ljava/lang/String;

    if-eqz v10, :cond_2d

    move-object/from16 v0, v23

    check-cast v0, Ljava/lang/String;

    move-object v10, v0

    const-string/jumbo v43, "%"

    move-object/from16 v0, v43

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2d

    move-object/from16 v26, v27

    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_1

    .line 493
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_2d
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_2

    :cond_2e
    move-object/from16 v26, v27

    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_1

    .line 499
    :cond_2f
    if-eqz v12, :cond_30

    .line 509
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/index/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    goto/16 :goto_2

    .line 286
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v4    # "streamInputWrapper":Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;
    .restart local v5    # "streamDataInput":Lorg/icepdf/index/core/io/SeekableInput;
    .restart local v6    # "filePositionOfStreamData":J
    .restart local v8    # "lengthOfStreamData":J
    .restart local v13    # "curChar":I
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v28    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v32    # "streamHash":Lorg/icepdf/index/core/util/Hashtable;
    .restart local v33    # "streamLength":I
    :catch_3
    move-exception v10

    goto/16 :goto_6

    .end local v4    # "streamInputWrapper":Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;
    .end local v5    # "streamDataInput":Lorg/icepdf/index/core/io/SeekableInput;
    .end local v6    # "filePositionOfStreamData":J
    .end local v8    # "lengthOfStreamData":J
    .end local v13    # "curChar":I
    .local v16, "e":Ljava/io/IOException;
    :catch_4
    move-exception v43

    goto/16 :goto_a

    .end local v16    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v43

    goto/16 :goto_e

    .line 314
    .restart local v4    # "streamInputWrapper":Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;
    .restart local v5    # "streamDataInput":Lorg/icepdf/index/core/io/SeekableInput;
    .restart local v6    # "filePositionOfStreamData":J
    .restart local v8    # "lengthOfStreamData":J
    .restart local v13    # "curChar":I
    .restart local v19    # "in":Ljava/io/InputStream;
    .restart local v31    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    .restart local v36    # "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    .restart local v39    # "type":Lorg/icepdf/index/core/pobjects/Name;
    .restart local v41    # "xrefStream":Lorg/icepdf/index/core/pobjects/CrossReference;
    :catch_6
    move-exception v10

    goto/16 :goto_10

    :catch_7
    move-exception v43

    goto/16 :goto_11

    .line 504
    .end local v4    # "streamInputWrapper":Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;
    .end local v5    # "streamDataInput":Lorg/icepdf/index/core/io/SeekableInput;
    .end local v6    # "filePositionOfStreamData":J
    .end local v8    # "lengthOfStreamData":J
    .end local v13    # "curChar":I
    .end local v19    # "in":Ljava/io/InputStream;
    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .end local v28    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v31    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    .end local v32    # "streamHash":Lorg/icepdf/index/core/util/Hashtable;
    .end local v33    # "streamLength":I
    .end local v36    # "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    .end local v39    # "type":Lorg/icepdf/index/core/pobjects/Name;
    .end local v41    # "xrefStream":Lorg/icepdf/index/core/pobjects/CrossReference;
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :catch_8
    move-exception v16

    goto/16 :goto_f

    .line 283
    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v13    # "curChar":I
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v29    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v32    # "streamHash":Lorg/icepdf/index/core/util/Hashtable;
    .restart local v33    # "streamLength":I
    :catchall_2
    move-exception v10

    move-object/from16 v28, v29

    .end local v29    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v28    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    goto/16 :goto_d

    .line 280
    .end local v28    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v29    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    :catch_9
    move-exception v16

    move-object/from16 v28, v29

    .end local v29    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v28    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    goto/16 :goto_9

    .end local v13    # "curChar":I
    .end local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .end local v28    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v32    # "streamHash":Lorg/icepdf/index/core/util/Hashtable;
    .end local v33    # "streamLength":I
    .restart local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_30
    move-object/from16 v27, v26

    .end local v26    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/index/core/pobjects/Reference;
    goto/16 :goto_0
.end method

.method public getStreamObject()Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 553
    const-string/jumbo v3, "<<"

    .line 554
    .local v3, "PDF_TAG_OPEN":Ljava/lang/String;
    const-string/jumbo v2, ">>"

    .line 555
    .local v2, "PDF_TAG_CLOSE":Ljava/lang/String;
    const-string/jumbo v1, "["

    .line 556
    .local v1, "ARRAY_OPEN":Ljava/lang/String;
    const-string/jumbo v0, "]"

    .line 558
    .local v0, "ARRAY_CLOSE":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/icepdf/index/core/util/Parser;->getToken()Ljava/lang/Object;

    move-result-object v5

    .line 560
    .local v5, "o":Ljava/lang/Object;
    :try_start_0
    instance-of v8, v5, Ljava/lang/String;

    if-eqz v8, :cond_1

    .line 561
    const-string/jumbo v8, "<<"

    invoke-virtual {v5, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 562
    new-instance v4, Lorg/icepdf/index/core/util/Hashtable;

    invoke-direct {v4}, Lorg/icepdf/index/core/util/Hashtable;-><init>()V

    .line 563
    .local v4, "h":Lorg/icepdf/index/core/util/Hashtable;
    invoke-virtual {p0}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v6

    .line 564
    .local v6, "o1":Ljava/lang/Object;
    :goto_0
    const-string/jumbo v8, ">>"

    invoke-virtual {v6, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 565
    invoke-virtual {p0}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v4, v6, v8}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    invoke-virtual {p0}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v6

    goto :goto_0

    .line 568
    :cond_0
    move-object v5, v4

    .line 584
    .end local v4    # "h":Lorg/icepdf/index/core/util/Hashtable;
    .end local v5    # "o":Ljava/lang/Object;
    .end local v6    # "o1":Ljava/lang/Object;
    :cond_1
    :goto_1
    return-object v5

    .line 572
    .restart local v5    # "o":Ljava/lang/Object;
    :cond_2
    const-string/jumbo v8, "["

    invoke-virtual {v5, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 573
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    .line 574
    .local v7, "v":Ljava/util/Vector;
    invoke-virtual {p0}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v6

    .line 575
    .restart local v6    # "o1":Ljava/lang/Object;
    :goto_2
    const-string/jumbo v8, "]"

    invoke-virtual {v6, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 576
    invoke-virtual {v7, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 577
    invoke-virtual {p0}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_2

    .line 579
    :cond_3
    move-object v5, v7

    .local v5, "o":Ljava/util/Vector;
    goto :goto_1

    .line 582
    .end local v6    # "o1":Ljava/lang/Object;
    .end local v7    # "v":Ljava/util/Vector;
    .local v5, "o":Ljava/lang/Object;
    :catch_0
    move-exception v8

    goto :goto_1
.end method

.method public getToken()Ljava/lang/Object;
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 599
    const/4 v13, 0x0

    .line 600
    .local v13, "inString":Z
    const/4 v10, 0x0

    .line 605
    .local v10, "hexString":Z
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->read()I

    move-result v5

    .line 607
    .local v5, "currentByte":I
    if-gez v5, :cond_1

    .line 608
    new-instance v17, Ljava/io/IOException;

    invoke-direct/range {v17 .. v17}, Ljava/io/IOException;-><init>()V

    throw v17

    .line 610
    :cond_1
    int-to-char v6, v5

    .line 612
    .local v6, "currentChar":C
    invoke-static {v6}, Lorg/icepdf/index/core/util/Parser;->isWhitespace(C)Z

    move-result v17

    if-nez v17, :cond_0

    .line 620
    const/16 v17, 0x28

    move/from16 v0, v17

    if-ne v6, v0, :cond_3

    .line 622
    const/4 v13, 0x1

    .line 650
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Ljava/io/InputStream;->mark(I)V

    .line 653
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->read()I

    move-result v17

    move/from16 v0, v17

    int-to-char v14, v0

    .line 656
    .local v14, "nextChar":C
    const/16 v17, 0x3e

    move/from16 v0, v17

    if-ne v6, v0, :cond_a

    const/16 v17, 0x3e

    move/from16 v0, v17

    if-ne v14, v0, :cond_a

    .line 657
    const-string/jumbo v17, ">>"

    .line 900
    .end local v14    # "nextChar":C
    :goto_0
    return-object v17

    .line 623
    :cond_3
    const/16 v17, 0x5d

    move/from16 v0, v17

    if-ne v6, v0, :cond_4

    .line 625
    const-string/jumbo v17, "]"

    goto :goto_0

    .line 626
    :cond_4
    const/16 v17, 0x5b

    move/from16 v0, v17

    if-ne v6, v0, :cond_5

    .line 628
    const-string/jumbo v17, "["

    goto :goto_0

    .line 629
    :cond_5
    const/16 v17, 0x25

    move/from16 v0, v17

    if-ne v6, v0, :cond_2

    .line 632
    new-instance v16, Ljava/lang/StringBuffer;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuffer;-><init>()V

    .line 634
    .local v16, "stringBuffer":Ljava/lang/StringBuffer;
    :cond_6
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 635
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->read()I

    move-result v5

    .line 636
    if-gez v5, :cond_8

    .line 638
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    if-lez v17, :cond_7

    .line 639
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    goto :goto_0

    .line 640
    :cond_7
    new-instance v17, Ljava/io/IOException;

    invoke-direct/range {v17 .. v17}, Ljava/io/IOException;-><init>()V

    throw v17

    .line 642
    :cond_8
    int-to-char v6, v5

    .line 644
    const/16 v17, 0xd

    move/from16 v0, v17

    if-eq v6, v0, :cond_9

    const/16 v17, 0xa

    move/from16 v0, v17

    if-ne v6, v0, :cond_6

    .line 646
    :cond_9
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    goto :goto_0

    .line 659
    .end local v16    # "stringBuffer":Ljava/lang/StringBuffer;
    .restart local v14    # "nextChar":C
    :cond_a
    const/16 v17, 0x3c

    move/from16 v0, v17

    if-ne v6, v0, :cond_c

    .line 661
    const/16 v17, 0x3c

    move/from16 v0, v17

    if-ne v14, v0, :cond_b

    .line 662
    const-string/jumbo v17, "<<"

    goto :goto_0

    .line 666
    :cond_b
    const/4 v13, 0x1

    .line 667
    const/4 v10, 0x1

    .line 672
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->reset()V

    .line 675
    new-instance v16, Ljava/lang/StringBuffer;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuffer;-><init>()V

    .line 676
    .restart local v16    # "stringBuffer":Ljava/lang/StringBuffer;
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 682
    const/4 v15, 0x0

    .line 683
    .local v15, "parenthesisCount":I
    const/4 v3, 0x0

    .line 686
    .local v3, "complete":Z
    const/4 v12, 0x0

    .line 691
    .local v12, "ignoreChar":Z
    :cond_d
    if-nez v13, :cond_e

    .line 692
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Ljava/io/InputStream;->mark(I)V

    .line 695
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->read()I

    move-result v5

    .line 699
    if-ltz v5, :cond_10

    .line 700
    int-to-char v6, v5

    .line 706
    if-eqz v13, :cond_1e

    .line 707
    if-eqz v10, :cond_11

    .line 709
    const/16 v17, 0x3e

    move/from16 v0, v17

    if-ne v6, v0, :cond_17

    .line 710
    const/4 v3, 0x1

    .line 711
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 859
    :cond_f
    :goto_1
    if-eqz v10, :cond_22

    .line 861
    new-instance v17, Lorg/icepdf/index/core/pobjects/HexStringObject;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/pobjects/HexStringObject;-><init>(Ljava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 702
    :cond_10
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_0

    .line 716
    :cond_11
    const/16 v17, 0x28

    move/from16 v0, v17

    if-ne v6, v0, :cond_12

    .line 717
    add-int/lit8 v15, v15, 0x1

    .line 719
    :cond_12
    const/16 v17, 0x29

    move/from16 v0, v17

    if-ne v6, v0, :cond_14

    .line 720
    if-nez v15, :cond_13

    .line 721
    const/4 v3, 0x1

    .line 722
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 725
    :cond_13
    add-int/lit8 v15, v15, -0x1

    .line 744
    :cond_14
    const/16 v17, 0x5c

    move/from16 v0, v17

    if-ne v6, v0, :cond_17

    .line 746
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->read()I

    move-result v17

    move/from16 v0, v17

    int-to-char v6, v0

    .line 750
    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v17

    if-eqz v17, :cond_18

    .line 752
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 753
    .local v7, "digit":Ljava/lang/StringBuffer;
    invoke-virtual {v7, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 756
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    const/16 v17, 0x2

    move/from16 v0, v17

    if-ge v11, v0, :cond_16

    .line 759
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Ljava/io/InputStream;->mark(I)V

    .line 761
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->read()I

    move-result v17

    move/from16 v0, v17

    int-to-char v6, v0

    .line 762
    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v17

    if-eqz v17, :cond_15

    .line 763
    invoke-virtual {v7, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 756
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 767
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->reset()V

    .line 773
    :cond_16
    const/4 v2, 0x0

    .line 775
    .local v2, "charNumber":I
    :try_start_0
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x8

    invoke-static/range {v17 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 781
    :goto_3
    int-to-char v6, v2

    .line 845
    .end local v2    # "charNumber":I
    .end local v7    # "digit":Ljava/lang/StringBuffer;
    .end local v11    # "i":I
    :cond_17
    :goto_4
    if-nez v12, :cond_21

    .line 846
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 853
    :goto_5
    if-eqz v3, :cond_d

    goto/16 :goto_1

    .line 784
    :cond_18
    const/16 v17, 0x28

    move/from16 v0, v17

    if-eq v6, v0, :cond_17

    const/16 v17, 0x29

    move/from16 v0, v17

    if-eq v6, v0, :cond_17

    const/16 v17, 0x5c

    move/from16 v0, v17

    if-eq v6, v0, :cond_17

    .line 790
    const/16 v17, 0x74

    move/from16 v0, v17

    if-ne v6, v0, :cond_19

    .line 791
    const/16 v6, 0x9

    goto :goto_4

    .line 794
    :cond_19
    const/16 v17, 0x72

    move/from16 v0, v17

    if-ne v6, v0, :cond_1a

    .line 795
    const/16 v6, 0xd

    goto :goto_4

    .line 798
    :cond_1a
    const/16 v17, 0x6e

    move/from16 v0, v17

    if-ne v6, v0, :cond_1b

    .line 799
    const/16 v6, 0xa

    goto :goto_4

    .line 802
    :cond_1b
    const/16 v17, 0x62

    move/from16 v0, v17

    if-ne v6, v0, :cond_1c

    .line 803
    const/16 v6, 0x8

    goto :goto_4

    .line 806
    :cond_1c
    const/16 v17, 0x66

    move/from16 v0, v17

    if-ne v6, v0, :cond_1d

    .line 807
    const/16 v6, 0xc

    goto :goto_4

    .line 810
    :cond_1d
    const/16 v17, 0xd

    move/from16 v0, v17

    if-ne v6, v0, :cond_17

    .line 811
    const/4 v12, 0x1

    goto :goto_4

    .line 825
    :cond_1e
    invoke-static {v6}, Lorg/icepdf/index/core/util/Parser;->isWhitespace(C)Z

    move-result v17

    if-eqz v17, :cond_20

    .line 829
    const/16 v17, 0xd

    move/from16 v0, v17

    if-eq v5, v0, :cond_1f

    const/16 v17, 0xa

    move/from16 v0, v17

    if-ne v5, v0, :cond_f

    .line 830
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->reset()V

    goto/16 :goto_1

    .line 838
    :cond_20
    invoke-static {v6}, Lorg/icepdf/index/core/util/Parser;->isDelimiter(C)Z

    move-result v17

    if-eqz v17, :cond_17

    .line 840
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->reset()V

    goto/16 :goto_1

    .line 850
    :cond_21
    const/4 v12, 0x0

    goto :goto_5

    .line 867
    :cond_22
    if-eqz v13, :cond_23

    .line 868
    new-instance v17, Lorg/icepdf/index/core/pobjects/LiteralStringObject;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/pobjects/LiteralStringObject;-><init>(Ljava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 871
    :cond_23
    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v17

    const/16 v18, 0x2f

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_24

    .line 872
    new-instance v17, Lorg/icepdf/index/core/pobjects/Name;

    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Lorg/icepdf/index/core/pobjects/Name;-><init>(Ljava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 876
    :cond_24
    const/4 v9, 0x0

    .line 877
    .local v9, "foundDigit":Z
    const/4 v8, 0x0

    .line 878
    .local v8, "foundDecimal":Z
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    add-int/lit8 v11, v17, -0x1

    .restart local v11    # "i":I
    :goto_6
    if-ltz v11, :cond_27

    .line 879
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    .line 880
    .local v4, "curr":C
    const/16 v17, 0x2e

    move/from16 v0, v17

    if-ne v4, v0, :cond_26

    .line 881
    const/4 v8, 0x1

    .line 878
    :cond_25
    :goto_7
    add-int/lit8 v11, v11, -0x1

    goto :goto_6

    .line 882
    :cond_26
    const/16 v17, 0x30

    move/from16 v0, v17

    if-lt v4, v0, :cond_25

    const/16 v17, 0x39

    move/from16 v0, v17

    if-gt v4, v0, :cond_25

    .line 883
    const/4 v9, 0x1

    goto :goto_7

    .line 887
    .end local v4    # "curr":C
    :cond_27
    if-eqz v9, :cond_29

    .line 889
    if-eqz v8, :cond_28

    .line 890
    :try_start_1
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v17

    goto/16 :goto_0

    .line 892
    :cond_28
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v17

    goto/16 :goto_0

    .line 895
    :catch_0
    move-exception v17

    .line 900
    :cond_29
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_0

    .line 777
    .end local v8    # "foundDecimal":Z
    .end local v9    # "foundDigit":Z
    .restart local v2    # "charNumber":I
    .restart local v7    # "digit":Ljava/lang/StringBuffer;
    :catch_1
    move-exception v17

    goto/16 :goto_3
.end method

.method hexToInt(Ljava/lang/String;)I
    .locals 2
    .param p1, "hex"    # Ljava/lang/String;

    .prologue
    .line 1045
    const/4 v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    .line 1046
    const/16 v0, 0x10

    invoke-static {p1, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method hexToString(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "hh"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x3

    const/16 v8, 0x46

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1053
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    .line 1054
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 1055
    .local v2, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v8, :cond_0

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x45

    if-ne v3, v4, :cond_0

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v8, :cond_0

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v8, :cond_0

    .line 1059
    const/4 v3, 0x4

    new-array v0, v3, [B

    .line 1060
    .local v0, "b":[B
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    div-int/lit8 v3, v3, 0x4

    if-ge v1, v3, :cond_1

    .line 1061
    mul-int/lit8 v3, v1, 0x4

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v6

    .line 1062
    mul-int/lit8 v3, v1, 0x4

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v5

    .line 1063
    mul-int/lit8 v3, v1, 0x4

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v7

    .line 1064
    mul-int/lit8 v3, v1, 0x4

    add-int/lit8 v3, v3, 0x3

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v9

    .line 1065
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    int-to-char v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1060
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1068
    .end local v0    # "b":[B
    .end local v1    # "i":I
    :cond_0
    new-array v0, v7, [B

    .line 1069
    .restart local v0    # "b":[B
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    if-ge v1, v3, :cond_1

    .line 1071
    const/4 v3, 0x0

    mul-int/lit8 v4, v1, 0x2

    :try_start_0
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 1072
    const/4 v3, 0x1

    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 1073
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Short;->parseShort(Ljava/lang/String;I)S

    move-result v3

    int-to-char v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1069
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1080
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 1075
    :catch_0
    move-exception v3

    goto :goto_2
.end method

.method peek2()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 1229
    iget-object v2, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v2, v3}, Ljava/io/InputStream;->mark(I)V

    .line 1230
    new-array v0, v3, [C

    .line 1231
    .local v0, "c":[C
    const/4 v2, 0x0

    iget-object v3, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v3

    int-to-char v3, v3

    aput-char v3, v0, v2

    .line 1232
    const/4 v2, 0x1

    iget-object v3, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v3

    int-to-char v3, v3

    aput-char v3, v0, v2

    .line 1233
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    .line 1234
    .local v1, "s":Ljava/lang/String;
    iget-object v2, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->reset()V

    .line 1235
    return-object v1
.end method

.method readByte()B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1147
    iget-object v0, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method readLineForInlineImage()Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v11, 0xd

    const/16 v10, 0xa

    const/4 v9, 0x2

    const/4 v6, 0x1

    .line 1092
    const/4 v0, 0x0

    .line 1093
    .local v0, "STATE_PRE_E":I
    const/4 v1, 0x1

    .line 1094
    .local v1, "STATE_PRE_I":I
    const/4 v2, 0x2

    .line 1095
    .local v2, "STATE_PRE_WHITESPACE":I
    const/4 v5, 0x0

    .line 1098
    .local v5, "state":I
    :cond_0
    :goto_0
    iget-object v7, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 1099
    .local v3, "c":I
    if-gez v3, :cond_3

    .line 1136
    :cond_1
    :goto_1
    if-ne v5, v9, :cond_7

    .line 1138
    :cond_2
    :goto_2
    return v6

    .line 1101
    :cond_3
    if-nez v5, :cond_4

    const/16 v7, 0x45

    if-ne v3, v7, :cond_4

    .line 1102
    add-int/lit8 v5, v5, 0x1

    .line 1103
    goto :goto_0

    .line 1104
    :cond_4
    if-ne v5, v6, :cond_5

    const/16 v7, 0x49

    if-ne v3, v7, :cond_5

    .line 1105
    add-int/lit8 v5, v5, 0x1

    .line 1106
    goto :goto_0

    .line 1107
    :cond_5
    if-ne v5, v9, :cond_6

    and-int/lit16 v7, v3, 0xff

    int-to-char v7, v7

    invoke-static {v7}, Lorg/icepdf/index/core/util/Parser;->isWhitespace(C)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1114
    iget-object v7, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    const/16 v8, 0x20

    invoke-static {v7, v8}, Lorg/icepdf/index/core/util/Parser;->isStillInlineImageData(Ljava/io/InputStream;I)Z

    move-result v4

    .line 1115
    .local v4, "imageDataFound":Z
    if-eqz v4, :cond_2

    .line 1116
    const/4 v5, 0x0

    .line 1118
    if-eq v3, v11, :cond_1

    if-ne v3, v10, :cond_0

    goto :goto_1

    .line 1128
    .end local v4    # "imageDataFound":Z
    :cond_6
    const/4 v5, 0x0

    .line 1129
    if-eq v3, v11, :cond_1

    if-ne v3, v10, :cond_0

    goto :goto_1

    .line 1138
    :cond_7
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public ungetNumberOrStringWithReset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 961
    iget-object v0, p0, Lorg/icepdf/index/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 962
    return-void
.end method
