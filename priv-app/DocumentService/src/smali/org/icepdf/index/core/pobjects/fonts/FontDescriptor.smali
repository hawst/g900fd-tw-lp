.class public Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;
.super Lorg/icepdf/index/core/pobjects/Dictionary;
.source "FontDescriptor.java"


# static fields
.field public static final ASCENT:Ljava/lang/String; = "Ascent"

.field public static final AVG_WIDTH:Ljava/lang/String; = "AvgWidth"

.field public static final CAP_HEIGHT:Ljava/lang/String; = "CapHeight"

.field public static final DESCENT:Ljava/lang/String; = "Descent"

.field public static final FLAGS:Ljava/lang/String; = "Flags"

.field public static final FONT_BBOX:Ljava/lang/String; = "FontBBox"

.field public static final FONT_FAMILY:Ljava/lang/String; = "FontFamily"

.field private static final FONT_FILE:Ljava/lang/String; = "FontFile"

.field private static final FONT_FILE_2:Ljava/lang/String; = "FontFile2"

.field private static final FONT_FILE_3:Ljava/lang/String; = "FontFile3"

.field private static final FONT_FILE_3_CID_FONT_TYPE_0:Ljava/lang/String; = "CIDFontType0"

.field private static final FONT_FILE_3_CID_FONT_TYPE_0C:Ljava/lang/String; = "CIDFontType0C"

.field private static final FONT_FILE_3_OPEN_TYPE:Ljava/lang/String; = "OpenType"

.field private static final FONT_FILE_3_TYPE_1C:Ljava/lang/String; = "Type1C"

.field public static final FONT_NAME:Ljava/lang/String; = "FontName"

.field public static final FONT_WEIGHT:Ljava/lang/String; = "FontWeight"

.field public static final ITALIC_ANGLE:Ljava/lang/String; = "ItalicAngle"

.field public static final LEADING:Ljava/lang/String; = "Leading"

.field public static final MAX_WIDTH:Ljava/lang/String; = "MaxWidth"

.field public static final MISSING_Stretch:Ljava/lang/String; = "FontStretch"

.field public static final MISSING_WIDTH:Ljava/lang/String; = "MissingWidth"

.field public static final STEM_H:Ljava/lang/String; = "StemH"

.field public static final STEM_V:Ljava/lang/String; = "StemV"

.field public static final X_HEIGHT:Ljava/lang/String; = "XHeight"


# instance fields
.field private font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "h"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 88
    return-void
.end method

.method public static createDescriptor(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/pobjects/fonts/AFM;)Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;
    .locals 3
    .param p0, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p1, "afm"    # Lorg/icepdf/index/core/pobjects/fonts/AFM;

    .prologue
    .line 99
    new-instance v0, Lorg/icepdf/index/core/util/Hashtable;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/util/Hashtable;-><init>(I)V

    .line 100
    .local v0, "properties":Lorg/icepdf/index/core/util/Hashtable;, "Lorg/icepdf/index/core/util/Hashtable<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v1, "FontName"

    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/fonts/AFM;->getFontName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    const-string/jumbo v1, "FontFamily"

    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/fonts/AFM;->getFamilyName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    const-string/jumbo v1, "FontBBox"

    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/fonts/AFM;->getFontBBox()[I

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    const-string/jumbo v1, "ItalicAngle"

    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/fonts/AFM;->getItalicAngle()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    const-string/jumbo v1, "MaxWidth"

    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/fonts/AFM;->getMaxWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    const-string/jumbo v1, "AvgWidth"

    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/fonts/AFM;->getAvgWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const-string/jumbo v1, "Flags"

    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/fonts/AFM;->getFlags()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    new-instance v1, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    invoke-direct {v1, p0, v0}, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    return-object v1
.end method


# virtual methods
.method public getAscent()F
    .locals 4

    .prologue
    .line 198
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "Ascent"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 199
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 200
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 202
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAverageWidth()F
    .locals 4

    .prologue
    .line 172
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "AvgWidth"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 173
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 174
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 176
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDescent()F
    .locals 4

    .prologue
    .line 211
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "Descent"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 212
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 213
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 215
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getEmbeddedFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    return-object v0
.end method

.method public getFlags()I
    .locals 4

    .prologue
    .line 249
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "Flags"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 250
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 251
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v1

    .line 253
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFontBBox()Lorg/icepdf/index/core/pobjects/PRectangle;
    .locals 5

    .prologue
    .line 233
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v4, "FontBBox"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 234
    .local v1, "value":Ljava/lang/Object;
    instance-of v2, v1, Ljava/util/Vector;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 235
    check-cast v0, Ljava/util/Vector;

    .line 236
    .local v0, "rectangle":Ljava/util/Vector;
    new-instance v2, Lorg/icepdf/index/core/pobjects/PRectangle;

    invoke-direct {v2, v0}, Lorg/icepdf/index/core/pobjects/PRectangle;-><init>(Ljava/util/Vector;)V

    .line 238
    .end local v0    # "rectangle":Ljava/util/Vector;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getFontFamily()Ljava/lang/String;
    .locals 5

    .prologue
    .line 130
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v4, "FontFamily"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 131
    .local v1, "value":Ljava/lang/Object;
    instance-of v2, v1, Lorg/icepdf/index/core/pobjects/StringObject;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 132
    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    .line 133
    .local v0, "familyName":Lorg/icepdf/index/core/pobjects/StringObject;
    invoke-interface {v0}, Lorg/icepdf/index/core/pobjects/StringObject;->toString()Ljava/lang/String;

    move-result-object v2

    .line 135
    .end local v0    # "familyName":Lorg/icepdf/index/core/pobjects/StringObject;
    :goto_0
    return-object v2

    :cond_0
    const-string/jumbo v2, "FontName"

    goto :goto_0
.end method

.method public getFontName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 116
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "FontName"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 117
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/index/core/pobjects/Name;

    if-eqz v1, :cond_0

    .line 118
    check-cast v0, Lorg/icepdf/index/core/pobjects/Name;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v1

    .line 120
    :goto_0
    return-object v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const-string/jumbo v1, "FontName"

    goto :goto_0
.end method

.method public getFontWeight()F
    .locals 4

    .prologue
    .line 145
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "FontWeight"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 146
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 147
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 149
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMaxWidth()F
    .locals 4

    .prologue
    .line 185
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "MaxWidth"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 186
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 187
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 189
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMissingWidth()F
    .locals 4

    .prologue
    .line 159
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "MissingWidth"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 160
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 161
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 163
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public init()V
    .locals 6

    .prologue
    .line 272
    :try_start_0
    invoke-static {}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->getInstance()Lorg/icepdf/index/core/pobjects/fonts/FontFactory;

    move-result-object v0

    .line 274
    .local v0, "fontFactory":Lorg/icepdf/index/core/pobjects/fonts/FontFactory;
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v4, "FontFile"

    invoke-virtual {v3, v4}, Lorg/icepdf/index/core/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 275
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v5, "FontFile"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/index/core/pobjects/Stream;

    .line 276
    .local v1, "fontStream":Lorg/icepdf/index/core/pobjects/Stream;
    if-eqz v1, :cond_0

    .line 277
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->createFontFile(Lorg/icepdf/index/core/pobjects/Stream;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 282
    .end local v1    # "fontStream":Lorg/icepdf/index/core/pobjects/Stream;
    :cond_0
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v4, "FontFile2"

    invoke-virtual {v3, v4}, Lorg/icepdf/index/core/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 283
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v5, "FontFile2"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/index/core/pobjects/Stream;

    .line 284
    .restart local v1    # "fontStream":Lorg/icepdf/index/core/pobjects/Stream;
    if-eqz v1, :cond_1

    .line 285
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->createFontFile(Lorg/icepdf/index/core/pobjects/Stream;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 290
    .end local v1    # "fontStream":Lorg/icepdf/index/core/pobjects/Stream;
    :cond_1
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v4, "FontFile3"

    invoke-virtual {v3, v4}, Lorg/icepdf/index/core/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 292
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v5, "FontFile3"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/index/core/pobjects/Stream;

    .line 293
    .restart local v1    # "fontStream":Lorg/icepdf/index/core/pobjects/Stream;
    const/4 v2, 0x0

    .line 294
    .local v2, "subType":Ljava/lang/String;
    if-eqz v1, :cond_2

    const-string/jumbo v3, "Subtype"

    invoke-virtual {v1, v3}, Lorg/icepdf/index/core/pobjects/Stream;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 295
    const-string/jumbo v3, "Subtype"

    invoke-virtual {v1, v3}, Lorg/icepdf/index/core/pobjects/Stream;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 296
    :cond_2
    if-eqz v2, :cond_4

    const-string/jumbo v3, "Type1C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string/jumbo v3, "CIDFontType0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string/jumbo v3, "CIDFontType0C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 301
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->createFontFile(Lorg/icepdf/index/core/pobjects/Stream;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 304
    :cond_4
    if-eqz v2, :cond_5

    const-string/jumbo v3, "OpenType"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 306
    const/4 v3, 0x5

    invoke-virtual {v0, v1, v3}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->createFontFile(Lorg/icepdf/index/core/pobjects/Stream;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    .end local v0    # "fontFactory":Lorg/icepdf/index/core/pobjects/fonts/FontFactory;
    .end local v1    # "fontStream":Lorg/icepdf/index/core/pobjects/Stream;
    .end local v2    # "subType":Ljava/lang/String;
    :cond_5
    :goto_0
    return-void

    .line 313
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 326
    const/4 v0, 0x0

    .line 327
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    if-eqz v1, :cond_0

    .line 328
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    invoke-interface {v1}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v0

    .line 329
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lorg/icepdf/index/core/pobjects/Dictionary;->getPObjectReference()Lorg/icepdf/index/core/pobjects/Reference;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " FONTDESCRIPTOR= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v2}, Lorg/icepdf/index/core/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
