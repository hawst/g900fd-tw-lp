.class public Lorg/icepdf/index/core/pobjects/HexStringObject;
.super Ljava/lang/Object;
.source "HexStringObject.java"

# interfaces
.implements Lorg/icepdf/index/core/pobjects/StringObject;


# instance fields
.field reference:Lorg/icepdf/index/core/pobjects/Reference;

.field private stringData:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>(Ljava/lang/StringBuffer;)V
    .locals 2
    .param p1, "stringBuffer"    # Ljava/lang/StringBuffer;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 64
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 66
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuffer;

    .line 67
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-static {p1}, Lorg/icepdf/index/core/pobjects/HexStringObject;->normalizeHex(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 68
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "bytes"    # [B

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuffer;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/HexStringObject;-><init>(Ljava/lang/StringBuffer;)V

    .line 51
    return-void
.end method

.method private hexToString(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 12
    .param p1, "hh"    # Ljava/lang/StringBuffer;

    .prologue
    const/4 v11, 0x2

    const/16 v10, 0x66

    const/16 v9, 0x46

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 247
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-nez v4, :cond_2

    .line 248
    :cond_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 275
    :cond_1
    return-object v2

    .line 252
    :cond_2
    invoke-virtual {p1, v6}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    if-ne v4, v9, :cond_4

    move v4, v5

    :goto_0
    invoke-virtual {p1, v6}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v7

    if-ne v7, v10, :cond_5

    move v7, v5

    :goto_1
    or-int/2addr v4, v7

    if-eqz v4, :cond_3

    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    const/16 v7, 0x45

    if-ne v4, v7, :cond_6

    move v4, v5

    :goto_2
    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v7

    const/16 v8, 0x65

    if-ne v7, v8, :cond_7

    move v7, v5

    :goto_3
    or-int/2addr v4, v7

    if-eqz v4, :cond_3

    invoke-virtual {p1, v11}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    if-ne v4, v9, :cond_8

    move v4, v5

    :goto_4
    invoke-virtual {p1, v11}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v7

    if-ne v7, v10, :cond_9

    move v7, v5

    :goto_5
    or-int/2addr v4, v7

    if-eqz v4, :cond_3

    const/4 v4, 0x3

    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    if-ne v4, v9, :cond_a

    move v4, v5

    :goto_6
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v7

    if-ne v7, v10, :cond_b

    :goto_7
    or-int/2addr v4, v5

    if-nez v4, :cond_c

    .line 256
    :cond_3
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    .line 257
    .local v1, "length":I
    new-instance v2, Ljava/lang/StringBuffer;

    div-int/lit8 v4, v1, 0x2

    invoke-direct {v2, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 260
    .local v2, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_8
    if-ge v0, v1, :cond_1

    .line 261
    add-int/lit8 v4, v0, 0x2

    invoke-virtual {p1, v0, v4}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 262
    .local v3, "subStr":Ljava/lang/String;
    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-char v4, v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 260
    add-int/lit8 v0, v0, 0x2

    goto :goto_8

    .end local v0    # "i":I
    .end local v1    # "length":I
    .end local v2    # "sb":Ljava/lang/StringBuffer;
    .end local v3    # "subStr":Ljava/lang/String;
    :cond_4
    move v4, v6

    .line 252
    goto :goto_0

    :cond_5
    move v7, v6

    goto :goto_1

    :cond_6
    move v4, v6

    goto :goto_2

    :cond_7
    move v7, v6

    goto :goto_3

    :cond_8
    move v4, v6

    goto :goto_4

    :cond_9
    move v7, v6

    goto :goto_5

    :cond_a
    move v4, v6

    goto :goto_6

    :cond_b
    move v5, v6

    goto :goto_7

    .line 268
    :cond_c
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    .line 269
    .restart local v1    # "length":I
    new-instance v2, Ljava/lang/StringBuffer;

    div-int/lit8 v4, v1, 0x4

    invoke-direct {v2, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 271
    .restart local v2    # "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_9
    if-ge v0, v1, :cond_1

    .line 272
    add-int/lit8 v4, v0, 0x4

    invoke-virtual {p1, v0, v4}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 273
    .restart local v3    # "subStr":Ljava/lang/String;
    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-char v4, v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 271
    add-int/lit8 v0, v0, 0x4

    goto :goto_9
.end method

.method private static isNoneHexChar(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 235
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-le p0, v0, :cond_3

    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-le p0, v0, :cond_3

    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-le p0, v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static normalizeHex(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 3
    .param p0, "hex"    # Ljava/lang/StringBuffer;

    .prologue
    .line 208
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    .line 209
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 210
    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lorg/icepdf/index/core/pobjects/HexStringObject;->isNoneHexChar(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 211
    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 212
    add-int/lit8 v1, v1, -0x1

    .line 213
    add-int/lit8 v0, v0, -0x1

    .line 209
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 216
    :cond_1
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    .line 218
    rem-int/lit8 v2, v1, 0x2

    if-eqz v2, :cond_2

    .line 219
    const/16 v2, 0x30

    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 221
    :cond_2
    const/4 v2, 0x2

    if-le v1, v2, :cond_3

    rem-int/lit8 v2, v1, 0x4

    if-eqz v2, :cond_3

    .line 222
    const-string/jumbo v2, "00"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 224
    :cond_3
    return-object p0
.end method


# virtual methods
.method public getDecryptedLiteralString(Lorg/icepdf/index/core/pobjects/security/SecurityManager;)Ljava/lang/String;
    .locals 5
    .param p1, "securityManager"    # Lorg/icepdf/index/core/pobjects/security/SecurityManager;

    .prologue
    .line 302
    if-eqz p1, :cond_2

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->reference:Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v4, :cond_2

    .line 304
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/security/SecurityManager;->getDecryptionKey()[B

    move-result-object v1

    .line 307
    .local v1, "key":[B
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    new-array v3, v4, [B

    .line 308
    .local v3, "textBytes":[B
    const/4 v0, 0x0

    .local v0, "i":I
    array-length v2, v3

    .local v2, "max":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 309
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v3, v0

    .line 308
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 313
    :cond_0
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->reference:Lorg/icepdf/index/core/pobjects/Reference;

    invoke-virtual {p1, v4, v1, v3}, Lorg/icepdf/index/core/pobjects/security/SecurityManager;->decrypt(Lorg/icepdf/index/core/pobjects/Reference;[B[B)[B

    move-result-object v3

    .line 319
    if-nez v3, :cond_1

    .line 320
    const-string/jumbo v4, ""

    .line 324
    .end local v0    # "i":I
    .end local v1    # "key":[B
    .end local v2    # "max":I
    .end local v3    # "textBytes":[B
    :goto_1
    return-object v4

    .line 322
    .restart local v0    # "i":I
    .restart local v1    # "key":[B
    .restart local v2    # "max":I
    .restart local v3    # "textBytes":[B
    :cond_1
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>([B)V

    goto :goto_1

    .line 324
    .end local v0    # "i":I
    .end local v1    # "key":[B
    .end local v2    # "max":I
    .end local v3    # "textBytes":[B
    :cond_2
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/HexStringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHexStringBuffer()Ljava/lang/StringBuffer;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuffer;

    return-object v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    return v0
.end method

.method public getLiteralString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/HexStringObject;->hexToString(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLiteralStringBuffer()Ljava/lang/StringBuffer;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/HexStringObject;->hexToString(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;
    .locals 8
    .param p1, "fontFormat"    # I
    .param p2, "font"    # Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .prologue
    .line 157
    const/4 v6, 0x1

    if-ne p1, v6, :cond_1

    .line 158
    const/4 v0, 0x2

    .line 159
    .local v0, "charOffset":I
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/HexStringObject;->getLength()I

    move-result v4

    .line 160
    .local v4, "length":I
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 161
    .local v5, "tmp":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .line 163
    .local v3, "lastIndex":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_4

    .line 164
    sub-int v6, v2, v3

    add-int v7, v3, v0

    invoke-virtual {p0, v6, v7}, Lorg/icepdf/index/core/pobjects/HexStringObject;->getUnsignedInt(II)I

    move-result v1

    .line 166
    .local v1, "charValue":I
    if-lez v1, :cond_0

    int-to-char v6, v1

    invoke-interface {p2, v6}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->canDisplayEchar(C)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 167
    int-to-char v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 168
    const/4 v3, 0x0

    .line 163
    :goto_1
    add-int/2addr v2, v0

    goto :goto_0

    .line 170
    :cond_0
    add-int/2addr v3, v0

    goto :goto_1

    .line 174
    .end local v0    # "charOffset":I
    .end local v1    # "charValue":I
    .end local v2    # "i":I
    .end local v3    # "lastIndex":I
    .end local v4    # "length":I
    .end local v5    # "tmp":Ljava/lang/StringBuffer;
    :cond_1
    const/4 v6, 0x2

    if-ne p1, v6, :cond_3

    .line 175
    const/4 v0, 0x4

    .line 176
    .restart local v0    # "charOffset":I
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/HexStringObject;->getLength()I

    move-result v4

    .line 178
    .restart local v4    # "length":I
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 179
    .restart local v5    # "tmp":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    if-ge v2, v4, :cond_4

    .line 180
    invoke-virtual {p0, v2, v0}, Lorg/icepdf/index/core/pobjects/HexStringObject;->getUnsignedInt(II)I

    move-result v1

    .line 181
    .restart local v1    # "charValue":I
    int-to-char v6, v1

    invoke-interface {p2, v6}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->canDisplayEchar(C)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 182
    int-to-char v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 179
    :cond_2
    add-int/2addr v2, v0

    goto :goto_2

    .line 187
    .end local v0    # "charOffset":I
    .end local v1    # "charValue":I
    .end local v2    # "i":I
    .end local v4    # "length":I
    .end local v5    # "tmp":Ljava/lang/StringBuffer;
    :cond_3
    const/4 v5, 0x0

    :cond_4
    return-object v5
.end method

.method public getUnsignedInt(II)I
    .locals 3
    .param p1, "start"    # I
    .param p2, "offset"    # I

    .prologue
    .line 79
    if-ltz p1, :cond_0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int v2, p1, p2

    if-ge v1, v2, :cond_1

    .line 80
    :cond_0
    const/4 v0, 0x0

    .line 89
    :goto_0
    return v0

    .line 81
    :cond_1
    const/4 v0, 0x0

    .line 83
    .local v0, "unsignedInt":I
    :try_start_0
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuffer;

    add-int v2, p1, p2

    invoke-virtual {v1, p1, v2}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 86
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public setReference(Lorg/icepdf/index/core/pobjects/Reference;)V
    .locals 0
    .param p1, "reference"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    .line 285
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/HexStringObject;->reference:Lorg/icepdf/index/core/pobjects/Reference;

    .line 286
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/HexStringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
