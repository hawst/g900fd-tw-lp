.class public Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;
.super Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
.source "CrossReference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/core/pobjects/CrossReference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UsedEntry"
.end annotation


# instance fields
.field private m_iGenerationNumber:I

.field private m_lFilePositionOfObject:J


# direct methods
.method constructor <init>(IJI)V
    .locals 2
    .param p1, "objectNumber"    # I
    .param p2, "filePositionOfObject"    # J
    .param p4, "generationNumber"    # I

    .prologue
    .line 306
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lorg/icepdf/index/core/pobjects/CrossReference$Entry;-><init>(II)V

    .line 307
    iput-wide p2, p0, Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;->m_lFilePositionOfObject:J

    .line 308
    iput p4, p0, Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;->m_iGenerationNumber:I

    .line 309
    return-void
.end method


# virtual methods
.method public getFilePositionOfObject()J
    .locals 2

    .prologue
    .line 312
    iget-wide v0, p0, Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;->m_lFilePositionOfObject:J

    return-wide v0
.end method

.method public getGenerationNumber()I
    .locals 1

    .prologue
    .line 316
    iget v0, p0, Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;->m_iGenerationNumber:I

    return v0
.end method
