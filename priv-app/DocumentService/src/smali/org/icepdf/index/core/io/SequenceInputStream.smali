.class public Lorg/icepdf/index/core/io/SequenceInputStream;
.super Ljava/io/InputStream;
.source "SequenceInputStream.java"


# instance fields
.field private m_isCurrent:Ljava/io/InputStream;

.field private m_itInputStreams:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/io/InputStream;)V
    .locals 5
    .param p1, "in1"    # Ljava/io/InputStream;
    .param p2, "in2"    # Ljava/io/InputStream;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 32
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 33
    .local v1, "lst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/InputStream;>;"
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/index/core/io/SequenceInputStream;->m_itInputStreams:Ljava/util/Iterator;

    .line 38
    :try_start_0
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->useNextInputStream()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Could not use first InputStream in SequenceInputStream(List) : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public constructor <init>(Ljava/util/Iterator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Ljava/io/InputStream;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "inputStreams":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/io/InputStream;>;"
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 46
    iput-object p1, p0, Lorg/icepdf/index/core/io/SequenceInputStream;->m_itInputStreams:Ljava/util/Iterator;

    .line 49
    :try_start_0
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->useNextInputStream()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Could not use first InputStream in SequenceInputStream(List) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private closeCurrentInputStream()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->getCurrentInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 75
    .local v0, "in":Ljava/io/InputStream;
    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 77
    :cond_0
    return-void
.end method

.method private getCurrentInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/icepdf/index/core/io/SequenceInputStream;->m_isCurrent:Ljava/io/InputStream;

    return-object v0
.end method

.method private useNextInputStream()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->closeCurrentInputStream()V

    .line 63
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/index/core/io/SequenceInputStream;->m_isCurrent:Ljava/io/InputStream;

    .line 64
    :cond_0
    iget-object v1, p0, Lorg/icepdf/index/core/io/SequenceInputStream;->m_itInputStreams:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65
    iget-object v1, p0, Lorg/icepdf/index/core/io/SequenceInputStream;->m_itInputStreams:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 66
    .local v0, "in":Ljava/io/InputStream;
    if-eqz v0, :cond_0

    .line 67
    iput-object v0, p0, Lorg/icepdf/index/core/io/SequenceInputStream;->m_isCurrent:Ljava/io/InputStream;

    .line 71
    .end local v0    # "in":Ljava/io/InputStream;
    :cond_1
    return-void
.end method


# virtual methods
.method public available()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->getCurrentInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 82
    .local v0, "in":Ljava/io/InputStream;
    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v1

    .line 84
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    :cond_0
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->useNextInputStream()V

    .line 142
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->getCurrentInputStream()Ljava/io/InputStream;

    move-result-object v0

    if-nez v0, :cond_0

    .line 143
    return-void
.end method

.method public mark(I)V
    .locals 0
    .param p1, "readlimit"    # I

    .prologue
    .line 150
    invoke-super {p0, p1}, Ljava/io/InputStream;->mark(I)V

    .line 151
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 146
    invoke-super {p0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    :goto_0
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->getCurrentInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 90
    .local v0, "in":Ljava/io/InputStream;
    if-nez v0, :cond_1

    .line 91
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->useNextInputStream()V

    .line 92
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->getCurrentInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 93
    if-nez v0, :cond_1

    .line 94
    const/4 v1, -0x1

    .line 99
    :cond_0
    return v1

    .line 97
    :cond_1
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 98
    .local v1, "readByte":I
    if-gez v1, :cond_0

    .line 100
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->useNextInputStream()V

    goto :goto_0
.end method

.method public read([BII)I
    .locals 6
    .param p1, "buffer"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    if-nez p1, :cond_0

    .line 106
    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    .line 107
    :cond_0
    if-ltz p2, :cond_1

    array-length v3, p1

    if-ge p2, v3, :cond_1

    if-ltz p3, :cond_1

    add-int v3, p2, p3

    array-length v4, p1

    if-gt v3, v4, :cond_1

    add-int v3, p2, p3

    if-gez v3, :cond_2

    .line 110
    :cond_1
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Offset: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", Length: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", Buffer length: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, p1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 111
    :cond_2
    if-nez p3, :cond_4

    .line 112
    const/4 v2, 0x0

    .line 136
    :cond_3
    :goto_0
    return v2

    .line 114
    :cond_4
    const/4 v2, 0x0

    .line 115
    .local v2, "totalRead":I
    :goto_1
    if-ge v2, p3, :cond_3

    .line 116
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->getCurrentInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 117
    .local v1, "in":Ljava/io/InputStream;
    if-nez v1, :cond_5

    .line 118
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->useNextInputStream()V

    .line 119
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->getCurrentInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 120
    if-nez v1, :cond_5

    .line 121
    if-gtz v2, :cond_3

    .line 124
    const/4 v2, -0x1

    goto :goto_0

    .line 128
    :cond_5
    add-int v3, p2, v2

    sub-int v4, p3, v2

    invoke-virtual {v1, p1, v3, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 129
    .local v0, "currRead":I
    if-lez v0, :cond_6

    .line 130
    add-int/2addr v2, v0

    goto :goto_1

    .line 132
    :cond_6
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SequenceInputStream;->useNextInputStream()V

    goto :goto_1
.end method

.method public reset()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 154
    invoke-super {p0}, Ljava/io/InputStream;->reset()V

    .line 155
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 159
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 160
    .local v2, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 161
    const-string/jumbo v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 163
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 164
    .local v1, "inputStreams":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    :goto_0
    iget-object v3, p0, Lorg/icepdf/index/core/io/SequenceInputStream;->m_itInputStreams:Ljava/util/Iterator;

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 165
    iget-object v3, p0, Lorg/icepdf/index/core/io/SequenceInputStream;->m_itInputStreams:Ljava/util/Iterator;

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 166
    .local v0, "in":Ljava/io/InputStream;
    const-string/jumbo v3, "\n  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 167
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 168
    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 169
    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 171
    .end local v0    # "in":Ljava/io/InputStream;
    :cond_0
    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/index/core/io/SequenceInputStream;->m_itInputStreams:Ljava/util/Iterator;

    .line 173
    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 174
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
