.class public Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;
.super Ljava/io/ByteArrayInputStream;
.source "SeekableByteArrayInputStream.java"

# interfaces
.implements Lorg/icepdf/index/core/io/SeekableInput;


# instance fields
.field private m_iBeginningOffset:I

.field private m_oCurrentUser:Ljava/lang/Object;

.field private final m_oLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>([B)V
    .locals 1
    .param p1, "buf"    # [B

    .prologue
    .line 34
    invoke-direct {p0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_oLock:Ljava/lang/Object;

    .line 37
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 41
    iput p2, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_oLock:Ljava/lang/Object;

    .line 43
    return-void
.end method


# virtual methods
.method public beginThreadAccess()V
    .locals 6

    .prologue
    .line 84
    iget-object v2, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_oLock:Ljava/lang/Object;

    monitor-enter v2

    .line 85
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 87
    .local v0, "requestingUser":Ljava/lang/Thread;
    :goto_0
    iget-object v1, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_oCurrentUser:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 88
    iput-object v0, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_oCurrentUser:Ljava/lang/Object;

    .line 100
    :cond_0
    monitor-exit v2

    .line 101
    return-void

    .line 90
    :cond_1
    iget-object v1, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_oCurrentUser:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v1, v0, :cond_0

    .line 94
    :try_start_1
    iget-object v1, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_oLock:Ljava/lang/Object;

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v1

    goto :goto_0

    .line 100
    .end local v0    # "requestingUser":Ljava/lang/Thread;
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public endThreadAccess()V
    .locals 3

    .prologue
    .line 104
    iget-object v2, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_oLock:Ljava/lang/Object;

    monitor-enter v2

    .line 105
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 106
    .local v0, "requestingUser":Ljava/lang/Thread;
    iget-object v1, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_oCurrentUser:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 107
    iget-object v1, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_oLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 114
    :cond_0
    :goto_0
    monitor-exit v2

    .line 115
    return-void

    .line 108
    :cond_1
    iget-object v1, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_oCurrentUser:Ljava/lang/Object;

    if-ne v1, v0, :cond_0

    .line 109
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_oCurrentUser:Ljava/lang/Object;

    .line 110
    iget-object v1, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_oLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    goto :goto_0

    .line 114
    .end local v0    # "requestingUser":Ljava/lang/Thread;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getAbsolutePosition()J
    .locals 6

    .prologue
    .line 69
    iget v1, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->pos:I

    iget v2, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    sub-int v0, v1, v2

    .line 70
    .local v0, "absPos":I
    int-to-long v2, v0

    const-wide/16 v4, -0x1

    and-long/2addr v2, v4

    return-wide v2
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 0

    .prologue
    .line 79
    return-object p0
.end method

.method public getLength()J
    .locals 6

    .prologue
    .line 74
    iget v1, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->count:I

    iget v2, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    sub-int v0, v1, v2

    .line 75
    .local v0, "len":I
    int-to-long v2, v0

    const-wide/16 v4, -0x1

    and-long/2addr v2, v4

    return-wide v2
.end method

.method public seekAbsolute(J)V
    .locals 5
    .param p1, "absolutePosition"    # J

    .prologue
    .line 52
    const-wide/16 v2, -0x1

    and-long/2addr v2, p1

    long-to-int v0, v2

    .line 53
    .local v0, "absPos":I
    iget v1, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->pos:I

    .line 54
    return-void
.end method

.method public seekEnd()V
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->getLength()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->seekAbsolute(J)V

    .line 66
    return-void
.end method

.method public seekRelative(J)V
    .locals 5
    .param p1, "relativeOffset"    # J

    .prologue
    .line 57
    const-wide/16 v2, -0x1

    and-long/2addr v2, p1

    long-to-int v1, v2

    .line 58
    .local v1, "relOff":I
    iget v2, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->pos:I

    add-int v0, v2, v1

    .line 59
    .local v0, "currPos":I
    iget v2, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    if-ge v0, v2, :cond_0

    .line 60
    iget v0, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    .line 61
    :cond_0
    iput v0, p0, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;->pos:I

    .line 62
    return-void
.end method
