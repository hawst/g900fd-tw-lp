.class public abstract Lorg/icepdf/index/java/awt/geom/Point2D;
.super Ljava/lang/Object;
.source "Point2D.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/index/java/awt/geom/Point2D$Double;,
        Lorg/icepdf/index/java/awt/geom/Point2D$Float;
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    return-void
.end method

.method public static distance(DDDD)D
    .locals 2
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "x2"    # D
    .param p6, "y2"    # D

    .prologue
    .line 129
    invoke-static/range {p0 .. p7}, Lorg/icepdf/index/java/awt/geom/Point2D;->distanceSq(DDDD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static distanceSq(DDDD)D
    .locals 4
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "x2"    # D
    .param p6, "y2"    # D

    .prologue
    .line 115
    sub-double/2addr p4, p0

    .line 116
    sub-double/2addr p6, p2

    .line 117
    mul-double v0, p4, p4

    mul-double v2, p6, p6

    add-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 143
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public distance(DD)D
    .locals 3
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    .line 133
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/icepdf/index/java/awt/geom/Point2D;->distanceSq(DD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public distance(Lorg/icepdf/index/java/awt/geom/Point2D;)D
    .locals 2
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 137
    invoke-virtual {p0, p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->distanceSq(Lorg/icepdf/index/java/awt/geom/Point2D;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public distanceSq(DD)D
    .locals 9
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    .line 121
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    move-wide v4, p1

    move-wide v6, p3

    invoke-static/range {v0 .. v7}, Lorg/icepdf/index/java/awt/geom/Point2D;->distanceSq(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public distanceSq(Lorg/icepdf/index/java/awt/geom/Point2D;)D
    .locals 8
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 125
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lorg/icepdf/index/java/awt/geom/Point2D;->distanceSq(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 159
    if-ne p1, p0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v1

    .line 162
    :cond_1
    instance-of v3, p1, Lorg/icepdf/index/java/awt/geom/Point2D;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 163
    check-cast v0, Lorg/icepdf/index/java/awt/geom/Point2D;

    .line 164
    .local v0, "p":Lorg/icepdf/index/java/awt/geom/Point2D;
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v4

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v4

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "p":Lorg/icepdf/index/java/awt/geom/Point2D;
    :cond_3
    move v1, v2

    .line 166
    goto :goto_0
.end method

.method public abstract getX()D
.end method

.method public abstract getY()D
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 151
    new-instance v0, Lorg/apache/harmony/misc/HashCode;

    invoke-direct {v0}, Lorg/apache/harmony/misc/HashCode;-><init>()V

    .line 152
    .local v0, "hash":Lorg/apache/harmony/misc/HashCode;
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/harmony/misc/HashCode;->append(D)Lorg/apache/harmony/misc/HashCode;

    .line 153
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/harmony/misc/HashCode;->append(D)Lorg/apache/harmony/misc/HashCode;

    .line 154
    invoke-virtual {v0}, Lorg/apache/harmony/misc/HashCode;->hashCode()I

    move-result v1

    return v1
.end method

.method public abstract setLocation(DD)V
.end method

.method public setLocation(Lorg/icepdf/index/java/awt/geom/Point2D;)V
    .locals 4
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 111
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/icepdf/index/java/awt/geom/Point2D;->setLocation(DD)V

    .line 112
    return-void
.end method
