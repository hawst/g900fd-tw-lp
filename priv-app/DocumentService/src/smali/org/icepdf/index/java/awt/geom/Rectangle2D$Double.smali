.class public Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;
.super Lorg/icepdf/index/java/awt/geom/Rectangle2D;
.source "Rectangle2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/Rectangle2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Double"
.end annotation


# instance fields
.field public height:D

.field public width:D

.field public x:D

.field public y:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;-><init>()V

    .line 168
    return-void
.end method

.method public constructor <init>(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 170
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;-><init>()V

    .line 171
    invoke-virtual/range {p0 .. p8}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->setRect(DDDD)V

    .line 172
    return-void
.end method


# virtual methods
.method public createIntersection(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 1
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 249
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 250
    .local v0, "dst":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    invoke-static {p0, p1, v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->intersect(Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    .line 251
    return-object v0
.end method

.method public createUnion(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 1
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 256
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 257
    .local v0, "dest":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    invoke-static {p0, p1, v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->union(Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    .line 258
    return-object v0
.end method

.method public getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 10

    .prologue
    .line 244
    new-instance v1, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->x:D

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->y:D

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->width:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->height:D

    invoke-direct/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    return-object v1
.end method

.method public getHeight()D
    .locals 2

    .prologue
    .line 191
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->height:D

    return-wide v0
.end method

.method public getWidth()D
    .locals 2

    .prologue
    .line 186
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->width:D

    return-wide v0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 176
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->x:D

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 181
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->y:D

    return-wide v0
.end method

.method public isEmpty()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 196
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->width:D

    cmpg-double v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->height:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public outcode(DD)I
    .locals 9
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    const-wide/16 v6, 0x0

    .line 217
    const/4 v0, 0x0

    .line 219
    .local v0, "code":I
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->width:D

    cmpg-double v1, v2, v6

    if-gtz v1, :cond_2

    .line 220
    or-int/lit8 v0, v0, 0x5

    .line 229
    :cond_0
    :goto_0
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->height:D

    cmpg-double v1, v2, v6

    if-gtz v1, :cond_4

    .line 230
    or-int/lit8 v0, v0, 0xa

    .line 239
    :cond_1
    :goto_1
    return v0

    .line 222
    :cond_2
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->x:D

    cmpg-double v1, p1, v2

    if-gez v1, :cond_3

    .line 223
    or-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 225
    :cond_3
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->x:D

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->width:D

    add-double/2addr v2, v4

    cmpl-double v1, p1, v2

    if-lez v1, :cond_0

    .line 226
    or-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 232
    :cond_4
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->y:D

    cmpg-double v1, p3, v2

    if-gez v1, :cond_5

    .line 233
    or-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 235
    :cond_5
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->y:D

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->height:D

    add-double/2addr v2, v4

    cmpl-double v1, p3, v2

    if-lez v1, :cond_1

    .line 236
    or-int/lit8 v0, v0, 0x8

    goto :goto_1
.end method

.method public setRect(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 201
    iput-wide p1, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->x:D

    .line 202
    iput-wide p3, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->y:D

    .line 203
    iput-wide p5, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->width:D

    .line 204
    iput-wide p7, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->height:D

    .line 205
    return-void
.end method

.method public setRect(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V
    .locals 2
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 209
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->x:D

    .line 210
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->y:D

    .line 211
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->width:D

    .line 212
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->height:D

    .line 213
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "[x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->x:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->y:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->width:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;->height:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
