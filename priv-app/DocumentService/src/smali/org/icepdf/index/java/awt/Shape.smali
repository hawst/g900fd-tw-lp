.class public interface abstract Lorg/icepdf/index/java/awt/Shape;
.super Ljava/lang/Object;
.source "Shape.java"


# virtual methods
.method public abstract contains(DD)Z
.end method

.method public abstract contains(DDDD)Z
.end method

.method public abstract contains(Lorg/icepdf/index/java/awt/geom/Point2D;)Z
.end method

.method public abstract contains(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
.end method

.method public abstract getBounds()Lorg/icepdf/index/java/awt/Rectangle;
.end method

.method public abstract getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
.end method

.method public abstract getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;
.end method

.method public abstract getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;D)Lorg/icepdf/index/java/awt/geom/PathIterator;
.end method

.method public abstract intersects(DDDD)Z
.end method

.method public abstract intersects(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
.end method
