.class public Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;
.super Lorg/icepdf/index/java/awt/geom/CubicCurve2D;
.source "CubicCurve2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/CubicCurve2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Float"
.end annotation


# instance fields
.field public ctrlx1:F

.field public ctrlx2:F

.field public ctrly1:F

.field public ctrly2:F

.field public x1:F

.field public x2:F

.field public y1:F

.field public y2:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;-><init>()V

    .line 46
    return-void
.end method

.method public constructor <init>(FFFFFFFF)V
    .locals 0
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "ctrlx1"    # F
    .param p4, "ctrly1"    # F
    .param p5, "ctrlx2"    # F
    .param p6, "ctrly2"    # F
    .param p7, "x2"    # F
    .param p8, "y2"    # F

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;-><init>()V

    .line 49
    invoke-virtual/range {p0 .. p8}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->setCurve(FFFFFFFF)V

    .line 50
    return-void
.end method


# virtual methods
.method public getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 7

    .prologue
    .line 140
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->x1:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->x2:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrlx1:F

    iget v6, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrlx2:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 141
    .local v0, "rx1":F
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->y1:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->y2:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrly1:F

    iget v6, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrly2:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 142
    .local v2, "ry1":F
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->x1:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->x2:F

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrlx1:F

    iget v6, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrlx2:F

    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 143
    .local v1, "rx2":F
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->y1:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->y2:F

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrly1:F

    iget v6, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrly2:F

    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 144
    .local v3, "ry2":F
    new-instance v4, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    sub-float v5, v1, v0

    sub-float v6, v3, v2

    invoke-direct {v4, v0, v2, v5, v6}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    return-object v4
.end method

.method public getCtrlP1()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 3

    .prologue
    .line 99
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrlx1:F

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrly1:F

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>(FF)V

    return-object v0
.end method

.method public getCtrlP2()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 3

    .prologue
    .line 104
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrlx2:F

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrly2:F

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>(FF)V

    return-object v0
.end method

.method public getCtrlX1()D
    .locals 2

    .prologue
    .line 64
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrlx1:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getCtrlX2()D
    .locals 2

    .prologue
    .line 74
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrlx2:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getCtrlY1()D
    .locals 2

    .prologue
    .line 69
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrly1:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getCtrlY2()D
    .locals 2

    .prologue
    .line 79
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrly2:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getP1()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 3

    .prologue
    .line 94
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->x1:F

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->y1:F

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>(FF)V

    return-object v0
.end method

.method public getP2()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 3

    .prologue
    .line 109
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->x2:F

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->y2:F

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>(FF)V

    return-object v0
.end method

.method public getX1()D
    .locals 2

    .prologue
    .line 54
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->x1:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getX2()D
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->x2:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getY1()D
    .locals 2

    .prologue
    .line 59
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->y1:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getY2()D
    .locals 2

    .prologue
    .line 89
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->y2:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public setCurve(DDDDDDDD)V
    .locals 3
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "ctrlx1"    # D
    .param p7, "ctrly1"    # D
    .param p9, "ctrlx2"    # D
    .param p11, "ctrly2"    # D
    .param p13, "x2"    # D
    .param p15, "y2"    # D

    .prologue
    .line 116
    double-to-float v2, p1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->x1:F

    .line 117
    double-to-float v2, p3

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->y1:F

    .line 118
    double-to-float v2, p5

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrlx1:F

    .line 119
    double-to-float v2, p7

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrly1:F

    .line 120
    double-to-float v2, p9

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrlx2:F

    .line 121
    double-to-float v2, p11

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrly2:F

    .line 122
    move-wide/from16 v0, p13

    double-to-float v2, v0

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->x2:F

    .line 123
    move-wide/from16 v0, p15

    double-to-float v2, v0

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->y2:F

    .line 124
    return-void
.end method

.method public setCurve(FFFFFFFF)V
    .locals 0
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "ctrlx1"    # F
    .param p4, "ctrly1"    # F
    .param p5, "ctrlx2"    # F
    .param p6, "ctrly2"    # F
    .param p7, "x2"    # F
    .param p8, "y2"    # F

    .prologue
    .line 129
    iput p1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->x1:F

    .line 130
    iput p2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->y1:F

    .line 131
    iput p3, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrlx1:F

    .line 132
    iput p4, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrly1:F

    .line 133
    iput p5, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrlx2:F

    .line 134
    iput p6, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->ctrly2:F

    .line 135
    iput p7, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->x2:F

    .line 136
    iput p8, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;->y2:F

    .line 137
    return-void
.end method
