.class Lorg/icepdf/index/java/awt/geom/Area$NullIterator;
.super Ljava/lang/Object;
.source "Area.java"

# interfaces
.implements Lorg/icepdf/index/java/awt/geom/PathIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/Area;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NullIterator"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    return-void
.end method


# virtual methods
.method public currentSegment([D)I
    .locals 2
    .param p1, "coords"    # [D

    .prologue
    .line 71
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string/jumbo v1, "awt.4B"

    invoke-static {v1}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public currentSegment([F)I
    .locals 2
    .param p1, "coords"    # [F

    .prologue
    .line 76
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string/jumbo v1, "awt.4B"

    invoke-static {v1}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getWindingRule()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    return v0
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method public next()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method
