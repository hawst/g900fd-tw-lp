.class public Lorg/icepdf/index/java/awt/geom/Area;
.super Ljava/lang/Object;
.source "Area.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/icepdf/index/java/awt/Shape;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/index/java/awt/geom/Area$NullIterator;
    }
.end annotation


# instance fields
.field s:Lorg/icepdf/index/java/awt/Shape;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/java/awt/Shape;)V
    .locals 1
    .param p1, "s"    # Lorg/icepdf/index/java/awt/Shape;

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    if-nez p1, :cond_0

    .line 95
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 97
    :cond_0
    iput-object p1, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    .line 98
    return-void
.end method


# virtual methods
.method public add(Lorg/icepdf/index/java/awt/geom/Area;)V
    .locals 2
    .param p1, "area"    # Lorg/icepdf/index/java/awt/geom/Area;

    .prologue
    .line 171
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 331
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Area;

    invoke-direct {v0, p0}, Lorg/icepdf/index/java/awt/geom/Area;-><init>(Lorg/icepdf/index/java/awt/Shape;)V

    return-object v0
.end method

.method public contains(DD)Z
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 101
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/icepdf/index/java/awt/Shape;->contains(DD)Z

    move-result v0

    goto :goto_0
.end method

.method public contains(DDDD)Z
    .locals 11
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 105
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    move-wide v2, p1

    move-wide v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    invoke-interface/range {v1 .. v9}, Lorg/icepdf/index/java/awt/Shape;->contains(DDDD)Z

    move-result v0

    goto :goto_0
.end method

.method public contains(Lorg/icepdf/index/java/awt/geom/Point2D;)Z
    .locals 1
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 109
    if-nez p1, :cond_0

    .line 110
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 112
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    invoke-interface {v0, p1}, Lorg/icepdf/index/java/awt/Shape;->contains(Lorg/icepdf/index/java/awt/geom/Point2D;)Z

    move-result v0

    goto :goto_0
.end method

.method public contains(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
    .locals 1
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 116
    if-nez p1, :cond_0

    .line 117
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 119
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    invoke-interface {v0, p1}, Lorg/icepdf/index/java/awt/Shape;->contains(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z

    move-result v0

    goto :goto_0
.end method

.method public createTransformedArea(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/Area;
    .locals 2
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 323
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    if-nez v0, :cond_0

    new-instance v0, Lorg/icepdf/index/java/awt/geom/Area;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/Area;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Area;

    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    invoke-virtual {p1, v1}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->createTransformedShape(Lorg/icepdf/index/java/awt/Shape;)Lorg/icepdf/index/java/awt/Shape;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/icepdf/index/java/awt/geom/Area;-><init>(Lorg/icepdf/index/java/awt/Shape;)V

    goto :goto_0
.end method

.method public equals(Lorg/icepdf/index/java/awt/geom/Area;)Z
    .locals 2
    .param p1, "obj"    # Lorg/icepdf/index/java/awt/geom/Area;

    .prologue
    .line 132
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public exclusiveOr(Lorg/icepdf/index/java/awt/geom/Area;)V
    .locals 2
    .param p1, "area"    # Lorg/icepdf/index/java/awt/geom/Area;

    .prologue
    .line 184
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method extractRectangle()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v9, 0x2

    const/4 v6, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 195
    iget-object v7, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    if-nez v7, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-object v6

    .line 198
    :cond_1
    const/16 v7, 0xc

    new-array v4, v7, [F

    .line 199
    .local v4, "points":[F
    const/4 v1, 0x0

    .line 200
    .local v1, "count":I
    iget-object v7, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    invoke-interface {v7, v6}, Lorg/icepdf/index/java/awt/Shape;->getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;

    move-result-object v3

    .line 201
    .local v3, "p":Lorg/icepdf/index/java/awt/geom/PathIterator;
    const/4 v7, 0x6

    new-array v0, v7, [F

    .line 202
    .local v0, "coords":[F
    :goto_1
    invoke-interface {v3}, Lorg/icepdf/index/java/awt/geom/PathIterator;->isDone()Z

    move-result v7

    if-nez v7, :cond_2

    .line 203
    invoke-interface {v3, v0}, Lorg/icepdf/index/java/awt/geom/PathIterator;->currentSegment([F)I

    move-result v5

    .line 204
    .local v5, "type":I
    const/16 v7, 0xc

    if-gt v1, v7, :cond_0

    if-eq v5, v9, :cond_0

    if-eq v5, v12, :cond_0

    .line 207
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "count":I
    .local v2, "count":I
    aget v7, v0, v10

    aput v7, v4, v1

    .line 208
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "count":I
    .restart local v1    # "count":I
    aget v7, v0, v11

    aput v7, v4, v2

    .line 209
    invoke-interface {v3}, Lorg/icepdf/index/java/awt/geom/PathIterator;->next()V

    goto :goto_1

    .line 211
    .end local v5    # "type":I
    :cond_2
    aget v7, v4, v10

    const/4 v8, 0x6

    aget v8, v4, v8

    cmpl-float v7, v7, v8

    if-nez v7, :cond_0

    const/4 v7, 0x6

    aget v7, v4, v7

    const/16 v8, 0x8

    aget v8, v4, v8

    cmpl-float v7, v7, v8

    if-nez v7, :cond_0

    aget v7, v4, v9

    const/4 v8, 0x4

    aget v8, v4, v8

    cmpl-float v7, v7, v8

    if-nez v7, :cond_0

    aget v7, v4, v11

    aget v8, v4, v12

    cmpl-float v7, v7, v8

    if-nez v7, :cond_0

    aget v7, v4, v12

    const/16 v8, 0x9

    aget v8, v4, v8

    cmpl-float v7, v7, v8

    if-nez v7, :cond_0

    const/4 v7, 0x5

    aget v7, v4, v7

    const/4 v8, 0x7

    aget v8, v4, v8

    cmpl-float v7, v7, v8

    if-nez v7, :cond_0

    .line 213
    new-instance v6, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    aget v7, v4, v10

    aget v8, v4, v11

    aget v9, v4, v9

    aget v10, v4, v10

    sub-float/2addr v9, v10

    const/4 v10, 0x7

    aget v10, v4, v10

    aget v11, v4, v11

    sub-float/2addr v10, v11

    invoke-direct {v6, v7, v8, v9, v10}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    goto :goto_0
.end method

.method public getBounds()Lorg/icepdf/index/java/awt/Rectangle;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    if-nez v0, :cond_0

    new-instance v0, Lorg/icepdf/index/java/awt/Rectangle;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/Rectangle;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    invoke-interface {v0}, Lorg/icepdf/index/java/awt/Shape;->getBounds()Lorg/icepdf/index/java/awt/Rectangle;

    move-result-object v0

    goto :goto_0
.end method

.method public getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    if-nez v0, :cond_0

    new-instance v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    invoke-interface {v0}, Lorg/icepdf/index/java/awt/Shape;->getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    move-result-object v0

    goto :goto_0
.end method

.method public getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;
    .locals 1
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 155
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    if-nez v0, :cond_0

    new-instance v0, Lorg/icepdf/index/java/awt/geom/Area$NullIterator;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/Area$NullIterator;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    invoke-interface {v0, p1}, Lorg/icepdf/index/java/awt/Shape;->getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;

    move-result-object v0

    goto :goto_0
.end method

.method public getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;D)Lorg/icepdf/index/java/awt/geom/PathIterator;
    .locals 2
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .param p2, "flatness"    # D

    .prologue
    .line 159
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    if-nez v0, :cond_0

    new-instance v0, Lorg/icepdf/index/java/awt/geom/Area$NullIterator;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/Area$NullIterator;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    invoke-interface {v0, p1, p2, p3}, Lorg/icepdf/index/java/awt/Shape;->getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;D)Lorg/icepdf/index/java/awt/geom/PathIterator;

    move-result-object v0

    goto :goto_0
.end method

.method public intersect(Lorg/icepdf/index/java/awt/geom/Area;)V
    .locals 3
    .param p1, "area"    # Lorg/icepdf/index/java/awt/geom/Area;

    .prologue
    .line 229
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Area;->extractRectangle()Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    move-result-object v0

    .line 230
    .local v0, "src1":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Area;->extractRectangle()Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    move-result-object v1

    .line 231
    .local v1, "src2":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 232
    iget-object v2, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    check-cast v2, Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    invoke-static {v0, v1, v2}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->intersect(Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    .line 234
    :cond_0
    return-void
.end method

.method public intersects(DDDD)Z
    .locals 11
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 136
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    move-wide v2, p1

    move-wide v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    invoke-interface/range {v1 .. v9}, Lorg/icepdf/index/java/awt/Shape;->intersects(DDDD)Z

    move-result v0

    goto :goto_0
.end method

.method public intersects(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
    .locals 1
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 140
    if-nez p1, :cond_0

    .line 141
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 143
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    invoke-interface {v0, p1}, Lorg/icepdf/index/java/awt/Shape;->intersects(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z

    move-result v0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 256
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isPolygonal()Z
    .locals 2

    .prologue
    .line 267
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isRectangular()Z
    .locals 2

    .prologue
    .line 278
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isSingular()Z
    .locals 2

    .prologue
    .line 289
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 299
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public subtract(Lorg/icepdf/index/java/awt/geom/Area;)V
    .locals 2
    .param p1, "area"    # Lorg/icepdf/index/java/awt/geom/Area;

    .prologue
    .line 245
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public transform(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V
    .locals 1
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 310
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    invoke-virtual {p1, v0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->createTransformedShape(Lorg/icepdf/index/java/awt/Shape;)Lorg/icepdf/index/java/awt/Shape;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/index/java/awt/geom/Area;->s:Lorg/icepdf/index/java/awt/Shape;

    .line 311
    return-void
.end method
