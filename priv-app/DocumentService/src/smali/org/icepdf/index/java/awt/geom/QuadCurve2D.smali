.class public abstract Lorg/icepdf/index/java/awt/geom/QuadCurve2D;
.super Ljava/lang/Object;
.source "QuadCurve2D.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/icepdf/index/java/awt/Shape;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;,
        Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;,
        Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298
    return-void
.end method

.method public static getFlatness(DDDDDD)D
    .locals 12
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "ctrlx"    # D
    .param p6, "ctrly"    # D
    .param p8, "x2"    # D
    .param p10, "y2"    # D

    .prologue
    .line 370
    move-wide v0, p0

    move-wide v2, p2

    move-wide/from16 v4, p8

    move-wide/from16 v6, p10

    move-wide/from16 v8, p4

    move-wide/from16 v10, p6

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptSegDist(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static getFlatness([DI)D
    .locals 12
    .param p0, "coords"    # [D
    .param p1, "offset"    # I

    .prologue
    .line 374
    add-int/lit8 v0, p1, 0x0

    aget-wide v0, p0, v0

    add-int/lit8 v2, p1, 0x1

    aget-wide v2, p0, v2

    add-int/lit8 v4, p1, 0x4

    aget-wide v4, p0, v4

    add-int/lit8 v6, p1, 0x5

    aget-wide v6, p0, v6

    add-int/lit8 v8, p1, 0x2

    aget-wide v8, p0, v8

    add-int/lit8 v10, p1, 0x3

    aget-wide v10, p0, v10

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptSegDist(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static getFlatnessSq(DDDDDD)D
    .locals 12
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "ctrlx"    # D
    .param p6, "ctrly"    # D
    .param p8, "x2"    # D
    .param p10, "y2"    # D

    .prologue
    .line 353
    move-wide v0, p0

    move-wide v2, p2

    move-wide/from16 v4, p8

    move-wide/from16 v6, p10

    move-wide/from16 v8, p4

    move-wide/from16 v10, p6

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptSegDistSq(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static getFlatnessSq([DI)D
    .locals 12
    .param p0, "coords"    # [D
    .param p1, "offset"    # I

    .prologue
    .line 357
    add-int/lit8 v0, p1, 0x0

    aget-wide v0, p0, v0

    add-int/lit8 v2, p1, 0x1

    aget-wide v2, p0, v2

    add-int/lit8 v4, p1, 0x4

    aget-wide v4, p0, v4

    add-int/lit8 v6, p1, 0x5

    aget-wide v6, p0, v6

    add-int/lit8 v8, p1, 0x2

    aget-wide v8, p0, v8

    add-int/lit8 v10, p1, 0x3

    aget-wide v10, p0, v10

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptSegDistSq(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static solveQuadratic([D)I
    .locals 1
    .param p0, "eqn"    # [D

    .prologue
    .line 439
    invoke-static {p0, p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->solveQuadratic([D[D)I

    move-result v0

    return v0
.end method

.method public static solveQuadratic([D[D)I
    .locals 1
    .param p0, "eqn"    # [D
    .param p1, "res"    # [D

    .prologue
    .line 443
    invoke-static {p0, p1}, Lorg/apache/harmony/awt/gl/Crossing;->solveQuad([D[D)I

    move-result v0

    return v0
.end method

.method public static subdivide(Lorg/icepdf/index/java/awt/geom/QuadCurve2D;Lorg/icepdf/index/java/awt/geom/QuadCurve2D;Lorg/icepdf/index/java/awt/geom/QuadCurve2D;)V
    .locals 28
    .param p0, "src"    # Lorg/icepdf/index/java/awt/geom/QuadCurve2D;
    .param p1, "left"    # Lorg/icepdf/index/java/awt/geom/QuadCurve2D;
    .param p2, "right"    # Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    .prologue
    .line 385
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getX1()D

    move-result-wide v2

    .line 386
    .local v2, "x1":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getY1()D

    move-result-wide v4

    .line 387
    .local v4, "y1":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getCtrlX()D

    move-result-wide v10

    .line 388
    .local v10, "cx":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getCtrlY()D

    move-result-wide v12

    .line 389
    .local v12, "cy":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getX2()D

    move-result-wide v24

    .line 390
    .local v24, "x2":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getY2()D

    move-result-wide v26

    .line 391
    .local v26, "y2":D
    add-double v0, v2, v10

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double v6, v0, v14

    .line 392
    .local v6, "cx1":D
    add-double v0, v4, v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double v8, v0, v14

    .line 393
    .local v8, "cy1":D
    add-double v0, v24, v10

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double v20, v0, v14

    .line 394
    .local v20, "cx2":D
    add-double v0, v26, v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double v22, v0, v14

    .line 395
    .local v22, "cy2":D
    add-double v0, v6, v20

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double v10, v0, v14

    .line 396
    add-double v0, v8, v22

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double v12, v0, v14

    .line 397
    if-eqz p1, :cond_0

    move-object/from16 v1, p1

    .line 398
    invoke-virtual/range {v1 .. v13}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->setCurve(DDDDDD)V

    .line 400
    :cond_0
    if-eqz p2, :cond_1

    move-object/from16 v15, p2

    move-wide/from16 v16, v10

    move-wide/from16 v18, v12

    .line 401
    invoke-virtual/range {v15 .. v27}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->setCurve(DDDDDD)V

    .line 403
    :cond_1
    return-void
.end method

.method public static subdivide([DI[DI[DI)V
    .locals 24
    .param p0, "src"    # [D
    .param p1, "srcoff"    # I
    .param p2, "left"    # [D
    .param p3, "leftOff"    # I
    .param p4, "right"    # [D
    .param p5, "rightOff"    # I

    .prologue
    .line 408
    add-int/lit8 v20, p1, 0x0

    aget-wide v12, p0, v20

    .line 409
    .local v12, "x1":D
    add-int/lit8 v20, p1, 0x1

    aget-wide v16, p0, v20

    .line 410
    .local v16, "y1":D
    add-int/lit8 v20, p1, 0x2

    aget-wide v0, p0, v20

    .line 411
    .local v0, "cx":D
    add-int/lit8 v20, p1, 0x3

    aget-wide v6, p0, v20

    .line 412
    .local v6, "cy":D
    add-int/lit8 v20, p1, 0x4

    aget-wide v14, p0, v20

    .line 413
    .local v14, "x2":D
    add-int/lit8 v20, p1, 0x5

    aget-wide v18, p0, v20

    .line 414
    .local v18, "y2":D
    add-double v20, v12, v0

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v2, v20, v22

    .line 415
    .local v2, "cx1":D
    add-double v20, v16, v6

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v8, v20, v22

    .line 416
    .local v8, "cy1":D
    add-double v20, v14, v0

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v4, v20, v22

    .line 417
    .local v4, "cx2":D
    add-double v20, v18, v6

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v10, v20, v22

    .line 418
    .local v10, "cy2":D
    add-double v20, v2, v4

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v0, v20, v22

    .line 419
    add-double v20, v8, v10

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v6, v20, v22

    .line 420
    if-eqz p2, :cond_0

    .line 421
    add-int/lit8 v20, p3, 0x0

    aput-wide v12, p2, v20

    .line 422
    add-int/lit8 v20, p3, 0x1

    aput-wide v16, p2, v20

    .line 423
    add-int/lit8 v20, p3, 0x2

    aput-wide v2, p2, v20

    .line 424
    add-int/lit8 v20, p3, 0x3

    aput-wide v8, p2, v20

    .line 425
    add-int/lit8 v20, p3, 0x4

    aput-wide v0, p2, v20

    .line 426
    add-int/lit8 v20, p3, 0x5

    aput-wide v6, p2, v20

    .line 428
    :cond_0
    if-eqz p4, :cond_1

    .line 429
    add-int/lit8 v20, p5, 0x0

    aput-wide v0, p4, v20

    .line 430
    add-int/lit8 v20, p5, 0x1

    aput-wide v6, p4, v20

    .line 431
    add-int/lit8 v20, p5, 0x2

    aput-wide v4, p4, v20

    .line 432
    add-int/lit8 v20, p5, 0x3

    aput-wide v10, p4, v20

    .line 433
    add-int/lit8 v20, p5, 0x4

    aput-wide v14, p4, v20

    .line 434
    add-int/lit8 v20, p5, 0x5

    aput-wide v18, p4, v20

    .line 436
    :cond_1
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 487
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 488
    :catch_0
    move-exception v0

    .line 489
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public contains(DD)Z
    .locals 1
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    .line 447
    invoke-static {p0, p1, p2, p3, p4}, Lorg/apache/harmony/awt/gl/Crossing;->crossShape(Lorg/icepdf/index/java/awt/Shape;DD)I

    move-result v0

    invoke-static {v0}, Lorg/apache/harmony/awt/gl/Crossing;->isInsideEvenOdd(I)Z

    move-result v0

    return v0
.end method

.method public contains(DDDD)Z
    .locals 3
    .param p1, "rx"    # D
    .param p3, "ry"    # D
    .param p5, "rw"    # D
    .param p7, "rh"    # D

    .prologue
    .line 451
    invoke-static/range {p0 .. p8}, Lorg/apache/harmony/awt/gl/Crossing;->intersectShape(Lorg/icepdf/index/java/awt/Shape;DDDD)I

    move-result v0

    .line 452
    .local v0, "cross":I
    const/16 v1, 0xff

    if-eq v0, v1, :cond_0

    invoke-static {v0}, Lorg/apache/harmony/awt/gl/Crossing;->isInsideEvenOdd(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public contains(Lorg/icepdf/index/java/awt/geom/Point2D;)Z
    .locals 4
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 461
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->contains(DD)Z

    move-result v0

    return v0
.end method

.method public contains(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
    .locals 10
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 469
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->contains(DDDD)Z

    move-result v0

    return v0
.end method

.method public getBounds()Lorg/icepdf/index/java/awt/Rectangle;
    .locals 1

    .prologue
    .line 473
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    move-result-object v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getBounds()Lorg/icepdf/index/java/awt/Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public abstract getCtrlPt()Lorg/icepdf/index/java/awt/geom/Point2D;
.end method

.method public abstract getCtrlX()D
.end method

.method public abstract getCtrlY()D
.end method

.method public getFlatness()D
    .locals 12

    .prologue
    .line 364
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getX1()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getY1()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getX2()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getY2()D

    move-result-wide v6

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getCtrlX()D

    move-result-wide v8

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getCtrlY()D

    move-result-wide v10

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptSegDist(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFlatnessSq()D
    .locals 12

    .prologue
    .line 346
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getX1()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getY1()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getX2()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getY2()D

    move-result-wide v6

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getCtrlX()D

    move-result-wide v8

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getCtrlY()D

    move-result-wide v10

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptSegDistSq(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public abstract getP1()Lorg/icepdf/index/java/awt/geom/Point2D;
.end method

.method public abstract getP2()Lorg/icepdf/index/java/awt/geom/Point2D;
.end method

.method public getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;
    .locals 1
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 477
    new-instance v0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;

    invoke-direct {v0, p0, p1}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;-><init>(Lorg/icepdf/index/java/awt/geom/QuadCurve2D;Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    return-object v0
.end method

.method public getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;D)Lorg/icepdf/index/java/awt/geom/PathIterator;
    .locals 2
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .param p2, "flatness"    # D

    .prologue
    .line 481
    new-instance v0, Lorg/icepdf/index/java/awt/geom/FlatteningPathIterator;

    invoke-virtual {p0, p1}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;

    move-result-object v1

    invoke-direct {v0, v1, p2, p3}, Lorg/icepdf/index/java/awt/geom/FlatteningPathIterator;-><init>(Lorg/icepdf/index/java/awt/geom/PathIterator;D)V

    return-object v0
.end method

.method public abstract getX1()D
.end method

.method public abstract getX2()D
.end method

.method public abstract getY1()D
.end method

.method public abstract getY2()D
.end method

.method public intersects(DDDD)Z
    .locals 3
    .param p1, "rx"    # D
    .param p3, "ry"    # D
    .param p5, "rw"    # D
    .param p7, "rh"    # D

    .prologue
    .line 456
    invoke-static/range {p0 .. p8}, Lorg/apache/harmony/awt/gl/Crossing;->intersectShape(Lorg/icepdf/index/java/awt/Shape;DDDD)I

    move-result v0

    .line 457
    .local v0, "cross":I
    const/16 v1, 0xff

    if-eq v0, v1, :cond_0

    invoke-static {v0}, Lorg/apache/harmony/awt/gl/Crossing;->isInsideEvenOdd(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public intersects(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
    .locals 10
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 465
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->intersects(DDDD)Z

    move-result v0

    return v0
.end method

.method public abstract setCurve(DDDDDD)V
.end method

.method public setCurve(Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;)V
    .locals 14
    .param p1, "p1"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "cp"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p3, "p2"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 321
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v4

    invoke-virtual/range {p2 .. p2}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v6

    invoke-virtual/range {p2 .. p2}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v8

    invoke-virtual/range {p3 .. p3}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v10

    invoke-virtual/range {p3 .. p3}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v12

    move-object v1, p0

    invoke-virtual/range {v1 .. v13}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->setCurve(DDDDDD)V

    .line 322
    return-void
.end method

.method public setCurve(Lorg/icepdf/index/java/awt/geom/QuadCurve2D;)V
    .locals 14
    .param p1, "curve"    # Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    .prologue
    .line 339
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getX1()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getY1()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getCtrlX()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getCtrlY()D

    move-result-wide v8

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getX2()D

    move-result-wide v10

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getY2()D

    move-result-wide v12

    move-object v1, p0

    invoke-virtual/range {v1 .. v13}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->setCurve(DDDDDD)V

    .line 343
    return-void
.end method

.method public setCurve([DI)V
    .locals 14
    .param p1, "coords"    # [D
    .param p2, "offset"    # I

    .prologue
    .line 325
    add-int/lit8 v0, p2, 0x0

    aget-wide v2, p1, v0

    add-int/lit8 v0, p2, 0x1

    aget-wide v4, p1, v0

    add-int/lit8 v0, p2, 0x2

    aget-wide v6, p1, v0

    add-int/lit8 v0, p2, 0x3

    aget-wide v8, p1, v0

    add-int/lit8 v0, p2, 0x4

    aget-wide v10, p1, v0

    add-int/lit8 v0, p2, 0x5

    aget-wide v12, p1, v0

    move-object v1, p0

    invoke-virtual/range {v1 .. v13}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->setCurve(DDDDDD)V

    .line 329
    return-void
.end method

.method public setCurve([Lorg/icepdf/index/java/awt/geom/Point2D;I)V
    .locals 14
    .param p1, "points"    # [Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "offset"    # I

    .prologue
    .line 332
    add-int/lit8 v0, p2, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v2

    add-int/lit8 v0, p2, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v4

    add-int/lit8 v0, p2, 0x1

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v6

    add-int/lit8 v0, p2, 0x1

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v8

    add-int/lit8 v0, p2, 0x2

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v10

    add-int/lit8 v0, p2, 0x2

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v12

    move-object v1, p0

    invoke-virtual/range {v1 .. v13}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->setCurve(DDDDDD)V

    .line 336
    return-void
.end method

.method public subdivide(Lorg/icepdf/index/java/awt/geom/QuadCurve2D;Lorg/icepdf/index/java/awt/geom/QuadCurve2D;)V
    .locals 0
    .param p1, "left"    # Lorg/icepdf/index/java/awt/geom/QuadCurve2D;
    .param p2, "right"    # Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    .prologue
    .line 381
    invoke-static {p0, p1, p2}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->subdivide(Lorg/icepdf/index/java/awt/geom/QuadCurve2D;Lorg/icepdf/index/java/awt/geom/QuadCurve2D;Lorg/icepdf/index/java/awt/geom/QuadCurve2D;)V

    .line 382
    return-void
.end method
