.class public Lorg/icepdf/index/java/awt/geom/Point2D$Float;
.super Lorg/icepdf/index/java/awt/geom/Point2D;
.source "Point2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/Point2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Float"
.end annotation


# instance fields
.field public x:F

.field public y:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;-><init>()V

    .line 32
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;-><init>()V

    .line 35
    iput p1, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;->x:F

    .line 36
    iput p2, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;->y:F

    .line 37
    return-void
.end method


# virtual methods
.method public getX()D
    .locals 2

    .prologue
    .line 41
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;->x:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 46
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;->y:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public setLocation(DD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 56
    double-to-float v0, p1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;->x:F

    .line 57
    double-to-float v0, p3

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;->y:F

    .line 58
    return-void
.end method

.method public setLocation(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 50
    iput p1, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;->x:F

    .line 51
    iput p2, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;->y:F

    .line 52
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "[x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
