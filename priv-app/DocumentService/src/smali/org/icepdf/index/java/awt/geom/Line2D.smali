.class public abstract Lorg/icepdf/index/java/awt/geom/Line2D;
.super Ljava/lang/Object;
.source "Line2D.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/icepdf/index/java/awt/Shape;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;,
        Lorg/icepdf/index/java/awt/geom/Line2D$Double;,
        Lorg/icepdf/index/java/awt/geom/Line2D$Float;
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295
    return-void
.end method

.method public static linesIntersect(DDDDDDDD)Z
    .locals 10
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "x2"    # D
    .param p6, "y2"    # D
    .param p8, "x3"    # D
    .param p10, "y3"    # D
    .param p12, "x4"    # D
    .param p14, "y4"    # D

    .prologue
    .line 368
    sub-double/2addr p4, p0

    .line 369
    sub-double p6, p6, p2

    .line 370
    sub-double p8, p8, p0

    .line 371
    sub-double p10, p10, p2

    .line 372
    sub-double p12, p12, p0

    .line 373
    sub-double p14, p14, p2

    .line 375
    mul-double v6, p4, p10

    mul-double v8, p8, p6

    sub-double v0, v6, v8

    .line 376
    .local v0, "AvB":D
    mul-double v6, p4, p14

    mul-double v8, p12, p6

    sub-double v2, v6, v8

    .line 379
    .local v2, "AvC":D
    const-wide/16 v6, 0x0

    cmpl-double v6, v0, v6

    if-nez v6, :cond_8

    const-wide/16 v6, 0x0

    cmpl-double v6, v2, v6

    if-nez v6, :cond_8

    .line 380
    const-wide/16 v6, 0x0

    cmpl-double v6, p4, v6

    if-eqz v6, :cond_3

    .line 381
    mul-double v6, p12, p8

    const-wide/16 v8, 0x0

    cmpg-double v6, v6, v8

    if-lez v6, :cond_0

    mul-double v6, p8, p4

    const-wide/16 v8, 0x0

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_2

    const-wide/16 v6, 0x0

    cmpl-double v6, p4, v6

    if-lez v6, :cond_1

    cmpg-double v6, p8, p4

    if-lez v6, :cond_0

    cmpg-double v6, p12, p4

    if-gtz v6, :cond_2

    :cond_0
    const/4 v6, 0x1

    .line 397
    :goto_0
    return v6

    .line 381
    :cond_1
    cmpl-double v6, p8, p4

    if-gez v6, :cond_0

    cmpl-double v6, p12, p4

    if-gez v6, :cond_0

    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 386
    :cond_3
    const-wide/16 v6, 0x0

    cmpl-double v6, p6, v6

    if-eqz v6, :cond_7

    .line 387
    mul-double v6, p14, p10

    const-wide/16 v8, 0x0

    cmpg-double v6, v6, v8

    if-lez v6, :cond_4

    mul-double v6, p10, p6

    const-wide/16 v8, 0x0

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_6

    const-wide/16 v6, 0x0

    cmpl-double v6, p6, v6

    if-lez v6, :cond_5

    cmpg-double v6, p10, p6

    if-lez v6, :cond_4

    cmpg-double v6, p14, p6

    if-gtz v6, :cond_6

    :cond_4
    const/4 v6, 0x1

    goto :goto_0

    :cond_5
    cmpl-double v6, p10, p6

    if-gez v6, :cond_4

    cmpl-double v6, p14, p6

    if-gez v6, :cond_4

    :cond_6
    const/4 v6, 0x0

    goto :goto_0

    .line 392
    :cond_7
    const/4 v6, 0x0

    goto :goto_0

    .line 395
    :cond_8
    mul-double v6, p8, p14

    mul-double v8, p12, p10

    sub-double v4, v6, v8

    .line 397
    .local v4, "BvC":D
    mul-double v6, v0, v2

    const-wide/16 v8, 0x0

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_9

    add-double v6, v0, v4

    sub-double/2addr v6, v2

    mul-double/2addr v6, v4

    const-wide/16 v8, 0x0

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_9

    const/4 v6, 0x1

    goto :goto_0

    :cond_9
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static ptLineDist(DDDDDD)D
    .locals 2
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "x2"    # D
    .param p6, "y2"    # D
    .param p8, "px"    # D
    .param p10, "py"    # D

    .prologue
    .line 465
    invoke-static/range {p0 .. p11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptLineDistSq(DDDDDD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static ptLineDistSq(DDDDDD)D
    .locals 8
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "x2"    # D
    .param p6, "y2"    # D
    .param p8, "px"    # D
    .param p10, "py"    # D

    .prologue
    .line 456
    sub-double/2addr p4, p0

    .line 457
    sub-double/2addr p6, p2

    .line 458
    sub-double p8, p8, p0

    .line 459
    sub-double p10, p10, p2

    .line 460
    mul-double v2, p8, p6

    mul-double v4, p10, p4

    sub-double v0, v2, v4

    .line 461
    .local v0, "s":D
    mul-double v2, v0, v0

    mul-double v4, p4, p4

    mul-double v6, p6, p6

    add-double/2addr v4, v6

    div-double/2addr v2, v4

    return-wide v2
.end method

.method public static ptSegDist(DDDDDD)D
    .locals 2
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "x2"    # D
    .param p6, "y2"    # D
    .param p8, "px"    # D
    .param p10, "py"    # D

    .prologue
    .line 436
    invoke-static/range {p0 .. p11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptSegDistSq(DDDDDD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static ptSegDistSq(DDDDDD)D
    .locals 8
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "x2"    # D
    .param p6, "y2"    # D
    .param p8, "px"    # D
    .param p10, "py"    # D

    .prologue
    .line 412
    sub-double/2addr p4, p0

    .line 413
    sub-double/2addr p6, p2

    .line 414
    sub-double p8, p8, p0

    .line 415
    sub-double p10, p10, p2

    .line 417
    mul-double v2, p8, p4

    mul-double v4, p10, p6

    add-double/2addr v2, v4

    const-wide/16 v4, 0x0

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_1

    .line 418
    mul-double v2, p8, p8

    mul-double v4, p10, p10

    add-double v0, v2, v4

    .line 429
    .local v0, "dist":D
    :goto_0
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    .line 430
    const-wide/16 v0, 0x0

    .line 432
    :cond_0
    return-wide v0

    .line 420
    .end local v0    # "dist":D
    :cond_1
    sub-double p8, p4, p8

    .line 421
    sub-double p10, p6, p10

    .line 422
    mul-double v2, p8, p4

    mul-double v4, p10, p6

    add-double/2addr v2, v4

    const-wide/16 v4, 0x0

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_2

    .line 423
    mul-double v2, p8, p8

    mul-double v4, p10, p10

    add-double v0, v2, v4

    .restart local v0    # "dist":D
    goto :goto_0

    .line 425
    .end local v0    # "dist":D
    :cond_2
    mul-double v2, p8, p6

    mul-double v4, p10, p4

    sub-double v0, v2, v4

    .line 426
    .restart local v0    # "dist":D
    mul-double v2, v0, v0

    mul-double v4, p4, p4

    mul-double v6, p6, p6

    add-double/2addr v4, v6

    div-double v0, v2, v4

    goto :goto_0
.end method

.method public static relativeCCW(DDDDDD)I
    .locals 6
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "x2"    # D
    .param p6, "y2"    # D
    .param p8, "px"    # D
    .param p10, "py"    # D

    .prologue
    .line 327
    sub-double/2addr p4, p0

    .line 328
    sub-double/2addr p6, p2

    .line 329
    sub-double/2addr p8, p0

    .line 330
    sub-double p10, p10, p2

    .line 331
    mul-double v2, p8, p6

    mul-double v4, p10, p4

    sub-double v0, v2, v4

    .line 332
    .local v0, "t":D
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-nez v2, :cond_0

    .line 333
    mul-double v2, p8, p4

    mul-double v4, p10, p6

    add-double v0, v2, v4

    .line 334
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    .line 335
    sub-double/2addr p8, p4

    .line 336
    sub-double p10, p10, p6

    .line 337
    mul-double v2, p8, p4

    mul-double v4, p10, p6

    add-double v0, v2, v4

    .line 338
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    .line 339
    const-wide/16 v0, 0x0

    .line 344
    :cond_0
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_1

    const/4 v2, -0x1

    :goto_0
    return v2

    :cond_1
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 519
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 520
    :catch_0
    move-exception v0

    .line 521
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public contains(DD)Z
    .locals 1
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    .line 485
    const/4 v0, 0x0

    return v0
.end method

.method public contains(DDDD)Z
    .locals 1
    .param p1, "rx"    # D
    .param p3, "ry"    # D
    .param p5, "rw"    # D
    .param p7, "rh"    # D

    .prologue
    .line 497
    const/4 v0, 0x0

    return v0
.end method

.method public contains(Lorg/icepdf/index/java/awt/geom/Point2D;)Z
    .locals 1
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 489
    const/4 v0, 0x0

    return v0
.end method

.method public contains(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
    .locals 1
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 493
    const/4 v0, 0x0

    return v0
.end method

.method public getBounds()Lorg/icepdf/index/java/awt/Rectangle;
    .locals 1

    .prologue
    .line 320
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    move-result-object v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getBounds()Lorg/icepdf/index/java/awt/Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public abstract getP1()Lorg/icepdf/index/java/awt/geom/Point2D;
.end method

.method public abstract getP2()Lorg/icepdf/index/java/awt/geom/Point2D;
.end method

.method public getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;
    .locals 1
    .param p1, "at"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 509
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;

    invoke-direct {v0, p0, p1}, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;-><init>(Lorg/icepdf/index/java/awt/geom/Line2D;Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    return-object v0
.end method

.method public getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;D)Lorg/icepdf/index/java/awt/geom/PathIterator;
    .locals 1
    .param p1, "at"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .param p2, "flatness"    # D

    .prologue
    .line 513
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;

    invoke-direct {v0, p0, p1}, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;-><init>(Lorg/icepdf/index/java/awt/geom/Line2D;Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    return-object v0
.end method

.method public abstract getX1()D
.end method

.method public abstract getX2()D
.end method

.method public abstract getY1()D
.end method

.method public abstract getY2()D
.end method

.method public intersects(DDDD)Z
    .locals 11
    .param p1, "rx"    # D
    .param p3, "ry"    # D
    .param p5, "rw"    # D
    .param p7, "rh"    # D

    .prologue
    .line 501
    new-instance v1, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    move-wide v2, p1

    move-wide v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    invoke-virtual {p0, v1}, Lorg/icepdf/index/java/awt/geom/Line2D;->intersects(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z

    move-result v0

    return v0
.end method

.method public intersects(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
    .locals 10
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 505
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v6

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v8

    move-object v1, p1

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->intersectsLine(DDDD)Z

    move-result v0

    return v0
.end method

.method public intersectsLine(DDDD)Z
    .locals 17
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "x2"    # D
    .param p7, "y2"    # D

    .prologue
    .line 401
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v8

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v10

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v12

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v14

    move-wide/from16 v0, p1

    move-wide/from16 v2, p3

    move-wide/from16 v4, p5

    move-wide/from16 v6, p7

    invoke-static/range {v0 .. v15}, Lorg/icepdf/index/java/awt/geom/Line2D;->linesIntersect(DDDDDDDD)Z

    move-result v0

    return v0
.end method

.method public intersectsLine(Lorg/icepdf/index/java/awt/geom/Line2D;)Z
    .locals 16
    .param p1, "l"    # Lorg/icepdf/index/java/awt/geom/Line2D;

    .prologue
    .line 405
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v0

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v6

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v8

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v10

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v12

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v14

    invoke-static/range {v0 .. v15}, Lorg/icepdf/index/java/awt/geom/Line2D;->linesIntersect(DDDDDDDD)Z

    move-result v0

    return v0
.end method

.method public ptLineDist(DD)D
    .locals 13
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    .line 477
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v6

    move-wide v8, p1

    move-wide/from16 v10, p3

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptLineDist(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public ptLineDist(Lorg/icepdf/index/java/awt/geom/Point2D;)D
    .locals 12
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 481
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v8

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v10

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptLineDist(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public ptLineDistSq(DD)D
    .locals 13
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    .line 469
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v6

    move-wide v8, p1

    move-wide/from16 v10, p3

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptLineDistSq(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public ptLineDistSq(Lorg/icepdf/index/java/awt/geom/Point2D;)D
    .locals 12
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 473
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v8

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v10

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptLineDistSq(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public ptSegDist(DD)D
    .locals 13
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    .line 448
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v6

    move-wide v8, p1

    move-wide/from16 v10, p3

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptSegDist(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public ptSegDist(Lorg/icepdf/index/java/awt/geom/Point2D;)D
    .locals 12
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 452
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v8

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v10

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptSegDist(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public ptSegDistSq(DD)D
    .locals 13
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    .line 440
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v6

    move-wide v8, p1

    move-wide/from16 v10, p3

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptSegDistSq(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public ptSegDistSq(Lorg/icepdf/index/java/awt/geom/Point2D;)D
    .locals 12
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 444
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v8

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v10

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptSegDistSq(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public relativeCCW(DD)I
    .locals 13
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    .line 348
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v6

    move-wide v8, p1

    move-wide/from16 v10, p3

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->relativeCCW(DDDDDD)I

    move-result v0

    return v0
.end method

.method public relativeCCW(Lorg/icepdf/index/java/awt/geom/Point2D;)I
    .locals 12
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 352
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v8

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v10

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->relativeCCW(DDDDDD)I

    move-result v0

    return v0
.end method

.method public abstract setLine(DDDD)V
.end method

.method public setLine(Lorg/icepdf/index/java/awt/geom/Line2D;)V
    .locals 10
    .param p1, "line"    # Lorg/icepdf/index/java/awt/geom/Line2D;

    .prologue
    .line 316
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Line2D;->setLine(DDDD)V

    .line 317
    return-void
.end method

.method public setLine(Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;)V
    .locals 10
    .param p1, "p1"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "p2"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 312
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v4

    invoke-virtual {p2}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v6

    invoke-virtual {p2}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Line2D;->setLine(DDDD)V

    .line 313
    return-void
.end method
