.class public Lorg/icepdf/index/java/awt/geom/Line2D$Double;
.super Lorg/icepdf/index/java/awt/geom/Line2D;
.source "Line2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/Line2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Double"
.end annotation


# instance fields
.field public x1:D

.field public x2:D

.field public y1:D

.field public y2:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;-><init>()V

    .line 122
    return-void
.end method

.method public constructor <init>(DDDD)V
    .locals 1
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "x2"    # D
    .param p7, "y2"    # D

    .prologue
    .line 124
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;-><init>()V

    .line 125
    invoke-virtual/range {p0 .. p8}, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->setLine(DDDD)V

    .line 126
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;)V
    .locals 0
    .param p1, "p1"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "p2"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 128
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;-><init>()V

    .line 129
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->setLine(Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;)V

    .line 130
    return-void
.end method


# virtual methods
.method public getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 12

    .prologue
    .line 172
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x1:D

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x2:D

    cmpg-double v0, v0, v10

    if-gez v0, :cond_0

    .line 173
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x1:D

    .line 174
    .local v2, "rx":D
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x2:D

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x1:D

    sub-double v6, v0, v10

    .line 179
    .local v6, "rw":D
    :goto_0
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y1:D

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y2:D

    cmpg-double v0, v0, v10

    if-gez v0, :cond_1

    .line 180
    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y1:D

    .line 181
    .local v4, "ry":D
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y2:D

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y1:D

    sub-double v8, v0, v10

    .line 186
    .local v8, "rh":D
    :goto_1
    new-instance v1, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    invoke-direct/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    return-object v1

    .line 176
    .end local v2    # "rx":D
    .end local v4    # "ry":D
    .end local v6    # "rw":D
    .end local v8    # "rh":D
    :cond_0
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x2:D

    .line 177
    .restart local v2    # "rx":D
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x1:D

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x2:D

    sub-double v6, v0, v10

    .restart local v6    # "rw":D
    goto :goto_0

    .line 183
    :cond_1
    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y2:D

    .line 184
    .restart local v4    # "ry":D
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y1:D

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y2:D

    sub-double v8, v0, v10

    .restart local v8    # "rh":D
    goto :goto_1
.end method

.method public getP1()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 6

    .prologue
    .line 154
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x1:D

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y1:D

    invoke-direct {v0, v2, v3, v4, v5}, Lorg/icepdf/index/java/awt/geom/Point2D$Double;-><init>(DD)V

    return-object v0
.end method

.method public getP2()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 6

    .prologue
    .line 159
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x2:D

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y2:D

    invoke-direct {v0, v2, v3, v4, v5}, Lorg/icepdf/index/java/awt/geom/Point2D$Double;-><init>(DD)V

    return-object v0
.end method

.method public getX1()D
    .locals 2

    .prologue
    .line 134
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x1:D

    return-wide v0
.end method

.method public getX2()D
    .locals 2

    .prologue
    .line 144
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x2:D

    return-wide v0
.end method

.method public getY1()D
    .locals 2

    .prologue
    .line 139
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y1:D

    return-wide v0
.end method

.method public getY2()D
    .locals 2

    .prologue
    .line 149
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y2:D

    return-wide v0
.end method

.method public setLine(DDDD)V
    .locals 1
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "x2"    # D
    .param p7, "y2"    # D

    .prologue
    .line 164
    iput-wide p1, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x1:D

    .line 165
    iput-wide p3, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y1:D

    .line 166
    iput-wide p5, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->x2:D

    .line 167
    iput-wide p7, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Double;->y2:D

    .line 168
    return-void
.end method
