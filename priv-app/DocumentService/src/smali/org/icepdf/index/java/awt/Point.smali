.class public Lorg/icepdf/index/java/awt/Point;
.super Lorg/icepdf/index/java/awt/geom/Point2D;
.source "Point.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x493b758dcb8137daL


# instance fields
.field public x:I

.field public y:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;-><init>()V

    .line 35
    invoke-virtual {p0, v0, v0}, Lorg/icepdf/index/java/awt/Point;->setLocation(II)V

    .line 36
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;-><init>()V

    .line 39
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/java/awt/Point;->setLocation(II)V

    .line 40
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/java/awt/Point;)V
    .locals 2
    .param p1, "p"    # Lorg/icepdf/index/java/awt/Point;

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;-><init>()V

    .line 43
    iget v0, p1, Lorg/icepdf/index/java/awt/Point;->x:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Point;->y:I

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/Point;->setLocation(II)V

    .line 44
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    if-ne p1, p0, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    instance-of v3, p1, Lorg/icepdf/index/java/awt/Point;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 52
    check-cast v0, Lorg/icepdf/index/java/awt/Point;

    .line 53
    .local v0, "p":Lorg/icepdf/index/java/awt/Point;
    iget v3, p0, Lorg/icepdf/index/java/awt/Point;->x:I

    iget v4, v0, Lorg/icepdf/index/java/awt/Point;->x:I

    if-ne v3, v4, :cond_2

    iget v3, p0, Lorg/icepdf/index/java/awt/Point;->y:I

    iget v4, v0, Lorg/icepdf/index/java/awt/Point;->y:I

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "p":Lorg/icepdf/index/java/awt/Point;
    :cond_3
    move v1, v2

    .line 55
    goto :goto_0
.end method

.method public getLocation()Lorg/icepdf/index/java/awt/Point;
    .locals 3

    .prologue
    .line 74
    new-instance v0, Lorg/icepdf/index/java/awt/Point;

    iget v1, p0, Lorg/icepdf/index/java/awt/Point;->x:I

    iget v2, p0, Lorg/icepdf/index/java/awt/Point;->y:I

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/java/awt/Point;-><init>(II)V

    return-object v0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 65
    iget v0, p0, Lorg/icepdf/index/java/awt/Point;->x:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 70
    iget v0, p0, Lorg/icepdf/index/java/awt/Point;->y:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public move(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 94
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/java/awt/Point;->setLocation(II)V

    .line 95
    return-void
.end method

.method public setLocation(DD)V
    .locals 5
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    const-wide v2, 0x41dfffffffc00000L    # 2.147483647E9

    const-wide/high16 v0, -0x3e20000000000000L    # -2.147483648E9

    .line 88
    cmpg-double v4, p1, v0

    if-gez v4, :cond_2

    move-wide p1, v0

    .line 89
    :cond_0
    :goto_0
    cmpg-double v4, p3, v0

    if-gez v4, :cond_3

    move-wide p3, v0

    .line 90
    :cond_1
    :goto_1
    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {p3, p4}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/Point;->setLocation(II)V

    .line 91
    return-void

    .line 88
    :cond_2
    cmpl-double v4, p1, v2

    if-lez v4, :cond_0

    move-wide p1, v2

    goto :goto_0

    .line 89
    :cond_3
    cmpl-double v0, p3, v2

    if-lez v0, :cond_1

    move-wide p3, v2

    goto :goto_1
.end method

.method public setLocation(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 82
    iput p1, p0, Lorg/icepdf/index/java/awt/Point;->x:I

    .line 83
    iput p2, p0, Lorg/icepdf/index/java/awt/Point;->y:I

    .line 84
    return-void
.end method

.method public setLocation(Lorg/icepdf/index/java/awt/Point;)V
    .locals 2
    .param p1, "p"    # Lorg/icepdf/index/java/awt/Point;

    .prologue
    .line 78
    iget v0, p1, Lorg/icepdf/index/java/awt/Point;->x:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Point;->y:I

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/Point;->setLocation(II)V

    .line 79
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "[x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/Point;->x:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/Point;->y:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public translate(II)V
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 98
    iget v0, p0, Lorg/icepdf/index/java/awt/Point;->x:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/icepdf/index/java/awt/Point;->x:I

    .line 99
    iget v0, p0, Lorg/icepdf/index/java/awt/Point;->y:I

    add-int/2addr v0, p2

    iput v0, p0, Lorg/icepdf/index/java/awt/Point;->y:I

    .line 100
    return-void
.end method
