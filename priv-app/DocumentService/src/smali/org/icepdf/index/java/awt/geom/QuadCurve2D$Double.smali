.class public Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;
.super Lorg/icepdf/index/java/awt/geom/QuadCurve2D;
.source "QuadCurve2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/QuadCurve2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Double"
.end annotation


# instance fields
.field public ctrlx:D

.field public ctrly:D

.field public x1:D

.field public x2:D

.field public y1:D

.field public y2:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;-><init>()V

    .line 131
    return-void
.end method

.method public constructor <init>(DDDDDD)V
    .locals 1
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "ctrlx"    # D
    .param p7, "ctrly"    # D
    .param p9, "x2"    # D
    .param p11, "y2"    # D

    .prologue
    .line 133
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;-><init>()V

    .line 134
    invoke-virtual/range {p0 .. p12}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->setCurve(DDDDDD)V

    .line 135
    return-void
.end method


# virtual methods
.method public getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 14

    .prologue
    .line 193
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->x1:D

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->x2:D

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->ctrlx:D

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 194
    .local v2, "rx0":D
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->y1:D

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->y2:D

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->ctrly:D

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    .line 195
    .local v4, "ry0":D
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->x1:D

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->x2:D

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->ctrlx:D

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    .line 196
    .local v10, "rx1":D
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->y1:D

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->y2:D

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->ctrly:D

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v12

    .line 197
    .local v12, "ry1":D
    new-instance v1, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    sub-double v6, v10, v2

    sub-double v8, v12, v4

    invoke-direct/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    return-object v1
.end method

.method public getCtrlPt()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 6

    .prologue
    .line 174
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->ctrlx:D

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->ctrly:D

    invoke-direct {v0, v2, v3, v4, v5}, Lorg/icepdf/index/java/awt/geom/Point2D$Double;-><init>(DD)V

    return-object v0
.end method

.method public getCtrlX()D
    .locals 2

    .prologue
    .line 149
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->ctrlx:D

    return-wide v0
.end method

.method public getCtrlY()D
    .locals 2

    .prologue
    .line 154
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->ctrly:D

    return-wide v0
.end method

.method public getP1()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 6

    .prologue
    .line 169
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->x1:D

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->y1:D

    invoke-direct {v0, v2, v3, v4, v5}, Lorg/icepdf/index/java/awt/geom/Point2D$Double;-><init>(DD)V

    return-object v0
.end method

.method public getP2()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 6

    .prologue
    .line 179
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->x2:D

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->y2:D

    invoke-direct {v0, v2, v3, v4, v5}, Lorg/icepdf/index/java/awt/geom/Point2D$Double;-><init>(DD)V

    return-object v0
.end method

.method public getX1()D
    .locals 2

    .prologue
    .line 139
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->x1:D

    return-wide v0
.end method

.method public getX2()D
    .locals 2

    .prologue
    .line 159
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->x2:D

    return-wide v0
.end method

.method public getY1()D
    .locals 2

    .prologue
    .line 144
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->y1:D

    return-wide v0
.end method

.method public getY2()D
    .locals 2

    .prologue
    .line 164
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->y2:D

    return-wide v0
.end method

.method public setCurve(DDDDDD)V
    .locals 1
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "ctrlx"    # D
    .param p7, "ctrly"    # D
    .param p9, "x2"    # D
    .param p11, "y2"    # D

    .prologue
    .line 184
    iput-wide p1, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->x1:D

    .line 185
    iput-wide p3, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->y1:D

    .line 186
    iput-wide p5, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->ctrlx:D

    .line 187
    iput-wide p7, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->ctrly:D

    .line 188
    iput-wide p9, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->x2:D

    .line 189
    iput-wide p11, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Double;->y2:D

    .line 190
    return-void
.end method
