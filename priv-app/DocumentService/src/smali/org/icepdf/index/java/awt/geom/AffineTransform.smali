.class public Lorg/icepdf/index/java/awt/geom/AffineTransform;
.super Ljava/lang/Object;
.source "AffineTransform.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final TYPE_FLIP:I = 0x40

.field public static final TYPE_GENERAL_ROTATION:I = 0x10

.field public static final TYPE_GENERAL_SCALE:I = 0x4

.field public static final TYPE_GENERAL_TRANSFORM:I = 0x20

.field public static final TYPE_IDENTITY:I = 0x0

.field public static final TYPE_MASK_ROTATION:I = 0x18

.field public static final TYPE_MASK_SCALE:I = 0x6

.field public static final TYPE_QUADRANT_ROTATION:I = 0x8

.field public static final TYPE_TRANSLATION:I = 0x1

.field public static final TYPE_UNIFORM_SCALE:I = 0x2

.field static final TYPE_UNKNOWN:I = -0x1

.field static final ZERO:D = 1.0E-10

.field private static final serialVersionUID:J = 0x127891154ad5ff62L


# instance fields
.field m00:D

.field m01:D

.field m02:D

.field m10:D

.field m11:D

.field m12:D

.field transient type:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 73
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    .line 74
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    .line 75
    return-void
.end method

.method public constructor <init>(DDDDDD)V
    .locals 1
    .param p1, "m00"    # D
    .param p3, "m10"    # D
    .param p5, "m01"    # D
    .param p7, "m11"    # D
    .param p9, "m02"    # D
    .param p11, "m12"    # D

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const/4 v0, -0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 99
    iput-wide p1, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    .line 100
    iput-wide p3, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    .line 101
    iput-wide p5, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    .line 102
    iput-wide p7, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    .line 103
    iput-wide p9, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    .line 104
    iput-wide p11, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    .line 105
    return-void
.end method

.method public constructor <init>(FFFFFF)V
    .locals 2
    .param p1, "m00"    # F
    .param p2, "m10"    # F
    .param p3, "m01"    # F
    .param p4, "m11"    # F
    .param p5, "m02"    # F
    .param p6, "m12"    # F

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/4 v0, -0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 89
    float-to-double v0, p1

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    .line 90
    float-to-double v0, p2

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    .line 91
    float-to-double v0, p3

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    .line 92
    float-to-double v0, p4

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    .line 93
    float-to-double v0, p5

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    .line 94
    float-to-double v0, p6

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    .line 95
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V
    .locals 2
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iget v0, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 79
    iget-wide v0, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    .line 80
    iget-wide v0, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    .line 81
    iget-wide v0, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    .line 82
    iget-wide v0, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    .line 83
    iget-wide v0, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    .line 84
    iget-wide v0, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    .line 85
    return-void
.end method

.method public constructor <init>([D)V
    .locals 3
    .param p1, "AffineTransform"    # [D

    .prologue
    const/4 v2, 0x4

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const/4 v0, -0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 121
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    .line 122
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    .line 123
    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    .line 124
    const/4 v0, 0x3

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    .line 125
    array-length v0, p1

    if-le v0, v2, :cond_0

    .line 126
    aget-wide v0, p1, v2

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    .line 127
    const/4 v0, 0x5

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    .line 129
    :cond_0
    return-void
.end method

.method public constructor <init>([F)V
    .locals 3
    .param p1, "AffineTransform"    # [F

    .prologue
    const/4 v2, 0x4

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v0, -0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 109
    const/4 v0, 0x0

    aget v0, p1, v0

    float-to-double v0, v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    .line 110
    const/4 v0, 0x1

    aget v0, p1, v0

    float-to-double v0, v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    .line 111
    const/4 v0, 0x2

    aget v0, p1, v0

    float-to-double v0, v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    .line 112
    const/4 v0, 0x3

    aget v0, p1, v0

    float-to-double v0, v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    .line 113
    array-length v0, p1

    if-le v0, v2, :cond_0

    .line 114
    aget v0, p1, v2

    float-to-double v0, v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    .line 115
    const/4 v0, 0x5

    aget v0, p1, v0

    float-to-double v0, v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    .line 117
    :cond_0
    return-void
.end method

.method public static getRotateInstance(D)Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .locals 2
    .param p0, "angle"    # D

    .prologue
    .line 338
    new-instance v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;-><init>()V

    .line 339
    .local v0, "t":Lorg/icepdf/index/java/awt/geom/AffineTransform;
    invoke-virtual {v0, p0, p1}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->setToRotation(D)V

    .line 340
    return-object v0
.end method

.method public static getRotateInstance(DDD)Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .locals 8
    .param p0, "angle"    # D
    .param p2, "x"    # D
    .param p4, "y"    # D

    .prologue
    .line 344
    new-instance v1, Lorg/icepdf/index/java/awt/geom/AffineTransform;

    invoke-direct {v1}, Lorg/icepdf/index/java/awt/geom/AffineTransform;-><init>()V

    .local v1, "t":Lorg/icepdf/index/java/awt/geom/AffineTransform;
    move-wide v2, p0

    move-wide v4, p2

    move-wide v6, p4

    .line 345
    invoke-virtual/range {v1 .. v7}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->setToRotation(DDD)V

    .line 346
    return-object v1
.end method

.method public static getScaleInstance(DD)Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .locals 2
    .param p0, "scx"    # D
    .param p2, "scY"    # D

    .prologue
    .line 326
    new-instance v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;-><init>()V

    .line 327
    .local v0, "t":Lorg/icepdf/index/java/awt/geom/AffineTransform;
    invoke-virtual {v0, p0, p1, p2, p3}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->setToScale(DD)V

    .line 328
    return-object v0
.end method

.method public static getShearInstance(DD)Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .locals 2
    .param p0, "shx"    # D
    .param p2, "shy"    # D

    .prologue
    .line 332
    new-instance v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;-><init>()V

    .line 333
    .local v0, "m":Lorg/icepdf/index/java/awt/geom/AffineTransform;
    invoke-virtual {v0, p0, p1, p2, p3}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->setToShear(DD)V

    .line 334
    return-object v0
.end method

.method public static getTranslateInstance(DD)Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .locals 2
    .param p0, "mx"    # D
    .param p2, "my"    # D

    .prologue
    .line 320
    new-instance v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;-><init>()V

    .line 321
    .local v0, "t":Lorg/icepdf/index/java/awt/geom/AffineTransform;
    invoke-virtual {v0, p0, p1, p2, p3}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->setToTranslation(DD)V

    .line 322
    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1
    .param p1, "stream"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 634
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 635
    const/4 v0, -0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 636
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 0
    .param p1, "stream"    # Ljava/io/ObjectOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 623
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 624
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 583
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 584
    :catch_0
    move-exception v0

    .line 585
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public concatenate(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V
    .locals 1
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 386
    invoke-virtual {p0, p1, p0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->multiply(Lorg/icepdf/index/java/awt/geom/AffineTransform;Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->setTransform(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    .line 387
    return-void
.end method

.method public createInverse()Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/index/java/awt/geom/NoninvertibleTransformException;
        }
    .end annotation

    .prologue
    .line 394
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->getDeterminant()D

    move-result-wide v16

    .line 395
    .local v16, "det":D
    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 397
    new-instance v2, Lorg/icepdf/index/java/awt/geom/NoninvertibleTransformException;

    const-string/jumbo v3, "awt.204"

    invoke-static {v3}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/icepdf/index/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 399
    :cond_0
    new-instance v3, Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    div-double v4, v4, v16

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    neg-double v6, v6

    div-double v6, v6, v16

    move-object/from16 v0, p0

    iget-wide v8, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    neg-double v8, v8

    div-double v8, v8, v16

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    div-double v10, v10, v16

    move-object/from16 v0, p0

    iget-wide v12, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    mul-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    move-wide/from16 v18, v0

    mul-double v14, v14, v18

    sub-double/2addr v12, v14

    div-double v12, v12, v16

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    move-wide/from16 v18, v0

    mul-double v14, v14, v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    sub-double v14, v14, v18

    div-double v14, v14, v16

    invoke-direct/range {v3 .. v15}, Lorg/icepdf/index/java/awt/geom/AffineTransform;-><init>(DDDDDD)V

    return-object v3
.end method

.method public createTransformedShape(Lorg/icepdf/index/java/awt/Shape;)Lorg/icepdf/index/java/awt/Shape;
    .locals 3
    .param p1, "src"    # Lorg/icepdf/index/java/awt/Shape;

    .prologue
    .line 560
    if-nez p1, :cond_0

    .line 561
    const/4 v0, 0x0

    .line 569
    .end local p1    # "src":Lorg/icepdf/index/java/awt/Shape;
    :goto_0
    return-object v0

    .line 563
    .restart local p1    # "src":Lorg/icepdf/index/java/awt/Shape;
    :cond_0
    instance-of v2, p1, Lorg/icepdf/index/java/awt/geom/GeneralPath;

    if-eqz v2, :cond_1

    .line 564
    check-cast p1, Lorg/icepdf/index/java/awt/geom/GeneralPath;

    .end local p1    # "src":Lorg/icepdf/index/java/awt/Shape;
    invoke-virtual {p1, p0}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->createTransformedShape(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/Shape;

    move-result-object v0

    goto :goto_0

    .line 566
    .restart local p1    # "src":Lorg/icepdf/index/java/awt/Shape;
    :cond_1
    invoke-interface {p1, p0}, Lorg/icepdf/index/java/awt/Shape;->getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;

    move-result-object v1

    .line 567
    .local v1, "path":Lorg/icepdf/index/java/awt/geom/PathIterator;
    new-instance v0, Lorg/icepdf/index/java/awt/geom/GeneralPath;

    invoke-interface {v1}, Lorg/icepdf/index/java/awt/geom/PathIterator;->getWindingRule()I

    move-result v2

    invoke-direct {v0, v2}, Lorg/icepdf/index/java/awt/geom/GeneralPath;-><init>(I)V

    .line 568
    .local v0, "dst":Lorg/icepdf/index/java/awt/geom/GeneralPath;
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->append(Lorg/icepdf/index/java/awt/geom/PathIterator;Z)V

    goto :goto_0
.end method

.method public deltaTransform(Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;)Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 10
    .param p1, "src"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "dst"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 496
    if-nez p2, :cond_0

    .line 497
    instance-of v4, p1, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    if-eqz v4, :cond_1

    .line 498
    new-instance p2, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    .end local p2    # "dst":Lorg/icepdf/index/java/awt/geom/Point2D;
    invoke-direct {p2}, Lorg/icepdf/index/java/awt/geom/Point2D$Double;-><init>()V

    .line 504
    .restart local p2    # "dst":Lorg/icepdf/index/java/awt/geom/Point2D;
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    .line 505
    .local v0, "x":D
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    .line 507
    .local v2, "y":D
    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v4, v0

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v6, v2

    add-double/2addr v4, v6

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v6, v0

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v8, v2

    add-double/2addr v6, v8

    invoke-virtual {p2, v4, v5, v6, v7}, Lorg/icepdf/index/java/awt/geom/Point2D;->setLocation(DD)V

    .line 508
    return-object p2

    .line 500
    .end local v0    # "x":D
    .end local v2    # "y":D
    :cond_1
    new-instance p2, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    .end local p2    # "dst":Lorg/icepdf/index/java/awt/geom/Point2D;
    invoke-direct {p2}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>()V

    .restart local p2    # "dst":Lorg/icepdf/index/java/awt/geom/Point2D;
    goto :goto_0
.end method

.method public deltaTransform([DI[DII)V
    .locals 10
    .param p1, "src"    # [D
    .param p2, "srcOff"    # I
    .param p3, "dst"    # [D
    .param p4, "dstOff"    # I
    .param p5, "length"    # I

    .prologue
    .line 512
    move v0, p4

    .end local p4    # "dstOff":I
    .local v0, "dstOff":I
    move v1, p2

    .end local p2    # "srcOff":I
    .local v1, "srcOff":I
    :goto_0
    add-int/lit8 p5, p5, -0x1

    if-ltz p5, :cond_0

    .line 513
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "srcOff":I
    .restart local p2    # "srcOff":I
    aget-wide v2, p1, v1

    .line 514
    .local v2, "x":D
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "srcOff":I
    .restart local v1    # "srcOff":I
    aget-wide v4, p1, p2

    .line 515
    .local v4, "y":D
    add-int/lit8 p4, v0, 0x1

    .end local v0    # "dstOff":I
    .restart local p4    # "dstOff":I
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v6, v2

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v8, v4

    add-double/2addr v6, v8

    aput-wide v6, p3, v0

    .line 516
    add-int/lit8 v0, p4, 0x1

    .end local p4    # "dstOff":I
    .restart local v0    # "dstOff":I
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v6, v2

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v8, v4

    add-double/2addr v6, v8

    aput-wide v6, p3, p4

    goto :goto_0

    .line 518
    .end local v2    # "x":D
    .end local v4    # "y":D
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 603
    if-ne p1, p0, :cond_1

    .line 613
    :cond_0
    :goto_0
    return v1

    .line 606
    :cond_1
    instance-of v3, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 607
    check-cast v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .line 608
    .local v0, "t":Lorg/icepdf/index/java/awt/geom/AffineTransform;
    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    iget-wide v6, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    iget-wide v6, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    iget-wide v6, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    iget-wide v6, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    iget-wide v6, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    iget-wide v6, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    cmpl-double v3, v4, v6

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "t":Lorg/icepdf/index/java/awt/geom/AffineTransform;
    :cond_3
    move v1, v2

    .line 613
    goto :goto_0
.end method

.method public getAffineTransform([D)V
    .locals 5
    .param p1, "AffineTransform"    # [D

    .prologue
    const/4 v4, 0x4

    .line 224
    const/4 v0, 0x0

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    aput-wide v2, p1, v0

    .line 225
    const/4 v0, 0x1

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    aput-wide v2, p1, v0

    .line 226
    const/4 v0, 0x2

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    aput-wide v2, p1, v0

    .line 227
    const/4 v0, 0x3

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    aput-wide v2, p1, v0

    .line 228
    array-length v0, p1

    if-le v0, v4, :cond_0

    .line 229
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    aput-wide v0, p1, v4

    .line 230
    const/4 v0, 0x5

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    aput-wide v2, p1, v0

    .line 232
    :cond_0
    return-void
.end method

.method public getDeterminant()D
    .locals 6

    .prologue
    .line 235
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method public getScaleX()D
    .locals 2

    .prologue
    .line 196
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    return-wide v0
.end method

.method public getScaleY()D
    .locals 2

    .prologue
    .line 200
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    return-wide v0
.end method

.method public getShearX()D
    .locals 2

    .prologue
    .line 204
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    return-wide v0
.end method

.method public getShearY()D
    .locals 2

    .prologue
    .line 208
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    return-wide v0
.end method

.method public getTranslateX()D
    .locals 2

    .prologue
    .line 212
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    return-wide v0
.end method

.method public getTranslateY()D
    .locals 2

    .prologue
    .line 216
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    return-wide v0
.end method

.method public getType()I
    .locals 12

    .prologue
    .line 151
    iget v5, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    .line 152
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 192
    :cond_0
    :goto_0
    return v4

    .line 155
    :cond_1
    const/4 v4, 0x0

    .line 157
    .local v4, "type":I
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-eqz v5, :cond_2

    .line 158
    or-int/lit8 v4, v4, 0x20

    .line 159
    goto :goto_0

    .line 162
    :cond_2
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-nez v5, :cond_3

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-eqz v5, :cond_9

    .line 163
    :cond_3
    or-int/lit8 v4, v4, 0x1

    .line 170
    :cond_4
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    const-wide/16 v8, 0x0

    cmpg-double v5, v6, v8

    if-gez v5, :cond_5

    .line 171
    or-int/lit8 v4, v4, 0x40

    .line 174
    :cond_5
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v8, v10

    add-double v0, v6, v8

    .line 175
    .local v0, "dx":D
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v8, v10

    add-double v2, v6, v8

    .line 176
    .local v2, "dy":D
    cmpl-double v5, v0, v2

    if-eqz v5, :cond_a

    .line 177
    or-int/lit8 v4, v4, 0x4

    .line 183
    :cond_6
    :goto_1
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-nez v5, :cond_7

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-eqz v5, :cond_8

    :cond_7
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-nez v5, :cond_b

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-nez v5, :cond_b

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    const-wide/16 v8, 0x0

    cmpg-double v5, v6, v8

    if-ltz v5, :cond_8

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    const-wide/16 v8, 0x0

    cmpg-double v5, v6, v8

    if-gez v5, :cond_b

    .line 186
    :cond_8
    or-int/lit8 v4, v4, 0x8

    goto/16 :goto_0

    .line 165
    .end local v0    # "dx":D
    .end local v2    # "dy":D
    :cond_9
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v5, v6, v8

    if-nez v5, :cond_4

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v5, v6, v8

    if-nez v5, :cond_4

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-nez v5, :cond_4

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-nez v5, :cond_4

    .line 166
    const/4 v4, 0x0

    .line 167
    goto/16 :goto_0

    .line 179
    .restart local v0    # "dx":D
    .restart local v2    # "dy":D
    :cond_a
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v5, v0, v6

    if-eqz v5, :cond_6

    .line 180
    or-int/lit8 v4, v4, 0x2

    goto :goto_1

    .line 188
    :cond_b
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-nez v5, :cond_c

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-eqz v5, :cond_0

    .line 189
    :cond_c
    or-int/lit8 v4, v4, 0x10

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 591
    new-instance v0, Lorg/apache/harmony/misc/HashCode;

    invoke-direct {v0}, Lorg/apache/harmony/misc/HashCode;-><init>()V

    .line 592
    .local v0, "hash":Lorg/apache/harmony/misc/HashCode;
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    invoke-virtual {v0, v2, v3}, Lorg/apache/harmony/misc/HashCode;->append(D)Lorg/apache/harmony/misc/HashCode;

    .line 593
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    invoke-virtual {v0, v2, v3}, Lorg/apache/harmony/misc/HashCode;->append(D)Lorg/apache/harmony/misc/HashCode;

    .line 594
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    invoke-virtual {v0, v2, v3}, Lorg/apache/harmony/misc/HashCode;->append(D)Lorg/apache/harmony/misc/HashCode;

    .line 595
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    invoke-virtual {v0, v2, v3}, Lorg/apache/harmony/misc/HashCode;->append(D)Lorg/apache/harmony/misc/HashCode;

    .line 596
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    invoke-virtual {v0, v2, v3}, Lorg/apache/harmony/misc/HashCode;->append(D)Lorg/apache/harmony/misc/HashCode;

    .line 597
    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    invoke-virtual {v0, v2, v3}, Lorg/apache/harmony/misc/HashCode;->append(D)Lorg/apache/harmony/misc/HashCode;

    .line 598
    invoke-virtual {v0}, Lorg/apache/harmony/misc/HashCode;->hashCode()I

    move-result v1

    return v1
.end method

.method public inverseTransform(Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;)Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 12
    .param p1, "src"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "dst"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/index/java/awt/geom/NoninvertibleTransformException;
        }
    .end annotation

    .prologue
    .line 521
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->getDeterminant()D

    move-result-wide v0

    .line 522
    .local v0, "det":D
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v8, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v6, v6, v8

    if-gez v6, :cond_0

    .line 524
    new-instance v6, Lorg/icepdf/index/java/awt/geom/NoninvertibleTransformException;

    const-string/jumbo v7, "awt.204"

    invoke-static {v7}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/icepdf/index/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 527
    :cond_0
    if-nez p2, :cond_1

    .line 528
    instance-of v6, p1, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    if-eqz v6, :cond_2

    .line 529
    new-instance p2, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    .end local p2    # "dst":Lorg/icepdf/index/java/awt/geom/Point2D;
    invoke-direct {p2}, Lorg/icepdf/index/java/awt/geom/Point2D$Double;-><init>()V

    .line 535
    .restart local p2    # "dst":Lorg/icepdf/index/java/awt/geom/Point2D;
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v6

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    sub-double v2, v6, v8

    .line 536
    .local v2, "x":D
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v6

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    sub-double v4, v6, v8

    .line 538
    .local v4, "y":D
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v6, v2

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v8, v4

    sub-double/2addr v6, v8

    div-double/2addr v6, v0

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v8, v4

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v10, v2

    sub-double/2addr v8, v10

    div-double/2addr v8, v0

    invoke-virtual {p2, v6, v7, v8, v9}, Lorg/icepdf/index/java/awt/geom/Point2D;->setLocation(DD)V

    .line 539
    return-object p2

    .line 531
    .end local v2    # "x":D
    .end local v4    # "y":D
    :cond_2
    new-instance p2, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    .end local p2    # "dst":Lorg/icepdf/index/java/awt/geom/Point2D;
    invoke-direct {p2}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>()V

    .restart local p2    # "dst":Lorg/icepdf/index/java/awt/geom/Point2D;
    goto :goto_0
.end method

.method public inverseTransform([DI[DII)V
    .locals 12
    .param p1, "src"    # [D
    .param p2, "srcOff"    # I
    .param p3, "dst"    # [D
    .param p4, "dstOff"    # I
    .param p5, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/index/java/awt/geom/NoninvertibleTransformException;
        }
    .end annotation

    .prologue
    .line 545
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->getDeterminant()D

    move-result-wide v0

    .line 546
    .local v0, "det":D
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v8, v8, v10

    if-gez v8, :cond_1

    .line 548
    new-instance v8, Lorg/icepdf/index/java/awt/geom/NoninvertibleTransformException;

    const-string/jumbo v9, "awt.204"

    invoke-static {v9}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/icepdf/index/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 551
    .end local p2    # "srcOff":I
    .end local p4    # "dstOff":I
    .local v2, "dstOff":I
    .local v3, "srcOff":I
    :goto_0
    add-int/lit8 p5, p5, -0x1

    if-ltz p5, :cond_0

    .line 552
    add-int/lit8 p2, v3, 0x1

    .end local v3    # "srcOff":I
    .restart local p2    # "srcOff":I
    aget-wide v8, p1, v3

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    sub-double v4, v8, v10

    .line 553
    .local v4, "x":D
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "srcOff":I
    .restart local v3    # "srcOff":I
    aget-wide v8, p1, p2

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    sub-double v6, v8, v10

    .line 554
    .local v6, "y":D
    add-int/lit8 p4, v2, 0x1

    .end local v2    # "dstOff":I
    .restart local p4    # "dstOff":I
    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v8, v4

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v10, v6

    sub-double/2addr v8, v10

    div-double/2addr v8, v0

    aput-wide v8, p3, v2

    .line 555
    add-int/lit8 v2, p4, 0x1

    .end local p4    # "dstOff":I
    .restart local v2    # "dstOff":I
    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v10, v4

    sub-double/2addr v8, v10

    div-double/2addr v8, v0

    aput-wide v8, p3, p4

    goto :goto_0

    .line 557
    .end local v4    # "x":D
    .end local v6    # "y":D
    :cond_0
    return-void

    .end local v2    # "dstOff":I
    .end local v3    # "srcOff":I
    .restart local p2    # "srcOff":I
    .restart local p4    # "dstOff":I
    :cond_1
    move/from16 v2, p4

    .end local p4    # "dstOff":I
    .restart local v2    # "dstOff":I
    move v3, p2

    .end local p2    # "srcOff":I
    .restart local v3    # "srcOff":I
    goto :goto_0
.end method

.method public isIdentity()Z
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->getType()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public mapPoints([F)V
    .locals 0
    .param p1, "pts"    # [F

    .prologue
    .line 647
    return-void
.end method

.method multiply(Lorg/icepdf/index/java/awt/geom/AffineTransform;Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .locals 20
    .param p1, "t1"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .param p2, "t2"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 376
    new-instance v3, Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    move-object/from16 v0, p2

    iget-wide v6, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v4, v6

    move-object/from16 v0, p1

    iget-wide v6, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    move-object/from16 v0, p2

    iget-wide v8, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    move-object/from16 v0, p1

    iget-wide v6, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    move-object/from16 v0, p2

    iget-wide v8, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v6, v8

    move-object/from16 v0, p1

    iget-wide v8, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    move-object/from16 v0, p2

    iget-wide v10, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    move-object/from16 v0, p1

    iget-wide v8, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    move-object/from16 v0, p2

    iget-wide v10, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v8, v10

    move-object/from16 v0, p1

    iget-wide v10, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    move-object/from16 v0, p1

    iget-wide v10, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v10, v12

    move-object/from16 v0, p1

    iget-wide v12, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    move-object/from16 v0, p2

    iget-wide v14, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    move-object/from16 v0, p1

    iget-wide v12, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    move-object/from16 v0, p2

    iget-wide v14, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v12, v14

    move-object/from16 v0, p1

    iget-wide v14, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    move-object/from16 v0, p2

    iget-wide v0, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    move-object/from16 v0, p2

    iget-wide v14, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v12, v14

    move-object/from16 v0, p1

    iget-wide v14, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    move-object/from16 v0, p2

    iget-wide v0, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p1

    iget-wide v0, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    add-double v14, v14, v16

    move-object/from16 v0, p2

    iget-wide v0, v0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    invoke-direct/range {v3 .. v15}, Lorg/icepdf/index/java/awt/geom/AffineTransform;-><init>(DDDDDD)V

    return-object v3
.end method

.method public preConcatenate(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V
    .locals 1
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 390
    invoke-virtual {p0, p0, p1}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->multiply(Lorg/icepdf/index/java/awt/geom/AffineTransform;Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->setTransform(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    .line 391
    return-void
.end method

.method public rotate(D)V
    .locals 1
    .param p1, "angle"    # D

    .prologue
    .line 362
    invoke-static {p1, p2}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->getRotateInstance(D)Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->concatenate(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    .line 363
    return-void
.end method

.method public rotate(DDD)V
    .locals 1
    .param p1, "angle"    # D
    .param p3, "px"    # D
    .param p5, "py"    # D

    .prologue
    .line 366
    invoke-static/range {p1 .. p6}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->getRotateInstance(DDD)Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->concatenate(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    .line 367
    return-void
.end method

.method public scale(DD)V
    .locals 1
    .param p1, "scx"    # D
    .param p3, "scy"    # D

    .prologue
    .line 354
    invoke-static {p1, p2, p3, p4}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->getScaleInstance(DD)Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->concatenate(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    .line 355
    return-void
.end method

.method public setToIdentity()V
    .locals 2

    .prologue
    .line 254
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 255
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    .line 256
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    .line 257
    return-void
.end method

.method public setToRotation(D)V
    .locals 9
    .param p1, "angle"    # D

    .prologue
    .line 295
    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 296
    .local v2, "sin":D
    invoke-static {p1, p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 297
    .local v0, "cos":D
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide v6, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v4, v4, v6

    if-gez v4, :cond_2

    .line 298
    const-wide/16 v0, 0x0

    .line 299
    const-wide/16 v4, 0x0

    cmpl-double v4, v2, v4

    if-lez v4, :cond_1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 305
    :cond_0
    :goto_0
    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    .line 306
    neg-double v4, v2

    iput-wide v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    .line 307
    iput-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    .line 308
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    iput-wide v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    .line 309
    const/4 v4, -0x1

    iput v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 310
    return-void

    .line 299
    :cond_1
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    goto :goto_0

    .line 301
    :cond_2
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide v6, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v4, v4, v6

    if-gez v4, :cond_0

    .line 302
    const-wide/16 v2, 0x0

    .line 303
    const-wide/16 v4, 0x0

    cmpl-double v4, v0, v4

    if-lez v4, :cond_3

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_1
    goto :goto_0

    :cond_3
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    goto :goto_1
.end method

.method public setToRotation(DDD)V
    .locals 7
    .param p1, "angle"    # D
    .param p3, "px"    # D
    .param p5, "py"    # D

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 313
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->setToRotation(D)V

    .line 314
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    sub-double v0, v4, v0

    mul-double/2addr v0, p3

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v2, p5

    add-double/2addr v0, v2

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    .line 315
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    sub-double v0, v4, v0

    mul-double/2addr v0, p5

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v2, p3

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    .line 316
    const/4 v0, -0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 317
    return-void
.end method

.method public setToScale(DD)V
    .locals 5
    .param p1, "scx"    # D
    .param p3, "scy"    # D

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 272
    iput-wide p1, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    .line 273
    iput-wide p3, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    .line 274
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    .line 275
    cmpl-double v0, p1, v2

    if-nez v0, :cond_0

    cmpl-double v0, p3, v2

    if-eqz v0, :cond_1

    .line 276
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 280
    :goto_0
    return-void

    .line 278
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    goto :goto_0
.end method

.method public setToShear(DD)V
    .locals 5
    .param p1, "shx"    # D
    .param p3, "shy"    # D

    .prologue
    const-wide/16 v2, 0x0

    .line 283
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    .line 284
    iput-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    iput-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    .line 285
    iput-wide p1, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    .line 286
    iput-wide p3, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    .line 287
    cmpl-double v0, p1, v2

    if-nez v0, :cond_0

    cmpl-double v0, p3, v2

    if-eqz v0, :cond_1

    .line 288
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 292
    :goto_0
    return-void

    .line 290
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    goto :goto_0
.end method

.method public setToTranslation(DD)V
    .locals 5
    .param p1, "mx"    # D
    .param p3, "my"    # D

    .prologue
    const-wide/16 v2, 0x0

    .line 260
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    .line 261
    iput-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    iput-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    .line 262
    iput-wide p1, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    .line 263
    iput-wide p3, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    .line 264
    cmpl-double v0, p1, v2

    if-nez v0, :cond_0

    cmpl-double v0, p3, v2

    if-nez v0, :cond_0

    .line 265
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 269
    :goto_0
    return-void

    .line 267
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    goto :goto_0
.end method

.method public setTransform(DDDDDD)V
    .locals 1
    .param p1, "m00"    # D
    .param p3, "m10"    # D
    .param p5, "m01"    # D
    .param p7, "m11"    # D
    .param p9, "m02"    # D
    .param p11, "m12"    # D

    .prologue
    .line 239
    const/4 v0, -0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 240
    iput-wide p1, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    .line 241
    iput-wide p3, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    .line 242
    iput-wide p5, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    .line 243
    iput-wide p7, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    .line 244
    iput-wide p9, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    .line 245
    iput-wide p11, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    .line 246
    return-void
.end method

.method public setTransform(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V
    .locals 14
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 249
    iget v0, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->type:I

    .line 250
    iget-wide v2, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    iget-wide v4, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    iget-wide v6, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    iget-wide v8, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    iget-wide v10, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    iget-wide v12, p1, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    move-object v1, p0

    invoke-virtual/range {v1 .. v13}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->setTransform(DDDDDD)V

    .line 251
    return-void
.end method

.method public setValues([F)V
    .locals 0
    .param p1, "fs"    # [F

    .prologue
    .line 642
    return-void
.end method

.method public shear(DD)V
    .locals 1
    .param p1, "shx"    # D
    .param p3, "shy"    # D

    .prologue
    .line 358
    invoke-static {p1, p2, p3, p4}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->getShearInstance(DD)Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->concatenate(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    .line 359
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 574
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "[["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "], ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transform(Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;)Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 10
    .param p1, "src"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "dst"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 410
    if-nez p2, :cond_0

    .line 411
    instance-of v4, p1, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    if-eqz v4, :cond_1

    .line 412
    new-instance p2, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    .end local p2    # "dst":Lorg/icepdf/index/java/awt/geom/Point2D;
    invoke-direct {p2}, Lorg/icepdf/index/java/awt/geom/Point2D$Double;-><init>()V

    .line 418
    .restart local p2    # "dst":Lorg/icepdf/index/java/awt/geom/Point2D;
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    .line 419
    .local v0, "x":D
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    .line 421
    .local v2, "y":D
    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v4, v0

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v6, v2

    add-double/2addr v4, v6

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v4, v6

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v6, v0

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v8, v2

    add-double/2addr v6, v8

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v6, v8

    invoke-virtual {p2, v4, v5, v6, v7}, Lorg/icepdf/index/java/awt/geom/Point2D;->setLocation(DD)V

    .line 422
    return-object p2

    .line 414
    .end local v0    # "x":D
    .end local v2    # "y":D
    :cond_1
    new-instance p2, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    .end local p2    # "dst":Lorg/icepdf/index/java/awt/geom/Point2D;
    invoke-direct {p2}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>()V

    .restart local p2    # "dst":Lorg/icepdf/index/java/awt/geom/Point2D;
    goto :goto_0
.end method

.method public transform([DI[DII)V
    .locals 10
    .param p1, "src"    # [D
    .param p2, "srcOff"    # I
    .param p3, "dst"    # [D
    .param p4, "dstOff"    # I
    .param p5, "length"    # I

    .prologue
    .line 444
    const/4 v0, 0x2

    .line 445
    .local v0, "step":I
    if-ne p1, p3, :cond_0

    if-ge p2, p4, :cond_0

    mul-int/lit8 v1, p5, 0x2

    add-int/2addr v1, p2

    if-ge p4, v1, :cond_0

    .line 446
    mul-int/lit8 v1, p5, 0x2

    add-int/2addr v1, p2

    add-int/lit8 p2, v1, -0x2

    .line 447
    mul-int/lit8 v1, p5, 0x2

    add-int/2addr v1, p4

    add-int/lit8 p4, v1, -0x2

    .line 448
    const/4 v0, -0x2

    .line 450
    :cond_0
    :goto_0
    add-int/lit8 p5, p5, -0x1

    if-ltz p5, :cond_1

    .line 451
    add-int/lit8 v1, p2, 0x0

    aget-wide v2, p1, v1

    .line 452
    .local v2, "x":D
    add-int/lit8 v1, p2, 0x1

    aget-wide v4, p1, v1

    .line 453
    .local v4, "y":D
    add-int/lit8 v1, p4, 0x0

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v6, v2

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v8, v4

    add-double/2addr v6, v8

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v6, v8

    aput-wide v6, p3, v1

    .line 454
    add-int/lit8 v1, p4, 0x1

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v6, v2

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v8, v4

    add-double/2addr v6, v8

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v6, v8

    aput-wide v6, p3, v1

    .line 455
    add-int/2addr p2, v0

    .line 456
    add-int/2addr p4, v0

    .line 457
    goto :goto_0

    .line 458
    .end local v2    # "x":D
    .end local v4    # "y":D
    :cond_1
    return-void
.end method

.method public transform([DI[FII)V
    .locals 10
    .param p1, "src"    # [D
    .param p2, "srcOff"    # I
    .param p3, "dst"    # [F
    .param p4, "dstOff"    # I
    .param p5, "length"    # I

    .prologue
    .line 487
    move v0, p4

    .end local p4    # "dstOff":I
    .local v0, "dstOff":I
    move v1, p2

    .end local p2    # "srcOff":I
    .local v1, "srcOff":I
    :goto_0
    add-int/lit8 p5, p5, -0x1

    if-ltz p5, :cond_0

    .line 488
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "srcOff":I
    .restart local p2    # "srcOff":I
    aget-wide v2, p1, v1

    .line 489
    .local v2, "x":D
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "srcOff":I
    .restart local v1    # "srcOff":I
    aget-wide v4, p1, p2

    .line 490
    .local v4, "y":D
    add-int/lit8 p4, v0, 0x1

    .end local v0    # "dstOff":I
    .restart local p4    # "dstOff":I
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v6, v2

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v8, v4

    add-double/2addr v6, v8

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v6, v8

    double-to-float v6, v6

    aput v6, p3, v0

    .line 491
    add-int/lit8 v0, p4, 0x1

    .end local p4    # "dstOff":I
    .restart local v0    # "dstOff":I
    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v6, v2

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v8, v4

    add-double/2addr v6, v8

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v6, v8

    double-to-float v6, v6

    aput v6, p3, p4

    goto :goto_0

    .line 493
    .end local v2    # "x":D
    .end local v4    # "y":D
    :cond_0
    return-void
.end method

.method public transform([FI[DII)V
    .locals 10
    .param p1, "src"    # [F
    .param p2, "srcOff"    # I
    .param p3, "dst"    # [D
    .param p4, "dstOff"    # I
    .param p5, "length"    # I

    .prologue
    .line 478
    move v0, p4

    .end local p4    # "dstOff":I
    .local v0, "dstOff":I
    move v1, p2

    .end local p2    # "srcOff":I
    .local v1, "srcOff":I
    :goto_0
    add-int/lit8 p5, p5, -0x1

    if-ltz p5, :cond_0

    .line 479
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "srcOff":I
    .restart local p2    # "srcOff":I
    aget v2, p1, v1

    .line 480
    .local v2, "x":F
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "srcOff":I
    .restart local v1    # "srcOff":I
    aget v3, p1, p2

    .line 481
    .local v3, "y":F
    add-int/lit8 p4, v0, 0x1

    .end local v0    # "dstOff":I
    .restart local p4    # "dstOff":I
    float-to-double v4, v2

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v4, v6

    float-to-double v6, v3

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v4, v6

    aput-wide v4, p3, v0

    .line 482
    add-int/lit8 v0, p4, 0x1

    .end local p4    # "dstOff":I
    .restart local v0    # "dstOff":I
    float-to-double v4, v2

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v4, v6

    float-to-double v6, v3

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v4, v6

    aput-wide v4, p3, p4

    goto :goto_0

    .line 484
    .end local v2    # "x":F
    .end local v3    # "y":F
    :cond_0
    return-void
.end method

.method public transform([FI[FII)V
    .locals 10
    .param p1, "src"    # [F
    .param p2, "srcOff"    # I
    .param p3, "dst"    # [F
    .param p4, "dstOff"    # I
    .param p5, "length"    # I

    .prologue
    .line 461
    const/4 v0, 0x2

    .line 462
    .local v0, "step":I
    if-ne p1, p3, :cond_0

    if-ge p2, p4, :cond_0

    mul-int/lit8 v3, p5, 0x2

    add-int/2addr v3, p2

    if-ge p4, v3, :cond_0

    .line 463
    mul-int/lit8 v3, p5, 0x2

    add-int/2addr v3, p2

    add-int/lit8 p2, v3, -0x2

    .line 464
    mul-int/lit8 v3, p5, 0x2

    add-int/2addr v3, p4

    add-int/lit8 p4, v3, -0x2

    .line 465
    const/4 v0, -0x2

    .line 467
    :cond_0
    :goto_0
    add-int/lit8 p5, p5, -0x1

    if-ltz p5, :cond_1

    .line 468
    add-int/lit8 v3, p2, 0x0

    aget v1, p1, v3

    .line 469
    .local v1, "x":F
    add-int/lit8 v3, p2, 0x1

    aget v2, p1, v3

    .line 470
    .local v2, "y":F
    add-int/lit8 v3, p4, 0x0

    float-to-double v4, v1

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v4, v6

    float-to-double v6, v2

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v4, v6

    double-to-float v4, v4

    aput v4, p3, v3

    .line 471
    add-int/lit8 v3, p4, 0x1

    float-to-double v4, v1

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v4, v6

    float-to-double v6, v2

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v4, v6

    double-to-float v4, v4

    aput v4, p3, v3

    .line 472
    add-int/2addr p2, v0

    .line 473
    add-int/2addr p4, v0

    .line 474
    goto :goto_0

    .line 475
    .end local v1    # "x":F
    .end local v2    # "y":F
    :cond_1
    return-void
.end method

.method public transform([Lorg/icepdf/index/java/awt/geom/Point2D;I[Lorg/icepdf/index/java/awt/geom/Point2D;II)V
    .locals 14
    .param p1, "src"    # [Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "srcOff"    # I
    .param p3, "dst"    # [Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p4, "dstOff"    # I
    .param p5, "length"    # I

    .prologue
    .line 426
    move/from16 v0, p4

    .end local p4    # "dstOff":I
    .local v0, "dstOff":I
    move/from16 v2, p2

    .end local p2    # "srcOff":I
    .local v2, "srcOff":I
    :goto_0
    add-int/lit8 p5, p5, -0x1

    if-ltz p5, :cond_2

    .line 427
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "srcOff":I
    .restart local p2    # "srcOff":I
    aget-object v3, p1, v2

    .line 428
    .local v3, "srcPoint":Lorg/icepdf/index/java/awt/geom/Point2D;
    invoke-virtual {v3}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v4

    .line 429
    .local v4, "x":D
    invoke-virtual {v3}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v6

    .line 430
    .local v6, "y":D
    aget-object v1, p3, v0

    .line 431
    .local v1, "dstPoint":Lorg/icepdf/index/java/awt/geom/Point2D;
    if-nez v1, :cond_0

    .line 432
    instance-of v8, v3, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    if-eqz v8, :cond_1

    .line 433
    new-instance v1, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    .end local v1    # "dstPoint":Lorg/icepdf/index/java/awt/geom/Point2D;
    invoke-direct {v1}, Lorg/icepdf/index/java/awt/geom/Point2D$Double;-><init>()V

    .line 438
    .restart local v1    # "dstPoint":Lorg/icepdf/index/java/awt/geom/Point2D;
    :cond_0
    :goto_1
    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m00:D

    mul-double/2addr v8, v4

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m01:D

    mul-double/2addr v10, v6

    add-double/2addr v8, v10

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v8, v10

    iget-wide v10, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m10:D

    mul-double/2addr v10, v4

    iget-wide v12, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m11:D

    mul-double/2addr v12, v6

    add-double/2addr v10, v12

    iget-wide v12, p0, Lorg/icepdf/index/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v10, v12

    invoke-virtual {v1, v8, v9, v10, v11}, Lorg/icepdf/index/java/awt/geom/Point2D;->setLocation(DD)V

    .line 439
    add-int/lit8 p4, v0, 0x1

    .end local v0    # "dstOff":I
    .restart local p4    # "dstOff":I
    aput-object v1, p3, v0

    move/from16 v0, p4

    .end local p4    # "dstOff":I
    .restart local v0    # "dstOff":I
    move/from16 v2, p2

    .line 440
    .end local p2    # "srcOff":I
    .restart local v2    # "srcOff":I
    goto :goto_0

    .line 435
    .end local v2    # "srcOff":I
    .restart local p2    # "srcOff":I
    :cond_1
    new-instance v1, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    .end local v1    # "dstPoint":Lorg/icepdf/index/java/awt/geom/Point2D;
    invoke-direct {v1}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>()V

    .restart local v1    # "dstPoint":Lorg/icepdf/index/java/awt/geom/Point2D;
    goto :goto_1

    .line 441
    .end local v1    # "dstPoint":Lorg/icepdf/index/java/awt/geom/Point2D;
    .end local v3    # "srcPoint":Lorg/icepdf/index/java/awt/geom/Point2D;
    .end local v4    # "x":D
    .end local v6    # "y":D
    .end local p2    # "srcOff":I
    .restart local v2    # "srcOff":I
    :cond_2
    return-void
.end method

.method public translate(DD)V
    .locals 1
    .param p1, "mx"    # D
    .param p3, "my"    # D

    .prologue
    .line 350
    invoke-static {p1, p2, p3, p4}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->getTranslateInstance(DD)Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->concatenate(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    .line 351
    return-void
.end method
