.class public final Lorg/icepdf/index/java/awt/geom/GeneralPath;
.super Ljava/lang/Object;
.source "GeneralPath.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/icepdf/index/java/awt/Shape;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/index/java/awt/geom/GeneralPath$Iterator;
    }
.end annotation


# static fields
.field private static final BUFFER_CAPACITY:I = 0xa

.field private static final BUFFER_SIZE:I = 0xa

.field public static final WIND_EVEN_ODD:I = 0x0

.field public static final WIND_NON_ZERO:I = 0x1

.field static pointShift:[I


# instance fields
.field pointSize:I

.field points:[F

.field rule:I

.field typeSize:I

.field types:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointShift:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x2
        0x4
        0x6
        0x0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 170
    const/4 v0, 0x1

    const/16 v1, 0xa

    invoke-direct {p0, v0, v1}, Lorg/icepdf/index/java/awt/geom/GeneralPath;-><init>(II)V

    .line 171
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "rule"    # I

    .prologue
    .line 174
    const/16 v0, 0xa

    invoke-direct {p0, p1, v0}, Lorg/icepdf/index/java/awt/geom/GeneralPath;-><init>(II)V

    .line 175
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "rule"    # I
    .param p2, "initialCapacity"    # I

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    invoke-virtual {p0, p1}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->setWindingRule(I)V

    .line 179
    new-array v0, p2, [B

    iput-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    .line 180
    mul-int/lit8 v0, p2, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    .line 181
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/java/awt/Shape;)V
    .locals 3
    .param p1, "shape"    # Lorg/icepdf/index/java/awt/Shape;

    .prologue
    .line 184
    const/4 v1, 0x1

    const/16 v2, 0xa

    invoke-direct {p0, v1, v2}, Lorg/icepdf/index/java/awt/geom/GeneralPath;-><init>(II)V

    .line 185
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Lorg/icepdf/index/java/awt/Shape;->getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;

    move-result-object v0

    .line 186
    .local v0, "p":Lorg/icepdf/index/java/awt/geom/PathIterator;
    invoke-interface {v0}, Lorg/icepdf/index/java/awt/geom/PathIterator;->getWindingRule()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->setWindingRule(I)V

    .line 187
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->append(Lorg/icepdf/index/java/awt/geom/PathIterator;Z)V

    .line 188
    return-void
.end method


# virtual methods
.method public append(Lorg/icepdf/index/java/awt/Shape;Z)V
    .locals 2
    .param p1, "shape"    # Lorg/icepdf/index/java/awt/Shape;
    .param p2, "connect"    # Z

    .prologue
    .line 270
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Lorg/icepdf/index/java/awt/Shape;->getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;

    move-result-object v0

    .line 271
    .local v0, "p":Lorg/icepdf/index/java/awt/geom/PathIterator;
    invoke-virtual {p0, v0, p2}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->append(Lorg/icepdf/index/java/awt/geom/PathIterator;Z)V

    .line 272
    return-void
.end method

.method public append(Lorg/icepdf/index/java/awt/geom/PathIterator;Z)V
    .locals 13
    .param p1, "path"    # Lorg/icepdf/index/java/awt/geom/PathIterator;
    .param p2, "connect"    # Z

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 275
    :goto_0
    invoke-interface {p1}, Lorg/icepdf/index/java/awt/geom/PathIterator;->isDone()Z

    move-result v0

    if-nez v0, :cond_4

    .line 276
    const/4 v0, 0x6

    new-array v7, v0, [F

    .line 277
    .local v7, "coords":[F
    invoke-interface {p1, v7}, Lorg/icepdf/index/java/awt/geom/PathIterator;->currentSegment([F)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 303
    :cond_0
    :goto_1
    invoke-interface {p1}, Lorg/icepdf/index/java/awt/geom/PathIterator;->next()V

    .line 304
    const/4 p2, 0x0

    .line 305
    goto :goto_0

    .line 279
    :pswitch_0
    if-eqz p2, :cond_1

    iget v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    if-nez v0, :cond_2

    .line 280
    :cond_1
    aget v0, v7, v8

    aget v1, v7, v9

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->moveTo(FF)V

    goto :goto_1

    .line 283
    :cond_2
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    add-int/lit8 v1, v1, -0x1

    aget-byte v0, v0, v1

    if-eq v0, v12, :cond_3

    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v1, v1, -0x2

    aget v0, v0, v1

    aget v1, v7, v8

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    aget v1, v7, v9

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 291
    :cond_3
    :pswitch_1
    aget v0, v7, v8

    aget v1, v7, v9

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->lineTo(FF)V

    goto :goto_1

    .line 294
    :pswitch_2
    aget v0, v7, v8

    aget v1, v7, v9

    aget v2, v7, v10

    aget v3, v7, v11

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->quadTo(FFFF)V

    goto :goto_1

    .line 297
    :pswitch_3
    aget v1, v7, v8

    aget v2, v7, v9

    aget v3, v7, v10

    aget v4, v7, v11

    aget v5, v7, v12

    const/4 v0, 0x5

    aget v6, v7, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->curveTo(FFFFFF)V

    goto :goto_1

    .line 300
    :pswitch_4
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->closePath()V

    goto :goto_1

    .line 306
    .end local v7    # "coords":[F
    :cond_4
    return-void

    .line 277
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method checkBuf(IZ)V
    .locals 4
    .param p1, "pointCount"    # I
    .param p2, "checkMove"    # Z

    .prologue
    const/4 v3, 0x0

    .line 207
    if-eqz p2, :cond_0

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    if-nez v1, :cond_0

    .line 209
    new-instance v1, Lorg/icepdf/index/java/awt/geom/IllegalPathStateException;

    const-string/jumbo v2, "awt.20A"

    invoke-static {v2}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/icepdf/index/java/awt/geom/IllegalPathStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 211
    :cond_0
    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    iget-object v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    array-length v2, v2

    if-ne v1, v2, :cond_1

    .line 212
    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    add-int/lit8 v1, v1, 0xa

    new-array v0, v1, [B

    .line 213
    .local v0, "tmp":[B
    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 214
    iput-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    .line 216
    .end local v0    # "tmp":[B
    :cond_1
    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/2addr v1, p1

    iget-object v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    array-length v2, v2

    if-le v1, v2, :cond_2

    .line 217
    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    const/16 v2, 0x14

    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/2addr v1, v2

    new-array v0, v1, [F

    .line 218
    .local v0, "tmp":[F
    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 219
    iput-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    .line 221
    .end local v0    # "tmp":[F
    :cond_2
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 424
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/index/java/awt/geom/GeneralPath;

    .line 425
    .local v1, "p":Lorg/icepdf/index/java/awt/geom/GeneralPath;
    iget-object v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    invoke-virtual {v2}, [B->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    iput-object v2, v1, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    .line 426
    iget-object v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    invoke-virtual {v2}, [F->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [F

    iput-object v2, v1, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 427
    return-object v1

    .line 428
    .end local v1    # "p":Lorg/icepdf/index/java/awt/geom/GeneralPath;
    :catch_0
    move-exception v0

    .line 429
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/InternalError;

    invoke-direct {v2}, Ljava/lang/InternalError;-><init>()V

    throw v2
.end method

.method public closePath()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 263
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    add-int/lit8 v1, v1, -0x1

    aget-byte v0, v0, v1

    if-eq v0, v3, :cond_1

    .line 264
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->checkBuf(IZ)V

    .line 265
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    aput-byte v3, v0, v1

    .line 267
    :cond_1
    return-void
.end method

.method public contains(DD)Z
    .locals 1
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    .line 388
    invoke-static {p0, p1, p2, p3, p4}, Lorg/apache/harmony/awt/gl/Crossing;->crossShape(Lorg/icepdf/index/java/awt/Shape;DD)I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->isInside(I)Z

    move-result v0

    return v0
.end method

.method public contains(DDDD)Z
    .locals 3
    .param p1, "rx"    # D
    .param p3, "ry"    # D
    .param p5, "rw"    # D
    .param p7, "rh"    # D

    .prologue
    .line 392
    invoke-static/range {p0 .. p8}, Lorg/apache/harmony/awt/gl/Crossing;->intersectShape(Lorg/icepdf/index/java/awt/Shape;DDDD)I

    move-result v0

    .line 393
    .local v0, "cross":I
    const/16 v1, 0xff

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->isInside(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public contains(Lorg/icepdf/index/java/awt/geom/Point2D;)Z
    .locals 4
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 402
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->contains(DD)Z

    move-result v0

    return v0
.end method

.method public contains(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
    .locals 10
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 406
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->contains(DDDD)Z

    move-result v0

    return v0
.end method

.method public createTransformedShape(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/Shape;
    .locals 1
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 336
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/java/awt/geom/GeneralPath;

    .line 337
    .local v0, "p":Lorg/icepdf/index/java/awt/geom/GeneralPath;
    if-eqz p1, :cond_0

    .line 338
    invoke-virtual {v0, p1}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->transform(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    .line 340
    :cond_0
    return-object v0
.end method

.method public curveTo(FFFFFF)V
    .locals 3
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "x2"    # F
    .param p4, "y2"    # F
    .param p5, "x3"    # F
    .param p6, "y3"    # F

    .prologue
    .line 252
    const/4 v0, 0x6

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->checkBuf(IZ)V

    .line 253
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    const/4 v2, 0x3

    aput-byte v2, v0, v1

    .line 254
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p1, v0, v1

    .line 255
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p2, v0, v1

    .line 256
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p3, v0, v1

    .line 257
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p4, v0, v1

    .line 258
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p5, v0, v1

    .line 259
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p6, v0, v1

    .line 260
    return-void
.end method

.method public getBounds()Lorg/icepdf/index/java/awt/Rectangle;
    .locals 1

    .prologue
    .line 372
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    move-result-object v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getBounds()Lorg/icepdf/index/java/awt/Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 11

    .prologue
    .line 345
    iget v8, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    if-nez v8, :cond_1

    .line 346
    const/4 v5, 0x0

    .local v5, "ry2":F
    move v3, v5

    .local v3, "rx2":F
    move v4, v5

    .local v4, "ry1":F
    move v2, v5

    .line 368
    .local v2, "rx1":F
    :cond_0
    new-instance v8, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    sub-float v9, v3, v2

    sub-float v10, v5, v4

    invoke-direct {v8, v2, v4, v9, v10}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    return-object v8

    .line 348
    .end local v2    # "rx1":F
    .end local v3    # "rx2":F
    .end local v4    # "ry1":F
    .end local v5    # "ry2":F
    :cond_1
    iget v8, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v0, v8, -0x1

    .line 349
    .local v0, "i":I
    iget-object v8, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    add-int/lit8 v1, v0, -0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aget v5, v8, v0

    .restart local v5    # "ry2":F
    move v4, v5

    .line 350
    .restart local v4    # "ry1":F
    iget-object v8, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    add-int/lit8 v0, v1, -0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget v3, v8, v1

    .restart local v3    # "rx2":F
    move v2, v3

    .restart local v2    # "rx1":F
    move v1, v0

    .line 351
    .end local v0    # "i":I
    .restart local v1    # "i":I
    :cond_2
    :goto_0
    if-lez v1, :cond_0

    .line 352
    iget-object v8, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    add-int/lit8 v0, v1, -0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget v7, v8, v1

    .line 353
    .local v7, "y":F
    iget-object v8, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    add-int/lit8 v1, v0, -0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    aget v6, v8, v0

    .line 354
    .local v6, "x":F
    cmpg-float v8, v6, v2

    if-gez v8, :cond_4

    .line 355
    move v2, v6

    .line 360
    :cond_3
    :goto_1
    cmpg-float v8, v7, v4

    if-gez v8, :cond_5

    .line 361
    move v4, v7

    goto :goto_0

    .line 357
    :cond_4
    cmpl-float v8, v6, v3

    if-lez v8, :cond_3

    .line 358
    move v3, v6

    goto :goto_1

    .line 363
    :cond_5
    cmpl-float v8, v7, v5

    if-lez v8, :cond_2

    .line 364
    move v5, v7

    goto :goto_0
.end method

.method public getCurrentPoint()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 7

    .prologue
    .line 309
    iget v3, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    if-nez v3, :cond_0

    .line 310
    const/4 v3, 0x0

    .line 323
    :goto_0
    return-object v3

    .line 312
    :cond_0
    iget v3, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v1, v3, -0x2

    .line 313
    .local v1, "j":I
    iget-object v3, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    iget v4, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    add-int/lit8 v4, v4, -0x1

    aget-byte v3, v3, v4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    .line 315
    iget v3, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    add-int/lit8 v0, v3, -0x2

    .local v0, "i":I
    :goto_1
    if-lez v0, :cond_1

    .line 316
    iget-object v3, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    aget-byte v2, v3, v0

    .line 317
    .local v2, "type":I
    if-nez v2, :cond_2

    .line 323
    .end local v0    # "i":I
    .end local v2    # "type":I
    :cond_1
    new-instance v3, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    iget-object v4, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    aget v4, v4, v1

    iget-object v5, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    add-int/lit8 v6, v1, 0x1

    aget v5, v5, v6

    invoke-direct {v3, v4, v5}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>(FF)V

    goto :goto_0

    .line 320
    .restart local v0    # "i":I
    .restart local v2    # "type":I
    :cond_2
    sget-object v3, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointShift:[I

    aget v3, v3, v2

    sub-int/2addr v1, v3

    .line 315
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;
    .locals 1
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 414
    new-instance v0, Lorg/icepdf/index/java/awt/geom/GeneralPath$Iterator;

    invoke-direct {v0, p0, p1}, Lorg/icepdf/index/java/awt/geom/GeneralPath$Iterator;-><init>(Lorg/icepdf/index/java/awt/geom/GeneralPath;Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    return-object v0
.end method

.method public getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;D)Lorg/icepdf/index/java/awt/geom/PathIterator;
    .locals 2
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .param p2, "flatness"    # D

    .prologue
    .line 418
    new-instance v0, Lorg/icepdf/index/java/awt/geom/FlatteningPathIterator;

    invoke-virtual {p0, p1}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;

    move-result-object v1

    invoke-direct {v0, v1, p2, p3}, Lorg/icepdf/index/java/awt/geom/FlatteningPathIterator;-><init>(Lorg/icepdf/index/java/awt/geom/PathIterator;D)V

    return-object v0
.end method

.method public getWindingRule()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->rule:I

    return v0
.end method

.method public intersects(DDDD)Z
    .locals 3
    .param p1, "rx"    # D
    .param p3, "ry"    # D
    .param p5, "rw"    # D
    .param p7, "rh"    # D

    .prologue
    .line 397
    invoke-static/range {p0 .. p8}, Lorg/apache/harmony/awt/gl/Crossing;->intersectShape(Lorg/icepdf/index/java/awt/Shape;DDDD)I

    move-result v0

    .line 398
    .local v0, "cross":I
    const/16 v1, 0xff

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->isInside(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public intersects(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
    .locals 10
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 410
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->intersects(DDDD)Z

    move-result v0

    return v0
.end method

.method isInside(I)Z
    .locals 2
    .param p1, "cross"    # I

    .prologue
    .line 381
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->rule:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 382
    invoke-static {p1}, Lorg/apache/harmony/awt/gl/Crossing;->isInsideNonZero(I)Z

    move-result v0

    .line 384
    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lorg/apache/harmony/awt/gl/Crossing;->isInsideEvenOdd(I)Z

    move-result v0

    goto :goto_0
.end method

.method public lineTo(FF)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v3, 0x1

    .line 236
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->checkBuf(IZ)V

    .line 237
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    aput-byte v3, v0, v1

    .line 238
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p1, v0, v1

    .line 239
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p2, v0, v1

    .line 240
    return-void
.end method

.method public moveTo(FF)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v3, 0x0

    .line 224
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    add-int/lit8 v1, v1, -0x1

    aget-byte v0, v0, v1

    if-nez v0, :cond_0

    .line 225
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v1, v1, -0x2

    aput p1, v0, v1

    .line 226
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v1, v1, -0x1

    aput p2, v0, v1

    .line 233
    :goto_0
    return-void

    .line 228
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->checkBuf(IZ)V

    .line 229
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    aput-byte v3, v0, v1

    .line 230
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p1, v0, v1

    .line 231
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p2, v0, v1

    goto :goto_0
.end method

.method public quadTo(FFFF)V
    .locals 3
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "x2"    # F
    .param p4, "y2"    # F

    .prologue
    .line 243
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->checkBuf(IZ)V

    .line 244
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->types:[B

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    const/4 v2, 0x2

    aput-byte v2, v0, v1

    .line 245
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p1, v0, v1

    .line 246
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p2, v0, v1

    .line 247
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p3, v0, v1

    .line 248
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    aput p4, v0, v1

    .line 249
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 327
    iput v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->typeSize:I

    .line 328
    iput v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    .line 329
    return-void
.end method

.method public setWindingRule(I)V
    .locals 2
    .param p1, "rule"    # I

    .prologue
    .line 191
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 193
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "awt.209"

    invoke-static {v1}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :cond_0
    iput p1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->rule:I

    .line 196
    return-void
.end method

.method public transform(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V
    .locals 6
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    const/4 v2, 0x0

    .line 332
    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget-object v3, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->points:[F

    iget v0, p0, Lorg/icepdf/index/java/awt/geom/GeneralPath;->pointSize:I

    div-int/lit8 v5, v0, 0x2

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->transform([FI[FII)V

    .line 333
    return-void
.end method
