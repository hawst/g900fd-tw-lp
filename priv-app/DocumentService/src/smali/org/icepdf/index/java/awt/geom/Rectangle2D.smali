.class public abstract Lorg/icepdf/index/java/awt/geom/Rectangle2D;
.super Lorg/icepdf/index/java/awt/geom/RectangularShape;
.source "Rectangle2D.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;,
        Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;,
        Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    }
.end annotation


# static fields
.field public static final OUT_BOTTOM:I = 0x8

.field public static final OUT_LEFT:I = 0x1

.field public static final OUT_RIGHT:I = 0x4

.field public static final OUT_TOP:I = 0x2


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 413
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;-><init>()V

    .line 414
    return-void
.end method

.method public static intersect(Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V
    .locals 14
    .param p0, "src1"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .param p1, "src2"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .param p2, "dst"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 503
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMinX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMinX()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    .line 504
    .local v2, "x1":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMinY()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMinY()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    .line 505
    .local v4, "y1":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMaxX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMaxX()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v10

    .line 506
    .local v10, "x2":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMaxY()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMaxY()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v12

    .line 507
    .local v12, "y2":D
    sub-double v6, v10, v2

    sub-double v8, v12, v4

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->setFrame(DDDD)V

    .line 508
    return-void
.end method

.method public static union(Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V
    .locals 14
    .param p0, "src1"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .param p1, "src2"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .param p2, "dst"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 511
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMinX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMinX()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 512
    .local v2, "x1":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMinY()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMinY()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    .line 513
    .local v4, "y1":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMaxX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMaxX()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    .line 514
    .local v10, "x2":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMaxY()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMaxY()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v12

    .line 515
    .local v12, "y2":D
    sub-double v6, v10, v2

    sub-double v8, v12, v4

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->setFrame(DDDD)V

    .line 516
    return-void
.end method


# virtual methods
.method public add(DD)V
    .locals 17
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 519
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMinX()D

    move-result-wide v2

    move-wide/from16 v0, p1

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    .line 520
    .local v4, "x1":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMinY()D

    move-result-wide v2

    move-wide/from16 v0, p3

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    .line 521
    .local v6, "y1":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMaxX()D

    move-result-wide v2

    move-wide/from16 v0, p1

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v12

    .line 522
    .local v12, "x2":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getMaxY()D

    move-result-wide v2

    move-wide/from16 v0, p3

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v14

    .line 523
    .local v14, "y2":D
    sub-double v8, v12, v4

    sub-double v10, v14, v6

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v11}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->setRect(DDDD)V

    .line 524
    return-void
.end method

.method public add(Lorg/icepdf/index/java/awt/geom/Point2D;)V
    .locals 4
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 527
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->add(DD)V

    .line 528
    return-void
.end method

.method public add(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V
    .locals 0
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 531
    invoke-static {p0, p1, p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->union(Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    .line 532
    return-void
.end method

.method public contains(DD)Z
    .locals 11
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 458
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 459
    const/4 v8, 0x0

    .line 467
    :goto_0
    return v8

    .line 462
    :cond_0
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    .line 463
    .local v0, "x1":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    .line 464
    .local v4, "y1":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v8

    add-double v2, v0, v8

    .line 465
    .local v2, "x2":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    add-double v6, v4, v8

    .line 467
    .local v6, "y2":D
    cmpg-double v8, v0, p1

    if-gtz v8, :cond_1

    cmpg-double v8, p1, v2

    if-gez v8, :cond_1

    cmpg-double v8, v4, p3

    if-gtz v8, :cond_1

    cmpg-double v8, p3, v6

    if-gez v8, :cond_1

    const/4 v8, 0x1

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public contains(DDDD)Z
    .locals 11
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 488
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    const-wide/16 v8, 0x0

    cmpg-double v8, p5, v8

    if-lez v8, :cond_0

    const-wide/16 v8, 0x0

    cmpg-double v8, p7, v8

    if-gtz v8, :cond_1

    .line 489
    :cond_0
    const/4 v8, 0x0

    .line 497
    :goto_0
    return v8

    .line 492
    :cond_1
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    .line 493
    .local v0, "x1":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    .line 494
    .local v4, "y1":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v8

    add-double v2, v0, v8

    .line 495
    .local v2, "x2":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    add-double v6, v4, v8

    .line 497
    .local v6, "y2":D
    cmpg-double v8, v0, p1

    if-gtz v8, :cond_2

    add-double v8, p1, p5

    cmpg-double v8, v8, v2

    if-gtz v8, :cond_2

    cmpg-double v8, v4, p3

    if-gtz v8, :cond_2

    add-double v8, p3, p7

    cmpg-double v8, v8, v6

    if-gtz v8, :cond_2

    const/4 v8, 0x1

    goto :goto_0

    :cond_2
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public abstract createIntersection(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Lorg/icepdf/index/java/awt/geom/Rectangle2D;
.end method

.method public abstract createUnion(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Lorg/icepdf/index/java/awt/geom/Rectangle2D;
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 555
    if-ne p1, p0, :cond_1

    .line 566
    :cond_0
    :goto_0
    return v1

    .line 558
    :cond_1
    instance-of v3, p1, Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 559
    check-cast v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .line 560
    .local v0, "r":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v4

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v4

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v4

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "r":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    :cond_3
    move v1, v2

    .line 566
    goto :goto_0
.end method

.method public getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 1

    .prologue
    .line 434
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    return-object v0
.end method

.method public getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;
    .locals 1
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 535
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;

    invoke-direct {v0, p0, p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;-><init>(Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    return-object v0
.end method

.method public getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;D)Lorg/icepdf/index/java/awt/geom/PathIterator;
    .locals 1
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .param p2, "flatness"    # D

    .prologue
    .line 540
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;

    invoke-direct {v0, p0, p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;-><init>(Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 545
    new-instance v0, Lorg/apache/harmony/misc/HashCode;

    invoke-direct {v0}, Lorg/apache/harmony/misc/HashCode;-><init>()V

    .line 546
    .local v0, "hash":Lorg/apache/harmony/misc/HashCode;
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/harmony/misc/HashCode;->append(D)Lorg/apache/harmony/misc/HashCode;

    .line 547
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/harmony/misc/HashCode;->append(D)Lorg/apache/harmony/misc/HashCode;

    .line 548
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/harmony/misc/HashCode;->append(D)Lorg/apache/harmony/misc/HashCode;

    .line 549
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/harmony/misc/HashCode;->append(D)Lorg/apache/harmony/misc/HashCode;

    .line 550
    invoke-virtual {v0}, Lorg/apache/harmony/misc/HashCode;->hashCode()I

    move-result v1

    return v1
.end method

.method public intersects(DDDD)Z
    .locals 11
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 473
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    const-wide/16 v8, 0x0

    cmpg-double v8, p5, v8

    if-lez v8, :cond_0

    const-wide/16 v8, 0x0

    cmpg-double v8, p7, v8

    if-gtz v8, :cond_1

    .line 474
    :cond_0
    const/4 v8, 0x0

    .line 482
    :goto_0
    return v8

    .line 477
    :cond_1
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    .line 478
    .local v0, "x1":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    .line 479
    .local v4, "y1":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v8

    add-double v2, v0, v8

    .line 480
    .local v2, "x2":D
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    add-double v6, v4, v8

    .line 482
    .local v6, "y2":D
    add-double v8, p1, p5

    cmpl-double v8, v8, v0

    if-lez v8, :cond_2

    cmpg-double v8, p1, v2

    if-gez v8, :cond_2

    add-double v8, p3, p7

    cmpl-double v8, v8, v4

    if-lez v8, :cond_2

    cmpg-double v8, p3, v6

    if-gez v8, :cond_2

    const/4 v8, 0x1

    goto :goto_0

    :cond_2
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public intersectsLine(DDDD)Z
    .locals 25
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "x2"    # D
    .param p7, "y2"    # D

    .prologue
    .line 438
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    .line 439
    .local v0, "rx1":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v2

    .line 440
    .local v2, "ry1":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v8

    add-double v4, v0, v8

    .line 441
    .local v4, "rx2":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    add-double v6, v2, v8

    .line 442
    .local v6, "ry2":D
    cmpg-double v8, v0, p1

    if-gtz v8, :cond_0

    cmpg-double v8, p1, v4

    if-gtz v8, :cond_0

    cmpg-double v8, v2, p3

    if-gtz v8, :cond_0

    cmpg-double v8, p3, v6

    if-lez v8, :cond_2

    :cond_0
    cmpg-double v8, v0, p5

    if-gtz v8, :cond_1

    cmpg-double v8, p5, v4

    if-gtz v8, :cond_1

    cmpg-double v8, v2, p7

    if-gtz v8, :cond_1

    cmpg-double v8, p7, v6

    if-lez v8, :cond_2

    :cond_1
    move-wide/from16 v8, p1

    move-wide/from16 v10, p3

    move-wide/from16 v12, p5

    move-wide/from16 v14, p7

    invoke-static/range {v0 .. v15}, Lorg/icepdf/index/java/awt/geom/Line2D;->linesIntersect(DDDDDDDD)Z

    move-result v8

    if-nez v8, :cond_2

    move-wide v8, v4

    move-wide v10, v2

    move-wide v12, v0

    move-wide v14, v6

    move-wide/from16 v16, p1

    move-wide/from16 v18, p3

    move-wide/from16 v20, p5

    move-wide/from16 v22, p7

    invoke-static/range {v8 .. v23}, Lorg/icepdf/index/java/awt/geom/Line2D;->linesIntersect(DDDDDDDD)Z

    move-result v8

    if-eqz v8, :cond_3

    :cond_2
    const/4 v8, 0x1

    :goto_0
    return v8

    :cond_3
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public intersectsLine(Lorg/icepdf/index/java/awt/geom/Line2D;)Z
    .locals 10
    .param p1, "l"    # Lorg/icepdf/index/java/awt/geom/Line2D;

    .prologue
    .line 450
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->intersectsLine(DDDD)Z

    move-result v0

    return v0
.end method

.method public abstract outcode(DD)I
.end method

.method public outcode(Lorg/icepdf/index/java/awt/geom/Point2D;)I
    .locals 4
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 454
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->outcode(DD)I

    move-result v0

    return v0
.end method

.method public setFrame(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 430
    invoke-virtual/range {p0 .. p8}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->setRect(DDDD)V

    .line 431
    return-void
.end method

.method public abstract setRect(DDDD)V
.end method

.method public setRect(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V
    .locals 10
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 425
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->setRect(DDDD)V

    .line 426
    return-void
.end method
