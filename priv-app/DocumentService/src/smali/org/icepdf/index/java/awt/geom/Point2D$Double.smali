.class public Lorg/icepdf/index/java/awt/geom/Point2D$Double;
.super Lorg/icepdf/index/java/awt/geom/Point2D;
.source "Point2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/Point2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Double"
.end annotation


# instance fields
.field public x:D

.field public y:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;-><init>()V

    .line 72
    return-void
.end method

.method public constructor <init>(DD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Point2D;-><init>()V

    .line 75
    iput-wide p1, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;->x:D

    .line 76
    iput-wide p3, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;->y:D

    .line 77
    return-void
.end method


# virtual methods
.method public getX()D
    .locals 2

    .prologue
    .line 81
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;->x:D

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;->y:D

    return-wide v0
.end method

.method public setLocation(DD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 91
    iput-wide p1, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;->x:D

    .line 92
    iput-wide p3, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;->y:D

    .line 93
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "[x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;->x:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;->y:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
