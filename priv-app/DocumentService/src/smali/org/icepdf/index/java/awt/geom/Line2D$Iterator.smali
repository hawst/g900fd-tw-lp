.class Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;
.super Ljava/lang/Object;
.source "Line2D.java"

# interfaces
.implements Lorg/icepdf/index/java/awt/geom/PathIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/Line2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Iterator"
.end annotation


# instance fields
.field index:I

.field t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

.field x1:D

.field x2:D

.field y1:D

.field y2:D


# direct methods
.method constructor <init>(Lorg/icepdf/index/java/awt/geom/Line2D;Lorg/icepdf/index/java/awt/geom/AffineTransform;)V
    .locals 2
    .param p1, "l"    # Lorg/icepdf/index/java/awt/geom/Line2D;
    .param p2, "at"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX1()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->x1:D

    .line 232
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY1()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->y1:D

    .line 233
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getX2()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->x2:D

    .line 234
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Line2D;->getY2()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->y2:D

    .line 235
    iput-object p2, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .line 236
    return-void
.end method


# virtual methods
.method public currentSegment([D)I
    .locals 7
    .param p1, "coords"    # [D

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 251
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string/jumbo v1, "awt.4B"

    invoke-static {v1}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 256
    :cond_0
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->index:I

    if-nez v0, :cond_2

    .line 257
    const/4 v6, 0x0

    .line 258
    .local v6, "type":I
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->x1:D

    aput-wide v0, p1, v2

    .line 259
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->y1:D

    aput-wide v0, p1, v5

    .line 265
    :goto_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    if-eqz v0, :cond_1

    .line 266
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-object v1, p1

    move-object v3, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->transform([DI[DII)V

    .line 268
    :cond_1
    return v6

    .line 261
    .end local v6    # "type":I
    :cond_2
    const/4 v6, 0x1

    .line 262
    .restart local v6    # "type":I
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->x2:D

    aput-wide v0, p1, v2

    .line 263
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->y2:D

    aput-wide v0, p1, v5

    goto :goto_0
.end method

.method public currentSegment([F)I
    .locals 7
    .param p1, "coords"    # [F

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 272
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string/jumbo v1, "awt.4B"

    invoke-static {v1}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_0
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->index:I

    if-nez v0, :cond_2

    .line 278
    const/4 v6, 0x0

    .line 279
    .local v6, "type":I
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->x1:D

    double-to-float v0, v0

    aput v0, p1, v2

    .line 280
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->y1:D

    double-to-float v0, v0

    aput v0, p1, v5

    .line 286
    :goto_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-object v1, p1

    move-object v3, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->transform([FI[FII)V

    .line 289
    :cond_1
    return v6

    .line 282
    .end local v6    # "type":I
    :cond_2
    const/4 v6, 0x1

    .line 283
    .restart local v6    # "type":I
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->x2:D

    double-to-float v0, v0

    aput v0, p1, v2

    .line 284
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->y2:D

    double-to-float v0, v0

    aput v0, p1, v5

    goto :goto_0
.end method

.method public getWindingRule()I
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x1

    return v0
.end method

.method public isDone()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 243
    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->index:I

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()V
    .locals 1

    .prologue
    .line 247
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Iterator;->index:I

    .line 248
    return-void
.end method
