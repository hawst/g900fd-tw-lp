.class public Lorg/icepdf/index/java/awt/Rectangle;
.super Lorg/icepdf/index/java/awt/geom/Rectangle2D;
.source "Rectangle.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/icepdf/index/java/awt/Shape;


# static fields
.field private static final serialVersionUID:J = -0x3c4f95fae535958cL


# instance fields
.field public height:I

.field public width:I

.field public x:I

.field public y:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;-><init>()V

    .line 37
    invoke-virtual {p0, v0, v0, v0, v0}, Lorg/icepdf/index/java/awt/Rectangle;->setBounds(IIII)V

    .line 38
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;-><init>()V

    .line 53
    invoke-virtual {p0, v0, v0, p1, p2}, Lorg/icepdf/index/java/awt/Rectangle;->setBounds(IIII)V

    .line 54
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;-><init>()V

    .line 49
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/icepdf/index/java/awt/Rectangle;->setBounds(IIII)V

    .line 50
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/java/awt/Dimension;)V
    .locals 3
    .param p1, "d"    # Lorg/icepdf/index/java/awt/Dimension;

    .prologue
    const/4 v2, 0x0

    .line 60
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;-><init>()V

    .line 61
    iget v0, p1, Lorg/icepdf/index/java/awt/Dimension;->width:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Dimension;->height:I

    invoke-virtual {p0, v2, v2, v0, v1}, Lorg/icepdf/index/java/awt/Rectangle;->setBounds(IIII)V

    .line 62
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/java/awt/Point;)V
    .locals 3
    .param p1, "p"    # Lorg/icepdf/index/java/awt/Point;

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;-><init>()V

    .line 41
    iget v0, p1, Lorg/icepdf/index/java/awt/Point;->x:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Point;->y:I

    invoke-virtual {p0, v0, v1, v2, v2}, Lorg/icepdf/index/java/awt/Rectangle;->setBounds(IIII)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/java/awt/Point;Lorg/icepdf/index/java/awt/Dimension;)V
    .locals 4
    .param p1, "p"    # Lorg/icepdf/index/java/awt/Point;
    .param p2, "d"    # Lorg/icepdf/index/java/awt/Dimension;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;-><init>()V

    .line 45
    iget v0, p1, Lorg/icepdf/index/java/awt/Point;->x:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Point;->y:I

    iget v2, p2, Lorg/icepdf/index/java/awt/Dimension;->width:I

    iget v3, p2, Lorg/icepdf/index/java/awt/Dimension;->height:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/icepdf/index/java/awt/Rectangle;->setBounds(IIII)V

    .line 46
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/java/awt/Rectangle;)V
    .locals 4
    .param p1, "r"    # Lorg/icepdf/index/java/awt/Rectangle;

    .prologue
    .line 56
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;-><init>()V

    .line 57
    iget v0, p1, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    iget v2, p1, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    iget v3, p1, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/icepdf/index/java/awt/Rectangle;->setBounds(IIII)V

    .line 58
    return-void
.end method


# virtual methods
.method public add(II)V
    .locals 6
    .param p1, "px"    # I
    .param p2, "py"    # I

    .prologue
    .line 182
    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    invoke-static {v4, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 183
    .local v0, "x1":I
    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v5, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    add-int/2addr v4, v5

    invoke-static {v4, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 184
    .local v1, "x2":I
    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    invoke-static {v4, p2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 185
    .local v2, "y1":I
    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    iget v5, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    add-int/2addr v4, v5

    invoke-static {v4, p2}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 186
    .local v3, "y2":I
    sub-int v4, v1, v0

    sub-int v5, v3, v2

    invoke-virtual {p0, v0, v2, v4, v5}, Lorg/icepdf/index/java/awt/Rectangle;->setBounds(IIII)V

    .line 187
    return-void
.end method

.method public add(Lorg/icepdf/index/java/awt/Point;)V
    .locals 2
    .param p1, "p"    # Lorg/icepdf/index/java/awt/Point;

    .prologue
    .line 190
    iget v0, p1, Lorg/icepdf/index/java/awt/Point;->x:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Point;->y:I

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/Rectangle;->add(II)V

    .line 191
    return-void
.end method

.method public add(Lorg/icepdf/index/java/awt/Rectangle;)V
    .locals 7
    .param p1, "r"    # Lorg/icepdf/index/java/awt/Rectangle;

    .prologue
    .line 194
    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v5, p1, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 195
    .local v0, "x1":I
    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v5, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    add-int/2addr v4, v5

    iget v5, p1, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v6, p1, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 196
    .local v1, "x2":I
    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    iget v5, p1, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 197
    .local v2, "y1":I
    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    iget v5, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    add-int/2addr v4, v5

    iget v5, p1, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    iget v6, p1, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 198
    .local v3, "y2":I
    sub-int v4, v1, v0

    sub-int v5, v3, v2

    invoke-virtual {p0, v0, v2, v4, v5}, Lorg/icepdf/index/java/awt/Rectangle;->setBounds(IIII)V

    .line 199
    return-void
.end method

.method public contains(II)Z
    .locals 2
    .param p1, "px"    # I
    .param p2, "py"    # I

    .prologue
    const/4 v0, 0x0

    .line 202
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/Rectangle;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 210
    :cond_0
    :goto_0
    return v0

    .line 205
    :cond_1
    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    if-lt p1, v1, :cond_0

    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    if-lt p2, v1, :cond_0

    .line 208
    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    sub-int/2addr p1, v1

    .line 209
    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    sub-int/2addr p2, v1

    .line 210
    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    if-ge p1, v1, :cond_0

    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    if-ge p2, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public contains(IIII)Z
    .locals 2
    .param p1, "rx"    # I
    .param p2, "ry"    # I
    .param p3, "rw"    # I
    .param p4, "rh"    # I

    .prologue
    .line 218
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/java/awt/Rectangle;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int v0, p1, p3

    add-int/lit8 v0, v0, -0x1

    add-int v1, p2, p4

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/Rectangle;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public contains(Lorg/icepdf/index/java/awt/Point;)Z
    .locals 2
    .param p1, "p"    # Lorg/icepdf/index/java/awt/Point;

    .prologue
    .line 214
    iget v0, p1, Lorg/icepdf/index/java/awt/Point;->x:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Point;->y:I

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/Rectangle;->contains(II)Z

    move-result v0

    return v0
.end method

.method public contains(Lorg/icepdf/index/java/awt/Rectangle;)Z
    .locals 4
    .param p1, "r"    # Lorg/icepdf/index/java/awt/Rectangle;

    .prologue
    .line 222
    iget v0, p1, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    iget v2, p1, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    iget v3, p1, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/icepdf/index/java/awt/Rectangle;->contains(IIII)Z

    move-result v0

    return v0
.end method

.method public createIntersection(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 2
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 235
    instance-of v1, p1, Lorg/icepdf/index/java/awt/Rectangle;

    if-eqz v1, :cond_0

    .line 236
    check-cast p1, Lorg/icepdf/index/java/awt/Rectangle;

    .end local p1    # "r":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    invoke-virtual {p0, p1}, Lorg/icepdf/index/java/awt/Rectangle;->intersection(Lorg/icepdf/index/java/awt/Rectangle;)Lorg/icepdf/index/java/awt/Rectangle;

    move-result-object v0

    .line 240
    :goto_0
    return-object v0

    .line 238
    .restart local p1    # "r":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    :cond_0
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 239
    .local v0, "dst":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    invoke-static {p0, p1, v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->intersect(Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    goto :goto_0
.end method

.method public createUnion(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 2
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 284
    instance-of v1, p1, Lorg/icepdf/index/java/awt/Rectangle;

    if-eqz v1, :cond_0

    .line 285
    check-cast p1, Lorg/icepdf/index/java/awt/Rectangle;

    .end local p1    # "r":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    invoke-virtual {p0, p1}, Lorg/icepdf/index/java/awt/Rectangle;->union(Lorg/icepdf/index/java/awt/Rectangle;)Lorg/icepdf/index/java/awt/Rectangle;

    move-result-object v0

    .line 289
    :goto_0
    return-object v0

    .line 287
    .restart local p1    # "r":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    :cond_0
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 288
    .local v0, "dst":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    invoke-static {p0, p1, v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->union(Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 300
    if-ne p1, p0, :cond_1

    .line 307
    :cond_0
    :goto_0
    return v1

    .line 303
    :cond_1
    instance-of v3, p1, Lorg/icepdf/index/java/awt/Rectangle;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 304
    check-cast v0, Lorg/icepdf/index/java/awt/Rectangle;

    .line 305
    .local v0, "r":Lorg/icepdf/index/java/awt/Rectangle;
    iget v3, v0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    if-ne v3, v4, :cond_2

    iget v3, v0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    if-ne v3, v4, :cond_2

    iget v3, v0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    if-ne v3, v4, :cond_2

    iget v3, v0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "r":Lorg/icepdf/index/java/awt/Rectangle;
    :cond_3
    move v1, v2

    .line 307
    goto :goto_0
.end method

.method public getBounds()Lorg/icepdf/index/java/awt/Rectangle;
    .locals 5

    .prologue
    .line 150
    new-instance v0, Lorg/icepdf/index/java/awt/Rectangle;

    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v2, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    iget v3, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/icepdf/index/java/awt/Rectangle;-><init>(IIII)V

    return-object v0
.end method

.method public getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/Rectangle;->getBounds()Lorg/icepdf/index/java/awt/Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()D
    .locals 2

    .prologue
    .line 76
    iget v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public getLocation()Lorg/icepdf/index/java/awt/Point;
    .locals 3

    .prologue
    .line 103
    new-instance v0, Lorg/icepdf/index/java/awt/Point;

    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v2, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/java/awt/Point;-><init>(II)V

    return-object v0
.end method

.method public getSize()Lorg/icepdf/index/java/awt/Dimension;
    .locals 3

    .prologue
    .line 90
    new-instance v0, Lorg/icepdf/index/java/awt/Dimension;

    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    iget v2, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/java/awt/Dimension;-><init>(II)V

    return-object v0
.end method

.method public getWidth()D
    .locals 2

    .prologue
    .line 81
    iget v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 66
    iget v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 71
    iget v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public grow(II)V
    .locals 2
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 170
    iget v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    sub-int/2addr v0, p1

    iput v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    .line 171
    iget v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    sub-int/2addr v0, p2

    iput v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    .line 172
    iget v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    add-int v1, p1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    .line 173
    iget v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    add-int v1, p2, p2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    .line 174
    return-void
.end method

.method public inside(II)Z
    .locals 1
    .param p1, "px"    # I
    .param p2, "py"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 230
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/java/awt/Rectangle;->contains(II)Z

    move-result v0

    return v0
.end method

.method public intersection(Lorg/icepdf/index/java/awt/Rectangle;)Lorg/icepdf/index/java/awt/Rectangle;
    .locals 7
    .param p1, "r"    # Lorg/icepdf/index/java/awt/Rectangle;

    .prologue
    .line 244
    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v5, p1, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 245
    .local v0, "x1":I
    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    iget v5, p1, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 246
    .local v2, "y1":I
    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v5, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    add-int/2addr v4, v5

    iget v5, p1, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v6, p1, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 247
    .local v1, "x2":I
    iget v4, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    iget v5, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    add-int/2addr v4, v5

    iget v5, p1, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    iget v6, p1, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 248
    .local v3, "y2":I
    new-instance v4, Lorg/icepdf/index/java/awt/Rectangle;

    sub-int v5, v1, v0

    sub-int v6, v3, v2

    invoke-direct {v4, v0, v2, v5, v6}, Lorg/icepdf/index/java/awt/Rectangle;-><init>(IIII)V

    return-object v4
.end method

.method public intersects(Lorg/icepdf/index/java/awt/Rectangle;)Z
    .locals 1
    .param p1, "r"    # Lorg/icepdf/index/java/awt/Rectangle;

    .prologue
    .line 252
    invoke-virtual {p0, p1}, Lorg/icepdf/index/java/awt/Rectangle;->intersection(Lorg/icepdf/index/java/awt/Rectangle;)Lorg/icepdf/index/java/awt/Rectangle;

    move-result-object v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/Rectangle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    if-lez v0, :cond_0

    iget v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public move(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 120
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/java/awt/Rectangle;->setLocation(II)V

    .line 121
    return-void
.end method

.method public outcode(DD)I
    .locals 5
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    .line 257
    const/4 v0, 0x0

    .line 259
    .local v0, "code":I
    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    if-gtz v1, :cond_2

    .line 260
    or-int/lit8 v0, v0, 0x5

    .line 269
    :cond_0
    :goto_0
    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    if-gtz v1, :cond_4

    .line 270
    or-int/lit8 v0, v0, 0xa

    .line 279
    :cond_1
    :goto_1
    return v0

    .line 262
    :cond_2
    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    int-to-double v2, v1

    cmpg-double v1, p1, v2

    if-gez v1, :cond_3

    .line 263
    or-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 265
    :cond_3
    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v2, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    add-int/2addr v1, v2

    int-to-double v2, v1

    cmpl-double v1, p1, v2

    if-lez v1, :cond_0

    .line 266
    or-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 272
    :cond_4
    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    int-to-double v2, v1

    cmpg-double v1, p3, v2

    if-gez v1, :cond_5

    .line 273
    or-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 275
    :cond_5
    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    iget v2, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    add-int/2addr v1, v2

    int-to-double v2, v1

    cmpl-double v1, p3, v2

    if-lez v1, :cond_1

    .line 276
    or-int/lit8 v0, v0, 0x8

    goto :goto_1
.end method

.method public reshape(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 145
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/icepdf/index/java/awt/Rectangle;->setBounds(IIII)V

    .line 146
    return-void
.end method

.method public resize(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 137
    iget v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    invoke-virtual {p0, v0, v1, p1, p2}, Lorg/icepdf/index/java/awt/Rectangle;->setBounds(IIII)V

    .line 138
    return-void
.end method

.method public setBounds(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 159
    iput p1, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    .line 160
    iput p2, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    .line 161
    iput p4, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    .line 162
    iput p3, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    .line 163
    return-void
.end method

.method public setBounds(Lorg/icepdf/index/java/awt/Rectangle;)V
    .locals 4
    .param p1, "r"    # Lorg/icepdf/index/java/awt/Rectangle;

    .prologue
    .line 166
    iget v0, p1, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    iget v2, p1, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    iget v3, p1, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/icepdf/index/java/awt/Rectangle;->setBounds(IIII)V

    .line 167
    return-void
.end method

.method public setLocation(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 107
    iput p1, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    .line 108
    iput p2, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    .line 109
    return-void
.end method

.method public setLocation(Lorg/icepdf/index/java/awt/Point;)V
    .locals 2
    .param p1, "p"    # Lorg/icepdf/index/java/awt/Point;

    .prologue
    .line 112
    iget v0, p1, Lorg/icepdf/index/java/awt/Point;->x:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Point;->y:I

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/Rectangle;->setLocation(II)V

    .line 113
    return-void
.end method

.method public setRect(DDDD)V
    .locals 7
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 125
    invoke-static {p1, p2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 126
    .local v0, "x1":I
    invoke-static {p3, p4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 127
    .local v2, "y1":I
    add-double v4, p1, p5

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v1, v4

    .line 128
    .local v1, "x2":I
    add-double v4, p3, p7

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 129
    .local v3, "y2":I
    sub-int v4, v1, v0

    sub-int v5, v3, v2

    invoke-virtual {p0, v0, v2, v4, v5}, Lorg/icepdf/index/java/awt/Rectangle;->setBounds(IIII)V

    .line 130
    return-void
.end method

.method public setSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 94
    iput p1, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    .line 95
    iput p2, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    .line 96
    return-void
.end method

.method public setSize(Lorg/icepdf/index/java/awt/Dimension;)V
    .locals 2
    .param p1, "d"    # Lorg/icepdf/index/java/awt/Dimension;

    .prologue
    .line 99
    iget v0, p1, Lorg/icepdf/index/java/awt/Dimension;->width:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Dimension;->height:I

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/Rectangle;->setSize(II)V

    .line 100
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 314
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "[x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->width:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/Rectangle;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public translate(II)V
    .locals 1
    .param p1, "mx"    # I
    .param p2, "my"    # I

    .prologue
    .line 177
    iget v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->x:I

    .line 178
    iget v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    add-int/2addr v0, p2

    iput v0, p0, Lorg/icepdf/index/java/awt/Rectangle;->y:I

    .line 179
    return-void
.end method

.method public union(Lorg/icepdf/index/java/awt/Rectangle;)Lorg/icepdf/index/java/awt/Rectangle;
    .locals 1
    .param p1, "r"    # Lorg/icepdf/index/java/awt/Rectangle;

    .prologue
    .line 293
    new-instance v0, Lorg/icepdf/index/java/awt/Rectangle;

    invoke-direct {v0, p0}, Lorg/icepdf/index/java/awt/Rectangle;-><init>(Lorg/icepdf/index/java/awt/Rectangle;)V

    .line 294
    .local v0, "dst":Lorg/icepdf/index/java/awt/Rectangle;
    invoke-virtual {v0, p1}, Lorg/icepdf/index/java/awt/Rectangle;->add(Lorg/icepdf/index/java/awt/Rectangle;)V

    .line 295
    return-object v0
.end method
