.class public Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;
.super Lorg/icepdf/index/java/awt/geom/CubicCurve2D;
.source "CubicCurve2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/CubicCurve2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Double"
.end annotation


# instance fields
.field public ctrlx1:D

.field public ctrlx2:D

.field public ctrly1:D

.field public ctrly2:D

.field public x1:D

.field public x2:D

.field public y1:D

.field public y2:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;-><init>()V

    .line 160
    return-void
.end method

.method public constructor <init>(DDDDDDDD)V
    .locals 1
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "ctrlx1"    # D
    .param p7, "ctrly1"    # D
    .param p9, "ctrlx2"    # D
    .param p11, "ctrly2"    # D
    .param p13, "x2"    # D
    .param p15, "y2"    # D

    .prologue
    .line 163
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;-><init>()V

    .line 164
    invoke-virtual/range {p0 .. p16}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->setCurve(DDDDDDDD)V

    .line 165
    return-void
.end method


# virtual methods
.method public getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 14

    .prologue
    .line 242
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->x1:D

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->x2:D

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrlx1:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrlx2:D

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 243
    .local v2, "rx1":D
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->y1:D

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->y2:D

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrly1:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrly2:D

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    .line 244
    .local v4, "ry1":D
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->x1:D

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->x2:D

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrlx1:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrlx2:D

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    .line 245
    .local v10, "rx2":D
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->y1:D

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->y2:D

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iget-wide v6, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrly1:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrly2:D

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v12

    .line 246
    .local v12, "ry2":D
    new-instance v1, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    sub-double v6, v10, v2

    sub-double v8, v12, v4

    invoke-direct/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    return-object v1
.end method

.method public getCtrlP1()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 6

    .prologue
    .line 214
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrlx1:D

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrly1:D

    invoke-direct {v0, v2, v3, v4, v5}, Lorg/icepdf/index/java/awt/geom/Point2D$Double;-><init>(DD)V

    return-object v0
.end method

.method public getCtrlP2()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 6

    .prologue
    .line 219
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrlx2:D

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrly2:D

    invoke-direct {v0, v2, v3, v4, v5}, Lorg/icepdf/index/java/awt/geom/Point2D$Double;-><init>(DD)V

    return-object v0
.end method

.method public getCtrlX1()D
    .locals 2

    .prologue
    .line 179
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrlx1:D

    return-wide v0
.end method

.method public getCtrlX2()D
    .locals 2

    .prologue
    .line 189
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrlx2:D

    return-wide v0
.end method

.method public getCtrlY1()D
    .locals 2

    .prologue
    .line 184
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrly1:D

    return-wide v0
.end method

.method public getCtrlY2()D
    .locals 2

    .prologue
    .line 194
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrly2:D

    return-wide v0
.end method

.method public getP1()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 6

    .prologue
    .line 209
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->x1:D

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->y1:D

    invoke-direct {v0, v2, v3, v4, v5}, Lorg/icepdf/index/java/awt/geom/Point2D$Double;-><init>(DD)V

    return-object v0
.end method

.method public getP2()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 6

    .prologue
    .line 224
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Double;

    iget-wide v2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->x2:D

    iget-wide v4, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->y2:D

    invoke-direct {v0, v2, v3, v4, v5}, Lorg/icepdf/index/java/awt/geom/Point2D$Double;-><init>(DD)V

    return-object v0
.end method

.method public getX1()D
    .locals 2

    .prologue
    .line 169
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->x1:D

    return-wide v0
.end method

.method public getX2()D
    .locals 2

    .prologue
    .line 199
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->x2:D

    return-wide v0
.end method

.method public getY1()D
    .locals 2

    .prologue
    .line 174
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->y1:D

    return-wide v0
.end method

.method public getY2()D
    .locals 2

    .prologue
    .line 204
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->y2:D

    return-wide v0
.end method

.method public setCurve(DDDDDDDD)V
    .locals 3
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "ctrlx1"    # D
    .param p7, "ctrly1"    # D
    .param p9, "ctrlx2"    # D
    .param p11, "ctrly2"    # D
    .param p13, "x2"    # D
    .param p15, "y2"    # D

    .prologue
    .line 231
    iput-wide p1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->x1:D

    .line 232
    iput-wide p3, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->y1:D

    .line 233
    iput-wide p5, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrlx1:D

    .line 234
    iput-wide p7, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrly1:D

    .line 235
    iput-wide p9, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrlx2:D

    .line 236
    iput-wide p11, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->ctrly2:D

    .line 237
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->x2:D

    .line 238
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;->y2:D

    .line 239
    return-void
.end method
