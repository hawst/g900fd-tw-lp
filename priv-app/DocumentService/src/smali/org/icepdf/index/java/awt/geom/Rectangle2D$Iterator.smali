.class Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;
.super Ljava/lang/Object;
.source "Rectangle2D.java"

# interfaces
.implements Lorg/icepdf/index/java/awt/geom/PathIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/Rectangle2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Iterator"
.end annotation


# instance fields
.field height:D

.field index:I

.field t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

.field width:D

.field x:D

.field y:D


# direct methods
.method constructor <init>(Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/AffineTransform;)V
    .locals 4
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .param p2, "at"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    const-wide/16 v2, 0x0

    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->x:D

    .line 312
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->y:D

    .line 313
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->width:D

    .line 314
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->height:D

    .line 315
    iput-object p2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .line 316
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->width:D

    cmpg-double v0, v0, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->height:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 317
    :cond_0
    const/4 v0, 0x6

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->index:I

    .line 319
    :cond_1
    return-void
.end method


# virtual methods
.method public currentSegment([D)I
    .locals 10
    .param p1, "coords"    # [D

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 334
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string/jumbo v1, "awt.4B"

    invoke-static {v1}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 337
    :cond_0
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->index:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 338
    const/4 v6, 0x4

    .line 369
    :cond_1
    :goto_0
    return v6

    .line 341
    :cond_2
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->index:I

    if-nez v0, :cond_3

    .line 342
    const/4 v6, 0x0

    .line 343
    .local v6, "type":I
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->x:D

    aput-wide v0, p1, v2

    .line 344
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->y:D

    aput-wide v0, p1, v5

    .line 366
    :goto_1
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    if-eqz v0, :cond_1

    .line 367
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-object v1, p1

    move-object v3, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->transform([DI[DII)V

    goto :goto_0

    .line 346
    .end local v6    # "type":I
    :cond_3
    const/4 v6, 0x1

    .line 347
    .restart local v6    # "type":I
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->index:I

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 349
    :pswitch_0
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->x:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->width:D

    add-double/2addr v0, v8

    aput-wide v0, p1, v2

    .line 350
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->y:D

    aput-wide v0, p1, v5

    goto :goto_1

    .line 353
    :pswitch_1
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->x:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->width:D

    add-double/2addr v0, v8

    aput-wide v0, p1, v2

    .line 354
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->y:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->height:D

    add-double/2addr v0, v8

    aput-wide v0, p1, v5

    goto :goto_1

    .line 357
    :pswitch_2
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->x:D

    aput-wide v0, p1, v2

    .line 358
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->y:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->height:D

    add-double/2addr v0, v8

    aput-wide v0, p1, v5

    goto :goto_1

    .line 361
    :pswitch_3
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->x:D

    aput-wide v0, p1, v2

    .line 362
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->y:D

    aput-wide v0, p1, v5

    goto :goto_1

    .line 347
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public currentSegment([F)I
    .locals 10
    .param p1, "coords"    # [F

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 373
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string/jumbo v1, "awt.4B"

    invoke-static {v1}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 376
    :cond_0
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->index:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 377
    const/4 v6, 0x4

    .line 408
    :cond_1
    :goto_0
    return v6

    .line 380
    :cond_2
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->index:I

    if-nez v0, :cond_3

    .line 381
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->x:D

    double-to-float v0, v0

    aput v0, p1, v2

    .line 382
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->y:D

    double-to-float v0, v0

    aput v0, p1, v5

    .line 383
    const/4 v6, 0x0

    .line 405
    .local v6, "type":I
    :goto_1
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    if-eqz v0, :cond_1

    .line 406
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-object v1, p1

    move-object v3, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->transform([FI[FII)V

    goto :goto_0

    .line 385
    .end local v6    # "type":I
    :cond_3
    const/4 v6, 0x1

    .line 386
    .restart local v6    # "type":I
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->index:I

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 388
    :pswitch_0
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->x:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->width:D

    add-double/2addr v0, v8

    double-to-float v0, v0

    aput v0, p1, v2

    .line 389
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->y:D

    double-to-float v0, v0

    aput v0, p1, v5

    goto :goto_1

    .line 392
    :pswitch_1
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->x:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->width:D

    add-double/2addr v0, v8

    double-to-float v0, v0

    aput v0, p1, v2

    .line 393
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->y:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->height:D

    add-double/2addr v0, v8

    double-to-float v0, v0

    aput v0, p1, v5

    goto :goto_1

    .line 396
    :pswitch_2
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->x:D

    double-to-float v0, v0

    aput v0, p1, v2

    .line 397
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->y:D

    iget-wide v8, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->height:D

    add-double/2addr v0, v8

    double-to-float v0, v0

    aput v0, p1, v5

    goto :goto_1

    .line 400
    :pswitch_3
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->x:D

    double-to-float v0, v0

    aput v0, p1, v2

    .line 401
    iget-wide v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->y:D

    double-to-float v0, v0

    aput v0, p1, v5

    goto :goto_1

    .line 386
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getWindingRule()I
    .locals 1

    .prologue
    .line 322
    const/4 v0, 0x1

    return v0
.end method

.method public isDone()Z
    .locals 2

    .prologue
    .line 326
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->index:I

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()V
    .locals 1

    .prologue
    .line 330
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Iterator;->index:I

    .line 331
    return-void
.end method
