.class public Lorg/icepdf/index/java/awt/geom/Line2D$Float;
.super Lorg/icepdf/index/java/awt/geom/Line2D;
.source "Line2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/Line2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Float"
.end annotation


# instance fields
.field public x1:F

.field public x2:F

.field public y1:F

.field public y2:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;-><init>()V

    .line 39
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "x2"    # F
    .param p4, "y2"    # F

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;-><init>()V

    .line 42
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->setLine(FFFF)V

    .line 43
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;)V
    .locals 0
    .param p1, "p1"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "p2"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Line2D;-><init>()V

    .line 46
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->setLine(Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;)V

    .line 47
    return-void
.end method


# virtual methods
.method public getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 6

    .prologue
    .line 96
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x1:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x2:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    .line 97
    iget v2, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x1:F

    .line 98
    .local v2, "rx":F
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x2:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x1:F

    sub-float v1, v4, v5

    .line 103
    .local v1, "rw":F
    :goto_0
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y1:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y2:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    .line 104
    iget v3, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y1:F

    .line 105
    .local v3, "ry":F
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y2:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y1:F

    sub-float v0, v4, v5

    .line 110
    .local v0, "rh":F
    :goto_1
    new-instance v4, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-direct {v4, v2, v3, v1, v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    return-object v4

    .line 100
    .end local v0    # "rh":F
    .end local v1    # "rw":F
    .end local v2    # "rx":F
    .end local v3    # "ry":F
    :cond_0
    iget v2, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x2:F

    .line 101
    .restart local v2    # "rx":F
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x1:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x2:F

    sub-float v1, v4, v5

    .restart local v1    # "rw":F
    goto :goto_0

    .line 107
    :cond_1
    iget v3, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y2:F

    .line 108
    .restart local v3    # "ry":F
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y1:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y2:F

    sub-float v0, v4, v5

    .restart local v0    # "rh":F
    goto :goto_1
.end method

.method public getP1()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 3

    .prologue
    .line 71
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x1:F

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y1:F

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>(FF)V

    return-object v0
.end method

.method public getP2()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 3

    .prologue
    .line 76
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x2:F

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y2:F

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>(FF)V

    return-object v0
.end method

.method public getX1()D
    .locals 2

    .prologue
    .line 51
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x1:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getX2()D
    .locals 2

    .prologue
    .line 61
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x2:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getY1()D
    .locals 2

    .prologue
    .line 56
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y1:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getY2()D
    .locals 2

    .prologue
    .line 66
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y2:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public setLine(DDDD)V
    .locals 1
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "x2"    # D
    .param p7, "y2"    # D

    .prologue
    .line 81
    double-to-float v0, p1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x1:F

    .line 82
    double-to-float v0, p3

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y1:F

    .line 83
    double-to-float v0, p5

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x2:F

    .line 84
    double-to-float v0, p7

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y2:F

    .line 85
    return-void
.end method

.method public setLine(FFFF)V
    .locals 0
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "x2"    # F
    .param p4, "y2"    # F

    .prologue
    .line 88
    iput p1, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x1:F

    .line 89
    iput p2, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y1:F

    .line 90
    iput p3, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->x2:F

    .line 91
    iput p4, p0, Lorg/icepdf/index/java/awt/geom/Line2D$Float;->y2:F

    .line 92
    return-void
.end method
