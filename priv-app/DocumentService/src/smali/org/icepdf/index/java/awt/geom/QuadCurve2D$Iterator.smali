.class Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;
.super Ljava/lang/Object;
.source "QuadCurve2D.java"

# interfaces
.implements Lorg/icepdf/index/java/awt/geom/PathIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/QuadCurve2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Iterator"
.end annotation


# instance fields
.field c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

.field index:I

.field t:Lorg/icepdf/index/java/awt/geom/AffineTransform;


# direct methods
.method constructor <init>(Lorg/icepdf/index/java/awt/geom/QuadCurve2D;Lorg/icepdf/index/java/awt/geom/AffineTransform;)V
    .locals 0
    .param p1, "q"    # Lorg/icepdf/index/java/awt/geom/QuadCurve2D;
    .param p2, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    iput-object p1, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    .line 228
    iput-object p2, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .line 229
    return-void
.end method


# virtual methods
.method public currentSegment([D)I
    .locals 10
    .param p1, "coords"    # [D

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 244
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string/jumbo v1, "awt.4B"

    invoke-static {v1}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :cond_0
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->index:I

    if-nez v0, :cond_2

    .line 251
    const/4 v6, 0x0

    .line 252
    .local v6, "type":I
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getX1()D

    move-result-wide v0

    aput-wide v0, p1, v2

    .line 253
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getY1()D

    move-result-wide v0

    aput-wide v0, p1, v3

    .line 254
    const/4 v5, 0x1

    .line 263
    .local v5, "count":I
    :goto_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    if-eqz v0, :cond_1

    .line 264
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-object v1, p1

    move-object v3, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->transform([DI[DII)V

    .line 266
    :cond_1
    return v6

    .line 256
    .end local v5    # "count":I
    .end local v6    # "type":I
    :cond_2
    const/4 v6, 0x2

    .line 257
    .restart local v6    # "type":I
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getCtrlX()D

    move-result-wide v0

    aput-wide v0, p1, v2

    .line 258
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getCtrlY()D

    move-result-wide v0

    aput-wide v0, p1, v3

    .line 259
    const/4 v0, 0x2

    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    invoke-virtual {v1}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getX2()D

    move-result-wide v8

    aput-wide v8, p1, v0

    .line 260
    const/4 v0, 0x3

    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    invoke-virtual {v1}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getY2()D

    move-result-wide v8

    aput-wide v8, p1, v0

    .line 261
    const/4 v5, 0x2

    .restart local v5    # "count":I
    goto :goto_0
.end method

.method public currentSegment([F)I
    .locals 10
    .param p1, "coords"    # [F

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 270
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string/jumbo v1, "awt.4B"

    invoke-static {v1}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 276
    :cond_0
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->index:I

    if-nez v0, :cond_2

    .line 277
    const/4 v6, 0x0

    .line 278
    .local v6, "type":I
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getX1()D

    move-result-wide v0

    double-to-float v0, v0

    aput v0, p1, v2

    .line 279
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getY1()D

    move-result-wide v0

    double-to-float v0, v0

    aput v0, p1, v3

    .line 280
    const/4 v5, 0x1

    .line 289
    .local v5, "count":I
    :goto_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    if-eqz v0, :cond_1

    .line 290
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-object v1, p1

    move-object v3, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->transform([FI[FII)V

    .line 292
    :cond_1
    return v6

    .line 282
    .end local v5    # "count":I
    .end local v6    # "type":I
    :cond_2
    const/4 v6, 0x2

    .line 283
    .restart local v6    # "type":I
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getCtrlX()D

    move-result-wide v0

    double-to-float v0, v0

    aput v0, p1, v2

    .line 284
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getCtrlY()D

    move-result-wide v0

    double-to-float v0, v0

    aput v0, p1, v3

    .line 285
    const/4 v0, 0x2

    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    invoke-virtual {v1}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getX2()D

    move-result-wide v8

    double-to-float v1, v8

    aput v1, p1, v0

    .line 286
    const/4 v0, 0x3

    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/QuadCurve2D;

    invoke-virtual {v1}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;->getY2()D

    move-result-wide v8

    double-to-float v1, v8

    aput v1, p1, v0

    .line 287
    const/4 v5, 0x2

    .restart local v5    # "count":I
    goto :goto_0
.end method

.method public getWindingRule()I
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x1

    return v0
.end method

.method public isDone()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 236
    iget v1, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->index:I

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()V
    .locals 1

    .prologue
    .line 240
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Iterator;->index:I

    .line 241
    return-void
.end method
