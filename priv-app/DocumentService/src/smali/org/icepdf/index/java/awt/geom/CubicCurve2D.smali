.class public abstract Lorg/icepdf/index/java/awt/geom/CubicCurve2D;
.super Ljava/lang/Object;
.source "CubicCurve2D.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/icepdf/index/java/awt/Shape;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;,
        Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Double;,
        Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Float;
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349
    return-void
.end method

.method public static getFlatness(DDDDDDDD)D
    .locals 2
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "ctrlx1"    # D
    .param p6, "ctrly1"    # D
    .param p8, "ctrlx2"    # D
    .param p10, "ctrly2"    # D
    .param p12, "x2"    # D
    .param p14, "y2"    # D

    .prologue
    .line 445
    invoke-static/range {p0 .. p15}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getFlatnessSq(DDDDDDDD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static getFlatness([DI)D
    .locals 16
    .param p0, "coords"    # [D
    .param p1, "offset"    # I

    .prologue
    .line 449
    add-int/lit8 v0, p1, 0x0

    aget-wide v0, p0, v0

    add-int/lit8 v2, p1, 0x1

    aget-wide v2, p0, v2

    add-int/lit8 v4, p1, 0x2

    aget-wide v4, p0, v4

    add-int/lit8 v6, p1, 0x3

    aget-wide v6, p0, v6

    add-int/lit8 v8, p1, 0x4

    aget-wide v8, p0, v8

    add-int/lit8 v10, p1, 0x5

    aget-wide v10, p0, v10

    add-int/lit8 v12, p1, 0x6

    aget-wide v12, p0, v12

    add-int/lit8 v14, p1, 0x7

    aget-wide v14, p0, v14

    invoke-static/range {v0 .. v15}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getFlatness(DDDDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static getFlatnessSq(DDDDDDDD)D
    .locals 14
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "ctrlx1"    # D
    .param p6, "ctrly1"    # D
    .param p8, "ctrlx2"    # D
    .param p10, "ctrly2"    # D
    .param p12, "x2"    # D
    .param p14, "y2"    # D

    .prologue
    .line 421
    move-wide v0, p0

    move-wide/from16 v2, p2

    move-wide/from16 v4, p12

    move-wide/from16 v6, p14

    move-wide/from16 v8, p4

    move-wide/from16 v10, p6

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptSegDistSq(DDDDDD)D

    move-result-wide v12

    move-wide v0, p0

    move-wide/from16 v2, p2

    move-wide/from16 v4, p12

    move-wide/from16 v6, p14

    move-wide/from16 v8, p8

    move-wide/from16 v10, p10

    invoke-static/range {v0 .. v11}, Lorg/icepdf/index/java/awt/geom/Line2D;->ptSegDistSq(DDDDDD)D

    move-result-wide v0

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static getFlatnessSq([DI)D
    .locals 16
    .param p0, "coords"    # [D
    .param p1, "offset"    # I

    .prologue
    .line 427
    add-int/lit8 v0, p1, 0x0

    aget-wide v0, p0, v0

    add-int/lit8 v2, p1, 0x1

    aget-wide v2, p0, v2

    add-int/lit8 v4, p1, 0x2

    aget-wide v4, p0, v4

    add-int/lit8 v6, p1, 0x3

    aget-wide v6, p0, v6

    add-int/lit8 v8, p1, 0x4

    aget-wide v8, p0, v8

    add-int/lit8 v10, p1, 0x5

    aget-wide v10, p0, v10

    add-int/lit8 v12, p1, 0x6

    aget-wide v12, p0, v12

    add-int/lit8 v14, p1, 0x7

    aget-wide v14, p0, v14

    invoke-static/range {v0 .. v15}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getFlatnessSq(DDDDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static solveCubic([D)I
    .locals 1
    .param p0, "eqn"    # [D

    .prologue
    .line 533
    invoke-static {p0, p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->solveCubic([D[D)I

    move-result v0

    return v0
.end method

.method public static solveCubic([D[D)I
    .locals 1
    .param p0, "eqn"    # [D
    .param p1, "res"    # [D

    .prologue
    .line 537
    invoke-static {p0, p1}, Lorg/apache/harmony/awt/gl/Crossing;->solveCubic([D[D)I

    move-result v0

    return v0
.end method

.method public static subdivide(Lorg/icepdf/index/java/awt/geom/CubicCurve2D;Lorg/icepdf/index/java/awt/geom/CubicCurve2D;Lorg/icepdf/index/java/awt/geom/CubicCurve2D;)V
    .locals 36
    .param p0, "src"    # Lorg/icepdf/index/java/awt/geom/CubicCurve2D;
    .param p1, "left"    # Lorg/icepdf/index/java/awt/geom/CubicCurve2D;
    .param p2, "right"    # Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    .prologue
    .line 461
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getX1()D

    move-result-wide v2

    .line 462
    .local v2, "x1":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getY1()D

    move-result-wide v4

    .line 463
    .local v4, "y1":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlX1()D

    move-result-wide v6

    .line 464
    .local v6, "cx1":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlY1()D

    move-result-wide v8

    .line 465
    .local v8, "cy1":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlX2()D

    move-result-wide v28

    .line 466
    .local v28, "cx2":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlY2()D

    move-result-wide v30

    .line 467
    .local v30, "cy2":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getX2()D

    move-result-wide v32

    .line 468
    .local v32, "x2":D
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getY2()D

    move-result-wide v34

    .line 469
    .local v34, "y2":D
    add-double v0, v6, v28

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    div-double v14, v0, v18

    .line 470
    .local v14, "cx":D
    add-double v0, v8, v30

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    div-double v16, v0, v18

    .line 471
    .local v16, "cy":D
    add-double v0, v2, v6

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    div-double v6, v0, v18

    .line 472
    add-double v0, v4, v8

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    div-double v8, v0, v18

    .line 473
    add-double v0, v32, v28

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    div-double v28, v0, v18

    .line 474
    add-double v0, v34, v30

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    div-double v30, v0, v18

    .line 475
    add-double v0, v6, v14

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    div-double v10, v0, v18

    .line 476
    .local v10, "ax":D
    add-double v0, v8, v16

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    div-double v12, v0, v18

    .line 477
    .local v12, "ay":D
    add-double v0, v28, v14

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    div-double v24, v0, v18

    .line 478
    .local v24, "bx":D
    add-double v0, v30, v16

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    div-double v26, v0, v18

    .line 479
    .local v26, "by":D
    add-double v0, v10, v24

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    div-double v14, v0, v18

    .line 480
    add-double v0, v12, v26

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    div-double v16, v0, v18

    .line 481
    if-eqz p1, :cond_0

    move-object/from16 v1, p1

    .line 482
    invoke-virtual/range {v1 .. v17}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->setCurve(DDDDDDDD)V

    .line 484
    :cond_0
    if-eqz p2, :cond_1

    move-object/from16 v19, p2

    move-wide/from16 v20, v14

    move-wide/from16 v22, v16

    .line 485
    invoke-virtual/range {v19 .. v35}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->setCurve(DDDDDDDD)V

    .line 487
    :cond_1
    return-void
.end method

.method public static subdivide([DI[DI[DI)V
    .locals 32
    .param p0, "src"    # [D
    .param p1, "srcOff"    # I
    .param p2, "left"    # [D
    .param p3, "leftOff"    # I
    .param p4, "right"    # [D
    .param p5, "rightOff"    # I

    .prologue
    .line 490
    add-int/lit8 v28, p1, 0x0

    aget-wide v20, p0, v28

    .line 491
    .local v20, "x1":D
    add-int/lit8 v28, p1, 0x1

    aget-wide v24, p0, v28

    .line 492
    .local v24, "y1":D
    add-int/lit8 v28, p1, 0x2

    aget-wide v10, p0, v28

    .line 493
    .local v10, "cx1":D
    add-int/lit8 v28, p1, 0x3

    aget-wide v16, p0, v28

    .line 494
    .local v16, "cy1":D
    add-int/lit8 v28, p1, 0x4

    aget-wide v12, p0, v28

    .line 495
    .local v12, "cx2":D
    add-int/lit8 v28, p1, 0x5

    aget-wide v18, p0, v28

    .line 496
    .local v18, "cy2":D
    add-int/lit8 v28, p1, 0x6

    aget-wide v22, p0, v28

    .line 497
    .local v22, "x2":D
    add-int/lit8 v28, p1, 0x7

    aget-wide v26, p0, v28

    .line 498
    .local v26, "y2":D
    add-double v28, v10, v12

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v8, v28, v30

    .line 499
    .local v8, "cx":D
    add-double v28, v16, v18

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v14, v28, v30

    .line 500
    .local v14, "cy":D
    add-double v28, v20, v10

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v10, v28, v30

    .line 501
    add-double v28, v24, v16

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v16, v28, v30

    .line 502
    add-double v28, v22, v12

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v12, v28, v30

    .line 503
    add-double v28, v26, v18

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v18, v28, v30

    .line 504
    add-double v28, v10, v8

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v0, v28, v30

    .line 505
    .local v0, "ax":D
    add-double v28, v16, v14

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v2, v28, v30

    .line 506
    .local v2, "ay":D
    add-double v28, v12, v8

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v4, v28, v30

    .line 507
    .local v4, "bx":D
    add-double v28, v18, v14

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v6, v28, v30

    .line 508
    .local v6, "by":D
    add-double v28, v0, v4

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v8, v28, v30

    .line 509
    add-double v28, v2, v6

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v14, v28, v30

    .line 510
    if-eqz p2, :cond_0

    .line 511
    add-int/lit8 v28, p3, 0x0

    aput-wide v20, p2, v28

    .line 512
    add-int/lit8 v28, p3, 0x1

    aput-wide v24, p2, v28

    .line 513
    add-int/lit8 v28, p3, 0x2

    aput-wide v10, p2, v28

    .line 514
    add-int/lit8 v28, p3, 0x3

    aput-wide v16, p2, v28

    .line 515
    add-int/lit8 v28, p3, 0x4

    aput-wide v0, p2, v28

    .line 516
    add-int/lit8 v28, p3, 0x5

    aput-wide v2, p2, v28

    .line 517
    add-int/lit8 v28, p3, 0x6

    aput-wide v8, p2, v28

    .line 518
    add-int/lit8 v28, p3, 0x7

    aput-wide v14, p2, v28

    .line 520
    :cond_0
    if-eqz p4, :cond_1

    .line 521
    add-int/lit8 v28, p5, 0x0

    aput-wide v8, p4, v28

    .line 522
    add-int/lit8 v28, p5, 0x1

    aput-wide v14, p4, v28

    .line 523
    add-int/lit8 v28, p5, 0x2

    aput-wide v4, p4, v28

    .line 524
    add-int/lit8 v28, p5, 0x3

    aput-wide v6, p4, v28

    .line 525
    add-int/lit8 v28, p5, 0x4

    aput-wide v12, p4, v28

    .line 526
    add-int/lit8 v28, p5, 0x5

    aput-wide v18, p4, v28

    .line 527
    add-int/lit8 v28, p5, 0x6

    aput-wide v22, p4, v28

    .line 528
    add-int/lit8 v28, p5, 0x7

    aput-wide v26, p4, v28

    .line 530
    :cond_1
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 581
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 582
    :catch_0
    move-exception v0

    .line 583
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public contains(DD)Z
    .locals 1
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    .line 541
    invoke-static {p0, p1, p2, p3, p4}, Lorg/apache/harmony/awt/gl/Crossing;->crossShape(Lorg/icepdf/index/java/awt/Shape;DD)I

    move-result v0

    invoke-static {v0}, Lorg/apache/harmony/awt/gl/Crossing;->isInsideEvenOdd(I)Z

    move-result v0

    return v0
.end method

.method public contains(DDDD)Z
    .locals 3
    .param p1, "rx"    # D
    .param p3, "ry"    # D
    .param p5, "rw"    # D
    .param p7, "rh"    # D

    .prologue
    .line 545
    invoke-static/range {p0 .. p8}, Lorg/apache/harmony/awt/gl/Crossing;->intersectShape(Lorg/icepdf/index/java/awt/Shape;DDDD)I

    move-result v0

    .line 546
    .local v0, "cross":I
    const/16 v1, 0xff

    if-eq v0, v1, :cond_0

    invoke-static {v0}, Lorg/apache/harmony/awt/gl/Crossing;->isInsideEvenOdd(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public contains(Lorg/icepdf/index/java/awt/geom/Point2D;)Z
    .locals 4
    .param p1, "p"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 555
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->contains(DD)Z

    move-result v0

    return v0
.end method

.method public contains(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
    .locals 10
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 563
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->contains(DDDD)Z

    move-result v0

    return v0
.end method

.method public getBounds()Lorg/icepdf/index/java/awt/Rectangle;
    .locals 1

    .prologue
    .line 567
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    move-result-object v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getBounds()Lorg/icepdf/index/java/awt/Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public abstract getCtrlP1()Lorg/icepdf/index/java/awt/geom/Point2D;
.end method

.method public abstract getCtrlP2()Lorg/icepdf/index/java/awt/geom/Point2D;
.end method

.method public abstract getCtrlX1()D
.end method

.method public abstract getCtrlX2()D
.end method

.method public abstract getCtrlY1()D
.end method

.method public abstract getCtrlY2()D
.end method

.method public getFlatness()D
    .locals 16

    .prologue
    .line 435
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getX1()D

    move-result-wide v0

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getY1()D

    move-result-wide v2

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlX1()D

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlY1()D

    move-result-wide v6

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlX2()D

    move-result-wide v8

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlY2()D

    move-result-wide v10

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getX2()D

    move-result-wide v12

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getY2()D

    move-result-wide v14

    invoke-static/range {v0 .. v15}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getFlatness(DDDDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFlatnessSq()D
    .locals 16

    .prologue
    .line 411
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getX1()D

    move-result-wide v0

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getY1()D

    move-result-wide v2

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlX1()D

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlY1()D

    move-result-wide v6

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlX2()D

    move-result-wide v8

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlY2()D

    move-result-wide v10

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getX2()D

    move-result-wide v12

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getY2()D

    move-result-wide v14

    invoke-static/range {v0 .. v15}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getFlatnessSq(DDDDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public abstract getP1()Lorg/icepdf/index/java/awt/geom/Point2D;
.end method

.method public abstract getP2()Lorg/icepdf/index/java/awt/geom/Point2D;
.end method

.method public getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;
    .locals 1
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 571
    new-instance v0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;

    invoke-direct {v0, p0, p1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;-><init>(Lorg/icepdf/index/java/awt/geom/CubicCurve2D;Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    return-object v0
.end method

.method public getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;D)Lorg/icepdf/index/java/awt/geom/PathIterator;
    .locals 2
    .param p1, "at"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .param p2, "flatness"    # D

    .prologue
    .line 575
    new-instance v0, Lorg/icepdf/index/java/awt/geom/FlatteningPathIterator;

    invoke-virtual {p0, p1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;

    move-result-object v1

    invoke-direct {v0, v1, p2, p3}, Lorg/icepdf/index/java/awt/geom/FlatteningPathIterator;-><init>(Lorg/icepdf/index/java/awt/geom/PathIterator;D)V

    return-object v0
.end method

.method public abstract getX1()D
.end method

.method public abstract getX2()D
.end method

.method public abstract getY1()D
.end method

.method public abstract getY2()D
.end method

.method public intersects(DDDD)Z
    .locals 3
    .param p1, "rx"    # D
    .param p3, "ry"    # D
    .param p5, "rw"    # D
    .param p7, "rh"    # D

    .prologue
    .line 550
    invoke-static/range {p0 .. p8}, Lorg/apache/harmony/awt/gl/Crossing;->intersectShape(Lorg/icepdf/index/java/awt/Shape;DDDD)I

    move-result v0

    .line 551
    .local v0, "cross":I
    const/16 v1, 0xff

    if-eq v0, v1, :cond_0

    invoke-static {v0}, Lorg/apache/harmony/awt/gl/Crossing;->isInsideEvenOdd(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public intersects(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
    .locals 10
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 559
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->intersects(DDDD)Z

    move-result v0

    return v0
.end method

.method public abstract setCurve(DDDDDDDD)V
.end method

.method public setCurve(Lorg/icepdf/index/java/awt/geom/CubicCurve2D;)V
    .locals 18
    .param p1, "curve"    # Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    .prologue
    .line 403
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getX1()D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getY1()D

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlX1()D

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlY1()D

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlX2()D

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlY2()D

    move-result-wide v12

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getX2()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getY2()D

    move-result-wide v16

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v17}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->setCurve(DDDDDDDD)V

    .line 408
    return-void
.end method

.method public setCurve(Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;)V
    .locals 18
    .param p1, "p1"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "cp1"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p3, "cp2"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p4, "p2"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 379
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v4

    invoke-virtual/range {p2 .. p2}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v6

    invoke-virtual/range {p2 .. p2}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v8

    invoke-virtual/range {p3 .. p3}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v10

    invoke-virtual/range {p3 .. p3}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v12

    invoke-virtual/range {p4 .. p4}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v14

    invoke-virtual/range {p4 .. p4}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v16

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v17}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->setCurve(DDDDDDDD)V

    .line 384
    return-void
.end method

.method public setCurve([DI)V
    .locals 18
    .param p1, "coords"    # [D
    .param p2, "offset"    # I

    .prologue
    .line 387
    add-int/lit8 v0, p2, 0x0

    aget-wide v2, p1, v0

    add-int/lit8 v0, p2, 0x1

    aget-wide v4, p1, v0

    add-int/lit8 v0, p2, 0x2

    aget-wide v6, p1, v0

    add-int/lit8 v0, p2, 0x3

    aget-wide v8, p1, v0

    add-int/lit8 v0, p2, 0x4

    aget-wide v10, p1, v0

    add-int/lit8 v0, p2, 0x5

    aget-wide v12, p1, v0

    add-int/lit8 v0, p2, 0x6

    aget-wide v14, p1, v0

    add-int/lit8 v0, p2, 0x7

    aget-wide v16, p1, v0

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v17}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->setCurve(DDDDDDDD)V

    .line 392
    return-void
.end method

.method public setCurve([Lorg/icepdf/index/java/awt/geom/Point2D;I)V
    .locals 18
    .param p1, "points"    # [Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "offset"    # I

    .prologue
    .line 395
    add-int/lit8 v0, p2, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v2

    add-int/lit8 v0, p2, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v4

    add-int/lit8 v0, p2, 0x1

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v6

    add-int/lit8 v0, p2, 0x1

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v8

    add-int/lit8 v0, p2, 0x2

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v10

    add-int/lit8 v0, p2, 0x2

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v12

    add-int/lit8 v0, p2, 0x3

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v14

    add-int/lit8 v0, p2, 0x3

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v16

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v17}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->setCurve(DDDDDDDD)V

    .line 400
    return-void
.end method

.method public subdivide(Lorg/icepdf/index/java/awt/geom/CubicCurve2D;Lorg/icepdf/index/java/awt/geom/CubicCurve2D;)V
    .locals 0
    .param p1, "left"    # Lorg/icepdf/index/java/awt/geom/CubicCurve2D;
    .param p2, "right"    # Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    .prologue
    .line 457
    invoke-static {p0, p1, p2}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->subdivide(Lorg/icepdf/index/java/awt/geom/CubicCurve2D;Lorg/icepdf/index/java/awt/geom/CubicCurve2D;Lorg/icepdf/index/java/awt/geom/CubicCurve2D;)V

    .line 458
    return-void
.end method
