.class public abstract Lorg/icepdf/index/java/awt/geom/RectangularShape;
.super Ljava/lang/Object;
.source "RectangularShape.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/icepdf/index/java/awt/Shape;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 138
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public contains(Lorg/icepdf/index/java/awt/geom/Point2D;)Z
    .locals 4
    .param p1, "point"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 112
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->contains(DD)Z

    move-result v0

    return v0
.end method

.method public contains(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
    .locals 10
    .param p1, "rect"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 120
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->contains(DDDD)Z

    move-result v0

    return v0
.end method

.method public getBounds()Lorg/icepdf/index/java/awt/Rectangle;
    .locals 7

    .prologue
    .line 124
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getMinX()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 125
    .local v0, "x1":I
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getMinY()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 126
    .local v2, "y1":I
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getMaxX()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v1, v4

    .line 127
    .local v1, "x2":I
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getMaxY()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 128
    .local v3, "y2":I
    new-instance v4, Lorg/icepdf/index/java/awt/Rectangle;

    sub-int v5, v1, v0

    sub-int v6, v3, v2

    invoke-direct {v4, v0, v2, v5, v6}, Lorg/icepdf/index/java/awt/Rectangle;-><init>(IIII)V

    return-object v4
.end method

.method public getCenterX()D
    .locals 6

    .prologue
    .line 59
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public getCenterY()D
    .locals 6

    .prologue
    .line 63
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public getFrame()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 10

    .prologue
    .line 67
    new-instance v1, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v2

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v4

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v6

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v8

    invoke-direct/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    return-object v1
.end method

.method public abstract getHeight()D
.end method

.method public getMaxX()D
    .locals 4

    .prologue
    .line 51
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public getMaxY()D
    .locals 4

    .prologue
    .line 55
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v0

    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public getMinX()D
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v0

    return-wide v0
.end method

.method public getMinY()D
    .locals 2

    .prologue
    .line 47
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v0

    return-wide v0
.end method

.method public getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;D)Lorg/icepdf/index/java/awt/geom/PathIterator;
    .locals 2
    .param p1, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;
    .param p2, "flatness"    # D

    .prologue
    .line 132
    new-instance v0, Lorg/icepdf/index/java/awt/geom/FlatteningPathIterator;

    invoke-virtual {p0, p1}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;

    move-result-object v1

    invoke-direct {v0, v1, p2, p3}, Lorg/icepdf/index/java/awt/geom/FlatteningPathIterator;-><init>(Lorg/icepdf/index/java/awt/geom/PathIterator;D)V

    return-object v0
.end method

.method public abstract getWidth()D
.end method

.method public abstract getX()D
.end method

.method public abstract getY()D
.end method

.method public intersects(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Z
    .locals 10
    .param p1, "rect"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 116
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->intersects(DDDD)Z

    move-result v0

    return v0
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract setFrame(DDDD)V
.end method

.method public setFrame(Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Dimension2D;)V
    .locals 10
    .param p1, "loc"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "size"    # Lorg/icepdf/index/java/awt/geom/Dimension2D;

    .prologue
    .line 71
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v4

    invoke-virtual {p2}, Lorg/icepdf/index/java/awt/geom/Dimension2D;->getWidth()D

    move-result-wide v6

    invoke-virtual {p2}, Lorg/icepdf/index/java/awt/geom/Dimension2D;->getHeight()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->setFrame(DDDD)V

    .line 72
    return-void
.end method

.method public setFrame(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V
    .locals 10
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 75
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->setFrame(DDDD)V

    .line 76
    return-void
.end method

.method public setFrameFromCenter(DDDD)V
    .locals 15
    .param p1, "centerX"    # D
    .param p3, "centerY"    # D
    .param p5, "cornerX"    # D
    .param p7, "cornerY"    # D

    .prologue
    .line 102
    sub-double v0, p5, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    .line 103
    .local v12, "width":D
    sub-double v0, p7, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v10

    .line 104
    .local v10, "height":D
    sub-double v2, p1, v12

    sub-double v4, p3, v10

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    mul-double v6, v12, v0

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    mul-double v8, v10, v0

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->setFrame(DDDD)V

    .line 105
    return-void
.end method

.method public setFrameFromCenter(Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;)V
    .locals 10
    .param p1, "center"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "corner"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 108
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v4

    invoke-virtual {p2}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v6

    invoke-virtual {p2}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->setFrameFromCenter(DDDD)V

    .line 109
    return-void
.end method

.method public setFrameFromDiagonal(DDDD)V
    .locals 11
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "x2"    # D
    .param p7, "y2"    # D

    .prologue
    .line 80
    cmpg-double v0, p1, p5

    if-gez v0, :cond_0

    .line 81
    move-wide v2, p1

    .line 82
    .local v2, "rx":D
    sub-double v6, p5, p1

    .line 87
    .local v6, "rw":D
    :goto_0
    cmpg-double v0, p3, p7

    if-gez v0, :cond_1

    .line 88
    move-wide v4, p3

    .line 89
    .local v4, "ry":D
    sub-double v8, p7, p3

    .local v8, "rh":D
    :goto_1
    move-object v1, p0

    .line 94
    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->setFrame(DDDD)V

    .line 95
    return-void

    .line 84
    .end local v2    # "rx":D
    .end local v4    # "ry":D
    .end local v6    # "rw":D
    .end local v8    # "rh":D
    :cond_0
    move-wide/from16 v2, p5

    .line 85
    .restart local v2    # "rx":D
    sub-double v6, p1, p5

    .restart local v6    # "rw":D
    goto :goto_0

    .line 91
    :cond_1
    move-wide/from16 v4, p7

    .line 92
    .restart local v4    # "ry":D
    sub-double v8, p3, p7

    .restart local v8    # "rh":D
    goto :goto_1
.end method

.method public setFrameFromDiagonal(Lorg/icepdf/index/java/awt/geom/Point2D;Lorg/icepdf/index/java/awt/geom/Point2D;)V
    .locals 10
    .param p1, "p1"    # Lorg/icepdf/index/java/awt/geom/Point2D;
    .param p2, "p2"    # Lorg/icepdf/index/java/awt/geom/Point2D;

    .prologue
    .line 98
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v4

    invoke-virtual {p2}, Lorg/icepdf/index/java/awt/geom/Point2D;->getX()D

    move-result-wide v6

    invoke-virtual {p2}, Lorg/icepdf/index/java/awt/geom/Point2D;->getY()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/RectangularShape;->setFrameFromDiagonal(DDDD)V

    .line 99
    return-void
.end method
