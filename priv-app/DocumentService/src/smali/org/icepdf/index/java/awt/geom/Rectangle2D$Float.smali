.class public Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
.super Lorg/icepdf/index/java/awt/geom/Rectangle2D;
.source "Rectangle2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/Rectangle2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Float"
.end annotation


# instance fields
.field public height:F

.field public width:F

.field public x:F

.field public y:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;-><init>()V

    .line 42
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;-><init>()V

    .line 45
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->setRect(FFFF)V

    .line 46
    return-void
.end method


# virtual methods
.method public createIntersection(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 2
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 131
    instance-of v1, p1, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    if-eqz v1, :cond_0

    .line 132
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 136
    .local v0, "dst":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    :goto_0
    invoke-static {p0, p1, v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->intersect(Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    .line 137
    return-object v0

    .line 134
    .end local v0    # "dst":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    :cond_0
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>()V

    .restart local v0    # "dst":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    goto :goto_0
.end method

.method public createUnion(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 2
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 143
    instance-of v1, p1, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    if-eqz v1, :cond_0

    .line 144
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 148
    .local v0, "dst":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    :goto_0
    invoke-static {p0, p1, v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->union(Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    .line 149
    return-object v0

    .line 146
    .end local v0    # "dst":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    :cond_0
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>()V

    .restart local v0    # "dst":Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    goto :goto_0
.end method

.method public getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 5

    .prologue
    .line 125
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->y:F

    iget v3, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    iget v4, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->height:F

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    return-object v0
.end method

.method public getHeight()D
    .locals 2

    .prologue
    .line 65
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->height:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getWidth()D
    .locals 2

    .prologue
    .line 60
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 50
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 55
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->y:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    cmpg-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->height:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public outcode(DD)I
    .locals 5
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    const/4 v4, 0x0

    .line 98
    const/4 v0, 0x0

    .line 100
    .local v0, "code":I
    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    cmpg-float v1, v1, v4

    if-gtz v1, :cond_2

    .line 101
    or-int/lit8 v0, v0, 0x5

    .line 110
    :cond_0
    :goto_0
    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->height:F

    cmpg-float v1, v1, v4

    if-gtz v1, :cond_4

    .line 111
    or-int/lit8 v0, v0, 0xa

    .line 120
    :cond_1
    :goto_1
    return v0

    .line 103
    :cond_2
    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    float-to-double v2, v1

    cmpg-double v1, p1, v2

    if-gez v1, :cond_3

    .line 104
    or-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :cond_3
    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    add-float/2addr v1, v2

    float-to-double v2, v1

    cmpl-double v1, p1, v2

    if-lez v1, :cond_0

    .line 107
    or-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 113
    :cond_4
    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->y:F

    float-to-double v2, v1

    cmpg-double v1, p3, v2

    if-gez v1, :cond_5

    .line 114
    or-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 116
    :cond_5
    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->y:F

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->height:F

    add-float/2addr v1, v2

    float-to-double v2, v1

    cmpl-double v1, p3, v2

    if-lez v1, :cond_1

    .line 117
    or-int/lit8 v0, v0, 0x8

    goto :goto_1
.end method

.method public setRect(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 82
    double-to-float v0, p1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    .line 83
    double-to-float v0, p3

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->y:F

    .line 84
    double-to-float v0, p5

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    .line 85
    double-to-float v0, p7

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->height:F

    .line 86
    return-void
.end method

.method public setRect(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 74
    iput p1, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    .line 75
    iput p2, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->y:F

    .line 76
    iput p3, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    .line 77
    iput p4, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->height:F

    .line 78
    return-void
.end method

.method public setRect(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V
    .locals 2
    .param p1, "r"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    .prologue
    .line 90
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    .line 91
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->y:F

    .line 92
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    .line 93
    invoke-virtual {p1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->height:F

    .line 94
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "[x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->height:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
