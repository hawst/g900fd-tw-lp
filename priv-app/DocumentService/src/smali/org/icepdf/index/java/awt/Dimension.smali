.class public Lorg/icepdf/index/java/awt/Dimension;
.super Lorg/icepdf/index/java/awt/geom/Dimension2D;
.source "Dimension.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x418ed9d7ac5f4414L


# instance fields
.field public height:I

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, v0, v0}, Lorg/icepdf/index/java/awt/Dimension;-><init>(II)V

    .line 42
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/Dimension2D;-><init>()V

    .line 45
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/java/awt/Dimension;->setSize(II)V

    .line 46
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/java/awt/Dimension;)V
    .locals 2
    .param p1, "d"    # Lorg/icepdf/index/java/awt/Dimension;

    .prologue
    .line 37
    iget v0, p1, Lorg/icepdf/index/java/awt/Dimension;->width:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Dimension;->height:I

    invoke-direct {p0, v0, v1}, Lorg/icepdf/index/java/awt/Dimension;-><init>(II)V

    .line 38
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    if-ne p1, p0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v1

    .line 61
    :cond_1
    instance-of v3, p1, Lorg/icepdf/index/java/awt/Dimension;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 62
    check-cast v0, Lorg/icepdf/index/java/awt/Dimension;

    .line 63
    .local v0, "d":Lorg/icepdf/index/java/awt/Dimension;
    iget v3, v0, Lorg/icepdf/index/java/awt/Dimension;->width:I

    iget v4, p0, Lorg/icepdf/index/java/awt/Dimension;->width:I

    if-ne v3, v4, :cond_2

    iget v3, v0, Lorg/icepdf/index/java/awt/Dimension;->height:I

    iget v4, p0, Lorg/icepdf/index/java/awt/Dimension;->height:I

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "d":Lorg/icepdf/index/java/awt/Dimension;
    :cond_3
    move v1, v2

    .line 65
    goto :goto_0
.end method

.method public getHeight()D
    .locals 2

    .prologue
    .line 95
    iget v0, p0, Lorg/icepdf/index/java/awt/Dimension;->height:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public getSize()Lorg/icepdf/index/java/awt/Dimension;
    .locals 3

    .prologue
    .line 90
    new-instance v0, Lorg/icepdf/index/java/awt/Dimension;

    iget v1, p0, Lorg/icepdf/index/java/awt/Dimension;->width:I

    iget v2, p0, Lorg/icepdf/index/java/awt/Dimension;->height:I

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/java/awt/Dimension;-><init>(II)V

    return-object v0
.end method

.method public getWidth()D
    .locals 2

    .prologue
    .line 100
    iget v0, p0, Lorg/icepdf/index/java/awt/Dimension;->width:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lorg/apache/harmony/misc/HashCode;

    invoke-direct {v0}, Lorg/apache/harmony/misc/HashCode;-><init>()V

    .line 51
    .local v0, "hash":Lorg/apache/harmony/misc/HashCode;
    iget v1, p0, Lorg/icepdf/index/java/awt/Dimension;->width:I

    invoke-virtual {v0, v1}, Lorg/apache/harmony/misc/HashCode;->append(I)Lorg/apache/harmony/misc/HashCode;

    .line 52
    iget v1, p0, Lorg/icepdf/index/java/awt/Dimension;->height:I

    invoke-virtual {v0, v1}, Lorg/apache/harmony/misc/HashCode;->append(I)Lorg/apache/harmony/misc/HashCode;

    .line 53
    invoke-virtual {v0}, Lorg/apache/harmony/misc/HashCode;->hashCode()I

    move-result v1

    return v1
.end method

.method public setSize(DD)V
    .locals 5
    .param p1, "width"    # D
    .param p3, "height"    # D

    .prologue
    .line 86
    invoke-static {p1, p2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {p3, p4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/Dimension;->setSize(II)V

    .line 87
    return-void
.end method

.method public setSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 76
    iput p1, p0, Lorg/icepdf/index/java/awt/Dimension;->width:I

    .line 77
    iput p2, p0, Lorg/icepdf/index/java/awt/Dimension;->height:I

    .line 78
    return-void
.end method

.method public setSize(Lorg/icepdf/index/java/awt/Dimension;)V
    .locals 2
    .param p1, "d"    # Lorg/icepdf/index/java/awt/Dimension;

    .prologue
    .line 81
    iget v0, p1, Lorg/icepdf/index/java/awt/Dimension;->width:I

    iget v1, p1, Lorg/icepdf/index/java/awt/Dimension;->height:I

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/java/awt/Dimension;->setSize(II)V

    .line 82
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "[width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/Dimension;->width:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/java/awt/Dimension;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
