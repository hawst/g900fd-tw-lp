.class public Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;
.super Lorg/icepdf/index/java/awt/geom/QuadCurve2D;
.source "QuadCurve2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/QuadCurve2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Float"
.end annotation


# instance fields
.field public ctrlx:F

.field public ctrly:F

.field public x1:F

.field public x2:F

.field public y1:F

.field public y2:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;-><init>()V

    .line 42
    return-void
.end method

.method public constructor <init>(FFFFFF)V
    .locals 0
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "ctrlx"    # F
    .param p4, "ctrly"    # F
    .param p5, "x2"    # F
    .param p6, "y2"    # F

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D;-><init>()V

    .line 45
    invoke-virtual/range {p0 .. p6}, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->setCurve(FFFFFF)V

    .line 46
    return-void
.end method


# virtual methods
.method public getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;
    .locals 7

    .prologue
    .line 113
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->x1:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->x2:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->ctrlx:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 114
    .local v0, "rx0":F
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->y1:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->y2:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->ctrly:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 115
    .local v2, "ry0":F
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->x1:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->x2:F

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->ctrlx:F

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 116
    .local v1, "rx1":F
    iget v4, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->y1:F

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->y2:F

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iget v5, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->ctrly:F

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 117
    .local v3, "ry1":F
    new-instance v4, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    sub-float v5, v1, v0

    sub-float v6, v3, v2

    invoke-direct {v4, v0, v2, v5, v6}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    return-object v4
.end method

.method public getCtrlPt()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 3

    .prologue
    .line 85
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->ctrlx:F

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->ctrly:F

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>(FF)V

    return-object v0
.end method

.method public getCtrlX()D
    .locals 2

    .prologue
    .line 60
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->ctrlx:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getCtrlY()D
    .locals 2

    .prologue
    .line 65
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->ctrly:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getP1()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 3

    .prologue
    .line 80
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->x1:F

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->y1:F

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>(FF)V

    return-object v0
.end method

.method public getP2()Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 3

    .prologue
    .line 90
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    iget v1, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->x2:F

    iget v2, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->y2:F

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>(FF)V

    return-object v0
.end method

.method public getX1()D
    .locals 2

    .prologue
    .line 50
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->x1:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getX2()D
    .locals 2

    .prologue
    .line 70
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->x2:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getY1()D
    .locals 2

    .prologue
    .line 55
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->y1:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getY2()D
    .locals 2

    .prologue
    .line 75
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->y2:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public setCurve(DDDDDD)V
    .locals 1
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "ctrlx"    # D
    .param p7, "ctrly"    # D
    .param p9, "x2"    # D
    .param p11, "y2"    # D

    .prologue
    .line 95
    double-to-float v0, p1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->x1:F

    .line 96
    double-to-float v0, p3

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->y1:F

    .line 97
    double-to-float v0, p5

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->ctrlx:F

    .line 98
    double-to-float v0, p7

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->ctrly:F

    .line 99
    double-to-float v0, p9

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->x2:F

    .line 100
    double-to-float v0, p11

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->y2:F

    .line 101
    return-void
.end method

.method public setCurve(FFFFFF)V
    .locals 0
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "ctrlx"    # F
    .param p4, "ctrly"    # F
    .param p5, "x2"    # F
    .param p6, "y2"    # F

    .prologue
    .line 104
    iput p1, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->x1:F

    .line 105
    iput p2, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->y1:F

    .line 106
    iput p3, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->ctrlx:F

    .line 107
    iput p4, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->ctrly:F

    .line 108
    iput p5, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->x2:F

    .line 109
    iput p6, p0, Lorg/icepdf/index/java/awt/geom/QuadCurve2D$Float;->y2:F

    .line 110
    return-void
.end method
