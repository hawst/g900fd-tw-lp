.class Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;
.super Ljava/lang/Object;
.source "CubicCurve2D.java"

# interfaces
.implements Lorg/icepdf/index/java/awt/geom/PathIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/java/awt/geom/CubicCurve2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Iterator"
.end annotation


# instance fields
.field c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

.field index:I

.field t:Lorg/icepdf/index/java/awt/geom/AffineTransform;


# direct methods
.method constructor <init>(Lorg/icepdf/index/java/awt/geom/CubicCurve2D;Lorg/icepdf/index/java/awt/geom/AffineTransform;)V
    .locals 0
    .param p1, "c"    # Lorg/icepdf/index/java/awt/geom/CubicCurve2D;
    .param p2, "t"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 276
    iput-object p1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    .line 277
    iput-object p2, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .line 278
    return-void
.end method


# virtual methods
.method public currentSegment([D)I
    .locals 10
    .param p1, "coords"    # [D

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 293
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string/jumbo v1, "awt.4B"

    invoke-static {v1}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 298
    :cond_0
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->index:I

    if-nez v0, :cond_2

    .line 299
    const/4 v6, 0x0

    .line 300
    .local v6, "type":I
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getX1()D

    move-result-wide v0

    aput-wide v0, p1, v2

    .line 301
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getY1()D

    move-result-wide v0

    aput-wide v0, p1, v3

    .line 302
    const/4 v5, 0x1

    .line 313
    .local v5, "count":I
    :goto_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    if-eqz v0, :cond_1

    .line 314
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-object v1, p1

    move-object v3, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->transform([DI[DII)V

    .line 316
    :cond_1
    return v6

    .line 304
    .end local v5    # "count":I
    .end local v6    # "type":I
    :cond_2
    const/4 v6, 0x3

    .line 305
    .restart local v6    # "type":I
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlX1()D

    move-result-wide v0

    aput-wide v0, p1, v2

    .line 306
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlY1()D

    move-result-wide v0

    aput-wide v0, p1, v3

    .line 307
    const/4 v0, 0x2

    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlX2()D

    move-result-wide v8

    aput-wide v8, p1, v0

    .line 308
    const/4 v0, 0x3

    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlY2()D

    move-result-wide v8

    aput-wide v8, p1, v0

    .line 309
    const/4 v0, 0x4

    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getX2()D

    move-result-wide v8

    aput-wide v8, p1, v0

    .line 310
    const/4 v0, 0x5

    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getY2()D

    move-result-wide v8

    aput-wide v8, p1, v0

    .line 311
    const/4 v5, 0x3

    .restart local v5    # "count":I
    goto :goto_0
.end method

.method public currentSegment([F)I
    .locals 10
    .param p1, "coords"    # [F

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 320
    invoke-virtual {p0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string/jumbo v1, "awt.4B"

    invoke-static {v1}, Lorg/apache/harmony/awt/internal/nls/Messages;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 325
    :cond_0
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->index:I

    if-nez v0, :cond_2

    .line 326
    const/4 v6, 0x0

    .line 327
    .local v6, "type":I
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getX1()D

    move-result-wide v0

    double-to-float v0, v0

    aput v0, p1, v2

    .line 328
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getY1()D

    move-result-wide v0

    double-to-float v0, v0

    aput v0, p1, v3

    .line 329
    const/4 v5, 0x1

    .line 340
    .local v5, "count":I
    :goto_0
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    if-eqz v0, :cond_1

    .line 341
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->t:Lorg/icepdf/index/java/awt/geom/AffineTransform;

    move-object v1, p1

    move-object v3, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lorg/icepdf/index/java/awt/geom/AffineTransform;->transform([FI[FII)V

    .line 343
    :cond_1
    return v6

    .line 331
    .end local v5    # "count":I
    .end local v6    # "type":I
    :cond_2
    const/4 v6, 0x3

    .line 332
    .restart local v6    # "type":I
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlX1()D

    move-result-wide v0

    double-to-float v0, v0

    aput v0, p1, v2

    .line 333
    iget-object v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlY1()D

    move-result-wide v0

    double-to-float v0, v0

    aput v0, p1, v3

    .line 334
    const/4 v0, 0x2

    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlX2()D

    move-result-wide v8

    double-to-float v1, v8

    aput v1, p1, v0

    .line 335
    const/4 v0, 0x3

    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getCtrlY2()D

    move-result-wide v8

    double-to-float v1, v8

    aput v1, p1, v0

    .line 336
    const/4 v0, 0x4

    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getX2()D

    move-result-wide v8

    double-to-float v1, v8

    aput v1, p1, v0

    .line 337
    const/4 v0, 0x5

    iget-object v1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->c:Lorg/icepdf/index/java/awt/geom/CubicCurve2D;

    invoke-virtual {v1}, Lorg/icepdf/index/java/awt/geom/CubicCurve2D;->getY2()D

    move-result-wide v8

    double-to-float v1, v8

    aput v1, p1, v0

    .line 338
    const/4 v5, 0x3

    .restart local v5    # "count":I
    goto :goto_0
.end method

.method public getWindingRule()I
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x1

    return v0
.end method

.method public isDone()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 285
    iget v1, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->index:I

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()V
    .locals 1

    .prologue
    .line 289
    iget v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/icepdf/index/java/awt/geom/CubicCurve2D$Iterator;->index:I

    .line 290
    return-void
.end method
