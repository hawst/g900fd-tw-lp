.class public Lcom/samsung/android/MtpApplication/MtpApplication;
.super Landroid/app/Activity;
.source "MtpApplication.java"


# static fields
.field private static ANIMATION_STATUS:I

.field static isSynchronizing:Z

.field static mtprunningStatus:Z

.field public static tc:Landroid/mtp/MTPJNIInterface;


# instance fields
.field private DESTROY_MTP:Z

.field private animImg:Landroid/widget/ImageView;

.field cr:Landroid/content/ContentResolver;

.field private isTablet:Z

.field final mAnimationHandler:Landroid/os/Handler;

.field private mCallButton:Landroid/widget/Button;

.field mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field private final mTimeClickReceiver:Landroid/content/BroadcastReceiver;

.field mstartAnimator:Ljava/lang/Runnable;

.field private textView:Landroid/widget/TextView;

.field wl:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpApplication;->mtprunningStatus:Z

    .line 79
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpApplication;->tc:Landroid/mtp/MTPJNIInterface;

    .line 82
    sput v1, Lcom/samsung/android/MtpApplication/MtpApplication;->ANIMATION_STATUS:I

    .line 106
    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpApplication;->isSynchronizing:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 80
    iput-object v1, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->cr:Landroid/content/ContentResolver;

    .line 89
    iput-object p0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->mContext:Landroid/content/Context;

    .line 90
    iput-boolean v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->DESTROY_MTP:Z

    .line 91
    iput-boolean v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->isTablet:Z

    .line 102
    iput-object v1, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->wl:Landroid/os/PowerManager$WakeLock;

    .line 141
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->mAnimationHandler:Landroid/os/Handler;

    .line 142
    new-instance v0, Lcom/samsung/android/MtpApplication/MtpApplication$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/MtpApplication/MtpApplication$1;-><init>(Lcom/samsung/android/MtpApplication/MtpApplication;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->mstartAnimator:Ljava/lang/Runnable;

    .line 224
    new-instance v0, Lcom/samsung/android/MtpApplication/MtpApplication$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/MtpApplication/MtpApplication$2;-><init>(Lcom/samsung/android/MtpApplication/MtpApplication;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->mHandler:Landroid/os/Handler;

    .line 312
    new-instance v0, Lcom/samsung/android/MtpApplication/MtpApplication$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/MtpApplication/MtpApplication$3;-><init>(Lcom/samsung/android/MtpApplication/MtpApplication;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->mTimeClickReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 58
    sget v0, Lcom/samsung/android/MtpApplication/MtpApplication;->ANIMATION_STATUS:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 58
    sput p0, Lcom/samsung/android/MtpApplication/MtpApplication;->ANIMATION_STATUS:I

    return p0
.end method

.method static synthetic access$100(Lcom/samsung/android/MtpApplication/MtpApplication;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpApplication;
    .param p1, "x1"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/samsung/android/MtpApplication/MtpApplication;->startSearchView(I)V

    return-void
.end method

.method private getDeviceType()V
    .locals 2

    .prologue
    .line 257
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/MtpApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 259
    const-string v0, "MTPAPP"

    const-string v1, "***Tablet***"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->isTablet:Z

    .line 265
    :goto_0
    return-void

    .line 262
    :cond_0
    const-string v0, "MTPAPP"

    const-string v1, "***Phone***"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->isTablet:Z

    goto :goto_0
.end method

.method private registerBroadCastRec()V
    .locals 3

    .prologue
    .line 130
    const-string v1, "MTPAPP"

    const-string v2, "< MTP > Registering BroadCast receiver :::::"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 132
    .local v0, "lIntentFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.MTP.NOTIFICATION_TYPE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 133
    const-string v1, "com.android.END_MTP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 134
    const-string v1, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 136
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->mTimeClickReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/MtpApplication/MtpApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 138
    return-void
.end method

.method private setMtpAnimationStatus()V
    .locals 3

    .prologue
    .line 219
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->textView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/MtpApplication;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->textView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/MtpApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setOrientation()V
    .locals 2

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->isTablet:Z

    if-eqz v0, :cond_0

    .line 268
    const-string v0, "MTPAPP"

    const-string v1, "Setting landscape for Tablet"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/MtpApplication/MtpApplication;->setRequestedOrientation(I)V

    .line 274
    :goto_0
    return-void

    .line 271
    :cond_0
    const-string v0, "MTPAPP"

    const-string v1, "Setting portrait for Phone"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/MtpApplication/MtpApplication;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method private startSearchView(I)V
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->animImg:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->DESTROY_MTP:Z

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->cr:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    .line 160
    :cond_0
    const-string v0, "MTPAPP"

    const-string v1, "animImg object is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :goto_0
    return-void

    .line 164
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpApplication;->setMtpAnimationStatus()V

    .line 167
    sget v0, Lcom/samsung/android/MtpApplication/MtpApplication;->ANIMATION_STATUS:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/MtpApplication/MtpApplication;->ANIMATION_STATUS:I

    .line 168
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->mAnimationHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->mstartAnimator:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x4

    .line 279
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 280
    const-string v3, "MTPAPP"

    const-string v4, "In onCreate of MTPAPP"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpApplication;->getDeviceType()V

    .line 282
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpApplication;->setOrientation()V

    .line 284
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 285
    .local v1, "closeDialogs":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/samsung/android/MtpApplication/MtpApplication;->sendBroadcast(Landroid/content/Intent;)V

    .line 288
    new-instance v3, Landroid/content/Intent;

    const-string v4, "intent.stop.app-in-app"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/samsung/android/MtpApplication/MtpApplication;->sendBroadcast(Landroid/content/Intent;)V

    .line 290
    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/MtpApplication;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v5, v5}, Landroid/view/Window;->setFlags(II)V

    .line 291
    const/high16 v3, 0x7f030000

    invoke-virtual {p0, v3}, Lcom/samsung/android/MtpApplication/MtpApplication;->setContentView(I)V

    .line 292
    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/MtpApplication;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 293
    .local v0, "bar":Landroid/app/ActionBar;
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 295
    const/high16 v3, 0x7f060000

    invoke-virtual {p0, v3}, Lcom/samsung/android/MtpApplication/MtpApplication;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->animImg:Landroid/widget/ImageView;

    .line 296
    const v3, 0x7f060001

    invoke-virtual {p0, v3}, Lcom/samsung/android/MtpApplication/MtpApplication;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->textView:Landroid/widget/TextView;

    .line 297
    const v3, 0x7f060002

    invoke-virtual {p0, v3}, Lcom/samsung/android/MtpApplication/MtpApplication;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->mCallButton:Landroid/widget/Button;

    .line 298
    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/MtpApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->cr:Landroid/content/ContentResolver;

    .line 299
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/samsung/android/MtpApplication/MtpApplication;->startSearchView(I)V

    .line 301
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->mHandler:Landroid/os/Handler;

    invoke-static {v3}, Lcom/samsung/android/MtpApplication/MtpReceiver;->setHandler(Landroid/os/Handler;)V

    .line 303
    const-string v3, "power"

    invoke-virtual {p0, v3}, Lcom/samsung/android/MtpApplication/MtpApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 304
    .local v2, "pm":Landroid/os/PowerManager;
    const/4 v3, 0x1

    const-string v4, "My Tag"

    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->wl:Landroid/os/PowerManager$WakeLock;

    .line 305
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 307
    const-string v3, "MTPAPP"

    const-string v4, "acquired wake lock."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpApplication;->registerBroadCastRec()V

    .line 309
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 111
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->DESTROY_MTP:Z

    .line 112
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->mContext:Landroid/content/Context;

    const-string v2, "statusbar"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 113
    .local v0, "sbm":Landroid/app/StatusBarManager;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 114
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 115
    const-string v1, "MTPAPP"

    const-string v2, "In destroy, wake lock releaseed, unregistering broadcast receiver"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpApplication;->mTimeClickReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/samsung/android/MtpApplication/MtpApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 117
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 118
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 122
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 123
    const-string v0, "MTPAPP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "keyCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
