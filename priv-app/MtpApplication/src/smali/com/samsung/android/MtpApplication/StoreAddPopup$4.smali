.class Lcom/samsung/android/MtpApplication/StoreAddPopup$4;
.super Ljava/lang/Object;
.source "StoreAddPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/MtpApplication/StoreAddPopup;->drawPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;


# direct methods
.method constructor <init>(Lcom/samsung/android/MtpApplication/StoreAddPopup;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$4;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v8, 0x0

    .line 135
    const-string v4, "StoreAddPopup"

    const-string v5, "OK Button is clicked..."

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$4;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    .line 138
    .local v1, "context":Landroid/content/Context;
    if-nez v1, :cond_0

    .line 139
    iget-object v4, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$4;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    invoke-virtual {v4}, Lcom/samsung/android/MtpApplication/StoreAddPopup;->finish()V

    .line 141
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$4;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    iget-object v4, v4, Lcom/samsung/android/MtpApplication/StoreAddPopup;->cr:Landroid/content/ContentResolver;

    if-nez v4, :cond_1

    .line 142
    iget-object v4, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$4;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iput-object v5, v4, Lcom/samsung/android/MtpApplication/StoreAddPopup;->cr:Landroid/content/ContentResolver;

    .line 144
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/samsung/android/MtpApplication/MtpReceiver;

    invoke-direct {v2, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 145
    .local v2, "intentBroadcast":Landroid/content/Intent;
    const-string v4, "com.android.MTP.LAUNCH_MTP"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    const-string v4, "StoreAddPopup"

    const-string v5, "Setting the sdcard_launch to 1 "

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v4, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$4;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    iget-object v4, v4, Lcom/samsung/android/MtpApplication/StoreAddPopup;->cr:Landroid/content/ContentResolver;

    const-string v5, "sdcard_launch"

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 150
    const v4, 0x2f145

    const/high16 v5, 0x8000000

    invoke-static {v1, v4, v2, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 152
    .local v3, "sender":Landroid/app/PendingIntent;
    iget-object v4, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$4;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    const-string v5, "alarm"

    invoke-virtual {v4, v5}, Lcom/samsung/android/MtpApplication/StoreAddPopup;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 153
    .local v0, "am":Landroid/app/AlarmManager;
    const-string v4, "StoreAddPopup"

    const-string v5, "sending broadcast after delay of 5 sec"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x1388

    add-long/2addr v4, v6

    invoke-virtual {v0, v8, v4, v5, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 157
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v4

    sput-object v4, Lcom/samsung/android/MtpApplication/MtpApplication;->tc:Landroid/mtp/MTPJNIInterface;

    .line 158
    sget-object v4, Lcom/samsung/android/MtpApplication/MtpApplication;->tc:Landroid/mtp/MTPJNIInterface;

    if-eqz v4, :cond_2

    .line 159
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "mtp_running_status"

    invoke-static {v4, v5, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 160
    sget-object v4, Lcom/samsung/android/MtpApplication/MtpApplication;->tc:Landroid/mtp/MTPJNIInterface;

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    .line 161
    iget-object v4, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$4;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    iget-object v4, v4, Lcom/samsung/android/MtpApplication/StoreAddPopup;->cr:Landroid/content/ContentResolver;

    const-string v5, "sdcard_launch"

    invoke-static {v4, v5, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 162
    iget-object v4, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$4;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    invoke-virtual {v4}, Lcom/samsung/android/MtpApplication/StoreAddPopup;->finish()V

    .line 164
    :cond_2
    return-void
.end method
