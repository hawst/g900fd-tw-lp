.class Lcom/samsung/android/MtpApplication/StoreAddPopup$1;
.super Landroid/content/BroadcastReceiver;
.source "StoreAddPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/MtpApplication/StoreAddPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;


# direct methods
.method constructor <init>(Lcom/samsung/android/MtpApplication/StoreAddPopup;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$1;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 87
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    const-string v1, "StoreAddPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "In StoreAddPopup Receiver:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$1;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/MtpApplication/StoreAddPopup;->cr:Landroid/content/ContentResolver;

    .line 92
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    const-string v1, "StoreAddPopup"

    const-string v2, "MMC is disconnected"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    sput-boolean v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->sdCardExists:Z

    .line 95
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$1;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    iget-object v1, v1, Lcom/samsung/android/MtpApplication/StoreAddPopup;->cr:Landroid/content/ContentResolver;

    const-string v2, "sdcard_launch"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 96
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$1;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    iget-object v2, v2, Lcom/samsung/android/MtpApplication/StoreAddPopup;->cr:Landroid/content/ContentResolver;

    const-string v3, "mtp_running_status"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 98
    const-string v1, "StoreAddPopup"

    const-string v2, "Top screen is MTP"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup$1;->this$0:Lcom/samsung/android/MtpApplication/StoreAddPopup;

    invoke-virtual {v1}, Lcom/samsung/android/MtpApplication/StoreAddPopup;->finish()V

    goto :goto_0
.end method
