.class public Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;
.super Landroid/database/ContentObserver;
.source "MtpPlaylistObserver.java"


# static fields
.field static cr:Landroid/content/ContentResolver;

.field static handle1:Landroid/os/Handler;

.field static mContext:Landroid/content/Context;

.field static mCount:I

.field static map:Ljava/util/LinkedHashMap;

.field static mtpJniInterface:Landroid/mtp/MTPJNIInterface;

.field private static mtpPlaylistObserver:Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;

.field static playlist_std_dir:Ljava/lang/String;

.field static playlist_test_std_dir:Ljava/lang/String;


# instance fields
.field handle:Landroid/os/Handler;

.field mPath:Ljava/lang/String;

.field mRowId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->handle1:Landroid/os/Handler;

    .line 31
    sput-object v1, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->cr:Landroid/content/ContentResolver;

    .line 32
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    .line 33
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->map:Ljava/util/LinkedHashMap;

    .line 37
    sput-object v1, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mContext:Landroid/content/Context;

    .line 41
    const-string v0, ""

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->playlist_std_dir:Ljava/lang/String;

    .line 42
    const-string v0, ""

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->playlist_test_std_dir:Ljava/lang/String;

    .line 43
    sput-object v1, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mtpJniInterface:Landroid/mtp/MTPJNIInterface;

    .line 49
    new-instance v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;

    sget-object v1, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->handle1:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;-><init>(Landroid/os/Handler;)V

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mtpPlaylistObserver:Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 30
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->handle:Landroid/os/Handler;

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mRowId:I

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mPath:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;
    .locals 2

    .prologue
    .line 55
    const-class v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mtpPlaylistObserver:Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private registerContentObserver()V
    .locals 4

    .prologue
    .line 525
    const-string v1, "MTPPlaObsrvr"

    const-string v2, "Inside registerContentObserver"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    new-instance v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;

    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->handle:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;-><init>(Landroid/os/Handler;)V

    .line 527
    .local v0, "plaObserver":Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->cr:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 528
    return-void
.end method


# virtual methods
.method public createPla(Ljava/lang/String;)V
    .locals 5
    .param p1, "playlistname"    # Ljava/lang/String;

    .prologue
    .line 503
    const-string v3, "Quick list"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 504
    const-string v3, "MTPPlaObsrvr"

    const-string v4, "Ignore Quick list.pla"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    :goto_0
    return-void

    .line 507
    :cond_0
    const/4 v2, 0x0

    .line 508
    .local v2, "playlist_path":Ljava/lang/String;
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->playlist_std_dir:Ljava/lang/String;

    .line 509
    .local v1, "playlist_dir":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 510
    .local v0, "plaDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 511
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 513
    :cond_1
    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 514
    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 515
    const-string v3, ".pla"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 521
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mtpJniInterface:Landroid/mtp/MTPJNIInterface;

    invoke-virtual {v3, v2}, Landroid/mtp/MTPJNIInterface;->sendPlaAdded(Ljava/lang/String;)V

    goto :goto_0
.end method

.method createPlaFiles()V
    .locals 21

    .prologue
    .line 384
    const-string v2, "MTPPlaObsrvr"

    const-string v4, " inside createplafiles"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    sget-object v3, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 387
    .local v3, "playlistUri":Landroid/net/Uri;
    sget-object v2, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->cr:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 388
    .local v9, "cur":Landroid/database/Cursor;
    if-nez v9, :cond_0

    .line 389
    const-string v2, "MTPPlaObsrvr"

    const-string v4, "Cursor is null"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    :goto_0
    return-void

    .line 392
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v2

    sput v2, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    .line 393
    const-string v2, "MTPPlaObsrvr"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "playlist count is"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    const-string v20, ""

    .line 396
    .local v20, "playlist_path":Ljava/lang/String;
    const-string v19, ""

    .line 397
    .local v19, "playlist_name":Ljava/lang/String;
    const-string v18, ""

    .line 399
    .local v18, "playlist_dir":Ljava/lang/String;
    sget-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->mountPath:Ljava/lang/String;

    const-string v4, "/Playlists"

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->playlist_std_dir:Ljava/lang/String;

    .line 400
    sget-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->mountPath:Ljava/lang/String;

    const-string v4, "/Test.txt"

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->playlist_test_std_dir:Ljava/lang/String;

    .line 401
    sget-object v18, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->playlist_std_dir:Ljava/lang/String;

    .line 406
    const/4 v11, 0x0

    .line 407
    .local v11, "i":I
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 408
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 410
    .local v16, "plaDir":Ljava/io/File;
    :try_start_0
    const-string v2, "MTPPlaObsrvr"

    const-string v4, " inside try branch createplafiles"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 412
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->mkdir()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 434
    :cond_1
    :goto_1
    sget v2, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    if-ge v11, v2, :cond_a

    .line 435
    const-string v2, "name"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 436
    const-string v2, "/"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "\\"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, ":"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "*"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "?"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "\""

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "<"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, ">"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "|"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 439
    :cond_2
    const-string v2, "MTPPlaObsrvr"

    const-string v4, "playlist file name is having special character so not creating pla file"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    .line 441
    add-int/lit8 v11, v11, 0x1

    .line 442
    goto :goto_1

    .line 415
    :cond_3
    :try_start_1
    const-string v2, "MTPPlaObsrvr"

    const-string v4, " inside deleteing plas createplafiles"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v15

    .line 417
    .local v15, "paths":[Ljava/io/File;
    if-nez v15, :cond_4

    .line 419
    const-string v2, "MTPPlaObsrvr"

    const-string v4, " close cursor and return as paths is null!."

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 431
    .end local v15    # "paths":[Ljava/io/File;
    :catch_0
    move-exception v10

    .line 432
    .local v10, "e":Ljava/lang/Exception;
    const-string v2, "MTPPlaObsrvr"

    const-string v4, "exception during creation and deletion of playlist dir"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 423
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v15    # "paths":[Ljava/io/File;
    :cond_4
    move-object v8, v15

    .local v8, "arr$":[Ljava/io/File;
    :try_start_2
    array-length v13, v8

    .local v13, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_2
    if-ge v12, v13, :cond_1

    aget-object v14, v8, v12

    .line 425
    .local v14, "path":Ljava/io/File;
    const-string v2, "MTPPlaObsrvr"

    const-string v4, " inside deleteing plas createplafiles"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const-string v4, ".pla"

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->isPlaylistPresent(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 427
    invoke-virtual {v14}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 423
    :cond_5
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 444
    .end local v8    # "arr$":[Ljava/io/File;
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    .end local v14    # "path":Ljava/io/File;
    .end local v15    # "paths":[Ljava/io/File;
    :cond_6
    const-string v2, "_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mRowId:I

    .line 445
    const-string v2, "MTPPlaObsrvr"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Row ID is"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mRowId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    const-string v2, "_data"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mPath:Ljava/lang/String;

    .line 447
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mPath:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 448
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mPath:Ljava/lang/String;

    const-string v4, ".pla"

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 449
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mPath:Ljava/lang/String;

    const-string v4, ".pla"

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mPath:Ljava/lang/String;

    .line 450
    const-string v2, "MTPPlaObsrvr"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mPath is"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    :cond_7
    sget-object v2, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->map:Ljava/util/LinkedHashMap;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mRowId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mPath:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mPath:Ljava/lang/String;

    const-string v4, "Quick list.pla"

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 454
    const-string v2, "MTPPlaObsrvr"

    const-string v4, "Ignore Quick list.pla"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    add-int/lit8 v11, v11, 0x1

    .line 456
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_1

    .line 460
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mPath:Ljava/lang/String;

    const-string v4, "/Playlists/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 461
    const-string v2, "/"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 462
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 463
    const-string v2, ".pla"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 464
    const-string v2, "MTPPlaObsrvr"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "playlist file path is"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 466
    .local v17, "plaFile":Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_9

    .line 468
    :try_start_3
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->createNewFile()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 475
    .end local v17    # "plaFile":Ljava/io/File;
    :cond_9
    :goto_3
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    .line 476
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 469
    .restart local v17    # "plaFile":Ljava/io/File;
    :catch_1
    move-exception v10

    .line 470
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 478
    .end local v10    # "e":Ljava/io/IOException;
    .end local v17    # "plaFile":Ljava/io/File;
    :cond_a
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public isPlaylistPresent(Ljava/lang/String;)Z
    .locals 9
    .param p1, "playlistPath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 483
    const-string v0, "MTPPlaObsrvr"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPlaylistPresent playlistPath= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 485
    .local v1, "playlistUri":Landroid/net/Uri;
    sget-object v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 486
    .local v7, "cur":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 487
    const-string v0, "MTPPlaObsrvr"

    const-string v2, "Cur is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v8

    .line 498
    :goto_0
    return v0

    .line 490
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 491
    .local v6, "count":I
    const-string v0, "MTPPlaObsrvr"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 493
    if-nez v6, :cond_1

    .line 494
    const-string v0, "MTPPlaObsrvr"

    const-string v2, "Count0, return false"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    const/4 v0, 0x0

    goto :goto_0

    .line 497
    :cond_1
    const-string v0, "MTPPlaObsrvr"

    const-string v2, "Count not 0, return true"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v8

    .line 498
    goto :goto_0
.end method

.method public onChange(Z)V
    .locals 33
    .param p1, "selfChange"    # Z

    .prologue
    .line 71
    invoke-super/range {p0 .. p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 72
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "On change is called."

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    sget-object v4, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 74
    .local v4, "uri":Landroid/net/Uri;
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "URI of the playlist"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->cr:Landroid/content/ContentResolver;

    if-nez v3, :cond_1

    .line 77
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "Content resolver is null"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mContext:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 79
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "mContext is also null"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    :goto_0
    return-void

    .line 82
    :cond_0
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->cr:Landroid/content/ContentResolver;

    .line 86
    :cond_1
    const/4 v13, 0x0

    .line 88
    .local v13, "cur":Landroid/database/Cursor;
    :try_start_0
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->cr:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 94
    if-nez v13, :cond_2

    .line 95
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "Cursor cur is null"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 89
    :catch_0
    move-exception v17

    .line 90
    .local v17, "e":Ljava/lang/Exception;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 98
    .end local v17    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v12

    .line 99
    .local v12, "count":I
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Count"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "mCount"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v7, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :try_start_1
    sget v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    if-ge v12, v3, :cond_8

    .line 104
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Case is for deletion. Count before deletion"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v7, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Actual count"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v3

    sput v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    .line 107
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 108
    sget v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    new-array v15, v3, [I

    .line 109
    .local v15, "dbEntries":[I
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "size of the integer array"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v7, v15

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    const/16 v22, 0x0

    .line 111
    .local v22, "i":I
    :goto_1
    sget v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    move/from16 v0, v22

    if-ge v0, v3, :cond_3

    .line 112
    const-string v3, "_id"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mRowId:I

    .line 113
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mRowId:I

    aput v3, v15, v22

    .line 115
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    .line 116
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 119
    :cond_3
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->map:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v32

    .line 120
    .local v32, "set":Ljava/util/Set;
    invoke-interface/range {v32 .. v32}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .line 121
    .local v23, "iter":Ljava/util/Iterator;
    const/16 v21, 0x0

    .line 122
    .local v21, "hashValue":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    .line 123
    .local v20, "hashKey":Ljava/lang/Integer;
    const/16 v19, 0x0

    .line 124
    .end local v20    # "hashKey":Ljava/lang/Integer;
    .local v19, "found":Z
    :cond_4
    :goto_2
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 125
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "checking for next playlist deleted"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const/16 v19, 0x0

    .line 127
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/util/Map$Entry;

    .line 128
    .local v18, "entry":Ljava/util/Map$Entry;
    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v20

    .line 129
    .local v20, "hashKey":Ljava/lang/Object;
    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v21

    .end local v21    # "hashValue":Ljava/lang/String;
    check-cast v21, Ljava/lang/String;

    .line 132
    .restart local v21    # "hashValue":Ljava/lang/String;
    const/16 v22, 0x0

    :goto_3
    sget v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    move/from16 v0, v22

    if-ge v0, v3, :cond_5

    .line 133
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "inside for loop"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v7, v15, v22

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    aget v3, v15, v22

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 135
    const/16 v19, 0x1

    .line 140
    :cond_5
    if-nez v19, :cond_4

    .line 141
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->map:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->map:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .line 143
    const/16 v26, 0x0

    .line 144
    .local v26, "playlistName":Ljava/lang/String;
    const-string v3, ".pla"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 145
    const-string v3, ".pla"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 157
    :goto_4
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mtpJniInterface:Landroid/mtp/MTPJNIInterface;

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Landroid/mtp/MTPJNIInterface;->sendPlaRemoved(Ljava/lang/String;)V

    .line 162
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "Playlist is deleted."

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 322
    .end local v15    # "dbEntries":[I
    .end local v18    # "entry":Ljava/util/Map$Entry;
    .end local v19    # "found":Z
    .end local v20    # "hashKey":Ljava/lang/Object;
    .end local v21    # "hashValue":Ljava/lang/String;
    .end local v22    # "i":I
    .end local v23    # "iter":Ljava/util/Iterator;
    .end local v26    # "playlistName":Ljava/lang/String;
    .end local v32    # "set":Ljava/util/Set;
    :catch_1
    move-exception v17

    .line 324
    .restart local v17    # "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 327
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 132
    .end local v17    # "e":Ljava/lang/Exception;
    .restart local v15    # "dbEntries":[I
    .restart local v18    # "entry":Ljava/util/Map$Entry;
    .restart local v19    # "found":Z
    .restart local v20    # "hashKey":Ljava/lang/Object;
    .restart local v21    # "hashValue":Ljava/lang/String;
    .restart local v22    # "i":I
    .restart local v23    # "iter":Ljava/util/Iterator;
    .restart local v32    # "set":Ljava/util/Set;
    :cond_6
    add-int/lit8 v22, v22, 0x1

    goto :goto_3

    .line 148
    .restart local v26    # "playlistName":Ljava/lang/String;
    :cond_7
    :try_start_3
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "In Delete playlistname already contains pla "

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    move-object/from16 v26, v21

    goto :goto_4

    .line 168
    .end local v15    # "dbEntries":[I
    .end local v18    # "entry":Ljava/util/Map$Entry;
    .end local v19    # "found":Z
    .end local v20    # "hashKey":Ljava/lang/Object;
    .end local v21    # "hashValue":Ljava/lang/String;
    .end local v22    # "i":I
    .end local v23    # "iter":Ljava/util/Iterator;
    .end local v26    # "playlistName":Ljava/lang/String;
    .end local v32    # "set":Ljava/util/Set;
    :cond_8
    sget v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    if-le v12, v3, :cond_f

    .line 170
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v3

    sput v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    .line 171
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Case is for adding the playlist"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v7, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    invoke-interface {v13}, Landroid/database/Cursor;->moveToLast()Z

    .line 173
    const-string v3, "name"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 175
    .local v24, "name":Ljava/lang/String;
    const-string v3, "_data"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 176
    .local v25, "path":Ljava/lang/String;
    const-string v3, ".pla"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 177
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "playlist is created by MTP, so don\'t send event"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :goto_5
    const-string v3, "_id"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v31

    .line 189
    .local v31, "rowId":I
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "In Creation Row ID is"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "InCreation Path is"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v3, ".pla"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 192
    const-string v3, ".pla"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 193
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "path is"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_9
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->map:Ljava/util/LinkedHashMap;

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v3, v5, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 327
    .end local v24    # "name":Ljava/lang/String;
    .end local v25    # "path":Ljava/lang/String;
    .end local v31    # "rowId":I
    :cond_a
    :goto_6
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 178
    .restart local v24    # "name":Ljava/lang/String;
    .restart local v25    # "path":Ljava/lang/String;
    :cond_b
    :try_start_4
    const-string v3, "/"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "\\"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, ":"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "*"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "?"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "\""

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "<"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, ">"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "|"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 181
    :cond_c
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "playlist file name is having special character so not sending sendPlaAdded"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_5

    .line 327
    .end local v24    # "name":Ljava/lang/String;
    .end local v25    # "path":Ljava/lang/String;
    :catchall_0
    move-exception v3

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v3

    .line 182
    .restart local v24    # "name":Ljava/lang/String;
    .restart local v25    # "path":Ljava/lang/String;
    :cond_d
    :try_start_5
    const-string v3, "/Playlists/"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 183
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "Playlist  path contains Playlist so creating it."

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->createPla(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 186
    :cond_e
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "Playlist  path contains Music folder so descarding it."

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 199
    .end local v24    # "name":Ljava/lang/String;
    .end local v25    # "path":Ljava/lang/String;
    :cond_f
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "Neither deleted nor added. Could be rename"

    invoke-static {v3, v5}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 203
    .local v16, "dbHash":Ljava/util/HashMap;
    const/16 v22, 0x0

    .line 204
    .restart local v22    # "i":I
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 205
    :goto_7
    sget v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mCount:I

    move/from16 v0, v22

    if-ge v0, v3, :cond_10

    .line 206
    const-string v3, "_id"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mRowId:I

    .line 208
    const-string v3, "name"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mPath:Ljava/lang/String;

    .line 209
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mRowId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mPath:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    .line 212
    add-int/lit8 v22, v22, 0x1

    goto :goto_7

    .line 215
    :cond_10
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->map:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v32

    .line 216
    .restart local v32    # "set":Ljava/util/Set;
    invoke-interface/range {v32 .. v32}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .line 217
    .restart local v23    # "iter":Ljava/util/Iterator;
    const/16 v21, 0x0

    .line 218
    .restart local v21    # "hashValue":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    .line 220
    .local v20, "hashKey":Ljava/lang/Integer;
    const/4 v11, 0x0

    .line 221
    .local v11, "DbVal":Ljava/lang/String;
    const/16 v29, 0x0

    .line 222
    .end local v20    # "hashKey":Ljava/lang/Integer;
    .local v29, "playlist_name":Ljava/lang/String;
    :goto_8
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v3

    if-eqz v3, :cond_16

    .line 224
    const/16 v18, 0x0

    .line 226
    .restart local v18    # "entry":Ljava/util/Map$Entry;
    :try_start_6
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/util/Map$Entry;

    move-object/from16 v18, v0
    :try_end_6
    .catch Ljava/util/ConcurrentModificationException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 230
    :goto_9
    :try_start_7
    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v20

    .line 231
    .local v20, "hashKey":Ljava/lang/Object;
    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v21

    .end local v21    # "hashValue":Ljava/lang/String;
    check-cast v21, Ljava/lang/String;

    .line 233
    .restart local v21    # "hashValue":Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "DbVal":Ljava/lang/String;
    check-cast v11, Ljava/lang/String;

    .line 235
    .restart local v11    # "DbVal":Ljava/lang/String;
    if-eqz v11, :cond_11

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_11

    if-eqz v21, :cond_11

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_12

    .line 236
    :cond_11
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "either DbVal or hashValue is NULL"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 327
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 227
    .end local v20    # "hashKey":Ljava/lang/Object;
    :catch_2
    move-exception v17

    .line 228
    .local v17, "e":Ljava/util/ConcurrentModificationException;
    :try_start_8
    invoke-virtual/range {v17 .. v17}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    goto :goto_9

    .line 241
    .end local v17    # "e":Ljava/util/ConcurrentModificationException;
    .restart local v20    # "hashKey":Ljava/lang/Object;
    :cond_12
    const-string v29, "/"

    .line 242
    move-object/from16 v0, v29

    invoke-virtual {v0, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 243
    const-string v3, ".pla"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 244
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "playlist_name is "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    move-object/from16 v0, v21

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 246
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "It is not renamed"

    invoke-static {v3, v5}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 248
    :cond_13
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "The DB is renamed."

    invoke-static {v3, v5}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    sget-object v6, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 250
    .local v6, "playlistUri":Landroid/net/Uri;
    const-string v27, ""

    .line 251
    .local v27, "playlistPath":Ljava/lang/String;
    const/16 v28, 0x0

    .line 252
    .local v28, "playlist_dir":Ljava/lang/String;
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v7, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "name=\""

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, "\""

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 253
    .local v14, "cur1":Landroid/database/Cursor;
    if-eqz v14, :cond_15

    .line 254
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    .line 255
    const-string v3, "_data"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 256
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "playlist path before updatePlaylistPath is"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "playlist path before updatePlaylistPath"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 260
    const-string v3, ".m3u"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string v3, ".M3U"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 261
    :cond_14
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "playlist is not renamed & m3u case"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 327
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 265
    :cond_15
    :try_start_9
    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 266
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "Playlist is renamed through MTP "

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    const-string v7, "/"

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ".pla"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    .line 268
    .local v30, "playlist_path":Ljava/lang/String;
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "playlist_path after renamed through MTP is "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->map:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v20

    move-object/from16 v1, v30

    invoke-virtual {v3, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    .end local v6    # "playlistUri":Landroid/net/Uri;
    .end local v14    # "cur1":Landroid/database/Cursor;
    .end local v18    # "entry":Ljava/util/Map$Entry;
    .end local v20    # "hashKey":Ljava/lang/Object;
    .end local v27    # "playlistPath":Ljava/lang/String;
    .end local v28    # "playlist_dir":Ljava/lang/String;
    .end local v30    # "playlist_path":Ljava/lang/String;
    :cond_16
    :goto_a
    invoke-virtual/range {v16 .. v16}, Ljava/util/HashMap;->clear()V

    goto/16 :goto_6

    .line 270
    .restart local v6    # "playlistUri":Landroid/net/Uri;
    .restart local v14    # "cur1":Landroid/database/Cursor;
    .restart local v18    # "entry":Ljava/util/Map$Entry;
    .restart local v20    # "hashKey":Ljava/lang/Object;
    .restart local v27    # "playlistPath":Ljava/lang/String;
    .restart local v28    # "playlist_dir":Ljava/lang/String;
    :cond_17
    const-string v3, "/Playlists/"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_18

    .line 271
    move-object/from16 v28, v21

    .line 272
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    const-string v7, "/"

    move-object/from16 v0, v28

    invoke-virtual {v0, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 274
    move-object/from16 v0, v28

    invoke-virtual {v0, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 275
    .restart local v30    # "playlist_path":Ljava/lang/String;
    const-string v3, ".pla"

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 276
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Rename 2nd case playlist_dir "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "  "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " -- "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " playlist_path  is  "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "  "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "Rename 2nd case playlist_dir"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mtpJniInterface:Landroid/mtp/MTPJNIInterface;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Landroid/mtp/MTPJNIInterface;->sendPlaRemoved(Ljava/lang/String;)V

    .line 285
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->map:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v20

    move-object/from16 v1, v30

    invoke-virtual {v3, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mtpJniInterface:Landroid/mtp/MTPJNIInterface;

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Landroid/mtp/MTPJNIInterface;->sendPlaAdded(Ljava/lang/String;)V

    .line 292
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v30

    invoke-virtual {v0, v11, v1, v2}, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->updatePlaylistPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 294
    .end local v30    # "playlist_path":Ljava/lang/String;
    :cond_18
    sget-object v28, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->playlist_std_dir:Ljava/lang/String;

    .line 295
    const-string v3, "/"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 296
    .restart local v30    # "playlist_path":Ljava/lang/String;
    move-object/from16 v0, v30

    invoke-virtual {v0, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 297
    const-string v3, ".pla"

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 298
    const-string v3, "MTPPlaObsrvr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Rename case"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const-string v3, "MTPPlaObsrvr"

    const-string v5, "Rename case"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mtpJniInterface:Landroid/mtp/MTPJNIInterface;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Landroid/mtp/MTPJNIInterface;->sendPlaRemoved(Ljava/lang/String;)V

    .line 306
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->map:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v20

    move-object/from16 v1, v30

    invoke-virtual {v3, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mtpJniInterface:Landroid/mtp/MTPJNIInterface;

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Landroid/mtp/MTPJNIInterface;->sendPlaAdded(Ljava/lang/String;)V

    .line 315
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v30

    invoke-virtual {v0, v11, v1, v2}, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->updatePlaylistPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_a
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const-string v0, "MTPPlaObsrvr"

    const-string v1, "inside setContext()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    sput-object p1, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mContext:Landroid/content/Context;

    .line 60
    sget-object v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 66
    :goto_0
    return-void

    .line 62
    :cond_0
    sget-object v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->cr:Landroid/content/ContentResolver;

    .line 63
    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->createPlaFiles()V

    .line 64
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->registerContentObserver()V

    .line 65
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mtpJniInterface:Landroid/mtp/MTPJNIInterface;

    goto :goto_0
.end method

.method updatePlaylistPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .param p1, "DbVal"    # Ljava/lang/String;
    .param p2, "hashValue"    # Ljava/lang/String;
    .param p3, "playlist_path"    # Ljava/lang/String;

    .prologue
    .line 332
    sget-object v9, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 333
    .local v9, "playlistUri":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 334
    .local v7, "noOfRowsUpdated":I
    const-wide/16 v2, 0x0

    .line 335
    .local v2, "dateModified":J
    const/4 v5, 0x0

    .line 336
    .local v5, "file":Ljava/io/File;
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 337
    .local v10, "values":Landroid/content/ContentValues;
    const-string v11, "MTPPlaObsrvr"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "updatePlaylistPath DbVal is = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    const-string v11, "MTPPlaObsrvr"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "updatePlaylistPath  hashValue is= "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v11, "MTPPlaObsrvr"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "updatePlaylistPath  playlist_path = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v12, 0x0

    const-string v13, "/"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 341
    .local v8, "playlistPathWithoutPla":Ljava/lang/String;
    const-string v11, "MTPPlaObsrvr"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "updatePlaylistPath  playlistPathWithoutPla = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    :try_start_0
    new-instance v6, Ljava/io/File;

    sget-object v11, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->playlist_test_std_dir:Ljava/lang/String;

    invoke-direct {v6, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 346
    .end local v5    # "file":Ljava/io/File;
    .local v6, "file":Ljava/io/File;
    :try_start_1
    invoke-virtual {v6}, Ljava/io/File;->exists()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v11

    if-nez v11, :cond_0

    .line 348
    :try_start_2
    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 353
    :cond_0
    :goto_0
    :try_start_3
    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v12

    const-wide/16 v14, 0x3e8

    div-long v2, v12, v14

    .line 354
    const-string v11, "MTPPlaObsrvr"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "In updatePlaylistPath dateModified is = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-object v5, v6

    .line 363
    .end local v6    # "file":Ljava/io/File;
    .restart local v5    # "file":Ljava/io/File;
    :goto_1
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 364
    const-string v11, "MTPPlaObsrvr"

    const-string v12, "In updatePlaylistPath file deleated "

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    :goto_2
    const-string v11, "name"

    move-object/from16 v0, p1

    invoke-virtual {v10, v11, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    const-string v11, "_data"

    invoke-virtual {v10, v11, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string v11, "date_modified"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 371
    sget-object v11, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mContext:Landroid/content/Context;

    if-nez v11, :cond_2

    .line 380
    :goto_3
    return-void

    .line 349
    .end local v5    # "file":Ljava/io/File;
    .restart local v6    # "file":Ljava/io/File;
    :catch_0
    move-exception v4

    .line 350
    .local v4, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 356
    .end local v4    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    move-object v5, v6

    .line 358
    .end local v6    # "file":Ljava/io/File;
    .local v4, "e":Ljava/lang/Exception;
    .restart local v5    # "file":Ljava/io/File;
    :goto_4
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 366
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v11, "MTPPlaObsrvr"

    const-string v12, "In updatePlaylistPath file not deleated "

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 373
    :cond_2
    sget-object v11, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "name=\""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v11, v9, v10, v12, v13}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 374
    const-string v11, "MTPPlaObsrvr"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "noOfRowsUpdated = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/mtp/MTPJNIInterface;->MTP_LOG_PRINT(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    const/4 v11, 0x1

    if-ne v7, v11, :cond_3

    .line 376
    const-string v11, "MTPPlaObsrvr"

    const-string v12, "playlist rename DB updation sucessful"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 378
    :cond_3
    const-string v11, "MTPPlaObsrvr"

    const-string v12, "Playlist Rename DB updation failed"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 356
    :catch_2
    move-exception v4

    goto :goto_4
.end method
