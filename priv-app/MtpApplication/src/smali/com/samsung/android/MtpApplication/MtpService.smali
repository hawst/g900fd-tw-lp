.class public Lcom/samsung/android/MtpApplication/MtpService;
.super Landroid/app/Service;
.source "MtpService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;
    }
.end annotation


# static fields
.field static EmergencyRegistered:Z

.field static MtpMediaReceiver:Z

.field static PolicyRegistered:Z

.field static mtpServiceHandler:Landroid/os/Handler;

.field static sdCardCurrentStatus:Z

.field static userPresentRegistered:Z


# instance fields
.field binder:Landroid/os/Binder;

.field private jniObj:Landroid/mtp/MTPJNIInterface;

.field private mContext:Landroid/content/Context;

.field mNotiManager:Landroid/app/NotificationManager;

.field private mServiceHandler:Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;

.field private mServiceLooper:Landroid/os/Looper;

.field private final mtpEmergencyReceiver:Landroid/content/BroadcastReceiver;

.field private final mtpMediaReceiver:Landroid/content/BroadcastReceiver;

.field private final mtpPolicyReceiver:Landroid/content/BroadcastReceiver;

.field private mtpUserPresentReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpService;->mtpServiceHandler:Landroid/os/Handler;

    .line 49
    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->userPresentRegistered:Z

    .line 50
    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->PolicyRegistered:Z

    .line 51
    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->MtpMediaReceiver:Z

    .line 52
    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->EmergencyRegistered:Z

    .line 53
    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->sdCardCurrentStatus:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->mServiceHandler:Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;

    .line 43
    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;

    .line 44
    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->mServiceLooper:Landroid/os/Looper;

    .line 45
    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->mContext:Landroid/content/Context;

    .line 46
    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->mNotiManager:Landroid/app/NotificationManager;

    .line 54
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->binder:Landroid/os/Binder;

    .line 216
    new-instance v0, Lcom/samsung/android/MtpApplication/MtpService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/MtpApplication/MtpService$1;-><init>(Lcom/samsung/android/MtpApplication/MtpService;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpPolicyReceiver:Landroid/content/BroadcastReceiver;

    .line 254
    new-instance v0, Lcom/samsung/android/MtpApplication/MtpService$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/MtpApplication/MtpService$2;-><init>(Lcom/samsung/android/MtpApplication/MtpService;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpEmergencyReceiver:Landroid/content/BroadcastReceiver;

    .line 322
    new-instance v0, Lcom/samsung/android/MtpApplication/MtpService$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/MtpApplication/MtpService$3;-><init>(Lcom/samsung/android/MtpApplication/MtpService;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpMediaReceiver:Landroid/content/BroadcastReceiver;

    .line 423
    new-instance v0, Lcom/samsung/android/MtpApplication/MtpService$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/MtpApplication/MtpService$4;-><init>(Lcom/samsung/android/MtpApplication/MtpService;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpUserPresentReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/mtp/MTPJNIInterface;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpService;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/MtpApplication/MtpService;Landroid/mtp/MTPJNIInterface;)Landroid/mtp/MTPJNIInterface;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpService;
    .param p1, "x1"    # Landroid/mtp/MTPJNIInterface;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpService;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/MtpApplication/MtpService;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/MtpApplication/MtpService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpService;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/samsung/android/MtpApplication/MtpService;->manageProcessForeground(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/MtpApplication/MtpService;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/samsung/android/MtpApplication/MtpService;->unRegisterAllIntent(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/MtpApplication/MtpService;)Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpService;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->mServiceHandler:Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpService;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpUserPresentReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method public static isUsbDisconnected()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 452
    const/4 v2, 0x0

    .line 453
    .local v2, "usbStatus":Ljava/lang/String;
    const-string v1, "/sys/class/android_usb/android0/state"

    .line 455
    .local v1, "path":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x20

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 460
    if-eqz v2, :cond_0

    const-string v4, "DISCONNECTED"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 461
    const-string v3, "MtpService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/sys/class/android_usb/android0/state is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    const/4 v3, 0x1

    .line 464
    :cond_0
    :goto_0
    return v3

    .line 456
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "MtpService"

    const-string v5, "cannot open file : /sys/class/android_usb/android0/state"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private manageProcessForeground(Z)V
    .locals 4
    .param p1, "start"    # Z

    .prologue
    .line 172
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    .line 174
    .local v1, "mAm":Landroid/app/IActivityManager;
    if-eqz v1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/MtpApplication/MtpService;->binder:Landroid/os/Binder;

    if-eqz v2, :cond_0

    .line 175
    iget-object v2, p0, Lcom/samsung/android/MtpApplication/MtpService;->binder:Landroid/os/Binder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-interface {v1, v2, v3, p1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private registerBroadCastEmergencyRec()V
    .locals 3

    .prologue
    .line 486
    const-string v1, "MtpService"

    const-string v2, "< MTP > Registering BroadCast registerBroadCastEmergencyRec :::::"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 488
    .local v0, "intentfilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 491
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpEmergencyReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/MtpApplication/MtpService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 492
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->EmergencyRegistered:Z

    .line 493
    return-void
.end method

.method private registerBroadCastPolicyRec()V
    .locals 4

    .prologue
    .line 477
    const-string v1, "MtpService"

    const-string v2, "< MTP > Registering BroadCast registerBroadCastPolicyRec :::::"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 479
    .local v0, "policyF":Landroid/content/IntentFilter;
    const-string v1, "edm.intent.action.disable.mtp"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 480
    const-string v1, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 482
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpPolicyReceiver:Landroid/content/BroadcastReceiver;

    const-string v2, "com.sec.restriction.permission.MTP_DISABLED"

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/samsung/android/MtpApplication/MtpService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 483
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->PolicyRegistered:Z

    .line 484
    return-void
.end method

.method private registerBroadCastRec()V
    .locals 3

    .prologue
    .line 495
    const-string v1, "MtpService"

    const-string v2, "< MTP > Registering BroadCast receiver :::::"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 501
    .local v0, "lIF":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 502
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 503
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 504
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 505
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/MtpApplication/MtpService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 506
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->MtpMediaReceiver:Z

    .line 507
    return-void
.end method

.method private registerBroadCastuserPresentRec()V
    .locals 3

    .prologue
    .line 467
    const-string v1, "MtpService"

    const-string v2, "< MTP > Registering BroadCast receiver for USER Present:::::"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 469
    .local v0, "mIntentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 473
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpUserPresentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/MtpApplication/MtpService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 474
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->userPresentRegistered:Z

    .line 475
    return-void
.end method

.method static setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p0, "handler"    # Landroid/os/Handler;

    .prologue
    .line 82
    sput-object p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpServiceHandler:Landroid/os/Handler;

    .line 83
    return-void
.end method

.method private unRegisterAllIntent(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 285
    :try_start_0
    sget-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->PolicyRegistered:Z

    if-ne v1, v3, :cond_0

    .line 286
    const-string v1, "MtpService"

    const-string v2, "Unregister Mtp disable Receiver"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpPolicyReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 288
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->PolicyRegistered:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    :cond_0
    :goto_0
    :try_start_1
    sget-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->EmergencyRegistered:Z

    if-ne v1, v3, :cond_1

    .line 295
    const-string v1, "MtpService"

    const-string v2, "Unregister mtpEmergencyReceiver"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpEmergencyReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 297
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->EmergencyRegistered:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 304
    :cond_1
    :goto_1
    :try_start_2
    sget-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->userPresentRegistered:Z

    if-ne v1, v3, :cond_2

    .line 305
    const-string v1, "MtpService"

    const-string v2, "unregistering mtpUserPresentReceiver in UnregisterAllIntent"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpUserPresentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 307
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->userPresentRegistered:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 313
    :cond_2
    :goto_2
    :try_start_3
    sget-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->MtpMediaReceiver:Z

    if-ne v1, v3, :cond_3

    .line 314
    const-string v1, "MtpService"

    const-string v2, "unregistering mtpMediaReceiver in UnregisterAllIntent"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mtpMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 316
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpService;->MtpMediaReceiver:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 321
    :cond_3
    :goto_3
    return-void

    .line 290
    :catch_0
    move-exception v0

    .line 291
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MtpService"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 299
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 300
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "MtpService"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 309
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 310
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "MtpService"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 318
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v0

    .line 319
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "MtpService"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 111
    const-string v0, "MtpService"

    const-string v1, "onBind."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 57
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 58
    const-string v1, "MtpService"

    const-string v2, "onCreate."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MtpService"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 61
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 63
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mServiceLooper:Landroid/os/Looper;

    .line 64
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mServiceLooper:Landroid/os/Looper;

    if-eqz v1, :cond_0

    .line 65
    new-instance v1, Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;

    iget-object v2, p0, Lcom/samsung/android/MtpApplication/MtpService;->mServiceLooper:Landroid/os/Looper;

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;-><init>(Lcom/samsung/android/MtpApplication/MtpService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mServiceHandler:Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;

    .line 67
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpService;->registerBroadCastRec()V

    .line 68
    invoke-static {}, Lcom/samsung/android/MtpApplication/MtpReceiver;->getMtpContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mContext:Landroid/content/Context;

    .line 69
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 70
    const-string v1, "MtpService"

    const-string v2, "mContext is NULL so getting the getApplicationContext"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/MtpService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mContext:Landroid/content/Context;

    .line 73
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "mtp_drive_display"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_2

    .line 74
    const-string v1, "MtpService"

    const-string v2, "Calling registerBroadCastuserPresentRec() "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpService;->registerBroadCastuserPresentRec()V

    .line 77
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpService;->registerBroadCastPolicyRec()V

    .line 78
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpService;->registerBroadCastEmergencyRec()V

    .line 79
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 96
    const-string v0, "MtpService"

    const-string v1, "onDestroy."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/MtpApplication/MtpService;->manageProcessForeground(Z)V

    .line 99
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/samsung/android/MtpApplication/MtpService;->unRegisterAllIntent(Landroid/content/Context;)V

    .line 100
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpService;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 105
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 106
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 86
    const-string v1, "MtpService"

    const-string v2, "onStartCommand."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mServiceHandler:Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;

    invoke-virtual {v1}, Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 89
    .local v0, "msg":Landroid/os/Message;
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 90
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 91
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService;->mServiceHandler:Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;

    invoke-virtual {v1, v0}, Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 92
    const/4 v1, 0x1

    return v1
.end method
