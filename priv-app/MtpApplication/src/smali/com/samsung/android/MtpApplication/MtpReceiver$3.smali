.class Lcom/samsung/android/MtpApplication/MtpReceiver$3;
.super Landroid/os/Handler;
.source "MtpReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/MtpApplication/MtpReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;


# direct methods
.method constructor <init>(Lcom/samsung/android/MtpApplication/MtpReceiver;)V
    .locals 0

    .prologue
    .line 620
    iput-object p1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$3;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v8, 0xe

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 622
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 624
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 738
    :pswitch_0
    const-string v3, "MTPRx"

    const-string v4, " Default case"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    :cond_0
    :goto_0
    return-void

    .line 626
    :pswitch_1
    const-string v3, "MTPRx"

    const-string v4, "calling native method"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 629
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v3, :cond_0

    .line 630
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    iget-object v4, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$3;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    iget-object v4, v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->setHandler(Landroid/os/Handler;)V

    .line 631
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    sget-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->setContext(Landroid/content/Context;)V

    .line 632
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$3;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    iget-object v3, v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-virtual {v3, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 633
    const-string v3, "MTPRx"

    const-string v4, "Checking the driver time out"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$3;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    # invokes: Lcom/samsung/android/MtpApplication/MtpReceiver;->sendMsg(I)V
    invoke-static {v3, v8}, Lcom/samsung/android/MtpApplication/MtpReceiver;->access$500(Lcom/samsung/android/MtpApplication/MtpReceiver;I)V

    .line 635
    sput-boolean v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->displayDriverPopup:Z

    .line 636
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    invoke-virtual {v3, v7}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    .line 637
    const-string v3, "MTPRx"

    const-string v4, "called native method"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-nez v3, :cond_1

    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 639
    :cond_1
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    invoke-virtual {v3, v6}, Landroid/mtp/MTPJNIInterface;->setMediaScannerStatus(I)V

    goto :goto_0

    .line 643
    :pswitch_2
    const-string v3, "MTPRx"

    const-string v4, "MTP can be launched"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    if-nez v3, :cond_2

    .line 645
    const-string v3, "MTPRx"

    const-string v4, "context is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 648
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$3;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    sget-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    # invokes: Lcom/samsung/android/MtpApplication/MtpReceiver;->sendNoti(Landroid/content/Context;Landroid/content/ContentResolver;I)V
    invoke-static {v3, v4, v5, v7}, Lcom/samsung/android/MtpApplication/MtpReceiver;->access$600(Lcom/samsung/android/MtpApplication/MtpReceiver;Landroid/content/Context;Landroid/content/ContentResolver;I)V

    goto :goto_0

    .line 651
    :pswitch_3
    const-string v3, "MTPRx"

    const-string v4, "restarting MTP"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    if-nez v3, :cond_3

    .line 653
    const-string v3, "MTPRx"

    const-string v4, "mContext is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$3;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    sget-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    # invokes: Lcom/samsung/android/MtpApplication/MtpReceiver;->launchMtpApp(Landroid/content/Context;Landroid/content/ContentResolver;)V
    invoke-static {v3, v4, v5}, Lcom/samsung/android/MtpApplication/MtpReceiver;->access$700(Lcom/samsung/android/MtpApplication/MtpReceiver;Landroid/content/Context;Landroid/content/ContentResolver;)V

    goto/16 :goto_0

    .line 658
    :pswitch_4
    const-string v3, "MTPRx"

    const-string v4, "calling native method"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 661
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v3, :cond_0

    .line 662
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    iget-object v4, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$3;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    iget-object v4, v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->setHandler(Landroid/os/Handler;)V

    .line 663
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    sget-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->setContext(Landroid/content/Context;)V

    .line 664
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    goto/16 :goto_0

    .line 669
    :pswitch_5
    const-string v3, "MTPRx"

    const-string v4, "calling native method"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 672
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v3, :cond_0

    .line 673
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    iget-object v4, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$3;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    iget-object v4, v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->setHandler(Landroid/os/Handler;)V

    .line 674
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    sget-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->setContext(Landroid/content/Context;)V

    .line 675
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    const/16 v4, 0xb

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    goto/16 :goto_0

    .line 680
    :pswitch_6
    const-string v3, "MTPRx"

    const-string v4, "calling native method"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 683
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v3, :cond_0

    .line 684
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    iget-object v4, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$3;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    iget-object v4, v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->setHandler(Landroid/os/Handler;)V

    .line 685
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    sget-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->setContext(Landroid/content/Context;)V

    .line 686
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    goto/16 :goto_0

    .line 691
    :pswitch_7
    const-string v3, "MTPRx"

    const-string v4, "calling native method"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 694
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v3, :cond_0

    .line 695
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    iget-object v4, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$3;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    iget-object v4, v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->setHandler(Landroid/os/Handler;)V

    .line 696
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    sget-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->setContext(Landroid/content/Context;)V

    .line 697
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    goto/16 :goto_0

    .line 702
    :pswitch_8
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    # setter for: Lcom/samsung/android/MtpApplication/MtpReceiver;->mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v3}, Lcom/samsung/android/MtpApplication/MtpReceiver;->access$802(Landroid/telephony/TelephonyManager;)Landroid/telephony/TelephonyManager;

    .line 703
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    # getter for: Lcom/samsung/android/MtpApplication/MtpReceiver;->mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {}, Lcom/samsung/android/MtpApplication/MtpReceiver;->access$800()Landroid/telephony/TelephonyManager;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/os/FactoryTest;->isFactoryMode(Landroid/content/Context;Landroid/telephony/TelephonyManager;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 704
    sput-boolean v6, Lcom/samsung/android/MtpApplication/MtpReceiver;->displayDriverPopup:Z

    .line 705
    const-string v3, "MTPRx"

    const-string v4, "DRIVER_TIME_OUT 60s lapsed, but Factory mode"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 706
    :cond_4
    invoke-static {}, Lcom/samsung/android/MtpApplication/MtpService;->isUsbDisconnected()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 707
    sput-boolean v6, Lcom/samsung/android/MtpApplication/MtpReceiver;->displayDriverPopup:Z

    .line 708
    const-string v3, "MTPRx"

    const-string v4, "DRIVER_TIME_OUT 60s lapsed, but do not started activity for popup."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 709
    :cond_5
    sget-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->displayDriverPopup:Z

    if-ne v5, v3, :cond_0

    .line 710
    const-string v3, "MTPRx"

    const-string v4, "DRIVER_TIME_OUT 60s lapsed"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    const-string v3, "MTPRx"

    const-string v4, "still no open session command from host, so toast"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 714
    .local v2, "resolverIntent":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 715
    const-string v3, "com.android.settings"

    const-string v4, "com.android.settings.SettingsReceiverActivity"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 717
    :try_start_0
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 718
    const-string v3, "MTPRx"

    const-string v4, "started activity for popup"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 734
    :goto_1
    sput-boolean v6, Lcom/samsung/android/MtpApplication/MtpReceiver;->displayDriverPopup:Z

    goto/16 :goto_0

    .line 720
    :catch_0
    move-exception v0

    .line 721
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "MTPRx"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unable to start activity "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 722
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v1

    .line 723
    .local v1, "f":Ljava/lang/Exception;
    const-string v3, "MTPRx"

    const-string v4, "exception in starting activity"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 624
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method
