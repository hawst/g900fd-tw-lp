.class Lcom/samsung/android/MtpApplication/MtpService$1;
.super Landroid/content/BroadcastReceiver;
.source "MtpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/MtpApplication/MtpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/MtpApplication/MtpService;


# direct methods
.method constructor <init>(Lcom/samsung/android/MtpApplication/MtpService;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/samsung/android/MtpApplication/MtpService$1;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v8, 0x13

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 218
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, "action":Ljava/lang/String;
    const-string v3, "MtpService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "In MTPAPP onReceive:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    const-string v3, "edm.intent.action.disable.mtp"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 222
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "mtp_running_status"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v7, v3, :cond_0

    .line 223
    const-string v3, "MtpService"

    const-string v4, "In MTPAPP inside disable drive in mtp disabled"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    invoke-static {p1}, Lcom/samsung/android/MtpApplication/MtpReceiver;->disableDrive(Landroid/content/Context;)Z

    .line 225
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$1;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # invokes: Lcom/samsung/android/MtpApplication/MtpService;->unRegisterAllIntent(Landroid/content/Context;)V
    invoke-static {v3, p1}, Lcom/samsung/android/MtpApplication/MtpService;->access$300(Lcom/samsung/android/MtpApplication/MtpService;Landroid/content/Context;)V

    .line 226
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpService;->mtpServiceHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    .line 227
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpService;->mtpServiceHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 228
    .local v2, "msg":Landroid/os/Message;
    iput v8, v2, Landroid/os/Message;->what:I

    .line 229
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpService;->mtpServiceHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 252
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    const-string v3, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 233
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "mtp_running_status"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v7, v3, :cond_0

    .line 234
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$1;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/MtpApplication/MtpService;->access$100(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/MtpApplication/MtpService$1;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->mServiceHandler:Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;
    invoke-static {v4}, Lcom/samsung/android/MtpApplication/MtpService;->access$400(Lcom/samsung/android/MtpApplication/MtpService;)Lcom/samsung/android/MtpApplication/MtpService$ServiceHandler;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/app/admin/DevicePolicyManager;->create(Landroid/content/Context;Landroid/os/Handler;)Landroid/app/admin/DevicePolicyManager;

    move-result-object v1

    .line 235
    .local v1, "dpm":Landroid/app/admin/DevicePolicyManager;
    const-string v3, "MtpService"

    const-string v4, "In MTPAPP inside disable drive in policy state changed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    if-nez v1, :cond_2

    .line 237
    const-string v3, "MtpService"

    const-string v4, " dpm is null. So return"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 240
    :cond_2
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/app/admin/DevicePolicyManager;->getAllowDesktopSync(Landroid/content/ComponentName;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 241
    invoke-static {p1}, Lcom/samsung/android/MtpApplication/MtpReceiver;->disableDrive(Landroid/content/Context;)Z

    .line 242
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$1;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # invokes: Lcom/samsung/android/MtpApplication/MtpService;->unRegisterAllIntent(Landroid/content/Context;)V
    invoke-static {v3, p1}, Lcom/samsung/android/MtpApplication/MtpService;->access$300(Lcom/samsung/android/MtpApplication/MtpService;Landroid/content/Context;)V

    .line 243
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpService;->mtpServiceHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    .line 244
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpService;->mtpServiceHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 245
    .restart local v2    # "msg":Landroid/os/Message;
    iput v8, v2, Landroid/os/Message;->what:I

    .line 246
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpService;->mtpServiceHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
