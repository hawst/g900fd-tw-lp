.class public Lcom/samsung/android/MtpApplication/MtpReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MtpReceiver.java"


# static fields
.field static PrivateExists:Z

.field static final Usb30SupportedSwitchMode:[Ljava/lang/String;

.field static final Usb30notiDevice:Ljava/lang/String;

.field static adbUpdate:Lcom/samsung/android/MtpApplication/MtpAdbObserver;

.field static boot_completed:Ljava/lang/String;

.field static configured:Z

.field static connected:Z

.field static currentUserId:I

.field static dev_boot_completed:Ljava/lang/String;

.field static displayDriverPopup:Z

.field private static firstMediaMount:Z

.field static firstTimeResetDone:Z

.field static gadgetReset:Z

.field static isConnectedwithdriver:Z

.field static isSynchronizing:Z

.field static lockStatusSet:Z

.field static mContext:Landroid/content/Context;

.field private static mDvfsHelper:Landroid/os/DVFSHelper;

.field private static mFirstTime:Z

.field private static mTelephonyManager:Landroid/telephony/TelephonyManager;

.field static mountPath:Ljava/lang/String;

.field static mtpEnabled:Z

.field static notifyAppHandler:Landroid/os/Handler;

.field static playlistUpdate:Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;

.field static privatemodeSet:Z

.field static ptpEnabled:Z

.field static qcomIgnoretwice:Z

.field private static screenWakeLock:Landroid/os/PowerManager$WakeLock;

.field static sdCardExists:Z

.field static sendBooster:Z

.field static statusUpdate:Landroid/mtp/MTPJNIInterface;

.field static usb30IdleTimer:Z

.field static usb30Mode:Z


# instance fields
.field private final MTPINITSTATUS_PATH:Ljava/lang/String;

.field private admin:Landroid/content/ComponentName;

.field private dpm:Landroid/app/admin/DevicePolicyManager;

.field dvfsLockIntent:Landroid/content/Intent;

.field iService:Landroid/content/Intent;

.field private isBootConnection:Z

.field mNotiHandler:Landroid/os/Handler;

.field mStartMtpHandler:Landroid/os/Handler;

.field private final mUEventObserver:Landroid/os/UEventObserver;

.field final toastHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 94
    sput-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mDvfsHelper:Landroid/os/DVFSHelper;

    .line 104
    sput-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 105
    sput-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->playlistUpdate:Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;

    .line 106
    sput-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->adbUpdate:Lcom/samsung/android/MtpApplication/MtpAdbObserver;

    .line 107
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpReceiver;->Usb30notiDevice:Ljava/lang/String;

    .line 108
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "ha3g"

    aput-object v1, v0, v3

    const-string v1, "hlte"

    aput-object v1, v0, v5

    const/4 v1, 0x2

    const-string v2, "k3g"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "klte"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "lentislte"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "h3g"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "htdlte"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpReceiver;->Usb30SupportedSwitchMode:[Ljava/lang/String;

    .line 110
    sput-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->notifyAppHandler:Landroid/os/Handler;

    .line 112
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->firstMediaMount:Z

    .line 125
    sput-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->boot_completed:Ljava/lang/String;

    .line 126
    sput-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->dev_boot_completed:Ljava/lang/String;

    .line 134
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->isSynchronizing:Z

    .line 135
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->displayDriverPopup:Z

    .line 136
    sput-boolean v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->isConnectedwithdriver:Z

    .line 156
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->lockStatusSet:Z

    .line 163
    const-string v0, ""

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mountPath:Ljava/lang/String;

    .line 164
    sput v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->currentUserId:I

    .line 172
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->connected:Z

    .line 173
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->configured:Z

    .line 174
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mtpEnabled:Z

    .line 175
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->ptpEnabled:Z

    .line 178
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendBooster:Z

    .line 179
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->gadgetReset:Z

    .line 180
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->qcomIgnoretwice:Z

    .line 181
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->firstTimeResetDone:Z

    .line 184
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mFirstTime:Z

    .line 186
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->sdCardExists:Z

    .line 187
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->PrivateExists:Z

    .line 188
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->privatemodeSet:Z

    .line 189
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->usb30Mode:Z

    .line 190
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->usb30IdleTimer:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 92
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 99
    const-string v0, "/sys/class/sec/switch/MtpInitStatusSel"

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->MTPINITSTATUS_PATH:Ljava/lang/String;

    .line 109
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->toastHandler:Landroid/os/Handler;

    .line 117
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.samsung.android.MtpApplication"

    const-string v2, "MtpReceiver"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->admin:Landroid/content/ComponentName;

    .line 118
    sget-object v0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->toastHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Landroid/app/admin/DevicePolicyManager;->create(Landroid/content/Context;Landroid/os/Handler;)Landroid/app/admin/DevicePolicyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->dpm:Landroid/app/admin/DevicePolicyManager;

    .line 121
    iput-object v3, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->iService:Landroid/content/Intent;

    .line 185
    iput-object v3, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->dvfsLockIntent:Landroid/content/Intent;

    .line 191
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->isBootConnection:Z

    .line 199
    new-instance v0, Lcom/samsung/android/MtpApplication/MtpReceiver$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/MtpApplication/MtpReceiver$1;-><init>(Lcom/samsung/android/MtpApplication/MtpReceiver;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mUEventObserver:Landroid/os/UEventObserver;

    .line 425
    new-instance v0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/MtpApplication/MtpReceiver$2;-><init>(Lcom/samsung/android/MtpApplication/MtpReceiver;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    .line 620
    new-instance v0, Lcom/samsung/android/MtpApplication/MtpReceiver$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/MtpApplication/MtpReceiver$3;-><init>(Lcom/samsung/android/MtpApplication/MtpReceiver;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mStartMtpHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/MtpApplication/MtpReceiver;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpReceiver;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->gadgetDisconnectAndConnectCheck()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/MtpApplication/MtpReceiver;)Landroid/os/UEventObserver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpReceiver;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mUEventObserver:Landroid/os/UEventObserver;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/MtpApplication/MtpReceiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpReceiver;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->usbRemoved()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/MtpApplication/MtpReceiver;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpReceiver;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/samsung/android/MtpApplication/MtpReceiver;->acquireWakeLock(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/MtpApplication/MtpReceiver;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpReceiver;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/samsung/android/MtpApplication/MtpReceiver;->enableUsb30(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/MtpApplication/MtpReceiver;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpReceiver;
    .param p1, "x1"    # I

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendMsg(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/MtpApplication/MtpReceiver;Landroid/content/Context;Landroid/content/ContentResolver;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpReceiver;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/content/ContentResolver;
    .param p3, "x3"    # I

    .prologue
    .line 92
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendNoti(Landroid/content/Context;Landroid/content/ContentResolver;I)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/MtpApplication/MtpReceiver;Landroid/content/Context;Landroid/content/ContentResolver;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/MtpApplication/MtpReceiver;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/content/ContentResolver;

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/MtpApplication/MtpReceiver;->launchMtpApp(Landroid/content/Context;Landroid/content/ContentResolver;)V

    return-void
.end method

.method static synthetic access$800()Landroid/telephony/TelephonyManager;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method static synthetic access$802(Landroid/telephony/TelephonyManager;)Landroid/telephony/TelephonyManager;
    .locals 0
    .param p0, "x0"    # Landroid/telephony/TelephonyManager;

    .prologue
    .line 92
    sput-object p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-object p0
.end method

.method private acquireWakeLock(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 566
    const-string v1, "power"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 567
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x10000006

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 570
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 571
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 573
    :cond_0
    return-void
.end method

.method static disableDrive(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 842
    if-nez p0, :cond_0

    .line 843
    const-string v2, "MTPRx"

    const-string v3, "context is NULL in disable drive"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 854
    :goto_0
    return v1

    .line 846
    :cond_0
    const-string v2, "MTPRx"

    const-string v3, "disabling the Drive"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 848
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v2, "mtp_drive_display"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 849
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 850
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v1, :cond_1

    .line 851
    const-string v1, "MTPRx"

    const-string v2, "Sending MTP_MODE_RESTRICTED_POLICY to stack"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    .line 854
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private enableUsb30(Z)V
    .locals 5
    .param p1, "enable"    # Z

    .prologue
    .line 556
    const-string v2, "MTPRx"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "in enableUsb30 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    :try_start_0
    sget-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    const-string v3, "usb"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbManager;

    .line 559
    .local v1, "usbManager":Landroid/hardware/usb/UsbManager;
    invoke-virtual {v1, p1}, Landroid/hardware/usb/UsbManager;->setUsb30Mode(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 563
    .end local v1    # "usbManager":Landroid/hardware/usb/UsbManager;
    :goto_0
    return-void

    .line 560
    :catch_0
    move-exception v0

    .line 561
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "MTPRx"

    const-string v3, "Unable to set usb mode"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private gadgetDisconnectAndConnectCheck()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 385
    const/4 v5, 0x0

    .line 386
    .local v5, "status":I
    const/4 v2, 0x0

    .line 387
    .local v2, "input":Ljava/io/FileInputStream;
    const-string v4, "/data/data/com.samsung.android.MtpApplication/gadgetDisconnectAndConnect.txt"

    .line 389
    .local v4, "path":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 390
    .end local v2    # "input":Ljava/io/FileInputStream;
    .local v3, "input":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->read()I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v5

    .line 401
    if-eqz v3, :cond_0

    .line 402
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 408
    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    :cond_1
    :goto_0
    new-instance v1, Ljava/io/File;

    const-string v8, "/data/data/com.samsung.android.MtpApplication/gadgetDisconnectAndConnect.txt"

    invoke-direct {v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 409
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 410
    sget-boolean v8, Lcom/samsung/android/MtpApplication/MtpReceiver;->qcomIgnoretwice:Z

    if-nez v8, :cond_5

    .line 411
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 415
    :goto_1
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 418
    :cond_2
    if-ne v6, v5, :cond_6

    .line 421
    .end local v1    # "f":Ljava/io/File;
    :goto_2
    return v6

    .line 404
    .end local v2    # "input":Ljava/io/FileInputStream;
    .restart local v3    # "input":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 405
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v2, v3

    .line 407
    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    goto :goto_0

    .line 391
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 401
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_3
    if-eqz v2, :cond_3

    .line 402
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_3
    :goto_4
    move v6, v7

    .line 406
    goto :goto_2

    .line 404
    .restart local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v0

    .line 405
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 396
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v0

    .line 397
    .restart local v0    # "e":Ljava/lang/Exception;
    :goto_5
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 401
    if-eqz v2, :cond_1

    .line 402
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 404
    :catch_4
    move-exception v0

    .line 405
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 400
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 401
    :goto_6
    if-eqz v2, :cond_4

    .line 402
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    .line 406
    :cond_4
    :goto_7
    throw v6

    .line 404
    :catch_5
    move-exception v0

    .line 405
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_7

    .line 413
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "f":Ljava/io/File;
    :cond_5
    sput-boolean v7, Lcom/samsung/android/MtpApplication/MtpReceiver;->qcomIgnoretwice:Z

    goto :goto_1

    :cond_6
    move v6, v7

    .line 421
    goto :goto_2

    .line 400
    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "input":Ljava/io/FileInputStream;
    .restart local v3    # "input":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    goto :goto_6

    .line 396
    .end local v2    # "input":Ljava/io/FileInputStream;
    .restart local v3    # "input":Ljava/io/FileInputStream;
    :catch_6
    move-exception v0

    move-object v2, v3

    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    goto :goto_5

    .line 391
    .end local v2    # "input":Ljava/io/FileInputStream;
    .restart local v3    # "input":Ljava/io/FileInputStream;
    :catch_7
    move-exception v0

    move-object v2, v3

    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    goto :goto_3
.end method

.method public static getExternalStorageStatus()Z
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 248
    const/4 v0, 0x0

    .line 249
    .local v0, "externalStoragePath":Ljava/lang/String;
    sget-object v7, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    if-nez v7, :cond_1

    .line 250
    const-string v6, "MTPRx"

    const-string v7, "mContext is coming NULL is returning False :"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :cond_0
    :goto_0
    return v5

    .line 253
    :cond_1
    sget-object v7, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    const-string v8, "storage"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/storage/StorageManager;

    .line 254
    .local v3, "storageManager":Landroid/os/storage/StorageManager;
    if-nez v3, :cond_2

    .line 255
    const-string v6, "MTPRx"

    const-string v7, "storageManager is coming NULL is returning False :"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 258
    :cond_2
    invoke-virtual {v3}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v4

    .line 259
    .local v4, "storageVolumes":[Landroid/os/storage/StorageVolume;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v7, v4

    if-ge v1, v7, :cond_3

    .line 260
    aget-object v7, v4, v1

    invoke-virtual {v7}, Landroid/os/storage/StorageVolume;->isRemovable()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 261
    aget-object v7, v4, v1

    invoke-virtual {v7}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 262
    const-string v7, "MTPRx"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sd-Card path"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :cond_3
    const/4 v2, 0x0

    .line 267
    .local v2, "status":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 268
    invoke-virtual {v3, v0}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 270
    :cond_4
    if-eqz v2, :cond_0

    .line 271
    const-string v7, "MTPRx"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Status for mount/Unmount :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    const-string v7, "mounted"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 273
    const-string v6, "MTPRx"

    const-string v7, "SDcard is not available"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 259
    .end local v2    # "status":Ljava/lang/String;
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 276
    .restart local v2    # "status":Ljava/lang/String;
    :cond_6
    const-string v5, "MTPRx"

    const-string v7, "SDcard is  available"

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    sput-boolean v6, Lcom/samsung/android/MtpApplication/MtpReceiver;->sdCardExists:Z

    move v5, v6

    .line 278
    goto/16 :goto_0
.end method

.method static getMtpContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 196
    sget-object v0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getPrivateDirectory(Landroid/content/Context;)Z
    .locals 4
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 232
    const-string v2, "MTPRx"

    const-string v3, " inside getprivateDirectory"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    const/4 v0, 0x0

    .line 234
    .local v0, "privateDirPath":Ljava/lang/String;
    if-nez p0, :cond_1

    .line 235
    const-string v2, "MTPRx"

    const-string v3, "context is coming NULL is returning False :"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :cond_0
    :goto_0
    return v1

    .line 238
    :cond_1
    invoke-static {p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 239
    if-eqz v0, :cond_0

    .line 240
    const-string v1, "MTPRx"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "privateDir Path is"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    const/4 v1, 0x1

    goto :goto_0
.end method

.method static getUSB30DelayTime()I
    .locals 4

    .prologue
    .line 878
    const/16 v0, 0xa

    .line 879
    .local v0, "delay":I
    const-string v1, "hltespr"

    sget-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->Usb30notiDevice:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "kltespr"

    sget-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->Usb30notiDevice:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 880
    :cond_0
    const/4 v0, 0x4

    .line 885
    :goto_0
    const-string v1, "MTPRx"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "value of usb30DelayTime is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 886
    return v0

    .line 881
    :cond_1
    const-string v1, "hltevzw"

    sget-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->Usb30notiDevice:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "kltevzw"

    sget-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->Usb30notiDevice:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 882
    :cond_2
    const/4 v0, 0x5

    goto :goto_0

    .line 884
    :cond_3
    const/16 v0, 0xa

    goto :goto_0
.end method

.method static isContainsArrString([Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "arr"    # [Ljava/lang/String;
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 870
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 871
    aget-object v1, p0, v0

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 872
    const/4 v1, 0x1

    .line 875
    :goto_1
    return v1

    .line 870
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 875
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private isEDMRestrictionPolicy()Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 303
    const-string v3, "content://com.sec.knox.provider/RestrictionPolicy4"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 304
    .local v1, "uri":Landroid/net/Uri;
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 305
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "isUsbMediaPlayerAvailable"

    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "false"

    aput-object v5, v4, v7

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 306
    .local v6, "cur":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 308
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 309
    const-string v2, "isUsbMediaPlayerAvailable"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 310
    const-string v2, "MTPRx"

    const-string v3, "RESTRICTIONPOLICY_USBMEDIAPLAYERAVAILABLE_METHOD is false"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v2, v7

    .line 317
    :goto_0
    return v2

    .line 314
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move v2, v8

    .line 317
    goto :goto_0

    .line 314
    :catchall_0
    move-exception v2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v2
.end method

.method private isKnoxCustomPolicy()Z
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v2, 0x0

    .line 321
    const/4 v7, 0x0

    .line 322
    .local v7, "sealedState":Z
    const/4 v9, 0x0

    .line 324
    .local v9, "sealedUsbMassStorageState":Z
    const-string v3, "content://com.sec.knox.provider2/KnoxCustomManagerService1"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 325
    .local v1, "uri":Landroid/net/Uri;
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 327
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "getSealedState"

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 328
    .local v8, "sealedStateCur":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 330
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 331
    const-string v3, "getSealedState"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "true"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 336
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 340
    :cond_0
    const-string v3, "getSealedUsbMassStorageState"

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 341
    .local v10, "sealedUsbMassStorageStateCur":Landroid/database/Cursor;
    if-eqz v10, :cond_1

    .line 343
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 344
    const-string v2, "getSealedUsbMassStorageState"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v9

    .line 349
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 352
    :cond_1
    const-string v2, "MTPRx"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sealedState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    const-string v2, "MTPRx"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sealedUsbMassStorageState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    if-eqz v7, :cond_2

    if-nez v9, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v11, v2

    .end local v10    # "sealedUsbMassStorageStateCur":Landroid/database/Cursor;
    :goto_1
    return v11

    .line 332
    :catch_0
    move-exception v6

    .line 333
    .local v6, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "MTPRx"

    const-string v3, "exception KNOXCUSTOMMANAGERSERVICE_STATE_METHOD."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 336
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    .line 345
    .restart local v10    # "sealedUsbMassStorageStateCur":Landroid/database/Cursor;
    :catch_1
    move-exception v6

    .line 346
    .restart local v6    # "e":Ljava/lang/Exception;
    :try_start_3
    const-string v2, "MTPRx"

    const-string v3, "exception KNOXCUSTOMMANAGERSERVICE_USBMASSSTORAGESTATE_METHOD."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 349
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_2
    move v2, v11

    .line 355
    goto :goto_0
.end method

.method private isPrivateMode()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 359
    const-string v2, "sys.samsung.personalpage.mode"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 360
    .local v0, "is_PrivateMode":Ljava/lang/String;
    const-string v2, "1"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 361
    const-string v2, "MTPRx"

    const-string v3, " is_Privatemode is 1"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    sget-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 363
    const-string v2, "MTPRx"

    const-string v3, " Private mounted proeprly"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    sget-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/MtpApplication/MtpReceiver;->getPrivateDirectory(Landroid/content/Context;)Z

    move-result v2

    sput-boolean v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->PrivateExists:Z

    .line 365
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v2

    sput-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 366
    sget-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v2, :cond_0

    .line 367
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    .line 370
    const/4 v1, 0x1

    .line 380
    :cond_0
    :goto_0
    return v1

    .line 375
    :cond_1
    const-string v2, "MTPRx"

    const-string v3, "Private not mountd properly"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 379
    :cond_2
    const-string v2, "MTPRx"

    const-string v3, "is_Privatemode is NOT 1"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isUsb30Mode()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 285
    const/4 v1, 0x0

    .line 287
    .local v1, "usbMode":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/io/File;

    const-string v5, "/sys/class/android_usb/android0/bcdUSB"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x20

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 288
    const-string v4, "MTPRx"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "usbMode is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    if-eqz v1, :cond_0

    const-string v4, "0300"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 295
    sput-boolean v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->usb30Mode:Z

    .line 299
    :goto_0
    return v2

    .line 289
    :catch_0
    move-exception v0

    .line 290
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "MTPRx"

    const-string v4, "cannot open file : /sys/class/android_usb/android0/bcdUSB"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->usb30Mode:Z

    move v2, v3

    .line 292
    goto :goto_0

    .line 298
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->usb30Mode:Z

    move v2, v3

    .line 299
    goto :goto_0
.end method

.method private launchMtpApp(Landroid/content/Context;Landroid/content/ContentResolver;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cr"    # Landroid/content/ContentResolver;

    .prologue
    .line 775
    invoke-static {}, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->getInstance()Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpReceiver;->playlistUpdate:Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;

    .line 776
    sget-object v0, Lcom/samsung/android/MtpApplication/MtpReceiver;->playlistUpdate:Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;

    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/MtpApplication/MtpPlaylistObserver;->setContext(Landroid/content/Context;)V

    .line 778
    invoke-static {}, Lcom/samsung/android/MtpApplication/MtpAdbObserver;->getInstance()Lcom/samsung/android/MtpApplication/MtpAdbObserver;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpReceiver;->adbUpdate:Lcom/samsung/android/MtpApplication/MtpAdbObserver;

    .line 779
    sget-object v0, Lcom/samsung/android/MtpApplication/MtpReceiver;->adbUpdate:Lcom/samsung/android/MtpApplication/MtpAdbObserver;

    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/MtpApplication/MtpAdbObserver;->setContext(Landroid/content/Context;)V

    .line 781
    const/4 v0, 0x2

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendNoti(Landroid/content/Context;Landroid/content/ContentResolver;I)V

    .line 782
    return-void
.end method

.method static resetDriveEmergency(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 857
    if-nez p0, :cond_0

    .line 858
    const-string v0, "MTPRx"

    const-string v1, "context is NULL in reset drive"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    const/4 v0, 0x0

    .line 867
    :goto_0
    return v0

    .line 861
    :cond_0
    const-string v0, "MTPRx"

    const-string v1, "resettinging the Drive"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 863
    sget-object v0, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v0, :cond_1

    .line 864
    const-string v0, "MTPRx"

    const-string v1, "Sending EMERGENCY_MODE_ENABLED to stack"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 865
    sget-object v0, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    .line 867
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private sendMsg(I)V
    .locals 4
    .param p1, "what"    # I

    .prologue
    .line 745
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mStartMtpHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 746
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 747
    const/16 v1, 0xe

    if-ne v1, p1, :cond_0

    .line 748
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mStartMtpHandler:Landroid/os/Handler;

    const-wide/32 v2, 0xea60

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 752
    :goto_0
    return-void

    .line 750
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mStartMtpHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private sendNoti(Landroid/content/Context;Landroid/content/ContentResolver;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cr"    # Landroid/content/ContentResolver;
    .param p3, "noti"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 755
    const/4 v0, 0x0

    .line 756
    .local v0, "qcomDevice":Ljava/lang/String;
    const-string v1, "mtp_usb_conditions_met"

    invoke-static {p2, v1, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    .line 757
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    const-class v3, Lcom/samsung/android/MtpApplication/MtpService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->iService:Landroid/content/Intent;

    .line 758
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->iService:Landroid/content/Intent;

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    .line 759
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-static {v1}, Lcom/samsung/android/MtpApplication/MtpService;->setHandler(Landroid/os/Handler;)V

    .line 760
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-static {v1}, Lcom/samsung/android/MtpApplication/MtpAdbObserver;->setHandler(Landroid/os/Handler;)V

    .line 761
    invoke-direct {p0, p3}, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendMsg(I)V

    .line 762
    const-string v1, "mtp_running_status"

    invoke-static {p2, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 763
    const-string v1, "mtp_usb_conditions_met"

    invoke-static {p2, v1, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 764
    sget-boolean v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->firstTimeResetDone:Z

    if-nez v1, :cond_0

    .line 765
    const-string v1, "ro.hardware"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 766
    const-string v1, "qcom"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->usb30Mode:Z

    if-nez v1, :cond_0

    .line 767
    sput-boolean v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->qcomIgnoretwice:Z

    .line 768
    sput-boolean v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->firstTimeResetDone:Z

    .line 772
    :cond_0
    return-void
.end method

.method static setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p0, "handler"    # Landroid/os/Handler;

    .prologue
    .line 617
    sput-object p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->notifyAppHandler:Landroid/os/Handler;

    .line 618
    return-void
.end method

.method private usbRemoved()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 784
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 785
    .local v1, "cr":Landroid/content/ContentResolver;
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    const/16 v4, 0x18

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 786
    const-string v3, "mtp_running_status"

    invoke-static {v1, v3, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 787
    .local v2, "mtpStatus":I
    const-string v3, "MTPRx"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MTP_RUNNING_STATUS = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    sput-boolean v6, Lcom/samsung/android/MtpApplication/MtpReceiver;->displayDriverPopup:Z

    .line 789
    sput-boolean v7, Lcom/samsung/android/MtpApplication/MtpReceiver;->isConnectedwithdriver:Z

    .line 790
    sput-boolean v6, Lcom/samsung/android/MtpApplication/MtpReceiver;->gadgetReset:Z

    .line 791
    sput-boolean v6, Lcom/samsung/android/MtpApplication/MtpReceiver;->mFirstTime:Z

    .line 795
    sget-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendBooster:Z

    if-ne v3, v7, :cond_1

    .line 796
    const-string v3, "MTPRx"

    const-string v4, " sendbooster is true, so release booster"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 798
    .local v0, "boostIntent":Landroid/content/Intent;
    const-string v3, "com.sec.android.intent.action.SSRM_REQUEST"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 799
    const-string v3, "SSRM_STATUS_NAME"

    const-string v4, "MTP_fileTransfer"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 800
    const-string v3, "SSRM_STATUS_VALUE"

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 801
    const-string v3, "PackageName"

    sget-object v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 802
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 803
    sput-boolean v6, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendBooster:Z

    .line 807
    .end local v0    # "boostIntent":Landroid/content/Intent;
    :goto_0
    if-ne v7, v2, :cond_0

    .line 808
    const-string v3, "MTPRx"

    const-string v4, "In usbRemoved Status bar enabled"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.MTP_FILE_SCAN"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 812
    const-string v3, "MTPRx"

    const-string v4, "Sending Broadcast"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    const-string v3, "mtp_usb_conditions_met"

    invoke-static {v1, v3, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 815
    const-string v3, "mtp_running_status"

    invoke-static {v1, v3, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 816
    const-string v3, "mtp_usb_connection_status"

    invoke-static {v1, v3, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 817
    const-string v3, "mtp_drive_display"

    invoke-static {v1, v3, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 818
    const-string v3, "mtp_sync_alive"

    invoke-static {v1, v3, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 819
    const-string v3, "sdcard_launch"

    invoke-static {v1, v3, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 820
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "mtp_open_session"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 821
    sget-object v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    const-class v6, Lcom/samsung/android/MtpApplication/MtpService;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v3, v4}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 823
    :cond_0
    return-void

    .line 805
    :cond_1
    const-string v3, "MTPRx"

    const-string v4, " sendbooster is false!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method finishSync()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 599
    sget-boolean v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->usb30Mode:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 600
    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->startTimerforUSB30()V

    .line 602
    :cond_0
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "mtp_sync_alive"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 603
    const-string v1, "MTPRx"

    const-string v2, "MS triggered"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MTP_FILE_SCAN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 606
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 607
    .local v0, "boostIntent":Landroid/content/Intent;
    const-string v1, "com.sec.android.intent.action.SSRM_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 608
    const-string v1, "SSRM_STATUS_NAME"

    const-string v2, "MTP_fileTransfer"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 609
    const-string v1, "SSRM_STATUS_VALUE"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 610
    const-string v1, "PackageName"

    sget-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 611
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 612
    sput-boolean v4, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendBooster:Z

    .line 614
    return-void
.end method

.method isSyncFinished()V
    .locals 4

    .prologue
    const/16 v3, 0xc

    .line 539
    const-string v1, "MTPRx"

    const-string v2, "in isSyncFinished posting message with delay of 4sec"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 541
    .local v0, "msg":Landroid/os/Message;
    iput v3, v0, Landroid/os/Message;->what:I

    .line 542
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 543
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->isSynchronizing:Z

    .line 544
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 545
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 904
    sput-object p1, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    .line 905
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 906
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 1217
    :cond_0
    :goto_0
    return-void

    .line 908
    :cond_1
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "In MtpReceiver"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 909
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 920
    .local v1, "cr":Landroid/content/ContentResolver;
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->isKnoxCustomPolicy()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 921
    const-string v9, "MTPRx"

    const-string v10, "MTP is disabled by KnoxCustomManager"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 926
    :cond_2
    const-string v9, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "android.intent.action.USER_PRESENT"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "com.android.KIESWIFI_DISCONNECT"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "com.intent.action.KIES_AUTHKEY_OK"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1c

    .line 928
    :cond_3
    const-string v9, "sys.boot_completed"

    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->boot_completed:Ljava/lang/String;

    .line 929
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "check value of boot_completed is"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Lcom/samsung/android/MtpApplication/MtpReceiver;->boot_completed:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 930
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->boot_completed:Ljava/lang/String;

    if-eqz v9, :cond_5

    .line 931
    const-string v9, "1"

    sget-object v10, Lcom/samsung/android/MtpApplication/MtpReceiver;->boot_completed:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 932
    const-string v9, "MTPRx"

    const-string v10, "check booting is completed_sys.boot_completed"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 940
    :goto_1
    invoke-static {}, Lcom/samsung/android/MtpApplication/MtpReceiver;->getExternalStorageStatus()Z

    move-result v9

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->sdCardExists:Z

    .line 945
    const-string v9, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 946
    const/4 v9, 0x1

    const-string v10, "mtp_running_status"

    const/4 v11, 0x0

    invoke-static {v1, v10, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    if-ne v9, v10, :cond_0

    .line 947
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v9

    sput-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 948
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v9, :cond_0

    const/4 v9, 0x1

    sget-boolean v10, Lcom/samsung/android/MtpApplication/MtpReceiver;->lockStatusSet:Z

    if-ne v9, v10, :cond_0

    .line 949
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    const/16 v10, 0xd

    invoke-virtual {v9, v10}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    .line 950
    const-string v9, "MTPRx"

    const-string v10, "MTP is already running. Setting the status to unlock"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 951
    const/4 v9, 0x0

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->lockStatusSet:Z

    .line 952
    const-string v9, "mtp_drive_display"

    const/4 v10, 0x1

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 934
    :cond_4
    const-string v9, "MTPRx"

    const-string v10, "check booting is not completed_sys.boot_completed, so just return"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 938
    :cond_5
    const-string v9, "MTPRx"

    const-string v10, "check boot_completed is coming null_dev.sfbootcomplete"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 956
    :cond_6
    const-string v9, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_16

    .line 957
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 958
    .local v2, "extras":Landroid/os/Bundle;
    if-nez v2, :cond_7

    .line 959
    const-string v9, "MTPRx"

    const-string v10, "extras from bundle is null. So return"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 962
    :cond_7
    const-string v9, "connected"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->connected:Z

    .line 963
    const-string v9, "configured"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->configured:Z

    .line 964
    const-string v9, "mtp"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mtpEnabled:Z

    .line 965
    const-string v9, "ptp"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->ptpEnabled:Z

    .line 966
    const-string v9, "mtp_running_status"

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    .line 967
    .local v7, "mtpRunningStatus":I
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "value of connected is"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-boolean v11, Lcom/samsung/android/MtpApplication/MtpReceiver;->connected:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 968
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "value of configured is"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-boolean v11, Lcom/samsung/android/MtpApplication/MtpReceiver;->configured:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 969
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "value of mtpEnabled is"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-boolean v11, Lcom/samsung/android/MtpApplication/MtpReceiver;->mtpEnabled:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "value of ptpEnabled is"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-boolean v11, Lcom/samsung/android/MtpApplication/MtpReceiver;->ptpEnabled:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Received USB_STATE with sdCardLaunch = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "sdcard_launch"

    const/4 v12, 0x0

    invoke-static {v1, v11, v12}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 973
    sget-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->connected:Z

    if-nez v9, :cond_8

    sget-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->configured:Z

    if-nez v9, :cond_8

    .line 974
    const-string v9, "MTPRx"

    const-string v10, "Unexpected Disconnection. call usbRemoved."

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 975
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->usbRemoved()V

    .line 978
    :cond_8
    sget-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->configured:Z

    if-eqz v9, :cond_13

    sget-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mtpEnabled:Z

    if-nez v9, :cond_9

    sget-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->ptpEnabled:Z

    if-eqz v9, :cond_13

    .line 979
    :cond_9
    const/4 v9, 0x1

    if-ne v7, v9, :cond_a

    .line 980
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mtpRunningStatus"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 981
    const-string v9, "MTPRx"

    const-string v10, "MTP already launched. So return."

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 985
    :cond_a
    const/4 v9, 0x1

    sget-object v10, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "mtp_switch_to_usb20"

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    if-ne v9, v10, :cond_b

    .line 987
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 988
    .local v3, "intentBroadcast":Landroid/content/Intent;
    const/high16 v9, 0x10800000

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 989
    const-string v9, "com.samsung.android.MtpApplication"

    const-string v10, "com.samsung.android.MtpApplication.USBConnectionActivity"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 990
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 992
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "mtp_switch_to_usb20"

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 995
    .end local v3    # "intentBroadcast":Landroid/content/Intent;
    :cond_b
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mFirstTime: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-boolean v11, Lcom/samsung/android/MtpApplication/MtpReceiver;->mFirstTime:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 996
    sget-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mFirstTime:Z

    if-nez v9, :cond_f

    .line 997
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v9

    sput-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 998
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v9, :cond_c

    .line 1000
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    invoke-static {}, Lcom/samsung/android/encryption/EncryptionKey;->getkey()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/mtp/MTPJNIInterface;->SetCryptionKey(Ljava/lang/String;)V

    .line 1002
    :cond_c
    const/4 v6, 0x0

    .line 1003
    .local v6, "mtpCurrentGlobalUserId":I
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mountPath:Ljava/lang/String;

    .line 1005
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mountPath:Ljava/lang/String;

    sget-object v10, Lcom/samsung/android/MtpApplication/MtpReceiver;->mountPath:Ljava/lang/String;

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1007
    .local v5, "mountPath1":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    sput v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->currentUserId:I

    .line 1008
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "currentUserId is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget v11, Lcom/samsung/android/MtpApplication/MtpReceiver;->currentUserId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1009
    const-string v9, "mtp_current_user_id"

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 1010
    sget v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->currentUserId:I

    if-eq v6, v9, :cond_d

    const/16 v9, 0x64

    if-ge v6, v9, :cond_d

    .line 1011
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "currentUserId is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget v11, Lcom/samsung/android/MtpApplication/MtpReceiver;->currentUserId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " mtpCurrentGlobalUserId is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " so not lauch MTP"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1014
    :cond_d
    new-instance v9, Landroid/os/DVFSHelper;

    sget-object v10, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    const/4 v11, 0x1

    const-wide/16 v12, 0xa

    invoke-direct {v9, v10, v11, v12, v13}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;IJ)V

    sput-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mDvfsHelper:Landroid/os/DVFSHelper;

    .line 1015
    const-string v9, "MTPRx"

    const-string v10, "Start observing USB_STATE_MATCH "

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1016
    iget-object v9, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mUEventObserver:Landroid/os/UEventObserver;

    const-string v10, "DEVPATH=/devices/virtual/android_usb/android0"

    invoke-virtual {v9, v10}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 1017
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->isUsb30Mode()Z

    move-result v9

    if-eqz v9, :cond_e

    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->Usb30SupportedSwitchMode:[Ljava/lang/String;

    sget-object v10, Lcom/samsung/android/MtpApplication/MtpReceiver;->Usb30notiDevice:Ljava/lang/String;

    invoke-static {v9, v10}, Lcom/samsung/android/MtpApplication/MtpReceiver;->isContainsArrString([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1018
    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->startTimerforUSB30()V

    .line 1020
    :cond_e
    const/4 v9, 0x1

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mFirstTime:Z

    .line 1022
    .end local v5    # "mountPath1":Ljava/lang/String;
    .end local v6    # "mtpCurrentGlobalUserId":I
    :cond_f
    const/4 v9, 0x1

    const-string v10, "sdcard_launch"

    const/4 v11, 0x0

    invoke-static {v1, v10, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    if-ne v9, v10, :cond_10

    .line 1025
    const-string v9, "MTPRx"

    const-string v10, "SD card launch, so just return!"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    const-string v9, "sdcard_launch"

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1027
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->isPrivateMode()Z

    move-result v9

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->privatemodeSet:Z

    goto/16 :goto_0

    .line 1042
    :cond_10
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->isPrivateMode()Z

    move-result v9

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->privatemodeSet:Z

    .line 1043
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    const-string v10, "keyguard"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/KeyguardManager;

    .line 1044
    .local v4, "kgm":Landroid/app/KeyguardManager;
    invoke-virtual {v4}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v8

    .line 1045
    .local v8, "screenLocked":Z
    const/4 v9, 0x1

    if-ne v9, v8, :cond_11

    .line 1046
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Phone is locked"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1047
    invoke-virtual {v4}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v9

    if-eqz v9, :cond_11

    .line 1048
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Secure lock : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v9

    sput-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 1050
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v9, :cond_11

    .line 1051
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    const/16 v10, 0xc

    invoke-virtual {v9, v10}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    .line 1052
    const/4 v9, 0x1

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->lockStatusSet:Z

    .line 1053
    const-string v9, "mtp_drive_display"

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1054
    const-string v9, "MTPRx"

    const-string v10, "Setting the status to phone Lock"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1082
    :cond_11
    const/4 v9, 0x1

    const-string v10, "boot_time_connected"

    const/4 v11, 0x0

    invoke-static {v1, v10, v11}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    if-ne v9, v10, :cond_12

    .line 1083
    const-string v9, "boot_time_connected"

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1084
    const-string v9, "MTPRx"

    const-string v10, "Sending NORMAL_BOOT to stack"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1085
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->isBootConnection:Z

    .line 1086
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v9

    sput-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 1087
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v9, :cond_12

    .line 1088
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    const/16 v10, 0x11

    invoke-virtual {v9, v10}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    .line 1115
    .end local v2    # "extras":Landroid/os/Bundle;
    .end local v4    # "kgm":Landroid/app/KeyguardManager;
    .end local v7    # "mtpRunningStatus":I
    .end local v8    # "screenLocked":Z
    :cond_12
    :goto_2
    iget-object v9, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->dpm:Landroid/app/admin/DevicePolicyManager;

    if-eqz v9, :cond_18

    iget-object v9, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->admin:Landroid/content/ComponentName;

    if-eqz v9, :cond_18

    .line 1116
    iget-object v9, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->dpm:Landroid/app/admin/DevicePolicyManager;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/app/admin/DevicePolicyManager;->getAllowDesktopSync(Landroid/content/ComponentName;)Z

    move-result v9

    if-nez v9, :cond_18

    .line 1117
    const-string v9, "MTPRx"

    const-string v10, "desktop sync is not allowed"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1118
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    const v10, 0x7f040014

    const/4 v11, 0x1

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 1121
    const/4 v9, 0x0

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mFirstTime:Z

    .line 1122
    iget-object v9, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mUEventObserver:Landroid/os/UEventObserver;

    invoke-virtual {v9}, Landroid/os/UEventObserver;->stopObserving()V

    goto/16 :goto_0

    .line 1091
    .restart local v2    # "extras":Landroid/os/Bundle;
    .restart local v7    # "mtpRunningStatus":I
    :cond_13
    sget-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->connected:Z

    if-eqz v9, :cond_15

    sget-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mtpEnabled:Z

    if-nez v9, :cond_15

    sget-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->ptpEnabled:Z

    if-nez v9, :cond_15

    .line 1092
    const/4 v9, 0x1

    if-ne v7, v9, :cond_14

    .line 1094
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->usbRemoved()V

    .line 1095
    const-string v9, "MTPRx"

    const-string v10, "tethering is started so turning off MTP "

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1097
    :cond_14
    const-string v9, "MTPRx"

    const-string v10, "tethering is started and mtp is not running so returning"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1100
    :cond_15
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "configured is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-boolean v11, Lcom/samsung/android/MtpApplication/MtpReceiver;->configured:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1103
    .end local v2    # "extras":Landroid/os/Bundle;
    .end local v7    # "mtpRunningStatus":I
    :cond_16
    const-string v9, "com.intent.action.KIES_AUTHKEY_OK"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_17

    .line 1104
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v9

    sput-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 1105
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v9, :cond_12

    .line 1106
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    const/16 v10, 0xd

    invoke-virtual {v9, v10}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    .line 1107
    const-string v9, "MTPRx"

    const-string v10, "MTP is already running. Setting the status to unlock_if"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1108
    const/4 v9, 0x0

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->lockStatusSet:Z

    .line 1109
    const-string v9, "mtp_drive_display"

    const/4 v10, 0x1

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 1112
    :cond_17
    const-string v9, "com.android.KIESWIFI_DISCONNECT"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 1113
    const-string v9, "MTPRx"

    const-string v10, "Inside com.android.KIESWIFI_DISCONNECT"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1126
    :cond_18
    const-string v9, "mtp_usb_conditions_met"

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1138
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->isEDMRestrictionPolicy()Z

    move-result v9

    if-nez v9, :cond_19

    .line 1139
    const-string v9, "MTPRx"

    const-string v10, "MTP is blocked by admin case1"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1140
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    const v10, 0x7f040014

    const/4 v11, 0x1

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 1141
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v9

    sput-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 1142
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v9, :cond_19

    .line 1143
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    const/16 v10, 0x1b

    invoke-virtual {v9, v10}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    .line 1147
    :cond_19
    sget-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->ptpEnabled:Z

    if-eqz v9, :cond_1b

    .line 1148
    const-string v9, "MTPRx"

    const-string v10, "sending PTP_ICON_ENABLED to stack "

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1150
    const/16 v9, 0xb

    invoke-direct {p0, p1, v1, v9}, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendNoti(Landroid/content/Context;Landroid/content/ContentResolver;I)V

    .line 1157
    :cond_1a
    :goto_3
    const-string v9, "MTPRx"

    const-string v10, "else part ... so first time!!!"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1158
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/MtpApplication/MtpReceiver;->launchMtpApp(Landroid/content/Context;Landroid/content/ContentResolver;)V

    goto/16 :goto_0

    .line 1152
    :cond_1b
    sget-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mtpEnabled:Z

    if-eqz v9, :cond_1a

    .line 1153
    const-string v9, "MTPRx"

    const-string v10, "sending MTP_ICON_ENABLED to stack"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1155
    const/16 v9, 0xa

    invoke-direct {p0, p1, v1, v9}, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendNoti(Landroid/content/Context;Landroid/content/ContentResolver;I)V

    goto :goto_3

    .line 1161
    :cond_1c
    const-string v9, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1d

    .line 1163
    const-string v9, "mtp_usb_connection_status"

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1164
    const-string v9, "media_player_mode"

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1165
    const-string v9, "mtp_usb_conditions_met"

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1166
    const-string v9, "mtp_running_status"

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1167
    const-string v9, "media_mount_count"

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1168
    const-string v9, "mtp_sync_alive"

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1169
    const-string v9, "sdcard_launch"

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1170
    const-string v9, "boot_time_connected"

    const/4 v10, 0x1

    invoke-static {v1, v9, v10}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1171
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "mtp_open_session"

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 1173
    :cond_1d
    const-string v9, "com.android.MTP.LAUNCH_MTP"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1174
    const-string v9, "MTPRx"

    const-string v10, "Inside com.android.MTP.LAUNCH_MTP"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1175
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 1176
    .restart local v2    # "extras":Landroid/os/Bundle;
    if-eqz v2, :cond_0

    .line 1178
    const-string v9, "rePtpEnabled"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->ptpEnabled:Z

    .line 1179
    const-string v9, "MTPRx"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ptpEnabled is"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-boolean v11, Lcom/samsung/android/MtpApplication/MtpReceiver;->ptpEnabled:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1180
    sget-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->ptpEnabled:Z

    if-eqz v9, :cond_1f

    .line 1181
    const-string v9, "MTPRx"

    const-string v10, "sending PTP_ICON_ENABLED to stack during relaunch"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1183
    const/16 v9, 0xb

    invoke-direct {p0, p1, v1, v9}, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendNoti(Landroid/content/Context;Landroid/content/ContentResolver;I)V

    .line 1191
    :goto_4
    invoke-static {}, Lcom/samsung/android/MtpApplication/MtpReceiver;->getExternalStorageStatus()Z

    move-result v9

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->sdCardExists:Z

    .line 1192
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->isPrivateMode()Z

    move-result v9

    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->privatemodeSet:Z

    .line 1203
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/MtpReceiver;->isEDMRestrictionPolicy()Z

    move-result v9

    if-nez v9, :cond_1e

    .line 1204
    const-string v9, "MTPRx"

    const-string v10, "MTP is blocked by admin case2"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1205
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    const v10, 0x7f040014

    const/4 v11, 0x1

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 1206
    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v9

    sput-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    .line 1207
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    if-eqz v9, :cond_1e

    .line 1208
    sget-object v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->statusUpdate:Landroid/mtp/MTPJNIInterface;

    const/16 v10, 0x1b

    invoke-virtual {v9, v10}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    .line 1212
    :cond_1e
    const-string v9, "MTPRx"

    const-string v10, "Launch MTP app"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1214
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/MtpApplication/MtpReceiver;->launchMtpApp(Landroid/content/Context;Landroid/content/ContentResolver;)V

    goto/16 :goto_0

    .line 1186
    :cond_1f
    const-string v9, "MTPRx"

    const-string v10, "sending MTP_ICON_ENABLED to stack during relaunch"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1188
    const/16 v9, 0xa

    invoke-direct {p0, p1, v1, v9}, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendNoti(Landroid/content/Context;Landroid/content/ContentResolver;I)V

    goto :goto_4
.end method

.method sendBooster()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 582
    const-string v1, "MTPRx"

    const-string v2, "Inside sendBooster"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 584
    .local v0, "boostIntent":Landroid/content/Intent;
    const-string v1, "com.sec.android.intent.action.SSRM_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 585
    const-string v1, "SSRM_STATUS_NAME"

    const-string v2, "MTP_fileTransfer"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 586
    const-string v1, "SSRM_STATUS_VALUE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 587
    const-string v1, "PackageName"

    sget-object v2, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 588
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 589
    sput-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendBooster:Z

    .line 591
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->mDvfsHelper:Landroid/os/DVFSHelper;

    if-eqz v1, :cond_0

    .line 592
    sget-object v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->mDvfsHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v1}, Landroid/os/DVFSHelper;->acquire()V

    .line 596
    :cond_0
    return-void
.end method

.method startTimerforUSB30()V
    .locals 4

    .prologue
    const/16 v3, 0x18

    .line 548
    const-string v1, "MTPRx"

    const-string v2, "in startTimerforUSB30"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 550
    .local v0, "msg":Landroid/os/Message;
    iput v3, v0, Landroid/os/Message;->what:I

    .line 551
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 552
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    const v2, 0xea60

    invoke-static {}, Lcom/samsung/android/MtpApplication/MtpReceiver;->getUSB30DelayTime()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 553
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/android/MtpApplication/MtpReceiver;->usb30IdleTimer:Z

    .line 554
    return-void
.end method
