.class Lcom/samsung/android/MtpApplication/MtpService$4;
.super Landroid/content/BroadcastReceiver;
.source "MtpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/MtpApplication/MtpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/MtpApplication/MtpService;


# direct methods
.method constructor <init>(Lcom/samsung/android/MtpApplication/MtpService;)V
    .locals 0

    .prologue
    .line 423
    iput-object p1, p0, Lcom/samsung/android/MtpApplication/MtpService$4;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 425
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 426
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 449
    :cond_0
    :goto_0
    return-void

    .line 429
    :cond_1
    const-string v1, "MtpService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "In MTPAPP onReceive:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 432
    const-string v1, "MtpService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Inside ACTION_USER_PRESENT:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "mtp_drive_display"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 434
    const-string v1, "MtpService"

    const-string v2, "unregistering mtpUserPresentReceiver in if case action_user_present"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService$4;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->mtpUserPresentReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1}, Lcom/samsung/android/MtpApplication/MtpService;->access$500(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 436
    sput-boolean v4, Lcom/samsung/android/MtpApplication/MtpService;->userPresentRegistered:Z

    goto :goto_0

    .line 438
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService$4;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v2

    # setter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v1, v2}, Lcom/samsung/android/MtpApplication/MtpService;->access$002(Lcom/samsung/android/MtpApplication/MtpService;Landroid/mtp/MTPJNIInterface;)Landroid/mtp/MTPJNIInterface;

    .line 439
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService$4;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v1}, Lcom/samsung/android/MtpApplication/MtpService;->access$000(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/mtp/MTPJNIInterface;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 440
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService$4;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v1}, Lcom/samsung/android/MtpApplication/MtpService;->access$000(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/mtp/MTPJNIInterface;

    move-result-object v1

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    .line 441
    const-string v1, "MtpService"

    const-string v2, "MTP is already running. Setting the status to unlock"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "mtp_drive_display"

    invoke-static {v1, v2, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 443
    const-string v1, "MtpService"

    const-string v2, "unregistering mtpUserPresentReceiver in else case action_user_present"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/MtpService$4;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->mtpUserPresentReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1}, Lcom/samsung/android/MtpApplication/MtpService;->access$500(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 445
    sput-boolean v4, Lcom/samsung/android/MtpApplication/MtpService;->userPresentRegistered:Z

    goto/16 :goto_0
.end method
