.class Lcom/samsung/android/MtpApplication/MtpReceiver$2;
.super Landroid/os/Handler;
.source "MtpReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/MtpApplication/MtpReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;


# direct methods
.method constructor <init>(Lcom/samsung/android/MtpApplication/MtpReceiver;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v12, 0x15

    const/16 v11, 0xe

    const/16 v10, 0xc

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 428
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 429
    const-string v5, "MTPRx"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "notification from stack "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    const/4 v5, 0x3

    iget v6, p1, Landroid/os/Message;->what:I

    if-ne v5, v6, :cond_1

    .line 432
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "mtp_sync_alive"

    invoke-static {v5, v6, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v9, :cond_0

    .line 433
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    invoke-virtual {v5}, Lcom/samsung/android/MtpApplication/MtpReceiver;->isSyncFinished()V

    .line 535
    :cond_0
    :goto_0
    return-void

    .line 435
    :cond_1
    const/4 v5, 0x5

    iget v6, p1, Landroid/os/Message;->what:I

    if-eq v5, v6, :cond_2

    const/4 v5, 0x4

    iget v6, p1, Landroid/os/Message;->what:I

    if-eq v5, v6, :cond_2

    const/16 v5, 0xa

    iget v6, p1, Landroid/os/Message;->what:I

    if-ne v5, v6, :cond_7

    .line 436
    :cond_2
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "mtp_sync_alive"

    invoke-static {v5, v6, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-nez v5, :cond_3

    .line 437
    const-string v5, "MTPRx"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Inside Syncronising"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    sget-boolean v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendBooster:Z

    if-nez v5, :cond_3

    .line 439
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    invoke-virtual {v5}, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendBooster()V

    .line 441
    :cond_3
    sget-boolean v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->usb30Mode:Z

    if-eqz v5, :cond_4

    sget-boolean v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->usb30IdleTimer:Z

    if-eqz v5, :cond_4

    .line 442
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    iget-object v5, v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    const/16 v6, 0x18

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 443
    sput-boolean v8, Lcom/samsung/android/MtpApplication/MtpReceiver;->usb30IdleTimer:Z

    .line 445
    :cond_4
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "mtp_sync_alive"

    invoke-static {v5, v6, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 446
    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->isSynchronizing:Z

    .line 448
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "mtp_sync_alive"

    invoke-static {v5, v6, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v9, :cond_5

    .line 449
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    iget-object v5, v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-virtual {v5, v10}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 452
    :cond_5
    const-string v5, "MTPRx"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Inside Syncronising"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    sget-boolean v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendBooster:Z

    if-nez v5, :cond_6

    .line 454
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    invoke-virtual {v5}, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendBooster()V

    .line 456
    :cond_6
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "mtp_sync_alive"

    invoke-static {v5, v6, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 458
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 459
    .local v3, "intentBroadcast":Landroid/content/Intent;
    const/high16 v5, 0x10800000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 460
    const-string v5, "com.samsung.android.MtpApplication"

    const-string v6, "com.samsung.android.MtpApplication.MtpApplication"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 461
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 462
    .end local v3    # "intentBroadcast":Landroid/content/Intent;
    :cond_7
    iget v5, p1, Landroid/os/Message;->what:I

    if-ne v10, v5, :cond_9

    .line 463
    sget-boolean v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->isSynchronizing:Z

    if-nez v5, :cond_8

    .line 464
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 465
    .local v1, "cr":Landroid/content/ContentResolver;
    const-string v5, "mtp_sync_alive"

    invoke-static {v1, v5, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 467
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    invoke-virtual {v5}, Lcom/samsung/android/MtpApplication/MtpReceiver;->finishSync()V

    goto/16 :goto_0

    .line 469
    .end local v1    # "cr":Landroid/content/ContentResolver;
    :cond_8
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    invoke-virtual {v5}, Lcom/samsung/android/MtpApplication/MtpReceiver;->isSyncFinished()V

    goto/16 :goto_0

    .line 471
    :cond_9
    const/16 v5, 0xb

    iget v6, p1, Landroid/os/Message;->what:I

    if-ne v5, v6, :cond_a

    .line 472
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    iget-object v5, v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-virtual {v5, v10}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 473
    :cond_a
    const/16 v5, 0xf

    iget v6, p1, Landroid/os/Message;->what:I

    if-ne v5, v6, :cond_b

    .line 474
    const-string v5, "MTPRx"

    const-string v6, "Condition matched"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "mtp_sync_alive"

    invoke-static {v5, v6, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v9, :cond_0

    .line 476
    const-string v5, "MTPRx"

    const-string v6, "Booster matched. Calling for boosting again."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    sget-boolean v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendBooster:Z

    if-nez v5, :cond_0

    .line 478
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    invoke-virtual {v5}, Lcom/samsung/android/MtpApplication/MtpReceiver;->sendBooster()V

    goto/16 :goto_0

    .line 480
    :cond_b
    const/16 v5, 0x13

    iget v6, p1, Landroid/os/Message;->what:I

    if-eq v5, v6, :cond_c

    const/16 v5, 0x14

    iget v6, p1, Landroid/os/Message;->what:I

    if-ne v5, v6, :cond_d

    .line 481
    :cond_c
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 482
    .restart local v1    # "cr":Landroid/content/ContentResolver;
    const-string v5, "mtp_sync_alive"

    invoke-static {v1, v5, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 483
    const-string v5, "MTPRx"

    const-string v6, "USB removed"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    # invokes: Lcom/samsung/android/MtpApplication/MtpReceiver;->usbRemoved()V
    invoke-static {v5}, Lcom/samsung/android/MtpApplication/MtpReceiver;->access$200(Lcom/samsung/android/MtpApplication/MtpReceiver;)V

    goto/16 :goto_0

    .line 489
    .end local v1    # "cr":Landroid/content/ContentResolver;
    :cond_d
    const/16 v5, 0x12

    iget v6, p1, Landroid/os/Message;->what:I

    if-ne v5, v6, :cond_e

    .line 490
    sget-boolean v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mtpEnabled:Z

    if-ne v5, v9, :cond_0

    .line 492
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 493
    .local v4, "resolverIntent":Landroid/content/Intent;
    const/high16 v5, 0x10000000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 494
    const-string v5, "com.android.settings"

    const-string v6, "com.android.settings.SettingsReceiverActivity"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 496
    :try_start_0
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 497
    const-string v5, "MTPRx"

    const-string v6, "started activity for popup"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 499
    :catch_0
    move-exception v2

    .line 500
    .local v2, "e":Landroid/content/ActivityNotFoundException;
    const-string v5, "MTPRx"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unable to start activity "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 503
    .end local v2    # "e":Landroid/content/ActivityNotFoundException;
    .end local v4    # "resolverIntent":Landroid/content/Intent;
    :cond_e
    iget v5, p1, Landroid/os/Message;->what:I

    if-ne v11, v5, :cond_f

    .line 504
    sput-boolean v8, Lcom/samsung/android/MtpApplication/MtpReceiver;->displayDriverPopup:Z

    .line 505
    sput-boolean v9, Lcom/samsung/android/MtpApplication/MtpReceiver;->isConnectedwithdriver:Z

    .line 506
    const-string v5, "MTPRx"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Open session has come  remove the  Message "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/samsung/android/MtpApplication/MtpReceiver;->displayDriverPopup:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    iget-object v5, v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mStartMtpHandler:Landroid/os/Handler;

    invoke-virtual {v5, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 508
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "mtp_open_session"

    invoke-static {v5, v6, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 509
    :cond_f
    iget v5, p1, Landroid/os/Message;->what:I

    if-ne v12, v5, :cond_10

    .line 510
    sput-boolean v8, Lcom/samsung/android/MtpApplication/MtpReceiver;->isConnectedwithdriver:Z

    .line 511
    const-string v5, "MTPRx"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Read Error Came "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/samsung/android/MtpApplication/MtpReceiver;->isConnectedwithdriver:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    iget-object v5, v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mNotiHandler:Landroid/os/Handler;

    invoke-virtual {v5, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 513
    const-string v5, "MTPRx"

    const-string v6, "Read Error Came so calling usbRemoved()"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    # invokes: Lcom/samsung/android/MtpApplication/MtpReceiver;->usbRemoved()V
    invoke-static {v5}, Lcom/samsung/android/MtpApplication/MtpReceiver;->access$200(Lcom/samsung/android/MtpApplication/MtpReceiver;)V

    goto/16 :goto_0

    .line 515
    :cond_10
    const/16 v5, 0x18

    iget v6, p1, Landroid/os/Message;->what:I

    if-ne v5, v6, :cond_0

    .line 516
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    sget-object v6, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    # invokes: Lcom/samsung/android/MtpApplication/MtpReceiver;->acquireWakeLock(Landroid/content/Context;)V
    invoke-static {v5, v6}, Lcom/samsung/android/MtpApplication/MtpReceiver;->access$300(Lcom/samsung/android/MtpApplication/MtpReceiver;Landroid/content/Context;)V

    .line 517
    const-string v5, "hltevzw"

    sget-object v6, Lcom/samsung/android/MtpApplication/MtpReceiver;->Usb30notiDevice:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_11

    const-string v5, "kltevzw"

    sget-object v6, Lcom/samsung/android/MtpApplication/MtpReceiver;->Usb30notiDevice:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 518
    :cond_11
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "airplane_mode_on_reason_usb3"

    invoke-static {v5, v6, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v9, :cond_12

    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "airplane_mode_on"

    invoke-static {v5, v6, v8}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v9, :cond_12

    .line 521
    const-string v5, "MTPRx"

    const-string v6, "Airplane mode turn off and send ACTION_AIRPLANE_MODE_CHANGED broadcast  intent"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "airplane_mode_on_reason_usb3"

    invoke-static {v5, v6, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 523
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "airplane_mode_on"

    invoke-static {v5, v6, v8}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 525
    new-instance v0, Landroid/content/Intent;

    const-string v5, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 526
    .local v0, "airplaneIntent":Landroid/content/Intent;
    const/high16 v5, 0x20000000

    invoke-virtual {v0, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 527
    const-string v5, "state"

    invoke-virtual {v0, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 528
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 530
    .end local v0    # "airplaneIntent":Landroid/content/Intent;
    :cond_12
    sget-object v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "mtp_switch_to_usb20"

    invoke-static {v5, v6, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 532
    :cond_13
    iget-object v5, p0, Lcom/samsung/android/MtpApplication/MtpReceiver$2;->this$0:Lcom/samsung/android/MtpApplication/MtpReceiver;

    # invokes: Lcom/samsung/android/MtpApplication/MtpReceiver;->enableUsb30(Z)V
    invoke-static {v5, v8}, Lcom/samsung/android/MtpApplication/MtpReceiver;->access$400(Lcom/samsung/android/MtpApplication/MtpReceiver;Z)V

    goto/16 :goto_0
.end method
