.class Lcom/samsung/android/MtpApplication/MtpService$3;
.super Landroid/content/BroadcastReceiver;
.source "MtpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/MtpApplication/MtpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/MtpApplication/MtpService;


# direct methods
.method constructor <init>(Lcom/samsung/android/MtpApplication/MtpService;)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 324
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 325
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/android/MtpApplication/MtpService;->isUsbDisconnected()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 421
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 329
    .local v1, "cr":Landroid/content/ContentResolver;
    const-string v3, "MtpService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "In MTPAPP onReceive:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 331
    sget-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->sdCardExists:Z

    if-eqz v3, :cond_2

    sget-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->privatemodeSet:Z

    if-eqz v3, :cond_2

    .line 332
    const-string v3, "MtpService"

    const-string v4, "SD card and Private already available. just return"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 335
    :cond_2
    sget-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->sdCardExists:Z

    if-nez v3, :cond_3

    invoke-static {}, Lcom/samsung/android/MtpApplication/MtpReceiver;->getExternalStorageStatus()Z

    move-result v3

    if-ne v3, v6, :cond_3

    .line 336
    const-string v3, "MtpService"

    const-string v4, "In SDCard add case"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    const-string v3, "mtp_running_status"

    invoke-static {v1, v3, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v6, v3, :cond_0

    .line 338
    const-string v3, "MtpService"

    const-string v4, "sd card added when MTP running"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    sput-boolean v6, Lcom/samsung/android/MtpApplication/MtpReceiver;->sdCardExists:Z

    .line 340
    const-string v3, "MtpService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Top screen is MTP and MtpReceiver.sdCardExists value = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->sdCardExists:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v4

    # setter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3, v4}, Lcom/samsung/android/MtpApplication/MtpService;->access$002(Lcom/samsung/android/MtpApplication/MtpService;Landroid/mtp/MTPJNIInterface;)Landroid/mtp/MTPJNIInterface;

    .line 354
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3}, Lcom/samsung/android/MtpApplication/MtpService;->access$000(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 355
    const-string v3, "MtpService"

    const-string v4, "sending sdcard connected noti to stack"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3}, Lcom/samsung/android/MtpApplication/MtpService;->access$000(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    goto/16 :goto_0

    .line 359
    :cond_3
    sget-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->privatemodeSet:Z

    if-nez v3, :cond_0

    invoke-static {p1}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v3

    if-ne v3, v6, :cond_0

    .line 360
    const-string v3, "MtpService"

    const-string v4, " Private added case"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    const-string v3, "mtp_running_status"

    invoke-static {v1, v3, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v6, v3, :cond_0

    .line 362
    const-string v3, "MtpService"

    const-string v4, "Private added when MTP running"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    sput-boolean v6, Lcom/samsung/android/MtpApplication/MtpReceiver;->privatemodeSet:Z

    .line 364
    const-string v3, "MtpService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Top screen is MTP and PrivatemodeSet value = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/samsung/android/MtpApplication/MtpReceiver;->privatemodeSet:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v4

    # setter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3, v4}, Lcom/samsung/android/MtpApplication/MtpService;->access$002(Lcom/samsung/android/MtpApplication/MtpService;Landroid/mtp/MTPJNIInterface;)Landroid/mtp/MTPJNIInterface;

    .line 366
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3}, Lcom/samsung/android/MtpApplication/MtpService;->access$000(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 367
    const-string v3, "MtpService"

    const-string v4, "sending Private connected noti to stack"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3}, Lcom/samsung/android/MtpApplication/MtpService;->access$000(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    const/16 v4, 0x17

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    goto/16 :goto_0

    .line 373
    :cond_4
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 374
    const-string v3, "MtpService"

    const-string v4, "media  ACTION_MEDIA_UNMOUNTED"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    const-string v3, "sys.samsung.personalpage.mode"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 376
    .local v2, "is_PrivateMode":Ljava/lang/String;
    sget-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->privatemodeSet:Z

    if-ne v3, v6, :cond_5

    invoke-static {p1}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 377
    const-string v3, "MtpService"

    const-string v4, "privatebox is removed "

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    sput-boolean v7, Lcom/samsung/android/MtpApplication/MtpReceiver;->privatemodeSet:Z

    .line 379
    const-string v3, "mtp_running_status"

    invoke-static {v1, v3, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v6, v3, :cond_0

    .line 380
    const-string v3, "MtpService"

    const-string v4, "Private removed during MTP session"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v4

    # setter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3, v4}, Lcom/samsung/android/MtpApplication/MtpService;->access$002(Lcom/samsung/android/MtpApplication/MtpService;Landroid/mtp/MTPJNIInterface;)Landroid/mtp/MTPJNIInterface;

    .line 382
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3}, Lcom/samsung/android/MtpApplication/MtpService;->access$000(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 383
    const-string v3, "MtpService"

    const-string v4, "sending Private disconnected noti to stack"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3}, Lcom/samsung/android/MtpApplication/MtpService;->access$000(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    const/16 v4, 0xf

    invoke-virtual {v3, v4}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    goto/16 :goto_0

    .line 387
    :cond_5
    sget-boolean v3, Lcom/samsung/android/MtpApplication/MtpReceiver;->sdCardExists:Z

    if-ne v3, v6, :cond_6

    invoke-static {}, Lcom/samsung/android/MtpApplication/MtpReceiver;->getExternalStorageStatus()Z

    move-result v3

    if-nez v3, :cond_6

    .line 388
    const-string v3, "MtpService"

    const-string v4, "MMC is disconnected in ACTION_MEDIA_UNMOUNTED"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    sput-boolean v7, Lcom/samsung/android/MtpApplication/MtpReceiver;->sdCardExists:Z

    .line 390
    const-string v3, "sdcard_launch"

    invoke-static {v1, v3, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 391
    const-string v3, "mtp_running_status"

    invoke-static {v1, v3, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v6, v3, :cond_0

    .line 393
    const-string v3, "MtpService"

    const-string v4, "Top screen is MTP"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v4

    # setter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3, v4}, Lcom/samsung/android/MtpApplication/MtpService;->access$002(Lcom/samsung/android/MtpApplication/MtpService;Landroid/mtp/MTPJNIInterface;)Landroid/mtp/MTPJNIInterface;

    .line 395
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3}, Lcom/samsung/android/MtpApplication/MtpService;->access$000(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 396
    const-string v3, "MtpService"

    const-string v4, "sending MMC disconnected noti to stack"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3}, Lcom/samsung/android/MtpApplication/MtpService;->access$000(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    goto/16 :goto_0

    .line 401
    :cond_6
    const-string v3, "MtpService"

    const-string v4, "SD card/Private already removed. just return"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 404
    .end local v2    # "is_PrivateMode":Ljava/lang/String;
    :cond_7
    const-string v3, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 405
    const-string v3, "MtpService"

    const-string v4, "MMC is disconnected"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    sput-boolean v7, Lcom/samsung/android/MtpApplication/MtpReceiver;->sdCardExists:Z

    .line 407
    const-string v3, "sdcard_launch"

    invoke-static {v1, v3, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 408
    const-string v3, "mtp_running_status"

    invoke-static {v1, v3, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v6, v3, :cond_0

    .line 410
    const-string v3, "MtpService"

    const-string v4, "Top screen is MTP"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    invoke-static {}, Landroid/mtp/MTPJNIInterface;->getInstance()Landroid/mtp/MTPJNIInterface;

    move-result-object v4

    # setter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3, v4}, Lcom/samsung/android/MtpApplication/MtpService;->access$002(Lcom/samsung/android/MtpApplication/MtpService;Landroid/mtp/MTPJNIInterface;)Landroid/mtp/MTPJNIInterface;

    .line 412
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3}, Lcom/samsung/android/MtpApplication/MtpService;->access$000(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 413
    const-string v3, "MtpService"

    const-string v4, "sending MMC disconnected noti to stack"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    iget-object v3, p0, Lcom/samsung/android/MtpApplication/MtpService$3;->this$0:Lcom/samsung/android/MtpApplication/MtpService;

    # getter for: Lcom/samsung/android/MtpApplication/MtpService;->jniObj:Landroid/mtp/MTPJNIInterface;
    invoke-static {v3}, Lcom/samsung/android/MtpApplication/MtpService;->access$000(Lcom/samsung/android/MtpApplication/MtpService;)Landroid/mtp/MTPJNIInterface;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/mtp/MTPJNIInterface;->notifyMTPStack(I)V

    goto/16 :goto_0

    .line 418
    :cond_8
    const-string v3, "MtpService"

    const-string v4, "SD card/Private already removed. just return"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
