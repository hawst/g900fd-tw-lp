.class public Lcom/samsung/android/MtpApplication/StoreAddPopup;
.super Landroid/app/Activity;
.source "StoreAddPopup.java"


# static fields
.field private static mHandler:Landroid/os/Handler;


# instance fields
.field alertDialog:Landroid/app/AlertDialog;

.field cr:Landroid/content/ContentResolver;

.field private final mMediaReceiver:Landroid/content/BroadcastReceiver;

.field private final mStoreAddPopupReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/MtpApplication/StoreAddPopup;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 83
    new-instance v0, Lcom/samsung/android/MtpApplication/StoreAddPopup$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/MtpApplication/StoreAddPopup$1;-><init>(Lcom/samsung/android/MtpApplication/StoreAddPopup;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    .line 106
    new-instance v0, Lcom/samsung/android/MtpApplication/StoreAddPopup$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/MtpApplication/StoreAddPopup$2;-><init>(Lcom/samsung/android/MtpApplication/StoreAddPopup;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup;->mStoreAddPopupReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private registerBroadCastRec()V
    .locals 4

    .prologue
    .line 72
    const-string v2, "StoreAddPopup"

    const-string v3, "Registering broadcast receiver."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 74
    .local v1, "lIntentFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 75
    iget-object v2, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup;->mStoreAddPopupReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/samsung/android/MtpApplication/StoreAddPopup;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 77
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 78
    .local v0, "lIF":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 79
    const-string v2, "file"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 80
    iget-object v2, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/samsung/android/MtpApplication/StoreAddPopup;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 81
    return-void
.end method


# virtual methods
.method drawPopup()V
    .locals 3

    .prologue
    .line 129
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f040016

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f04000e

    new-instance v2, Lcom/samsung/android/MtpApplication/StoreAddPopup$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/MtpApplication/StoreAddPopup$4;-><init>(Lcom/samsung/android/MtpApplication/StoreAddPopup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f04000f

    new-instance v2, Lcom/samsung/android/MtpApplication/StoreAddPopup$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/MtpApplication/StoreAddPopup$3;-><init>(Lcom/samsung/android/MtpApplication/StoreAddPopup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup;->alertDialog:Landroid/app/AlertDialog;

    .line 181
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup;->alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 182
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/StoreAddPopup;->registerBroadCastRec()V

    .line 50
    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/StoreAddPopup;->drawPopup()V

    .line 51
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 64
    const-string v0, "StoreAddPopup"

    const-string v1, "on destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup;->alertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup;->alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup;->mStoreAddPopupReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/MtpApplication/StoreAddPopup;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 67
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/MtpApplication/StoreAddPopup;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 68
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 69
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 56
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/StoreAddPopup;->alertDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/StoreAddPopup;->drawPopup()V

    .line 59
    :cond_0
    return-void
.end method
