.class public Lcom/samsung/android/MtpApplication/USBConnectionActivity;
.super Landroid/app/Activity;
.source "USBConnectionActivity.java"


# instance fields
.field alertDialog:Landroid/app/AlertDialog;

.field private final mUSBRemovalReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 69
    new-instance v0, Lcom/samsung/android/MtpApplication/USBConnectionActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/MtpApplication/USBConnectionActivity$1;-><init>(Lcom/samsung/android/MtpApplication/USBConnectionActivity;)V

    iput-object v0, p0, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->mUSBRemovalReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private registerBroadCastRec()V
    .locals 3

    .prologue
    .line 63
    const-string v1, "USBConnectionActivity"

    const-string v2, "Registering broadcast receiver."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 65
    .local v0, "lIntentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->mUSBRemovalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 67
    return-void
.end method


# virtual methods
.method drawPopup()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 91
    const/4 v0, 0x0

    .line 92
    .local v0, "notiMsg":Ljava/lang/String;
    const v1, 0x7f040019

    invoke-virtual {p0, v1}, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {}, Lcom/samsung/android/MtpApplication/MtpReceiver;->getUSB30DelayTime()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 93
    new-instance v1, Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x3

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f040018

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f04000e

    new-instance v3, Lcom/samsung/android/MtpApplication/USBConnectionActivity$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/MtpApplication/USBConnectionActivity$2;-><init>(Lcom/samsung/android/MtpApplication/USBConnectionActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->alertDialog:Landroid/app/AlertDialog;

    .line 107
    iget-object v1, p0, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 108
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-direct {p0}, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->registerBroadCastRec()V

    .line 42
    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->drawPopup()V

    .line 43
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 56
    const-string v0, "USBConnectionActivity"

    const-string v1, "on destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->alertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->mUSBRemovalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 59
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 60
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 48
    iget-object v0, p0, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->alertDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/samsung/android/MtpApplication/USBConnectionActivity;->drawPopup()V

    .line 51
    :cond_0
    return-void
.end method
