.class Lsiso/AutoCollage/Threading;
.super Ljava/lang/Object;
.source "Threading.java"


# static fields
.field private static final IMAGE_MAX_SIZE:I = 0x500


# instance fields
.field collage:Lsiso/AutoCollage/Collage;

.field count:I

.field mDoneSignal:Ljava/util/concurrent/CountDownLatch;

.field pixel_format:I


# direct methods
.method constructor <init>(I)V
    .locals 2
    .param p1, "pixel_fomat"    # I

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lsiso/AutoCollage/Threading;->mDoneSignal:Ljava/util/concurrent/CountDownLatch;

    .line 27
    iput p1, p0, Lsiso/AutoCollage/Threading;->pixel_format:I

    .line 28
    return-void
.end method

.method constructor <init>(Lsiso/AutoCollage/Collage;)V
    .locals 2
    .param p1, "collage"    # Lsiso/AutoCollage/Collage;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lsiso/AutoCollage/Threading;->mDoneSignal:Ljava/util/concurrent/CountDownLatch;

    .line 34
    iput-object p1, p0, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    .line 35
    iget-object v0, p1, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    invoke-virtual {v0}, Lsiso/AutoCollage/SaCConfig;->getPixelFormat()I

    move-result v0

    iput v0, p0, Lsiso/AutoCollage/Threading;->pixel_format:I

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lsiso/AutoCollage/Threading;->count:I

    .line 37
    return-void
.end method


# virtual methods
.method DecodeFile(Ljava/lang/String;I)V
    .locals 21
    .param p1, "szArg"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 148
    const/4 v11, 0x0

    .line 149
    .local v11, "bmp":Landroid/graphics/Bitmap;
    const/4 v15, 0x0

    .line 150
    .local v15, "owidth":I
    const/4 v14, 0x0

    .line 151
    .local v14, "oheight":I
    sget-object v8, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "decode request comes for file  index = "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v8, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    const-string v8, "empty"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 154
    invoke-virtual/range {p0 .. p0}, Lsiso/AutoCollage/Threading;->createBlackFrame()Landroid/graphics/Bitmap;

    move-result-object v11

    .line 155
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 156
    .local v4, "width":I
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 157
    .local v5, "height":I
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v8

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v18

    mul-int v16, v8, v18

    .line 158
    .local v16, "size":I
    invoke-static/range {v16 .. v16}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 159
    .local v3, "dst":Ljava/nio/ByteBuffer;
    invoke-virtual {v11, v3}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 160
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    .line 161
    new-instance v2, Lsiso/AutoCollage/NativeBuffer;

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v7, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget-object v8, v0, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    invoke-virtual {v8}, Lsiso/AutoCollage/Collage;->generateBufferKey()I

    move-result v8

    invoke-direct/range {v2 .. v8}, Lsiso/AutoCollage/NativeBuffer;-><init>(Ljava/nio/ByteBuffer;IIFFI)V

    .line 164
    .local v2, "pbuff":Lsiso/AutoCollage/NativeBuffer;
    move-object/from16 v0, p0

    iget-object v8, v0, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    iget-object v8, v8, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    move/from16 v0, p2

    invoke-virtual {v8, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v8, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Threading Decoding Log : set  On Index : "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v8, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const/4 v3, 0x0

    .line 167
    const/4 v2, 0x0

    .line 285
    .end local v2    # "pbuff":Lsiso/AutoCollage/NativeBuffer;
    .end local v3    # "dst":Ljava/nio/ByteBuffer;
    .end local v4    # "width":I
    .end local v5    # "height":I
    .end local v16    # "size":I
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    :try_start_0
    new-instance v10, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v10}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 174
    .local v10, "bitopt":Landroid/graphics/BitmapFactory$Options;
    const/4 v8, 0x1

    iput-boolean v8, v10, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 175
    move-object/from16 v0, p1

    invoke-static {v0, v10}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 176
    iget v15, v10, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 177
    iget v14, v10, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 178
    const/4 v13, 0x0

    .line 179
    .local v13, "h":I
    const/16 v17, 0x0

    .line 183
    .local v17, "w":I
    iget v8, v10, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    const/16 v18, 0x500

    move/from16 v0, v18

    if-gt v8, v0, :cond_2

    iget v8, v10, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/16 v18, 0x500

    move/from16 v0, v18

    if-le v8, v0, :cond_4

    .line 185
    :cond_2
    iget v8, v10, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v8, v8

    const/high16 v18, 0x44a00000    # 1280.0f

    div-float v8, v8, v18

    float-to-double v0, v8

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-int v13, v0

    .line 186
    iget v8, v10, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v8, v8

    const/high16 v18, 0x44a00000    # 1280.0f

    div-float v8, v8, v18

    float-to-double v0, v8

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v17, v0

    .line 188
    const/4 v8, 0x1

    if-gt v13, v8, :cond_3

    const/4 v8, 0x1

    move/from16 v0, v17

    if-le v0, v8, :cond_4

    .line 190
    :cond_3
    move/from16 v0, v17

    if-le v13, v0, :cond_b

    .line 192
    iput v13, v10, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 202
    :cond_4
    :goto_1
    const/4 v8, 0x0

    iput-boolean v8, v10, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 204
    move-object/from16 v0, p0

    iget v8, v0, Lsiso/AutoCollage/Threading;->pixel_format:I

    if-nez v8, :cond_c

    .line 206
    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v8, v10, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 213
    :goto_2
    move-object/from16 v0, p1

    invoke-static {v0, v10}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 215
    iget v8, v10, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/16 v18, 0x1

    move/from16 v0, v18

    if-le v8, v0, :cond_5

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    if-ne v8, v15, :cond_5

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    if-ne v8, v14, :cond_5

    .line 217
    if-le v15, v14, :cond_d

    .line 218
    const/16 v8, 0x500

    const/high16 v18, 0x44a00000    # 1280.0f

    int-to-float v0, v15

    move/from16 v19, v0

    int-to-float v0, v14

    move/from16 v20, v0

    div-float v19, v19, v20

    div-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    const/16 v19, 0x0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v11, v8, v0, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 224
    :cond_5
    :goto_3
    if-eqz v11, :cond_0

    .line 227
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 228
    .restart local v4    # "width":I
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 230
    .restart local v5    # "height":I
    const/4 v8, 0x1

    if-eq v4, v8, :cond_6

    const/4 v8, 0x1

    if-ne v5, v8, :cond_7

    .line 232
    :cond_6
    mul-int/lit8 v8, v4, 0x2

    mul-int/lit8 v18, v5, 0x2

    const/16 v19, 0x0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v11, v8, v0, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 233
    const/4 v4, 0x2

    .line 234
    const/4 v5, 0x2

    .line 236
    :cond_7
    rem-int/lit8 v8, v4, 0x2

    const/16 v18, 0x1

    move/from16 v0, v18

    if-eq v8, v0, :cond_8

    rem-int/lit8 v8, v5, 0x2

    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v8, v0, :cond_9

    .line 238
    :cond_8
    const v8, 0xfffe

    and-int/2addr v4, v8

    .line 239
    const v8, 0xfffe

    and-int/2addr v5, v8

    .line 240
    const/4 v8, 0x0

    invoke-static {v11, v4, v5, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 245
    :cond_9
    int-to-float v8, v4

    int-to-float v0, v15

    move/from16 v18, v0

    div-float v6, v8, v18

    .line 246
    .local v6, "scaleFactorX":F
    int-to-float v8, v5

    int-to-float v0, v14

    move/from16 v18, v0

    div-float v7, v8, v18

    .line 250
    .local v7, "scaleFactorY":F
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v8

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v18

    move/from16 v0, v18

    if-ne v8, v0, :cond_a

    .line 252
    move-object/from16 v0, p0

    iget-object v8, v0, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    iget-object v8, v8, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    invoke-virtual {v8}, Lsiso/AutoCollage/SaCConfig;->getPixelFormat()I

    move-result v8

    if-nez v8, :cond_e

    .line 254
    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v11, v8, v0}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 264
    :cond_a
    :goto_4
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v8

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v18

    mul-int v16, v8, v18

    .line 265
    .restart local v16    # "size":I
    invoke-static/range {v16 .. v16}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 266
    .restart local v3    # "dst":Ljava/nio/ByteBuffer;
    invoke-virtual {v11, v3}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 267
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    .line 268
    new-instance v2, Lsiso/AutoCollage/NativeBuffer;

    move-object/from16 v0, p0

    iget-object v8, v0, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    invoke-virtual {v8}, Lsiso/AutoCollage/Collage;->generateBufferKey()I

    move-result v8

    invoke-direct/range {v2 .. v8}, Lsiso/AutoCollage/NativeBuffer;-><init>(Ljava/nio/ByteBuffer;IIFFI)V

    .line 271
    .restart local v2    # "pbuff":Lsiso/AutoCollage/NativeBuffer;
    move-object/from16 v0, p0

    iget-object v8, v0, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    iget-object v8, v8, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    move/from16 v0, p2

    invoke-virtual {v8, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 272
    sget-object v8, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Threading Decoding Log : set  On Index : "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v8, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    const/4 v3, 0x0

    .line 275
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 196
    .end local v2    # "pbuff":Lsiso/AutoCollage/NativeBuffer;
    .end local v3    # "dst":Ljava/nio/ByteBuffer;
    .end local v4    # "width":I
    .end local v5    # "height":I
    .end local v6    # "scaleFactorX":F
    .end local v7    # "scaleFactorY":F
    .end local v16    # "size":I
    :cond_b
    move/from16 v0, v17

    iput v0, v10, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 277
    .end local v10    # "bitopt":Landroid/graphics/BitmapFactory$Options;
    .end local v13    # "h":I
    .end local v17    # "w":I
    :catch_0
    move-exception v12

    .line 279
    .local v12, "e":Ljava/lang/Exception;
    const-string v9, "Decoding File :  Falied"

    .line 280
    .local v9, "Error":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v8, v0, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    invoke-virtual {v8, v9}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 281
    sget-object v8, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Exception occured in Decoding : "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 210
    .end local v9    # "Error":Ljava/lang/String;
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v10    # "bitopt":Landroid/graphics/BitmapFactory$Options;
    .restart local v13    # "h":I
    .restart local v17    # "w":I
    :cond_c
    :try_start_1
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v8, v10, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    goto/16 :goto_2

    .line 220
    :cond_d
    const/high16 v8, 0x44a00000    # 1280.0f

    int-to-float v0, v15

    move/from16 v18, v0

    int-to-float v0, v14

    move/from16 v19, v0

    div-float v18, v18, v19

    mul-float v8, v8, v18

    float-to-int v8, v8

    const/16 v18, 0x500

    const/16 v19, 0x0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v11, v8, v0, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v11

    goto/16 :goto_3

    .line 258
    .restart local v4    # "width":I
    .restart local v5    # "height":I
    .restart local v6    # "scaleFactorX":F
    .restart local v7    # "scaleFactorY":F
    :cond_e
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v11, v8, v0}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v11

    goto/16 :goto_4
.end method

.method DecodedOnThread(I)V
    .locals 1
    .param p1, "imageIndex"    # I

    .prologue
    .line 56
    new-instance v0, Lsiso/AutoCollage/Threading$1;

    invoke-direct {v0, p0, p1}, Lsiso/AutoCollage/Threading$1;-><init>(Lsiso/AutoCollage/Threading;I)V

    .line 96
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 97
    return-void
.end method

.method WaitForEndingThread()V
    .locals 2

    .prologue
    .line 45
    :try_start_0
    iget-object v1, p0, Lsiso/AutoCollage/Threading;->mDoneSignal:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :goto_0
    return-void

    .line 46
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public createBlackFrame()Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 103
    iget-object v7, p0, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    invoke-virtual {v7}, Lsiso/AutoCollage/Collage;->getCollageWidth()I

    move-result v6

    .line 104
    .local v6, "width":I
    iget-object v7, p0, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    invoke-virtual {v7}, Lsiso/AutoCollage/Collage;->getCollageHeight()I

    move-result v4

    .line 105
    .local v4, "height":I
    const/4 v2, 0x4

    .line 106
    .local v2, "bpp":I
    iget v7, p0, Lsiso/AutoCollage/Threading;->pixel_format:I

    if-nez v7, :cond_0

    .line 108
    const/4 v2, 0x2

    .line 114
    :goto_0
    mul-int v7, v6, v4

    mul-int/2addr v7, v2

    new-array v3, v7, [B

    .line 116
    .local v3, "bytes":[B
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    mul-int v7, v6, v4

    mul-int/2addr v7, v2

    if-lt v5, v7, :cond_1

    .line 126
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 127
    .local v0, "blackFrameBuf":Ljava/nio/ByteBuffer;
    const/4 v1, 0x0

    .line 129
    .local v1, "bmp":Landroid/graphics/Bitmap;
    iget v7, p0, Lsiso/AutoCollage/Threading;->pixel_format:I

    if-nez v7, :cond_3

    .line 131
    sget-object v7, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v4, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 139
    :goto_2
    invoke-virtual {v1, v0}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 140
    const/4 v0, 0x0

    .line 141
    const/4 v3, 0x0

    check-cast v3, [B

    .line 142
    return-object v1

    .line 112
    .end local v0    # "blackFrameBuf":Ljava/nio/ByteBuffer;
    .end local v1    # "bmp":Landroid/graphics/Bitmap;
    .end local v3    # "bytes":[B
    .end local v5    # "i":I
    :cond_0
    const/4 v2, 0x4

    goto :goto_0

    .line 118
    .restart local v3    # "bytes":[B
    .restart local v5    # "i":I
    :cond_1
    aput-byte v9, v3, v5

    .line 119
    add-int/lit8 v7, v5, 0x1

    aput-byte v9, v3, v7

    .line 120
    const/4 v7, 0x4

    if-ne v2, v7, :cond_2

    .line 122
    add-int/lit8 v7, v5, 0x2

    aput-byte v9, v3, v7

    .line 123
    add-int/lit8 v7, v5, 0x3

    const/4 v8, -0x1

    aput-byte v8, v3, v7

    .line 116
    :cond_2
    add-int/2addr v5, v2

    goto :goto_1

    .line 135
    .restart local v0    # "blackFrameBuf":Ljava/nio/ByteBuffer;
    .restart local v1    # "bmp":Landroid/graphics/Bitmap;
    :cond_3
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v4, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_2
.end method
