.class public Lsiso/AutoCollage/AndroidApiCalls;
.super Ljava/lang/Object;
.source "AndroidApiCalls.java"


# static fields
.field static final NUMBER_OF_CORES:I


# instance fields
.field collage:Lsiso/AutoCollage/Collage;

.field private columns:[Ljava/lang/String;

.field private cr:Landroid/content/ContentResolver;

.field private cursor:Landroid/database/Cursor;

.field private dataIndex:I

.field private idIndex:I

.field private orentIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    sput v0, Lsiso/AutoCollage/AndroidApiCalls;->NUMBER_OF_CORES:I

    .line 24
    return-void
.end method

.method public constructor <init>(Lsiso/AutoCollage/Collage;)V
    .locals 0
    .param p1, "collage"    # Lsiso/AutoCollage/Collage;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    .line 42
    return-void
.end method


# virtual methods
.method public DecodeFile(Ljava/lang/String;)Lsiso/AutoCollage/NativeBuffer;
    .locals 6
    .param p1, "szArg"    # Ljava/lang/String;

    .prologue
    .line 92
    const/4 v1, 0x0

    .line 93
    .local v1, "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v3, v3, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    move-object v2, v1

    .line 106
    .end local v1    # "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    .local v2, "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    :goto_1
    return-object v2

    .line 95
    .end local v2    # "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    .restart local v1    # "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    :cond_0
    iget-object v3, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v3, v3, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 97
    iget-object v3, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v3, v3, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    check-cast v1, Lsiso/AutoCollage/NativeBuffer;

    .line 98
    .restart local v1    # "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DecodeFile fileReadFlag = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lsiso/AutoCollage/NativeBuffer;->getBufferReadFlag()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-virtual {v1}, Lsiso/AutoCollage/NativeBuffer;->getBufferReadFlag()Z

    move-result v3

    if-nez v3, :cond_1

    .line 101
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lsiso/AutoCollage/NativeBuffer;->setBufferReadFlag(Z)V

    move-object v2, v1

    .line 102
    .end local v1    # "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    .restart local v2    # "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    goto :goto_1

    .line 93
    .end local v2    # "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    .restart local v1    # "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method FaceDetect(Ljava/lang/String;)[Ljava/lang/Object;
    .locals 16
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 264
    sget-object v13, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v14, "FaceDetect Method called for: "

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    const/4 v8, 0x0

    .line 266
    .local v8, "scaleW":F
    const/4 v7, 0x0

    .line 267
    .local v7, "scaleH":F
    const/4 v4, 0x0

    .line 268
    .local v4, "index":I
    const/4 v12, 0x0

    .line 269
    .local v12, "width":I
    const/4 v2, 0x0

    .line 273
    .local v2, "height":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lt v3, v13, :cond_2

    .line 301
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Lsiso/AutoCollage/rectpos;

    array-length v13, v13

    new-array v6, v13, [Lsiso/AutoCollage/rectpos;

    .line 303
    .local v6, "rect":[Lsiso/AutoCollage/rectpos;
    const/4 v3, 0x0

    :goto_2
    array-length v13, v6

    if-lt v3, v13, :cond_5

    .line 315
    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 318
    .local v5, "orientation":I
    sparse-switch v5, :sswitch_data_0

    .line 363
    sget-object v13, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v14, "angle not correct"

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    :cond_1
    :sswitch_0
    return-object v6

    .line 275
    .end local v5    # "orientation":I
    .end local v6    # "rect":[Lsiso/AutoCollage/rectpos;
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 277
    move v4, v3

    .line 279
    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lsiso/AutoCollage/NativeBuffer;

    iget v8, v13, Lsiso/AutoCollage/NativeBuffer;->bufscaleFactorW:F

    .line 280
    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lsiso/AutoCollage/NativeBuffer;

    iget v7, v13, Lsiso/AutoCollage/NativeBuffer;->bufscaleFactorH:F

    .line 281
    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lsiso/AutoCollage/NativeBuffer;

    iget v12, v13, Lsiso/AutoCollage/NativeBuffer;->bufWidth:I

    .line 282
    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lsiso/AutoCollage/NativeBuffer;

    iget v2, v13, Lsiso/AutoCollage/NativeBuffer;->bufHeight:I

    .line 283
    sget-object v13, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "FaceDetect Method called for: scaleW = "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "scaleH = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "width = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "Height = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 285
    .local v1, "angle":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lsiso/AutoCollage/NativeBuffer;

    iget-boolean v13, v13, Lsiso/AutoCollage/NativeBuffer;->m_bIsRotated:Z

    if-eqz v13, :cond_0

    .line 287
    const/16 v13, 0x5a

    if-eq v1, v13, :cond_3

    const/16 v13, 0x10e

    if-ne v1, v13, :cond_0

    .line 289
    :cond_3
    move v9, v12

    .line 290
    .local v9, "temp":I
    move v12, v2

    .line 291
    move v2, v9

    .line 294
    goto/16 :goto_1

    .line 273
    .end local v1    # "angle":I
    .end local v9    # "temp":I
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 305
    .restart local v6    # "rect":[Lsiso/AutoCollage/rectpos;
    :cond_5
    new-instance v13, Lsiso/AutoCollage/rectpos;

    invoke-direct {v13}, Lsiso/AutoCollage/rectpos;-><init>()V

    aput-object v13, v6, v3

    .line 306
    aget-object v14, v6, v3

    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Lsiso/AutoCollage/rectpos;

    aget-object v13, v13, v3

    iget v13, v13, Lsiso/AutoCollage/rectpos;->x:I

    int-to-float v13, v13

    mul-float/2addr v13, v8

    float-to-int v13, v13

    iput v13, v14, Lsiso/AutoCollage/rectpos;->x:I

    .line 307
    aget-object v14, v6, v3

    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Lsiso/AutoCollage/rectpos;

    aget-object v13, v13, v3

    iget v13, v13, Lsiso/AutoCollage/rectpos;->y:I

    int-to-float v13, v13

    mul-float/2addr v13, v7

    float-to-int v13, v13

    iput v13, v14, Lsiso/AutoCollage/rectpos;->y:I

    .line 308
    aget-object v14, v6, v3

    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Lsiso/AutoCollage/rectpos;

    aget-object v13, v13, v3

    iget v13, v13, Lsiso/AutoCollage/rectpos;->w:I

    int-to-float v13, v13

    mul-float/2addr v13, v8

    float-to-int v13, v13

    iput v13, v14, Lsiso/AutoCollage/rectpos;->w:I

    .line 309
    aget-object v14, v6, v3

    move-object/from16 v0, p0

    iget-object v13, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v13, v13, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Lsiso/AutoCollage/rectpos;

    aget-object v13, v13, v3

    iget v13, v13, Lsiso/AutoCollage/rectpos;->h:I

    int-to-float v13, v13

    mul-float/2addr v13, v7

    float-to-int v13, v13

    iput v13, v14, Lsiso/AutoCollage/rectpos;->h:I

    .line 303
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 325
    .restart local v5    # "orientation":I
    :sswitch_1
    const/4 v3, 0x0

    :goto_3
    array-length v13, v6

    if-ge v3, v13, :cond_1

    .line 327
    aget-object v13, v6, v3

    iget v11, v13, Lsiso/AutoCollage/rectpos;->x:I

    .line 328
    .local v11, "tempX":I
    aget-object v13, v6, v3

    aget-object v14, v6, v3

    iget v14, v14, Lsiso/AutoCollage/rectpos;->y:I

    aget-object v15, v6, v3

    iget v15, v15, Lsiso/AutoCollage/rectpos;->h:I

    add-int/2addr v14, v15

    sub-int v14, v2, v14

    iput v14, v13, Lsiso/AutoCollage/rectpos;->x:I

    .line 329
    aget-object v13, v6, v3

    iput v11, v13, Lsiso/AutoCollage/rectpos;->y:I

    .line 330
    aget-object v13, v6, v3

    iget v10, v13, Lsiso/AutoCollage/rectpos;->w:I

    .line 331
    .local v10, "tempW":I
    aget-object v13, v6, v3

    aget-object v14, v6, v3

    iget v14, v14, Lsiso/AutoCollage/rectpos;->h:I

    iput v14, v13, Lsiso/AutoCollage/rectpos;->w:I

    .line 332
    aget-object v13, v6, v3

    iput v10, v13, Lsiso/AutoCollage/rectpos;->h:I

    .line 325
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 339
    .end local v10    # "tempW":I
    .end local v11    # "tempX":I
    :sswitch_2
    const/4 v3, 0x0

    :goto_4
    array-length v13, v6

    if-ge v3, v13, :cond_1

    .line 341
    aget-object v13, v6, v3

    aget-object v14, v6, v3

    iget v14, v14, Lsiso/AutoCollage/rectpos;->w:I

    aget-object v15, v6, v3

    iget v15, v15, Lsiso/AutoCollage/rectpos;->x:I

    add-int/2addr v14, v15

    sub-int v14, v12, v14

    iput v14, v13, Lsiso/AutoCollage/rectpos;->x:I

    .line 342
    aget-object v13, v6, v3

    aget-object v14, v6, v3

    iget v14, v14, Lsiso/AutoCollage/rectpos;->h:I

    aget-object v15, v6, v3

    iget v15, v15, Lsiso/AutoCollage/rectpos;->y:I

    add-int/2addr v14, v15

    sub-int v14, v2, v14

    iput v14, v13, Lsiso/AutoCollage/rectpos;->y:I

    .line 343
    aget-object v13, v6, v3

    aget-object v14, v6, v3

    iget v14, v14, Lsiso/AutoCollage/rectpos;->w:I

    int-to-float v14, v14

    float-to-int v14, v14

    iput v14, v13, Lsiso/AutoCollage/rectpos;->w:I

    .line 344
    aget-object v13, v6, v3

    aget-object v14, v6, v3

    iget v14, v14, Lsiso/AutoCollage/rectpos;->h:I

    int-to-float v14, v14

    float-to-int v14, v14

    iput v14, v13, Lsiso/AutoCollage/rectpos;->h:I

    .line 339
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 351
    :sswitch_3
    const/4 v3, 0x0

    :goto_5
    array-length v13, v6

    if-ge v3, v13, :cond_1

    .line 353
    aget-object v13, v6, v3

    aget-object v14, v6, v3

    iget v14, v14, Lsiso/AutoCollage/rectpos;->y:I

    aget-object v15, v6, v3

    iget v15, v15, Lsiso/AutoCollage/rectpos;->h:I

    add-int/2addr v14, v15

    sub-int v14, v2, v14

    iput v14, v13, Lsiso/AutoCollage/rectpos;->x:I

    .line 354
    aget-object v13, v6, v3

    aget-object v14, v6, v3

    iget v14, v14, Lsiso/AutoCollage/rectpos;->y:I

    sub-int v14, v2, v14

    iput v14, v13, Lsiso/AutoCollage/rectpos;->y:I

    .line 355
    aget-object v13, v6, v3

    iget v10, v13, Lsiso/AutoCollage/rectpos;->w:I

    .line 356
    .restart local v10    # "tempW":I
    aget-object v13, v6, v3

    aget-object v14, v6, v3

    iget v14, v14, Lsiso/AutoCollage/rectpos;->h:I

    iput v14, v13, Lsiso/AutoCollage/rectpos;->w:I

    .line 357
    aget-object v13, v6, v3

    iput v10, v13, Lsiso/AutoCollage/rectpos;->h:I

    .line 351
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 318
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method GetImageWidthHeight(Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p1, "szArg"    # Ljava/lang/String;

    .prologue
    .line 375
    new-instance v1, Lsiso/AutoCollage/rectpos;

    invoke-direct {v1}, Lsiso/AutoCollage/rectpos;-><init>()V

    .line 376
    .local v1, "imagedim":Lsiso/AutoCollage/rectpos;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v4, v4, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v0, v4, :cond_1

    .line 395
    :cond_0
    :goto_1
    return-object v1

    .line 378
    :cond_1
    iget-object v4, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v4, v4, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_3

    .line 380
    iget-object v4, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v4, v4, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsiso/AutoCollage/NativeBuffer;

    .line 381
    .local v2, "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    iget v4, v2, Lsiso/AutoCollage/NativeBuffer;->bufWidth:I

    iput v4, v1, Lsiso/AutoCollage/rectpos;->x:I

    .line 382
    iget v4, v2, Lsiso/AutoCollage/NativeBuffer;->bufHeight:I

    iput v4, v1, Lsiso/AutoCollage/rectpos;->y:I

    .line 383
    iget-object v4, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v4, v4, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsiso/AutoCollage/NativeBuffer;

    iget-boolean v4, v4, Lsiso/AutoCollage/NativeBuffer;->m_bIsRotated:Z

    if-eqz v4, :cond_0

    .line 385
    iget-object v4, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v4, v4, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x5a

    if-eq v4, v5, :cond_2

    iget-object v4, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v4, v4, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x10e

    if-ne v4, v5, :cond_0

    .line 387
    :cond_2
    iget v3, v1, Lsiso/AutoCollage/rectpos;->x:I

    .line 388
    .local v3, "tempI":I
    iget v4, v1, Lsiso/AutoCollage/rectpos;->y:I

    iput v4, v1, Lsiso/AutoCollage/rectpos;->x:I

    .line 389
    iput v3, v1, Lsiso/AutoCollage/rectpos;->y:I

    goto :goto_1

    .line 376
    .end local v2    # "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    .end local v3    # "tempI":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method GetOrientation(Ljava/lang/String;I)I
    .locals 10
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "key"    # I

    .prologue
    const/4 v9, -0x1

    .line 403
    sget-object v6, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "GetOrientation  key "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    iget-object v6, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v6, v6, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget-boolean v6, v6, Lsiso/AutoCollage/SaCConfig;->m_bMediaDBEnabled:Z

    if-eqz v6, :cond_3

    .line 406
    const/4 v4, 0x0

    .local v4, "index":I
    :goto_0
    iget-object v6, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v6, v6, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v4, v6, :cond_1

    .line 424
    sget-object v6, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v7, "GetOrientation : No match found so returning 0"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    const/4 v0, 0x0

    .line 496
    .end local v4    # "index":I
    :cond_0
    :goto_1
    :pswitch_0
    return v0

    .line 408
    .restart local v4    # "index":I
    :cond_1
    iget-object v6, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v6, v6, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {p0, p1, v6}, Lsiso/AutoCollage/AndroidApiCalls;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 410
    iget-object v6, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v6, v6, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lsiso/AutoCollage/NativeBuffer;

    iget v6, v6, Lsiso/AutoCollage/NativeBuffer;->bufferKey:I

    if-ne v6, p2, :cond_2

    .line 412
    iget-object v6, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v6, v6, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lsiso/AutoCollage/NativeBuffer;

    iget-boolean v6, v6, Lsiso/AutoCollage/NativeBuffer;->m_bIsRotated:Z

    if-nez v6, :cond_2

    .line 414
    iget-object v6, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v6, v6, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 416
    sget-object v6, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "GetOrientation : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v8, v8, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    iget-object v6, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v6, v6, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 406
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 429
    .end local v4    # "index":I
    :cond_3
    const/4 v2, 0x0

    .line 430
    .local v2, "exif":Landroid/media/ExifInterface;
    const/4 v0, 0x0

    .line 433
    .local v0, "degree":I
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "exif":Landroid/media/ExifInterface;
    .local v3, "exif":Landroid/media/ExifInterface;
    move-object v2, v3

    .line 440
    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    :goto_2
    if-eqz v2, :cond_0

    .line 442
    const-string v6, "Orientation"

    invoke-virtual {v2, v6, v9}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v5

    .line 443
    .local v5, "orientation":I
    if-eq v5, v9, :cond_0

    .line 446
    packed-switch v5, :pswitch_data_0

    goto :goto_1

    .line 463
    :pswitch_1
    const/16 v0, 0xb4

    .line 464
    goto :goto_1

    .line 435
    .end local v5    # "orientation":I
    :catch_0
    move-exception v1

    .line 437
    .local v1, "e":Ljava/io/IOException;
    sget-object v6, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v7, "cannot read exif"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 468
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v5    # "orientation":I
    :pswitch_2
    const/16 v0, 0x10e

    .line 469
    goto/16 :goto_1

    .line 473
    :pswitch_3
    const/16 v0, 0x5a

    .line 474
    goto/16 :goto_1

    .line 446
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public closeDb()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lsiso/AutoCollage/AndroidApiCalls;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lsiso/AutoCollage/AndroidApiCalls;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 48
    :cond_0
    return-void
.end method

.method compare(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "str1"    # Ljava/lang/String;
    .param p2, "str2"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 513
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 527
    :cond_0
    :goto_0
    return v1

    .line 519
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 527
    const/4 v1, 0x1

    goto :goto_0

    .line 521
    :cond_2
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v2

    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v3

    if-ne v2, v3, :cond_0

    .line 519
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method getCoreCount()I
    .locals 1

    .prologue
    .line 507
    sget v0, Lsiso/AutoCollage/AndroidApiCalls;->NUMBER_OF_CORES:I

    return v0
.end method

.method getFaceRect(Landroid/content/Context;I)[Lsiso/AutoCollage/rectpos;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I

    .prologue
    const/4 v10, 0x0

    .line 57
    const/4 v9, 0x0

    :try_start_0
    check-cast v9, [Lsiso/AutoCollage/rectpos;

    .line 59
    .local v9, "rt":[Lsiso/AutoCollage/rectpos;
    iget-object v0, p0, Lsiso/AutoCollage/AndroidApiCalls;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://media/external/faces"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 60
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "image_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "pos_left"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "pos_top"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "pos_right"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    .line 61
    const-string v4, "pos_bottom"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "image_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 59
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 63
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v9, v0, [Lsiso/AutoCollage/rectpos;

    .line 64
    const/4 v8, 0x0

    .line 65
    .local v8, "i":I
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    :cond_0
    new-instance v0, Lsiso/AutoCollage/rectpos;

    invoke-direct {v0}, Lsiso/AutoCollage/rectpos;-><init>()V

    aput-object v0, v9, v8

    .line 70
    aget-object v0, v9, v8

    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-float v1, v1

    float-to-int v1, v1

    iput v1, v0, Lsiso/AutoCollage/rectpos;->x:I

    .line 71
    aget-object v0, v9, v8

    const/4 v1, 0x2

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-float v1, v1

    float-to-int v1, v1

    iput v1, v0, Lsiso/AutoCollage/rectpos;->y:I

    .line 72
    aget-object v0, v9, v8

    const/4 v1, 0x3

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-float v1, v1

    aget-object v2, v9, v8

    iget v2, v2, Lsiso/AutoCollage/rectpos;->x:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Lsiso/AutoCollage/rectpos;->w:I

    .line 73
    aget-object v0, v9, v8

    const/4 v1, 0x4

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-float v1, v1

    aget-object v2, v9, v8

    iget v2, v2, Lsiso/AutoCollage/rectpos;->y:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Lsiso/AutoCollage/rectpos;->h:I

    .line 74
    add-int/lit8 v8, v8, 0x1

    .line 75
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    .line 67
    if-nez v0, :cond_0

    .line 77
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v8    # "i":I
    .end local v9    # "rt":[Lsiso/AutoCollage/rectpos;
    :goto_0
    return-object v9

    .line 80
    :catch_0
    move-exception v7

    .line 82
    .local v7, "e":Ljava/lang/IllegalStateException;
    sget-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v9, v10

    .line 84
    goto :goto_0
.end method

.method getOutputBuffer()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v0, v0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    invoke-virtual {v0}, Lsiso/AutoCollage/Savedbuffer;->GetBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public init()Z
    .locals 22

    .prologue
    .line 115
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->cr:Landroid/content/ContentResolver;

    .line 119
    const-string v2, "content://media/external/face_scanner"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 120
    .local v3, "FACE_SCANNER_REQUEST_URI":Landroid/net/Uri;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->cursor:Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 123
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->cr:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->cursor:Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    .line 133
    const/4 v2, 0x4

    :try_start_2
    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "title"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "_data"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "orientation"

    aput-object v5, v2, v4

    move-object/from16 v0, p0

    iput-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->columns:[Ljava/lang/String;

    .line 134
    move-object/from16 v0, p0

    iget-object v4, v0, Lsiso/AutoCollage/AndroidApiCalls;->cr:Landroid/content/ContentResolver;

    sget-object v5, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lsiso/AutoCollage/AndroidApiCalls;->columns:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "bucket_id"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->cursor:Landroid/database/Cursor;

    .line 135
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->cursor:Landroid/database/Cursor;

    const-string v4, "_id"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->idIndex:I

    .line 136
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->cursor:Landroid/database/Cursor;

    const-string v4, "_data"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->dataIndex:I

    .line 137
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->cursor:Landroid/database/Cursor;

    const-string v4, "orientation"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->orentIndex:I

    .line 139
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v4, "calling init"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    const/4 v12, 0x0

    .local v12, "f":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v12, v2, :cond_0

    .line 160
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 162
    .local v18, "timeDecoding":J
    new-instance v15, Lsiso/AutoCollage/Threading;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    invoke-direct {v15, v2}, Lsiso/AutoCollage/Threading;-><init>(Lsiso/AutoCollage/Collage;)V

    .line 163
    .local v15, "pthread":Lsiso/AutoCollage/Threading;
    const/4 v10, 0x0

    .line 167
    .local v10, "count":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sget v4, Lsiso/AutoCollage/AndroidApiCalls;->NUMBER_OF_CORES:I

    if-lt v2, v4, :cond_4

    .line 169
    sget v10, Lsiso/AutoCollage/AndroidApiCalls;->NUMBER_OF_CORES:I

    .line 177
    :goto_1
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "calling DecodedOnThread = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    const/4 v14, 0x0

    .local v14, "m":I
    :goto_2
    if-lt v14, v10, :cond_5

    .line 182
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v4, "calling DecodedOnThread end"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 188
    .local v20, "timeFace":J
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v13, v2, :cond_6

    .line 196
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Time Profiling : Face Detection : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v6, v20

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-virtual/range {p0 .. p0}, Lsiso/AutoCollage/AndroidApiCalls;->closeDb()V

    .line 200
    invoke-virtual {v15}, Lsiso/AutoCollage/Threading;->WaitForEndingThread()V

    .line 202
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Time Profiling : Decoding & FaceDetection : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v6, v18

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    const/4 v13, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v2

    if-lt v13, v2, :cond_8

    .line 219
    .end local v3    # "FACE_SCANNER_REQUEST_URI":Landroid/net/Uri;
    .end local v10    # "count":I
    .end local v12    # "f":I
    .end local v13    # "i":I
    .end local v14    # "m":I
    .end local v15    # "pthread":Lsiso/AutoCollage/Threading;
    .end local v18    # "timeDecoding":J
    .end local v20    # "timeFace":J
    :goto_5
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v4, "init is done"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 v2, 0x1

    :goto_6
    return v2

    .line 125
    .restart local v3    # "FACE_SCANNER_REQUEST_URI":Landroid/net/Uri;
    :catch_0
    move-exception v11

    .line 127
    .local v11, "e":Ljava/lang/IllegalStateException;
    :try_start_3
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    const/4 v2, 0x0

    goto :goto_6

    .line 142
    .end local v11    # "e":Ljava/lang/IllegalStateException;
    .restart local v12    # "f":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_1

    .line 144
    const/16 v16, 0x0

    .local v16, "q":I
    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->cursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    move/from16 v0, v16

    if-lt v0, v2, :cond_2

    .line 140
    .end local v16    # "q":I
    :cond_1
    :goto_8
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 146
    .restart local v16    # "q":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->cursor:Landroid/database/Cursor;

    move/from16 v0, v16

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 147
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->cursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v4, v0, Lsiso/AutoCollage/AndroidApiCalls;->dataIndex:I

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 148
    .local v17, "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 150
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsiso/AutoCollage/AndroidApiCalls;->cursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v5, v0, Lsiso/AutoCollage/AndroidApiCalls;->idIndex:I

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v12, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 151
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsiso/AutoCollage/AndroidApiCalls;->cursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v5, v0, Lsiso/AutoCollage/AndroidApiCalls;->orentIndex:I

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v12, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_8

    .line 215
    .end local v3    # "FACE_SCANNER_REQUEST_URI":Landroid/net/Uri;
    .end local v12    # "f":I
    .end local v16    # "q":I
    .end local v17    # "temp":Ljava/lang/String;
    :catch_1
    move-exception v11

    .line 217
    .restart local v11    # "e":Ljava/lang/IllegalStateException;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 144
    .end local v11    # "e":Ljava/lang/IllegalStateException;
    .restart local v3    # "FACE_SCANNER_REQUEST_URI":Landroid/net/Uri;
    .restart local v12    # "f":I
    .restart local v16    # "q":I
    .restart local v17    # "temp":Ljava/lang/String;
    :cond_3
    add-int/lit8 v16, v16, 0x1

    goto :goto_7

    .line 173
    .end local v16    # "q":I
    .end local v17    # "temp":Ljava/lang/String;
    .restart local v10    # "count":I
    .restart local v15    # "pthread":Lsiso/AutoCollage/Threading;
    .restart local v18    # "timeDecoding":J
    :cond_4
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v10

    goto/16 :goto_1

    .line 180
    .restart local v14    # "m":I
    :cond_5
    invoke-virtual {v15, v14}, Lsiso/AutoCollage/Threading;->DecodedOnThread(I)V

    .line 178
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2

    .line 190
    .restart local v13    # "i":I
    .restart local v20    # "timeFace":J
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_7

    .line 192
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v4, v2, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v5, v2, Lsiso/AutoCollage/Collage;->context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v2}, Lsiso/AutoCollage/AndroidApiCalls;->getFaceRect(Landroid/content/Context;I)[Lsiso/AutoCollage/rectpos;

    move-result-object v2

    invoke-virtual {v4, v13, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 188
    :cond_7
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_3

    .line 209
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v2

    if-nez v2, :cond_9

    .line 211
    const/4 v2, 0x0

    goto/16 :goto_6

    .line 207
    :cond_9
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_4
.end method

.method public initWithoutMediaDB()Z
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 226
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 227
    .local v6, "timeDecoding":J
    new-instance v4, Lsiso/AutoCollage/Threading;

    iget-object v8, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    invoke-direct {v4, v8}, Lsiso/AutoCollage/Threading;-><init>(Lsiso/AutoCollage/Collage;)V

    .line 228
    .local v4, "pthread":Lsiso/AutoCollage/Threading;
    const/4 v0, 0x0

    .line 230
    .local v0, "count":I
    iget-object v8, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v8, v8, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    sget v9, Lsiso/AutoCollage/AndroidApiCalls;->NUMBER_OF_CORES:I

    if-lt v8, v9, :cond_1

    .line 232
    sget v0, Lsiso/AutoCollage/AndroidApiCalls;->NUMBER_OF_CORES:I

    .line 238
    :goto_0
    const/4 v3, 0x0

    .local v3, "m":I
    :goto_1
    if-lt v3, v0, :cond_2

    .line 242
    invoke-virtual {v4}, Lsiso/AutoCollage/Threading;->WaitForEndingThread()V

    .line 243
    sget-object v8, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Time Profiling : Decoding  : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v6

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    iget-object v8, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v8, v8, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v2, v8, :cond_3

    .line 257
    const/4 v5, 0x1

    .end local v0    # "count":I
    .end local v2    # "i":I
    .end local v3    # "m":I
    .end local v4    # "pthread":Lsiso/AutoCollage/Threading;
    .end local v6    # "timeDecoding":J
    :cond_0
    :goto_3
    return v5

    .line 236
    .restart local v0    # "count":I
    .restart local v4    # "pthread":Lsiso/AutoCollage/Threading;
    .restart local v6    # "timeDecoding":J
    :cond_1
    iget-object v8, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v8, v8, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    .line 240
    .restart local v3    # "m":I
    :cond_2
    invoke-virtual {v4, v3}, Lsiso/AutoCollage/Threading;->DecodedOnThread(I)V

    .line 238
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 246
    .restart local v2    # "i":I
    :cond_3
    iget-object v8, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iget-object v8, v8, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    if-eqz v8, :cond_0

    .line 244
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 252
    .end local v0    # "count":I
    .end local v2    # "i":I
    .end local v3    # "m":I
    .end local v4    # "pthread":Lsiso/AutoCollage/Threading;
    .end local v6    # "timeDecoding":J
    :catch_0
    move-exception v1

    .line 254
    .local v1, "e":Ljava/lang/IllegalStateException;
    sget-object v8, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Init Method failed with Exception : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method response(I)Z
    .locals 3
    .param p1, "resp"    # I

    .prologue
    .line 531
    iget-object v0, p0, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    invoke-virtual {v0, p1}, Lsiso/AutoCollage/Collage;->response(I)V

    .line 532
    sget-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Async Reponse coming "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    const/4 v0, 0x1

    return v0
.end method
