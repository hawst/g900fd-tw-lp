.class interface abstract Lsiso/AutoCollage/Collage$CollageResponse;
.super Ljava/lang/Object;
.source "Collage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsiso/AutoCollage/Collage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "CollageResponse"
.end annotation


# static fields
.field public static final SAC_RESPONSE_ADD_IMAGE_DONE:I = 0xc

.field public static final SAC_RESPONSE_CREATE_COLLAGE_DONE:I = 0x2

.field public static final SAC_RESPONSE_DELETE_IMAGE_DONE:I = 0xd

.field public static final SAC_RESPONSE_EDITING_OPERATION_FAILED:I = 0x10

.field public static final SAC_RESPONSE_EVENT_DEINIT_DONE:I = 0x3

.field public static final SAC_RESPONSE_EVENT_PROCESSING:I = 0x5

.field public static final SAC_RESPONSE_EVENT_READY_FOR_PREVIEW:I = 0x4

.field public static final SAC_RESPONSE_GENERIC_ERROR:I = 0x6

.field public static final SAC_RESPONSE_INIT_COLLAGE_DONE:I = 0x1

.field public static final SAC_RESPONSE_INSUFFICIENT_MEMORY:I = 0x7

.field public static final SAC_RESPONSE_LOAD_DECODED_BUFFER_DATA_DONE:I = 0x11

.field public static final SAC_RESPONSE_LOAD_ROI_INFO_DONE:I = 0xf

.field public static final SAC_RESPONSE_LOAD_ROI_INFO_WO_BUFFER_DONE:I = 0x12

.field public static final SAC_RESPONSE_ROI_REPLACE_DONE:I = 0xb

.field public static final SAC_RESPONSE_ROI_RESIZE_DONE:I = 0xa

.field public static final SAC_RESPONSE_ROI_SWAPPING_DONE:I = 0x8

.field public static final SAC_RESPONSE_ROI_TRANSLATION_DONE:I = 0x9

.field public static final SAC_RESPONSE_SAVE_ROI_INFO_DONE:I = 0xe


# virtual methods
.method public abstract onCollageResponse(I)V
.end method
