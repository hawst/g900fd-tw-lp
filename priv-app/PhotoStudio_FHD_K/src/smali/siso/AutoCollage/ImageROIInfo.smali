.class public Lsiso/AutoCollage/ImageROIInfo;
.super Ljava/lang/Object;
.source "ImageROIInfo.java"


# instance fields
.field private bufferKey:[I

.field private count:I

.field private height:[I

.field private imageFilePath:[Ljava/lang/String;

.field pixelFormat:I

.field private width:[I

.field private xROI:[I

.field private yROI:[I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lsiso/AutoCollage/ImageROIInfo;->pixelFormat:I

    .line 25
    new-array v0, p1, [Ljava/lang/String;

    iput-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->imageFilePath:[Ljava/lang/String;

    .line 26
    new-array v0, p1, [I

    iput-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->xROI:[I

    .line 27
    new-array v0, p1, [I

    iput-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->yROI:[I

    .line 28
    new-array v0, p1, [I

    iput-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->width:[I

    .line 29
    new-array v0, p1, [I

    iput-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->height:[I

    .line 30
    new-array v0, p1, [I

    iput-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->bufferKey:[I

    .line 31
    iput p1, p0, Lsiso/AutoCollage/ImageROIInfo;->count:I

    .line 32
    return-void
.end method


# virtual methods
.method public Add(Lsiso/AutoCollage/AndroidApiCalls;Lsiso/AutoCollage/ImageROIInfo;)V
    .locals 1
    .param p1, "androidApiCalls"    # Lsiso/AutoCollage/AndroidApiCalls;
    .param p2, "pObject"    # Lsiso/AutoCollage/ImageROIInfo;

    .prologue
    .line 143
    iget-object v0, p1, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iput-object p2, v0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    .line 144
    return-void
.end method

.method public SetData(Ljava/lang/String;IIIIIIIIII)V
    .locals 3
    .param p1, "imageFilePath"    # Ljava/lang/String;
    .param p2, "xROI"    # I
    .param p3, "yROI"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "xFinalRoi"    # I
    .param p7, "YFinalRoi"    # I
    .param p8, "Finalwidth"    # I
    .param p9, "FinalHeight"    # I
    .param p10, "index"    # I
    .param p11, "key"    # I

    .prologue
    .line 39
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->imageFilePath:[Ljava/lang/String;

    aput-object p1, v0, p10

    .line 40
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->xROI:[I

    aput p2, v0, p10

    .line 41
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->yROI:[I

    aput p3, v0, p10

    .line 42
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->width:[I

    aput p4, v0, p10

    .line 43
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->height:[I

    aput p5, v0, p10

    .line 44
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->bufferKey:[I

    aput p11, v0, p10

    .line 45
    sget-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ImageROIInfo setData index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " key "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    return-void
.end method

.method public SetPixelFormat(I)V
    .locals 0
    .param p1, "pixelFormat"    # I

    .prologue
    .line 52
    iput p1, p0, Lsiso/AutoCollage/ImageROIInfo;->pixelFormat:I

    .line 53
    return-void
.end method

.method public getAllImagePath()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->imageFilePath:[Ljava/lang/String;

    return-object v0
.end method

.method public getBufferKey(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 115
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->bufferKey:[I

    aget v0, v0, p1

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lsiso/AutoCollage/ImageROIInfo;->count:I

    return v0
.end method

.method public getHeight(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 129
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->height:[I

    aget v0, v0, p1

    return v0
.end method

.method public getImageFilePath(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 59
    const/4 v0, 0x0

    .line 60
    .local v0, "filePath":Ljava/lang/String;
    iget-object v1, p0, Lsiso/AutoCollage/ImageROIInfo;->imageFilePath:[Ljava/lang/String;

    aget-object v0, v1, p1

    .line 61
    return-object v0
.end method

.method public getWidth(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->width:[I

    aget v0, v0, p1

    return v0
.end method

.method public getxROI(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->xROI:[I

    aget v0, v0, p1

    return v0
.end method

.method public getyROI(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->yROI:[I

    aget v0, v0, p1

    return v0
.end method

.method public setHeight(II)V
    .locals 1
    .param p1, "height"    # I
    .param p2, "index"    # I

    .prologue
    .line 136
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->height:[I

    aput p1, v0, p2

    .line 137
    return-void
.end method

.method public setImageFilePath(Ljava/lang/String;I)V
    .locals 1
    .param p1, "imageFilePath"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->imageFilePath:[Ljava/lang/String;

    aput-object p1, v0, p2

    .line 76
    return-void
.end method

.method public setWidth(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "index"    # I

    .prologue
    .line 122
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->width:[I

    aput p1, v0, p2

    .line 123
    return-void
.end method

.method public setxROI(II)V
    .locals 1
    .param p1, "xROI"    # I
    .param p2, "index"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->xROI:[I

    aput p1, v0, p2

    .line 90
    return-void
.end method

.method public setyROI(II)V
    .locals 1
    .param p1, "yROI"    # I
    .param p2, "index"    # I

    .prologue
    .line 103
    iget-object v0, p0, Lsiso/AutoCollage/ImageROIInfo;->yROI:[I

    aput p1, v0, p2

    .line 104
    return-void
.end method
