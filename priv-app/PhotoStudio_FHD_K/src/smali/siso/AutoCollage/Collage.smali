.class public Lsiso/AutoCollage/Collage;
.super Ljava/lang/Object;
.source "Collage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsiso/AutoCollage/Collage$CollageResponse;,
        Lsiso/AutoCollage/Collage$FileFormat;,
        Lsiso/AutoCollage/Collage$PixelFormat;
    }
.end annotation


# static fields
.field static TAG:Ljava/lang/String;

.field private static m_bAllowRequest:Z


# instance fields
.field context:Landroid/content/Context;

.field m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

.field m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

.field m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

.field m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

.field m_IsEngineError:Z

.field m_SacConfig:Lsiso/AutoCollage/SaCConfig;

.field m_WaitingOver:Z

.field private m_eLastError:Ljava/lang/String;

.field m_iBufferKey:I

.field m_nSacControllerID:I

.field m_outputBitmap:Landroid/graphics/Bitmap;

.field m_szAngleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field m_szFaceRectInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[",
            "Lsiso/AutoCollage/rectpos;",
            ">;"
        }
    .end annotation
.end field

.field m_szImageID:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field m_szInputFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field m_szInputImageBuffers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lsiso/AutoCollage/NativeBuffer;",
            ">;"
        }
    .end annotation
.end field

.field m_szIsAlreadySent:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field m_szOrientInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "COLLAGE"

    sput-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    sget-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v1, "jar LIB Version CollageLib_WK35Y2013R01"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iput-object p1, p0, Lsiso/AutoCollage/Collage;->context:Landroid/content/Context;

    .line 88
    new-instance v0, Lsiso/AutoCollage/SaCConfig;

    invoke-direct {v0}, Lsiso/AutoCollage/SaCConfig;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    .line 89
    iput-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    .line 90
    iput-object v2, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    .line 91
    iput-object v2, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    .line 92
    iput-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_WaitingOver:Z

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    .line 94
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iput-boolean v4, v0, Lsiso/AutoCollage/SaCConfig;->m_bEnableBlending:Z

    .line 95
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iput-boolean v4, v0, Lsiso/AutoCollage/SaCConfig;->m_bEnableFaceDetection:Z

    .line 96
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iput-boolean v4, v0, Lsiso/AutoCollage/SaCConfig;->m_bMediaDBEnabled:Z

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    .line 103
    iput-object v2, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    .line 104
    iput-object v2, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    .line 105
    iput-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    .line 106
    iput v3, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    .line 107
    iput v3, p0, Lsiso/AutoCollage/Collage;->m_iBufferKey:I

    .line 108
    iput-object v2, p0, Lsiso/AutoCollage/Collage;->m_outputBitmap:Landroid/graphics/Bitmap;

    .line 109
    sput-boolean v4, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    .line 110
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lsiso/AutoCollage/Collage$CollageResponse;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callback"    # Lsiso/AutoCollage/Collage$CollageResponse;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    sget-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v1, "jar LIB Version CollageLib_WK39Y2013R01"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iput-object p1, p0, Lsiso/AutoCollage/Collage;->context:Landroid/content/Context;

    .line 116
    new-instance v0, Lsiso/AutoCollage/SaCConfig;

    invoke-direct {v0}, Lsiso/AutoCollage/SaCConfig;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    .line 117
    iput-object v4, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    .line 118
    iput-object v4, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    .line 119
    iput-object v4, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    .line 120
    iput-boolean v2, p0, Lsiso/AutoCollage/Collage;->m_WaitingOver:Z

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    .line 122
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iput-boolean v3, v0, Lsiso/AutoCollage/SaCConfig;->m_bEnableBlending:Z

    .line 123
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iput-boolean v3, v0, Lsiso/AutoCollage/SaCConfig;->m_bEnableFaceDetection:Z

    .line 124
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iput-boolean v3, v0, Lsiso/AutoCollage/SaCConfig;->m_bMediaDBEnabled:Z

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    .line 131
    iput-object v4, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    .line 132
    iput-object p2, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    .line 133
    iput-boolean v2, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    .line 134
    iput v2, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    .line 135
    iput v2, p0, Lsiso/AutoCollage/Collage;->m_iBufferKey:I

    .line 136
    sput-boolean v3, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    .line 137
    return-void
.end method

.method private ArrayListToArrayInt(Ljava/util/ArrayList;)[I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)[I"
        }
    .end annotation

    .prologue
    .line 1981
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v0, v2, [I

    .line 1982
    .local v0, "array":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 1986
    return-object v0

    .line 1984
    :cond_0
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v0, v1

    .line 1982
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private ArrayListToArrayString(Ljava/util/ArrayList;)[Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1971
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v0, v2, [Ljava/lang/String;

    .line 1972
    .local v0, "array":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 1976
    return-object v0

    .line 1974
    :cond_0
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v0, v1

    .line 1972
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private checkIfRotated(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 27
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "roiFileName"    # Ljava/lang/String;

    .prologue
    .line 2226
    const/4 v15, 0x0

    .line 2227
    .local v15, "isRotated":Z
    const-wide/16 v10, 0x0

    .line 2235
    .local v10, "filelength":J
    :try_start_0
    new-instance v8, Ljava/io/RandomAccessFile;

    const-string v3, "r"

    move-object/from16 v0, p1

    invoke-direct {v8, v0, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2236
    .local v8, "fr":Ljava/io/RandomAccessFile;
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v10

    .line 2237
    const-wide/16 v4, 0x1e

    sub-long v4, v10, v4

    invoke-virtual {v8, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 2238
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v17

    .line 2239
    .local v17, "seekpos":I
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v17

    .line 2241
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2242
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    move-result-object v20

    .line 2246
    .local v20, "string":Ljava/lang/String;
    const-string v3, "COLLAGE_21061981_15011982"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2251
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    .line 2255
    .local v14, "isPhotoNote":Ljava/lang/Boolean;
    move/from16 v0, v17

    int-to-long v6, v0

    invoke-virtual {v14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-direct/range {v3 .. v11}, Lsiso/AutoCollage/Collage;->loadMultiGridData(Ljava/lang/String;Ljava/lang/String;JLjava/io/RandomAccessFile;ZJ)Z

    move-result v15

    move v3, v15

    .line 2330
    .end local v8    # "fr":Ljava/io/RandomAccessFile;
    .end local v14    # "isPhotoNote":Ljava/lang/Boolean;
    .end local v17    # "seekpos":I
    .end local v20    # "string":Ljava/lang/String;
    :goto_0
    return v3

    .line 2259
    .restart local v8    # "fr":Ljava/io/RandomAccessFile;
    .restart local v17    # "seekpos":I
    .restart local v20    # "string":Ljava/lang/String;
    :cond_0
    const-string v3, "Samsung"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2264
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    .line 2265
    new-instance v8, Ljava/io/RandomAccessFile;

    .end local v8    # "fr":Ljava/io/RandomAccessFile;
    const-string v3, "r"

    move-object/from16 v0, p1

    invoke-direct {v8, v0, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2266
    .restart local v8    # "fr":Ljava/io/RandomAccessFile;
    const-wide/16 v18, 0x0

    .line 2270
    .local v18, "seekposphotonote":J
    const-wide/16 v4, 0x10

    sub-long v4, v10, v4

    invoke-virtual {v8, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 2275
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    .line 2276
    .restart local v14    # "isPhotoNote":Ljava/lang/Boolean;
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v3

    int-to-char v0, v3

    move/from16 v26, v0

    .line 2277
    .local v26, "unitPlace":C
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v3

    int-to-char v0, v3

    move/from16 v25, v0

    .line 2278
    .local v25, "tensPlace":C
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v3

    int-to-char v13, v3

    .line 2279
    .local v13, "hundPlace":C
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v3

    int-to-char v0, v3

    move/from16 v24, v0

    .line 2281
    .local v24, "tenThousPlace":C
    const-wide/16 v22, 0x0

    .line 2282
    .local v22, "temp":J
    and-int/lit8 v3, v26, 0xf

    int-to-long v0, v3

    move-wide/from16 v22, v0

    .line 2283
    shr-int/lit8 v3, v26, 0x4

    mul-int/lit8 v3, v3, 0x10

    int-to-long v4, v3

    add-long v4, v4, v22

    const-wide/16 v6, 0xff

    and-long v22, v4, v6

    .line 2285
    and-int/lit8 v3, v25, 0xf

    mul-int/lit16 v3, v3, 0x100

    shr-int/lit8 v4, v25, 0x4

    mul-int/lit16 v4, v4, 0x1000

    add-int/2addr v3, v4

    int-to-long v4, v3

    add-long v4, v4, v22

    const-wide/32 v6, 0xffff

    and-long v22, v4, v6

    .line 2286
    and-int/lit8 v3, v13, 0xf

    const/high16 v4, 0x10000

    mul-int/2addr v3, v4

    shr-int/lit8 v4, v13, 0x4

    const/high16 v5, 0x100000

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    int-to-long v4, v3

    add-long v4, v4, v22

    const-wide/32 v6, 0xffffff

    and-long v22, v4, v6

    .line 2288
    and-int/lit8 v3, v24, 0xf

    const/high16 v4, 0x1000000

    mul-int/2addr v3, v4

    shr-int/lit8 v4, v24, 0x4

    const/high16 v5, 0x10000000

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    int-to-long v4, v3

    add-long v4, v4, v22

    const-wide/16 v6, -0x1

    and-long v18, v4, v6

    .line 2290
    const/16 v16, 0x0

    .line 2292
    .local v16, "markerfound":Z
    sub-long v4, v10, v18

    const-wide/16 v6, 0x2d

    sub-long/2addr v4, v6

    invoke-virtual {v8, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 2293
    const/4 v2, 0x0

    .line 2295
    .local v2, "collageseekpos":I
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    move-result-object v21

    .line 2299
    .local v21, "stringvar":Ljava/lang/String;
    const-string v3, "COLLAGE_21061981_15011982"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2301
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 2302
    .local v12, "filePointer":Ljava/lang/Long;
    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v3

    int-to-long v6, v3

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x7

    sub-long/2addr v4, v6

    invoke-virtual {v8, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 2303
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v2

    .line 2304
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v2

    .line 2305
    const/16 v16, 0x1

    .line 2308
    .end local v12    # "filePointer":Ljava/lang/Long;
    :cond_1
    if-nez v16, :cond_2

    .line 2310
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    .line 2311
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 2316
    :cond_2
    int-to-long v4, v2

    add-long v6, v18, v4

    invoke-virtual {v14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-direct/range {v3 .. v11}, Lsiso/AutoCollage/Collage;->loadMultiGridData(Ljava/lang/String;Ljava/lang/String;JLjava/io/RandomAccessFile;ZJ)Z

    move-result v15

    .line 2317
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    move v3, v15

    .line 2318
    goto/16 :goto_0

    .line 2323
    .end local v2    # "collageseekpos":I
    .end local v13    # "hundPlace":C
    .end local v14    # "isPhotoNote":Ljava/lang/Boolean;
    .end local v16    # "markerfound":Z
    .end local v18    # "seekposphotonote":J
    .end local v21    # "stringvar":Ljava/lang/String;
    .end local v22    # "temp":J
    .end local v24    # "tenThousPlace":C
    .end local v25    # "tensPlace":C
    .end local v26    # "unitPlace":C
    :cond_3
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2324
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 2327
    .end local v8    # "fr":Ljava/io/RandomAccessFile;
    .end local v17    # "seekpos":I
    .end local v20    # "string":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 2330
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method private convertIndex(I)I
    .locals 7
    .param p1, "indexROI"    # I

    .prologue
    .line 2154
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v4}, Lsiso/AutoCollage/ImageROIInfo;->getCount()I

    move-result v4

    if-lt v2, v4, :cond_0

    .line 2174
    sget-object v4, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v5, "convertIndex index not matching match "

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2175
    const/4 v2, -0x1

    .end local v2    # "i":I
    :goto_1
    return v2

    .line 2156
    .restart local v2    # "i":I
    :cond_0
    sget-object v4, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "indedx = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2159
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v4, p1}, Lsiso/AutoCollage/ImageROIInfo;->getImageFilePath(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2161
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsiso/AutoCollage/NativeBuffer;

    .line 2162
    .local v0, "buffer":Lsiso/AutoCollage/NativeBuffer;
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v4, p1}, Lsiso/AutoCollage/ImageROIInfo;->getBufferKey(I)I

    move-result v3

    .line 2163
    .local v3, "roiInfoKey":I
    invoke-virtual {v0}, Lsiso/AutoCollage/NativeBuffer;->getBufferKey()I

    move-result v1

    .line 2164
    .local v1, "bufferKey":I
    sget-object v4, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "roiInfoKey  "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  bufferKey "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2165
    if-ne v3, v1, :cond_1

    .line 2167
    sget-object v4, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "convertIndex index match "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2154
    .end local v0    # "buffer":Lsiso/AutoCollage/NativeBuffer;
    .end local v1    # "bufferKey":I
    .end local v3    # "roiInfoKey":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private createCollage()Z
    .locals 15

    .prologue
    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 2072
    sget-boolean v11, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    if-nez v11, :cond_0

    .line 2074
    sget-object v10, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "createCollage m_bAllowRequest = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v12, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " Controller id = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2149
    :goto_0
    return v3

    .line 2077
    :cond_0
    sput-boolean v3, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    .line 2080
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 2081
    .local v6, "t1":J
    iget-object v11, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget v11, v11, Lsiso/AutoCollage/SaCConfig;->m_collageWidth:I

    if-lez v11, :cond_1

    iget-object v11, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget v11, v11, Lsiso/AutoCollage/SaCConfig;->m_collageHeight:I

    if-gtz v11, :cond_2

    .line 2083
    :cond_1
    const-string v0, "Width or Height are not Properly initialised"

    .line 2084
    .local v0, "Error":Ljava/lang/String;
    sget-object v11, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2085
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 2086
    sput-boolean v10, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 2090
    .end local v0    # "Error":Ljava/lang/String;
    :cond_2
    iget-object v11, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget-boolean v11, v11, Lsiso/AutoCollage/SaCConfig;->m_bMediaDBEnabled:Z

    if-eqz v11, :cond_3

    .line 2092
    iget-object v11, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-virtual {v11}, Lsiso/AutoCollage/AndroidApiCalls;->init()Z

    move-result v11

    if-nez v11, :cond_4

    .line 2094
    sput-boolean v10, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    .line 2095
    sget-object v10, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "createCollage m_bAllowRequest 2 "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v12, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2101
    :cond_3
    iget-object v11, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-virtual {v11}, Lsiso/AutoCollage/AndroidApiCalls;->initWithoutMediaDB()Z

    move-result v11

    if-nez v11, :cond_4

    .line 2103
    sget-object v11, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "createCollage m_bAllowRequest 3 "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v13, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2104
    sput-boolean v10, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 2109
    :cond_4
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 2111
    iget-object v11, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    iget-object v12, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    .line 2112
    iget-object v13, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-direct {p0, v13}, Lsiso/AutoCollage/Collage;->ArrayListToArrayString(Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v13

    iget-object v14, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-direct {p0, v14}, Lsiso/AutoCollage/Collage;->ArrayListToArrayInt(Ljava/util/ArrayList;)[I

    move-result-object v14

    .line 2111
    invoke-static {v11, v12, v13, v3, v14}, Lsiso/AutoCollage/AutoCollageEngineLib;->CreateCollage(Lsiso/AutoCollage/AndroidApiCalls;Lsiso/AutoCollage/SaCConfig;[Ljava/lang/String;I[I)I

    move-result v1

    .line 2114
    .local v1, "controllerID":I
    if-nez v1, :cond_5

    .line 2116
    const-string v0, "Create Collage Failed"

    .line 2117
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v11, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2118
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 2119
    sput-boolean v10, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto/16 :goto_0

    .line 2123
    .end local v0    # "Error":Ljava/lang/String;
    :cond_5
    sget-object v11, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Controller id at Java : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2125
    iput v1, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    .line 2127
    iget-object v11, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v11, :cond_7

    .line 2129
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 2130
    iget-boolean v11, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v11, :cond_6

    .line 2132
    const-string v0, "Create Collage Failed"

    .line 2133
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v11, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2134
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 2135
    sput-boolean v10, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto/16 :goto_0

    .line 2138
    .end local v0    # "Error":Ljava/lang/String;
    :cond_6
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createOutputBitmap()V

    .line 2140
    :cond_7
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_8

    .line 2144
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 2145
    .local v8, "t2":J
    sub-long v4, v8, v6

    .line 2146
    .local v4, "t":J
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Time Profiling async in create"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v3, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2148
    sput-boolean v10, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    move v3, v10

    .line 2149
    goto/16 :goto_0

    .line 2142
    .end local v4    # "t":J
    .end local v8    # "t2":J
    :cond_8
    invoke-direct {p0, v2}, Lsiso/AutoCollage/Collage;->setRotationDoneFlag(I)V

    .line 2140
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private createOutputBitmap()V
    .locals 5

    .prologue
    .line 1992
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Profiling: createOutputBitmap "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    invoke-virtual {v4}, Lsiso/AutoCollage/SaCConfig;->getPixelFormat()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1994
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    if-nez v2, :cond_0

    .line 1996
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v3, "createOutputBitmap return null as there is no buffer"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2028
    :goto_0
    return-void

    .line 2000
    :cond_0
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    invoke-virtual {v2}, Lsiso/AutoCollage/Savedbuffer;->GetWidth()I

    move-result v2

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    invoke-virtual {v3}, Lsiso/AutoCollage/Savedbuffer;->GetHeight()I

    move-result v3

    mul-int v1, v2, v3

    .line 2002
    .local v1, "bytesCount":I
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    invoke-virtual {v2}, Lsiso/AutoCollage/SaCConfig;->getPixelFormat()I

    move-result v2

    if-nez v2, :cond_3

    .line 2004
    mul-int/lit8 v1, v1, 0x2

    .line 2011
    :goto_1
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_outputBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_outputBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_outputBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v2

    if-eq v2, v1, :cond_2

    .line 2013
    :cond_1
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    invoke-virtual {v2}, Lsiso/AutoCollage/SaCConfig;->getPixelFormat()I

    move-result v2

    if-nez v2, :cond_4

    .line 2015
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    invoke-virtual {v2}, Lsiso/AutoCollage/Savedbuffer;->GetWidth()I

    move-result v2

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    invoke-virtual {v3}, Lsiso/AutoCollage/Savedbuffer;->GetHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lsiso/AutoCollage/Collage;->m_outputBitmap:Landroid/graphics/Bitmap;

    .line 2026
    :cond_2
    :goto_2
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    invoke-virtual {v2}, Lsiso/AutoCollage/Savedbuffer;->GetBuffer()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 2027
    .local v0, "buf":Ljava/nio/ByteBuffer;
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_outputBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v0}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    goto :goto_0

    .line 2008
    .end local v0    # "buf":Ljava/nio/ByteBuffer;
    :cond_3
    mul-int/lit8 v1, v1, 0x4

    goto :goto_1

    .line 2019
    :cond_4
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    invoke-virtual {v2}, Lsiso/AutoCollage/Savedbuffer;->GetWidth()I

    move-result v2

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    invoke-virtual {v3}, Lsiso/AutoCollage/Savedbuffer;->GetHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lsiso/AutoCollage/Collage;->m_outputBitmap:Landroid/graphics/Bitmap;

    goto :goto_2
.end method

.method private getNativeBuffer(Ljava/lang/String;Landroid/graphics/Bitmap;)Lsiso/AutoCollage/NativeBuffer;
    .locals 15
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 2032
    new-instance v12, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v12}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2033
    .local v12, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    iput-boolean v3, v12, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 2034
    const/4 v10, 0x0

    .line 2037
    .local v10, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v11, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2044
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .local v11, "fis":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    invoke-static {v11, v3, v12}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 2047
    :try_start_1
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2053
    :goto_0
    iget v14, v12, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 2054
    .local v14, "originalWidth":I
    iget v13, v12, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 2055
    .local v13, "originalHeight":I
    if-nez p2, :cond_0

    .line 2057
    const/4 v1, 0x0

    move-object v10, v11

    .line 2067
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .end local v13    # "originalHeight":I
    .end local v14    # "originalWidth":I
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    :goto_1
    return-object v1

    .line 2039
    :catch_0
    move-exception v9

    .line 2041
    .local v9, "e":Ljava/io/FileNotFoundException;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v4, "file not found"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2042
    const/4 v1, 0x0

    goto :goto_1

    .line 2049
    .end local v9    # "e":Ljava/io/FileNotFoundException;
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v9

    .line 2051
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 2059
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v13    # "originalHeight":I
    .restart local v14    # "originalWidth":I
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int v8, v3, v4

    .line 2060
    .local v8, "bufferSize":I
    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 2061
    .local v2, "byteBuffer":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 2062
    new-instance v1, Lsiso/AutoCollage/NativeBuffer;

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    .line 2063
    int-to-float v6, v14

    div-float/2addr v5, v6

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    int-to-float v7, v13

    div-float/2addr v6, v7

    invoke-virtual {p0}, Lsiso/AutoCollage/Collage;->generateBufferKey()I

    move-result v7

    .line 2062
    invoke-direct/range {v1 .. v7}, Lsiso/AutoCollage/NativeBuffer;-><init>(Ljava/nio/ByteBuffer;IIFFI)V

    .line 2064
    .local v1, "nativeBuffer":Lsiso/AutoCollage/NativeBuffer;
    const/4 v2, 0x0

    .line 2065
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->recycle()V

    move-object v10, v11

    .line 2067
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method private initWaitFlag()V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsiso/AutoCollage/Collage;->m_WaitingOver:Z

    .line 142
    return-void
.end method

.method private loadMultiGridData(Ljava/lang/String;Ljava/lang/String;JLjava/io/RandomAccessFile;ZJ)Z
    .locals 25
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "roiFilename"    # Ljava/lang/String;
    .param p3, "seekpos"    # J
    .param p5, "fr"    # Ljava/io/RandomAccessFile;
    .param p6, "isPhotoNote"    # Z
    .param p7, "filelength"    # J

    .prologue
    .line 2341
    if-eqz p6, :cond_0

    .line 2342
    const-wide/16 v22, 0x32

    add-long v22, v22, p3

    sub-long v22, p7, v22

    :try_start_0
    move-object/from16 v0, p5

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 2348
    :goto_0
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v16

    .line 2349
    .local v16, "nROI":I
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v16

    .line 2350
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2354
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    move/from16 v0, v16

    if-lt v8, v0, :cond_1

    .line 2370
    const/4 v8, 0x0

    :goto_2
    move/from16 v0, v16

    if-lt v8, v0, :cond_3

    .line 2389
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2390
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2391
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2392
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2393
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2394
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2395
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2396
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2397
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v12

    .line 2398
    .local v12, "len":I
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2399
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_3
    if-lt v10, v12, :cond_4

    .line 2403
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2405
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2406
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2408
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2409
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2413
    const/4 v8, 0x0

    :goto_4
    move/from16 v0, v16

    if-lt v8, v0, :cond_5

    .line 2426
    const/4 v8, 0x0

    :goto_5
    move/from16 v0, v16

    if-lt v8, v0, :cond_7

    .line 2432
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2433
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2435
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2436
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2438
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2439
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2441
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2442
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2446
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2447
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2448
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2449
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2450
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2451
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2452
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2453
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2457
    const/4 v15, 0x0

    .line 2458
    .local v15, "nImages":I
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v15

    .line 2459
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2461
    const/4 v10, 0x0

    :goto_6
    if-lt v10, v15, :cond_8

    .line 2617
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->close()V

    .line 2624
    const/16 v21, 0x0

    .end local v8    # "i":I
    .end local v10    # "j":I
    .end local v12    # "len":I
    .end local v15    # "nImages":I
    .end local v16    # "nROI":I
    :goto_7
    return v21

    .line 2344
    :cond_0
    const-wide/16 v22, 0x1e

    add-long v22, v22, p3

    sub-long v22, p7, v22

    move-object/from16 v0, p5

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2619
    :catch_0
    move-exception v5

    .line 2621
    .local v5, "e":Ljava/lang/Exception;
    sget-object v21, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "Collage:"

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2622
    const/16 v21, 0x0

    goto :goto_7

    .line 2356
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v8    # "i":I
    .restart local v16    # "nROI":I
    :cond_1
    :try_start_1
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v13

    .line 2357
    .local v13, "length":I
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v13

    .line 2359
    new-array v4, v13, [B

    .line 2360
    .local v4, "c":[B
    const/4 v10, 0x0

    .restart local v10    # "j":I
    :goto_8
    if-lt v10, v13, :cond_2

    .line 2365
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2354
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 2362
    :cond_2
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v21

    aput-byte v21, v4, v10

    .line 2360
    add-int/lit8 v10, v10, 0x1

    goto :goto_8

    .line 2372
    .end local v4    # "c":[B
    .end local v10    # "j":I
    .end local v13    # "length":I
    :cond_3
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v19

    .line 2373
    .local v19, "x":I
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v19

    .line 2374
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2375
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v20

    .line 2376
    .local v20, "y":I
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v20

    .line 2377
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2378
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v18

    .line 2379
    .local v18, "width":I
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v18

    .line 2380
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2381
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v7

    .line 2382
    .local v7, "height":I
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v7

    .line 2383
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2370
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    .line 2401
    .end local v7    # "height":I
    .end local v18    # "width":I
    .end local v19    # "x":I
    .end local v20    # "y":I
    .restart local v10    # "j":I
    .restart local v12    # "len":I
    :cond_4
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2399
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_3

    .line 2415
    :cond_5
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v12

    .line 2416
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2417
    const/4 v10, 0x0

    :goto_9
    if-lt v10, v12, :cond_6

    .line 2421
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2413
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_4

    .line 2419
    :cond_6
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2417
    add-int/lit8 v10, v10, 0x1

    goto :goto_9

    .line 2428
    :cond_7
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2429
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2426
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_5

    .line 2465
    .restart local v15    # "nImages":I
    :cond_8
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v12

    .line 2466
    new-array v4, v12, [B

    .line 2467
    .restart local v4    # "c":[B
    const/4 v11, 0x0

    .local v11, "k":I
    :goto_a
    if-lt v11, v12, :cond_c

    .line 2471
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([B)V

    .line 2472
    .local v6, "filePath":Ljava/lang/String;
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2478
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v18

    .line 2479
    .restart local v18    # "width":I
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2480
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v7

    .line 2483
    .restart local v7    # "height":I
    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 2485
    const/4 v9, 0x0

    .local v9, "index":I
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lsiso/AutoCollage/ImageROIInfo;->getCount()I

    move-result v21

    move/from16 v0, v21

    if-lt v9, v0, :cond_d

    .line 2506
    .end local v9    # "index":I
    :cond_9
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2508
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readFloat()F

    .line 2509
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2513
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2514
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2515
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2516
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2517
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2518
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2522
    const/4 v14, 0x0

    .line 2523
    .local v14, "nFaces":I
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v14

    .line 2524
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2527
    if-lez v14, :cond_a

    .line 2529
    const/4 v8, 0x0

    :goto_c
    if-lt v8, v14, :cond_12

    .line 2543
    :cond_a
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    move-result v14

    .line 2544
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2545
    if-lez v14, :cond_b

    .line 2547
    const/4 v8, 0x0

    :goto_d
    if-lt v8, v14, :cond_13

    .line 2562
    :cond_b
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2563
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2564
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2565
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2566
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2567
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2568
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2569
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2573
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2574
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2575
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2576
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2577
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2578
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2579
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2580
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2581
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2582
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2583
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2584
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2585
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2586
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2590
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2591
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2592
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2593
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2594
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2595
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2596
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2597
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2598
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    .line 2599
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2600
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    .line 2601
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2602
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lsiso/AutoCollage/Collage;->swap(I)I

    .line 2603
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2605
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readBoolean()Z

    .line 2606
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2608
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readBoolean()Z

    .line 2609
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2611
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readBoolean()Z

    .line 2612
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2461
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_6

    .line 2469
    .end local v6    # "filePath":Ljava/lang/String;
    .end local v7    # "height":I
    .end local v14    # "nFaces":I
    .end local v18    # "width":I
    :cond_c
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v21

    aput-byte v21, v4, v11

    .line 2467
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_a

    .line 2487
    .restart local v6    # "filePath":Ljava/lang/String;
    .restart local v7    # "height":I
    .restart local v9    # "index":I
    .restart local v18    # "width":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Lsiso/AutoCollage/ImageROIInfo;->getImageFilePath(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_11

    .line 2491
    move-object/from16 v0, p0

    iget-object v0, v0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    const/16 v22, 0x5a

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    const/16 v22, 0x10e

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_f

    .line 2493
    :cond_e
    move/from16 v17, v18

    .line 2494
    .local v17, "temp":I
    move/from16 v18, v7

    .line 2495
    move/from16 v7, v17

    .line 2500
    .end local v17    # "temp":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lsiso/AutoCollage/NativeBuffer;

    move-object/from16 v0, v21

    iget v0, v0, Lsiso/AutoCollage/NativeBuffer;->bufWidth:I

    move/from16 v21, v0

    move/from16 v0, v18

    move/from16 v1, v21

    if-ne v0, v1, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lsiso/AutoCollage/NativeBuffer;

    move-object/from16 v0, v21

    iget v0, v0, Lsiso/AutoCollage/NativeBuffer;->bufHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-eq v7, v0, :cond_11

    .line 2501
    :cond_10
    const/16 v21, 0x1

    goto/16 :goto_7

    .line 2485
    :cond_11
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_b

    .line 2531
    .end local v9    # "index":I
    .restart local v14    # "nFaces":I
    :cond_12
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2532
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2533
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2534
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2535
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2536
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2537
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2538
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2529
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_c

    .line 2549
    :cond_13
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2550
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2551
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2552
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2553
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2554
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B

    .line 2555
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readInt()I

    .line 2556
    invoke-virtual/range {p5 .. p5}, Ljava/io/RandomAccessFile;->readByte()B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2547
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_d
.end method

.method private resetNativeBufferReadFlag()V
    .locals 3

    .prologue
    .line 2187
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 2195
    return-void

    .line 2189
    :cond_0
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsiso/AutoCollage/NativeBuffer;

    .line 2190
    .local v1, "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    if-eqz v1, :cond_1

    .line 2192
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lsiso/AutoCollage/NativeBuffer;->setBufferReadFlag(Z)V

    .line 2187
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setRotationDoneFlag(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 2199
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsiso/AutoCollage/NativeBuffer;

    .line 2200
    .local v1, "pbSavedbuffer":Lsiso/AutoCollage/NativeBuffer;
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2201
    .local v0, "angle":I
    iget-boolean v3, v1, Lsiso/AutoCollage/NativeBuffer;->m_bIsRotated:Z

    if-nez v3, :cond_1

    .line 2203
    const/4 v3, 0x1

    iput-boolean v3, v1, Lsiso/AutoCollage/NativeBuffer;->m_bIsRotated:Z

    .line 2204
    const/16 v3, 0x5a

    if-eq v0, v3, :cond_0

    const/16 v3, 0x10e

    if-ne v0, v3, :cond_1

    .line 2206
    :cond_0
    iget v2, v1, Lsiso/AutoCollage/NativeBuffer;->bufWidth:I

    .line 2207
    .local v2, "temp":I
    iget v3, v1, Lsiso/AutoCollage/NativeBuffer;->bufHeight:I

    iput v3, v1, Lsiso/AutoCollage/NativeBuffer;->bufWidth:I

    .line 2208
    iput v2, v1, Lsiso/AutoCollage/NativeBuffer;->bufHeight:I

    .line 2211
    .end local v2    # "temp":I
    :cond_1
    return-void
.end method

.method private swap(I)I
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 2216
    shr-int/lit8 v4, p1, 0x0

    and-int/lit16 v0, v4, 0xff

    .line 2217
    .local v0, "b1":I
    shr-int/lit8 v4, p1, 0x8

    and-int/lit16 v1, v4, 0xff

    .line 2218
    .local v1, "b2":I
    shr-int/lit8 v4, p1, 0x10

    and-int/lit16 v2, v4, 0xff

    .line 2219
    .local v2, "b3":I
    shr-int/lit8 v4, p1, 0x18

    and-int/lit16 v3, v4, 0xff

    .line 2221
    .local v3, "b4":I
    shl-int/lit8 v4, v0, 0x18

    shl-int/lit8 v5, v1, 0x10

    or-int/2addr v4, v5

    shl-int/lit8 v5, v2, 0x8

    or-int/2addr v4, v5

    shl-int/lit8 v5, v3, 0x0

    or-int/2addr v4, v5

    return v4
.end method

.method private waitForResponse()V
    .locals 4

    .prologue
    .line 146
    iget-object v1, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v1, :cond_0

    .line 148
    sget-object v1, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "waitForResponse start "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_WaitingOver:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lsiso/AutoCollage/Collage;->m_WaitingOver:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_1

    .line 161
    :goto_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lsiso/AutoCollage/Collage;->m_WaitingOver:Z

    .line 162
    sget-object v1, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "waitForResponse end "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_WaitingOver:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :cond_0
    return-void

    .line 153
    :cond_1
    const-wide/16 v2, 0xa

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 156
    :catch_0
    move-exception v0

    .line 158
    .local v0, "ex":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 159
    sget-object v1, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v2, "waitForResponse m_CollageBuffer exception"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public addImage(Ljava/util/ArrayList;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "addImagePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 1673
    if-nez p1, :cond_0

    .line 1675
    const-string v0, "Input Parameters are null for Add method"

    .line 1676
    .local v0, "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1677
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v4

    .line 1742
    .end local v0    # "Error":Ljava/lang/String;
    :goto_0
    return v3

    .line 1682
    :cond_0
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "addImage addImagePaths size = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1689
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_1

    .line 1691
    const-string v0, "Number of Input Images are less then 1, Minimum expectation is 1"

    .line 1692
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1693
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v4

    .line 1694
    goto :goto_0

    .line 1699
    .end local v0    # "Error":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 1700
    iget v3, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-static {v3}, Lsiso/AutoCollage/AutoCollageEngineLib;->DoDeInit(I)I

    move-result v2

    .line 1701
    .local v2, "retVal":I
    if-nez v2, :cond_2

    .line 1703
    const-string v0, "Engine Data Clearance failed"

    .line 1704
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1705
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v4

    .line 1706
    goto :goto_0

    .line 1709
    .end local v0    # "Error":Ljava/lang/String;
    :cond_2
    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v3, :cond_3

    .line 1711
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 1712
    iget-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v3, :cond_3

    .line 1714
    const-string v0, "Engine DoDeInit  failed"

    .line 1715
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1716
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v4

    .line 1717
    goto :goto_0

    .line 1721
    .end local v0    # "Error":Ljava/lang/String;
    :cond_3
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->resetNativeBufferReadFlag()V

    .line 1723
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_5

    .line 1734
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    if-eqz v3, :cond_4

    .line 1736
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    invoke-virtual {v3}, Lsiso/AutoCollage/Savedbuffer;->CleanBuffer()V

    .line 1737
    iput-object v7, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    .line 1740
    :cond_4
    iput-object v7, p0, Lsiso/AutoCollage/Collage;->m_outputBitmap:Landroid/graphics/Bitmap;

    .line 1742
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createCollage()Z

    move-result v3

    goto :goto_0

    .line 1725
    :cond_5
    iget-object v5, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1726
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget v5, v3, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v3, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    .line 1727
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1728
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1729
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1730
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1731
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1732
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1723
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public addImage(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "addImagePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p2, "addImageBitmaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/4 v5, 0x0

    .line 1592
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1594
    :cond_0
    const-string v0, "Input Parameters are null for Add method"

    .line 1595
    .local v0, "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1596
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v5

    .line 1662
    .end local v0    # "Error":Ljava/lang/String;
    :goto_0
    return v3

    .line 1599
    :cond_1
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "addImage addImagePaths size = "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "bitmaps = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1607
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_2

    .line 1609
    const-string v0, "Number of Input Images are less then 1, Minimum expectation is 1"

    .line 1610
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1611
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v5

    .line 1612
    goto :goto_0

    .line 1617
    .end local v0    # "Error":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eq v3, v4, :cond_3

    .line 1619
    const-string v0, "Number of Input Bitmaps is not equal to Number of Input Images, Expected same"

    .line 1620
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1621
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v5

    .line 1622
    goto :goto_0

    .line 1627
    .end local v0    # "Error":Ljava/lang/String;
    :cond_3
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 1628
    iget v3, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-static {v3}, Lsiso/AutoCollage/AutoCollageEngineLib;->DoDeInit(I)I

    move-result v2

    .line 1629
    .local v2, "retVal":I
    if-nez v2, :cond_4

    .line 1631
    const-string v0, "Engine Data Clearance failed"

    .line 1632
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1633
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v5

    .line 1634
    goto :goto_0

    .line 1637
    .end local v0    # "Error":Ljava/lang/String;
    :cond_4
    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v3, :cond_5

    .line 1639
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 1640
    iget-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v3, :cond_5

    .line 1642
    const-string v0, "Engine DoDeInit  failed"

    .line 1643
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1644
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v5

    .line 1645
    goto :goto_0

    .line 1648
    .end local v0    # "Error":Ljava/lang/String;
    :cond_5
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->resetNativeBufferReadFlag()V

    .line 1650
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_6

    .line 1662
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createCollage()Z

    move-result v3

    goto/16 :goto_0

    .line 1652
    :cond_6
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1653
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget v4, v3, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    .line 1654
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1655
    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-direct {p0, v3, v4}, Lsiso/AutoCollage/Collage;->getNativeBuffer(Ljava/lang/String;Landroid/graphics/Bitmap;)Lsiso/AutoCollage/NativeBuffer;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1656
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1657
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1658
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1659
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1650
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public addImage(Ljava/util/ArrayList;Ljava/util/ArrayList;[I)Z
    .locals 7
    .param p3, "addAngleList"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;[I)Z"
        }
    .end annotation

    .prologue
    .local p1, "addImagePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p2, "addImageBitmaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/4 v5, 0x0

    .line 1418
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 1420
    :cond_0
    const-string v0, "Input Parameters are null for Add method"

    .line 1421
    .local v0, "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1422
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v5

    .line 1499
    .end local v0    # "Error":Ljava/lang/String;
    :goto_0
    return v3

    .line 1425
    :cond_1
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "addImage addImagePaths size = "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "addImageBitmaps = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "addAngleList = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v6, p3

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1433
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_2

    .line 1435
    const-string v0, "Number of Input Images are less then 1, Minimum expectation is 1"

    .line 1436
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1437
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v5

    .line 1438
    goto :goto_0

    .line 1443
    .end local v0    # "Error":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eq v3, v4, :cond_3

    .line 1445
    const-string v0, "Number of Input Bitmaps is not equal to Number of Input Images, Expected same"

    .line 1446
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1447
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v5

    .line 1448
    goto :goto_0

    .line 1453
    .end local v0    # "Error":Ljava/lang/String;
    :cond_3
    array-length v3, p3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eq v3, v4, :cond_4

    .line 1455
    const-string v0, "Number of Input Angles is not equal to Number of Input Images, Expected same"

    .line 1456
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1457
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v5

    .line 1458
    goto :goto_0

    .line 1463
    .end local v0    # "Error":Ljava/lang/String;
    :cond_4
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 1464
    iget v3, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-static {v3}, Lsiso/AutoCollage/AutoCollageEngineLib;->DoDeInit(I)I

    move-result v2

    .line 1465
    .local v2, "retVal":I
    if-nez v2, :cond_5

    .line 1467
    const-string v0, "Engine Data Clearance failed"

    .line 1468
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1469
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v5

    .line 1470
    goto/16 :goto_0

    .line 1473
    .end local v0    # "Error":Ljava/lang/String;
    :cond_5
    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v3, :cond_6

    .line 1475
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 1476
    iget-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v3, :cond_6

    .line 1478
    const-string v0, "Engine DoDeInit  failed"

    .line 1479
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1480
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v5

    .line 1481
    goto/16 :goto_0

    .line 1485
    .end local v0    # "Error":Ljava/lang/String;
    :cond_6
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->resetNativeBufferReadFlag()V

    .line 1487
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_7

    .line 1499
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createCollage()Z

    move-result v3

    goto/16 :goto_0

    .line 1489
    :cond_7
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1490
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget v4, v3, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    .line 1491
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    aget v4, p3, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1492
    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-direct {p0, v3, v4}, Lsiso/AutoCollage/Collage;->getNativeBuffer(Ljava/lang/String;Landroid/graphics/Bitmap;)Lsiso/AutoCollage/NativeBuffer;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1493
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1494
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1495
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1496
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1487
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public addImage(Ljava/util/ArrayList;[I)Z
    .locals 8
    .param p2, "addAngleList"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;[I)Z"
        }
    .end annotation

    .prologue
    .local p1, "addImagePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 1510
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1512
    :cond_0
    const-string v0, "Input Parameters are null for Add method"

    .line 1513
    .local v0, "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1514
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v4

    .line 1581
    .end local v0    # "Error":Ljava/lang/String;
    :goto_0
    return v3

    .line 1517
    :cond_1
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "addImage addImagePaths size = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "addAngleList = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, p2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1525
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_2

    .line 1527
    const-string v0, "Number of Input Images are less then 1, Minimum expectation is 1"

    .line 1528
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1529
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v4

    .line 1530
    goto :goto_0

    .line 1535
    .end local v0    # "Error":Ljava/lang/String;
    :cond_2
    array-length v3, p2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eq v3, v5, :cond_3

    .line 1537
    const-string v0, "Number of Input Angles is not equal to Number of Input Images, Expected same"

    .line 1538
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1539
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v4

    .line 1540
    goto :goto_0

    .line 1545
    .end local v0    # "Error":Ljava/lang/String;
    :cond_3
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 1546
    iget v3, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-static {v3}, Lsiso/AutoCollage/AutoCollageEngineLib;->DoDeInit(I)I

    move-result v2

    .line 1547
    .local v2, "retVal":I
    if-nez v2, :cond_4

    .line 1549
    const-string v0, "Engine Data Clearance failed"

    .line 1550
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1551
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v4

    .line 1552
    goto :goto_0

    .line 1555
    .end local v0    # "Error":Ljava/lang/String;
    :cond_4
    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v3, :cond_5

    .line 1557
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 1558
    iget-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v3, :cond_5

    .line 1560
    const-string v0, "Engine DoDeInit  failed"

    .line 1561
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1562
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v3, v4

    .line 1563
    goto :goto_0

    .line 1567
    .end local v0    # "Error":Ljava/lang/String;
    :cond_5
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->resetNativeBufferReadFlag()V

    .line 1569
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_6

    .line 1581
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createCollage()Z

    move-result v3

    goto/16 :goto_0

    .line 1571
    :cond_6
    iget-object v5, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1572
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget v5, v3, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v3, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    .line 1573
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    aget v5, p2, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1574
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1575
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1576
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1577
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1578
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1569
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public clear()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1860
    sget-boolean v3, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    if-nez v3, :cond_0

    .line 1862
    :goto_0
    sget-boolean v3, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    if-eqz v3, :cond_c

    .line 1874
    :cond_0
    sput-boolean v6, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    .line 1876
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "clear called with m_nSacControllerID = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1878
    iget v3, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    if-eqz v3, :cond_2

    .line 1880
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 1881
    iget v3, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-static {v3}, Lsiso/AutoCollage/AutoCollageEngineLib;->DoDeInit(I)I

    move-result v2

    .line 1882
    .local v2, "retVal":I
    if-nez v2, :cond_d

    .line 1884
    const-string v0, "Engine Data Clearance failed"

    .line 1885
    .local v0, "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1886
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1898
    .end local v0    # "Error":Ljava/lang/String;
    :cond_1
    :goto_1
    iput v6, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    .line 1901
    .end local v2    # "retVal":I
    :cond_2
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    if-eqz v3, :cond_3

    .line 1903
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    invoke-virtual {v3}, Lsiso/AutoCollage/Savedbuffer;->CleanBuffer()V

    .line 1904
    iput-object v7, p0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    .line 1907
    :cond_3
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    if-eqz v3, :cond_4

    .line 1908
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-virtual {v3}, Lsiso/AutoCollage/AndroidApiCalls;->closeDb()V

    .line 1911
    :cond_4
    iput-object v7, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    .line 1912
    iput v6, p0, Lsiso/AutoCollage/Collage;->m_iBufferKey:I

    .line 1916
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    if-eqz v3, :cond_5

    .line 1918
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1920
    :cond_5
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    if-eqz v3, :cond_6

    .line 1922
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1924
    :cond_6
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    if-eqz v3, :cond_7

    .line 1926
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1928
    :cond_7
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    if-eqz v3, :cond_8

    .line 1930
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1932
    :cond_8
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    if-eqz v3, :cond_9

    .line 1934
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1936
    :cond_9
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    if-eqz v3, :cond_a

    .line 1938
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1940
    :cond_a
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    if-eqz v3, :cond_b

    .line 1942
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1945
    :cond_b
    iput-object v7, p0, Lsiso/AutoCollage/Collage;->m_outputBitmap:Landroid/graphics/Bitmap;

    .line 1947
    sput-boolean v8, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    .line 1951
    return-void

    .line 1866
    :cond_c
    const-wide/16 v4, 0xa

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1868
    :catch_0
    move-exception v1

    .line 1870
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_0

    .line 1888
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v2    # "retVal":I
    :cond_d
    if-ne v2, v8, :cond_1

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v3, :cond_1

    .line 1890
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 1891
    iget-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v3, :cond_1

    .line 1893
    const-string v0, "Engine DoDeInit  failed"

    .line 1894
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1895
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public create(Ljava/util/ArrayList;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "inputImages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 320
    if-nez p1, :cond_0

    .line 322
    const-string v0, "Input Parameters are null in create method"

    .line 323
    .local v0, "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v2, v3

    .line 376
    .end local v0    # "Error":Ljava/lang/String;
    :goto_0
    return v2

    .line 328
    :cond_0
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "create called inputImages size = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v4, 0x1

    if-le v2, v4, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v4, 0x6

    if-le v2, v4, :cond_2

    .line 339
    :cond_1
    const-string v0, "Number of Input Images are less then 2, Minimum expectation is 2"

    .line 340
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v2, v3

    .line 342
    goto :goto_0

    .line 348
    .end local v0    # "Error":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_4

    .line 360
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    iput v4, v2, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    .line 364
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 366
    const/4 v1, 0x0

    :goto_2
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_5

    .line 372
    :cond_3
    new-instance v2, Lsiso/AutoCollage/AndroidApiCalls;

    invoke-direct {v2, p0}, Lsiso/AutoCollage/AndroidApiCalls;-><init>(Lsiso/AutoCollage/Collage;)V

    iput-object v2, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    .line 376
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createCollage()Z

    move-result v2

    goto :goto_0

    .line 350
    :cond_4
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 368
    :cond_5
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public create(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "inputImages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p2, "inputBitmaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/4 v4, 0x0

    .line 385
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 387
    :cond_0
    const-string v0, "Input Parameters are null in create method"

    .line 388
    .local v0, "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v2, v4

    .line 448
    .end local v0    # "Error":Ljava/lang/String;
    :goto_0
    return v2

    .line 392
    :cond_1
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "create called inputImages size = "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "inputBitmaps = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x6

    if-le v2, v3, :cond_3

    .line 403
    :cond_2
    const-string v0, "Number of Input Images are less then 2, Minimum expectation is 2"

    .line 404
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v2, v4

    .line 406
    goto :goto_0

    .line 411
    .end local v0    # "Error":Ljava/lang/String;
    :cond_3
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eq v2, v3, :cond_4

    .line 413
    const-string v0, "Number of Input Bitmaps is not equal to Number of Input Images, Expected same"

    .line 414
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v2, v4

    .line 416
    goto :goto_0

    .line 422
    .end local v0    # "Error":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_6

    .line 434
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    iput v3, v2, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    .line 438
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_5

    .line 440
    const/4 v1, 0x0

    :goto_2
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_7

    .line 446
    :cond_5
    new-instance v2, Lsiso/AutoCollage/AndroidApiCalls;

    invoke-direct {v2, p0}, Lsiso/AutoCollage/AndroidApiCalls;-><init>(Lsiso/AutoCollage/Collage;)V

    iput-object v2, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    .line 448
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createCollage()Z

    move-result v2

    goto/16 :goto_0

    .line 424
    :cond_6
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 425
    iget-object v5, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-direct {p0, v2, v3}, Lsiso/AutoCollage/Collage;->getNativeBuffer(Ljava/lang/String;Landroid/graphics/Bitmap;)Lsiso/AutoCollage/NativeBuffer;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 426
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 427
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 428
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 422
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 442
    :cond_7
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public create(Ljava/util/ArrayList;Ljava/util/ArrayList;[I)Z
    .locals 6
    .param p3, "angleList"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;[I)Z"
        }
    .end annotation

    .prologue
    .local p1, "inputImages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p2, "inputBitmaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/4 v4, 0x0

    .line 525
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 527
    :cond_0
    const-string v0, "Input Parameters are null in create method"

    .line 528
    .local v0, "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v2, v4

    .line 598
    .end local v0    # "Error":Ljava/lang/String;
    :goto_0
    return v2

    .line 532
    :cond_1
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "create called inputImages size = "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "inputBitmaps ="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "angleList length = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v5, p3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x6

    if-le v2, v3, :cond_3

    .line 543
    :cond_2
    const-string v0, "Number of Input Images are less then 2, Minimum expectation is 2"

    .line 544
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v2, v4

    .line 546
    goto :goto_0

    .line 551
    .end local v0    # "Error":Ljava/lang/String;
    :cond_3
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eq v2, v3, :cond_4

    .line 553
    const-string v0, "Number of Input Bitmaps is not equal to Number of Input Images, Expected same"

    .line 554
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v2, v4

    .line 556
    goto :goto_0

    .line 561
    .end local v0    # "Error":Ljava/lang/String;
    :cond_4
    array-length v2, p3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eq v2, v3, :cond_5

    .line 563
    const-string v0, "Number of Input Angles is not equal to Number of Input Images, Expected same"

    .line 564
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v2, v4

    .line 566
    goto :goto_0

    .line 572
    .end local v0    # "Error":Ljava/lang/String;
    :cond_5
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_7

    .line 584
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    iput v3, v2, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    .line 588
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_6

    .line 590
    const/4 v1, 0x0

    :goto_2
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_8

    .line 596
    :cond_6
    new-instance v2, Lsiso/AutoCollage/AndroidApiCalls;

    invoke-direct {v2, p0}, Lsiso/AutoCollage/AndroidApiCalls;-><init>(Lsiso/AutoCollage/Collage;)V

    iput-object v2, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    .line 598
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createCollage()Z

    move-result v2

    goto/16 :goto_0

    .line 574
    :cond_7
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 575
    iget-object v5, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-direct {p0, v2, v3}, Lsiso/AutoCollage/Collage;->getNativeBuffer(Ljava/lang/String;Landroid/graphics/Bitmap;)Lsiso/AutoCollage/NativeBuffer;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 576
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 577
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 578
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 579
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 572
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 592
    :cond_8
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    aget v3, p3, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 590
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public create(Ljava/util/ArrayList;[I)Z
    .locals 7
    .param p2, "angleList"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;[I)Z"
        }
    .end annotation

    .prologue
    .local p1, "inputImages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 456
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 458
    :cond_0
    const-string v0, "Input Parameters are null in create method"

    .line 459
    .local v0, "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v2, v3

    .line 518
    .end local v0    # "Error":Ljava/lang/String;
    :goto_0
    return v2

    .line 463
    :cond_1
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "create called inputImages size = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "angleList length = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, p2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v4, 0x1

    if-le v2, v4, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v4, 0x6

    if-le v2, v4, :cond_3

    .line 474
    :cond_2
    const-string v0, "Number of Input Images are less then 2, Minimum expectation is 2"

    .line 475
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v2, v3

    .line 477
    goto :goto_0

    .line 482
    .end local v0    # "Error":Ljava/lang/String;
    :cond_3
    array-length v2, p2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eq v2, v4, :cond_4

    .line 484
    const-string v0, "Number of Input Angles is not equal to Number of Input Images, Expected same"

    .line 485
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v2, v3

    .line 487
    goto :goto_0

    .line 493
    .end local v0    # "Error":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_6

    .line 505
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    iput v3, v2, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    .line 509
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_5

    .line 511
    const/4 v1, 0x0

    :goto_2
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_7

    .line 517
    :cond_5
    new-instance v2, Lsiso/AutoCollage/AndroidApiCalls;

    invoke-direct {v2, p0}, Lsiso/AutoCollage/AndroidApiCalls;-><init>(Lsiso/AutoCollage/Collage;)V

    iput-object v2, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    .line 518
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createCollage()Z

    move-result v2

    goto :goto_0

    .line 495
    :cond_6
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 496
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 497
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 498
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 499
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 500
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 493
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 513
    :cond_7
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    aget v3, p2, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 511
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public deleteImage(Ljava/util/ArrayList;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "deleteIndexes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1752
    if-nez p1, :cond_0

    .line 1754
    const-string v0, "Input Parameters are null for Add method"

    .line 1755
    .local v0, "Error":Ljava/lang/String;
    sget-object v6, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1756
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v6, v7

    .line 1852
    .end local v0    # "Error":Ljava/lang/String;
    :goto_0
    return v6

    .line 1759
    :cond_0
    sget-object v6, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "deleteImage deleteIndexes size = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1768
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    iget-object v9, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ge v6, v9, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-gtz v6, :cond_2

    .line 1770
    :cond_1
    const-string v0, "Number of Images to be deleted are 0, Expected more then 0"

    .line 1771
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v6, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1772
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v6, v7

    .line 1773
    goto :goto_0

    .line 1778
    .end local v0    # "Error":Ljava/lang/String;
    :cond_2
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 1779
    iget v6, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-static {v6}, Lsiso/AutoCollage/AutoCollageEngineLib;->DoDeInit(I)I

    move-result v5

    .line 1780
    .local v5, "retVal":I
    if-nez v5, :cond_3

    .line 1782
    const-string v0, "Engine Data Clearance failed"

    .line 1783
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v6, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1784
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v6, v7

    .line 1785
    goto :goto_0

    .line 1788
    .end local v0    # "Error":Ljava/lang/String;
    :cond_3
    if-ne v5, v8, :cond_4

    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v6, :cond_4

    .line 1790
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 1791
    iget-boolean v6, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v6, :cond_4

    .line 1793
    const-string v0, "Engine DoDeInit  failed"

    .line 1794
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v6, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1795
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v6, v7

    .line 1796
    goto :goto_0

    .line 1800
    .end local v0    # "Error":Ljava/lang/String;
    :cond_4
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->resetNativeBufferReadFlag()V

    .line 1802
    const/4 v3, 0x0

    .line 1803
    .local v3, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v4, v6, :cond_5

    .line 1829
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 1830
    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    iget-object v9, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-direct {p0, v10}, Lsiso/AutoCollage/Collage;->ArrayListToArrayString(Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v10

    .line 1831
    iget-object v11, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-direct {p0, v11}, Lsiso/AutoCollage/Collage;->ArrayListToArrayInt(Ljava/util/ArrayList;)[I

    move-result-object v11

    .line 1830
    invoke-static {v6, v9, v10, v7, v11}, Lsiso/AutoCollage/AutoCollageEngineLib;->CreateCollage(Lsiso/AutoCollage/AndroidApiCalls;Lsiso/AutoCollage/SaCConfig;[Ljava/lang/String;I[I)I

    move-result v2

    .line 1832
    .local v2, "controllerID":I
    if-nez v2, :cond_8

    .line 1834
    const-string v0, "Create Collage Failed"

    .line 1835
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v6, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1836
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v6, v7

    .line 1837
    goto/16 :goto_0

    .line 1806
    .end local v0    # "Error":Ljava/lang/String;
    .end local v2    # "controllerID":I
    :cond_5
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {p0, v6}, Lsiso/AutoCollage/Collage;->convertIndex(I)I

    move-result v6

    sub-int v1, v6, v3

    .line 1810
    .local v1, "ROIIndex":I
    if-ltz v1, :cond_6

    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v1, v6, :cond_7

    .line 1812
    :cond_6
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Invalid Indexe passed in deleteImage function : index - "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1813
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v6, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1814
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v6, v7

    .line 1815
    goto/16 :goto_0

    .line 1818
    .end local v0    # "Error":Ljava/lang/String;
    :cond_7
    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1819
    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1820
    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1821
    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1822
    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1823
    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1824
    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget v9, v6, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    add-int/lit8 v9, v9, -0x1

    iput v9, v6, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    .line 1825
    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1826
    add-int/lit8 v3, v3, -0x1

    .line 1803
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 1839
    .end local v1    # "ROIIndex":I
    .restart local v2    # "controllerID":I
    :cond_8
    iput v2, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    .line 1840
    iget-object v6, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v6, :cond_a

    .line 1842
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 1843
    iget-boolean v6, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v6, :cond_9

    .line 1845
    const-string v0, "Engine Delete Image failed"

    .line 1846
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v6, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1847
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    move v6, v7

    .line 1848
    goto/16 :goto_0

    .line 1850
    .end local v0    # "Error":Ljava/lang/String;
    :cond_9
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createOutputBitmap()V

    :cond_a
    move v6, v8

    .line 1852
    goto/16 :goto_0
.end method

.method generateBufferKey()I
    .locals 3

    .prologue
    .line 2180
    iget v0, p0, Lsiso/AutoCollage/Collage;->m_iBufferKey:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsiso/AutoCollage/Collage;->m_iBufferKey:I

    .line 2181
    sget-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "generateBufferKey return "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lsiso/AutoCollage/Collage;->m_iBufferKey:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2182
    iget v0, p0, Lsiso/AutoCollage/Collage;->m_iBufferKey:I

    return v0
.end method

.method public getAllImageROI()Lsiso/AutoCollage/ImageROIInfo;
    .locals 1

    .prologue
    .line 985
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    return-object v0
.end method

.method public getCollageHeight()I
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget v0, v0, Lsiso/AutoCollage/SaCConfig;->m_collageHeight:I

    return v0
.end method

.method public getCollagePixelFormat()I
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    invoke-virtual {v0}, Lsiso/AutoCollage/SaCConfig;->getPixelFormat()I

    move-result v0

    return v0
.end method

.method public getCollageWidth()I
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget v0, v0, Lsiso/AutoCollage/SaCConfig;->m_collageWidth:I

    return v0
.end method

.method public getImageIndexForPoint(FF)I
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 992
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    if-eqz v2, :cond_0

    .line 994
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v2}, Lsiso/AutoCollage/ImageROIInfo;->getCount()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 1008
    .end local v1    # "i":I
    :cond_0
    const-string v0, "Specified X Y cordinates are out of range"

    .line 1009
    .local v0, "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1010
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1011
    const/4 v1, -0x1

    .end local v0    # "Error":Ljava/lang/String;
    :cond_1
    return v1

    .line 996
    .restart local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v2, v1}, Lsiso/AutoCollage/ImageROIInfo;->getxROI(I)I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, p1, v2

    if-gez v2, :cond_4

    .line 994
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 998
    :cond_4
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v2, v1}, Lsiso/AutoCollage/ImageROIInfo;->getWidth(I)I

    move-result v2

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v3, v1}, Lsiso/AutoCollage/ImageROIInfo;->getxROI(I)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v2, p1, v2

    if-gtz v2, :cond_3

    .line 1000
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v2, v1}, Lsiso/AutoCollage/ImageROIInfo;->getyROI(I)I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, p2, v2

    if-ltz v2, :cond_3

    .line 1002
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v2, v1}, Lsiso/AutoCollage/ImageROIInfo;->getHeight(I)I

    move-result v2

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v3, v1}, Lsiso/AutoCollage/ImageROIInfo;->getyROI(I)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v2, p2, v2

    if-lez v2, :cond_1

    goto :goto_1
.end method

.method public getImagePathForPoint(FF)Ljava/lang/String;
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 1018
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    if-eqz v2, :cond_0

    .line 1020
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v2}, Lsiso/AutoCollage/ImageROIInfo;->getCount()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 1035
    .end local v1    # "i":I
    :cond_0
    const-string v0, "Specified X Y cordinates are out of range"

    .line 1036
    .local v0, "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1037
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1038
    const/4 v2, 0x0

    .end local v0    # "Error":Ljava/lang/String;
    :goto_1
    return-object v2

    .line 1022
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v2, v1}, Lsiso/AutoCollage/ImageROIInfo;->getxROI(I)I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, p1, v2

    if-gez v2, :cond_3

    .line 1020
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1024
    :cond_3
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v2, v1}, Lsiso/AutoCollage/ImageROIInfo;->getWidth(I)I

    move-result v2

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v3, v1}, Lsiso/AutoCollage/ImageROIInfo;->getxROI(I)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v2, p1, v2

    if-gtz v2, :cond_2

    .line 1026
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v2, v1}, Lsiso/AutoCollage/ImageROIInfo;->getyROI(I)I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, p2, v2

    if-ltz v2, :cond_2

    .line 1028
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v2, v1}, Lsiso/AutoCollage/ImageROIInfo;->getHeight(I)I

    move-result v2

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v3, v1}, Lsiso/AutoCollage/ImageROIInfo;->getyROI(I)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v2, p2, v2

    if-gtz v2, :cond_2

    .line 1031
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v2, v1}, Lsiso/AutoCollage/ImageROIInfo;->getImageFilePath(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public getLastError()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1957
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_eLastError:Ljava/lang/String;

    return-object v0
.end method

.method public getOutputBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_outputBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getSpecificROI(FF)Landroid/graphics/Rect;
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 1072
    invoke-virtual {p0, p1, p2}, Lsiso/AutoCollage/Collage;->getImagePathForPoint(FF)Ljava/lang/String;

    move-result-object v1

    .line 1074
    .local v1, "filePath":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1076
    invoke-virtual {p0, v1}, Lsiso/AutoCollage/Collage;->getSpecificROI(Ljava/lang/String;)Landroid/graphics/Rect;

    move-result-object v2

    .line 1081
    :goto_0
    return-object v2

    .line 1078
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid Cordinates in getSpecificROi function : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1080
    .local v0, "Error":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1081
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getSpecificROI(Ljava/lang/String;)Landroid/graphics/Rect;
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1045
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 1047
    .local v2, "rect":Landroid/graphics/Rect;
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    if-eqz v3, :cond_0

    .line 1049
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v3}, Lsiso/AutoCollage/ImageROIInfo;->getCount()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 1062
    .end local v1    # "i":I
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid Filepath in getSpecificROi function : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1064
    .local v0, "Error":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1065
    const/4 v2, 0x0

    .end local v0    # "Error":Ljava/lang/String;
    .end local v2    # "rect":Landroid/graphics/Rect;
    :goto_1
    return-object v2

    .line 1051
    .restart local v1    # "i":I
    .restart local v2    # "rect":Landroid/graphics/Rect;
    :cond_1
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v3, v1}, Lsiso/AutoCollage/ImageROIInfo;->getImageFilePath(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1053
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v3, v1}, Lsiso/AutoCollage/ImageROIInfo;->getxROI(I)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 1054
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v3, v1}, Lsiso/AutoCollage/ImageROIInfo;->getyROI(I)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 1055
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v3, v1}, Lsiso/AutoCollage/ImageROIInfo;->getWidth(I)I

    move-result v3

    iget v4, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 1056
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v3, v1}, Lsiso/AutoCollage/ImageROIInfo;->getHeight(I)I

    move-result v3

    iget v4, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 1049
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public loadCollage(Ljava/lang/String;)Z
    .locals 14
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 643
    sget-boolean v10, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    if-nez v10, :cond_0

    .line 645
    sget-object v9, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "createCollage m_bAllowRequest = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v11, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " Controller id = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    :goto_0
    return v8

    .line 648
    :cond_0
    sput-boolean v8, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    .line 651
    if-nez p1, :cond_1

    .line 653
    const-string v0, "FilePath is null for Loading collage"

    .line 654
    .local v0, "Error":Ljava/lang/String;
    sget-object v10, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v10, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 656
    sput-boolean v9, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 663
    .end local v0    # "Error":Ljava/lang/String;
    :cond_1
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 667
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-nez v1, :cond_2

    .line 672
    sput-boolean v9, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 676
    :cond_2
    iput-object v1, p0, Lsiso/AutoCollage/Collage;->m_outputBitmap:Landroid/graphics/Bitmap;

    .line 677
    new-instance v10, Lsiso/AutoCollage/AndroidApiCalls;

    invoke-direct {v10, p0}, Lsiso/AutoCollage/AndroidApiCalls;-><init>(Lsiso/AutoCollage/Collage;)V

    iput-object v10, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    .line 680
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 681
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-static {v10, p1}, Lsiso/AutoCollage/AutoCollageEngineLib;->LoadCollage(Lsiso/AutoCollage/AndroidApiCalls;Ljava/lang/String;)I

    move-result v2

    .line 682
    .local v2, "controllerID":I
    if-nez v2, :cond_3

    .line 684
    sget-object v10, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v11, "LoadCollage failed"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    sput-boolean v9, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 688
    :cond_3
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v10, :cond_4

    .line 690
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 691
    iget-boolean v10, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v10, :cond_4

    .line 693
    const-string v0, "loadCollage Failed"

    .line 694
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v10, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v10, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 696
    sput-boolean v9, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 703
    .end local v0    # "Error":Ljava/lang/String;
    :cond_4
    iput v2, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    .line 707
    sget-object v10, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "LoadCollage count = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v12, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v12}, Lsiso/AutoCollage/ImageROIInfo;->getCount()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v10}, Lsiso/AutoCollage/ImageROIInfo;->getCount()I

    move-result v10

    if-lt v4, v10, :cond_6

    .line 725
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget-object v11, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    iput v11, v10, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    .line 729
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_5

    .line 731
    const/4 v4, 0x0

    :goto_2
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v4, v10, :cond_7

    .line 737
    :cond_5
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    iget v10, v10, Lsiso/AutoCollage/ImageROIInfo;->pixelFormat:I

    if-nez v10, :cond_8

    .line 739
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    sget-object v12, Lsiso/AutoCollage/Collage$PixelFormat;->RGB_565:Lsiso/AutoCollage/Collage$PixelFormat;

    invoke-virtual {p0, v10, v11, v12}, Lsiso/AutoCollage/Collage;->setConfig(IILsiso/AutoCollage/Collage$PixelFormat;)V

    .line 747
    :goto_3
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget-boolean v10, v10, Lsiso/AutoCollage/SaCConfig;->m_bMediaDBEnabled:Z

    if-eqz v10, :cond_9

    .line 749
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-virtual {v10}, Lsiso/AutoCollage/AndroidApiCalls;->init()Z

    move-result v10

    if-nez v10, :cond_a

    .line 751
    sput-boolean v9, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto/16 :goto_0

    .line 713
    :cond_6
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v10, v4}, Lsiso/AutoCollage/ImageROIInfo;->getImageFilePath(I)Ljava/lang/String;

    move-result-object v3

    .line 715
    .local v3, "fileName":Ljava/lang/String;
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 716
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v10, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 717
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 718
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v10, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 719
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 720
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 711
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 733
    .end local v3    # "fileName":Ljava/lang/String;
    :cond_7
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 731
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 743
    :cond_8
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    sget-object v12, Lsiso/AutoCollage/Collage$PixelFormat;->ARGB_8888:Lsiso/AutoCollage/Collage$PixelFormat;

    invoke-virtual {p0, v10, v11, v12}, Lsiso/AutoCollage/Collage;->setConfig(IILsiso/AutoCollage/Collage$PixelFormat;)V

    goto :goto_3

    .line 757
    :cond_9
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-virtual {v10}, Lsiso/AutoCollage/AndroidApiCalls;->initWithoutMediaDB()Z

    move-result v10

    if-nez v10, :cond_a

    .line 759
    sput-boolean v9, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto/16 :goto_0

    .line 765
    :cond_a
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 766
    iget v10, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    iget-object v11, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-static {v10, v11}, Lsiso/AutoCollage/AutoCollageEngineLib;->DecodeImages(ILsiso/AutoCollage/AndroidApiCalls;)I

    move-result v10

    if-nez v10, :cond_b

    .line 768
    sput-boolean v9, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto/16 :goto_0

    .line 771
    :cond_b
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v10, :cond_c

    .line 773
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 774
    iget-boolean v10, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v10, :cond_c

    .line 776
    const-string v0, "loadCollage Failed"

    .line 777
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v10, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v10, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 779
    sput-boolean v9, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto/16 :goto_0

    .line 785
    .end local v0    # "Error":Ljava/lang/String;
    :cond_c
    sput-boolean v9, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    .line 789
    const/4 v5, 0x0

    .local v5, "index":I
    :goto_4
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v10}, Lsiso/AutoCollage/ImageROIInfo;->getCount()I

    move-result v10

    if-lt v5, v10, :cond_d

    move v8, v9

    .line 813
    goto/16 :goto_0

    .line 794
    :cond_d
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v10, v5}, Lsiso/AutoCollage/ImageROIInfo;->getImageFilePath(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, p1, v10}, Lsiso/AutoCollage/Collage;->checkIfRotated(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 796
    .local v6, "isRotated":Z
    if-nez v6, :cond_f

    .line 789
    :cond_e
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 801
    :cond_f
    iget-object v10, p0, Lsiso/AutoCollage/Collage;->m_ImageROIInfo:Lsiso/AutoCollage/ImageROIInfo;

    invoke-virtual {v10, v5}, Lsiso/AutoCollage/ImageROIInfo;->getImageFilePath(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10, v5}, Lsiso/AutoCollage/Collage;->replaceROI(Ljava/lang/String;I)Z

    move-result v7

    .line 804
    .local v7, "replaced":Z
    if-nez v7, :cond_e

    .line 806
    sget-object v9, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v10, "ROI Rotation changes --> Falied in LoadCollage"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public loadCollageROI(Ljava/lang/String;)Z
    .locals 6
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 604
    if-nez p1, :cond_0

    .line 606
    const-string v0, "FilePath is null for Loading collage"

    .line 607
    .local v0, "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 638
    .end local v0    # "Error":Ljava/lang/String;
    :goto_0
    return v2

    .line 615
    :cond_0
    new-instance v3, Lsiso/AutoCollage/AndroidApiCalls;

    invoke-direct {v3, p0}, Lsiso/AutoCollage/AndroidApiCalls;-><init>(Lsiso/AutoCollage/Collage;)V

    iput-object v3, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    .line 617
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 619
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-static {v3, p1}, Lsiso/AutoCollage/AutoCollageEngineLib;->LoadCollageROI(Lsiso/AutoCollage/AndroidApiCalls;Ljava/lang/String;)I

    move-result v1

    .line 620
    .local v1, "controllerID":I
    if-nez v1, :cond_1

    .line 622
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v4, "LoadCollage failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 625
    :cond_1
    iput v1, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    .line 626
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SacController ID : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v3, :cond_2

    .line 629
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 630
    iget-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v3, :cond_2

    .line 632
    const-string v0, "loadCollageROI failed"

    .line 633
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    goto :goto_0

    .line 638
    .end local v0    # "Error":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public replaceROI(Ljava/lang/String;I)Z
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1131
    sget-object v4, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "replaceROI called index = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1133
    if-ltz p2, :cond_0

    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt p2, v4, :cond_2

    .line 1135
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid Indexe passed in replaceROI function : index - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1136
    .local v0, "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1137
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1200
    .end local v0    # "Error":Ljava/lang/String;
    :cond_1
    :goto_0
    return v2

    .line 1147
    :cond_2
    invoke-direct {p0, p2}, Lsiso/AutoCollage/Collage;->convertIndex(I)I

    move-result v1

    .line 1151
    .local v1, "ROIIndex":I
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1152
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-virtual {v4, v1, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1153
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    invoke-virtual {v4, v1, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1154
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1155
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1156
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1157
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1161
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget-boolean v4, v4, Lsiso/AutoCollage/SaCConfig;->m_bMediaDBEnabled:Z

    if-eqz v4, :cond_4

    .line 1163
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-virtual {v4}, Lsiso/AutoCollage/AndroidApiCalls;->init()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1177
    :cond_3
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 1178
    iget v4, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    iget-object v5, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-static {v4, v5, p2, p1}, Lsiso/AutoCollage/AutoCollageEngineLib;->ReplaceROI(ILsiso/AutoCollage/AndroidApiCalls;ILjava/lang/String;)I

    move-result v4

    if-ne v4, v3, :cond_7

    .line 1180
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v4, :cond_6

    .line 1182
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 1183
    iget-boolean v4, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v4, :cond_5

    .line 1185
    const-string v0, "Engine Replace Image failed"

    .line 1186
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1187
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    goto :goto_0

    .line 1170
    .end local v0    # "Error":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-virtual {v4}, Lsiso/AutoCollage/AndroidApiCalls;->initWithoutMediaDB()Z

    move-result v4

    if-nez v4, :cond_3

    goto :goto_0

    .line 1190
    :cond_5
    invoke-direct {p0, v1}, Lsiso/AutoCollage/Collage;->setRotationDoneFlag(I)V

    .line 1191
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createOutputBitmap()V

    :cond_6
    move v2, v3

    .line 1193
    goto :goto_0

    .line 1197
    :cond_7
    const-string v0, "Engine Replace Image failed"

    .line 1198
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1199
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public replaceROI(Ljava/lang/String;Landroid/graphics/Bitmap;I)Z
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "index"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1208
    sget-object v4, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "replaceROI called  + Bitmap at index = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1212
    if-ltz p3, :cond_0

    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt p3, v4, :cond_2

    .line 1214
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid Indexe passed in replaceROI function : index - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1215
    .local v0, "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1216
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1279
    .end local v0    # "Error":Ljava/lang/String;
    :cond_1
    :goto_0
    return v2

    .line 1226
    :cond_2
    invoke-direct {p0, p3}, Lsiso/AutoCollage/Collage;->convertIndex(I)I

    move-result v1

    .line 1230
    .local v1, "ROIIndex":I
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1231
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lsiso/AutoCollage/Collage;->getNativeBuffer(Ljava/lang/String;Landroid/graphics/Bitmap;)Lsiso/AutoCollage/NativeBuffer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1232
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szFaceRectInfo:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1233
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szImageID:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1234
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szOrientInfo:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1235
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szIsAlreadySent:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1236
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_szAngleList:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1240
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iget-boolean v4, v4, Lsiso/AutoCollage/SaCConfig;->m_bMediaDBEnabled:Z

    if-eqz v4, :cond_4

    .line 1242
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-virtual {v4}, Lsiso/AutoCollage/AndroidApiCalls;->init()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1256
    :cond_3
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 1257
    iget v4, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    iget-object v5, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-static {v4, v5, p3, p1}, Lsiso/AutoCollage/AutoCollageEngineLib;->ReplaceROI(ILsiso/AutoCollage/AndroidApiCalls;ILjava/lang/String;)I

    move-result v4

    if-ne v4, v3, :cond_7

    .line 1259
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v4, :cond_6

    .line 1261
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 1262
    iget-boolean v4, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v4, :cond_5

    .line 1264
    const-string v0, "Engine Replace Image failed"

    .line 1265
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1266
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    goto :goto_0

    .line 1249
    .end local v0    # "Error":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-virtual {v4}, Lsiso/AutoCollage/AndroidApiCalls;->initWithoutMediaDB()Z

    move-result v4

    if-nez v4, :cond_3

    goto :goto_0

    .line 1269
    :cond_5
    invoke-direct {p0, v1}, Lsiso/AutoCollage/Collage;->setRotationDoneFlag(I)V

    .line 1270
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createOutputBitmap()V

    :cond_6
    move v2, v3

    .line 1272
    goto :goto_0

    .line 1276
    :cond_7
    const-string v0, "Engine Replace Image failed"

    .line 1277
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1278
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public resizeROI(II)Z
    .locals 6
    .param p1, "index"    # I
    .param p2, "value"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1354
    sget-boolean v3, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    if-nez v3, :cond_1

    .line 1408
    :cond_0
    :goto_0
    return v1

    .line 1359
    :cond_1
    iget v3, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    if-eqz v3, :cond_0

    .line 1364
    sput-boolean v1, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    .line 1366
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "resizeROI called at index = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with value = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1368
    if-ltz p1, :cond_2

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt p1, v3, :cond_3

    .line 1370
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid Indexe passed in resizeROI function : index - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1371
    .local v0, "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1372
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1373
    sput-boolean v2, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 1376
    .end local v0    # "Error":Ljava/lang/String;
    :cond_3
    if-nez p2, :cond_4

    .line 1378
    sget-object v1, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v3, "Resize factor is 0..returning!!"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1379
    sput-boolean v2, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    move v1, v2

    .line 1380
    goto :goto_0

    .line 1383
    :cond_4
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 1384
    iget v3, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-static {v3, v4, p1, p2}, Lsiso/AutoCollage/AutoCollageEngineLib;->ResizeRoiAndRecreateCollage(ILsiso/AutoCollage/AndroidApiCalls;II)I

    move-result v3

    if-ne v3, v2, :cond_7

    .line 1386
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v3, :cond_6

    .line 1388
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 1389
    iget-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v3, :cond_5

    .line 1391
    const-string v0, "Engine Resize Image failed, may not consider as en err since user is not allowed to resize it after some factor from engine itself"

    .line 1392
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1393
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1394
    sput-boolean v2, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 1397
    .end local v0    # "Error":Ljava/lang/String;
    :cond_5
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createOutputBitmap()V

    .line 1399
    :cond_6
    sput-boolean v2, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    move v1, v2

    .line 1400
    goto/16 :goto_0

    .line 1404
    :cond_7
    const-string v0, "Engine Resize Image failed, may not consider as en err since user is not allowed to resize it after some factor from engine itself"

    .line 1405
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1406
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1407
    sput-boolean v2, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto/16 :goto_0
.end method

.method public response(I)V
    .locals 5
    .param p1, "resp"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 168
    sget-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "response from engine  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-eqz v0, :cond_0

    .line 171
    packed-switch p1, :pswitch_data_0

    .line 235
    :goto_0
    :pswitch_0
    return-void

    .line 176
    :pswitch_1
    iput-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    .line 177
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createOutputBitmap()V

    .line 178
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    invoke-interface {v0, p1}, Lsiso/AutoCollage/Collage$CollageResponse;->onCollageResponse(I)V

    goto :goto_0

    .line 194
    :pswitch_2
    iput-boolean v4, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    .line 195
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createOutputBitmap()V

    .line 196
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    invoke-interface {v0, p1}, Lsiso/AutoCollage/Collage$CollageResponse;->onCollageResponse(I)V

    goto :goto_0

    .line 205
    :cond_0
    packed-switch p1, :pswitch_data_1

    :pswitch_3
    goto :goto_0

    .line 227
    :pswitch_4
    iput-boolean v4, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    .line 228
    iput-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_WaitingOver:Z

    goto :goto_0

    .line 210
    :pswitch_5
    iput-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    .line 211
    iput-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_WaitingOver:Z

    goto :goto_0

    .line 171
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 205
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public save(Ljava/lang/String;Lsiso/AutoCollage/Collage$FileFormat;)Z
    .locals 13
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "fileFormat"    # Lsiso/AutoCollage/Collage$FileFormat;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 851
    sget-boolean v11, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    if-nez v11, :cond_0

    .line 853
    sget-object v10, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "createCollage m_bAllowRequest = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v12, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " Controller id = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    :goto_0
    return v9

    .line 856
    :cond_0
    sput-boolean v9, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    .line 859
    if-nez p1, :cond_1

    .line 861
    const-string v0, "FilePath is null for Save collage"

    .line 862
    .local v0, "Error":Ljava/lang/String;
    sget-object v11, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 863
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 864
    sput-boolean v10, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 868
    .end local v0    # "Error":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lsiso/AutoCollage/Collage;->getOutputBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    .line 870
    .local v7, "outBitmap":Landroid/graphics/Bitmap;
    if-nez v7, :cond_2

    .line 872
    const-string v0, "Output Bitmap is not Available, First create collage an then save"

    .line 873
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v11, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 874
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 875
    sput-boolean v10, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 878
    .end local v0    # "Error":Ljava/lang/String;
    :cond_2
    const/4 v5, 0x0

    .line 882
    .local v5, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 883
    .end local v5    # "out":Ljava/io/FileOutputStream;
    .local v6, "out":Ljava/io/FileOutputStream;
    :try_start_1
    sget-object v11, Lsiso/AutoCollage/Collage$FileFormat;->JPEG:Lsiso/AutoCollage/Collage$FileFormat;

    if-ne v11, p2, :cond_5

    .line 885
    sget-object v11, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v12, 0x64

    invoke-virtual {v7, v11, v12, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 887
    const-string v0, "Encoding to JPG file is failed"

    .line 888
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v11, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 890
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 891
    const/4 v11, 0x1

    sput-boolean v11, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 957
    .end local v0    # "Error":Ljava/lang/String;
    :catch_0
    move-exception v3

    move-object v5, v6

    .line 959
    .end local v6    # "out":Ljava/io/FileOutputStream;
    .local v3, "e":Ljava/io/FileNotFoundException;
    .restart local v5    # "out":Ljava/io/FileOutputStream;
    :goto_1
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 960
    sput-boolean v10, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 897
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .end local v5    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "out":Ljava/io/FileOutputStream;
    :cond_3
    :try_start_2
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 898
    iget v11, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-static {v11, p1}, Lsiso/AutoCollage/AutoCollageEngineLib;->DumpROI(ILjava/lang/String;)I

    move-result v11

    if-nez v11, :cond_4

    .line 900
    sget-object v11, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v12, "DumpROI Failed!!"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 901
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 902
    const/4 v11, 0x1

    sput-boolean v11, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 963
    :catch_1
    move-exception v3

    move-object v5, v6

    .line 967
    .end local v6    # "out":Ljava/io/FileOutputStream;
    .local v3, "e":Ljava/io/IOException;
    .restart local v5    # "out":Ljava/io/FileOutputStream;
    :goto_2
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 973
    :goto_3
    sput-boolean v10, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 905
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "out":Ljava/io/FileOutputStream;
    :cond_4
    :try_start_4
    iget-object v11, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v11, :cond_8

    .line 907
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 908
    iget-boolean v11, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v11, :cond_8

    .line 910
    const-string v0, "DumpROI failed"

    .line 911
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v11, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 912
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 913
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 914
    const/4 v11, 0x1

    sput-boolean v11, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto/16 :goto_0

    .line 921
    .end local v0    # "Error":Ljava/lang/String;
    :cond_5
    sget-object v11, Lsiso/AutoCollage/Collage$FileFormat;->PNG:Lsiso/AutoCollage/Collage$FileFormat;

    if-ne v11, p2, :cond_7

    .line 923
    sget-object v11, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v12, 0x64

    invoke-virtual {v7, v11, v12, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 925
    const-string v0, "Encoding to PNG file is failed"

    .line 926
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v11, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 927
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 928
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 929
    const/4 v11, 0x1

    sput-boolean v11, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto/16 :goto_0

    .line 936
    .end local v0    # "Error":Ljava/lang/String;
    :cond_6
    iget v11, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-static {v11, p1}, Lsiso/AutoCollage/AutoCollageEngineLib;->DumpROI(ILjava/lang/String;)I

    move-result v11

    if-nez v11, :cond_8

    .line 938
    sget-object v11, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v12, "DumpROI Failed!!"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 939
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 940
    const/4 v11, 0x1

    sput-boolean v11, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto/16 :goto_0

    .line 945
    :cond_7
    sget-object v11, Lsiso/AutoCollage/Collage$FileFormat;->RAW:Lsiso/AutoCollage/Collage$FileFormat;

    if-ne v11, p2, :cond_8

    .line 947
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v11

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    mul-int v8, v11, v12

    .line 948
    .local v8, "size":I
    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 949
    .local v2, "dst":Ljava/nio/ByteBuffer;
    invoke-virtual {v7, v2}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 950
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 951
    .local v1, "bitmapdata":[B
    invoke-virtual {v6, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 954
    .end local v1    # "bitmapdata":[B
    .end local v2    # "dst":Ljava/nio/ByteBuffer;
    .end local v8    # "size":I
    :cond_8
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 977
    sput-boolean v10, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    move v9, v10

    .line 978
    goto/16 :goto_0

    .line 969
    .end local v6    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "e":Ljava/io/IOException;
    .restart local v5    # "out":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v4

    .line 971
    .local v4, "e1":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 963
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "e1":Ljava/io/IOException;
    :catch_3
    move-exception v3

    goto/16 :goto_2

    .line 957
    :catch_4
    move-exception v3

    goto/16 :goto_1
.end method

.method public saveROI(Ljava/lang/String;Lsiso/AutoCollage/Collage$FileFormat;)Z
    .locals 5
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "fileFormat"    # Lsiso/AutoCollage/Collage$FileFormat;

    .prologue
    const/4 v1, 0x0

    .line 818
    if-nez p1, :cond_1

    .line 820
    const-string v0, "FilePath is null for Save collage"

    .line 821
    .local v0, "Error":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 822
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 844
    .end local v0    # "Error":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 827
    :cond_1
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 828
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Saving ROI SacController ID : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    iget v2, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-static {v2, p1}, Lsiso/AutoCollage/AutoCollageEngineLib;->DumpROI(ILjava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 833
    iget-object v2, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v2, :cond_2

    .line 835
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 836
    iget-boolean v2, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v2, :cond_2

    .line 838
    const-string v0, "saveROI failed"

    .line 839
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 840
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    goto :goto_0

    .line 844
    .end local v0    # "Error":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setBlendingEnabled(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 274
    const/4 v0, 0x0

    .line 276
    .local v0, "value":I
    if-eqz p1, :cond_1

    .line 277
    const/4 v0, 0x1

    .line 281
    :goto_0
    iget v1, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    if-lez v1, :cond_0

    .line 283
    iget v1, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-static {v1, v0}, Lsiso/AutoCollage/AutoCollageEngineLib;->SetBlendingEnabled(II)V

    .line 286
    :cond_0
    sget-object v1, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setBlendingEnabled enable = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-object v1, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iput-boolean p1, v1, Lsiso/AutoCollage/SaCConfig;->m_bEnableBlending:Z

    .line 288
    return-void

    .line 279
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setConfig(IILsiso/AutoCollage/Collage$PixelFormat;)V
    .locals 3
    .param p1, "collageWidth"    # I
    .param p2, "collageHeight"    # I
    .param p3, "pix_format"    # Lsiso/AutoCollage/Collage$PixelFormat;

    .prologue
    .line 240
    sget-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setConfig collageWidth = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "collageHeight = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "pix_format = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iput p2, v0, Lsiso/AutoCollage/SaCConfig;->m_collageHeight:I

    .line 242
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iput p1, v0, Lsiso/AutoCollage/SaCConfig;->m_collageWidth:I

    .line 243
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    invoke-virtual {v0, p3}, Lsiso/AutoCollage/SaCConfig;->setPixelFormat(Lsiso/AutoCollage/Collage$PixelFormat;)V

    .line 245
    return-void
.end method

.method public setFaceDetectionEnabled(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 294
    sget-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setFaceDetectionEnabled enable = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iput-boolean p1, v0, Lsiso/AutoCollage/SaCConfig;->m_bEnableFaceDetection:Z

    .line 296
    return-void
.end method

.method setLastErr(Ljava/lang/String;)V
    .locals 0
    .param p1, "Error"    # Ljava/lang/String;

    .prologue
    .line 1964
    iput-object p1, p0, Lsiso/AutoCollage/Collage;->m_eLastError:Ljava/lang/String;

    .line 1965
    return-void
.end method

.method public setMediaDBUsageEnabled(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 302
    sget-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setMediaDBUsageEnabled enable = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iput-boolean p1, v0, Lsiso/AutoCollage/SaCConfig;->m_bMediaDBEnabled:Z

    .line 304
    return-void
.end method

.method public setRandomPlacementEnabled(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 266
    sget-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setRandomPlacementEnabled enable = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iget-object v0, p0, Lsiso/AutoCollage/Collage;->m_SacConfig:Lsiso/AutoCollage/SaCConfig;

    iput-boolean p1, v0, Lsiso/AutoCollage/SaCConfig;->m_bRandomPlacementEnabled:Z

    .line 268
    return-void
.end method

.method public swapROI(II)Z
    .locals 6
    .param p1, "index1"    # I
    .param p2, "index2"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1089
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "swapROI called index1 = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "index2 = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1092
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge p1, v3, :cond_0

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge p2, v3, :cond_0

    if-ltz p1, :cond_0

    if-gez p2, :cond_1

    .line 1094
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid Indexes passed in swapROI function : indexes - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1095
    .local v0, "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1096
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1123
    .end local v0    # "Error":Ljava/lang/String;
    :goto_0
    return v1

    .line 1101
    :cond_1
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 1102
    iget v3, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-static {v3, v4, p1, p2}, Lsiso/AutoCollage/AutoCollageEngineLib;->SwapROI(ILsiso/AutoCollage/AndroidApiCalls;II)I

    move-result v3

    if-ne v3, v2, :cond_4

    .line 1104
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v3, :cond_3

    .line 1106
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 1107
    iget-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v3, :cond_2

    .line 1109
    const-string v0, "Engine Swap Image failed"

    .line 1110
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1111
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    goto :goto_0

    .line 1114
    .end local v0    # "Error":Ljava/lang/String;
    :cond_2
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createOutputBitmap()V

    :cond_3
    move v1, v2

    .line 1116
    goto :goto_0

    .line 1120
    :cond_4
    const-string v0, "Engine Swap Image failed"

    .line 1121
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v2, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1122
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public translateROI(III)Z
    .locals 6
    .param p1, "indexi"    # I
    .param p2, "deltaX"    # I
    .param p3, "deltaY"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1288
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "translateROI m_bAllowRequest = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v5, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "m_nSacControllerID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1289
    const-string v5, " indexi = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " deltaX = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " deltaY = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1288
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1291
    sget-boolean v3, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    if-nez v3, :cond_1

    .line 1345
    :cond_0
    :goto_0
    return v1

    .line 1296
    :cond_1
    iget v3, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    if-eqz v3, :cond_0

    .line 1301
    sput-boolean v1, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    .line 1304
    if-ltz p1, :cond_2

    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt p1, v3, :cond_3

    .line 1306
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid Indexe passed in translateROI function : index - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1307
    .local v0, "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1308
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1309
    sput-boolean v2, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 1313
    .end local v0    # "Error":Ljava/lang/String;
    :cond_3
    if-nez p2, :cond_4

    if-nez p3, :cond_4

    .line 1315
    sget-object v1, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v3, "translate factor is 0..returning!!"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1316
    sput-boolean v2, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    move v1, v2

    .line 1317
    goto :goto_0

    .line 1320
    :cond_4
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->initWaitFlag()V

    .line 1321
    iget v3, p0, Lsiso/AutoCollage/Collage;->m_nSacControllerID:I

    iget-object v4, p0, Lsiso/AutoCollage/Collage;->m_AndroidApiCalls:Lsiso/AutoCollage/AndroidApiCalls;

    invoke-static {v3, v4, p1, p2, p3}, Lsiso/AutoCollage/AutoCollageEngineLib;->TranslateRoiAndRecreateCollage(ILsiso/AutoCollage/AndroidApiCalls;III)I

    move-result v3

    if-ne v3, v2, :cond_7

    .line 1323
    iget-object v3, p0, Lsiso/AutoCollage/Collage;->m_CollageResponseHandler:Lsiso/AutoCollage/Collage$CollageResponse;

    if-nez v3, :cond_6

    .line 1325
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->waitForResponse()V

    .line 1326
    iget-boolean v3, p0, Lsiso/AutoCollage/Collage;->m_IsEngineError:Z

    if-eqz v3, :cond_5

    .line 1328
    const-string v0, "Engine Translate Image failed, may not consider as en err since user is not allowed to Translate it after some factor from engine itself"

    .line 1329
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1330
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1331
    sput-boolean v2, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0

    .line 1334
    .end local v0    # "Error":Ljava/lang/String;
    :cond_5
    invoke-direct {p0}, Lsiso/AutoCollage/Collage;->createOutputBitmap()V

    .line 1336
    :cond_6
    sput-boolean v2, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    move v1, v2

    .line 1337
    goto :goto_0

    .line 1341
    :cond_7
    const-string v0, "Engine Translate Image failed, may not consider as en err since user is not allowed to Translate it after some factor from engine itself"

    .line 1342
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v3, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1343
    invoke-virtual {p0, v0}, Lsiso/AutoCollage/Collage;->setLastErr(Ljava/lang/String;)V

    .line 1344
    sput-boolean v2, Lsiso/AutoCollage/Collage;->m_bAllowRequest:Z

    goto :goto_0
.end method
