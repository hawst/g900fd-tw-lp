.class Lsiso/AutoCollage/Threading$1;
.super Ljava/lang/Thread;
.source "Threading.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsiso/AutoCollage/Threading;->DecodedOnThread(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lsiso/AutoCollage/Threading;

.field private final synthetic val$imageIndex:I


# direct methods
.method constructor <init>(Lsiso/AutoCollage/Threading;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    iput p2, p0, Lsiso/AutoCollage/Threading$1;->val$imageIndex:I

    .line 56
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 63
    iget-object v1, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    iget-object v1, v1, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    iget-object v1, v1, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    iget v2, p0, Lsiso/AutoCollage/Threading$1;->val$imageIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 65
    iget-object v2, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    iget-object v1, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    iget-object v1, v1, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    iget-object v1, v1, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    iget v3, p0, Lsiso/AutoCollage/Threading$1;->val$imageIndex:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget v3, p0, Lsiso/AutoCollage/Threading$1;->val$imageIndex:I

    invoke-virtual {v2, v1, v3}, Lsiso/AutoCollage/Threading;->DecodeFile(Ljava/lang/String;I)V

    .line 67
    :cond_0
    iget-object v1, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    iget v2, v1, Lsiso/AutoCollage/Threading;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lsiso/AutoCollage/Threading;->count:I

    .line 72
    sget v0, Lsiso/AutoCollage/AndroidApiCalls;->NUMBER_OF_CORES:I

    .line 73
    .local v0, "i":I
    sget-object v1, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "number of cores = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    :goto_0
    iget v1, p0, Lsiso/AutoCollage/Threading$1;->val$imageIndex:I

    add-int/2addr v1, v0

    iget-object v2, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    iget-object v2, v2, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 89
    iget-object v1, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    iget v1, v1, Lsiso/AutoCollage/Threading;->count:I

    iget-object v2, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    iget-object v2, v2, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    iget-object v2, v2, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 91
    iget-object v1, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    const/4 v2, 0x0

    iput v2, v1, Lsiso/AutoCollage/Threading;->count:I

    .line 92
    iget-object v1, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    iget-object v1, v1, Lsiso/AutoCollage/Threading;->mDoneSignal:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 94
    :cond_1
    return-void

    .line 79
    :cond_2
    iget-object v1, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    iget-object v1, v1, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    iget-object v1, v1, Lsiso/AutoCollage/Collage;->m_szInputImageBuffers:Ljava/util/ArrayList;

    iget v2, p0, Lsiso/AutoCollage/Threading$1;->val$imageIndex:I

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    .line 81
    iget-object v2, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    iget-object v1, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    iget-object v1, v1, Lsiso/AutoCollage/Threading;->collage:Lsiso/AutoCollage/Collage;

    iget-object v1, v1, Lsiso/AutoCollage/Collage;->m_szInputFileList:Ljava/util/ArrayList;

    iget v3, p0, Lsiso/AutoCollage/Threading$1;->val$imageIndex:I

    add-int/2addr v3, v0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget v3, p0, Lsiso/AutoCollage/Threading$1;->val$imageIndex:I

    add-int/2addr v3, v0

    invoke-virtual {v2, v1, v3}, Lsiso/AutoCollage/Threading;->DecodeFile(Ljava/lang/String;I)V

    .line 83
    :cond_3
    sget v1, Lsiso/AutoCollage/AndroidApiCalls;->NUMBER_OF_CORES:I

    add-int/2addr v0, v1

    .line 84
    iget-object v1, p0, Lsiso/AutoCollage/Threading$1;->this$0:Lsiso/AutoCollage/Threading;

    iget v2, v1, Lsiso/AutoCollage/Threading;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lsiso/AutoCollage/Threading;->count:I

    goto :goto_0
.end method
