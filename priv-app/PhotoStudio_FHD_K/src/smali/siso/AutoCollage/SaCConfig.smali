.class Lsiso/AutoCollage/SaCConfig;
.super Ljava/lang/Object;
.source "SaCConfig.java"


# instance fields
.field m_bEnableBlending:Z

.field m_bEnableFaceDetection:Z

.field m_bMediaDBEnabled:Z

.field m_bRandomPlacementEnabled:Z

.field m_collageHeight:I

.field m_collageWidth:I

.field m_nImages:I

.field private pixelFormat:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput v0, p0, Lsiso/AutoCollage/SaCConfig;->m_nImages:I

    .line 24
    iput v0, p0, Lsiso/AutoCollage/SaCConfig;->m_collageWidth:I

    .line 25
    iput v0, p0, Lsiso/AutoCollage/SaCConfig;->m_collageHeight:I

    .line 26
    iput-boolean v1, p0, Lsiso/AutoCollage/SaCConfig;->m_bEnableBlending:Z

    .line 27
    iput-boolean v1, p0, Lsiso/AutoCollage/SaCConfig;->m_bEnableFaceDetection:Z

    .line 28
    iput-boolean v0, p0, Lsiso/AutoCollage/SaCConfig;->m_bRandomPlacementEnabled:Z

    .line 29
    iput-boolean v1, p0, Lsiso/AutoCollage/SaCConfig;->m_bMediaDBEnabled:Z

    .line 30
    iput v0, p0, Lsiso/AutoCollage/SaCConfig;->pixelFormat:I

    .line 31
    return-void
.end method


# virtual methods
.method getPixelFormat()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lsiso/AutoCollage/SaCConfig;->pixelFormat:I

    return v0
.end method

.method setPixelFormat(Lsiso/AutoCollage/Collage$PixelFormat;)V
    .locals 1
    .param p1, "pixFormat"    # Lsiso/AutoCollage/Collage$PixelFormat;

    .prologue
    .line 37
    sget-object v0, Lsiso/AutoCollage/Collage$PixelFormat;->RGB_565:Lsiso/AutoCollage/Collage$PixelFormat;

    if-ne p1, v0, :cond_0

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lsiso/AutoCollage/SaCConfig;->pixelFormat:I

    .line 41
    :goto_0
    return-void

    .line 40
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lsiso/AutoCollage/SaCConfig;->pixelFormat:I

    goto :goto_0
.end method
