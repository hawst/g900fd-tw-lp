.class public final enum Lsiso/AutoCollage/Collage$FileFormat;
.super Ljava/lang/Enum;
.source "Collage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsiso/AutoCollage/Collage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FileFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsiso/AutoCollage/Collage$FileFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lsiso/AutoCollage/Collage$FileFormat;

.field public static final enum JPEG:Lsiso/AutoCollage/Collage$FileFormat;

.field public static final enum PNG:Lsiso/AutoCollage/Collage$FileFormat;

.field public static final enum RAW:Lsiso/AutoCollage/Collage$FileFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-instance v0, Lsiso/AutoCollage/Collage$FileFormat;

    const-string v1, "JPEG"

    invoke-direct {v0, v1, v2}, Lsiso/AutoCollage/Collage$FileFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsiso/AutoCollage/Collage$FileFormat;->JPEG:Lsiso/AutoCollage/Collage$FileFormat;

    new-instance v0, Lsiso/AutoCollage/Collage$FileFormat;

    const-string v1, "PNG"

    invoke-direct {v0, v1, v3}, Lsiso/AutoCollage/Collage$FileFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsiso/AutoCollage/Collage$FileFormat;->PNG:Lsiso/AutoCollage/Collage$FileFormat;

    new-instance v0, Lsiso/AutoCollage/Collage$FileFormat;

    const-string v1, "RAW"

    invoke-direct {v0, v1, v4}, Lsiso/AutoCollage/Collage$FileFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsiso/AutoCollage/Collage$FileFormat;->RAW:Lsiso/AutoCollage/Collage$FileFormat;

    .line 53
    const/4 v0, 0x3

    new-array v0, v0, [Lsiso/AutoCollage/Collage$FileFormat;

    sget-object v1, Lsiso/AutoCollage/Collage$FileFormat;->JPEG:Lsiso/AutoCollage/Collage$FileFormat;

    aput-object v1, v0, v2

    sget-object v1, Lsiso/AutoCollage/Collage$FileFormat;->PNG:Lsiso/AutoCollage/Collage$FileFormat;

    aput-object v1, v0, v3

    sget-object v1, Lsiso/AutoCollage/Collage$FileFormat;->RAW:Lsiso/AutoCollage/Collage$FileFormat;

    aput-object v1, v0, v4

    sput-object v0, Lsiso/AutoCollage/Collage$FileFormat;->ENUM$VALUES:[Lsiso/AutoCollage/Collage$FileFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsiso/AutoCollage/Collage$FileFormat;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lsiso/AutoCollage/Collage$FileFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsiso/AutoCollage/Collage$FileFormat;

    return-object v0
.end method

.method public static values()[Lsiso/AutoCollage/Collage$FileFormat;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lsiso/AutoCollage/Collage$FileFormat;->ENUM$VALUES:[Lsiso/AutoCollage/Collage$FileFormat;

    array-length v1, v0

    new-array v2, v1, [Lsiso/AutoCollage/Collage$FileFormat;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
