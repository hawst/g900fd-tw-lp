.class Lsiso/AutoCollage/NativeBuffer;
.super Ljava/lang/Object;
.source "Savedbuffer.java"


# instance fields
.field bufHeight:I

.field bufWidth:I

.field bufferKey:I

.field bufscaleFactorH:F

.field bufscaleFactorW:F

.field m_bIsRotated:Z

.field private pbuf:Ljava/nio/ByteBuffer;

.field readFlag:Z


# direct methods
.method constructor <init>(Ljava/nio/ByteBuffer;IIFFI)V
    .locals 1
    .param p1, "input"    # Ljava/nio/ByteBuffer;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "scaleFactorW"    # F
    .param p5, "scaleFactorH"    # F
    .param p6, "key"    # I

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lsiso/AutoCollage/NativeBuffer;->pbuf:Ljava/nio/ByteBuffer;

    .line 28
    iput p2, p0, Lsiso/AutoCollage/NativeBuffer;->bufWidth:I

    .line 29
    iput p3, p0, Lsiso/AutoCollage/NativeBuffer;->bufHeight:I

    .line 30
    iput p4, p0, Lsiso/AutoCollage/NativeBuffer;->bufscaleFactorW:F

    .line 31
    iput p5, p0, Lsiso/AutoCollage/NativeBuffer;->bufscaleFactorH:F

    .line 32
    iput-boolean v0, p0, Lsiso/AutoCollage/NativeBuffer;->m_bIsRotated:Z

    .line 33
    iput p6, p0, Lsiso/AutoCollage/NativeBuffer;->bufferKey:I

    .line 34
    iput-boolean v0, p0, Lsiso/AutoCollage/NativeBuffer;->readFlag:Z

    .line 35
    return-void
.end method


# virtual methods
.method GetBuffer()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lsiso/AutoCollage/NativeBuffer;->pbuf:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public getBufferHeight()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lsiso/AutoCollage/NativeBuffer;->bufHeight:I

    return v0
.end method

.method public getBufferKey()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lsiso/AutoCollage/NativeBuffer;->bufferKey:I

    return v0
.end method

.method public getBufferReadFlag()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lsiso/AutoCollage/NativeBuffer;->readFlag:Z

    return v0
.end method

.method public getBufferWidth()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lsiso/AutoCollage/NativeBuffer;->bufWidth:I

    return v0
.end method

.method public setBufferReadFlag(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lsiso/AutoCollage/NativeBuffer;->readFlag:Z

    .line 52
    return-void
.end method
