.class Lsiso/AutoCollage/Savedbuffer;
.super Ljava/lang/Object;
.source "Savedbuffer.java"


# instance fields
.field private height:I

.field private pbuf:Ljava/nio/ByteBuffer;

.field private width:I


# direct methods
.method constructor <init>(III)V
    .locals 3
    .param p1, "Width"    # I
    .param p2, "Height"    # I
    .param p3, "bytesPerPixel"    # I

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput p1, p0, Lsiso/AutoCollage/Savedbuffer;->width:I

    .line 81
    iput p2, p0, Lsiso/AutoCollage/Savedbuffer;->height:I

    .line 82
    iget v1, p0, Lsiso/AutoCollage/Savedbuffer;->width:I

    iget v2, p0, Lsiso/AutoCollage/Savedbuffer;->height:I

    mul-int/2addr v1, v2

    mul-int v0, v1, p3

    .line 83
    .local v0, "size":I
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lsiso/AutoCollage/Savedbuffer;->pbuf:Ljava/nio/ByteBuffer;

    .line 84
    return-void
.end method


# virtual methods
.method Add(Lsiso/AutoCollage/AndroidApiCalls;Lsiso/AutoCollage/Savedbuffer;)V
    .locals 2
    .param p1, "androidApiCalls"    # Lsiso/AutoCollage/AndroidApiCalls;
    .param p2, "pObject"    # Lsiso/AutoCollage/Savedbuffer;

    .prologue
    .line 116
    sget-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v1, "SavedBuffer Add invoked "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iget-object v0, p1, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    const/4 v1, 0x0

    iput-object v1, v0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    .line 118
    iget-object v0, p1, Lsiso/AutoCollage/AndroidApiCalls;->collage:Lsiso/AutoCollage/Collage;

    iput-object p2, v0, Lsiso/AutoCollage/Collage;->m_CollageBuffer:Lsiso/AutoCollage/Savedbuffer;

    .line 119
    return-void
.end method

.method CleanBuffer()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lsiso/AutoCollage/Savedbuffer;->pbuf:Ljava/nio/ByteBuffer;

    .line 110
    return-void
.end method

.method GetBuffer()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lsiso/AutoCollage/Savedbuffer;->pbuf:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method GetHeight()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lsiso/AutoCollage/Savedbuffer;->height:I

    return v0
.end method

.method GetWidth()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lsiso/AutoCollage/Savedbuffer;->width:I

    return v0
.end method
