.class public Lsiso/AutoCollage/AutoCollageEngineLib;
.super Ljava/lang/Object;
.source "AutoCollageEngineLib.java"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    const-string v0, "CollageEngine"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 14
    sget-object v0, Lsiso/AutoCollage/Collage;->TAG:Ljava/lang/String;

    const-string v1, "== *********  Collage Engine Lib is loaded ********* =="

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static native Cancel()V
.end method

.method static native CreateCollage(Lsiso/AutoCollage/AndroidApiCalls;Lsiso/AutoCollage/SaCConfig;[Ljava/lang/String;I[I)I
.end method

.method static native DecodeImages(ILsiso/AutoCollage/AndroidApiCalls;)I
.end method

.method static native DoDeInit(I)I
.end method

.method static native DumpROI(ILjava/lang/String;)I
.end method

.method static native LoadCollage(Lsiso/AutoCollage/AndroidApiCalls;Ljava/lang/String;)I
.end method

.method static native LoadCollageROI(Lsiso/AutoCollage/AndroidApiCalls;Ljava/lang/String;)I
.end method

.method static native ReplaceROI(ILsiso/AutoCollage/AndroidApiCalls;ILjava/lang/String;)I
.end method

.method static native ResizeRoiAndRecreateCollage(ILsiso/AutoCollage/AndroidApiCalls;II)I
.end method

.method static native SetBlendingEnabled(II)V
.end method

.method static native SwapROI(ILsiso/AutoCollage/AndroidApiCalls;II)I
.end method

.method static native TranslateRoiAndRecreateCollage(ILsiso/AutoCollage/AndroidApiCalls;III)I
.end method

.method static native test([Ljava/lang/String;)I
.end method
