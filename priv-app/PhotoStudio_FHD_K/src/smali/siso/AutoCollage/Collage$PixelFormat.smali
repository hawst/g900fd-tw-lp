.class public final enum Lsiso/AutoCollage/Collage$PixelFormat;
.super Ljava/lang/Enum;
.source "Collage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsiso/AutoCollage/Collage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PixelFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsiso/AutoCollage/Collage$PixelFormat;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ARGB_8888:Lsiso/AutoCollage/Collage$PixelFormat;

.field private static final synthetic ENUM$VALUES:[Lsiso/AutoCollage/Collage$PixelFormat;

.field public static final enum RGB_565:Lsiso/AutoCollage/Collage$PixelFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    new-instance v0, Lsiso/AutoCollage/Collage$PixelFormat;

    const-string v1, "RGB_565"

    invoke-direct {v0, v1, v2}, Lsiso/AutoCollage/Collage$PixelFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsiso/AutoCollage/Collage$PixelFormat;->RGB_565:Lsiso/AutoCollage/Collage$PixelFormat;

    new-instance v0, Lsiso/AutoCollage/Collage$PixelFormat;

    const-string v1, "ARGB_8888"

    invoke-direct {v0, v1, v3}, Lsiso/AutoCollage/Collage$PixelFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsiso/AutoCollage/Collage$PixelFormat;->ARGB_8888:Lsiso/AutoCollage/Collage$PixelFormat;

    .line 48
    const/4 v0, 0x2

    new-array v0, v0, [Lsiso/AutoCollage/Collage$PixelFormat;

    sget-object v1, Lsiso/AutoCollage/Collage$PixelFormat;->RGB_565:Lsiso/AutoCollage/Collage$PixelFormat;

    aput-object v1, v0, v2

    sget-object v1, Lsiso/AutoCollage/Collage$PixelFormat;->ARGB_8888:Lsiso/AutoCollage/Collage$PixelFormat;

    aput-object v1, v0, v3

    sput-object v0, Lsiso/AutoCollage/Collage$PixelFormat;->ENUM$VALUES:[Lsiso/AutoCollage/Collage$PixelFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsiso/AutoCollage/Collage$PixelFormat;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lsiso/AutoCollage/Collage$PixelFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsiso/AutoCollage/Collage$PixelFormat;

    return-object v0
.end method

.method public static values()[Lsiso/AutoCollage/Collage$PixelFormat;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lsiso/AutoCollage/Collage$PixelFormat;->ENUM$VALUES:[Lsiso/AutoCollage/Collage$PixelFormat;

    array-length v1, v0

    new-array v2, v1, [Lsiso/AutoCollage/Collage$PixelFormat;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
