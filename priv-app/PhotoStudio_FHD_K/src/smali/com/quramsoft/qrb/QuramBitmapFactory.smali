.class public Lcom/quramsoft/qrb/QuramBitmapFactory;
.super Ljava/lang/Object;
.source "QuramBitmapFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quramsoft/qrb/QuramBitmapFactory$MidPointerData;,
        Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field public static final DEC_CANCELED:I = 0x6

.field public static final DEC_FAIL:I = 0x0

.field public static final DEC_PROGRESS:I = 0x4

.field public static final DEC_SUCCESS:I = 0x1

.field public static final LENGTH_OF_MID_POINTER:I = 0x3c

.field public static final Quram_JPEG:Ljava/lang/String; = "Quram_JPEG"

.field private static final TAG:Ljava/lang/String; = "QuramBitmapFactory"

.field protected static final USE_AUTO_BUFFERMODE:I = 0x2

.field protected static final USE_AUTO_FILEMODE:I = 0x0

.field public static final USE_FULLSIZE_BUFFER:I = 0x0

.field public static final USE_ITERSIZE_BUFFER:I = 0x1

.field public static final USE_MAKE_REGIONMAP:I = 0x2

.field protected static final USE_POWER_PROCESS:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    :try_start_0
    const-string v0, "qjpegforphotoeditor"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1187
    :goto_0
    return-void

    .line 35
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native DecodeCancel(I)V
.end method

.method public static native DecodeJpegFromStream(Ljava/io/InputStream;ILcom/quramsoft/qrb/QuramBitmapFactory$Options;[BILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
.end method

.method public static cancelDecode(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V
    .locals 1
    .param p0, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    .line 1157
    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inCancelingRequested:Z

    if-eqz v0, :cond_1

    .line 1161
    :cond_0
    :goto_0
    return-void

    .line 1159
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inCancelingRequested:Z

    .line 1160
    invoke-virtual {p0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    invoke-static {v0}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeCancel(I)V

    goto :goto_0
.end method

.method public static compressToByte(Landroid/graphics/Bitmap;Ljava/lang/String;[BII)I
    .locals 2
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "compressFormat"    # Ljava/lang/String;
    .param p2, "out"    # [B
    .param p3, "out_bufsize"    # I
    .param p4, "quality"    # I

    .prologue
    .line 1101
    const/4 v0, 0x0

    .line 1102
    .local v0, "ret":I
    const-string v1, "Quram_JPEG"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1104
    const/4 v1, 0x0

    .line 1109
    :goto_0
    return v1

    .line 1107
    :cond_0
    const/4 v1, 0x0

    invoke-static {p0, p2, p3, p4, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativeEncodeByteArray(Landroid/graphics/Bitmap;[BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v0

    move v1, v0

    .line 1109
    goto :goto_0
.end method

.method public static compressToFile(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;ILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
    .locals 7
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "compressFormat"    # Ljava/lang/String;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "quality"    # I
    .param p4, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    .line 1122
    const/4 v6, 0x0

    .line 1124
    .local v6, "ret":I
    const-string v0, "Quram_JPEG"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1126
    const/4 v0, 0x0

    .line 1132
    :goto_0
    return v0

    .line 1130
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move-object v0, p0

    move-object v1, p2

    move v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativeEncodeFile(Landroid/graphics/Bitmap;Ljava/lang/String;IIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v6

    move v0, v6

    .line 1132
    goto :goto_0
.end method

.method public static compressToFile([BLjava/lang/String;Ljava/lang/String;IIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
    .locals 7
    .param p0, "data"    # [B
    .param p1, "compressFormat"    # Ljava/lang/String;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "quality"    # I
    .param p6, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    .line 1137
    const/4 v6, 0x0

    .line 1139
    .local v6, "ret":I
    const-string v0, "Quram_JPEG"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1141
    const/4 v0, 0x0

    .line 1147
    :goto_0
    return v0

    :cond_0
    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    .line 1145
    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativeEncodeFileFromByte([BLjava/lang/String;IIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v6

    move v0, v6

    .line 1147
    goto :goto_0
.end method

.method public static decodeByteArray([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/4 v1, 0x0

    .line 250
    const/4 v0, 0x0

    .line 252
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-gez p1, :cond_1

    .line 273
    :cond_0
    :goto_0
    return-object v1

    .line 257
    :cond_1
    if-lez p2, :cond_0

    .line 262
    array-length v2, p0

    add-int v3, p2, p1

    if-lt v2, v3, :cond_0

    .line 267
    invoke-virtual {p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v2

    if-nez v2, :cond_0

    .line 272
    invoke-static {p0, p1, p2, p3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativeDecodeByteArray([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 273
    goto :goto_0
.end method

.method public static decodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    .line 482
    const/4 v0, 0x0

    .line 484
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    if-eqz v1, :cond_0

    .line 488
    const/4 v1, 0x0

    .line 494
    :goto_0
    return-object v1

    .line 491
    :cond_0
    invoke-static {p0, p1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativeDecodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 493
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v1, v0

    .line 494
    goto :goto_0
.end method

.method public static decodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;III)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "origId"    # I

    .prologue
    .line 222
    const/4 v0, 0x0

    .line 224
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    if-eqz v1, :cond_0

    .line 228
    const/4 v1, 0x0

    .line 237
    :goto_0
    return-object v1

    .line 231
    :cond_0
    invoke-virtual {p1, p2}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setWidth(I)V

    .line 232
    invoke-virtual {p1, p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHeight(I)V

    .line 234
    invoke-static {p0, p1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativeDecodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 236
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v1, v0

    .line 237
    goto :goto_0
.end method

.method public static decodeFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "fd"    # Ljava/io/FileDescriptor;

    .prologue
    .line 1346
    new-instance v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 1347
    .local v0, "option":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "outpadding"    # Landroid/graphics/Rect;
    .param p2, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/4 v8, 0x0

    .line 1352
    const/4 v6, 0x0

    .line 1353
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    const/16 v1, 0x4000

    new-array v3, v1, [B

    .line 1355
    .local v3, "bytearray":[B
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 1357
    .local v0, "fis":Ljava/io/FileInputStream;
    invoke-virtual {p2}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1359
    const-string v1, "QuramBitmapFactory"

    const-string v2, "option Fail"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v8

    .line 1388
    :goto_0
    return-object v1

    .line 1365
    :cond_0
    :try_start_0
    iget v1, p2, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    if-nez v1, :cond_2

    .line 1366
    invoke-virtual {v0}, Ljava/io/FileInputStream;->available()I

    move-result v1

    iget v4, p2, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJpegFromStream(Ljava/io/InputStream;ILcom/quramsoft/qrb/QuramBitmapFactory$Options;[BILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 1377
    :cond_1
    :goto_1
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 1379
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1387
    const/4 v3, 0x0

    move-object v1, v6

    .line 1388
    goto :goto_0

    .line 1367
    :cond_2
    :try_start_2
    iget v1, p2, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1

    .line 1368
    invoke-virtual {v0}, Ljava/io/FileInputStream;->available()I

    move-result v1

    iget v4, p2, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->DecodeJpegFromStream(Ljava/io/InputStream;ILcom/quramsoft/qrb/QuramBitmapFactory$Options;[BILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v6

    goto :goto_1

    .line 1371
    :catch_0
    move-exception v7

    .line 1373
    .local v7, "e":Ljava/lang/Exception;
    const-string v1, "QuramBitmapFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v8

    .line 1374
    goto :goto_0

    .line 1381
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v7

    .line 1383
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v1, "QuramBitmapFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v8

    .line 1384
    goto :goto_0
.end method

.method public static decodeFileFromThumbnail(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;III)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "origId"    # I

    .prologue
    .line 62
    const/4 v0, 0x0

    .line 64
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    const/4 v1, 0x0

    .line 77
    :goto_0
    return-object v1

    .line 71
    :cond_0
    invoke-virtual {p1, p2}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setWidth(I)V

    .line 72
    invoke-virtual {p1, p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHeight(I)V

    .line 74
    invoke-static {p0, p1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativeDecodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 76
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v1, v0

    .line 77
    goto :goto_0
.end method

.method public static decodeFileStream(Ljava/io/InputStream;IILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "inputstream"    # Ljava/io/InputStream;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    .line 336
    invoke-static {p0, p1, p2, p3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeStream(Ljava/io/InputStream;IILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeFileToBuffer(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;II)Ljava/nio/ByteBuffer;
    .locals 1
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 166
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFileToBuffer(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;III)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static decodeFileToBuffer(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;III)Ljava/nio/ByteBuffer;
    .locals 7
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "origId"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 172
    const/4 v0, 0x0

    .line 174
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    if-eqz v3, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-object v2

    .line 181
    :cond_1
    iget v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v4, 0x7

    if-ne v3, v4, :cond_2

    .line 182
    mul-int v3, p2, p3

    mul-int/lit8 v3, v3, 0x4

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 193
    :goto_1
    sget-object v3, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 195
    invoke-virtual {p1, p2}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setWidth(I)V

    .line 196
    invoke-virtual {p1, p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHeight(I)V

    .line 198
    invoke-static {p0, v0, p1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativeDecodeFileToBuffer(Ljava/lang/String;Ljava/nio/ByteBuffer;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v1

    .line 200
    .local v1, "ret":I
    invoke-virtual {p1, v6}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 202
    if-eqz v1, :cond_0

    move-object v2, v0

    .line 207
    goto :goto_0

    .line 183
    .end local v1    # "ret":I
    :cond_2
    iget v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    if-nez v3, :cond_3

    .line 184
    mul-int v3, p2, p3

    mul-int/lit8 v3, v3, 0x2

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_1

    .line 185
    :cond_3
    iget v3, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_4

    .line 186
    mul-int v3, p2, p3

    add-int/lit8 v4, p2, 0x1

    shr-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, p3, 0x1

    shr-int/lit8 v5, v5, 0x1

    mul-int/2addr v4, v5

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_1

    .line 189
    :cond_4
    invoke-virtual {p1, v6}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0
.end method

.method public static decodeStream(Ljava/io/InputStream;IILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "inputstream"    # Ljava/io/InputStream;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/4 v5, 0x0

    .line 278
    const/4 v0, 0x0

    .line 280
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .line 282
    .local v1, "data":[B
    const/4 v4, 0x0

    .line 284
    .local v4, "ret":I
    const/4 v3, 0x0

    .line 286
    .local v3, "len":I
    if-nez p0, :cond_0

    .line 288
    const-string v6, "QuramBitmapFactory"

    const-string v7, "inputstream is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    :goto_0
    return-object v5

    .line 294
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->available()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 302
    :goto_1
    if-gtz v3, :cond_1

    .line 304
    const-string v6, "QuramBitmapFactory"

    const-string v7, "inpustream open fail"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 296
    :catch_0
    move-exception v2

    .line 298
    .local v2, "e":Ljava/io/IOException;
    const/4 v3, 0x0

    .line 299
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 308
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    new-array v1, v3, [B

    .line 312
    :try_start_1
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 320
    :goto_2
    invoke-virtual {p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v6

    if-eqz v6, :cond_2

    .line 322
    const-string v6, "QuramBitmapFactory"

    const-string v7, "option Fail"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 314
    :catch_1
    move-exception v2

    .line 316
    .restart local v2    # "e":Ljava/io/IOException;
    const/4 v1, 0x0

    .line 317
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 326
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    invoke-virtual {p3, p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setWidth(I)V

    .line 327
    invoke-virtual {p3, p2}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHeight(I)V

    .line 329
    const/4 v5, 0x0

    invoke-static {v1, v5, v3, p3}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativeDecodeByteArray([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v5, v0

    .line 331
    goto :goto_0
.end method

.method public static decodeThumbnailByteArrayToBuffer([BIILcom/quramsoft/qrb/ImageBufferData;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
    .locals 7
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "output"    # Lcom/quramsoft/qrb/ImageBufferData;
    .param p4, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/4 v3, 0x0

    .line 351
    const/4 v2, 0x0

    .line 352
    .local v2, "width":I
    const/4 v0, 0x0

    .line 354
    .local v0, "height":I
    const/4 v1, 0x0

    .line 356
    .local v1, "ret":I
    if-gez p1, :cond_1

    .line 419
    :cond_0
    :goto_0
    return v3

    .line 361
    :cond_1
    if-lez p2, :cond_0

    .line 366
    array-length v4, p0

    add-int v5, p2, p1

    if-lt v4, v5, :cond_0

    .line 371
    if-eqz p3, :cond_0

    .line 376
    invoke-virtual {p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v4

    if-nez v4, :cond_0

    .line 381
    invoke-static {p0, v3, p2, p4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativeGetImageInfoFromBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v1

    .line 383
    invoke-virtual {p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget v5, p4, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    invoke-static {v4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->round(F)I

    move-result v2

    .line 384
    invoke-virtual {p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p4, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    invoke-static {v4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->round(F)I

    move-result v0

    .line 386
    if-nez v1, :cond_2

    .line 388
    invoke-virtual {p4, v3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 392
    :cond_2
    iget v4, p4, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v5, 0x7

    if-ne v4, v5, :cond_3

    .line 393
    mul-int v4, v2, v0

    mul-int/lit8 v4, v4, 0x4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    iput-object v4, p3, Lcom/quramsoft/qrb/ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    .line 404
    :goto_1
    iget-object v4, p3, Lcom/quramsoft/qrb/ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    sget-object v5, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 406
    invoke-virtual {p4, v2}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setWidth(I)V

    .line 407
    invoke-virtual {p4, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHeight(I)V

    .line 409
    iget-object v4, p3, Lcom/quramsoft/qrb/ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    invoke-static {p0, v4, p1, p2, p4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativeDecodeByteArrayToBuffer([BLjava/nio/ByteBuffer;IILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v1

    .line 411
    invoke-virtual {p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getWidth()I

    move-result v4

    iput v4, p3, Lcom/quramsoft/qrb/ImageBufferData;->width:I

    .line 412
    invoke-virtual {p4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHeight()I

    move-result v4

    iput v4, p3, Lcom/quramsoft/qrb/ImageBufferData;->height:I

    .line 414
    invoke-virtual {p4, v3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 416
    iget-object v4, p3, Lcom/quramsoft/qrb/ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    if-eqz v4, :cond_0

    .line 419
    const/4 v3, 0x1

    goto :goto_0

    .line 394
    :cond_3
    iget v4, p4, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    if-nez v4, :cond_4

    .line 395
    mul-int v4, v2, v0

    mul-int/lit8 v4, v4, 0x2

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    iput-object v4, p3, Lcom/quramsoft/qrb/ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    goto :goto_1

    .line 396
    :cond_4
    iget v4, p4, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_5

    .line 397
    mul-int v4, v2, v0

    add-int/lit8 v5, v2, 0x1

    shr-int/lit8 v5, v5, 0x1

    add-int/lit8 v6, v0, 0x1

    shr-int/lit8 v6, v6, 0x1

    mul-int/2addr v5, v6

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    iput-object v4, p3, Lcom/quramsoft/qrb/ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    goto :goto_1

    .line 400
    :cond_5
    invoke-virtual {p4, v3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    goto/16 :goto_0
.end method

.method public static getExifData(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
    .locals 3
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "options"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/4 v1, 0x0

    .line 671
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v0, v1

    .line 680
    :cond_1
    :goto_0
    return v0

    .line 674
    :cond_2
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v2

    invoke-static {p0, v2, p1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativeGetExifData(Ljava/lang/String;ILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v0

    .line 676
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 677
    invoke-virtual {p1, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setExif(I)V

    goto :goto_0
.end method

.method public static native nativeCreateDecBufferInfo([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
.end method

.method public static native nativeCreateDecFileInfo(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)I
.end method

.method public static native nativeDecodeByteArray([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
.end method

.method public static native nativeDecodeByteArrayToBuffer([BLjava/nio/ByteBuffer;IILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
.end method

.method public static native nativeDecodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
.end method

.method public static native nativeDecodeFileToBuffer(Ljava/lang/String;Ljava/nio/ByteBuffer;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
.end method

.method public static native nativeEncodeByteArray(Landroid/graphics/Bitmap;[BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
.end method

.method public static native nativeEncodeFile(Landroid/graphics/Bitmap;Ljava/lang/String;IIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
.end method

.method public static native nativeEncodeFileFromByte([BLjava/lang/String;IIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
.end method

.method public static native nativeGetExifData(Ljava/lang/String;ILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
.end method

.method public static native nativeGetImageInfoFromBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
.end method

.method public static native nativeGetImageInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
.end method

.method public static native nativePartialDecodeByteArray([BIIIIIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
.end method

.method public static native nativePartialDecodeByteArrayToBuffer([BLjava/nio/ByteBuffer;IIIIIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I
.end method

.method public static native nativePartialDecodeFile(Ljava/lang/String;IIIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
.end method

.method public static partialDecodeByteArray([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;IIII)Landroid/graphics/Bitmap;
    .locals 14
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p4, "left"    # I
    .param p5, "right"    # I
    .param p6, "top"    # I
    .param p7, "bottom"    # I

    .prologue
    .line 506
    const/4 v9, 0x0

    .line 507
    .local v9, "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p3

    iget v12, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 508
    .local v12, "sampleSize":I
    const/4 v13, 0x0

    .line 509
    .local v13, "width":I
    const/4 v11, 0x0

    .line 511
    .local v11, "height":I
    if-gez p1, :cond_0

    .line 513
    const/4 v1, 0x0

    .line 553
    :goto_0
    return-object v1

    .line 516
    :cond_0
    if-gtz p2, :cond_1

    .line 518
    const/4 v1, 0x0

    goto :goto_0

    .line 521
    :cond_1
    array-length v1, p0

    add-int v2, p2, p1

    if-ge v1, v2, :cond_2

    .line 523
    const/4 v1, 0x0

    goto :goto_0

    .line 526
    :cond_2
    invoke-virtual/range {p3 .. p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    if-eqz v1, :cond_3

    .line 528
    const/4 v1, 0x0

    goto :goto_0

    .line 531
    :cond_3
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v1, :cond_5

    .line 532
    const/4 v1, 0x1

    move-object/from16 v0, p3

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 536
    :cond_4
    :goto_1
    sub-int v6, p5, p4

    sub-int v7, p7, p6

    move-object v1, p0

    move v2, p1

    move/from16 v3, p2

    move/from16 v4, p4

    move/from16 v5, p6

    move-object/from16 v8, p3

    invoke-static/range {v1 .. v8}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativePartialDecodeByteArray([BIIIIIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 538
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 540
    invoke-virtual/range {p3 .. p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getWidth()I

    move-result v13

    .line 541
    invoke-virtual/range {p3 .. p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHeight()I

    move-result v11

    .line 543
    if-nez v9, :cond_6

    .line 544
    const/4 v1, 0x0

    goto :goto_0

    .line 533
    :cond_5
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v2, 0x8

    if-le v1, v2, :cond_4

    .line 534
    const/16 v1, 0x8

    move-object/from16 v0, p3

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    goto :goto_1

    .line 546
    :cond_6
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-ge v1, v12, :cond_7

    .line 547
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v1, v13

    div-int/2addr v1, v12

    move-object/from16 v0, p3

    iget v2, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v2, v11

    div-int/2addr v2, v12

    const/4 v3, 0x0

    invoke-static {v9, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 548
    .local v10, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    .line 549
    move-object v9, v10

    .line 550
    move-object/from16 v0, p3

    iput v12, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .end local v10    # "bm":Landroid/graphics/Bitmap;
    :cond_7
    move-object v1, v9

    .line 553
    goto :goto_0
.end method

.method public static partialDecodeByteArrayToBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;IIIILcom/quramsoft/qrb/ImageBufferData;)I
    .locals 13
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p4, "left"    # I
    .param p5, "right"    # I
    .param p6, "top"    # I
    .param p7, "bottom"    # I
    .param p8, "output"    # Lcom/quramsoft/qrb/ImageBufferData;

    .prologue
    .line 564
    const/4 v11, 0x0

    .line 568
    .local v11, "ret":I
    if-gez p1, :cond_0

    .line 570
    const/4 v1, 0x0

    .line 638
    :goto_0
    return v1

    .line 573
    :cond_0
    if-gtz p2, :cond_1

    .line 575
    const/4 v1, 0x0

    goto :goto_0

    .line 578
    :cond_1
    array-length v1, p0

    add-int v2, p2, p1

    if-ge v1, v2, :cond_2

    .line 580
    const/4 v1, 0x0

    goto :goto_0

    .line 583
    :cond_2
    invoke-virtual/range {p3 .. p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    if-eqz v1, :cond_3

    .line 585
    const/4 v1, 0x0

    goto :goto_0

    .line 588
    :cond_3
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v2, 0x8

    if-gt v1, v2, :cond_4

    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-gez v1, :cond_5

    .line 590
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 593
    :cond_5
    if-nez p8, :cond_6

    .line 595
    const/4 v1, 0x0

    goto :goto_0

    .line 598
    :cond_6
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v1, :cond_8

    .line 599
    const/4 v1, 0x1

    move-object/from16 v0, p3

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 603
    :cond_7
    :goto_1
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-static {p0, v1, p2, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativeGetImageInfoFromBuffer([BIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v11

    .line 605
    invoke-virtual/range {p3 .. p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getWidth()I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, p3

    iget v2, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->round(F)I

    move-result v12

    .line 606
    .local v12, "width":I
    invoke-virtual/range {p3 .. p3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHeight()I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, p3

    iget v2, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v1}, Lcom/quramsoft/qrb/QuramBitmapFactory;->round(F)I

    move-result v10

    .line 608
    .local v10, "height":I
    if-nez v11, :cond_9

    .line 610
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 611
    const/4 v1, 0x0

    goto :goto_0

    .line 600
    .end local v10    # "height":I
    .end local v12    # "width":I
    :cond_8
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v2, 0x8

    if-le v1, v2, :cond_7

    .line 601
    const/16 v1, 0x8

    move-object/from16 v0, p3

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    goto :goto_1

    .line 614
    .restart local v10    # "height":I
    .restart local v12    # "width":I
    :cond_9
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_a

    .line 615
    mul-int v1, v12, v10

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    move-object/from16 v0, p8

    iput-object v1, v0, Lcom/quramsoft/qrb/ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    .line 626
    :goto_2
    move-object/from16 v0, p8

    iget-object v1, v0, Lcom/quramsoft/qrb/ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    sget-object v2, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 628
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setWidth(I)V

    .line 629
    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHeight(I)V

    .line 631
    move-object/from16 v0, p8

    iget-object v2, v0, Lcom/quramsoft/qrb/ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    sub-int v7, p5, p4

    sub-int v8, p7, p6

    move-object v1, p0

    move v3, p1

    move v4, p2

    move/from16 v5, p4

    move/from16 v6, p6

    move-object/from16 v9, p3

    invoke-static/range {v1 .. v9}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativePartialDecodeByteArrayToBuffer([BLjava/nio/ByteBuffer;IIIIIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)I

    move-result v11

    .line 633
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 635
    move-object/from16 v0, p8

    iget-object v1, v0, Lcom/quramsoft/qrb/ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    if-nez v1, :cond_d

    .line 636
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 616
    :cond_a
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    if-nez v1, :cond_b

    .line 617
    mul-int v1, v12, v10

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    move-object/from16 v0, p8

    iput-object v1, v0, Lcom/quramsoft/qrb/ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    goto :goto_2

    .line 618
    :cond_b
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_c

    .line 619
    mul-int v1, v12, v10

    add-int/lit8 v2, v12, 0x1

    shr-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v10, 0x1

    shr-int/lit8 v3, v3, 0x1

    mul-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    move-object/from16 v0, p8

    iput-object v1, v0, Lcom/quramsoft/qrb/ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    goto :goto_2

    .line 622
    :cond_c
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 623
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 638
    :cond_d
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public static partialDecodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;IIII)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p2, "left"    # I
    .param p3, "right"    # I
    .param p4, "top"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 1048
    const/4 v9, 0x0

    .line 1049
    .local v9, "ret":I
    const/4 v6, 0x0

    .line 1050
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    iget v10, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 1051
    .local v10, "sampleSize":I
    const/4 v11, 0x0

    .line 1052
    .local v11, "width":I
    const/4 v8, 0x0

    .line 1054
    .local v8, "height":I
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1058
    const/4 v0, 0x0

    .line 1087
    :goto_0
    return-object v0

    .line 1061
    :cond_0
    iget v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v0, :cond_2

    .line 1062
    const/4 v0, 0x1

    iput v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 1066
    :cond_1
    :goto_1
    sub-int v3, p3, p2

    sub-int v4, p5, p4

    move-object v0, p0

    move v1, p2

    move/from16 v2, p4

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrb/QuramBitmapFactory;->nativePartialDecodeFile(Ljava/lang/String;IIIILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 1068
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->setHandle(I)V

    .line 1070
    if-nez v6, :cond_3

    .line 1071
    const/4 v0, 0x0

    goto :goto_0

    .line 1063
    :cond_2
    iget v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v1, 0x8

    if-le v0, v1, :cond_1

    .line 1064
    const/16 v0, 0x8

    iput v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    goto :goto_1

    .line 1073
    :cond_3
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getWidth()I

    move-result v11

    .line 1074
    invoke-virtual {p1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->getHeight()I

    move-result v8

    .line 1076
    iget v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    if-ge v0, v10, :cond_4

    .line 1078
    sub-int v0, p3, p2

    iget v1, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v11, v0, v1

    .line 1079
    sub-int v0, p5, p4

    iget v1, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v8, v0, v1

    .line 1081
    iget v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v0, v11

    div-int/2addr v0, v10

    iget v1, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v1, v8

    div-int/2addr v1, v10

    const/4 v2, 0x0

    invoke-static {v6, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1082
    .local v7, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 1083
    move-object v6, v7

    .line 1084
    iput v10, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .end local v7    # "bm":Landroid/graphics/Bitmap;
    :cond_4
    move-object v0, v6

    .line 1087
    goto :goto_0
.end method

.method public static round(F)I
    .locals 1
    .param p0, "val"    # F

    .prologue
    .line 47
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static translateBitmapFactoryOptions(Landroid/graphics/BitmapFactory$Options;)Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .locals 4
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    const/4 v3, 0x7

    .line 1323
    new-instance v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v0}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 1324
    .local v0, "qrbOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    iget-object v1, p0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v1, v2, :cond_0

    .line 1326
    iput v3, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    .line 1337
    :goto_0
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 1338
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->access$0(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)V

    .line 1339
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v0, v1}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->access$1(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;I)V

    .line 1341
    return-object v0

    .line 1328
    :cond_0
    iget-object v1, p0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-ne v1, v2, :cond_1

    .line 1330
    const/4 v1, 0x0

    iput v1, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    goto :goto_0

    .line 1334
    :cond_1
    iput v3, v0, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    goto :goto_0
.end method
