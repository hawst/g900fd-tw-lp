.class public Lcom/samsung/recognitionengine/RecognitionEngineJNI;
.super Ljava/lang/Object;
.source "RecognitionEngineJNI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native FloatVector_add(JLcom/samsung/recognitionengine/FloatVector;F)V
.end method

.method public static final native FloatVector_capacity(JLcom/samsung/recognitionengine/FloatVector;)J
.end method

.method public static final native FloatVector_clear(JLcom/samsung/recognitionengine/FloatVector;)V
.end method

.method public static final native FloatVector_get(JLcom/samsung/recognitionengine/FloatVector;I)F
.end method

.method public static final native FloatVector_isEmpty(JLcom/samsung/recognitionengine/FloatVector;)Z
.end method

.method public static final native FloatVector_reserve(JLcom/samsung/recognitionengine/FloatVector;J)V
.end method

.method public static final native FloatVector_set(JLcom/samsung/recognitionengine/FloatVector;IF)V
.end method

.method public static final native FloatVector_size(JLcom/samsung/recognitionengine/FloatVector;)J
.end method

.method public static final native PointFVector_add(JLcom/samsung/recognitionengine/PointFVector;JLcom/samsung/recognitionengine/PointF;)V
.end method

.method public static final native PointFVector_capacity(JLcom/samsung/recognitionengine/PointFVector;)J
.end method

.method public static final native PointFVector_clear(JLcom/samsung/recognitionengine/PointFVector;)V
.end method

.method public static final native PointFVector_get(JLcom/samsung/recognitionengine/PointFVector;I)J
.end method

.method public static final native PointFVector_isEmpty(JLcom/samsung/recognitionengine/PointFVector;)Z
.end method

.method public static final native PointFVector_reserve(JLcom/samsung/recognitionengine/PointFVector;J)V
.end method

.method public static final native PointFVector_set(JLcom/samsung/recognitionengine/PointFVector;IJLcom/samsung/recognitionengine/PointF;)V
.end method

.method public static final native PointFVector_size(JLcom/samsung/recognitionengine/PointFVector;)J
.end method

.method public static final native PointF_equals(JLcom/samsung/recognitionengine/PointF;JLcom/samsung/recognitionengine/PointF;)Z
.end method

.method public static final native PointF_getX(JLcom/samsung/recognitionengine/PointF;)F
.end method

.method public static final native PointF_getY(JLcom/samsung/recognitionengine/PointF;)F
.end method

.method public static final native PointF_infinitePoint()J
.end method

.method public static final native PointF_setX(JLcom/samsung/recognitionengine/PointF;F)V
.end method

.method public static final native PointF_setY(JLcom/samsung/recognitionengine/PointF;F)V
.end method

.method public static final native PolylineSmoother_smoothPolyline(JLcom/samsung/recognitionengine/PolylineSmoother;JLcom/samsung/recognitionengine/PointFVector;)J
.end method

.method public static final native ShapeInfoVector_add(JLcom/samsung/recognitionengine/ShapeInfoVector;JLcom/samsung/recognitionengine/ShapeInfo;)V
.end method

.method public static final native ShapeInfoVector_capacity(JLcom/samsung/recognitionengine/ShapeInfoVector;)J
.end method

.method public static final native ShapeInfoVector_clear(JLcom/samsung/recognitionengine/ShapeInfoVector;)V
.end method

.method public static final native ShapeInfoVector_get(JLcom/samsung/recognitionengine/ShapeInfoVector;I)J
.end method

.method public static final native ShapeInfoVector_isEmpty(JLcom/samsung/recognitionengine/ShapeInfoVector;)Z
.end method

.method public static final native ShapeInfoVector_reserve(JLcom/samsung/recognitionengine/ShapeInfoVector;J)V
.end method

.method public static final native ShapeInfoVector_set(JLcom/samsung/recognitionengine/ShapeInfoVector;IJLcom/samsung/recognitionengine/ShapeInfo;)V
.end method

.method public static final native ShapeInfoVector_size(JLcom/samsung/recognitionengine/ShapeInfoVector;)J
.end method

.method public static final native ShapeInfo_adjustToAxis__SWIG_0(JLcom/samsung/recognitionengine/ShapeInfo;F)V
.end method

.method public static final native ShapeInfo_adjustToAxis__SWIG_1(JLcom/samsung/recognitionengine/ShapeInfo;)V
.end method

.method public static final native ShapeInfo_angle(JLcom/samsung/recognitionengine/ShapeInfo;)F
.end method

.method public static final native ShapeInfo_clone(JLcom/samsung/recognitionengine/ShapeInfo;)J
.end method

.method public static final native ShapeInfo_equals(JLcom/samsung/recognitionengine/ShapeInfo;JLcom/samsung/recognitionengine/ShapeInfo;)Z
.end method

.method public static final native ShapeInfo_generatePoints(JLcom/samsung/recognitionengine/ShapeInfo;J)J
.end method

.method public static final native ShapeInfo_getRecognizedPoints(JLcom/samsung/recognitionengine/ShapeInfo;)J
.end method

.method public static final native ShapeInfo_getRelevance(JLcom/samsung/recognitionengine/ShapeInfo;)I
.end method

.method public static final native ShapeInfo_getShapeType(JLcom/samsung/recognitionengine/ShapeInfo;)I
.end method

.method public static final native ShapeInfo_isClosedShape(JLcom/samsung/recognitionengine/ShapeInfo;)Z
.end method

.method public static final native ShapeInfo_isComplexShape(JLcom/samsung/recognitionengine/ShapeInfo;)Z
.end method

.method public static final native ShapeInfo_setAngle(JLcom/samsung/recognitionengine/ShapeInfo;FJLcom/samsung/recognitionengine/PointF;)V
.end method

.method public static final native ShapeInfo_setRecognizedPoints(JLcom/samsung/recognitionengine/ShapeInfo;JLcom/samsung/recognitionengine/PointFVector;)V
.end method

.method public static final native ShapeInfo_shapeTypeToString(JLcom/samsung/recognitionengine/ShapeInfo;)Ljava/lang/String;
.end method

.method public static final native ShapeRecognizer_getGesturesForShapeInfo(JLcom/samsung/recognitionengine/ShapeRecognizer;JLcom/samsung/recognitionengine/ShapeInfo;)J
.end method

.method public static final native ShapeRecognizer_getIndexesOfGesturesForShapeInfo(JLcom/samsung/recognitionengine/ShapeRecognizer;JLcom/samsung/recognitionengine/ShapeInfo;)J
.end method

.method public static final native ShapeRecognizer_recognize(JLcom/samsung/recognitionengine/ShapeRecognizer;JLcom/samsung/recognitionengine/VectorPointFVectors;)J
.end method

.method public static final native ShapeRecognizer_setEnabledShapes(JLcom/samsung/recognitionengine/ShapeRecognizer;JLcom/samsung/recognitionengine/ShapeTypeVector;)V
.end method

.method public static final native ShapeTypeVector_add(JLcom/samsung/recognitionengine/ShapeTypeVector;I)V
.end method

.method public static final native ShapeTypeVector_capacity(JLcom/samsung/recognitionengine/ShapeTypeVector;)J
.end method

.method public static final native ShapeTypeVector_clear(JLcom/samsung/recognitionengine/ShapeTypeVector;)V
.end method

.method public static final native ShapeTypeVector_get(JLcom/samsung/recognitionengine/ShapeTypeVector;I)I
.end method

.method public static final native ShapeTypeVector_isEmpty(JLcom/samsung/recognitionengine/ShapeTypeVector;)Z
.end method

.method public static final native ShapeTypeVector_reserve(JLcom/samsung/recognitionengine/ShapeTypeVector;J)V
.end method

.method public static final native ShapeTypeVector_set(JLcom/samsung/recognitionengine/ShapeTypeVector;II)V
.end method

.method public static final native ShapeTypeVector_size(JLcom/samsung/recognitionengine/ShapeTypeVector;)J
.end method

.method public static final native Signature_add(JLcom/samsung/recognitionengine/Signature;JLcom/samsung/recognitionengine/Stroke;)V
.end method

.method public static final native Signature_capacity(JLcom/samsung/recognitionengine/Signature;)J
.end method

.method public static final native Signature_clear(JLcom/samsung/recognitionengine/Signature;)V
.end method

.method public static final native Signature_get(JLcom/samsung/recognitionengine/Signature;I)J
.end method

.method public static final native Signature_isEmpty(JLcom/samsung/recognitionengine/Signature;)Z
.end method

.method public static final native Signature_reserve(JLcom/samsung/recognitionengine/Signature;J)V
.end method

.method public static final native Signature_set(JLcom/samsung/recognitionengine/Signature;IJLcom/samsung/recognitionengine/Stroke;)V
.end method

.method public static final native Signature_size(JLcom/samsung/recognitionengine/Signature;)J
.end method

.method public static final native SizeTVector_add(JLcom/samsung/recognitionengine/SizeTVector;J)V
.end method

.method public static final native SizeTVector_capacity(JLcom/samsung/recognitionengine/SizeTVector;)J
.end method

.method public static final native SizeTVector_clear(JLcom/samsung/recognitionengine/SizeTVector;)V
.end method

.method public static final native SizeTVector_get(JLcom/samsung/recognitionengine/SizeTVector;I)J
.end method

.method public static final native SizeTVector_isEmpty(JLcom/samsung/recognitionengine/SizeTVector;)Z
.end method

.method public static final native SizeTVector_reserve(JLcom/samsung/recognitionengine/SizeTVector;J)V
.end method

.method public static final native SizeTVector_set(JLcom/samsung/recognitionengine/SizeTVector;IJ)V
.end method

.method public static final native SizeTVector_size(JLcom/samsung/recognitionengine/SizeTVector;)J
.end method

.method public static final native Stroke_add(JLcom/samsung/recognitionengine/Stroke;JLcom/samsung/recognitionengine/TouchPoint;)V
.end method

.method public static final native Stroke_capacity(JLcom/samsung/recognitionengine/Stroke;)J
.end method

.method public static final native Stroke_clear(JLcom/samsung/recognitionengine/Stroke;)V
.end method

.method public static final native Stroke_get(JLcom/samsung/recognitionengine/Stroke;I)J
.end method

.method public static final native Stroke_isEmpty(JLcom/samsung/recognitionengine/Stroke;)Z
.end method

.method public static final native Stroke_reserve(JLcom/samsung/recognitionengine/Stroke;J)V
.end method

.method public static final native Stroke_set(JLcom/samsung/recognitionengine/Stroke;IJLcom/samsung/recognitionengine/TouchPoint;)V
.end method

.method public static final native Stroke_size(JLcom/samsung/recognitionengine/Stroke;)J
.end method

.method public static final native TouchPoint_addBatch__SWIG_0(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;DFFF)V
.end method

.method public static final native TouchPoint_addBatch__SWIG_1(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;DFF)V
.end method

.method public static final native TouchPoint_addBatch__SWIG_2(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;DF)V
.end method

.method public static final native TouchPoint_addBatch__SWIG_3(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;D)V
.end method

.method public static final native TouchPoint_addBatch__SWIG_4(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;)V
.end method

.method public static final native TouchPoint_addHistoricalPoint__SWIG_0(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;DFFF)V
.end method

.method public static final native TouchPoint_addHistoricalPoint__SWIG_1(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;DFF)V
.end method

.method public static final native TouchPoint_addHistoricalPoint__SWIG_2(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;DF)V
.end method

.method public static final native TouchPoint_addHistoricalPoint__SWIG_3(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;D)V
.end method

.method public static final native TouchPoint_addHistoricalPoint__SWIG_4(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;)V
.end method

.method public static final native TouchPoint_equals(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/TouchPoint;)Z
.end method

.method public static final native TouchPoint_getHistoricalOrientation(JLcom/samsung/recognitionengine/TouchPoint;J)F
.end method

.method public static final native TouchPoint_getHistoricalPoint(JLcom/samsung/recognitionengine/TouchPoint;J)J
.end method

.method public static final native TouchPoint_getHistoricalPressure(JLcom/samsung/recognitionengine/TouchPoint;J)F
.end method

.method public static final native TouchPoint_getHistoricalTilt(JLcom/samsung/recognitionengine/TouchPoint;J)F
.end method

.method public static final native TouchPoint_getHistoricalTimestamp(JLcom/samsung/recognitionengine/TouchPoint;J)D
.end method

.method public static final native TouchPoint_getHistoricalX(JLcom/samsung/recognitionengine/TouchPoint;J)F
.end method

.method public static final native TouchPoint_getHistoricalY(JLcom/samsung/recognitionengine/TouchPoint;J)F
.end method

.method public static final native TouchPoint_getHistorySize(JLcom/samsung/recognitionengine/TouchPoint;)J
.end method

.method public static final native TouchPoint_getOrientation(JLcom/samsung/recognitionengine/TouchPoint;)F
.end method

.method public static final native TouchPoint_getPoint(JLcom/samsung/recognitionengine/TouchPoint;)J
.end method

.method public static final native TouchPoint_getPressure(JLcom/samsung/recognitionengine/TouchPoint;)F
.end method

.method public static final native TouchPoint_getTilt(JLcom/samsung/recognitionengine/TouchPoint;)F
.end method

.method public static final native TouchPoint_getTimestamp(JLcom/samsung/recognitionengine/TouchPoint;)D
.end method

.method public static final native TouchPoint_getX(JLcom/samsung/recognitionengine/TouchPoint;)F
.end method

.method public static final native TouchPoint_getY(JLcom/samsung/recognitionengine/TouchPoint;)F
.end method

.method public static final native TouchPoint_setHistoricalPoint(JLcom/samsung/recognitionengine/TouchPoint;JJLcom/samsung/recognitionengine/PointF;)V
.end method

.method public static final native TouchPoint_setOrientation(JLcom/samsung/recognitionengine/TouchPoint;F)V
.end method

.method public static final native TouchPoint_setPoint(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;)V
.end method

.method public static final native TouchPoint_setPressure(JLcom/samsung/recognitionengine/TouchPoint;F)V
.end method

.method public static final native TouchPoint_setTilt(JLcom/samsung/recognitionengine/TouchPoint;F)V
.end method

.method public static final native TouchPoint_setTimestamp(JLcom/samsung/recognitionengine/TouchPoint;D)V
.end method

.method public static final native Trainer_addSignature(JLcom/samsung/recognitionengine/Trainer;JLcom/samsung/recognitionengine/Signature;)I
.end method

.method public static final native Trainer_getSignaturesNumber(JLcom/samsung/recognitionengine/Trainer;)J
.end method

.method public static final native Trainer_getValidateNextSignature(JLcom/samsung/recognitionengine/Trainer;)Z
.end method

.method public static final native Trainer_isSimplicityChecking(JLcom/samsung/recognitionengine/Trainer;)Z
.end method

.method public static final native Trainer_setSimplicityChecking(JLcom/samsung/recognitionengine/Trainer;Z)V
.end method

.method public static final native Trainer_setValidateNextSignature(JLcom/samsung/recognitionengine/Trainer;Z)V
.end method

.method public static final native Trainer_trainModel(JLcom/samsung/recognitionengine/Trainer;)J
.end method

.method public static final native UInt32Vector_add(JLcom/samsung/recognitionengine/UInt32Vector;J)V
.end method

.method public static final native UInt32Vector_capacity(JLcom/samsung/recognitionengine/UInt32Vector;)J
.end method

.method public static final native UInt32Vector_clear(JLcom/samsung/recognitionengine/UInt32Vector;)V
.end method

.method public static final native UInt32Vector_get(JLcom/samsung/recognitionengine/UInt32Vector;I)J
.end method

.method public static final native UInt32Vector_isEmpty(JLcom/samsung/recognitionengine/UInt32Vector;)Z
.end method

.method public static final native UInt32Vector_reserve(JLcom/samsung/recognitionengine/UInt32Vector;J)V
.end method

.method public static final native UInt32Vector_set(JLcom/samsung/recognitionengine/UInt32Vector;IJ)V
.end method

.method public static final native UInt32Vector_size(JLcom/samsung/recognitionengine/UInt32Vector;)J
.end method

.method public static final native UserModelReader_read__SWIG_0(JLcom/samsung/recognitionengine/UserModelReader;JLcom/samsung/recognitionengine/FloatVector;)Z
.end method

.method public static final native UserModelReader_read__SWIG_1(JLcom/samsung/recognitionengine/UserModelReader;JLcom/samsung/recognitionengine/UInt32Vector;)Z
.end method

.method public static final native UserModelStringReader_SWIGUpcast(J)J
.end method

.method public static final native UserModelStringWriter_SWIGUpcast(J)J
.end method

.method public static final native UserModelStringWriter_getString(JLcom/samsung/recognitionengine/UserModelStringWriter;)Ljava/lang/String;
.end method

.method public static final native UserModelWriter_write__SWIG_0(JLcom/samsung/recognitionengine/UserModelWriter;JLcom/samsung/recognitionengine/FloatVector;)Z
.end method

.method public static final native UserModelWriter_write__SWIG_1(JLcom/samsung/recognitionengine/UserModelWriter;JLcom/samsung/recognitionengine/UInt32Vector;)Z
.end method

.method public static final native UserModel_getSignaturesNumber(JLcom/samsung/recognitionengine/UserModel;)J
.end method

.method public static final native UserModel_isValid(JLcom/samsung/recognitionengine/UserModel;)Z
.end method

.method public static final native UserModel_readModel(JLcom/samsung/recognitionengine/UserModelReader;)J
.end method

.method public static final native UserModel_writeModel(JLcom/samsung/recognitionengine/UserModel;JLcom/samsung/recognitionengine/UserModelWriter;)Z
.end method

.method public static final native VectorPointFVectors_add(JLcom/samsung/recognitionengine/VectorPointFVectors;JLcom/samsung/recognitionengine/PointFVector;)V
.end method

.method public static final native VectorPointFVectors_capacity(JLcom/samsung/recognitionengine/VectorPointFVectors;)J
.end method

.method public static final native VectorPointFVectors_clear(JLcom/samsung/recognitionengine/VectorPointFVectors;)V
.end method

.method public static final native VectorPointFVectors_get(JLcom/samsung/recognitionengine/VectorPointFVectors;I)J
.end method

.method public static final native VectorPointFVectors_isEmpty(JLcom/samsung/recognitionengine/VectorPointFVectors;)Z
.end method

.method public static final native VectorPointFVectors_reserve(JLcom/samsung/recognitionengine/VectorPointFVectors;J)V
.end method

.method public static final native VectorPointFVectors_set(JLcom/samsung/recognitionengine/VectorPointFVectors;IJLcom/samsung/recognitionengine/PointFVector;)V
.end method

.method public static final native VectorPointFVectors_size(JLcom/samsung/recognitionengine/VectorPointFVectors;)J
.end method

.method public static final native VectorShapeInfoVectors_add(JLcom/samsung/recognitionengine/VectorShapeInfoVectors;JLcom/samsung/recognitionengine/ShapeInfoVector;)V
.end method

.method public static final native VectorShapeInfoVectors_capacity(JLcom/samsung/recognitionengine/VectorShapeInfoVectors;)J
.end method

.method public static final native VectorShapeInfoVectors_clear(JLcom/samsung/recognitionengine/VectorShapeInfoVectors;)V
.end method

.method public static final native VectorShapeInfoVectors_get(JLcom/samsung/recognitionengine/VectorShapeInfoVectors;I)J
.end method

.method public static final native VectorShapeInfoVectors_isEmpty(JLcom/samsung/recognitionengine/VectorShapeInfoVectors;)Z
.end method

.method public static final native VectorShapeInfoVectors_reserve(JLcom/samsung/recognitionengine/VectorShapeInfoVectors;J)V
.end method

.method public static final native VectorShapeInfoVectors_set(JLcom/samsung/recognitionengine/VectorShapeInfoVectors;IJLcom/samsung/recognitionengine/ShapeInfoVector;)V
.end method

.method public static final native VectorShapeInfoVectors_size(JLcom/samsung/recognitionengine/VectorShapeInfoVectors;)J
.end method

.method public static final native Verifier_getStrictnessLevel(JLcom/samsung/recognitionengine/Verifier;)I
.end method

.method public static final native Verifier_isAuthentic(JLcom/samsung/recognitionengine/Verifier;JLcom/samsung/recognitionengine/Signature;)Z
.end method

.method public static final native Verifier_setStrictnessLevel(JLcom/samsung/recognitionengine/Verifier;I)V
.end method

.method public static final native delete_FloatVector(J)V
.end method

.method public static final native delete_PointF(J)V
.end method

.method public static final native delete_PointFVector(J)V
.end method

.method public static final native delete_PolylineSmoother(J)V
.end method

.method public static final native delete_ShapeInfo(J)V
.end method

.method public static final native delete_ShapeInfoVector(J)V
.end method

.method public static final native delete_ShapeRecognizer(J)V
.end method

.method public static final native delete_ShapeTypeVector(J)V
.end method

.method public static final native delete_Signature(J)V
.end method

.method public static final native delete_SizeTVector(J)V
.end method

.method public static final native delete_Stroke(J)V
.end method

.method public static final native delete_TouchPoint(J)V
.end method

.method public static final native delete_Trainer(J)V
.end method

.method public static final native delete_UInt32Vector(J)V
.end method

.method public static final native delete_UserModel(J)V
.end method

.method public static final native delete_UserModelReader(J)V
.end method

.method public static final native delete_UserModelStringReader(J)V
.end method

.method public static final native delete_UserModelStringWriter(J)V
.end method

.method public static final native delete_UserModelWriter(J)V
.end method

.method public static final native delete_VectorPointFVectors(J)V
.end method

.method public static final native delete_VectorShapeInfoVectors(J)V
.end method

.method public static final native delete_Verifier(J)V
.end method

.method public static final native new_FloatVector__SWIG_0()J
.end method

.method public static final native new_FloatVector__SWIG_1(J)J
.end method

.method public static final native new_PointFVector__SWIG_0()J
.end method

.method public static final native new_PointFVector__SWIG_1(J)J
.end method

.method public static final native new_PointF__SWIG_0()J
.end method

.method public static final native new_PointF__SWIG_1(FF)J
.end method

.method public static final native new_PolylineSmoother()J
.end method

.method public static final native new_ShapeInfoVector__SWIG_0()J
.end method

.method public static final native new_ShapeInfoVector__SWIG_1(J)J
.end method

.method public static final native new_ShapeInfo__SWIG_0()J
.end method

.method public static final native new_ShapeInfo__SWIG_1(IJLcom/samsung/recognitionengine/PointFVector;I)J
.end method

.method public static final native new_ShapeInfo__SWIG_2(IJLcom/samsung/recognitionengine/PointFVector;)J
.end method

.method public static final native new_ShapeInfo__SWIG_3(IJLcom/samsung/recognitionengine/PointFVector;FI)J
.end method

.method public static final native new_ShapeInfo__SWIG_4(IJLcom/samsung/recognitionengine/PointFVector;F)J
.end method

.method public static final native new_ShapeInfo__SWIG_5(IJI)J
.end method

.method public static final native new_ShapeInfo__SWIG_6(IJ)J
.end method

.method public static final native new_ShapeInfo__SWIG_7(JLcom/samsung/recognitionengine/ShapeInfo;)J
.end method

.method public static final native new_ShapeRecognizer__SWIG_0()J
.end method

.method public static final native new_ShapeRecognizer__SWIG_1(JLcom/samsung/recognitionengine/ShapeTypeVector;)J
.end method

.method public static final native new_ShapeTypeVector__SWIG_0()J
.end method

.method public static final native new_ShapeTypeVector__SWIG_1(J)J
.end method

.method public static final native new_Signature__SWIG_0()J
.end method

.method public static final native new_Signature__SWIG_1(J)J
.end method

.method public static final native new_SizeTVector__SWIG_0()J
.end method

.method public static final native new_SizeTVector__SWIG_1(J)J
.end method

.method public static final native new_Stroke__SWIG_0()J
.end method

.method public static final native new_Stroke__SWIG_1(J)J
.end method

.method public static final native new_TouchPoint__SWIG_0()J
.end method

.method public static final native new_TouchPoint__SWIG_1(JLcom/samsung/recognitionengine/PointF;DF)J
.end method

.method public static final native new_TouchPoint__SWIG_2(JLcom/samsung/recognitionengine/PointF;D)J
.end method

.method public static final native new_TouchPoint__SWIG_3(JLcom/samsung/recognitionengine/PointF;)J
.end method

.method public static final native new_TouchPoint__SWIG_4(FFDF)J
.end method

.method public static final native new_TouchPoint__SWIG_5(FFD)J
.end method

.method public static final native new_TouchPoint__SWIG_6(FF)J
.end method

.method public static final native new_Trainer()J
.end method

.method public static final native new_UInt32Vector__SWIG_0()J
.end method

.method public static final native new_UInt32Vector__SWIG_1(J)J
.end method

.method public static final native new_UserModelStringReader(Ljava/lang/String;)J
.end method

.method public static final native new_UserModelStringWriter()J
.end method

.method public static final native new_UserModel__SWIG_0()J
.end method

.method public static final native new_UserModel__SWIG_1(JLcom/samsung/recognitionengine/UserModel;)J
.end method

.method public static final native new_VectorPointFVectors__SWIG_0()J
.end method

.method public static final native new_VectorPointFVectors__SWIG_1(J)J
.end method

.method public static final native new_VectorShapeInfoVectors__SWIG_0()J
.end method

.method public static final native new_VectorShapeInfoVectors__SWIG_1(J)J
.end method

.method public static final native new_Verifier(JLcom/samsung/recognitionengine/UserModel;)J
.end method
