.class public Lcom/samsung/recognitionengine/UserModelStringReader;
.super Lcom/samsung/recognitionengine/UserModelReader;
.source "UserModelStringReader.java"


# instance fields
.field private swigCPtr:J


# direct methods
.method protected constructor <init>(JZ)V
    .locals 3

    .prologue
    .line 15
    invoke-static {p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->UserModelStringReader_SWIGUpcast(J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p3}, Lcom/samsung/recognitionengine/UserModelReader;-><init>(JZ)V

    .line 16
    iput-wide p1, p0, Lcom/samsung/recognitionengine/UserModelStringReader;->swigCPtr:J

    .line 17
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 39
    invoke-static {p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_UserModelStringReader(Ljava/lang/String;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/UserModelStringReader;-><init>(JZ)V

    .line 40
    return-void
.end method

.method protected static getCPtr(Lcom/samsung/recognitionengine/UserModelStringReader;)J
    .locals 2

    .prologue
    .line 20
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModelStringReader;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 28
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModelStringReader;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 29
    iget-boolean v0, p0, Lcom/samsung/recognitionengine/UserModelStringReader;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/recognitionengine/UserModelStringReader;->swigCMemOwn:Z

    .line 31
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModelStringReader;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->delete_UserModelStringReader(J)V

    .line 33
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/recognitionengine/UserModelStringReader;->swigCPtr:J

    .line 35
    :cond_1
    invoke-super {p0}, Lcom/samsung/recognitionengine/UserModelReader;->delete()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    monitor-exit p0

    return-void

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/samsung/recognitionengine/UserModelStringReader;->delete()V

    .line 25
    return-void
.end method
