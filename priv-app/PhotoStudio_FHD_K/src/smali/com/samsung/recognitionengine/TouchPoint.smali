.class public Lcom/samsung/recognitionengine/TouchPoint;
.super Ljava/lang/Object;
.source "TouchPoint.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_TouchPoint__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/TouchPoint;-><init>(JZ)V

    .line 40
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 3

    .prologue
    .line 63
    invoke-static {p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_TouchPoint__SWIG_6(FF)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/TouchPoint;-><init>(JZ)V

    .line 64
    return-void
.end method

.method public constructor <init>(FFD)V
    .locals 3

    .prologue
    .line 59
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_TouchPoint__SWIG_5(FFD)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/TouchPoint;-><init>(JZ)V

    .line 60
    return-void
.end method

.method public constructor <init>(FFDF)V
    .locals 3

    .prologue
    .line 55
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_TouchPoint__SWIG_4(FFDF)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/TouchPoint;-><init>(JZ)V

    .line 56
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/PointF;)V
    .locals 3

    .prologue
    .line 51
    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_TouchPoint__SWIG_3(JLcom/samsung/recognitionengine/PointF;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/TouchPoint;-><init>(JZ)V

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/PointF;D)V
    .locals 4

    .prologue
    .line 47
    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2, p3}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_TouchPoint__SWIG_2(JLcom/samsung/recognitionengine/PointF;D)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/TouchPoint;-><init>(JZ)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/PointF;DF)V
    .locals 6

    .prologue
    .line 43
    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v0

    move-object v2, p1

    move-wide v3, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_TouchPoint__SWIG_1(JLcom/samsung/recognitionengine/PointF;DF)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/TouchPoint;-><init>(JZ)V

    .line 44
    return-void
.end method

.method protected static getCPtr(Lcom/samsung/recognitionengine/TouchPoint;)J
    .locals 2

    .prologue
    .line 21
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public addBatch(Lcom/samsung/recognitionengine/PointF;)V
    .locals 6

    .prologue
    .line 135
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_addBatch__SWIG_4(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;)V

    .line 136
    return-void
.end method

.method public addBatch(Lcom/samsung/recognitionengine/PointF;D)V
    .locals 8

    .prologue
    .line 131
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    invoke-static/range {v0 .. v7}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_addBatch__SWIG_3(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;D)V

    .line 132
    return-void
.end method

.method public addBatch(Lcom/samsung/recognitionengine/PointF;DF)V
    .locals 10

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    move v8, p4

    invoke-static/range {v0 .. v8}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_addBatch__SWIG_2(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;DF)V

    .line 128
    return-void
.end method

.method public addBatch(Lcom/samsung/recognitionengine/PointF;DFF)V
    .locals 10

    .prologue
    .line 123
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    move v8, p4

    move v9, p5

    invoke-static/range {v0 .. v9}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_addBatch__SWIG_1(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;DFF)V

    .line 124
    return-void
.end method

.method public addBatch(Lcom/samsung/recognitionengine/PointF;DFFF)V
    .locals 12

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    move/from16 v8, p4

    move/from16 v9, p5

    move/from16 v10, p6

    invoke-static/range {v0 .. v10}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_addBatch__SWIG_0(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;DFFF)V

    .line 120
    return-void
.end method

.method public addHistoricalPoint(Lcom/samsung/recognitionengine/PointF;)V
    .locals 6

    .prologue
    .line 155
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_addHistoricalPoint__SWIG_4(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;)V

    .line 156
    return-void
.end method

.method public addHistoricalPoint(Lcom/samsung/recognitionengine/PointF;D)V
    .locals 8

    .prologue
    .line 151
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    invoke-static/range {v0 .. v7}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_addHistoricalPoint__SWIG_3(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;D)V

    .line 152
    return-void
.end method

.method public addHistoricalPoint(Lcom/samsung/recognitionengine/PointF;DF)V
    .locals 10

    .prologue
    .line 147
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    move v8, p4

    invoke-static/range {v0 .. v8}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_addHistoricalPoint__SWIG_2(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;DF)V

    .line 148
    return-void
.end method

.method public addHistoricalPoint(Lcom/samsung/recognitionengine/PointF;DFF)V
    .locals 10

    .prologue
    .line 143
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    move v8, p4

    move v9, p5

    invoke-static/range {v0 .. v9}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_addHistoricalPoint__SWIG_1(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;DFF)V

    .line 144
    return-void
.end method

.method public addHistoricalPoint(Lcom/samsung/recognitionengine/PointF;DFFF)V
    .locals 12

    .prologue
    .line 139
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    move/from16 v8, p4

    move/from16 v9, p5

    move/from16 v10, p6

    invoke-static/range {v0 .. v10}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_addHistoricalPoint__SWIG_0(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;DFFF)V

    .line 140
    return-void
.end method

.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->delete_TouchPoint(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public equals(Lcom/samsung/recognitionengine/TouchPoint;)Z
    .locals 6

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/TouchPoint;->getCPtr(Lcom/samsung/recognitionengine/TouchPoint;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_equals(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/TouchPoint;)Z

    move-result v0

    return v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/recognitionengine/TouchPoint;->delete()V

    .line 26
    return-void
.end method

.method public getHistoricalOrientation(J)F
    .locals 3

    .prologue
    .line 187
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getHistoricalOrientation(JLcom/samsung/recognitionengine/TouchPoint;J)F

    move-result v0

    return v0
.end method

.method public getHistoricalPoint(J)Lcom/samsung/recognitionengine/PointF;
    .locals 5

    .prologue
    .line 163
    new-instance v0, Lcom/samsung/recognitionengine/PointF;

    iget-wide v2, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v2, v3, p0, p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getHistoricalPoint(JLcom/samsung/recognitionengine/TouchPoint;J)J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/recognitionengine/PointF;-><init>(JZ)V

    return-object v0
.end method

.method public getHistoricalPressure(J)F
    .locals 3

    .prologue
    .line 179
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getHistoricalPressure(JLcom/samsung/recognitionengine/TouchPoint;J)F

    move-result v0

    return v0
.end method

.method public getHistoricalTilt(J)F
    .locals 3

    .prologue
    .line 183
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getHistoricalTilt(JLcom/samsung/recognitionengine/TouchPoint;J)F

    move-result v0

    return v0
.end method

.method public getHistoricalTimestamp(J)D
    .locals 3

    .prologue
    .line 175
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getHistoricalTimestamp(JLcom/samsung/recognitionengine/TouchPoint;J)D

    move-result-wide v0

    return-wide v0
.end method

.method public getHistoricalX(J)F
    .locals 3

    .prologue
    .line 167
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getHistoricalX(JLcom/samsung/recognitionengine/TouchPoint;J)F

    move-result v0

    return v0
.end method

.method public getHistoricalY(J)F
    .locals 3

    .prologue
    .line 171
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getHistoricalY(JLcom/samsung/recognitionengine/TouchPoint;J)F

    move-result v0

    return v0
.end method

.method public getHistorySize()J
    .locals 2

    .prologue
    .line 159
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getHistorySize(JLcom/samsung/recognitionengine/TouchPoint;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getOrientation()F
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getOrientation(JLcom/samsung/recognitionengine/TouchPoint;)F

    move-result v0

    return v0
.end method

.method public getPoint()Lcom/samsung/recognitionengine/PointF;
    .locals 4

    .prologue
    .line 79
    new-instance v0, Lcom/samsung/recognitionengine/PointF;

    iget-wide v2, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getPoint(JLcom/samsung/recognitionengine/TouchPoint;)J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/recognitionengine/PointF;-><init>(JZ)V

    return-object v0
.end method

.method public getPressure()F
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getPressure(JLcom/samsung/recognitionengine/TouchPoint;)F

    move-result v0

    return v0
.end method

.method public getTilt()F
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getTilt(JLcom/samsung/recognitionengine/TouchPoint;)F

    move-result v0

    return v0
.end method

.method public getTimestamp()D
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getTimestamp(JLcom/samsung/recognitionengine/TouchPoint;)D

    move-result-wide v0

    return-wide v0
.end method

.method public getX()F
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getX(JLcom/samsung/recognitionengine/TouchPoint;)F

    move-result v0

    return v0
.end method

.method public getY()F
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_getY(JLcom/samsung/recognitionengine/TouchPoint;)F

    move-result v0

    return v0
.end method

.method public setHistoricalPoint(JLcom/samsung/recognitionengine/PointF;)V
    .locals 9

    .prologue
    .line 191
    iget-wide v1, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {p3}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v6

    move-object v3, p0

    move-wide v4, p1

    move-object v8, p3

    invoke-static/range {v1 .. v8}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_setHistoricalPoint(JLcom/samsung/recognitionengine/TouchPoint;JJLcom/samsung/recognitionengine/PointF;)V

    .line 192
    return-void
.end method

.method public setOrientation(F)V
    .locals 2

    .prologue
    .line 115
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_setOrientation(JLcom/samsung/recognitionengine/TouchPoint;F)V

    .line 116
    return-void
.end method

.method public setPoint(Lcom/samsung/recognitionengine/PointF;)V
    .locals 6

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_setPoint(JLcom/samsung/recognitionengine/TouchPoint;JLcom/samsung/recognitionengine/PointF;)V

    .line 100
    return-void
.end method

.method public setPressure(F)V
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_setPressure(JLcom/samsung/recognitionengine/TouchPoint;F)V

    .line 108
    return-void
.end method

.method public setTilt(F)V
    .locals 2

    .prologue
    .line 111
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_setTilt(JLcom/samsung/recognitionengine/TouchPoint;F)V

    .line 112
    return-void
.end method

.method public setTimestamp(D)V
    .locals 3

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/samsung/recognitionengine/TouchPoint;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->TouchPoint_setTimestamp(JLcom/samsung/recognitionengine/TouchPoint;D)V

    .line 104
    return-void
.end method
