.class public Lcom/samsung/vip/engine/VIRecogMode;
.super Ljava/lang/Object;
.source "VIRecogMode.java"


# static fields
.field private static final LANG_LIST_ID:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 8
    const/16 v0, 0x14

    new-array v0, v0, [[I

    const/4 v1, 0x0

    .line 9
    new-array v2, v3, [I

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 10
    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    .line 11
    new-array v1, v3, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v3

    const/4 v1, 0x3

    .line 12
    new-array v2, v3, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 13
    new-array v2, v3, [I

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 14
    new-array v2, v3, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 15
    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 16
    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 17
    new-array v2, v3, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 18
    new-array v2, v3, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 20
    new-array v2, v3, [I

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 21
    new-array v2, v3, [I

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 22
    new-array v2, v3, [I

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 23
    new-array v2, v3, [I

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 24
    new-array v2, v3, [I

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 25
    new-array v2, v3, [I

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 27
    new-array v2, v3, [I

    fill-array-data v2, :array_f

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 28
    new-array v2, v3, [I

    fill-array-data v2, :array_10

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 30
    new-array v2, v3, [I

    fill-array-data v2, :array_11

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 31
    new-array v2, v3, [I

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    .line 8
    sput-object v0, Lcom/samsung/vip/engine/VIRecogMode;->LANG_LIST_ID:[[I

    .line 32
    return-void

    .line 10
    :array_0
    .array-data 4
        0x1
        0x1
    .end array-data

    .line 11
    :array_1
    .array-data 4
        0x2
        0x2
    .end array-data

    .line 12
    :array_2
    .array-data 4
        0x3
        0xa
    .end array-data

    .line 13
    :array_3
    .array-data 4
        0x4
        0xb
    .end array-data

    .line 14
    :array_4
    .array-data 4
        0x5
        0xc
    .end array-data

    .line 15
    :array_5
    .array-data 4
        0x6
        0xd
    .end array-data

    .line 16
    :array_6
    .array-data 4
        0x7
        0xe
    .end array-data

    .line 17
    :array_7
    .array-data 4
        0x8
        0xf
    .end array-data

    .line 18
    :array_8
    .array-data 4
        0x9
        0x10
    .end array-data

    .line 20
    :array_9
    .array-data 4
        0x11
        0x11
    .end array-data

    .line 21
    :array_a
    .array-data 4
        0x12
        0x12
    .end array-data

    .line 22
    :array_b
    .array-data 4
        0x13
        0x13
    .end array-data

    .line 23
    :array_c
    .array-data 4
        0x14
        0x14
    .end array-data

    .line 24
    :array_d
    .array-data 4
        0x15
        0x15
    .end array-data

    .line 25
    :array_e
    .array-data 4
        0x16
        0x16
    .end array-data

    .line 27
    :array_f
    .array-data 4
        0x17
        0x19
    .end array-data

    .line 28
    :array_10
    .array-data 4
        0x18
        0x1a
    .end array-data

    .line 30
    :array_11
    .array-data 4
        0x1c
        0x1c
    .end array-data

    .line 31
    :array_12
    .array-data 4
        0x1d
        0x1d
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLanguageMode(IZ)I
    .locals 4
    .param p0, "lang"    # I
    .param p1, "bFreeStyle"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 51
    move v1, p0

    .line 52
    .local v1, "nLang":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v3, Lcom/samsung/vip/engine/VIRecogMode;->LANG_LIST_ID:[[I

    array-length v3, v3

    if-lt v0, v3, :cond_0

    .line 58
    :goto_1
    return v1

    .line 53
    :cond_0
    sget-object v3, Lcom/samsung/vip/engine/VIRecogMode;->LANG_LIST_ID:[[I

    aget-object v3, v3, v0

    aget v3, v3, v2

    if-ne v1, v3, :cond_2

    .line 54
    sget-object v3, Lcom/samsung/vip/engine/VIRecogMode;->LANG_LIST_ID:[[I

    aget-object v3, v3, v0

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    :cond_1
    aget v1, v3, v2

    .line 55
    goto :goto_1

    .line 52
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getRangeMode(ZZZ)S
    .locals 2
    .param p0, "bNumeric"    # Z
    .param p1, "bGesture"    # Z
    .param p2, "bExtendSymbol"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 36
    const/4 v0, 0x0

    .line 37
    .local v0, "sRet":S
    if-eqz p0, :cond_0

    .line 38
    const/4 v0, 0x1

    .line 40
    :cond_0
    if-eqz p1, :cond_1

    .line 41
    add-int/lit8 v1, v0, 0x2

    int-to-short v0, v1

    .line 43
    :cond_1
    if-eqz p2, :cond_2

    .line 44
    add-int/lit8 v1, v0, 0x4

    int-to-short v0, v1

    .line 46
    :cond_2
    return v0
.end method
