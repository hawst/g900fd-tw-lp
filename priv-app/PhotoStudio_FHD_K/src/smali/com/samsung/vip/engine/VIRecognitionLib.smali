.class public Lcom/samsung/vip/engine/VIRecognitionLib;
.super Ljava/lang/Object;
.source "VIRecognitionLib.java"


# static fields
.field public static final LIBNAME:Ljava/lang/String; = "VIRecognition"

.field private static final TAG:Ljava/lang/String;

.field public static final VERSION:Ljava/lang/String; = "0.8.2"

.field public static final VI_EQ_ENGINE_RAM_SIZE:I = 0x4b000

.field public static final VI_EQ_MAX_GROUP_NUM:I = 0x80

.field public static final VI_SH_ENGINE_RAM_SIZE:I = 0xa00000


# instance fields
.field protected mXstrokeList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<[F>;"
        }
    .end annotation
.end field

.field protected mYstrokeList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<[F>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/samsung/vip/engine/VIRecognitionLib;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/vip/engine/VIRecognitionLib;->TAG:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/samsung/vip/engine/VIRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    .line 15
    iput-object v0, p0, Lcom/samsung/vip/engine/VIRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    .line 18
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/vip/engine/VIRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    .line 19
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/vip/engine/VIRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    .line 20
    return-void
.end method


# virtual methods
.method protected native VIEQ_Close()V
.end method

.method protected native VIEQ_GetCharInfo()[Lcom/samsung/vip/engine/VIEqCharInfo;
.end method

.method protected native VIEQ_Init(Ljava/lang/String;III)I
.end method

.method protected native VIEQ_InitWithScreenInfo(Ljava/lang/String;IIII)I
.end method

.method protected native VIEQ_Recog([II)Ljava/lang/String;
.end method

.method protected native VISH_ClearScene()V
.end method

.method protected native VISH_ClearSelected()V
.end method

.method protected native VISH_DeObject()V
.end method

.method protected native VISH_DeleteChoosenPrimitive()S
.end method

.method protected native VISH_GetGraphPrimitiveArray()[Lcom/samsung/vip/engine/shape/GraphPrimitive;
.end method

.method protected native VISH_GetLastShapeType()I
.end method

.method protected native VISH_GetLastStrokeBreakPoints()[I
.end method

.method protected native VISH_InitSmartShapeEngine(I)V
.end method

.method protected native VISH_InitSmartShapeEngineWithData(ILjava/lang/String;)V
.end method

.method protected native VISH_JoinSelectedObject()V
.end method

.method protected native VISH_PrimitiveRotate([I[I)I
.end method

.method protected native VISH_PrimitiveTranslate([F)I
.end method

.method protected native VISH_PrimitiveZoom([I[I)I
.end method

.method protected native VISH_ReleaseSmartShapeEngine()V
.end method

.method protected native VISH_SearchPrimitive(I[I)S
.end method

.method protected native VISH_SetDeviceDPI(I)V
.end method

.method protected native VISH_UpdateMovePrimitivesData([F)V
.end method

.method protected native VISH_UpdateScene([I)V
.end method

.method public loadLibrary(Ljava/lang/String;)V
    .locals 0
    .param p1, "pathName"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-static {p1}, Ljava/lang/System;->load(Ljava/lang/String;)V

    .line 24
    return-void
.end method
