.class public Lcom/samsung/vip/engine/VIEquationRecognitionLib;
.super Lcom/samsung/vip/engine/VIRecognitionLib;
.source "VIEquationRecognitionLib.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/samsung/vip/engine/VIRecognitionLib;-><init>()V

    .line 11
    return-void
.end method

.method private getRecogResultCandidate(Ljava/lang/String;)[Ljava/lang/String;
    .locals 8
    .param p1, "rawRet"    # Ljava/lang/String;

    .prologue
    const v7, 0xffff

    .line 83
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 84
    .local v4, "nLen":I
    const/4 v3, 0x0

    .line 85
    .local v3, "nCandNum":I
    const/4 v1, 0x0

    .local v1, "ii":I
    :goto_0
    if-lt v1, v4, :cond_1

    .line 92
    if-nez v3, :cond_3

    .line 93
    const/4 v0, 0x0

    .line 107
    :cond_0
    return-object v0

    .line 86
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v7, :cond_2

    .line 87
    add-int/lit8 v3, v3, 0x1

    .line 85
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    :cond_3
    new-array v0, v3, [Ljava/lang/String;

    .line 97
    .local v0, "arr":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 98
    .local v5, "startId":I
    const/4 v2, 0x0

    .line 99
    .local v2, "jj":I
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_0

    .line 100
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v7, :cond_4

    .line 101
    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v2

    .line 102
    add-int/lit8 v2, v2, 0x1

    .line 103
    add-int/lit8 v5, v1, 0x1

    .line 99
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public addStroke([F[F)V
    .locals 1
    .param p1, "x"    # [F
    .param p2, "y"    # [F

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 72
    iget-object v0, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 73
    return-void
.end method

.method public clearStrokes()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 77
    iget-object v0, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 78
    return-void
.end method

.method public close()V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->VIEQ_Close()V

    .line 23
    return-void
.end method

.method public init(Ljava/lang/String;)I
    .locals 3
    .param p1, "dataPath"    # Ljava/lang/String;

    .prologue
    .line 14
    const v0, 0x4b000

    const/16 v1, 0x640

    const/16 v2, 0x4b0

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->VIEQ_Init(Ljava/lang/String;III)I

    move-result v0

    return v0
.end method

.method public init(Ljava/lang/String;III)I
    .locals 6
    .param p1, "dataPath"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "dpi"    # I

    .prologue
    .line 18
    const v2, 0x4b000

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->VIEQ_InitWithScreenInfo(Ljava/lang/String;IIII)I

    move-result v0

    return v0
.end method

.method public recog()Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v12, 0x0

    const v11, 0xffff

    .line 26
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v6

    .line 27
    .local v6, "nStrokeSize":I
    const/4 v7, 0x0

    .line 28
    .local v7, "nTotalPointSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v6, :cond_0

    .line 38
    add-int/lit8 v7, v7, 0x1

    .line 40
    const/4 v3, 0x0

    .line 41
    .local v3, "nPointIndex":I
    mul-int/lit8 v10, v7, 0x2

    new-array v8, v10, [I

    .line 42
    .local v8, "pPointData":[I
    const/4 v1, 0x0

    move v4, v3

    .end local v3    # "nPointIndex":I
    .local v4, "nPointIndex":I
    :goto_1
    if-lt v1, v6, :cond_1

    .line 51
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    aput v11, v8, v4

    .line 52
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nPointIndex":I
    .restart local v4    # "nPointIndex":I
    aput v11, v8, v3

    .line 54
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->clear()V

    .line 55
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->clear()V

    .line 57
    invoke-virtual {p0, v8, v7}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->VIEQ_Recog([II)Ljava/lang/String;

    move-result-object v9

    .line 58
    .local v9, "result":Ljava/lang/String;
    if-nez v9, :cond_3

    .line 59
    sget-object v10, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->TAG:Ljava/lang/String;

    const-string v11, "Output result is null!"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    const/4 v10, 0x0

    .line 67
    :goto_2
    return-object v10

    .line 29
    .end local v4    # "nPointIndex":I
    .end local v8    # "pPointData":[I
    .end local v9    # "result":Ljava/lang/String;
    :cond_0
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [F

    array-length v5, v10

    .line 36
    .local v5, "nPointSize":I
    add-int/lit8 v10, v5, 0x1

    add-int/2addr v7, v10

    .line 28
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 43
    .end local v5    # "nPointSize":I
    .restart local v4    # "nPointIndex":I
    .restart local v8    # "pPointData":[I
    :cond_1
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [F

    array-length v5, v10

    .line 44
    .restart local v5    # "nPointSize":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_3
    if-lt v2, v5, :cond_2

    .line 48
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    aput v11, v8, v4

    .line 49
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nPointIndex":I
    .restart local v4    # "nPointIndex":I
    aput v12, v8, v3

    .line 42
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 45
    :cond_2
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [F

    aget v10, v10, v2

    float-to-int v10, v10

    aput v10, v8, v4

    .line 46
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nPointIndex":I
    .restart local v4    # "nPointIndex":I
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [F

    aget v10, v10, v2

    float-to-int v10, v10

    aput v10, v8, v3

    .line 44
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 63
    .end local v2    # "j":I
    .end local v5    # "nPointSize":I
    .restart local v9    # "result":Ljava/lang/String;
    :cond_3
    invoke-direct {p0, v9}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->getRecogResultCandidate(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "candList":[Ljava/lang/String;
    if-eqz v0, :cond_4

    array-length v10, v0

    if-lez v10, :cond_4

    .line 65
    aget-object v10, v0, v12

    goto :goto_2

    .line 67
    :cond_4
    const-string v10, ""

    goto :goto_2
.end method
