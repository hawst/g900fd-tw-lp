.class public Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;
.super Ljava/lang/Object;
.source "PrimitiveEllipseArc.java"


# instance fields
.field public Center:Landroid/graphics/Point;

.field public Direction:I

.field public cosTheta:F

.field public end:Landroid/graphics/Point;

.field public longAxis:I

.field public shortAxis:I

.field public sinTheta:F

.field public start:Landroid/graphics/Point;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCenter()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->Center:Landroid/graphics/Point;

    return-object v0
.end method

.method public getCosTheta()F
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->cosTheta:F

    return v0
.end method

.method public getDirection()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->Direction:I

    return v0
.end method

.method public getEnd()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->end:Landroid/graphics/Point;

    return-object v0
.end method

.method public getLongAxis()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->longAxis:I

    return v0
.end method

.method public getShortAxis()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->shortAxis:I

    return v0
.end method

.method public getSinTheta()F
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->sinTheta:F

    return v0
.end method

.method public getStart()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->start:Landroid/graphics/Point;

    return-object v0
.end method

.method public setCenter(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "center"    # Landroid/graphics/Point;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->Center:Landroid/graphics/Point;

    .line 35
    return-void
.end method

.method public setCosTheta(F)V
    .locals 0
    .param p1, "cosTheta"    # F

    .prologue
    .line 74
    iput p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->cosTheta:F

    .line 75
    return-void
.end method

.method public setDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->Direction:I

    .line 27
    return-void
.end method

.method public setEnd(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "end"    # Landroid/graphics/Point;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->end:Landroid/graphics/Point;

    .line 51
    return-void
.end method

.method public setLongAxis(I)V
    .locals 0
    .param p1, "longAxis"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->longAxis:I

    .line 59
    return-void
.end method

.method public setShortAxis(I)V
    .locals 0
    .param p1, "shortAxis"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->shortAxis:I

    .line 67
    return-void
.end method

.method public setSinTheta(F)V
    .locals 0
    .param p1, "sinTheta"    # F

    .prologue
    .line 82
    iput p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->sinTheta:F

    .line 83
    return-void
.end method

.method public setStart(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "start"    # Landroid/graphics/Point;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->start:Landroid/graphics/Point;

    .line 43
    return-void
.end method
