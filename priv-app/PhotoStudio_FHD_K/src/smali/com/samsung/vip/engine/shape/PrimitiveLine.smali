.class public Lcom/samsung/vip/engine/shape/PrimitiveLine;
.super Ljava/lang/Object;
.source "PrimitiveLine.java"


# instance fields
.field public end:Landroid/graphics/Point;

.field public start:Landroid/graphics/Point;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getEnd()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveLine;->end:Landroid/graphics/Point;

    return-object v0
.end method

.method public getStart()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveLine;->start:Landroid/graphics/Point;

    return-object v0
.end method

.method public isEqual(Lcom/samsung/vip/engine/shape/PrimitiveLine;Z)Z
    .locals 4
    .param p1, "line"    # Lcom/samsung/vip/engine/shape/PrimitiveLine;
    .param p2, "bIgnoreDirection"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 27
    if-eqz p2, :cond_3

    .line 28
    invoke-virtual {p0}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getStart()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getStart()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getEnd()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getEnd()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 29
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getStart()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getEnd()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getEnd()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getStart()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 31
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 28
    goto :goto_0

    .line 31
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getStart()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getStart()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getEnd()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getEnd()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public setEnd(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "end"    # Landroid/graphics/Point;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveLine;->end:Landroid/graphics/Point;

    .line 25
    return-void
.end method

.method public setStart(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "start"    # Landroid/graphics/Point;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveLine;->start:Landroid/graphics/Point;

    .line 19
    return-void
.end method
