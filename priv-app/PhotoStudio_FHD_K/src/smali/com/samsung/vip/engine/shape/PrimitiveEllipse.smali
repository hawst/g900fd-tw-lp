.class public Lcom/samsung/vip/engine/shape/PrimitiveEllipse;
.super Ljava/lang/Object;
.source "PrimitiveEllipse.java"


# instance fields
.field public Center:Landroid/graphics/Point;

.field public CosTheta:F

.field public LongAxis:I

.field public ShortAxis:I

.field public SinTheta:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCenter()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->Center:Landroid/graphics/Point;

    return-object v0
.end method

.method public getCosTheta()F
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->CosTheta:F

    return v0
.end method

.method public getLongAxis()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->LongAxis:I

    return v0
.end method

.method public getShortAxis()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->ShortAxis:I

    return v0
.end method

.method public getSinTheta()F
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->SinTheta:F

    return v0
.end method

.method public setCenter(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "center"    # Landroid/graphics/Point;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->Center:Landroid/graphics/Point;

    .line 23
    return-void
.end method

.method public setCosTheta(F)V
    .locals 0
    .param p1, "cosTheta"    # F

    .prologue
    .line 46
    iput p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->CosTheta:F

    .line 47
    return-void
.end method

.method public setLongAxis(I)V
    .locals 0
    .param p1, "longAxis"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->LongAxis:I

    .line 31
    return-void
.end method

.method public setShortAxis(I)V
    .locals 0
    .param p1, "shortAxis"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->ShortAxis:I

    .line 39
    return-void
.end method

.method public setSinTheta(F)V
    .locals 0
    .param p1, "sinTheta"    # F

    .prologue
    .line 54
    iput p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->SinTheta:F

    .line 55
    return-void
.end method
