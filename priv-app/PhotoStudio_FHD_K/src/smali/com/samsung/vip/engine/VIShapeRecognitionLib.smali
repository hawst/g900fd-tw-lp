.class public Lcom/samsung/vip/engine/VIShapeRecognitionLib;
.super Lcom/samsung/vip/engine/VIRecognitionLib;
.source "VIShapeRecognitionLib.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/samsung/vip/engine/VIRecognitionLib;-><init>()V

    .line 15
    return-void
.end method


# virtual methods
.method public addStroke([F[F)V
    .locals 1
    .param p1, "x"    # [F
    .param p2, "y"    # [F

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 73
    iget-object v0, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method public clearScene()V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_ClearScene()V

    .line 33
    return-void
.end method

.method public close()V
    .locals 0

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_ReleaseSmartShapeEngine()V

    .line 28
    return-void
.end method

.method public getPrimitiveName(I)Ljava/lang/String;
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 77
    packed-switch p1, :pswitch_data_0

    .line 101
    :pswitch_0
    const-string v0, ""

    :goto_0
    return-object v0

    .line 79
    :pswitch_1
    const-string v0, "line"

    goto :goto_0

    .line 81
    :pswitch_2
    const-string v0, "arrow"

    goto :goto_0

    .line 83
    :pswitch_3
    const-string v0, "bezier"

    goto :goto_0

    .line 85
    :pswitch_4
    const-string v0, "circle"

    goto :goto_0

    .line 87
    :pswitch_5
    const-string v0, "circlearc"

    goto :goto_0

    .line 89
    :pswitch_6
    const-string v0, "ellipse"

    goto :goto_0

    .line 91
    :pswitch_7
    const-string v0, "polygon"

    goto :goto_0

    .line 93
    :pswitch_8
    const-string v0, "ellipsearc"

    goto :goto_0

    .line 95
    :pswitch_9
    const-string v0, "table"

    goto :goto_0

    .line 97
    :pswitch_a
    const-string v0, "group"

    goto :goto_0

    .line 99
    :pswitch_b
    const-string v0, "polyline"

    goto :goto_0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_8
        :pswitch_3
        :pswitch_a
        :pswitch_2
        :pswitch_9
        :pswitch_b
    .end packed-switch
.end method

.method public init()V
    .locals 1

    .prologue
    .line 18
    const/high16 v0, 0xa00000

    invoke-virtual {p0, v0}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_InitSmartShapeEngine(I)V

    .line 19
    return-void
.end method

.method public init(Ljava/lang/String;)V
    .locals 1
    .param p1, "dataPath"    # Ljava/lang/String;

    .prologue
    .line 22
    const/high16 v0, 0xa00000

    invoke-virtual {p0, v0, p1}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_InitSmartShapeEngineWithData(ILjava/lang/String;)V

    .line 23
    return-void
.end method

.method public recog()[Lcom/samsung/vip/engine/shape/GraphPrimitive;
    .locals 12

    .prologue
    .line 36
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    move-result v6

    .line 37
    .local v6, "nStrokeSize":I
    const/4 v7, 0x0

    .line 38
    .local v7, "nTotalPointSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v6, :cond_0

    .line 43
    const/4 v3, 0x0

    .line 44
    .local v3, "nPointIndex":I
    mul-int/lit8 v9, v7, 0x2

    new-array v8, v9, [I

    .line 45
    .local v8, "pPointData":[I
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v6, :cond_1

    .line 57
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->clear()V

    .line 58
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->clear()V

    .line 60
    invoke-virtual {p0, v8}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_UpdateScene([I)V

    .line 62
    invoke-virtual {p0}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_GetGraphPrimitiveArray()[Lcom/samsung/vip/engine/shape/GraphPrimitive;

    move-result-object v0

    .line 63
    .local v0, "graphPrimitives":[Lcom/samsung/vip/engine/shape/GraphPrimitive;
    if-nez v0, :cond_3

    .line 64
    const/4 v0, 0x0

    .line 68
    .end local v0    # "graphPrimitives":[Lcom/samsung/vip/engine/shape/GraphPrimitive;
    :goto_2
    return-object v0

    .line 39
    .end local v3    # "nPointIndex":I
    .end local v8    # "pPointData":[I
    :cond_0
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [F

    array-length v9, v9

    add-int/lit8 v9, v9, 0x1

    add-int/2addr v7, v9

    .line 38
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 46
    .restart local v3    # "nPointIndex":I
    .restart local v8    # "pPointData":[I
    :cond_1
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [F

    array-length v5, v9

    .line 47
    .local v5, "nPointSize":I
    const/4 v2, 0x0

    .local v2, "j":I
    move v4, v3

    .end local v3    # "nPointIndex":I
    .local v4, "nPointIndex":I
    :goto_3
    if-lt v2, v5, :cond_2

    .line 51
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    const v9, 0xffff

    aput v9, v8, v4

    .line 52
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nPointIndex":I
    .restart local v4    # "nPointIndex":I
    const/4 v9, 0x0

    aput v9, v8, v3

    .line 45
    add-int/lit8 v1, v1, 0x1

    move v3, v4

    .end local v4    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    goto :goto_1

    .line 48
    .end local v3    # "nPointIndex":I
    .restart local v4    # "nPointIndex":I
    :cond_2
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [F

    aget v9, v9, v2

    float-to-int v9, v9

    aput v9, v8, v4

    .line 49
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nPointIndex":I
    .restart local v4    # "nPointIndex":I
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [F

    aget v9, v9, v2

    float-to-int v9, v9

    aput v9, v8, v3

    .line 47
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 66
    .end local v2    # "j":I
    .end local v4    # "nPointIndex":I
    .end local v5    # "nPointSize":I
    .restart local v0    # "graphPrimitives":[Lcom/samsung/vip/engine/shape/GraphPrimitive;
    .restart local v3    # "nPointIndex":I
    :cond_3
    sget-object v9, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Result group # : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v11, v0

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public setDeviceDPI(I)V
    .locals 0
    .param p1, "deviceDPI"    # I

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_SetDeviceDPI(I)V

    .line 107
    return-void
.end method
