.class public Lcom/samsung/vip/engine/VIEqCharInfo;
.super Ljava/lang/Object;
.source "VIEqCharInfo.java"


# instance fields
.field public StrokeIDs:[I

.field public nRect:Landroid/graphics/Rect;

.field public nStrokeNum:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/16 v1, 0x14

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/samsung/vip/engine/VIEqCharInfo;->StrokeIDs:[I

    .line 14
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/vip/engine/VIEqCharInfo;->StrokeIDs:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 17
    iput v2, p0, Lcom/samsung/vip/engine/VIEqCharInfo;->nStrokeNum:I

    .line 18
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v2, v2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/samsung/vip/engine/VIEqCharInfo;->nRect:Landroid/graphics/Rect;

    .line 19
    return-void

    .line 15
    :cond_0
    iget-object v1, p0, Lcom/samsung/vip/engine/VIEqCharInfo;->StrokeIDs:[I

    aput v2, v1, v0

    .line 14
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>([IILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "strokeIDs"    # [I
    .param p2, "strokeNum"    # I
    .param p3, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/samsung/vip/engine/VIEqCharInfo;->StrokeIDs:[I

    .line 23
    iput p2, p0, Lcom/samsung/vip/engine/VIEqCharInfo;->nStrokeNum:I

    .line 24
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/samsung/vip/engine/VIEqCharInfo;->nRect:Landroid/graphics/Rect;

    .line 25
    return-void
.end method
