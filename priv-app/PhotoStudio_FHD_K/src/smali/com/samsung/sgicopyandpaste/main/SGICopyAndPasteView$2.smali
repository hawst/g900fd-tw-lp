.class Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$2;
.super Landroid/os/Handler;
.source "SGICopyAndPasteView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;


# direct methods
.method constructor <init>(Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$2;->this$0:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;

    .line 300
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 302
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 319
    :goto_0
    return-void

    .line 304
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$2;->this$0:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;

    iget-object v0, v0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mPrevSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$2;->this$0:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;

    iget-object v1, v1, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SASize;->mWidth:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SASize;->mWidth:F

    .line 305
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$2;->this$0:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;

    iget-object v0, v0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mPrevSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$2;->this$0:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;

    iget-object v1, v1, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SASize;->mHeight:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SASize;->mHeight:F

    .line 306
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$2;->this$0:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;

    iget-object v0, v0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v1, p1, Landroid/os/Message;->arg1:I

    int-to-float v1, v1

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SASize;->mWidth:F

    .line 307
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$2;->this$0:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;

    iget-object v0, v0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v1, p1, Landroid/os/Message;->arg2:I

    int-to-float v1, v1

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SASize;->mHeight:F

    .line 308
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$2;->this$0:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;

    invoke-virtual {v0}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->updateRoot()V

    goto :goto_0

    .line 312
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$2;->this$0:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->setVisibility(I)V

    goto :goto_0

    .line 316
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$2;->this$0:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->setVisibility(I)V

    goto :goto_0

    .line 302
    :pswitch_data_0
    .packed-switch 0x9001
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
