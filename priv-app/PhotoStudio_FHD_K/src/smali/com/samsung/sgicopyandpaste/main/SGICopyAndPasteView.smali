.class public Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;
.super Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;
.source "SGICopyAndPasteView.java"

# interfaces
.implements Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$RenderNotifier;
.implements Lcom/sec/android/samsunganimation/slide/SASlideManager$SASlideRequestRender;


# instance fields
.field private final BUILD_VERSION:Ljava/lang/String;

.field public final MSG_INVISIBLE:I

.field public final MSG_SURFACE_RESIZE:I

.field public final MSG_VISIBLE:I

.field protected mAnimationListener:Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;

.field protected mCallback:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteCallback;

.field protected mEffectColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

.field protected mEffectImage:Lcom/sec/android/samsunganimation/basetype/SAImage;

.field protected mEffectRadius:F

.field protected mEffectRegion:Lcom/sec/android/samsunganimation/basetype/SARect;

.field protected mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

.field protected mEffectVertexCount:I

.field protected mPoints:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/samsunganimation/basetype/SAVector2;",
            ">;"
        }
    .end annotation
.end field

.field protected mPrevSize:Lcom/sec/android/samsunganimation/basetype/SASize;

.field protected mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

.field protected mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

.field private mViewHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v7, 0x8

    const/high16 v2, 0x44a00000    # 1280.0f

    const/high16 v1, 0x44340000    # 720.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 81
    invoke-direct {p0, p1}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;-><init>(Landroid/content/Context;)V

    .line 37
    const-string v0, "RELEASE v1.0a Date:2013-06-05 14:40"

    iput-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->BUILD_VERSION:Ljava/lang/String;

    .line 39
    const v0, 0x9001

    iput v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->MSG_SURFACE_RESIZE:I

    .line 40
    const v0, 0x9002

    iput v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->MSG_INVISIBLE:I

    .line 41
    const v0, 0x9003

    iput v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->MSG_VISIBLE:I

    .line 44
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SASize;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/samsunganimation/basetype/SASize;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    .line 45
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SASize;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/samsunganimation/basetype/SASize;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mPrevSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    .line 46
    iput-object v5, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    .line 48
    iput-object v5, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectImage:Lcom/sec/android/samsunganimation/basetype/SAImage;

    .line 49
    iput-object v5, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    .line 50
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SARect;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SARect;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectRegion:Lcom/sec/android/samsunganimation/basetype/SARect;

    .line 51
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAColor;

    const v1, 0x3e4ccccd    # 0.2f

    const v2, 0x3e99999a    # 0.3f

    const v3, 0x3f4ccccd    # 0.8f

    const v4, 0x3f333333    # 0.7f

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/samsunganimation/basetype/SAColor;-><init>(FFFF)V

    iput-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    .line 52
    const/16 v0, 0x20

    iput v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectVertexCount:I

    .line 53
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectRadius:F

    .line 55
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mPoints:Ljava/util/Vector;

    .line 56
    iput-object v5, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mCallback:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteCallback;

    .line 58
    new-instance v0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$1;

    invoke-direct {v0, p0}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$1;-><init>(Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;)V

    iput-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mAnimationListener:Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;

    .line 300
    new-instance v0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$2;

    invoke-direct {v0, p0}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView$2;-><init>(Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;)V

    iput-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mViewHandler:Landroid/os/Handler;

    .line 83
    invoke-virtual {p0, v6, v7, v7}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->initialize(ZII)V

    .line 84
    invoke-static {}, Lcom/sec/android/samsunganimation/slide/SASlideManager;->getInstance()Lcom/sec/android/samsunganimation/slide/SASlideManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/samsunganimation/slide/SASlideManager;->setSASlideRequestRender(Lcom/sec/android/samsunganimation/slide/SASlideManager$SASlideRequestRender;)V

    .line 86
    invoke-virtual {p0}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->createSlide()Z

    .line 87
    invoke-virtual {p0, p0}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->setRenderNotifier(Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$RenderNotifier;)V

    .line 88
    invoke-virtual {p0, v6}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->enableUpdateListener(Z)V

    .line 89
    invoke-virtual {p0, v6}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->setZOrderOnTop(Z)V

    .line 91
    const-string v0, "SGICopyAndPaste"

    const-string v1, "BUILD VERSION:RELEASE v1.0a Date:2013-06-05 14:40"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    return-void
.end method


# virtual methods
.method public addPoint(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mPoints:Ljava/util/Vector;

    new-instance v1, Lcom/sec/android/samsunganimation/basetype/SAVector2;

    invoke-direct {v1, p1, p2}, Lcom/sec/android/samsunganimation/basetype/SAVector2;-><init>(FF)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 134
    return-void
.end method

.method public addPoint(Lcom/sec/android/samsunganimation/basetype/SAVector2;)V
    .locals 1
    .param p1, "vector2"    # Lcom/sec/android/samsunganimation/basetype/SAVector2;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mPoints:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 138
    return-void
.end method

.method public animationSlideNotify()V
    .locals 0

    .prologue
    .line 255
    return-void
.end method

.method protected createEffectSlide()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 104
    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    if-eqz v1, :cond_0

    .line 106
    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    iget-object v2, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    invoke-virtual {v1, v2}, Lcom/sec/android/samsunganimation/slide/SASlide;->removeSlide(Lcom/sec/android/samsunganimation/slide/SASlide;)V

    .line 107
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    .line 110
    :cond_0
    const/4 v1, 0x4

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    .line 112
    .local v0, "color":[F
    new-instance v1, Lcom/sec/android/samsunganimation/slide/SASlide;

    invoke-direct {v1}, Lcom/sec/android/samsunganimation/slide/SASlide;-><init>()V

    iput-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    .line 113
    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    iget-object v2, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    invoke-virtual {v1, v2}, Lcom/sec/android/samsunganimation/slide/SASlide;->addSlide(Lcom/sec/android/samsunganimation/slide/SASlide;)V

    .line 114
    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    iget-object v2, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SASize;->mWidth:F

    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v3, v3, Lcom/sec/android/samsunganimation/basetype/SASize;->mHeight:F

    invoke-virtual {v1, v4, v4, v2, v3}, Lcom/sec/android/samsunganimation/slide/SASlide;->setRegion(FFFF)V

    .line 115
    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    invoke-virtual {v1, v0}, Lcom/sec/android/samsunganimation/slide/SASlide;->setBackgroundColor([F)V

    .line 116
    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    invoke-virtual {v1, v4, v4}, Lcom/sec/android/samsunganimation/slide/SASlide;->setPivotPoint(FF)V

    .line 117
    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/samsunganimation/slide/SASlide;->setMorphingType(I)V

    .line 119
    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    invoke-virtual {v1, v4}, Lcom/sec/android/samsunganimation/slide/SASlide;->setOpacity(F)V

    .line 120
    return-void

    .line 110
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method protected createSlide()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 95
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    if-nez v0, :cond_0

    .line 96
    const/4 v0, 0x4

    new-array v7, v0, [F

    fill-array-data v7, :array_0

    .line 97
    .local v7, "color":[F
    new-instance v0, Lcom/sec/android/samsunganimation/slide/SASlide;

    iget-object v2, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v5, v2, Lcom/sec/android/samsunganimation/basetype/SASize;->mWidth:F

    iget-object v2, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v6, v2, Lcom/sec/android/samsunganimation/basetype/SASize;->mHeight:F

    move v2, v1

    move v4, v3

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/samsunganimation/slide/SASlide;-><init>(IIFFFF[F)V

    iput-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    .line 98
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/slide/SASlide;->getNativeSlideHandle()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/samsunganimation/slide/SASlide;->nativeSetRootSlideSASlide(I)V

    .line 100
    .end local v7    # "color":[F
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 96
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->destroySlide()Z

    .line 267
    return-void
.end method

.method protected destroySlide()Z
    .locals 3

    .prologue
    .line 270
    iget-object v2, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    if-eqz v2, :cond_0

    .line 271
    iget-object v2, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    invoke-virtual {v2}, Lcom/sec/android/samsunganimation/slide/SASlide;->getSubSlideCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_1

    .line 276
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/android/samsunganimation/slide/SASlide;->nativeSetRootSlideSASlide(I)V

    .line 278
    .end local v0    # "i":I
    :cond_0
    const/4 v2, 0x1

    return v2

    .line 272
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    invoke-virtual {v2, v0}, Lcom/sec/android/samsunganimation/slide/SASlide;->getSubSlide(I)Lcom/sec/android/samsunganimation/slide/SASlide;

    move-result-object v1

    .line 273
    .local v1, "removeSlide":Lcom/sec/android/samsunganimation/slide/SASlide;
    if-eqz v1, :cond_2

    .line 274
    iget-object v2, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    invoke-virtual {v2, v1}, Lcom/sec/android/samsunganimation/slide/SASlide;->removeSlide(Lcom/sec/android/samsunganimation/slide/SASlide;)V

    .line 271
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mViewHandler:Landroid/os/Handler;

    const v1, 0x9002

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 298
    return-void
.end method

.method public onResize(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 284
    invoke-super {p0, p1, p2}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->onResize(II)V

    .line 285
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 286
    .local v0, "msg":Landroid/os/Message;
    const v1, 0x9001

    iput v1, v0, Landroid/os/Message;->what:I

    .line 287
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 288
    iput p2, v0, Landroid/os/Message;->arg2:I

    .line 289
    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mViewHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 290
    return-void
.end method

.method public onUpdateFinished()V
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mCallback:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteCallback;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mCallback:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteCallback;

    invoke-interface {v0}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteCallback;->onFirstRenderFinished()V

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    if-eqz v0, :cond_1

    .line 246
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/samsunganimation/slide/SASlide;->setOpacity(F)V

    .line 247
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->enableUpdateListener(Z)V

    .line 251
    :goto_0
    return-void

    .line 249
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->enableUpdateListener(Z)V

    goto :goto_0
.end method

.method public play(I)V
    .locals 13
    .param p1, "duration"    # I

    .prologue
    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 197
    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mPoints:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v3, v10, :cond_0

    .line 235
    :goto_0
    return-void

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->createEffectSlide()V

    .line 202
    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    iget-object v4, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectRegion:Lcom/sec/android/samsunganimation/basetype/SARect;

    iget-object v4, v4, Lcom/sec/android/samsunganimation/basetype/SARect;->mOrigin:Lcom/sec/android/samsunganimation/basetype/SAPoint;

    iget v4, v4, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mX:F

    iget-object v5, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectRegion:Lcom/sec/android/samsunganimation/basetype/SARect;

    iget-object v5, v5, Lcom/sec/android/samsunganimation/basetype/SARect;->mOrigin:Lcom/sec/android/samsunganimation/basetype/SAPoint;

    iget v5, v5, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mY:F

    iget-object v6, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectRegion:Lcom/sec/android/samsunganimation/basetype/SARect;

    iget-object v6, v6, Lcom/sec/android/samsunganimation/basetype/SARect;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v6, v6, Lcom/sec/android/samsunganimation/basetype/SASize;->mWidth:F

    iget-object v7, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectRegion:Lcom/sec/android/samsunganimation/basetype/SARect;

    iget-object v7, v7, Lcom/sec/android/samsunganimation/basetype/SARect;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v7, v7, Lcom/sec/android/samsunganimation/basetype/SASize;->mHeight:F

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/sec/android/samsunganimation/slide/SASlide;->setRegion(FFFF)V

    .line 203
    const/4 v3, 0x4

    new-array v1, v3, [F

    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v3, v3, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    aput v3, v1, v8

    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v3, v3, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    aput v3, v1, v9

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v4, v4, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    aput v4, v1, v3

    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v3, v3, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    aput v3, v1, v10

    .line 204
    .local v1, "color":[F
    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    invoke-virtual {v3, v1}, Lcom/sec/android/samsunganimation/slide/SASlide;->setMorphingColor([F)V

    .line 205
    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    iget v4, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectVertexCount:I

    invoke-virtual {v3, v4}, Lcom/sec/android/samsunganimation/slide/SASlide;->setMorphingVertexCount(I)V

    .line 206
    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/samsunganimation/slide/SASlide;->setImage(Lcom/sec/android/samsunganimation/basetype/SAImage;)V

    .line 207
    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    iget-object v4, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectImage:Lcom/sec/android/samsunganimation/basetype/SAImage;

    invoke-virtual {v3, v4}, Lcom/sec/android/samsunganimation/slide/SASlide;->setImage(Lcom/sec/android/samsunganimation/basetype/SAImage;)V

    .line 208
    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    iget v4, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectRadius:F

    invoke-virtual {v3, v4}, Lcom/sec/android/samsunganimation/slide/SASlide;->setMorphingRadius(F)V

    .line 211
    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    iget-object v4, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mPoints:Ljava/util/Vector;

    invoke-virtual {v3, v4}, Lcom/sec/android/samsunganimation/slide/SASlide;->setTesselationPointList(Ljava/util/Vector;)V

    .line 213
    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    invoke-virtual {v3, v9}, Lcom/sec/android/samsunganimation/slide/SASlide;->setMorphingType(I)V

    .line 215
    new-instance v0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;-><init>()V

    .line 216
    .local v0, "aniSet":Lcom/sec/android/samsunganimation/animation/SAAnimationSet;
    invoke-virtual {v0, v10}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->setAnimationSetInterpolator(I)V

    .line 217
    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mAnimationListener:Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;

    invoke-virtual {v0, v3}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->setListener(Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;)V

    .line 218
    invoke-virtual {v0, p1}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->setDuration(I)V

    .line 219
    invoke-virtual {v0, v8}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->setRepeatCount(I)V

    .line 220
    invoke-virtual {v0, v8}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->setAutoReverse(Z)V

    .line 221
    invoke-virtual {v0, v8}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->setOffset(I)V

    .line 223
    new-instance v2, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;

    const/16 v3, 0x22

    invoke-direct {v2, v3}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;-><init>(I)V

    .line 224
    .local v2, "keyMorphingTimeAni":Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;
    invoke-virtual {v2, p1}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->setDuration(I)V

    .line 225
    invoke-virtual {v2, v8}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->setRepeatCount(I)V

    .line 226
    invoke-virtual {v2, v8}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->setAutoReverse(Z)V

    .line 227
    invoke-virtual {v2, v8}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->setOffset(I)V

    .line 228
    invoke-virtual {v2, v11, v11}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->addKeyProperty(FF)V

    .line 229
    invoke-virtual {v2, v12, v12}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->addKeyProperty(FF)V

    .line 230
    invoke-virtual {v0, v2}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->addAnimation(Lcom/sec/android/samsunganimation/animation/SAAnimation;)Z

    .line 232
    iget-object v3, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    invoke-virtual {v3, v0}, Lcom/sec/android/samsunganimation/slide/SASlide;->startAnimation(Lcom/sec/android/samsunganimation/animation/SAAnimation;)V

    .line 234
    invoke-virtual {p0, v9}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->enableUpdateListener(Z)V

    goto/16 :goto_0
.end method

.method protected reLayout()Z
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method

.method public requestSlideRender()Z
    .locals 1

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->swapAnimationSAGLSurface()V

    .line 261
    invoke-virtual {p0}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->requestRender()V

    .line 262
    const/4 v0, 0x0

    return v0
.end method

.method public setCallbackListener(Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteCallback;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mCallback:Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteCallback;

    .line 142
    return-void
.end method

.method public setFloatPoints(Ljava/util/Vector;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/samsunganimation/basetype/SAVector2;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 154
    .local p1, "points":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/sec/android/samsunganimation/basetype/SAVector2;>;"
    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mPoints:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    .line 155
    if-eqz p1, :cond_0

    .line 156
    invoke-virtual {p1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 160
    :cond_0
    return-void

    .line 156
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/samsunganimation/basetype/SAVector2;

    .line 157
    .local v0, "vec":Lcom/sec/android/samsunganimation/basetype/SAVector2;
    invoke-virtual {p0, v0}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->addPoint(Lcom/sec/android/samsunganimation/basetype/SAVector2;)V

    goto :goto_0
.end method

.method public setIntPoints(Ljava/util/Vector;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Landroid/graphics/Point;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "points":Ljava/util/Vector;, "Ljava/util/Vector<Landroid/graphics/Point;>;"
    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mPoints:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    .line 146
    if-eqz p1, :cond_0

    .line 147
    invoke-virtual {p1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 151
    :cond_0
    return-void

    .line 147
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    .line 148
    .local v0, "p":Landroid/graphics/Point;
    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->addPoint(FF)V

    goto :goto_0
.end method

.method public setMorphingColor([F)V
    .locals 2
    .param p1, "morphingColor"    # [F

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    const/4 v1, 0x0

    aget v1, p1, v1

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    .line 191
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    const/4 v1, 0x1

    aget v1, p1, v1

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    .line 192
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    const/4 v1, 0x2

    aget v1, p1, v1

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    .line 193
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    const/4 v1, 0x3

    aget v1, p1, v1

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    .line 194
    return-void
.end method

.method public setMorphingGridCount(I)V
    .locals 1
    .param p1, "gridCount"    # I

    .prologue
    .line 182
    mul-int/lit8 v0, p1, 0x2

    iput v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectVertexCount:I

    .line 183
    return-void
.end method

.method public setMorphingRadius(F)V
    .locals 0
    .param p1, "radius"    # F

    .prologue
    .line 178
    iput p1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectRadius:F

    .line 179
    return-void
.end method

.method public setMorphingVertexCount(I)V
    .locals 0
    .param p1, "vertexCount"    # I

    .prologue
    .line 186
    iput p1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectVertexCount:I

    .line 187
    return-void
.end method

.method public setWindowImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectImage:Lcom/sec/android/samsunganimation/basetype/SAImage;

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAImage;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAImage;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectImage:Lcom/sec/android/samsunganimation/basetype/SAImage;

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectImage:Lcom/sec/android/samsunganimation/basetype/SAImage;

    invoke-virtual {v0, p1}, Lcom/sec/android/samsunganimation/basetype/SAImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 175
    return-void
.end method

.method public setWindowRegion(FFFF)V
    .locals 1
    .param p1, "offsetX"    # F
    .param p2, "offsetY"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectRegion:Lcom/sec/android/samsunganimation/basetype/SARect;

    iget-object v0, v0, Lcom/sec/android/samsunganimation/basetype/SARect;->mOrigin:Lcom/sec/android/samsunganimation/basetype/SAPoint;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mX:F

    .line 164
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectRegion:Lcom/sec/android/samsunganimation/basetype/SARect;

    iget-object v0, v0, Lcom/sec/android/samsunganimation/basetype/SARect;->mOrigin:Lcom/sec/android/samsunganimation/basetype/SAPoint;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mY:F

    .line 166
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectRegion:Lcom/sec/android/samsunganimation/basetype/SARect;

    iget-object v0, v0, Lcom/sec/android/samsunganimation/basetype/SARect;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SASize;->mWidth:F

    .line 167
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mEffectRegion:Lcom/sec/android/samsunganimation/basetype/SARect;

    iget-object v0, v0, Lcom/sec/android/samsunganimation/basetype/SARect;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iput p4, v0, Lcom/sec/android/samsunganimation/basetype/SASize;->mHeight:F

    .line 168
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mViewHandler:Landroid/os/Handler;

    const v1, 0x9003

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 294
    return-void
.end method

.method protected updateRoot()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 123
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mRootSlide:Lcom/sec/android/samsunganimation/slide/SASlide;

    iget-object v1, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SASize;->mWidth:F

    iget-object v2, p0, Lcom/samsung/sgicopyandpaste/main/SGICopyAndPasteView;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SASize;->mHeight:F

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/sec/android/samsunganimation/slide/SASlide;->setRegion(FFFF)V

    .line 126
    :cond_0
    return-void
.end method
