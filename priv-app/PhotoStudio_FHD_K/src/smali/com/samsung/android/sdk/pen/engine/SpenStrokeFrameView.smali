.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
.super Landroid/view/SurfaceView;
.source "SpenStrokeFrameView.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CameraFrame"


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mCamera:Landroid/hardware/Camera;

.field private mCameraDegree:I

.field private mCameraType:I

.field private mCompleteListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;

.field private mIsAutofocus:Z

.field private mIsPreviewCreated:Z

.field private mPictureHeight:I

.field private mPictureWidth:I

.field private mPreviewCallback:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;

.field private mPreviewHeight:I

.field private mPreviewWidth:I

.field private final mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mZoomMax:I

.field private final pictureListener:Landroid/hardware/Camera$PictureCallback;

.field private final shutterListener:Landroid/hardware/Camera$ShutterCallback;


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "cameraType"    # I

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 32
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    .line 33
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewHeight:I

    .line 34
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    .line 35
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureHeight:I

    .line 40
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsAutofocus:Z

    .line 41
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsPreviewCreated:Z

    .line 339
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->shutterListener:Landroid/hardware/Camera$ShutterCallback;

    .line 346
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->pictureListener:Landroid/hardware/Camera$PictureCallback;

    .line 58
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mActivity:Landroid/app/Activity;

    .line 59
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraType:I

    .line 60
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 61
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 62
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setKeepScreenOn(Z)V

    .line 63
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCompleteListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;

    return-object v0
.end method

.method public static isFrontCamera()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 365
    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 366
    .local v2, "cameraInfo":Landroid/hardware/Camera$CameraInfo;
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    .line 367
    .local v1, "cameraCount":I
    const/4 v0, 0x0

    .local v0, "camIdx":I
    :goto_0
    if-lt v0, v1, :cond_1

    .line 373
    const/4 v3, 0x0

    :cond_0
    return v3

    .line 368
    :cond_1
    invoke-static {v0, v2}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 369
    iget v4, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-eq v4, v3, :cond_0

    .line 367
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public cancelAutoFocus()V
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsAutofocus:Z

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    .line 84
    :cond_0
    return-void
.end method

.method public getCamera()Landroid/hardware/Camera;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    return-object v0
.end method

.method public getCameraDegree()I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraDegree:I

    return v0
.end method

.method public getCameraType()I
    .locals 1

    .prologue
    .line 382
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraType:I

    return v0
.end method

.method public initCamera()V
    .locals 15

    .prologue
    .line 87
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    if-eqz v12, :cond_0

    .line 88
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 89
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v12}, Landroid/hardware/Camera;->stopPreview()V

    .line 90
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v12}, Landroid/hardware/Camera;->release()V

    .line 91
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    .line 95
    :cond_0
    :try_start_0
    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraType:I

    if-nez v12, :cond_2

    .line 96
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v12

    iput-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :cond_1
    :goto_0
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    if-nez v12, :cond_4

    .line 114
    const-string v12, "TAG"

    const-string v13, "Camera open failed"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCompleteListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;

    const/4 v13, 0x0

    invoke-interface {v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;->onComplete([B)V

    .line 236
    :goto_1
    return-void

    .line 98
    :cond_2
    :try_start_1
    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 99
    .local v2, "cameraInfo":Landroid/hardware/Camera$CameraInfo;
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    .line 100
    .local v1, "cameraCount":I
    const/4 v0, 0x0

    .local v0, "camIdx":I
    :goto_2
    if-ge v0, v1, :cond_1

    .line 101
    invoke-static {v0, v2}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 102
    iget v12, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_3

    .line 103
    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v12

    iput-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 108
    .end local v0    # "camIdx":I
    .end local v1    # "cameraCount":I
    .end local v2    # "cameraInfo":Landroid/hardware/Camera$CameraInfo;
    :catch_0
    move-exception v4

    .line 109
    .local v4, "e":Ljava/lang/Exception;
    const-string v12, "TAG"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Camera open exception"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCompleteListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;

    const/4 v13, 0x0

    invoke-interface {v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;->onComplete([B)V

    goto :goto_1

    .line 100
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v0    # "camIdx":I
    .restart local v1    # "cameraCount":I
    .restart local v2    # "cameraInfo":Landroid/hardware/Camera$CameraInfo;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 120
    .end local v0    # "camIdx":I
    .end local v1    # "cameraCount":I
    .end local v2    # "cameraInfo":Landroid/hardware/Camera$CameraInfo;
    :cond_4
    :try_start_2
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v12, v13}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 126
    :goto_3
    :try_start_3
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v12, p0}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 131
    :goto_4
    const/4 v12, 0x0

    iput v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureHeight:I

    iput v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    .line 132
    const/4 v12, 0x0

    iput v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewHeight:I

    iput v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    .line 134
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v12}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v7

    .line 136
    .local v7, "parameters":Landroid/hardware/Camera$Parameters;
    if-eqz v7, :cond_b

    .line 137
    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v8

    .line 139
    .local v8, "pictureSizeList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    if-eqz v8, :cond_9

    .line 140
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_5
    :goto_5
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_c

    .line 149
    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    if-nez v12, :cond_6

    .line 150
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/hardware/Camera$Size;

    .line 151
    .local v11, "size":Landroid/hardware/Camera$Size;
    iget v12, v11, Landroid/hardware/Camera$Size;->width:I

    iput v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    .line 152
    iget v12, v11, Landroid/hardware/Camera$Size;->height:I

    iput v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureHeight:I

    .line 156
    .end local v11    # "size":Landroid/hardware/Camera$Size;
    :cond_6
    const-string v12, "CameraFrame"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "PictureSize Width: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v14, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "Height :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureHeight:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v9

    .line 160
    .local v9, "previewSizeList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    if-eqz v9, :cond_8

    .line 161
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_7
    :goto_6
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_d

    .line 171
    :cond_8
    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    if-nez v12, :cond_9

    .line 172
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_9

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/hardware/Camera$Size;

    .line 173
    .restart local v11    # "size":Landroid/hardware/Camera$Size;
    iget v12, v11, Landroid/hardware/Camera$Size;->width:I

    iput v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    .line 174
    iget v12, v11, Landroid/hardware/Camera$Size;->height:I

    iput v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewHeight:I

    .line 179
    .end local v9    # "previewSizeList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    .end local v11    # "size":Landroid/hardware/Camera$Size;
    :cond_9
    const-string v12, "CameraFrame"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "PreviewSize Width: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v14, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "Height :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewHeight:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewHeight:I

    invoke-virtual {v7, v12, v13}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 182
    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureHeight:I

    invoke-virtual {v7, v12, v13}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 184
    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v5

    .line 185
    .local v5, "focusModes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v5, :cond_a

    .line 186
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsAutofocus:Z

    .line 187
    const-string v12, "continuous-picture"

    invoke-interface {v5, v12}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 188
    const-string v12, "continuous-picture"

    invoke-virtual {v7, v12}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 197
    :cond_a
    :goto_7
    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    move-result v12

    iput v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mZoomMax:I

    .line 198
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v12, v7}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 202
    .end local v5    # "focusModes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v8    # "pictureSizeList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_b
    :try_start_4
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v12}, Landroid/hardware/Camera;->startPreview()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 208
    :goto_8
    new-instance v6, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v6}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 209
    .local v6, "info":Landroid/hardware/Camera$CameraInfo;
    const/4 v12, 0x0

    invoke-static {v12, v6}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 210
    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraType:I

    invoke-static {v12, v6}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 212
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v12}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v12

    invoke-interface {v12}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/Display;->getRotation()I

    move-result v10

    .line 213
    .local v10, "rotation":I
    const/4 v3, 0x0

    .line 214
    .local v3, "degrees":I
    packed-switch v10, :pswitch_data_0

    .line 229
    :goto_9
    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraType:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_10

    .line 230
    iget v12, v6, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v12, v3

    rem-int/lit16 v12, v12, 0x168

    iput v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraDegree:I

    .line 231
    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraDegree:I

    rsub-int v12, v12, 0x168

    rem-int/lit16 v12, v12, 0x168

    iput v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraDegree:I

    .line 235
    :goto_a
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraDegree:I

    invoke-virtual {v12, v13}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    goto/16 :goto_1

    .line 121
    .end local v3    # "degrees":I
    .end local v6    # "info":Landroid/hardware/Camera$CameraInfo;
    .end local v7    # "parameters":Landroid/hardware/Camera$Parameters;
    .end local v10    # "rotation":I
    :catch_1
    move-exception v4

    .line 122
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 127
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v4

    .line 128
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    .line 140
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v7    # "parameters":Landroid/hardware/Camera$Parameters;
    .restart local v8    # "pictureSizeList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_c
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/hardware/Camera$Size;

    .line 141
    .restart local v11    # "size":Landroid/hardware/Camera$Size;
    iget v13, v11, Landroid/hardware/Camera$Size;->width:I

    mul-int/lit8 v13, v13, 0x3

    iget v14, v11, Landroid/hardware/Camera$Size;->height:I

    mul-int/lit8 v14, v14, 0x4

    if-ne v13, v14, :cond_5

    iget v13, v11, Landroid/hardware/Camera$Size;->width:I

    const/16 v14, 0x4b0

    if-ge v13, v14, :cond_5

    .line 142
    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    iget v14, v11, Landroid/hardware/Camera$Size;->width:I

    if-ge v13, v14, :cond_5

    .line 143
    iget v13, v11, Landroid/hardware/Camera$Size;->width:I

    iput v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    .line 144
    iget v13, v11, Landroid/hardware/Camera$Size;->height:I

    iput v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureHeight:I

    goto/16 :goto_5

    .line 161
    .end local v11    # "size":Landroid/hardware/Camera$Size;
    .restart local v9    # "previewSizeList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_d
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/hardware/Camera$Size;

    .line 162
    .restart local v11    # "size":Landroid/hardware/Camera$Size;
    iget v13, v11, Landroid/hardware/Camera$Size;->width:I

    mul-int/lit8 v13, v13, 0x3

    iget v14, v11, Landroid/hardware/Camera$Size;->height:I

    mul-int/lit8 v14, v14, 0x4

    if-ne v13, v14, :cond_7

    .line 163
    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    iget v14, v11, Landroid/hardware/Camera$Size;->width:I

    if-ge v13, v14, :cond_7

    iget v13, v11, Landroid/hardware/Camera$Size;->width:I

    const/16 v14, 0x320

    if-ge v13, v14, :cond_7

    .line 164
    iget v13, v11, Landroid/hardware/Camera$Size;->width:I

    iput v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    .line 165
    iget v13, v11, Landroid/hardware/Camera$Size;->height:I

    iput v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewHeight:I

    goto/16 :goto_6

    .line 189
    .end local v9    # "previewSizeList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    .end local v11    # "size":Landroid/hardware/Camera$Size;
    .restart local v5    # "focusModes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_e
    const-string v12, "auto"

    invoke-interface {v5, v12}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_f

    .line 190
    const-string v12, "auto"

    invoke-virtual {v7, v12}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 192
    :cond_f
    const/4 v12, 0x0

    iput-boolean v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsAutofocus:Z

    .line 193
    const-string v12, "CameraFrame"

    const-string v13, "No focus mode"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 203
    .end local v5    # "focusModes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v8    # "pictureSizeList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :catch_3
    move-exception v4

    .line 205
    .restart local v4    # "e":Ljava/lang/Exception;
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCompleteListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;

    const/4 v13, 0x0

    invoke-interface {v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;->onComplete([B)V

    goto/16 :goto_8

    .line 216
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v3    # "degrees":I
    .restart local v6    # "info":Landroid/hardware/Camera$CameraInfo;
    .restart local v10    # "rotation":I
    :pswitch_0
    const/4 v3, 0x0

    .line 217
    goto/16 :goto_9

    .line 219
    :pswitch_1
    const/16 v3, 0x5a

    .line 220
    goto/16 :goto_9

    .line 222
    :pswitch_2
    const/16 v3, 0xb4

    .line 223
    goto/16 :goto_9

    .line 225
    :pswitch_3
    const/16 v3, 0x10e

    goto/16 :goto_9

    .line 233
    :cond_10
    iget v12, v6, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v12, v3

    add-int/lit16 v12, v12, 0x168

    rem-int/lit16 v12, v12, 0x168

    iput v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraDegree:I

    goto/16 :goto_a

    .line 214
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 326
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 327
    return-void
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 252
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewCallback:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;->OnPreview()V

    .line 253
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 331
    invoke-super {p0}, Landroid/view/SurfaceView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public setCameraType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 377
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraType:I

    .line 378
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->initCamera()V

    .line 379
    return-void
.end method

.method public setFocus(Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 281
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsAutofocus:Z

    if-eqz v4, :cond_0

    .line 282
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    if-nez v4, :cond_1

    .line 283
    const-string v4, "CameraFrame"

    const-string v5, "Camera is not created yet."

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 287
    :cond_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsPreviewCreated:Z

    if-nez v4, :cond_2

    .line 288
    const-string v4, "CameraFrame"

    const-string v5, "Preview of Camera is not  created yet."

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 293
    :cond_2
    new-instance v1, Landroid/hardware/Camera$Area;

    const/16 v4, 0x3e8

    invoke-direct {v1, p1, v4}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    .line 294
    .local v1, "area":Landroid/hardware/Camera$Area;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 295
    .local v0, "al":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/hardware/Camera$Area;>;"
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v4}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v3

    .line 297
    .local v3, "parameters":Landroid/hardware/Camera$Parameters;
    invoke-virtual {v3, v0}, Landroid/hardware/Camera$Parameters;->setFocusAreas(Ljava/util/List;)V

    .line 298
    const-string v4, "auto"

    invoke-virtual {v3, v4}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 299
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v4, v3}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 301
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 302
    :catch_0
    move-exception v2

    .line 303
    .local v2, "e":Ljava/lang/RuntimeException;
    const-string v4, "CameraFrame"

    const-string v5, "autoFocus is failed"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setOnCompleteCameraFrameListener(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCompleteListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;

    .line 53
    return-void
.end method

.method public setOnPreviewCallback(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;)V
    .locals 0
    .param p1, "cb"    # Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewCallback:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;

    .line 248
    return-void
.end method

.method public setZoom(I)V
    .locals 3
    .param p1, "zoom"    # I

    .prologue
    .line 309
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    if-nez v1, :cond_0

    .line 310
    const-string v1, "CameraFrame"

    const-string v2, "camera open error"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :goto_0
    return-void

    .line 313
    :cond_0
    if-gez p1, :cond_1

    .line 314
    const/4 p1, 0x0

    .line 316
    :cond_1
    const/16 v1, 0x64

    if-le p1, v1, :cond_2

    .line 317
    const/16 p1, 0x64

    .line 319
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 320
    .local v0, "parameters":Landroid/hardware/Camera$Parameters;
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mZoomMax:I

    mul-int/2addr v1, p1

    div-int/lit8 v1, v1, 0x64

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    .line 321
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 68
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 69
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 70
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    .line 72
    :cond_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 244
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->initCamera()V

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsPreviewCreated:Z

    .line 78
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->stop()V

    .line 337
    return-void
.end method

.method public takePicture()V
    .locals 7

    .prologue
    .line 256
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    if-nez v3, :cond_0

    .line 257
    const-string v3, "CameraFrame"

    const-string v4, "camera is not created"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    :goto_0
    return-void

    .line 262
    :cond_0
    :try_start_0
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;)V

    .line 270
    .local v1, "mAudioFocusListener":Landroid/media/AudioManager$OnAudioFocusChangeListener;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mActivity:Landroid/app/Activity;

    const-string v4, "audio"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    .line 271
    .local v2, "mAudioManager":Landroid/media/AudioManager;
    const/4 v3, 0x3

    .line 272
    const/4 v4, 0x2

    .line 271
    invoke-virtual {v2, v1, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 273
    invoke-virtual {v2, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 274
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->shutterListener:Landroid/hardware/Camera$ShutterCallback;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->pictureListener:Landroid/hardware/Camera$PictureCallback;

    invoke-virtual {v3, v4, v5, v6}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 275
    .end local v1    # "mAudioFocusListener":Landroid/media/AudioManager$OnAudioFocusChangeListener;
    .end local v2    # "mAudioManager":Landroid/media/AudioManager;
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v3, "CameraFrame"

    const-string v4, "exsynos takepicture exception"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
