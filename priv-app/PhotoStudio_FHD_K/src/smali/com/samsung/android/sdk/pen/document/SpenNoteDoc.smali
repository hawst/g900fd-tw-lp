.class public Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
.super Ljava/lang/Object;
.source "SpenNoteDoc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;
    }
.end annotation


# static fields
.field public static final MODE_READ_ONLY:I = 0x0

.field public static final MODE_WRITABLE:I = 0x1

.field private static final NATIVE_COMMAND_APPEND_PAGES:I = 0x1

.field private static final NATIVE_COMMAND_DUMMY:I = 0x0

.field public static final ORIENTATION_LANDSCAPE:I = 0x1

.field public static final ORIENTATION_PORTRAIT:I = 0x0

.field private static final SCHEME_TEMPLATE_NAME:Ljava/lang/String; = "template_name://"


# instance fields
.field private final is32Bit:Z

.field private mContext:Landroid/content/Context;

.field private mHandle:I

.field private mHandle2:J


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v2, 0x20

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 103
    iput v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 104
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 107
    return-void

    .line 101
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v1

    const/16 v4, 0x20

    if-ne v1, v4, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 102
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 103
    iput v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 104
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 136
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 138
    if-le p2, p3, :cond_2

    .line 139
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v3, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;III)I

    move-result v0

    .line 143
    .local v0, "rnt":I
    :goto_1
    if-nez v0, :cond_0

    .line 144
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 150
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 153
    :cond_0
    return-void

    .end local v0    # "rnt":I
    :cond_1
    move v1, v3

    .line 101
    goto :goto_0

    .line 141
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v2, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;III)I

    move-result v0

    .restart local v0    # "rnt":I
    goto :goto_1

    .line 146
    :sswitch_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 148
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SpenNoteDoc("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is already closed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 144
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public constructor <init>(Landroid/content/Context;III)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "orientation"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v1

    const/16 v3, 0x20

    if-ne v1, v3, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 102
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 103
    iput v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 104
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 186
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 187
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;III)I

    move-result v0

    .line 188
    .local v0, "rnt":I
    if-nez v0, :cond_0

    .line 189
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 195
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 198
    :cond_0
    return-void

    .line 101
    .end local v0    # "rnt":I
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 191
    .restart local v0    # "rnt":I
    :sswitch_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 193
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SpenNoteDoc("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is already closed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 189
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/io/InputStream;II)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "stream"    # Ljava/io/InputStream;
    .param p3, "width"    # I
    .param p4, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v1

    const/16 v3, 0x20

    if-ne v1, v3, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 102
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 103
    iput v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 104
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 202
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 203
    const/4 v0, 0x0

    .line 204
    .local v0, "rnt":I
    instance-of v1, p2, Ljava/io/ByteArrayInputStream;

    if-eqz v1, :cond_2

    .line 205
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    check-cast p2, Ljava/io/ByteArrayInputStream;

    .end local p2    # "stream":Ljava/io/InputStream;
    invoke-direct {p0, v1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/io/ByteArrayInputStream;II)I

    move-result v0

    .line 215
    :goto_1
    if-nez v0, :cond_0

    .line 216
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 226
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 229
    :cond_0
    :goto_2
    return-void

    .line 101
    .end local v0    # "rnt":I
    .restart local p2    # "stream":Ljava/io/InputStream;
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 206
    .restart local v0    # "rnt":I
    :cond_2
    instance-of v1, p2, Ljava/io/FileInputStream;

    if-eqz v1, :cond_3

    .line 207
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    check-cast p2, Ljava/io/FileInputStream;

    .end local p2    # "stream":Ljava/io/InputStream;
    invoke-virtual {p2}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {p0, v1, v2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/io/FileDescriptor;II)I

    move-result v0

    .line 208
    goto :goto_1

    .line 210
    .restart local p2    # "stream":Ljava/io/InputStream;
    :cond_3
    const/4 v1, 0x7

    .line 211
    const-string v2, "The parameter \'stream\' is unsupported type. This method supports only ByteArrayInputStream and FileInputStream"

    .line 210
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_2

    .line 218
    .end local p2    # "stream":Ljava/io/InputStream;
    :pswitch_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 220
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    const-string v2, "It does not correspond to the NoteDoc file format"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 222
    :pswitch_3
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v2, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 224
    :pswitch_4
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SpenNoteDoc("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is already closed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 216
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;DI)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "rotation"    # D
    .param p5, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v2, 0x20

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 102
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 103
    iput v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 104
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 328
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 329
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DI)I

    move-result v7

    .line 331
    .local v7, "rnt":I
    if-nez v7, :cond_0

    .line 332
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 343
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 346
    :cond_0
    return-void

    .line 101
    .end local v7    # "rnt":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 334
    .restart local v7    # "rnt":I
    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 336
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    .line 337
    const-string v1, "E_UNSUPPORTED_TYPE : It does not correspond to the NoteDoc file format"

    .line 336
    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 339
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v1, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341
    :pswitch_4
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 332
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;II)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    move v0, v6

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 102
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 103
    iput v6, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 104
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 270
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 271
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)I

    move-result v7

    .line 273
    .local v7, "rnt":I
    if-nez v7, :cond_0

    .line 274
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 285
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 288
    :cond_0
    return-void

    .line 101
    .end local v7    # "rnt":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 276
    .restart local v7    # "rnt":I
    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 278
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    .line 279
    const-string v1, "E_UNSUPPORTED_TYPE : It does not correspond to the NoteDoc file format"

    .line 278
    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 281
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v1, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :pswitch_4
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IIZ)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "mode"    # I
    .param p5, "discardUnsavedData"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v2, 0x20

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 102
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 103
    iput v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 104
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 513
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 514
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)I

    move-result v7

    .line 516
    .local v7, "rnt":I
    if-nez v7, :cond_0

    .line 517
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 528
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 531
    :cond_0
    return-void

    .line 101
    .end local v7    # "rnt":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 519
    .restart local v7    # "rnt":I
    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 521
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    .line 522
    const-string v1, "E_UNSUPPORTED_TYPE : It does not correspond to the NoteDoc file format"

    .line 521
    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 524
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v1, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 526
    :pswitch_4
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 517
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;DI)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;
    .param p4, "rotation"    # D
    .param p6, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v2, 0x20

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 103
    iput v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 104
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 450
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 451
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DI)I

    move-result v7

    .line 453
    .local v7, "rnt":I
    if-nez v7, :cond_0

    .line 454
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 465
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 468
    :cond_0
    return-void

    .line 101
    .end local v7    # "rnt":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 456
    .restart local v7    # "rnt":I
    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 458
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    .line 459
    const-string v1, "E_UNSUPPORTED_TYPE : It does not correspond to the NoteDoc file format"

    .line 458
    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 461
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v1, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 463
    :pswitch_4
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;
    .param p4, "width"    # I
    .param p5, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    move v0, v6

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 103
    iput v6, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 104
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 387
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 388
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)I

    move-result v7

    .line 390
    .local v7, "rnt":I
    if-nez v7, :cond_0

    .line 391
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 402
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 405
    :cond_0
    return-void

    .line 101
    .end local v7    # "rnt":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 393
    .restart local v7    # "rnt":I
    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 395
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    .line 396
    const-string v1, "E_UNSUPPORTED_TYPE : It does not correspond to the NoteDoc file format"

    .line 395
    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 398
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v1, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 400
    :pswitch_4
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 391
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIZ)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;
    .param p4, "width"    # I
    .param p5, "mode"    # I
    .param p6, "discardUnsavedData"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v2, 0x20

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 103
    iput v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 104
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 578
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 579
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)I

    move-result v7

    .line 582
    .local v7, "rnt":I
    if-nez v7, :cond_0

    .line 583
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 594
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 597
    :cond_0
    return-void

    .line 101
    .end local v7    # "rnt":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 585
    .restart local v7    # "rnt":I
    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 587
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    .line 588
    const-string v1, "E_UNSUPPORTED_TYPE : It does not correspond to the NoteDoc file format"

    .line 587
    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 590
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v1, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 592
    :pswitch_4
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 583
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private native Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private native NoteDoc_appendPage(II)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_appendPage(ILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_appendPages(Ljava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_appendTemplatePage(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_attachFile(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private native NoteDoc_attachTemplatePage(Ljava/lang/String;Ljava/lang/String;I)Z
.end method

.method private native NoteDoc_attachToFile(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_backupObjectList(Ljava/util/ArrayList;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation
.end method

.method private native NoteDoc_close(Z)Z
.end method

.method private native NoteDoc_copyPage(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_detachFile(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_detachTemplatePage(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_discard()Z
.end method

.method private native NoteDoc_finalize()V
.end method

.method private native NoteDoc_getAppMajorVersion()I
.end method

.method private native NoteDoc_getAppMinorVersion()I
.end method

.method private native NoteDoc_getAppName()Ljava/lang/String;
.end method

.method private native NoteDoc_getAppPatchName()Ljava/lang/String;
.end method

.method private native NoteDoc_getAttachedFile(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native NoteDoc_getAttachedFileCount()I
.end method

.method private native NoteDoc_getAuthorInfo()Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;
.end method

.method private native NoteDoc_getCoverImagePath()Ljava/lang/String;
.end method

.method private native NoteDoc_getExtraDataByteArray(Ljava/lang/String;)[B
.end method

.method private native NoteDoc_getExtraDataInt(Ljava/lang/String;)I
.end method

.method private native NoteDoc_getExtraDataString(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native NoteDoc_getExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method private native NoteDoc_getGeoTagLatitude()D
.end method

.method private native NoteDoc_getGeoTagLongitude()D
.end method

.method private native NoteDoc_getHeight()I
.end method

.method private native NoteDoc_getId()Ljava/lang/String;
.end method

.method private native NoteDoc_getInternalDirectory()Ljava/lang/String;
.end method

.method private native NoteDoc_getLastEditedPageIndex()I
.end method

.method private native NoteDoc_getOrientation()I
.end method

.method private native NoteDoc_getOrientation2(Ljava/io/ByteArrayInputStream;)I
.end method

.method private native NoteDoc_getOrientation3(Ljava/io/FileDescriptor;)I
.end method

.method private native NoteDoc_getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_getPageCount()I
.end method

.method private native NoteDoc_getPageIdByIndex(I)Ljava/lang/String;
.end method

.method private native NoteDoc_getPageIndexById(Ljava/lang/String;)I
.end method

.method private native NoteDoc_getRotation()I
.end method

.method private native NoteDoc_getTemplatePage(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_getTemplatePageCount()I
.end method

.method private native NoteDoc_getTemplatePageName(I)Ljava/lang/String;
.end method

.method private native NoteDoc_getTemplateUri()Ljava/lang/String;
.end method

.method private native NoteDoc_getWidth()I
.end method

.method private native NoteDoc_hasAttachedFile(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_hasExtraDataByteArray(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_hasExtraDataInt(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_hasExtraDataString(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_hasExtraDataStringArray(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_hasTaggedPage()Z
.end method

.method private native NoteDoc_hasTemplatePage(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_init(Ljava/lang/String;III)I
.end method

.method private native NoteDoc_init(Ljava/lang/String;Ljava/io/ByteArrayInputStream;II)I
.end method

.method private native NoteDoc_init(Ljava/lang/String;Ljava/io/ByteArrayInputStream;Ljava/lang/String;II)I
.end method

.method private native NoteDoc_init(Ljava/lang/String;Ljava/io/FileDescriptor;II)I
.end method

.method private native NoteDoc_init(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/lang/String;II)I
.end method

.method private native NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DI)I
.end method

.method private native NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)I
.end method

.method private native NoteDoc_insertPage(III)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_insertPage(IILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_insertPages(Ljava/lang/String;II)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_insertTemplatePage(ILjava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_isAllPageTextOnly()Z
.end method

.method private native NoteDoc_isChanged()Z
.end method

.method private native NoteDoc_movePageIndex(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;I)Z
.end method

.method private native NoteDoc_removeExtraDataByteArray(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_removeExtraDataInt(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_removeExtraDataString(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_removeExtraDataStringArray(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_removePage(I)Z
.end method

.method private native NoteDoc_requestSave(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_restoreObjectList(Ljava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native NoteDoc_revertToTemplatePage(I)Z
.end method

.method private native NoteDoc_reviseObjectList(Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native NoteDoc_save(Ljava/io/ByteArrayOutputStream;)Z
.end method

.method private native NoteDoc_save(Ljava/io/FileDescriptor;)Z
.end method

.method private native NoteDoc_save(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_setAppName(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_setAppVersion(IILjava/lang/String;)Z
.end method

.method private native NoteDoc_setAuthorInfo(Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;)Z
.end method

.method private native NoteDoc_setCoverImage(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_setExtraDataByteArray(Ljava/lang/String;[BI)Z
.end method

.method private native NoteDoc_setExtraDataInt(Ljava/lang/String;I)Z
.end method

.method private native NoteDoc_setExtraDataString(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private native NoteDoc_setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z
.end method

.method private native NoteDoc_setGeoTag(DD)Z
.end method

.method private native NoteDoc_setTemplateUri(Ljava/lang/String;)Z
.end method

.method private static isBuildTypeEngMode()Z
    .locals 2

    .prologue
    .line 618
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private throwUncheckedException(I)V
    .locals 3
    .param p1, "errno"    # I

    .prologue
    .line 2482
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    .line 2483
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2485
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2487
    return-void
.end method


# virtual methods
.method public appendPage()Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 4

    .prologue
    .line 1257
    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_appendPage(ILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    .line 1258
    .local v0, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v0, :cond_0

    .line 1259
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1261
    :cond_0
    return-object v0
.end method

.method public appendPage(ILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 2
    .param p1, "backgroundColor"    # I
    .param p2, "backgroundImagePath"    # Ljava/lang/String;
    .param p3, "backgourndImageMode"    # I

    .prologue
    .line 1290
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_appendPage(ILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    .line 1291
    .local v0, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v0, :cond_0

    .line 1292
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1294
    :cond_0
    return-object v0
.end method

.method public appendPages(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 5
    .param p1, "count"    # I

    .prologue
    const/4 v4, 0x1

    .line 1309
    if-ge p1, v4, :cond_0

    .line 1310
    const/4 v3, 0x7

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1312
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1313
    .local v0, "input":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1315
    invoke-direct {p0, v4, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1316
    .local v1, "output":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-nez v1, :cond_1

    .line 1317
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1320
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 1321
    .local v2, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v2, :cond_2

    .line 1322
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1324
    :cond_2
    return-object v2
.end method

.method public appendPages(Ljava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 4
    .param p1, "templateName"    # Ljava/lang/String;
    .param p2, "count"    # I

    .prologue
    .line 2357
    const/4 v2, 0x1

    if-ge p2, v2, :cond_0

    .line 2358
    const/4 v2, 0x7

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 2361
    :cond_0
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_appendPages(Ljava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    .line 2362
    .local v1, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v1, :cond_1

    .line 2363
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2369
    .end local v1    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_1
    :goto_0
    return-object v1

    .line 2366
    :catch_0
    move-exception v0

    .line 2367
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2368
    const-string v2, "SpenNoteDoc"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2369
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public appendTemplatePage(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 14
    .param p1, "templatePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v13, 0x0

    .line 1398
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1399
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1400
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_appendTemplatePage(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v6

    .line 1401
    .local v6, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v6, :cond_0

    .line 1402
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 1408
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v10

    invoke-static {v10}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 1462
    .end local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_0
    :goto_0
    return-object v6

    .line 1404
    .restart local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :pswitch_1
    new-instance v10, Ljava/io/IOException;

    invoke-direct {v10}, Ljava/io/IOException;-><init>()V

    throw v10

    .line 1406
    :pswitch_2
    new-instance v10, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    const-string v11, "It does not correspond to the NoteDoc file format"

    invoke-direct {v10, v11}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1414
    .end local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_1
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 1415
    .local v0, "assetMgr":Landroid/content/res/AssetManager;
    invoke-virtual {v0, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 1416
    .local v5, "is":Ljava/io/InputStream;
    invoke-virtual {v5}, Ljava/io/InputStream;->available()I

    move-result v4

    .line 1417
    .local v4, "fileSize":I
    new-array v7, v4, [B

    .line 1419
    .local v7, "tempData":[B
    const/4 v11, 0x0

    :try_start_0
    array-length v12, v7

    invoke-virtual {v5, v7, v11, v12}, Ljava/io/InputStream;->read([BII)I

    move-result v11

    array-length v12, v7

    if-eq v11, v12, :cond_2

    .line 1420
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 1421
    new-instance v10, Ljava/io/IOException;

    const-string v11, "Failed to is.read()"

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1423
    :catch_0
    move-exception v2

    .line 1424
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 1425
    throw v2

    .line 1427
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 1429
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1431
    .local v1, "byteArrayIS":Ljava/io/ByteArrayInputStream;
    new-instance v8, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getWidth()I

    move-result v12

    invoke-direct {v8, v11, v1, v12, v13}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;Ljava/io/InputStream;II)V

    .line 1433
    .local v8, "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v11

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v12

    if-eq v11, v12, :cond_3

    .line 1434
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1435
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1436
    const/4 v11, 0x7

    .line 1437
    const-string v12, "The orientation of the template is not matched with this NoteDoc."

    .line 1436
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 1440
    :cond_3
    invoke-virtual {v8, v13}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v9

    .line 1441
    .local v9, "templatePage":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v9, :cond_4

    .line 1442
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1443
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1444
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v11

    invoke-static {v11}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    move-object v6, v10

    .line 1445
    goto :goto_0

    .line 1448
    :cond_4
    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v11

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v12

    invoke-direct {p0, v11, v12}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_appendPage(II)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v6

    .line 1449
    .restart local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v6, :cond_5

    .line 1450
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1451
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1452
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v11

    invoke-static {v11}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    move-object v6, v10

    .line 1453
    goto/16 :goto_0

    .line 1456
    :cond_5
    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->copy(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 1457
    invoke-virtual {v6, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setTemplateUri(Ljava/lang/String;)V

    .line 1458
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearChangedFlagOfLayer()V

    .line 1460
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1461
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    goto/16 :goto_0

    .line 1402
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public attachFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1915
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_attachFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1916
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1918
    :cond_0
    return-void
.end method

.method public attachTemplatePage(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "templateName"    # Ljava/lang/String;
    .param p2, "templatePath"    # Ljava/lang/String;
    .param p3, "pageIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 2204
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_attachTemplatePage(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2205
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2211
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 2218
    :cond_0
    :goto_0
    return-void

    .line 2207
    :pswitch_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2214
    :catch_0
    move-exception v0

    .line 2215
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2216
    const-string v1, "SpenNoteDoc"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2209
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :pswitch_2
    :try_start_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    const-string v2, "It does not correspond to the NoteDoc file format"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0

    .line 2205
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public attachToFile(Ljava/lang/String;)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2069
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_attachToFile(Ljava/lang/String;)Z

    move-result v0

    .line 2071
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2072
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2081
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2084
    :cond_0
    return-void

    .line 2074
    :sswitch_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 2079
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SpenNoteDoc("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is already closed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2072
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public backupObjectList(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 1
    .param p2, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2123
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_backupObjectList(Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2124
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 2126
    :cond_0
    return-void
.end method

.method public close()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, -0x1

    .line 684
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    if-eqz v0, :cond_1

    .line 685
    iget v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    if-ne v0, v2, :cond_2

    .line 710
    :cond_0
    :goto_0
    return-void

    .line 689
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 694
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_close(Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 695
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 701
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 704
    :cond_3
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 705
    iput v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 709
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    goto :goto_0

    .line 697
    :sswitch_0
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 699
    :sswitch_1
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 707
    :cond_4
    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    goto :goto_1

    .line 695
    nop

    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public copyPage(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 4
    .param p1, "sourcePage"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "pageIndex"    # I

    .prologue
    .line 2432
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_copyPage(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    .line 2433
    .local v1, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v1, :cond_0

    .line 2434
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2440
    .end local v1    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_0
    :goto_0
    return-object v1

    .line 2437
    :catch_0
    move-exception v0

    .line 2438
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2439
    const-string v2, "SpenNoteDoc"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2440
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public detachFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1931
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_detachFile(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1932
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1934
    :cond_0
    return-void
.end method

.method public detachTemplatePage(Ljava/lang/String;)V
    .locals 3
    .param p1, "templateName"    # Ljava/lang/String;

    .prologue
    .line 2236
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_detachTemplatePage(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2237
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2243
    :cond_0
    :goto_0
    return-void

    .line 2239
    :catch_0
    move-exception v0

    .line 2240
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2241
    const-string v1, "SpenNoteDoc"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public discard()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, -0x1

    .line 639
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    if-eqz v0, :cond_1

    .line 640
    iget v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    if-ne v0, v2, :cond_2

    .line 665
    :cond_0
    :goto_0
    return-void

    .line 644
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 649
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_discard()Z

    move-result v0

    if-nez v0, :cond_3

    .line 650
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 656
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 659
    :cond_3
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 660
    iput v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 664
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    goto :goto_0

    .line 652
    :sswitch_0
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 654
    :sswitch_1
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 662
    :cond_4
    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    goto :goto_1

    .line 650
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    .line 2150
    instance-of v1, p1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    if-eqz v1, :cond_2

    .line 2151
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    if-eqz v1, :cond_1

    .line 2152
    iget v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    .end local p1    # "o":Ljava/lang/Object;
    iget v2, p1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    if-ne v1, v2, :cond_2

    .line 2161
    :cond_0
    :goto_0
    return v0

    .line 2156
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    .end local p1    # "o":Ljava/lang/Object;
    iget-wide v4, p1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2161
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 604
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;

    .line 606
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_finalize()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 608
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 610
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    if-eqz v0, :cond_0

    .line 611
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 615
    :goto_0
    return-void

    .line 607
    :catchall_0
    move-exception v0

    .line 608
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 609
    throw v0

    .line 613
    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    goto :goto_0
.end method

.method public getAppMajorVersion()I
    .locals 1

    .prologue
    .line 973
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAppMajorVersion()I

    move-result v0

    return v0
.end method

.method public getAppMinorVersion()I
    .locals 1

    .prologue
    .line 985
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAppMinorVersion()I

    move-result v0

    return v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 941
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAppName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppPatchName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 997
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAppPatchName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAttachedFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1960
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAttachedFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1961
    .local v0, "filepath":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1962
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1964
    :cond_0
    return-object v0
.end method

.method public getAttachedFileCount()I
    .locals 1

    .prologue
    .line 1944
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAttachedFileCount()I

    move-result v0

    return v0
.end method

.method public getAuthorInfo()Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;
    .locals 1

    .prologue
    .line 872
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAuthorInfo()Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;

    move-result-object v0

    return-object v0
.end method

.method public getCoverImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 822
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getCoverImagePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataByteArray(Ljava/lang/String;)[B
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1137
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getExtraDataByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataInt(Ljava/lang/String;)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1109
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getExtraDataInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getExtraDataString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1095
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getExtraDataString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1123
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGeoTagLatitude()D
    .locals 2

    .prologue
    .line 903
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getGeoTagLatitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getGeoTagLongitude()D
    .locals 2

    .prologue
    .line 915
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getGeoTagLongitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 768
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getHeight()I

    move-result v0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 720
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInternalDirectory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2094
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getInternalDirectory()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastEditedPageIndex()I
    .locals 1

    .prologue
    .line 2105
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getLastEditedPageIndex()I

    move-result v0

    return v0
.end method

.method public getOrientation()I
    .locals 2

    .prologue
    .line 792
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getOrientation()I

    move-result v0

    .line 793
    .local v0, "rnt":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 794
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 796
    :cond_0
    return v0
.end method

.method public getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 2
    .param p1, "pageIndex"    # I

    .prologue
    .line 1867
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    .line 1868
    .local v0, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v0, :cond_0

    .line 1869
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1871
    :cond_0
    return-object v0
.end method

.method public getPageCount()I
    .locals 4

    .prologue
    .line 1882
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    if-eqz v0, :cond_0

    .line 1883
    const-string v0, "Model_SpenNoteDoc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mHandle = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1887
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getPageCount()I

    move-result v0

    return v0

    .line 1885
    :cond_0
    const-string v0, "Model_SpenNoteDoc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mHandle = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPageIdByIndex(I)Ljava/lang/String;
    .locals 2
    .param p1, "pageIndex"    # I

    .prologue
    .line 1829
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getPageIdByIndex(I)Ljava/lang/String;

    move-result-object v0

    .line 1830
    .local v0, "id":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1831
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1833
    :cond_0
    return-object v0
.end method

.method public getPageIndexById(Ljava/lang/String;)I
    .locals 2
    .param p1, "pageId"    # Ljava/lang/String;

    .prologue
    .line 1848
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getPageIndexById(Ljava/lang/String;)I

    move-result v0

    .line 1849
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1850
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1852
    :cond_0
    return v0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 779
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getRotation()I

    move-result v0

    return v0
.end method

.method public getTemplatePage(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 4
    .param p1, "templateName"    # Ljava/lang/String;

    .prologue
    .line 2330
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getTemplatePage(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    .line 2331
    .local v1, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v1, :cond_0

    .line 2332
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2338
    .end local v1    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_0
    :goto_0
    return-object v1

    .line 2335
    :catch_0
    move-exception v0

    .line 2336
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2337
    const-string v2, "SpenNoteDoc"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2338
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTemplatePageCount()I
    .locals 3

    .prologue
    .line 2254
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getTemplatePageCount()I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2258
    :goto_0
    return v1

    .line 2255
    :catch_0
    move-exception v0

    .line 2256
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2257
    const-string v1, "SpenNoteDoc"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2258
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTemplatePageName(I)Ljava/lang/String;
    .locals 4
    .param p1, "templateIndex"    # I

    .prologue
    .line 2276
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getTemplatePageName(I)Ljava/lang/String;

    move-result-object v1

    .line 2277
    .local v1, "templatePageName":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 2278
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2284
    .end local v1    # "templatePageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 2281
    :catch_0
    move-exception v0

    .line 2282
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2283
    const-string v2, "SpenNoteDoc"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2284
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTemplateUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 746
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getTemplateUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 757
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getWidth()I

    move-result v0

    return v0
.end method

.method public hasAttachedFile(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1978
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasAttachedFile(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataByteArray(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1194
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasExtraDataByteArray(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataInt(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1166
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasExtraDataInt(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataString(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1151
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasExtraDataString(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataStringArray(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1180
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasExtraDataStringArray(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasTaggedPage()Z
    .locals 1

    .prologue
    .line 834
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasTaggedPage()Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 2171
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    if-eqz v0, :cond_0

    .line 2172
    iget v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 2174
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    long-to-int v0, v0

    goto :goto_0
.end method

.method public insertPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 4
    .param p1, "pageIndex"    # I

    .prologue
    .line 1340
    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, p1, v1, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_insertPage(IILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    .line 1341
    .local v0, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v0, :cond_0

    .line 1342
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1344
    :cond_0
    return-object v0
.end method

.method public insertPage(IILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 2
    .param p1, "pageIndex"    # I
    .param p2, "backgroundColor"    # I
    .param p3, "backgroundImagePath"    # Ljava/lang/String;
    .param p4, "backgourndImageMode"    # I

    .prologue
    .line 1368
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_insertPage(IILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    .line 1369
    .local v0, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v0, :cond_0

    .line 1370
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1372
    :cond_0
    return-object v0
.end method

.method public insertPages(Ljava/lang/String;II)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 4
    .param p1, "templateName"    # Ljava/lang/String;
    .param p2, "pageIndex"    # I
    .param p3, "count"    # I

    .prologue
    .line 2396
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_insertPages(Ljava/lang/String;II)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    .line 2397
    .local v1, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v1, :cond_0

    .line 2398
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2404
    .end local v1    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_0
    :goto_0
    return-object v1

    .line 2401
    :catch_0
    move-exception v0

    .line 2402
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2403
    const-string v2, "SpenNoteDoc"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2404
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public insertTemplatePage(ILjava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 13
    .param p1, "pageIndex"    # I
    .param p2, "templatePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 1491
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1492
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1493
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_insertTemplatePage(ILjava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v6

    .line 1494
    .local v6, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v6, :cond_0

    .line 1495
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    .line 1503
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v10

    invoke-static {v10}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 1555
    .end local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_0
    :goto_0
    return-object v6

    .line 1497
    .restart local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :sswitch_0
    new-instance v10, Ljava/io/IOException;

    invoke-direct {v10}, Ljava/io/IOException;-><init>()V

    throw v10

    .line 1499
    :sswitch_1
    new-instance v10, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    const-string v11, "It does not correspond to the NoteDoc file format"

    invoke-direct {v10, v11}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1501
    :sswitch_2
    new-instance v10, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "SpenNoteDoc("

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ") is already closed."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1508
    .end local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 1509
    .local v0, "assetMgr":Landroid/content/res/AssetManager;
    invoke-virtual {v0, p2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 1510
    .local v5, "is":Ljava/io/InputStream;
    invoke-virtual {v5}, Ljava/io/InputStream;->available()I

    move-result v4

    .line 1511
    .local v4, "fileSize":I
    new-array v7, v4, [B

    .line 1513
    .local v7, "tempData":[B
    const/4 v10, 0x0

    :try_start_0
    array-length v11, v7

    invoke-virtual {v5, v7, v10, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v10

    array-length v11, v7

    if-eq v10, v11, :cond_2

    .line 1514
    new-instance v10, Ljava/io/IOException;

    const-string v11, "Failed to is.read()"

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1516
    :catch_0
    move-exception v2

    .line 1517
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 1518
    const/4 v6, 0x0

    goto :goto_0

    .line 1520
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 1522
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1524
    .local v1, "byteArrayIS":Ljava/io/ByteArrayInputStream;
    new-instance v8, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getWidth()I

    move-result v11

    const/4 v12, 0x0

    invoke-direct {v8, v10, v1, v11, v12}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;Ljava/io/InputStream;II)V

    .line 1526
    .local v8, "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v10

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v11

    if-eq v10, v11, :cond_3

    .line 1527
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1528
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1529
    const/4 v10, 0x7

    .line 1530
    const-string v11, "The orientation of the template is not matched with this NoteDoc."

    .line 1529
    invoke-static {v10, v11}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 1533
    :cond_3
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v9

    .line 1534
    .local v9, "templatePage":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v9, :cond_4

    .line 1535
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1536
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1537
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v10

    invoke-static {v10}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 1538
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 1541
    :cond_4
    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v10

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v11

    invoke-direct {p0, p1, v10, v11}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_insertPage(III)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v6

    .line 1542
    .restart local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v6, :cond_5

    .line 1543
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1544
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1545
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v10

    invoke-static {v10}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 1546
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 1549
    :cond_5
    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->copy(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 1550
    invoke-virtual {v6, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setTemplateUri(Ljava/lang/String;)V

    .line 1551
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearChangedFlagOfLayer()V

    .line 1553
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1554
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    goto/16 :goto_0

    .line 1495
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0xd -> :sswitch_1
        0x13 -> :sswitch_2
    .end sparse-switch
.end method

.method public isAllPageTextOnly()Z
    .locals 1

    .prologue
    .line 846
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_isAllPageTextOnly()Z

    move-result v0

    return v0
.end method

.method public isChanged()Z
    .locals 1

    .prologue
    .line 1899
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_isChanged()Z

    move-result v0

    return v0
.end method

.method public movePageIndex(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;I)V
    .locals 1
    .param p1, "page"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "step"    # I

    .prologue
    .line 1812
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_movePageIndex(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1813
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1815
    :cond_0
    return-void
.end method

.method public removeExtraDataByteArray(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1244
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_removeExtraDataByteArray(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1245
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1247
    :cond_0
    return-void
.end method

.method public removeExtraDataInt(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1218
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_removeExtraDataInt(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1219
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1221
    :cond_0
    return-void
.end method

.method public removeExtraDataString(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1205
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_removeExtraDataString(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1206
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1208
    :cond_0
    return-void
.end method

.method public removeExtraDataStringArray(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1231
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_removeExtraDataStringArray(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1232
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1234
    :cond_0
    return-void
.end method

.method public removePage(I)V
    .locals 1
    .param p1, "pageIndex"    # I

    .prologue
    .line 1788
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_removePage(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1789
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1791
    :cond_0
    return-void
.end method

.method public restoreObjectList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2140
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_restoreObjectList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public revertToTemplatePage(I)V
    .locals 25
    .param p1, "pageIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 1575
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v15

    .line 1576
    .local v15, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getTemplateUri()Ljava/lang/String;

    move-result-object v21

    .line 1578
    .local v21, "templatePath":Ljava/lang/String;
    if-nez v21, :cond_1

    .line 1579
    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V

    .line 1695
    :cond_0
    :goto_0
    return-void

    .line 1583
    :cond_1
    const/4 v10, 0x0

    .line 1585
    .local v10, "flagTemplatePage":Z
    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasTemplatePage(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v22

    if-eqz v22, :cond_2

    .line 1586
    const/4 v10, 0x1

    .line 1597
    :cond_2
    :goto_1
    if-eqz v10, :cond_3

    .line 1598
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasTemplatePage(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 1599
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_revertToTemplatePage(I)Z

    move-result v16

    .line 1600
    .local v16, "rnt":Z
    if-nez v16, :cond_0

    .line 1601
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v22

    sparse-switch v22, :sswitch_data_0

    .line 1609
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 1588
    .end local v16    # "rnt":Z
    :catch_0
    move-exception v7

    .line 1589
    .local v7, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v7}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 1590
    const-string v22, "SpenNoteDoc"

    const-string v23, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1592
    const-string v22, "template_name://"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 1593
    const/4 v10, 0x1

    goto :goto_1

    .line 1603
    .end local v7    # "e":Ljava/lang/UnsatisfiedLinkError;
    .restart local v16    # "rnt":Z
    :sswitch_0
    new-instance v22, Ljava/io/IOException;

    invoke-direct/range {v22 .. v22}, Ljava/io/IOException;-><init>()V

    throw v22

    .line 1605
    :sswitch_1
    new-instance v22, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    const-string v23, "It does not correspond to the NoteDoc file format"

    invoke-direct/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 1607
    :sswitch_2
    new-instance v22, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "SpenNoteDoc("

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ") is already closed."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 1616
    .end local v16    # "rnt":Z
    :cond_3
    new-instance v8, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1618
    .local v8, "file":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v22

    if-eqz v22, :cond_4

    .line 1619
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_revertToTemplatePage(I)Z

    move-result v16

    .line 1620
    .restart local v16    # "rnt":Z
    if-nez v16, :cond_0

    .line 1621
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v22

    sparse-switch v22, :sswitch_data_1

    .line 1629
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto/16 :goto_0

    .line 1623
    :sswitch_3
    new-instance v22, Ljava/io/IOException;

    invoke-direct/range {v22 .. v22}, Ljava/io/IOException;-><init>()V

    throw v22

    .line 1625
    :sswitch_4
    new-instance v22, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    const-string v23, "It does not correspond to the NoteDoc file format"

    invoke-direct/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 1627
    :sswitch_5
    new-instance v22, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "SpenNoteDoc("

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ") is already closed."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 1634
    .end local v16    # "rnt":Z
    :cond_4
    const/16 v18, 0x0

    .line 1635
    .local v18, "tempData":[B
    const/4 v12, 0x0

    .line 1636
    .local v12, "is":Ljava/io/InputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    .line 1638
    .local v4, "assetMgr":Landroid/content/res/AssetManager;
    :try_start_1
    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v12

    .line 1639
    invoke-virtual {v12}, Ljava/io/InputStream;->available()I

    move-result v9

    .line 1640
    .local v9, "fileSize":I
    if-nez v9, :cond_6

    .line 1641
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V

    .line 1642
    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1652
    .end local v9    # "fileSize":I
    :catch_1
    move-exception v7

    .line 1653
    .local v7, "e":Ljava/io/IOException;
    if-eqz v12, :cond_5

    .line 1654
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V

    .line 1656
    :cond_5
    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V

    goto/16 :goto_0

    .line 1645
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v9    # "fileSize":I
    :cond_6
    :try_start_2
    new-array v0, v9, [B

    move-object/from16 v18, v0

    .line 1647
    const/16 v22, 0x0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v12, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v22

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_7

    .line 1648
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V

    .line 1649
    new-instance v22, Ljava/io/IOException;

    const-string v23, "Failed to is.read()"

    invoke-direct/range {v22 .. v23}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 1651
    :cond_7
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1660
    new-instance v5, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1662
    .local v5, "byteArrayIS":Ljava/io/ByteArrayInputStream;
    new-instance v19, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getWidth()I

    move-result v23

    const/16 v24, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;Ljava/io/InputStream;II)V

    .line 1664
    .local v19, "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v22

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_8

    .line 1665
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1666
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1667
    const/16 v22, 0x7

    .line 1668
    const-string v23, "The orientation of the template is not matched with this NoteDoc."

    .line 1667
    invoke-static/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 1671
    :cond_8
    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V

    .line 1673
    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v20

    .line 1675
    .local v20, "templatePage":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v20, :cond_9

    .line 1676
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1677
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1678
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto/16 :goto_0

    .line 1682
    :cond_9
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObjectList()Ljava/util/ArrayList;

    move-result-object v13

    .line 1683
    .local v13, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1684
    .local v6, "count":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    if-lt v11, v6, :cond_a

    .line 1693
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1694
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    goto/16 :goto_0

    .line 1685
    :cond_a
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v17

    .line 1686
    .local v17, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->createObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v14

    .line 1687
    .local v14, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-eqz v14, :cond_b

    .line 1688
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1689
    invoke-virtual {v15, v14}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1684
    :cond_b
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1601
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0xd -> :sswitch_1
        0x13 -> :sswitch_2
    .end sparse-switch

    .line 1621
    :sswitch_data_1
    .sparse-switch
        0xb -> :sswitch_3
        0xd -> :sswitch_4
        0x13 -> :sswitch_5
    .end sparse-switch
.end method

.method public revertToTemplatePage(ILjava/lang/String;)V
    .locals 15
    .param p1, "pageIndex"    # I
    .param p2, "absolutePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 1716
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v7

    .line 1718
    .local v7, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V

    .line 1720
    if-nez p2, :cond_0

    .line 1770
    :goto_0
    return-void

    .line 1724
    :cond_0
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1726
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1728
    const/4 v9, 0x0

    .line 1730
    .local v9, "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :try_start_0
    new-instance v10, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getWidth()I

    move-result v13

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-direct {v10, v12, v0, v13, v14}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;Ljava/lang/String;II)V
    :try_end_0
    .catch Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException; {:try_start_0 .. :try_end_0} :catch_1

    .end local v9    # "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    .local v10, "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    move-object v9, v10

    .line 1739
    .end local v10    # "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    .restart local v9    # "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :goto_1
    if-nez v9, :cond_1

    .line 1740
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v12

    invoke-static {v12}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 1731
    :catch_0
    move-exception v2

    .line 1733
    .local v2, "e":Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;->printStackTrace()V

    goto :goto_1

    .line 1734
    .end local v2    # "e":Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;
    :catch_1
    move-exception v2

    .line 1736
    .local v2, "e":Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;->printStackTrace()V

    goto :goto_1

    .line 1744
    .end local v2    # "e":Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v12

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v13

    if-eq v12, v13, :cond_2

    .line 1745
    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1746
    const/4 v12, 0x7

    .line 1747
    const-string v13, "The orientation of the template is not matched with this NoteDoc."

    .line 1746
    invoke-static {v12, v13}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 1750
    :cond_2
    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v11

    .line 1752
    .local v11, "templatePage":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v11, :cond_3

    .line 1753
    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1754
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v12

    invoke-static {v12}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 1758
    :cond_3
    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObjectList()Ljava/util/ArrayList;

    move-result-object v5

    .line 1759
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 1760
    .local v1, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-lt v4, v1, :cond_4

    .line 1769
    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    goto :goto_0

    .line 1761
    :cond_4
    invoke-virtual {v11, v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v8

    .line 1762
    .local v8, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v12

    invoke-virtual {v7, v12}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->createObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v6

    .line 1763
    .local v6, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-eqz v6, :cond_5

    .line 1764
    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1765
    invoke-virtual {v7, v6}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1760
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1772
    .end local v1    # "count":I
    .end local v4    # "i":I
    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    .end local v6    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .end local v8    # "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .end local v9    # "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    .end local v11    # "templatePage":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_6
    new-instance v12, Ljava/io/IOException;

    const-string v13, "The file does not exist."

    invoke-direct {v12, v13}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v12
.end method

.method public reviseObjectList(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2457
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 2458
    :cond_0
    const/4 v4, 0x7

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 2460
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2461
    .local v3, "reviseObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_2

    .line 2471
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_5

    .line 2479
    .end local v1    # "i":I
    .end local v3    # "reviseObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :goto_1
    return-void

    .line 2462
    .restart local v1    # "i":I
    .restart local v3    # "reviseObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :cond_2
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 2463
    .local v2, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-eqz v2, :cond_3

    .line 2464
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    .line 2465
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2461
    .end local v2    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2466
    .restart local v2    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_4
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_3

    .line 2467
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .end local v2    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->reviseObjectList(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2475
    .end local v1    # "i":I
    .end local v3    # "reviseObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :catch_0
    move-exception v0

    .line 2476
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2477
    const-string v4, "SpenNoteDoc"

    const-string v5, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2474
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    .restart local v1    # "i":I
    .restart local v3    # "reviseObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :cond_5
    :try_start_1
    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_reviseObjectList(Ljava/util/ArrayList;)Z
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public save(Ljava/io/OutputStream;)V
    .locals 4
    .param p1, "stream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1996
    const/4 v0, 0x0

    .line 1997
    .local v0, "rnt":Z
    instance-of v1, p1, Ljava/io/ByteArrayOutputStream;

    if-eqz v1, :cond_1

    .line 1998
    check-cast p1, Ljava/io/ByteArrayOutputStream;

    .end local p1    # "stream":Ljava/io/OutputStream;
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_save(Ljava/io/ByteArrayOutputStream;)Z

    move-result v0

    .line 2015
    :goto_0
    if-nez v0, :cond_0

    .line 2016
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2025
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2028
    :cond_0
    :goto_1
    return-void

    .line 1999
    .restart local p1    # "stream":Ljava/io/OutputStream;
    :cond_1
    instance-of v1, p1, Ljava/io/FileOutputStream;

    if-eqz v1, :cond_2

    .line 2001
    check-cast p1, Ljava/io/FileOutputStream;

    .end local p1    # "stream":Ljava/io/OutputStream;
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_save(Ljava/io/FileDescriptor;)Z

    move-result v0

    .line 2002
    goto :goto_0

    .line 2010
    .restart local p1    # "stream":Ljava/io/OutputStream;
    :cond_2
    const/4 v1, 0x7

    .line 2011
    const-string v2, "The parameter \'stream\' is unsupported type. This method supports only ByteArrayOutputStream and FileOutputStream"

    .line 2010
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1

    .line 2018
    .end local p1    # "stream":Ljava/io/OutputStream;
    :sswitch_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 2023
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SpenNoteDoc("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is already closed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2016
    nop

    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public save(Ljava/lang/String;)V
    .locals 4
    .param p1, "filepath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2042
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_save(Ljava/lang/String;)Z

    move-result v0

    .line 2044
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2045
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2054
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2057
    :cond_0
    return-void

    .line 2047
    :sswitch_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 2052
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SpenNoteDoc("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is already closed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2045
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 927
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setAppName(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 928
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 930
    :cond_0
    return-void
.end method

.method public setAppVersion(IILjava/lang/String;)V
    .locals 1
    .param p1, "majorVersion"    # I
    .param p2, "minorVersion"    # I
    .param p3, "patchName"    # Ljava/lang/String;

    .prologue
    .line 959
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setAppVersion(IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 960
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 962
    :cond_0
    return-void
.end method

.method public setAuthorInfo(Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;

    .prologue
    .line 858
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setAuthorInfo(Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 859
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 861
    :cond_0
    return-void
.end method

.method public setCoverImage(Ljava/lang/String;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 808
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setCoverImage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 809
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 811
    :cond_0
    return-void
.end method

.method public setExtraDataByteArray(Ljava/lang/String;[B)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 1073
    if-eqz p2, :cond_1

    .line 1074
    array-length v0, p2

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setExtraDataByteArray(Ljava/lang/String;[BI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1075
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1082
    :cond_0
    :goto_0
    return-void

    .line 1078
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setExtraDataByteArray(Ljava/lang/String;[BI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1079
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    goto :goto_0
.end method

.method public setExtraDataInt(Ljava/lang/String;I)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 1031
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setExtraDataInt(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1032
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1034
    :cond_0
    return-void
.end method

.method public setExtraDataString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1013
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setExtraDataString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1014
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1016
    :cond_0
    return-void
.end method

.method public setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [Ljava/lang/String;

    .prologue
    .line 1049
    if-eqz p2, :cond_1

    .line 1050
    array-length v0, p2

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1051
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1058
    :cond_0
    :goto_0
    return-void

    .line 1054
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1055
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    goto :goto_0
.end method

.method public setGeoTag(DD)V
    .locals 1
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 889
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setGeoTag(DD)Z

    move-result v0

    if-nez v0, :cond_0

    .line 890
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 892
    :cond_0
    return-void
.end method

.method public setTemplateUri(Ljava/lang/String;)V
    .locals 1
    .param p1, "templateUri"    # Ljava/lang/String;

    .prologue
    .line 732
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setTemplateUri(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 733
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 735
    :cond_0
    return-void
.end method
