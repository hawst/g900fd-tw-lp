.class public Lcom/samsung/android/sdk/pen/engine/SpenMultiView;
.super Landroid/view/View;
.source "SpenMultiView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;
    }
.end annotation


# static fields
.field public static final PAGE_TRANSITION_EFFECT_LEFT:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_RIGHT:I = 0x1

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SHADOW:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SLIDE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SpenMultiView"

.field private static cancelStroke:I = 0x0

.field private static final penNameBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Brush"

.field private static final penNameChineseBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

.field private static final penNameFountainPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.FountainPen"

.field private static final penNameInkPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.InkPen"

.field private static final penNameMagicPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.MagicPen"

.field private static final penNameMarker:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Marker"

.field private static final penNameObliquePen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

.field private static final penNamePencil:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Pencil"

.field private static requestAllocateLayer:I

.field private static requestReleaseLayer:I

.field private static setLayerBitmap:I

.field private static setLayerCount:I


# instance fields
.field private activePen:I

.field private bIsSupport:Z

.field private isEraserCursor:Z

.field private isSkipTouch:Z

.field private localUserId:I

.field private mAntiAliasPaint:Landroid/graphics/Paint;

.field private mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

.field private mBlackPaint:Landroid/graphics/Paint;

.field private mCanvasHeight:I

.field private mCanvasWidth:I

.field private mCirclePaint:Landroid/graphics/Paint;

.field private mCirclePoint:Landroid/graphics/PointF;

.field private mCircleRadius:F

.field private mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

.field private mContext:Landroid/content/Context;

.field private mDebugPaint:Landroid/graphics/Paint;

.field private mDeltaX:F

.field private mDeltaY:F

.field private mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

.field private mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

.field private mFBCanvas:Landroid/graphics/Canvas;

.field private mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

.field private mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

.field private mFloatingLayerUsed:[Z

.field private mFloatingLayerUsedId:[I

.field private mFrameBuffer:Landroid/graphics/Bitmap;

.field private mFrameHeight:I

.field private mFrameStartX:I

.field private mFrameStartY:I

.field private mFrameWidth:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

.field private mHoverDrawable:Landroid/graphics/drawable/Drawable;

.field private mHoverEnable:Z

.field private mHoverIconID:I

.field private mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

.field private mHoverPoint:Landroid/graphics/Point;

.field private mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

.field private mIndexBrush:I

.field private mIndexEraser:I

.field private mIndexMarker:I

.field private mIndexPencil:I

.field private mIs64:Z

.field private mIsCancelFling:Z

.field private mIsDoubleTap:Z

.field private mIsToolTip:Z

.field private mMagicPenEnabled:Z

.field private mMaxDeltaX:F

.field private mMaxDeltaY:F

.field private mNativeMulti:J

.field private mNumberFloatingLayer:I

.field private mOldX:F

.field private mOldY:F

.field private mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

.field private mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

.field private mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

.field private mParentRect:Landroid/graphics/Rect;

.field private mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

.field private mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

.field private mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private mRatio:F

.field private mRatioCanvasWidth:I

.field private mRtoCvsItstFrmHeight:I

.field private mRtoCvsItstFrmWidth:I

.field private mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

.field private mSmps:Lcom/samsung/audio/SmpsManager;

.field private mThreadId:J

.field private mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

.field private mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private mTouchProcessingTime:J

.field private mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setLayerCount:I

    .line 55
    const/4 v0, 0x3

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setLayerBitmap:I

    .line 56
    const/4 v0, 0x4

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->requestAllocateLayer:I

    .line 57
    const/4 v0, 0x5

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->requestReleaseLayer:I

    .line 58
    const/4 v0, 0x6

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->cancelStroke:I

    .line 201
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 225
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 64
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 65
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    .line 67
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    .line 69
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    .line 70
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    .line 71
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    .line 73
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    .line 74
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    .line 76
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    .line 78
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    .line 79
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    .line 81
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    .line 82
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    .line 83
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 85
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 86
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 87
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 89
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    .line 90
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 91
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .line 92
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 93
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .line 94
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 95
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 96
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 97
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 98
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 99
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 101
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    .line 102
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    .line 103
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    .line 104
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    .line 106
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    .line 107
    new-instance v2, Landroid/graphics/PointF;

    const/high16 v3, -0x3d380000    # -100.0f

    const/high16 v4, -0x3d380000    # -100.0f

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    .line 108
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    .line 109
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isEraserCursor:Z

    .line 111
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 112
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 113
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 114
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 115
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverEnable:Z

    .line 116
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverIconID:I

    .line 117
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 118
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    .line 119
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    .line 120
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsDoubleTap:Z

    .line 122
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsCancelFling:Z

    .line 125
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 127
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldX:F

    .line 128
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldY:F

    .line 131
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    .line 132
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 133
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    .line 134
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexMarker:I

    .line 135
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexBrush:I

    .line 136
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexEraser:I

    .line 137
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    .line 147
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    .line 226
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    .line 227
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    .line 228
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_init()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    .line 229
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->construct()V

    .line 230
    return-void

    :cond_0
    move v0, v1

    .line 227
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 258
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 65
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    .line 67
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    .line 69
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    .line 70
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    .line 71
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    .line 73
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    .line 74
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    .line 76
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    .line 78
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    .line 79
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    .line 81
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    .line 82
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    .line 83
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 85
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 86
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 87
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 89
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    .line 90
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 91
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .line 92
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 93
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .line 94
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 95
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 96
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 97
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 98
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 99
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 101
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    .line 102
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    .line 103
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    .line 104
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    .line 106
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    .line 107
    new-instance v2, Landroid/graphics/PointF;

    const/high16 v3, -0x3d380000    # -100.0f

    const/high16 v4, -0x3d380000    # -100.0f

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    .line 108
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    .line 109
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isEraserCursor:Z

    .line 111
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 112
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 113
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 114
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 115
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverEnable:Z

    .line 116
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverIconID:I

    .line 117
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 118
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    .line 119
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    .line 120
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsDoubleTap:Z

    .line 122
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsCancelFling:Z

    .line 125
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 127
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldX:F

    .line 128
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldY:F

    .line 131
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    .line 132
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 133
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    .line 134
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexMarker:I

    .line 135
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexBrush:I

    .line 136
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexEraser:I

    .line 137
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    .line 147
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    .line 259
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    .line 260
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    .line 261
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_init()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    .line 262
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->construct()V

    .line 263
    return-void

    :cond_0
    move v0, v1

    .line 260
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 302
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 65
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    .line 67
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    .line 69
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    .line 70
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    .line 71
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    .line 73
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    .line 74
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    .line 76
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    .line 78
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    .line 79
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    .line 81
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    .line 82
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    .line 83
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 85
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 86
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 87
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 89
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    .line 90
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 91
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .line 92
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 93
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .line 94
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 95
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 96
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 97
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 98
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 99
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 101
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    .line 102
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    .line 103
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    .line 104
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    .line 106
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    .line 107
    new-instance v2, Landroid/graphics/PointF;

    const/high16 v3, -0x3d380000    # -100.0f

    const/high16 v4, -0x3d380000    # -100.0f

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    .line 108
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    .line 109
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isEraserCursor:Z

    .line 111
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 112
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 113
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 114
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 115
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverEnable:Z

    .line 116
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverIconID:I

    .line 117
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 118
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    .line 119
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    .line 120
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsDoubleTap:Z

    .line 122
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsCancelFling:Z

    .line 125
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 127
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldX:F

    .line 128
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldY:F

    .line 131
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    .line 132
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 133
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    .line 134
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexMarker:I

    .line 135
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexBrush:I

    .line 136
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexEraser:I

    .line 137
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    .line 147
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    .line 303
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    .line 304
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    .line 305
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_init()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    .line 306
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->construct()V

    .line 307
    return-void

    :cond_0
    move v0, v1

    .line 304
    goto :goto_0
.end method

.method private Native_addUser(JI)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3336
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3337
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_addUser(JI)Z

    move-result v0

    .line 3339
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_addUser(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "command"    # I
    .param p5, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3352
    .local p4, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3353
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 3355
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "view"    # Lcom/samsung/android/sdk/pen/engine/SpenMultiView;
    .param p5, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 3072
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3073
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z

    move-result v0

    .line 3075
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_construct(ILandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_enablePenCurve(JIZ)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "curve"    # Z

    .prologue
    .line 3256
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3257
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_enablePenCurve(JIZ)V

    .line 3261
    :goto_0
    return-void

    .line 3259
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_enablePenCurve(IIZ)V

    goto :goto_0
.end method

.method private Native_enableZoom(JZ)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "mode"    # Z

    .prologue
    .line 3128
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3129
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_enableZoom(JZ)V

    .line 3133
    :goto_0
    return-void

    .line 3131
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_enableZoom(IZ)V

    goto :goto_0
.end method

.method private Native_finalize(J)V
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3056
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3057
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_finalize(J)V

    .line 3061
    :goto_0
    return-void

    .line 3059
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_finalize(I)V

    goto :goto_0
.end method

.method private Native_getAdvancedSetting(JI)Ljava/lang/String;
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3280
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3281
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getAdvancedSetting(JI)Ljava/lang/String;

    move-result-object v0

    .line 3283
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getAdvancedSetting(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_getEraserSize(JI)F
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3296
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3297
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getEraserSize(JI)F

    move-result v0

    .line 3299
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getEraserSize(II)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getLocalUserId(J)I
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3328
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3329
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getLocalUserId(J)I

    move-result v0

    .line 3331
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getLocalUserId(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getMaxZoomRatio(J)F
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3168
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3169
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getMaxZoomRatio(J)F

    move-result v0

    .line 3171
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getMaxZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getMinZoomRatio(J)F
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3184
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3185
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getMinZoomRatio(J)F

    move-result v0

    .line 3187
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getMinZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getPan(JLandroid/graphics/PointF;)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 3200
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3201
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPan(JLandroid/graphics/PointF;)V

    .line 3205
    :goto_0
    return-void

    .line 3203
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPan(ILandroid/graphics/PointF;)V

    goto :goto_0
.end method

.method private Native_getPenColor(JI)I
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3232
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3233
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenColor(JI)I

    move-result v0

    .line 3235
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenColor(II)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getPenSize(JI)F
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3248
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3249
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenSize(JI)F

    move-result v0

    .line 3251
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenSize(II)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getPenStyle(JI)Ljava/lang/String;
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3216
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3217
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenStyle(JI)Ljava/lang/String;

    move-result-object v0

    .line 3219
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenStyle(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_getToolTypeAction(JII)I
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "toolType"    # I

    .prologue
    .line 3120
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3121
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getToolTypeAction(JII)I

    move-result v0

    .line 3123
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getToolTypeAction(III)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getZoomRatio(J)F
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3152
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3153
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getZoomRatio(J)F

    move-result v0

    .line 3155
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_init()J
    .locals 2

    .prologue
    .line 3048
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3049
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_init_64()J

    move-result-wide v0

    .line 3051
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_init()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private Native_isPenCurve(JI)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3264
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3265
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_isPenCurve(JI)Z

    move-result v0

    .line 3267
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_isPenCurve(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_isZoomable(J)Z
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3136
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3137
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_isZoomable(J)Z

    move-result v0

    .line 3139
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_isZoomable(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onHover(JILandroid/view/MotionEvent;I)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "event"    # Landroid/view/MotionEvent;
    .param p5, "toolType"    # I

    .prologue
    .line 3088
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3089
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_onHover(JILandroid/view/MotionEvent;I)Z

    move-result v0

    .line 3091
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_onHover(IILandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onTouch(JILandroid/view/MotionEvent;I)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "event"    # Landroid/view/MotionEvent;
    .param p5, "toolType"    # I

    .prologue
    .line 3080
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3081
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_onTouch(JILandroid/view/MotionEvent;I)Z

    move-result v0

    .line 3083
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_onTouch(IILandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_removeUser(JI)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3344
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3345
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_removeUser(JI)Z

    move-result v0

    .line 3347
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_removeUser(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setAdvancedSetting(JILjava/lang/String;)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "advancedSetting"    # Ljava/lang/String;

    .prologue
    .line 3272
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3273
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setAdvancedSetting(JILjava/lang/String;)V

    .line 3277
    :goto_0
    return-void

    .line 3275
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setAdvancedSetting(IILjava/lang/String;)V

    goto :goto_0
.end method

.method private Native_setBitmap(JLandroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 3096
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3097
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setBitmap(JLandroid/graphics/Bitmap;)V

    .line 3101
    :goto_0
    return-void

    .line 3099
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private Native_setEraserSize(JIF)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "width"    # F

    .prologue
    .line 3288
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3289
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setEraserSize(JIF)Z

    move-result v0

    .line 3291
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setEraserSize(IIF)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setLocalUserId(JI)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3320
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3321
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setLocalUserId(JI)Z

    move-result v0

    .line 3323
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setLocalUserId(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setMaxZoomRatio(JF)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "ratio"    # F

    .prologue
    .line 3160
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3161
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setMaxZoomRatio(JF)Z

    move-result v0

    .line 3163
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setMaxZoomRatio(IF)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setMinZoomRatio(JF)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "ratio"    # F

    .prologue
    .line 3176
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3177
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setMinZoomRatio(JF)Z

    move-result v0

    .line 3179
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setMinZoomRatio(IF)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p4, "isUpdate"    # Z

    .prologue
    .line 3104
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3105
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    .line 3107
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setPan(JFFZ)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "deltaX"    # F
    .param p4, "deltaY"    # F
    .param p5, "isUpdate"    # Z

    .prologue
    .line 3192
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3193
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPan(JFFZ)V

    .line 3197
    :goto_0
    return-void

    .line 3195
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPan(IFFZ)V

    goto :goto_0
.end method

.method private Native_setPenColor(JII)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "color"    # I

    .prologue
    .line 3224
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3225
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenColor(JII)V

    .line 3229
    :goto_0
    return-void

    .line 3227
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenColor(III)V

    goto :goto_0
.end method

.method private Native_setPenSize(JIF)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "width"    # F

    .prologue
    .line 3240
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3241
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenSize(JIF)V

    .line 3245
    :goto_0
    return-void

    .line 3243
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenSize(IIF)V

    goto :goto_0
.end method

.method private Native_setPenStyle(JILjava/lang/String;)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "style"    # Ljava/lang/String;

    .prologue
    .line 3208
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3209
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenStyle(JILjava/lang/String;)Z

    move-result v0

    .line 3211
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenStyle(IILjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setScreenSize(JII)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 3064
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3065
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setScreenSize(JII)V

    .line 3069
    :goto_0
    return-void

    .line 3067
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setScreenSize(III)V

    goto :goto_0
.end method

.method private Native_setToolTypeAction(JIII)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "toolType"    # I
    .param p5, "action"    # I

    .prologue
    .line 3112
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3113
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setToolTypeAction(JIII)Z

    move-result v0

    .line 3115
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setToolTypeAction(IIII)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setZoom(JFFF)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "centerX"    # F
    .param p4, "centerY"    # F
    .param p5, "ratio"    # F

    .prologue
    .line 3144
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3145
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setZoom(JFFF)V

    .line 3149
    :goto_0
    return-void

    .line 3147
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setZoom(IFFF)V

    goto :goto_0
.end method

.method private Native_update(J)Z
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3304
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3305
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_update(J)Z

    move-result v0

    .line 3307
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_update(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_updateHistory(J)Z
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3312
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3313
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_updateHistory(J)Z

    move-result v0

    .line 3315
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_updateHistory(I)Z

    move-result v0

    goto :goto_0
.end method

.method private absoluteCoordinate(Landroid/graphics/Rect;FFFF)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/Rect;
    .param p2, "l"    # F
    .param p3, "t"    # F
    .param p4, "r"    # F
    .param p5, "b"    # F

    .prologue
    .line 629
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float v0, p2, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 630
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float v0, p4, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 631
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float v0, p3, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 632
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float v0, p5, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 633
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/Canvas;Z)V
    .locals 0

    .prologue
    .line 499
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas(Landroid/graphics/Canvas;Z)V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 488
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas2(Landroid/graphics/Canvas;)V

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsDoubleTap:Z

    return v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    return v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    return v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    return-wide v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;JFFZ)V
    .locals 1

    .prologue
    .line 3191
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPan(JFFZ)V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;Z)V
    .locals 0

    .prologue
    .line 547
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsCancelFling:Z

    return v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    return-object v0
.end method

.method private construct()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 310
    const-string v1, "SpenMultiView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "nativeMulti = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 312
    const-string v1, " : nativeMulti must not be null"

    invoke-static {v9, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 358
    :goto_0
    return-void

    .line 315
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 316
    const-string v1, " : context must not be null"

    invoke-static {v9, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 320
    :cond_1
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 321
    .local v6, "rect":Landroid/graphics/RectF;
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 322
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 325
    :cond_2
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    .line 326
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 327
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    const v2, -0xf2c5b1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 329
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    .line 330
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 331
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 333
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 334
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 336
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    .line 337
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 338
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 339
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 341
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;

    invoke-direct {v3, p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;)V

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 342
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;

    invoke-direct {v2, p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;)V

    invoke-virtual {v1, v2}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 343
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 344
    .local v0, "mDisplayMetrics":Landroid/util/DisplayMetrics;
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    iget v3, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v3, v3

    .line 345
    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;-><init>(Landroid/content/Context;FF)V

    .line 344
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 346
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;

    invoke-direct {v2, p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;)V

    .line 348
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;

    invoke-direct {v2, p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;)V

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 349
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setPaint(Landroid/graphics/Paint;)V

    .line 351
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 352
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 354
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    .line 356
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->initHapticFeedback()V

    .line 357
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->registerPensoundSolution()V

    goto/16 :goto_0
.end method

.method private convertPenNameToMaxThicknessValue(Ljava/lang/String;)I
    .locals 2
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 863
    const/4 v0, 0x0

    .line 865
    .local v0, "maxValue":I
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 866
    :cond_0
    const/16 v0, 0x40

    .line 877
    :cond_1
    :goto_0
    return v0

    .line 867
    :cond_2
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 868
    :cond_3
    const/16 v0, 0x20

    .line 869
    goto :goto_0

    :cond_4
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 870
    :cond_5
    const/16 v0, 0x50

    .line 871
    goto :goto_0

    :cond_6
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 872
    const/16 v0, 0x6c

    .line 873
    goto :goto_0

    :cond_7
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "Eraser"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 874
    :cond_8
    const/16 v0, 0x64

    goto :goto_0
.end method

.method private createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 10
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    const/4 v9, 0x6

    const/4 v8, 0x2

    .line 435
    if-eqz p1, :cond_0

    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    .line 486
    :cond_0
    :goto_0
    return-void

    .line 439
    :cond_1
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    .line 440
    .local v3, "width":I
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 441
    .local v2, "height":I
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    .line 442
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 443
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-nez v4, :cond_2

    .line 444
    const-string v4, "The width of pageDoc is 0"

    invoke-static {v9, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 447
    :cond_2
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    if-nez v4, :cond_3

    .line 448
    const-string v4, "The height of pageDoc is 0"

    invoke-static {v9, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 451
    :cond_3
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-ne v4, v3, :cond_4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    if-eq v4, v2, :cond_0

    .line 454
    :cond_4
    const-string v4, "SpenMultiView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "createBitmap Width="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Height="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_5

    .line 458
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 460
    :cond_5
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    .line 461
    new-instance v4, Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    .line 462
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 463
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-direct {p0, v4, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setBitmap(JLandroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 468
    :goto_1
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_2
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-lt v0, v4, :cond_6

    .line 485
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->deltaZoomSizeChanged()V

    goto/16 :goto_0

    .line 464
    .end local v0    # "cnt":I
    :catch_0
    move-exception v1

    .line 465
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "Failed to create bitmap of frame buffer"

    invoke-static {v8, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1

    .line 469
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "cnt":I
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v0

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_7

    .line 470
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 472
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    aput-object v5, v4, v0

    .line 473
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-lez v4, :cond_8

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    if-lez v4, :cond_8

    .line 475
    :try_start_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 476
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 475
    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    aput-object v5, v4, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 481
    :goto_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v0

    invoke-direct {p0, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setLayerBitmap(ILandroid/graphics/Bitmap;)Z

    .line 468
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 477
    :catch_1
    move-exception v1

    .line 479
    .restart local v1    # "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to create bitmap of frame buffer mFloatingLayerBitmap["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 478
    invoke-static {v8, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_3
.end method

.method private deltaZoomSizeChanged()V
    .locals 5

    .prologue
    .line 577
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getVariableForOnUpdateCanvas()V

    .line 579
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onZoom(FFF)V

    .line 581
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setLimitHeight(FF)V

    .line 582
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    .line 583
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    .line 582
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setDrawInformation(IIII)V

    .line 586
    :cond_0
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onZoom. dx : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", dy : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", r : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", MaxDx : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 587
    const-string v2, ", MaxDy : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 586
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    return-void
.end method

.method private getVariableForOnUpdateCanvas()V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 556
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    .line 557
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_0

    .line 558
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    .line 561
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    .line 562
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_1

    .line 563
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    .line 566
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    .line 567
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 569
    .local v0, "mRatioCanvasHeight":I
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    if-ge v1, v2, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    :goto_0
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    .line 570
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    if-ge v0, v1, :cond_3

    .end local v0    # "mRatioCanvasHeight":I
    :goto_1
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    .line 572
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    .line 573
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    .line 574
    return-void

    .line 569
    .restart local v0    # "mRatioCanvasHeight":I
    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    goto :goto_0

    .line 570
    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    goto :goto_1
.end method

.method private initHapticFeedback()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 798
    const-string v2, "SpenMultiView"

    const-string v3, "initHapticFeedback() - Start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-nez v2, :cond_0

    .line 801
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 802
    .local v0, "dm":Landroid/util/DisplayMetrics;
    new-instance v2, Lcom/samsung/hapticfeedback/HapticEffect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/hapticfeedback/HapticEffect;-><init>(Landroid/content/Context;II)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 811
    .end local v0    # "dm":Landroid/util/DisplayMetrics;
    :cond_0
    :goto_0
    const-string v2, "SpenMultiView"

    const-string v3, "initHapticFeedback() - End"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    return-void

    .line 803
    :catch_0
    move-exception v1

    .line 804
    .local v1, "error":Ljava/lang/UnsatisfiedLinkError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 805
    const-string v2, "TAG"

    const-string v3, "Haptic Effect UnsatisfiedLinkError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 806
    .end local v1    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v1

    .line 807
    .local v1, "error":Ljava/lang/NoClassDefFoundError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 808
    const-string v2, "TAG"

    const-string v3, "Haptic Effect NoClassDefFoundError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static native native_addUser(II)Z
.end method

.method private static native native_addUser(JI)Z
.end method

.method private static native native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_construct(ILandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z
.end method

.method private static native native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z
.end method

.method private static native native_enablePenCurve(IIZ)V
.end method

.method private static native native_enablePenCurve(JIZ)V
.end method

.method private static native native_enableZoom(IZ)V
.end method

.method private static native native_enableZoom(JZ)V
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_finalize(J)V
.end method

.method private static native native_getAdvancedSetting(II)Ljava/lang/String;
.end method

.method private static native native_getAdvancedSetting(JI)Ljava/lang/String;
.end method

.method private static native native_getEraserSize(II)F
.end method

.method private static native native_getEraserSize(JI)F
.end method

.method private static native native_getLocalUserId(I)I
.end method

.method private static native native_getLocalUserId(J)I
.end method

.method private static native native_getMaxZoomRatio(I)F
.end method

.method private static native native_getMaxZoomRatio(J)F
.end method

.method private static native native_getMinZoomRatio(I)F
.end method

.method private static native native_getMinZoomRatio(J)F
.end method

.method private static native native_getPan(ILandroid/graphics/PointF;)V
.end method

.method private static native native_getPan(JLandroid/graphics/PointF;)V
.end method

.method private static native native_getPenColor(II)I
.end method

.method private static native native_getPenColor(JI)I
.end method

.method private static native native_getPenSize(II)F
.end method

.method private static native native_getPenSize(JI)F
.end method

.method private static native native_getPenStyle(II)Ljava/lang/String;
.end method

.method private static native native_getPenStyle(JI)Ljava/lang/String;
.end method

.method private static native native_getToolTypeAction(III)I
.end method

.method private static native native_getToolTypeAction(JII)I
.end method

.method private static native native_getZoomRatio(I)F
.end method

.method private static native native_getZoomRatio(J)F
.end method

.method private static native native_init()I
.end method

.method private static native native_init_64()J
.end method

.method private static native native_isPenCurve(II)Z
.end method

.method private static native native_isPenCurve(JI)Z
.end method

.method private static native native_isZoomable(I)Z
.end method

.method private static native native_isZoomable(J)Z
.end method

.method private static native native_onHover(IILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onHover(JILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onTouch(IILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onTouch(JILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_removeUser(II)Z
.end method

.method private static native native_removeUser(JI)Z
.end method

.method private static native native_setAdvancedSetting(IILjava/lang/String;)V
.end method

.method private static native native_setAdvancedSetting(JILjava/lang/String;)V
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setBitmap(JLandroid/graphics/Bitmap;)V
.end method

.method private static native native_setEraserSize(IIF)Z
.end method

.method private static native native_setEraserSize(JIF)Z
.end method

.method private static native native_setLocalUserId(II)Z
.end method

.method private static native native_setLocalUserId(JI)Z
.end method

.method private static native native_setMaxZoomRatio(IF)Z
.end method

.method private static native native_setMaxZoomRatio(JF)Z
.end method

.method private static native native_setMinZoomRatio(IF)Z
.end method

.method private static native native_setMinZoomRatio(JF)Z
.end method

.method private static native native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
.end method

.method private static native native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
.end method

.method private static native native_setPan(IFFZ)V
.end method

.method private static native native_setPan(JFFZ)V
.end method

.method private static native native_setPenColor(III)V
.end method

.method private static native native_setPenColor(JII)V
.end method

.method private static native native_setPenSize(IIF)V
.end method

.method private static native native_setPenSize(JIF)V
.end method

.method private static native native_setPenStyle(IILjava/lang/String;)Z
.end method

.method private static native native_setPenStyle(JILjava/lang/String;)Z
.end method

.method private static native native_setScreenSize(III)V
.end method

.method private static native native_setScreenSize(JII)V
.end method

.method private static native native_setToolTypeAction(IIII)Z
.end method

.method private static native native_setToolTypeAction(JIII)Z
.end method

.method private static native native_setZoom(IFFF)V
.end method

.method private static native native_setZoom(JFFF)V
.end method

.method private static native native_update(I)Z
.end method

.method private static native native_update(J)Z
.end method

.method private static native native_updateHistory(I)Z
.end method

.method private static native native_updateHistory(J)Z
.end method

.method private onColorPickerChanged(III)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "color"    # I

    .prologue
    .line 605
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onColorPickerChanged color"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    if-eqz v0, :cond_0

    .line 607
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    invoke-interface {v0, p3, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;->onChanged(III)V

    .line 609
    :cond_0
    return-void
.end method

.method private onUpdateCanvas(Landroid/graphics/RectF;Z)V
    .locals 4
    .param p1, "abRect"    # Landroid/graphics/RectF;
    .param p2, "isScreenFramebuffer"    # Z

    .prologue
    .line 548
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 549
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->invalidate()V

    .line 553
    :goto_0
    return-void

    .line 551
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->postInvalidate()V

    goto :goto_0
.end method

.method private onZoom(FFF)V
    .locals 1
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 593
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    .line 594
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    .line 595
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    .line 597
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->deltaZoomSizeChanged()V

    .line 599
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;->onZoom(FFF)V

    .line 602
    :cond_0
    return-void
.end method

.method private printRect(Ljava/lang/String;Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "srcRect"    # Landroid/graphics/Rect;

    .prologue
    .line 612
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 613
    const-string v2, ") w = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 612
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    return-void
.end method

.method private printRect(Ljava/lang/String;Landroid/graphics/RectF;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "srcRect"    # Landroid/graphics/RectF;

    .prologue
    .line 617
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->right:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 618
    const-string v2, ") w = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 617
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    return-void
.end method

.method private registerPensoundSolution()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 824
    const-string v1, "SpenMultiView"

    const-string v2, "registerPensoundSolution() - Start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    :try_start_0
    sget-boolean v1, Lcom/samsung/audio/SmpsManager;->isSupport:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 836
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-nez v1, :cond_0

    .line 837
    new-instance v1, Lcom/samsung/audio/SmpsManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/audio/SmpsManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 838
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v1, :cond_0

    .line 839
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    .line 840
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexMarker:I

    .line 841
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexBrush:I

    .line 842
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexEraser:I

    .line 843
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    if-eq v1, v3, :cond_1

    .line 844
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 850
    :cond_0
    :goto_0
    const-string v1, "SpenMultiView"

    const-string v2, "registerPensoundSolution() - End"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    :goto_1
    return-void

    .line 827
    :catch_0
    move-exception v0

    .line 828
    .local v0, "error":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "SpenMultiView"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    goto :goto_1

    .line 831
    .end local v0    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 832
    .local v0, "error":Ljava/lang/NoClassDefFoundError;
    const-string v1, "SpenMultiView"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 833
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    goto :goto_1

    .line 845
    .end local v0    # "error":Ljava/lang/NoClassDefFoundError;
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    if-eq v1, v3, :cond_0

    .line 846
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    goto :goto_0
.end method

.method private relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/Rect;

    .prologue
    .line 643
    iget v0, p2, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 644
    iget v0, p2, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 645
    iget v0, p2, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 646
    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 647
    return-void
.end method

.method private relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/RectF;

    .prologue
    .line 636
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 637
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 638
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 639
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 640
    return-void
.end method

.method private releaseHapticFeedback()V
    .locals 2

    .prologue
    .line 815
    const-string v0, "SpenMultiView"

    const-string v1, "releaseHapticFeedback() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v0, :cond_0

    .line 817
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    invoke-virtual {v0}, Lcom/samsung/hapticfeedback/HapticEffect;->closeDevice()V

    .line 818
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 820
    :cond_0
    const-string v0, "SpenMultiView"

    const-string v1, "releaseHapticFeedback() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 821
    return-void
.end method

.method private removeHoveringIcon(I)Z
    .locals 6
    .param p1, "nHoverIconID"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 650
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    if-nez v4, :cond_1

    .line 693
    :cond_0
    :goto_0
    return v2

    .line 654
    :cond_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverEnable:Z

    if-eqz v4, :cond_0

    .line 657
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverEnable:Z

    .line 658
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 659
    .local v1, "pm":Landroid/content/pm/PackageManager;
    if-eqz v1, :cond_0

    .line 660
    const-string v4, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 668
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-eqz v4, :cond_2

    .line 669
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(I)V

    .line 670
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-virtual {v4, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->removeHoveringSpenCustomIcon(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    :cond_2
    move v2, v3

    .line 693
    goto :goto_0

    .line 672
    :catch_0
    move-exception v0

    .line 673
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "SpenMultiView"

    const-string v4, "removeCustomHoveringIcon() IllegalArgumentException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 676
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 677
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v3, "SpenMultiView"

    const-string v4, "removeCustomHoveringIcon() ClassNotFoundException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 680
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_2
    move-exception v0

    .line 681
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v3, "SpenMultiView"

    const-string v4, "removeCustomHoveringIcon() NoSuchMethodException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 684
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_3
    move-exception v0

    .line 685
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "SpenMultiView"

    const-string v4, "removeCustomHoveringIcon() IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 688
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 689
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "SpenMultiView"

    const-string v4, "removeCustomHoveringIcon() IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private setCustomHoveringIcon(Landroid/graphics/drawable/Drawable;)I
    .locals 7
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v3, -0x1

    .line 697
    const/4 v1, -0x1

    .line 698
    .local v1, "nHoverIconID":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    if-nez v4, :cond_0

    move v3, v1

    .line 741
    :goto_0
    return v3

    .line 701
    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverEnable:Z

    .line 703
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 704
    .local v2, "pm":Landroid/content/pm/PackageManager;
    if-eqz v2, :cond_1

    .line 705
    const-string v4, "com.sec.feature.hovering_ui"

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    move v3, v1

    .line 706
    goto :goto_0

    :cond_1
    move v3, v1

    .line 709
    goto :goto_0

    .line 713
    :cond_2
    if-eqz p1, :cond_3

    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-eqz v4, :cond_3

    .line 714
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v4, v5, p1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(ILandroid/graphics/drawable/Drawable;Landroid/graphics/Point;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    move-result v1

    :cond_3
    move v3, v1

    .line 741
    goto :goto_0

    .line 716
    :catch_0
    move-exception v0

    .line 717
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "SpenMultiView"

    const-string v5, "setCustomHoveringIcon() IllegalArgumentException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 720
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 721
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v4, "SpenMultiView"

    const-string v5, "setCustomHoveringIcon() NotFoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0

    .line 724
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    :catch_2
    move-exception v0

    .line 725
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v4, "SpenMultiView"

    const-string v5, "setCustomHoveringIcon() ClassNotFoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 728
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_3
    move-exception v0

    .line 729
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v4, "SpenMultiView"

    const-string v5, "setCustomHoveringIcon() NoSuchMethodException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 732
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v0

    .line 733
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "SpenMultiView"

    const-string v5, "setCustomHoveringIcon() IllegalAccessException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 736
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_5
    move-exception v0

    .line 737
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v4, "SpenMultiView"

    const-string v5, "setCustomHoveringIcon() InvocationTargetException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 745
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 750
    :goto_0
    return-void

    .line 748
    :cond_0
    const-string v0, "SpenMultiView"

    const-string v1, "setHoverPointerDrawable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private setLayerBitmap(ILandroid/graphics/Bitmap;)Z
    .locals 8
    .param p1, "index"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v0, 0x0

    .line 2667
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-nez v1, :cond_1

    .line 2676
    :cond_0
    :goto_0
    return v0

    .line 2670
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2671
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v5, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2672
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v4, 0x3

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2673
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setPenHoverPoint(Ljava/lang/String;)V
    .locals 4
    .param p1, "penname"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x3

    const/16 v2, 0x32

    .line 3018
    if-nez p1, :cond_0

    .line 3045
    :goto_0
    return-void

    .line 3021
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    if-nez v0, :cond_1

    .line 3022
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    .line 3024
    :cond_1
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 3025
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3026
    :cond_2
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 3027
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3028
    :cond_3
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 3029
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3030
    :cond_4
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 3031
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3032
    :cond_5
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    .line 3033
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3034
    :cond_6
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    .line 3035
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3036
    :cond_7
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_8

    .line 3037
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3038
    :cond_8
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    .line 3039
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3040
    :cond_9
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_a

    .line 3041
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 3043
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0
.end method

.method private unregisterPensoundSolution()V
    .locals 2

    .prologue
    .line 854
    const-string v0, "SpenMultiView"

    const-string v1, "unregisterPensoundSolution() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v0, :cond_0

    .line 856
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    invoke-virtual {v0}, Lcom/samsung/audio/SmpsManager;->onDestroy()V

    .line 857
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 859
    :cond_0
    const-string v0, "SpenMultiView"

    const-string v1, "unregisterPensoundSolution() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    return-void
.end method

.method private updateCanvas(Landroid/graphics/Canvas;Z)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "framebufferUpdate"    # Z

    .prologue
    const/4 v12, 0x0

    const/4 v1, 0x0

    .line 501
    if-nez p1, :cond_0

    .line 545
    :goto_0
    return-void

    .line 504
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    .line 505
    .local v10, "time":J
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_7

    .line 506
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v12, v0}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 508
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    int-to-float v0, v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 509
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 510
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    add-int/2addr v0, v2

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    int-to-float v5, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    int-to-float v6, v0

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 512
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    int-to-float v0, v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 513
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    add-int/2addr v0, v2

    int-to-float v2, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 516
    :cond_2
    if-eqz p2, :cond_3

    .line 517
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v12, v12}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    invoke-virtual {v0, v12, v12, v2}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 520
    :cond_3
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 521
    .local v3, "srcRect":Landroid/graphics/Rect;
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    int-to-float v6, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    int-to-float v7, v0

    move-object v2, p0

    move v4, v1

    move v5, v1

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    .line 522
    new-instance v9, Landroid/graphics/Rect;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    invoke-direct {v9, v12, v12, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 523
    .local v9, "dstRect":Landroid/graphics/Rect;
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    invoke-virtual {v9, v0, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 525
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v9, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 527
    const/4 v8, 0x0

    .local v8, "cnt":I
    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-lt v8, v0, :cond_5

    .line 538
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 539
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 544
    .end local v3    # "srcRect":Landroid/graphics/Rect;
    .end local v8    # "cnt":I
    .end local v9    # "dstRect":Landroid/graphics/Rect;
    :cond_4
    :goto_2
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Performance updateCanvas end "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v10

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 528
    .restart local v3    # "srcRect":Landroid/graphics/Rect;
    .restart local v8    # "cnt":I
    .restart local v9    # "dstRect":Landroid/graphics/Rect;
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v8

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    aget-boolean v0, v0, v8

    if-eqz v0, :cond_6

    .line 529
    const-string v0, "SpenMultiView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Multi start"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " layer width = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 530
    const-string v4, " height = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 529
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v8

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v8

    invoke-virtual {v2, v12, v12}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    invoke-virtual {v0, v12, v12, v2}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 532
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v8

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v9, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 533
    const-string v0, "SpenMultiView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Multi end"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " layer width = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 534
    const-string v4, " height = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 533
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    :cond_6
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 542
    .end local v3    # "srcRect":Landroid/graphics/Rect;
    .end local v8    # "cnt":I
    .end local v9    # "dstRect":Landroid/graphics/Rect;
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_2
.end method

.method private updateCanvas2(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 489
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    .line 490
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 491
    .local v1, "srcRect":Landroid/graphics/Rect;
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    int-to-float v4, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    int-to-float v5, v0

    move-object v0, p0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    .line 492
    new-instance v6, Landroid/graphics/Rect;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    invoke-direct {v6, v7, v7, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 493
    .local v6, "dstRect":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 497
    .end local v1    # "srcRect":Landroid/graphics/Rect;
    .end local v6    # "dstRect":Landroid/graphics/Rect;
    :goto_0
    return-void

    .line 495
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_0
.end method


# virtual methods
.method public addUser(I)Z
    .locals 4
    .param p1, "userId"    # I

    .prologue
    .line 1908
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1909
    const/4 v0, 0x0

    .line 1911
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_addUser(JI)Z

    move-result v0

    goto :goto_0
.end method

.method public cancelStroke(I)V
    .locals 7
    .param p1, "userId"    # I

    .prologue
    .line 2741
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2746
    :goto_0
    return-void

    .line 2745
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public captureCurrentView(Z)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "includeBlank"    # Z

    .prologue
    const/4 v3, 0x0

    .line 2538
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    move-object v0, v3

    .line 2564
    :goto_0
    return-object v0

    .line 2542
    :cond_0
    const/4 v0, 0x0

    .line 2544
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    const-string v4, "SpenMultiView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "RtoCvs["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] Frame["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 2545
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2544
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2546
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    if-nez v4, :cond_2

    :cond_1
    move-object v0, v3

    .line 2547
    goto :goto_0

    .line 2549
    :cond_2
    if-nez p1, :cond_4

    .line 2550
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2558
    :goto_1
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2559
    .local v1, "canvas":Landroid/graphics/Canvas;
    if-nez p1, :cond_3

    .line 2560
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    neg-int v3, v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2562
    :cond_3
    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas(Landroid/graphics/Canvas;Z)V

    .line 2563
    const/4 v1, 0x0

    .line 2564
    goto :goto_0

    .line 2552
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    :cond_4
    :try_start_1
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 2554
    :catch_0
    move-exception v2

    .line 2555
    .local v2, "e":Ljava/lang/Throwable;
    const-string v3, "SpenMultiView"

    const-string v4, "Failed to create bitmap"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2556
    const/4 v3, 0x2

    const-string v4, " : fail createBitmap."

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public capturePage(F)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 2595
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 2596
    const/4 v0, 0x0

    .line 2599
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    .line 2600
    const/4 v3, 0x1

    .line 2599
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public close()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 370
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    cmp-long v1, v4, v8

    if-eqz v1, :cond_0

    .line 371
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_finalize(J)V

    .line 372
    iput-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    .line 375
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 376
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 377
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    .line 379
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 380
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    array-length v4, v3

    move v1, v2

    :goto_0
    if-lt v1, v4, :cond_6

    .line 388
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v1, :cond_3

    .line 389
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->close()V

    .line 390
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 392
    :cond_3
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    .line 393
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    .line 394
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    .line 395
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    .line 397
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    .line 398
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    .line 399
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    .line 400
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 401
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    .line 402
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    .line 403
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 404
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 405
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    .line 407
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 408
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v1, :cond_4

    .line 409
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->close()V

    .line 410
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 412
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v1, :cond_5

    .line 413
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->close()V

    .line 414
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 417
    :cond_5
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 418
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .line 419
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 420
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .line 421
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 422
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 423
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 424
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 425
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 426
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 427
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    .line 428
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 430
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->releaseHapticFeedback()V

    .line 431
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->unregisterPensoundSolution()V

    .line 432
    return-void

    .line 380
    :cond_6
    aget-object v0, v3, v1

    .line 381
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v5

    if-nez v5, :cond_7

    .line 382
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 380
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public closeControl()V
    .locals 0

    .prologue
    .line 3528
    return-void
.end method

.method public getBlankColor()I
    .locals 4

    .prologue
    .line 2134
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2135
    const/4 v0, 0x0

    .line 2137
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasHeight()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1821
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1824
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasWidth()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1807
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1810
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 6

    .prologue
    .line 1469
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1470
    const/4 v0, 0x0

    .line 1478
    :cond_0
    :goto_0
    return-object v0

    .line 1472
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    .line 1473
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    if-eqz v0, :cond_0

    .line 1474
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getEraserSize(JI)F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 1475
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    goto :goto_0
.end method

.method public getEraserSettingInfo(I)Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 6
    .param p1, "userId"    # I

    .prologue
    .line 1499
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1500
    const/4 v0, 0x0

    .line 1507
    :cond_0
    :goto_0
    return-object v0

    .line 1502
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    .line 1503
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    if-eqz v0, :cond_0

    .line 1504
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getEraserSize(JI)F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    goto :goto_0
.end method

.method public getLayerCount()I
    .locals 1

    .prologue
    .line 2663
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    return v0
.end method

.method public getLocalUserId()I
    .locals 4

    .prologue
    .line 1885
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1886
    const/4 v0, -0x1

    .line 1889
    :goto_0
    return v0

    .line 1888
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getLocalUserId(J)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    .line 1889
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    goto :goto_0
.end method

.method public getMaxZoomRatio()F
    .locals 4

    .prologue
    .line 2012
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2013
    const/4 v0, 0x0

    .line 2015
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getMaxZoomRatio(J)F

    move-result v0

    goto :goto_0
.end method

.method public getMinZoomRatio()F
    .locals 4

    .prologue
    .line 2051
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2052
    const/4 v0, 0x0

    .line 2054
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getMinZoomRatio(J)F

    move-result v0

    goto :goto_0
.end method

.method public getPan()Landroid/graphics/PointF;
    .locals 6

    .prologue
    .line 2090
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2091
    const/4 v0, 0x0

    .line 2095
    :goto_0
    return-object v0

    .line 2093
    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 2094
    .local v0, "point":Landroid/graphics/PointF;
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPan(JLandroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 6

    .prologue
    .line 1320
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1321
    const/4 v0, 0x0

    .line 1333
    :cond_0
    :goto_0
    return-object v0

    .line 1323
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    .line 1324
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    if-eqz v0, :cond_0

    .line 1325
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPenStyle(JI)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 1326
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPenSize(JI)F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1327
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPenColor(JI)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 1328
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_isPenCurve(JI)Z

    move-result v1

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    .line 1329
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getAdvancedSetting(JI)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 1330
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    goto :goto_0
.end method

.method public getPenSettingInfo(I)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 6
    .param p1, "userId"    # I

    .prologue
    .line 1354
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1355
    const/4 v0, 0x0

    .line 1366
    :cond_0
    :goto_0
    return-object v0

    .line 1357
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    .line 1358
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    if-eqz v0, :cond_0

    .line 1359
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPenStyle(JI)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 1360
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPenSize(JI)F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1361
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPenColor(JI)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 1362
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_isPenCurve(JI)Z

    move-result v1

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    .line 1363
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getAdvancedSetting(JI)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    .prologue
    .line 1550
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    .locals 1

    .prologue
    .line 1593
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    .prologue
    .line 1202
    const/4 v0, 0x0

    return-object v0
.end method

.method public getToolTypeAction(I)I
    .locals 6
    .param p1, "toolType"    # I

    .prologue
    .line 1750
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1751
    const/4 v0, 0x0

    .line 1757
    :cond_0
    :goto_0
    return v0

    .line 1753
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getToolTypeAction(JII)I

    move-result v0

    .line 1754
    .local v0, "action":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1755
    const/4 v1, 0x1

    const-string v2, "not set LocalUserId"

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public getToolTypeAction(II)I
    .locals 4
    .param p1, "userId"    # I
    .param p2, "toolType"    # I

    .prologue
    .line 1788
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1789
    const/4 v0, 0x0

    .line 1791
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getToolTypeAction(JII)I

    move-result v0

    goto :goto_0
.end method

.method public getZoomRatio()F
    .locals 4

    .prologue
    .line 1973
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1974
    const/4 v0, 0x0

    .line 1976
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getZoomRatio(J)F

    move-result v0

    goto :goto_0
.end method

.method public isDoubleTapEnabled()Z
    .locals 1

    .prologue
    .line 2842
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsDoubleTap:Z

    return v0
.end method

.method public isToolTipEnabled()Z
    .locals 1

    .prologue
    .line 2816
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    return v0
.end method

.method public isZoomable()Z
    .locals 4

    .prologue
    .line 2784
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2785
    const/4 v0, 0x0

    .line 2788
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_isZoomable(J)Z

    move-result v0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 782
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 795
    :goto_0
    return-void

    .line 786
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 787
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->drawAnimation(Landroid/graphics/Canvas;)V

    .line 794
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 789
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas(Landroid/graphics/Canvas;Z)V

    .line 790
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_1

    .line 791
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1119
    if-eqz p1, :cond_0

    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v12, 0x0

    cmp-long v1, v10, v12

    if-nez v1, :cond_1

    .line 1120
    :cond_0
    const/4 v1, 0x1

    .line 1159
    :goto_0
    return v1

    .line 1123
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v10, 0x9

    if-ne v1, v10, :cond_4

    .line 1124
    const-string v1, "SpenMultiView"

    const-string v10, "[HOVER_CONTROL] Hover Enter"

    invoke-static {v1, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1125
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    .line 1126
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setCustomHoveringIcon(Landroid/graphics/drawable/Drawable;)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverIconID:I

    .line 1133
    :cond_2
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1134
    .local v0, "action":I
    const/16 v1, 0x9

    if-ne v0, v1, :cond_3

    .line 1135
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v12

    sub-long v2, v10, v12

    .line 1136
    .local v2, "diffTime":J
    const-wide/16 v10, 0x64

    iget-wide v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    add-long/2addr v10, v12

    cmp-long v1, v2, v10

    if-lez v1, :cond_5

    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    .line 1137
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    if-eqz v1, :cond_3

    .line 1138
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    .line 1139
    .local v6, "eventTime":J
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    .line 1140
    .local v4, "downTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 1141
    .local v8, "systemTime":J
    const-string v1, "SpenMultiView"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "skiptouch hover action = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " eventTime = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " downTime = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 1142
    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " systemTime = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " diffTime = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1141
    invoke-static {v1, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1146
    .end local v2    # "diffTime":J
    .end local v4    # "downTime":J
    .end local v6    # "eventTime":J
    .end local v8    # "systemTime":J
    :cond_3
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    if-eqz v1, :cond_6

    .line 1147
    const-string v1, "SpenMultiView"

    const-string v10, "skiptouch hover"

    invoke-static {v1, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1148
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 1128
    .end local v0    # "action":I
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v10, 0xa

    if-ne v1, v10, :cond_2

    .line 1129
    const-string v1, "SpenMultiView"

    const-string v10, "[HOVER_CONTROL] Hover Exit"

    invoke-static {v1, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1130
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverIconID:I

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->removeHoveringIcon(I)Z

    goto/16 :goto_1

    .line 1136
    .restart local v0    # "action":I
    .restart local v2    # "diffTime":J
    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    .line 1155
    .end local v2    # "diffTime":J
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    invoke-interface {v1, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;->onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1156
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 1159
    :cond_7
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 754
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 756
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mParentRect:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 757
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mParentRect:Landroid/graphics/Rect;

    .line 759
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 760
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_1

    .line 761
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setParentRect(Landroid/graphics/Rect;)V

    .line 763
    :cond_1
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 767
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 778
    :goto_0
    return-void

    .line 771
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    .line 772
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    .line 774
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setScreenResolution(II)V

    .line 775
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setScreenSize(JII)V

    .line 777
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    goto :goto_0
.end method

.method public onTouchEvent(ILandroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "userId"    # I
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    .line 1091
    if-eqz p2, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1097
    :cond_0
    :goto_0
    return v7

    .line 1095
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_onTouch(JILandroid/view/MotionEvent;I)Z

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 28
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 926
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    .line 927
    :cond_0
    const/4 v5, 0x1

    .line 1075
    :goto_0
    return v5

    .line 930
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    and-int/lit16 v4, v5, 0xff

    .line 931
    .local v4, "action":I
    if-nez v4, :cond_2

    .line 932
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsCancelFling:Z

    .line 934
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    .line 935
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsCancelFling:Z

    .line 937
    :cond_3
    if-nez v4, :cond_4

    .line 938
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v8

    sub-long v12, v6, v8

    .line 939
    .local v12, "diffTime":J
    const-wide/16 v6, 0x64

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    add-long/2addr v6, v8

    cmp-long v5, v12, v6

    if-lez v5, :cond_c

    const/4 v5, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    .line 940
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    if-eqz v5, :cond_4

    .line 941
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v16

    .line 942
    .local v16, "eventTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v14

    .line 943
    .local v14, "downTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v22

    .line 944
    .local v22, "systemTime":J
    const-string v5, "SpenMultiView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "skiptouch action = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " eventTime = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v16

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " downTime = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 945
    const-string v7, " systemTime = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v22

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " diffTime = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 944
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 949
    .end local v12    # "diffTime":J
    .end local v14    # "downTime":J
    .end local v16    # "eventTime":J
    .end local v22    # "systemTime":J
    :cond_4
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    neg-int v5, v5

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    neg-int v6, v6

    int-to-float v6, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 951
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 952
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 954
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    if-nez v5, :cond_1b

    .line 955
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v26

    .line 956
    .local v26, "time":J
    const-string v5, "SpenMultiView"

    const-string v6, "Performance mPreTouchListener start"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 958
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v5, v8}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getToolTypeAction(JII)I

    move-result v25

    .line 960
    .local v25, "toolTypeAction":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v11

    .line 961
    .local v11, "newX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v18

    .line 963
    .local v18, "newY":F
    const/4 v5, 0x2

    move/from16 v0, v25

    if-eq v0, v5, :cond_5

    const/4 v5, 0x3

    move/from16 v0, v25

    if-eq v0, v5, :cond_5

    .line 964
    const/4 v5, 0x4

    move/from16 v0, v25

    if-ne v0, v5, :cond_b

    .line 965
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    if-eqz v5, :cond_a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v5, :cond_a

    .line 966
    const/4 v5, 0x3

    move/from16 v0, v25

    if-eq v0, v5, :cond_6

    const/4 v5, 0x4

    move/from16 v0, v25

    if-ne v0, v5, :cond_7

    .line 967
    :cond_6
    if-nez v4, :cond_d

    .line 968
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexEraser:I

    invoke-virtual {v5, v6}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 969
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    const-string v7, "Eraser"

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    float-to-double v6, v6

    invoke-virtual {v5, v6, v7}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    .line 977
    :cond_7
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v21

    .line 978
    .local v21, "tempX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v24

    .line 979
    .local v24, "tempY":F
    move/from16 v19, v21

    .line 980
    .local v19, "originX":F
    move/from16 v20, v24

    .line 981
    .local v20, "originY":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    const/4 v6, 0x0

    cmpg-float v5, v5, v6

    if-gez v5, :cond_8

    .line 982
    const/16 v24, 0x0

    .line 984
    :cond_8
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    const/4 v6, 0x0

    cmpg-float v5, v5, v6

    if-gez v5, :cond_9

    .line 985
    const/16 v21, 0x0

    .line 987
    :cond_9
    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 988
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/samsung/audio/SmpsManager;->generateSound(Landroid/view/MotionEvent;)V

    .line 989
    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 992
    .end local v19    # "originX":F
    .end local v20    # "originY":F
    .end local v21    # "tempX":F
    .end local v24    # "tempY":F
    :cond_a
    if-nez v4, :cond_e

    .line 993
    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldX:F

    .line 994
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldY:F

    .line 1010
    :cond_b
    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    if-eqz v5, :cond_11

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-interface {v5, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 1011
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 939
    .end local v11    # "newX":F
    .end local v18    # "newY":F
    .end local v25    # "toolTypeAction":I
    .end local v26    # "time":J
    .restart local v12    # "diffTime":J
    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 970
    .end local v12    # "diffTime":J
    .restart local v11    # "newX":F
    .restart local v18    # "newY":F
    .restart local v25    # "toolTypeAction":I
    .restart local v26    # "time":J
    :cond_d
    const/4 v5, 0x1

    if-ne v4, v5, :cond_7

    .line 971
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    invoke-virtual {v5, v6}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 972
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 973
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    float-to-double v6, v6

    .line 972
    invoke-virtual {v5, v6, v7}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    goto/16 :goto_2

    .line 995
    :cond_e
    const/4 v5, 0x2

    if-ne v4, v5, :cond_10

    .line 997
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v5, :cond_f

    .line 998
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldX:F

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldY:F

    move/from16 v0, v18

    invoke-virtual {v5, v6, v7, v11, v0}, Lcom/samsung/hapticfeedback/HapticEffect;->playEffectByDistance(FFFF)V

    .line 1000
    :cond_f
    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldX:F

    .line 1001
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldY:F

    goto :goto_3

    .line 1002
    :cond_10
    const/4 v5, 0x1

    if-ne v4, v5, :cond_b

    .line 1004
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v5, :cond_b

    .line 1005
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    invoke-virtual {v5}, Lcom/samsung/hapticfeedback/HapticEffect;->stopAllEffect()V

    goto :goto_3

    .line 1013
    :cond_11
    const-string v5, "SpenMultiView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Performance mPreTouchListener.onTouch end "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v26

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    if-nez v4, :cond_16

    .line 1016
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v5, :cond_12

    .line 1017
    const/4 v5, 0x3

    move/from16 v0, v25

    if-eq v0, v5, :cond_12

    const/4 v5, 0x4

    move/from16 v0, v25

    if-eq v0, v5, :cond_12

    .line 1018
    const/4 v5, 0x5

    move/from16 v0, v25

    if-eq v0, v5, :cond_12

    .line 1019
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 1022
    :cond_12
    const/4 v5, 0x3

    move/from16 v0, v25

    if-eq v0, v5, :cond_13

    const/4 v5, 0x4

    move/from16 v0, v25

    if-ne v0, v5, :cond_15

    .line 1023
    :cond_13
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isEraserCursor:Z

    .line 1024
    const/4 v5, 0x3

    move/from16 v0, v25

    if-ne v0, v5, :cond_14

    .line 1025
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getEraserSize(JI)F

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    .line 1033
    :cond_14
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 1034
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/PointF;->y:F

    .line 1035
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v6, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1062
    :cond_15
    :goto_4
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v10

    move-object/from16 v5, p0

    move-object/from16 v9, p1

    invoke-direct/range {v5 .. v10}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_onTouch(JILandroid/view/MotionEvent;I)Z

    .line 1063
    const-string v5, "SpenMultiView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Performance native_onTouch end "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v26

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1065
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    if-eqz v5, :cond_1a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-interface {v5, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 1066
    const-string v5, "SpenMultiView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Performance mTouchListener.onTouch end "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v26

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1067
    const-string v7, " ms action = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1066
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1068
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 1037
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isEraserCursor:Z

    if-eqz v5, :cond_15

    .line 1038
    const/4 v5, 0x2

    if-ne v4, v5, :cond_18

    .line 1039
    const/4 v5, 0x3

    move/from16 v0, v25

    if-ne v0, v5, :cond_17

    .line 1040
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getEraserSize(JI)F

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    .line 1048
    :cond_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 1049
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/PointF;->y:F

    .line 1050
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v6, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto/16 :goto_4

    .line 1052
    :cond_18
    const/4 v5, 0x1

    if-eq v4, v5, :cond_19

    const/4 v5, 0x3

    if-ne v4, v5, :cond_15

    .line 1053
    :cond_19
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isEraserCursor:Z

    .line 1054
    const/high16 v5, -0x3d380000    # -100.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    .line 1056
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    const/high16 v6, -0x3d380000    # -100.0f

    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 1057
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    const/high16 v6, -0x3d380000    # -100.0f

    iput v6, v5, Landroid/graphics/PointF;->y:F

    goto/16 :goto_4

    .line 1070
    :cond_1a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v6, v6, v26

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    .line 1071
    const-string v5, "SpenMultiView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Performance mTouchListener.onTouch end "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v26

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1072
    const-string v7, " ms action = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1071
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1075
    .end local v11    # "newX":F
    .end local v18    # "newY":F
    .end local v25    # "toolTypeAction":I
    .end local v26    # "time":J
    :cond_1b
    const/4 v5, 0x1

    goto/16 :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 896
    const-string v0, "SpenMultiView"

    const-string v1, "onWindowFocusChanged() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    if-eqz p1, :cond_0

    .line 898
    const-string v0, "SpenMultiView"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 899
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->registerPensoundSolution()V

    .line 904
    :goto_0
    const-string v0, "SpenMultiView"

    const-string v1, "onWindowFocusChanged() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    return-void

    .line 901
    :cond_0
    const-string v0, "SpenMultiView"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 902
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->unregisterPensoundSolution()V

    goto :goto_0
.end method

.method public removeUser(I)Z
    .locals 4
    .param p1, "userId"    # I

    .prologue
    .line 1924
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1925
    const/4 v0, 0x0

    .line 1927
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_removeUser(JI)Z

    move-result v0

    goto :goto_0
.end method

.method public requestAllocateLayer(I)Z
    .locals 10
    .param p1, "userId"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2689
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    move v1, v8

    .line 2704
    :goto_0
    return v1

    .line 2693
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 2695
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz v0, :cond_3

    .line 2696
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;

    .line 2697
    .local v7, "tmp":Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;
    iget v1, v7, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;->a:I

    if-ltz v1, :cond_1

    .line 2698
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    iget v2, v7, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;->a:I

    aput-boolean v9, v1, v2

    .line 2699
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    iget v2, v7, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;->a:I

    aput p1, v1, v2

    .line 2702
    :cond_1
    iget v1, v7, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;->a:I

    if-ltz v1, :cond_2

    move v1, v9

    goto :goto_0

    :cond_2
    move v1, v8

    goto :goto_0

    .end local v7    # "tmp":Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;
    :cond_3
    move v1, v8

    .line 2704
    goto :goto_0
.end method

.method public requestReleaseLayer(I)V
    .locals 8
    .param p1, "userId"    # I

    .prologue
    const/4 v7, 0x0

    .line 2716
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2729
    :cond_0
    :goto_0
    return-void

    .line 2720
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v4, 0x5

    const/4 v5, 0x0

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 2722
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-ge v0, v1, :cond_0

    .line 2723
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_2

    .line 2724
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    aput-boolean v7, v1, v0

    .line 2725
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    aput v7, v1, v0

    goto :goto_0

    .line 2722
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "listener"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .prologue
    .line 1834
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1849
    :cond_0
    :goto_0
    return-void

    .line 1837
    :cond_1
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    if-eqz v1, :cond_0

    .line 1838
    const-string v1, "SpenMultiView"

    const-string v2, "setBackgroundColorListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1839
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 1841
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v1, :cond_0

    .line 1842
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v1

    shr-int/lit8 v1, v1, 0x18

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_2

    const/4 v0, 0x1

    .line 1843
    .local v0, "enable":Z
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_0

    .line 1844
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 1845
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    goto :goto_0

    .line 1842
    .end local v0    # "enable":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setBlankColor(I)V
    .locals 4
    .param p1, "color"    # I

    .prologue
    .line 2112
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2120
    :cond_0
    :goto_0
    return-void

    .line 2115
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 2116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2117
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setPaint(Landroid/graphics/Paint;)V

    .line 2118
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .prologue
    .line 2429
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2433
    :goto_0
    return-void

    .line 2432
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    goto :goto_0
.end method

.method public setDoubleTapEnabled(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 2829
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setDoubleTapEnabled="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2830
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsDoubleTap:Z

    .line 2831
    return-void
.end method

.method public setEraserChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .prologue
    .line 2509
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2513
    :goto_0
    return-void

    .line 2512
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    goto :goto_0
.end method

.method public setEraserSettingInfo(ILcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 7
    .param p1, "userId"    # I
    .param p2, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 1438
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1452
    :cond_0
    :goto_0
    return-void

    .line 1442
    :cond_1
    if-eqz p2, :cond_0

    .line 1443
    iget v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    cmpg-float v1, v1, v6

    if-gez v1, :cond_2

    .line 1444
    iput v6, p2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 1446
    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-ge v1, v2, :cond_4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 1447
    .local v0, "maxSize":I
    :goto_1
    iget v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    int-to-float v2, v0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 1448
    int-to-float v1, v0

    iput v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 1450
    :cond_3
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-direct {p0, v2, v3, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setEraserSize(JIF)Z

    goto :goto_0

    .line 1446
    .end local v0    # "maxSize":I
    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    goto :goto_1
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 8
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x5

    .line 1389
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1415
    :cond_0
    :goto_0
    return-void

    .line 1393
    :cond_1
    if-eqz p1, :cond_4

    .line 1394
    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    cmpg-float v1, v1, v7

    if-gez v1, :cond_2

    .line 1395
    iput v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 1397
    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-ge v1, v2, :cond_5

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 1398
    .local v0, "maxSize":I
    :goto_1
    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    int-to-float v2, v0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 1399
    int-to-float v1, v0

    iput v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 1401
    :cond_3
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-direct {p0, v2, v3, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setEraserSize(JIF)Z

    .line 1403
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 1404
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v6, :cond_4

    .line 1405
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v6, :cond_4

    .line 1406
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v6, :cond_4

    .line 1407
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v6, :cond_4

    .line 1408
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(F)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1412
    .end local v0    # "maxSize":I
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    if-eqz v1, :cond_0

    .line 1413
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    invoke-interface {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto :goto_0

    .line 1397
    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    goto :goto_1
.end method

.method public setFlickListener(Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .prologue
    .line 2389
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2393
    :goto_0
    return-void

    .line 2392
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    goto :goto_0
.end method

.method public setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .prologue
    .line 2409
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2413
    :goto_0
    return-void

    .line 2412
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    goto :goto_0
.end method

.method public setLayerCount(I)V
    .locals 11
    .param p1, "count"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 2612
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v8, 0x0

    cmp-long v1, v2, v8

    if-nez v1, :cond_1

    .line 2654
    :cond_0
    return-void

    .line 2616
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v4, 0x1

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 2618
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-lez v1, :cond_2

    .line 2619
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-lt v0, v1, :cond_5

    .line 2626
    .end local v0    # "cnt":I
    :cond_2
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    .line 2627
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    .line 2628
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    .line 2630
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    .line 2631
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-gez v1, :cond_3

    .line 2632
    iput v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    .line 2634
    :cond_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-lez v1, :cond_0

    .line 2635
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    new-array v1, v1, [Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    .line 2636
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    new-array v1, v1, [Z

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    .line 2637
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    .line 2638
    const/4 v0, 0x0

    .restart local v0    # "cnt":I
    :goto_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-ge v0, v1, :cond_0

    .line 2639
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aput-object v5, v1, v0

    .line 2640
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    aput-boolean v10, v1, v0

    .line 2641
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    aput v10, v1, v0

    .line 2642
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-lez v1, :cond_4

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    if-lez v1, :cond_4

    .line 2644
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 2645
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 2644
    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2650
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setLayerBitmap(ILandroid/graphics/Bitmap;)Z

    .line 2638
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2620
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2621
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2622
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aput-object v5, v1, v0

    .line 2619
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2646
    :catch_0
    move-exception v7

    .line 2647
    .local v7, "e":Ljava/lang/Exception;
    const/4 v1, 0x2

    .line 2648
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create bitmap of frame buffer mFloatingLayerBitmap["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2647
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_2
.end method

.method public setLocalUserId(I)V
    .locals 4
    .param p1, "userId"    # I

    .prologue
    .line 1865
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1871
    :cond_0
    :goto_0
    return-void

    .line 1868
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setLocalUserId(JI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1869
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    goto :goto_0
.end method

.method public setMaxZoomRatio(F)Z
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 1994
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1995
    const/4 v0, 0x0

    .line 1997
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setMaxZoomRatio(JF)Z

    move-result v0

    goto :goto_0
.end method

.method public setMinZoomRatio(F)Z
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 2033
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2034
    const/4 v0, 0x0

    .line 2036
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setMinZoomRatio(JF)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;IIF)Z
    .locals 8
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "direction"    # I
    .param p3, "type"    # I
    .param p4, "centerY"    # F

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2230
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2263
    :cond_0
    :goto_0
    return v6

    .line 2233
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2234
    const-string v1, "SpenMultiView"

    const-string v2, "setPageDoc is closed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2237
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2241
    const-string v1, "SpenMultiView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setPageDoc, direction="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2243
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1, p3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setType(I)V

    .line 2244
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setCanvasInformation(IIII)V

    .line 2245
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->saveScreenshot()Z

    .line 2247
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 2249
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 2251
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v1, :cond_3

    .line 2252
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v1

    shr-int/lit8 v1, v1, 0x18

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_4

    move v0, v7

    .line 2253
    .local v0, "enable":Z
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_3

    .line 2254
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 2255
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    .line 2258
    .end local v0    # "enable":Z
    :cond_3
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v4, 0x0

    move-object v1, p0

    move v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPan(JFFZ)V

    .line 2259
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v2, v3, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    .line 2261
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->startAnimation(I)Z

    move v6, v7

    .line 2263
    goto/16 :goto_0

    :cond_4
    move v0, v6

    .line 2252
    goto :goto_1
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 6
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "isUpdate"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2166
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 2189
    :goto_0
    return v0

    .line 2169
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2170
    const-string v1, "SpenMultiView"

    const-string v2, "setPageDoc is closed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2174
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 2176
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 2178
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v2, :cond_3

    .line 2179
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v2

    shr-int/lit8 v2, v2, 0x18

    and-int/lit16 v2, v2, 0xff

    const/16 v3, 0xff

    if-ne v2, v3, :cond_2

    move v0, v1

    .line 2180
    .local v0, "enable":Z
    :cond_2
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    if-eq v0, v2, :cond_3

    .line 2181
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 2182
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    .line 2185
    .end local v0    # "enable":Z
    :cond_3
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2186
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    :cond_4
    move v0, v1

    .line 2189
    goto :goto_0
.end method

.method public setPageEffectListener(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .prologue
    .line 2469
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2473
    :goto_0
    return-void

    .line 2472
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    goto :goto_0
.end method

.method public setPan(Landroid/graphics/PointF;)V
    .locals 7
    .param p1, "position"    # Landroid/graphics/PointF;

    .prologue
    .line 2071
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2076
    :goto_0
    return-void

    .line 2075
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v4, p1, Landroid/graphics/PointF;->x:F

    iget v5, p1, Landroid/graphics/PointF;->y:F

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPan(JFFZ)V

    goto :goto_0
.end method

.method public setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .prologue
    .line 2489
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2493
    :goto_0
    return-void

    .line 2492
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    goto :goto_0
.end method

.method public setPenSettingInfo(ILcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 4
    .param p1, "userId"    # I
    .param p2, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    .line 1289
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1303
    :cond_0
    :goto_0
    return-void

    .line 1293
    :cond_1
    if-eqz p2, :cond_0

    .line 1294
    iget v0, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 1295
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1297
    :cond_2
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget-object v2, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPenStyle(JILjava/lang/String;)Z

    .line 1298
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPenColor(JII)V

    .line 1299
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPenSize(JIF)V

    .line 1300
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget-boolean v2, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_enablePenCurve(JIZ)V

    .line 1301
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget-object v2, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setAdvancedSetting(JILjava/lang/String;)V

    goto :goto_0
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 6
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x5

    .line 1222
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1267
    :cond_0
    :goto_0
    return-void

    .line 1226
    :cond_1
    if-eqz p1, :cond_0

    .line 1227
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 1228
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1230
    :cond_2
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget-object v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPenStyle(JILjava/lang/String;)Z

    .line 1231
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPenColor(JII)V

    .line 1232
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPenSize(JIF)V

    .line 1233
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget-boolean v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_enablePenCurve(JIZ)V

    .line 1234
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget-object v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setAdvancedSetting(JILjava/lang/String;)V

    .line 1236
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 1237
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v0, :cond_5

    .line 1238
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1239
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1240
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1241
    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    .line 1248
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    invoke-virtual {v0, v1}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 1249
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    .line 1252
    :cond_5
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_6

    .line 1253
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_6

    .line 1254
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_6

    .line 1255
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_6

    .line 1256
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 1257
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1256
    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableImage(Ljava/lang/String;IF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1258
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-ne v0, v5, :cond_6

    .line 1259
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setPenHoverPoint(Ljava/lang/String;)V

    .line 1263
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    if-eqz v0, :cond_0

    .line 1264
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto/16 :goto_0

    .line 1242
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1243
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1244
    :cond_8
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexBrush:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    goto/16 :goto_1

    .line 1245
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1246
    :cond_a
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexMarker:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    goto/16 :goto_1
.end method

.method public setPreTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .prologue
    .line 2349
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2353
    :goto_0
    return-void

    .line 2352
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    goto :goto_0
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    const/4 v1, 0x5

    .line 1527
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1528
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1529
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1530
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1531
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1533
    :cond_0
    return-void
.end method

.method public setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .prologue
    const/4 v1, 0x5

    .line 1570
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1571
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1572
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1573
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1574
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1576
    :cond_0
    return-void
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    const/4 v1, 0x5

    .line 1179
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1180
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1181
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1182
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1183
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1185
    :cond_0
    return-void
.end method

.method public setToolTipEnabled(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 2800
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setToolTipEnabled="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2801
    if-nez p1, :cond_0

    .line 2802
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2804
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    .line 2805
    return-void
.end method

.method public setToolTypeAction(II)V
    .locals 7
    .param p1, "toolType"    # I
    .param p2, "action"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 1632
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1676
    :cond_0
    :goto_0
    return-void

    .line 1636
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setToolTypeAction(II)V

    .line 1638
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v0, :cond_4

    .line 1639
    if-ne p2, v4, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_5

    .line 1640
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    if-nez v0, :cond_2

    .line 1641
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    .line 1646
    :cond_2
    :goto_1
    if-ne p2, v4, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_6

    .line 1647
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v0, :cond_3

    .line 1648
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 1650
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 1651
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1650
    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableImage(Ljava/lang/String;IF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1652
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setPenHoverPoint(Ljava/lang/String;)V

    .line 1673
    :cond_4
    :goto_2
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    move-object v1, p0

    move v5, p1

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setToolTypeAction(JIII)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1674
    const/4 v0, 0x1

    const-string v1, "not set LocalUserId"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 1644
    :cond_5
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    goto :goto_1

    .line 1653
    :cond_6
    const/4 v0, 0x3

    if-ne p2, v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_8

    .line 1654
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-nez v0, :cond_7

    .line 1655
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 1657
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(F)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 1658
    :cond_8
    const/4 v0, 0x4

    if-ne p2, v0, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-nez v0, :cond_4

    .line 1667
    :cond_9
    const/4 v0, 0x5

    if-ne p2, v0, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_a

    .line 1668
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableHoverImage()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 1670
    :cond_a
    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method public setToolTypeAction(III)V
    .locals 7
    .param p1, "userId"    # I
    .param p2, "toolType"    # I
    .param p3, "action"    # I

    .prologue
    .line 1711
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1716
    :goto_0
    return-void

    .line 1715
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setToolTypeAction(JIII)Z

    goto :goto_0
.end method

.method public setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .prologue
    .line 2369
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2373
    :goto_0
    return-void

    .line 2372
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    goto :goto_0
.end method

.method public setZoom(FFF)V
    .locals 7
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 1955
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1959
    :goto_0
    return-void

    .line 1958
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setZoom(JFFF)V

    goto :goto_0
.end method

.method public setZoomListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .prologue
    .line 2449
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2453
    :goto_0
    return-void

    .line 2452
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    goto :goto_0
.end method

.method public setZoomable(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 2763
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2768
    :goto_0
    return-void

    .line 2767
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_enableZoom(JZ)V

    goto :goto_0
.end method

.method public update()V
    .locals 6

    .prologue
    .line 2278
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2293
    :cond_0
    :goto_0
    return-void

    .line 2281
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_update(J)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2282
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    .line 2283
    const-string v2, "We can\'t update this state, we can update just for append object"

    .line 2282
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 2286
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v1, :cond_0

    .line 2287
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v1

    shr-int/lit8 v1, v1, 0x18

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_3

    const/4 v0, 0x1

    .line 2288
    .local v0, "enable":Z
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_0

    .line 2289
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 2290
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    goto :goto_0

    .line 2287
    .end local v0    # "enable":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)V
    .locals 4
    .param p1, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .param p2, "userId"    # I

    .prologue
    .line 2327
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2333
    :cond_0
    :goto_0
    return-void

    .line 2330
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_updateHistory(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2331
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    const-string v1, "We can\'t redo"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)V
    .locals 4
    .param p1, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .param p2, "userId"    # I

    .prologue
    .line 2307
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2313
    :cond_0
    :goto_0
    return-void

    .line 2310
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_updateHistory(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2311
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    const-string v1, "We can\'t undo"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method
