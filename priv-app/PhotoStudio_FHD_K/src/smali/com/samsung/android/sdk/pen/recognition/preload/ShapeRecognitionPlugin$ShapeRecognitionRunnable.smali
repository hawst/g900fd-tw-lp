.class Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;
.super Ljava/lang/Object;
.source "ShapeRecognitionPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ShapeRecognitionRunnable"
.end annotation


# instance fields
.field private mInput:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p2, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->mInput:Ljava/util/List;

    .line 121
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->mInput:Ljava/util/List;

    .line 122
    return-void
.end method


# virtual methods
.method public run()V
    .locals 20

    .prologue
    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->mInput:Ljava/util/List;

    move-object/from16 v16, v0

    check-cast v16, Ljava/util/ArrayList;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    # invokes: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->addStroke(Ljava/util/ArrayList;)V
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->access$1(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;Ljava/util/ArrayList;)V

    .line 129
    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->access$2()Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->process()Ljava/util/ArrayList;

    move-result-object v9

    .line 131
    .local v9, "pointList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    const-string v16, "ShapeRecognitionPlugin"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "process: pointList size = "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const/4 v8, 0x0

    .line 135
    .local v8, "output":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v16

    if-lez v16, :cond_0

    .line 136
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 137
    .local v14, "strokeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->access$2()Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->getShapeTypeList()Ljava/util/ArrayList;

    move-result-object v12

    .line 139
    .local v12, "shapeTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "output":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 141
    .restart local v8    # "output":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v2, 0x0

    .line 142
    .local v2, "c":I
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-nez v16, :cond_1

    .line 175
    .end local v2    # "c":I
    .end local v12    # "shapeTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v14    # "strokeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mHandler:Landroid/os/Handler;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->access$3(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;)Landroid/os/Handler;

    move-result-object v16

    const/16 v17, 0x0

    new-instance v18, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$HandleInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->mInput:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v8}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$HandleInfo;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static/range {v16 .. v18}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    .line 176
    .local v7, "msg":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mHandler:Landroid/os/Handler;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->access$3(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;)Landroid/os/Handler;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 177
    return-void

    .line 142
    .end local v7    # "msg":Landroid/os/Message;
    .restart local v2    # "c":I
    .restart local v12    # "shapeTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v14    # "strokeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :cond_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Landroid/graphics/PointF;

    .line 143
    .local v10, "points":[Landroid/graphics/PointF;
    array-length v0, v10

    move/from16 v16, v0

    const/16 v18, 0x1

    move/from16 v0, v16

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 144
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v16

    if-lez v16, :cond_2

    .line 145
    new-instance v4, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v4, v14}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>(Ljava/util/ArrayList;)V

    .line 146
    .local v4, "container":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    const-string v18, "ShapeRecognitionPlugin"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v16, "process: name = "

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v19, "["

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v19, "]"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    const-string v18, "ShapeType"

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "c":I
    .local v3, "c":I
    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v4, v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setExtraDataString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v3

    .line 151
    .end local v3    # "c":I
    .end local v4    # "container":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .restart local v2    # "c":I
    :cond_2
    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    .line 155
    :cond_3
    array-length v0, v10

    move/from16 v16, v0

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    new-array v11, v0, [F

    .line 156
    .local v11, "pressures":[F
    array-length v0, v10

    move/from16 v16, v0

    move/from16 v0, v16

    new-array v15, v0, [I

    .line 158
    .local v15, "timestamps":[I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    array-length v0, v10

    move/from16 v16, v0

    move/from16 v0, v16

    if-lt v5, v0, :cond_4

    .line 162
    array-length v0, v10

    move/from16 v16, v0

    const/high16 v18, 0x3f800000    # 1.0f

    aput v18, v11, v16

    .line 164
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->mInput:Ljava/util/List;

    move-object/from16 v16, v0

    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 165
    .local v6, "inputStroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    new-instance v13, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v13, v0, v10, v11, v15}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>(Ljava/lang/String;[Landroid/graphics/PointF;[F[I)V

    .line 168
    .local v13, "stroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPenSize(F)V

    .line 169
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getColor()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setColor(I)V

    .line 170
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setToolType(I)V

    .line 171
    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 159
    .end local v6    # "inputStroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    .end local v13    # "stroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    :cond_4
    const/high16 v16, 0x3f800000    # 1.0f

    aput v16, v11, v5

    .line 160
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v16, v0

    aput v16, v15, v5

    .line 158
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method
