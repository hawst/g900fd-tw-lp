.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;
.super Ljava/lang/Object;
.source "SpenSettingTextLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    .line 898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 902
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 903
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getCurrentTableIndex()I

    move-result v1

    .line 904
    .local v1, "index":I
    const/4 v3, 0x4

    if-ne v1, v3, :cond_0

    .line 905
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$66(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 908
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setNextColorTable(I)V

    .line 909
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$31(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v4

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setSelectBoxPos(I)V

    .line 911
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getCurrentTableIndex()I

    move-result v1

    .line 912
    const/4 v3, 0x3

    if-ne v1, v3, :cond_1

    .line 913
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$67(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 915
    :cond_1
    if-lez v1, :cond_2

    const/4 v3, 0x5

    if-ge v1, v3, :cond_2

    .line 916
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTableSet:[[Ljava/lang/String;

    add-int/lit8 v5, v1, -0x1

    aget-object v4, v4, v5

    iput-object v4, v3, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    .line 918
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v3, 0x2

    if-lt v0, v3, :cond_4

    .line 925
    .end local v0    # "i":I
    .end local v1    # "index":I
    :cond_3
    return-void

    .line 919
    .restart local v0    # "i":I
    .restart local v1    # "index":I
    :cond_4
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    const/4 v3, 0x7

    if-lt v2, v3, :cond_5

    .line 918
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 920
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$68(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)[Landroid/view/View;

    move-result-object v3

    mul-int/lit8 v4, v0, 0x7

    add-int/2addr v4, v2

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    .line 921
    mul-int/lit8 v5, v0, 0x7

    add-int/2addr v5, v2

    aget-object v4, v4, v5

    .line 920
    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 919
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
