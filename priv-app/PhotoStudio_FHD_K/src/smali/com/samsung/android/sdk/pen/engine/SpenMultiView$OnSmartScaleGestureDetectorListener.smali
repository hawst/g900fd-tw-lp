.class Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;
.super Ljava/lang/Object;
.source "SpenMultiView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenMultiView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnSmartScaleGestureDetectorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V
    .locals 0

    .prologue
    .line 2911
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;)V
    .locals 0

    .prologue
    .line 2911
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V

    return-void
.end method


# virtual methods
.method public onChangePan(FF)V
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 2918
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)J

    move-result-wide v2

    const/4 v6, 0x1

    move v4, p1

    move v5, p2

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPan(JFFZ)V
    invoke-static/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;JFFZ)V

    .line 2919
    return-void
.end method

.method public onChangeScale(FFF)V
    .locals 0
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 2914
    return-void
.end method

.method public onFlick(I)Z
    .locals 3
    .param p1, "direction"    # I

    .prologue
    .line 2923
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFlick direction = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2924
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2925
    const/4 v0, 0x1

    .line 2930
    :goto_0
    return v0

    .line 2927
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2928
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;->onFlick(I)Z

    move-result v0

    goto :goto_0

    .line 2930
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onUpdate(Z)V
    .locals 3
    .param p1, "isScreenFrameBuffer"    # Z

    .prologue
    .line 2935
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;Z)V

    .line 2936
    return-void
.end method

.method public onUpdateScreenFrameBuffer()V
    .locals 0

    .prologue
    .line 2940
    return-void
.end method
