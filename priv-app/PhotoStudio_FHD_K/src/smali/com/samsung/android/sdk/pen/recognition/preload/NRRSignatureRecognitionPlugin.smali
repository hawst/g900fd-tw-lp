.class public Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;
.super Ljava/lang/Object;
.source "NRRSignatureRecognitionPlugin.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;,
        Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field public static final VERIFICATION_LEVEL_KEY:Ljava/lang/String; = "VerificationLevel"

.field public static final VERIFICATION_STRICTNESS_HIGH:I = 0x3

.field public static final VERIFICATION_STRICTNESS_LOW:I = 0x1

.field public static final VERIFICATION_STRICTNESS_MEDIUM:I = 0x2

.field private static volatile mModel:Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;


# instance fields
.field private mExecutor:Ljava/util/concurrent/ExecutorService;

.field private mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

.field private mStrictLevel:I

.field private final mUiHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->LOG_TAG:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mModel:Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mUiHandler:Landroid/os/Handler;

    .line 35
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mStrictLevel:I

    .line 23
    return-void
.end method

.method static synthetic access$0()Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mModel:Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    return-object v0
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    return-object v0
.end method


# virtual methods
.method public getMinimumRequiredCount()I
    .locals 1

    .prologue
    .line 147
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->getMinimumRequiredCount()I

    move-result v0

    return v0
.end method

.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 217
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 206
    if-nez p1, :cond_0

    .line 209
    :goto_0
    return-void

    .line 208
    :cond_0
    const-string v0, "VerificationLevel"

    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mStrictLevel:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public getRegisteredCount()I
    .locals 1

    .prologue
    .line 156
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mModel:Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->getSignaturesNumber()I

    move-result v0

    return v0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onLoad"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRNativeInit;->initialize()V

    .line 48
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 49
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mModel:Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mModel:Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    .line 51
    :cond_0
    return-void
.end method

.method public onUnload()V
    .locals 2

    .prologue
    .line 59
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onUnload"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 62
    return-void
.end method

.method public register(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "spenObjectStrokes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->LOG_TAG:Ljava/lang/String;

    const-string v1, "register"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mModel:Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mExecutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_1

    .line 73
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Signature Engine is not Opened!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_1
    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "stroke is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_2
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mModel:Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->addSignature(Ljava/util/List;)Z

    .line 79
    return-void
.end method

.method public request(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "spenObjectStrokes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->LOG_TAG:Ljava/lang/String;

    const-string v1, "request"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mModel:Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mExecutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_1

    .line 90
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Signature Engine is not Opened!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_1
    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "stroke is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "stroke is empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mStrictLevel:I

    const/4 v3, 0x0

    invoke-direct {v1, p0, p1, v2, v3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;Ljava/util/List;ILcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 97
    return-void
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 185
    if-nez p1, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    const-string v1, "VerificationLevel"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    const-string v1, "VerificationLevel"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 189
    .local v0, "level":I
    const/4 v1, 0x1

    if-lt v0, v1, :cond_2

    const/4 v1, 0x3

    if-le v0, v1, :cond_3

    .line 190
    :cond_2
    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Illegal parameter for verification strictness level: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 191
    const-string v3, ". Verification strictness will be set to medium"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 190
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const/4 v0, 0x2

    .line 195
    :cond_3
    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mStrictLevel:I

    .line 196
    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Strictness level set to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mStrictLevel:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 176
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    .line 177
    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 226
    const/4 v0, 0x1

    return v0
.end method

.method public unregisterAll()V
    .locals 2

    .prologue
    .line 165
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->LOG_TAG:Ljava/lang/String;

    const-string v1, "unregisterAll()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mModel:Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->drop()V

    .line 168
    return-void
.end method
