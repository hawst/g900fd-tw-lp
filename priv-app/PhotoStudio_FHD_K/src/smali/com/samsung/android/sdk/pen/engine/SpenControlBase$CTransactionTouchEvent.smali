.class public Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;
.super Ljava/lang/Object;
.source "SpenControlBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "CTransactionTouchEvent"
.end annotation


# instance fields
.field protected mIsTouchDownPressed:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->mIsTouchDownPressed:Z

    .line 339
    return-void
.end method


# virtual methods
.method public check(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 342
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v0, v1, 0xff

    .line 343
    .local v0, "action":I
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    if-ne v0, v2, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->mIsTouchDownPressed:Z

    if-nez v1, :cond_1

    .line 344
    const/4 v0, 0x0

    .line 345
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 348
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 358
    :goto_0
    return-void

    .line 350
    :pswitch_0
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->mIsTouchDownPressed:Z

    goto :goto_0

    .line 353
    :pswitch_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->mIsTouchDownPressed:Z

    goto :goto_0

    .line 348
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
