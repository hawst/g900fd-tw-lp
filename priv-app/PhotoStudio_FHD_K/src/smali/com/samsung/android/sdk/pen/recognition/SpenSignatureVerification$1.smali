.class Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$1;
.super Ljava/lang/Object;
.source "SpenSignatureVerification.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->setResultListener(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;

    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Ljava/util/List;Z)V
    .locals 1
    .param p2, "result"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 260
    .local p1, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->access$0(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;)Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->access$0(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;)Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;->onResult(Ljava/util/List;Z)V

    .line 263
    :cond_0
    return-void
.end method
