.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;
.super Landroid/widget/LinearLayout;
.source "SpenSettingEraserLayout2.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$ActionListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;
    }
.end annotation


# static fields
.field protected static final BODY_LAYOUT_HEIGHT:I = 0x90

.field protected static final ERASER_SIZE_MAX:I = 0x63

.field protected static final EXIT_BUTTON_RAW_HEIGHT:I = 0x24

.field protected static final EXIT_BUTTON_RAW_WIDTH:I = 0x26

.field protected static final LINE_BUTTON_RAW_HEIGHT:I = 0x11

.field protected static final LINE_BUTTON_RAW_WIDTH:I = 0x1

.field private static final REP_DELAY:I = 0x14

.field protected static final TITLE_LAYOUT_HEIGHT:I = 0x2a

.field public static final VIEW_MODE_NORMAL:I = 0x0

.field public static final VIEW_MODE_SIZE:I = 0x2

.field public static final VIEW_MODE_TITLE:I = 0x3

.field public static final VIEW_MODE_TYPE:I = 0x1

.field private static final bodyLeftPath:Ljava/lang/String; = "vienna_popup_bg"

.field private static final btnFocusPath:Ljava/lang/String; = "snote_popup_btn_focus"

.field private static final btnNoramlPath:Ljava/lang/String; = "snote_popup_btn_normal"

.field private static final btnPressPath:Ljava/lang/String; = "snote_popup_btn_press"

.field private static final eraserPopupDrawPress:Ljava/lang/String; = "snote_eraser_popup_draw_press"

.field private static final eraserPopupDrawUnselect:Ljava/lang/String; = "snote_eraser_popup_draw"

.field private static final eraserPopupTextPress:Ljava/lang/String; = "snote_eraser_popup_text_press"

.field private static final eraserPopupTextUnselect:Ljava/lang/String; = "snote_eraser_popup_text"

.field private static final handelFocusPath:Ljava/lang/String; = "progress_handle_focus"

.field private static final handelPath:Ljava/lang/String; = "progress_handle_normal"

.field private static final handelPressPath:Ljava/lang/String; = "progress_handle_press"

.field private static final minusBgFocusPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_focus"

.field private static final minusBgPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_normal"

.field private static final minusBgPressPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_press"

.field private static final plusBgFocusPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_focus"

.field private static final plusBgPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_normal"

.field private static final plusBgPressPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_press"

.field private static final popupMaxPath:Ljava/lang/String; = "snote_popup_arrow_max_normal"

.field private static final popupMinPath:Ljava/lang/String; = "snote_popup_arrow_min_normal"

.field private static final progressBgPath:Ljava/lang/String; = "progress_bg"

.field private static final progressShadowPath:Ljava/lang/String; = "progress_shadow"

.field private static final titleLeftPath:Ljava/lang/String; = "vienna_popup_title_bg"


# instance fields
.field private mAutoDecrement:Z

.field private mAutoIncrement:Z

.field protected mBodyLayout:Landroid/view/View;

.field protected mCanvasLayout:Landroid/widget/RelativeLayout;

.field protected mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

.field protected mClearAllButton:Landroid/view/View;

.field private final mClearAllListener:Landroid/view/View$OnClickListener;

.field private mCount:I

.field protected mCurrentEraserType:I

.field protected mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field protected mEraserContext:Landroid/content/Context;

.field protected mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

.field private final mEraserKeyListener:Landroid/view/View$OnKeyListener;

.field protected mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;

.field protected mEraserMinusButton:Landroid/view/View;

.field protected mEraserPlusButton:Landroid/view/View;

.field protected mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

.field private final mEraserSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field protected mEraserSizeSeekBar:Landroid/widget/SeekBar;

.field protected mEraserSizeTextView:Landroid/widget/TextView;

.field protected mEraserType01:Landroid/widget/ImageButton;

.field protected mEraserType02:Landroid/widget/ImageButton;

.field protected mEraserTypeLayout:Landroid/view/ViewGroup;

.field private final mEraserTypeListner:Landroid/view/View$OnClickListener;

.field protected mEraserTypeView:[Landroid/view/View;

.field protected mFirstLongPress:Z

.field private final mHandler:Landroid/os/Handler;

.field private mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field protected mLeftMargin:I

.field private final mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

.field private final mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

.field private final mMinusButtonListener:Landroid/view/View$OnClickListener;

.field protected mMovableRect:Landroid/graphics/Rect;

.field private final mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

.field private final mPlusButtonListener:Landroid/view/View$OnClickListener;

.field private final mPopupButtonListener:Landroid/view/View$OnClickListener;

.field private mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

.field protected mPopupMaxButton:Landroid/view/View;

.field protected mPopupMinButton:Landroid/view/View;

.field private mScrollTimer:Ljava/util/Timer;

.field protected mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

.field protected mSettingSizeLayout:Landroid/view/ViewGroup;

.field protected mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field protected mTitleLayout:Landroid/view/View;

.field protected mTopMargin:I

.field protected mTotalLeftMargin:I

.field protected mTotalTopMargin:I

.field private final mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

.field private final mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

.field protected mViewMode:I

.field private final repeatUpdateHandler:Landroid/os/Handler;

.field protected requestLayoutDisable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p3, "relativeLayout"    # Landroid/widget/RelativeLayout;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 455
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 65
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

    .line 66
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    .line 92
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    .line 93
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 94
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mFirstLongPress:Z

    .line 103
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 105
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;

    .line 109
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    .line 120
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mHandler:Landroid/os/Handler;

    .line 172
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 181
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllListener:Landroid/view/View$OnClickListener;

    .line 210
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    .line 225
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 238
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 251
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->repeatUpdateHandler:Landroid/os/Handler;

    .line 252
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoIncrement:Z

    .line 253
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoDecrement:Z

    .line 277
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 290
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    .line 303
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 316
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    .line 330
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 398
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeListner:Landroid/view/View$OnClickListener;

    .line 406
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserKeyListener:Landroid/view/View$OnKeyListener;

    .line 456
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    .line 457
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 458
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 459
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 460
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    .line 461
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 462
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->initView()V

    .line 463
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setListener()V

    .line 464
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    .line 465
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p3, "relativeLayout"    # Landroid/widget/RelativeLayout;
    .param p4, "ratio"    # F

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 493
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 65
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

    .line 66
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    .line 92
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    .line 93
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 94
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mFirstLongPress:Z

    .line 103
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 105
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;

    .line 109
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    .line 120
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mHandler:Landroid/os/Handler;

    .line 172
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 181
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllListener:Landroid/view/View$OnClickListener;

    .line 210
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    .line 225
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 238
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 251
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->repeatUpdateHandler:Landroid/os/Handler;

    .line 252
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoIncrement:Z

    .line 253
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoDecrement:Z

    .line 277
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 290
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    .line 303
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 316
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    .line 330
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 398
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeListner:Landroid/view/View$OnClickListener;

    .line 406
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserKeyListener:Landroid/view/View$OnKeyListener;

    .line 494
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    .line 495
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, p1, p2, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 496
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 497
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 498
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    .line 499
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 500
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->initView()V

    .line 501
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setListener()V

    .line 502
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    .line 503
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;Z)V
    .locals 0

    .prologue
    .line 252
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoIncrement:Z

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScrollTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->repeatUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Z
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoIncrement:Z

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;Z)V
    .locals 0

    .prologue
    .line 253
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoDecrement:Z

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Z
    .locals 1

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoDecrement:Z

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1213
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->eraserTypeSetting(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCount:I

    return v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;I)V
    .locals 0

    .prologue
    .line 118
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCount:I

    return-void
.end method

.method private bodyBg()Landroid/view/ViewGroup;
    .locals 7

    .prologue
    .line 657
    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 658
    .local v2, "layout":Landroid/widget/LinearLayout;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    .line 659
    const/4 v5, -0x2

    .line 658
    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 661
    .local v3, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 663
    new-instance v0, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 664
    .local v0, "bodyLeft":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 665
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v5, 0x43a48000    # 329.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x43100000    # 144.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 664
    invoke-direct {v1, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 669
    .local v1, "bodyLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v4, 0x1

    iput-boolean v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 670
    const/16 v4, 0x9

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 671
    const/16 v4, 0xa

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 672
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 674
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v5, "vienna_popup_bg"

    invoke-virtual {v4, v0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 677
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 678
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 680
    return-object v2
.end method

.method private bodyLayout()Landroid/view/ViewGroup;
    .locals 4

    .prologue
    .line 633
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 634
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 635
    const/4 v3, -0x2

    .line 634
    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 636
    .local v1, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 637
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->bodyBg()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 639
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->eraserSettingClearAllButton()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    .line 640
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->eraserTypeLayout()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    .line 641
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->eraserSizeLayout()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    .line 642
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 643
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 644
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 645
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 647
    return-object v0
.end method

.method private checkPosition()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1335
    const/4 v4, 0x2

    new-array v0, v4, [I

    .line 1336
    .local v0, "location":[I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43640000    # 228.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1337
    .local v2, "minWidth":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43010000    # 129.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1339
    .local v1, "minHeight":I
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLocationOnScreen([I)V

    .line 1341
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1343
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    aget v4, v0, v6

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-ge v4, v5, :cond_0

    .line 1344
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1346
    :cond_0
    aget v4, v0, v7

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    if-ge v4, v5, :cond_1

    .line 1347
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1350
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    aget v5, v0, v6

    sub-int/2addr v4, v5

    if-ge v4, v2, :cond_2

    .line 1351
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v4, v2

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1353
    iget v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-gez v4, :cond_2

    .line 1354
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1357
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    aget v5, v0, v7

    sub-int/2addr v4, v5

    if-ge v4, v1, :cond_3

    .line 1358
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, v1

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1360
    iget v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-gez v4, :cond_3

    .line 1361
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1365
    :cond_3
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1366
    return-void
.end method

.method private eraserSettingClearAllButton()Landroid/view/View;
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/high16 v12, -0x1000000

    const/high16 v11, 0x41300000    # 11.0f

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1124
    new-instance v2, Landroid/widget/Button;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v2, v5}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1125
    .local v2, "clearAllButton":Landroid/widget/Button;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1126
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v6, 0x43998000    # 307.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x423c0000    # 47.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1125
    invoke-direct {v3, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1127
    .local v3, "clearAllButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1128
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x42ac0000    # 86.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1133
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41800000    # 16.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1134
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1127
    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1136
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1137
    invoke-virtual {v2, v10}, Landroid/widget/Button;->setFocusable(Z)V

    .line 1138
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v6, "string_clear_all"

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1139
    new-array v0, v13, [[I

    .line 1140
    .local v0, "arrayOfStates":[[I
    new-array v1, v10, [I

    .line 1141
    .local v1, "arrayOfStates1":[I
    const v5, 0x10100a7

    aput v5, v1, v9

    .line 1142
    aput-object v1, v0, v9

    .line 1143
    new-array v5, v9, [I

    aput-object v5, v0, v10

    .line 1144
    new-array v4, v13, [I

    .line 1145
    .local v4, "textColor":[I
    aput v12, v4, v9

    .line 1146
    aput v12, v4, v10

    .line 1147
    new-instance v5, Landroid/content/res/ColorStateList;

    invoke-direct {v5, v0, v4}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1148
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41600000    # 14.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v2, v9, v5}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1150
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v6, "snote_popup_btn_normal"

    const-string v7, "snote_popup_btn_press"

    const-string v8, "snote_popup_btn_focus"

    invoke-virtual {v5, v2, v6, v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    return-object v2
.end method

.method private eraserSettingSizeSeekBar()Landroid/widget/SeekBar;
    .locals 14

    .prologue
    .line 1022
    new-instance v11, Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v11, v3}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 1025
    .local v11, "seekBar":Landroid/widget/SeekBar;
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1026
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43650000    # 229.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1028
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42040000    # 33.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1025
    invoke-direct {v12, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1029
    .local v12, "seekBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1030
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40400000    # 3.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1031
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x40a00000    # 5.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1029
    invoke-virtual {v12, v3, v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1033
    const/4 v3, 0x1

    iput-boolean v3, v12, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1034
    const/16 v3, 0xe

    invoke-virtual {v12, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1035
    const/16 v3, 0x8

    invoke-virtual {v12, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1036
    invoke-virtual {v11, v12}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1037
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    const/4 v4, 0x0

    .line 1038
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    const/4 v6, 0x0

    .line 1037
    invoke-virtual {v11, v3, v4, v5, v6}, Landroid/widget/SeekBar;->setPadding(IIII)V

    .line 1042
    const/16 v3, 0x63

    invoke-virtual {v11, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1044
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "progress_handle_normal"

    const-string v2, "progress_handle_press"

    const-string v3, "progress_handle_focus"

    const/16 v4, 0x21

    const/16 v5, 0x21

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v3

    invoke-virtual {v11, v3}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 1045
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40400000    # 3.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v11, v3}, Landroid/widget/SeekBar;->setThumbOffset(I)V

    .line 1047
    new-instance v9, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v9}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 1048
    .local v9, "localGradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    const/4 v3, 0x0

    invoke-virtual {v9, v3}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1049
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40e00000    # 7.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v9, v3}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 1051
    new-instance v8, Landroid/graphics/drawable/ClipDrawable;

    const/4 v3, 0x3

    const/4 v4, 0x1

    invoke-direct {v8, v9, v3, v4}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 1052
    .local v8, "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v4, "progress_bg"

    const/16 v5, 0xe5

    const/16 v6, 0xc

    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1053
    .local v1, "bgDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v4, "progress_shadow"

    const/16 v5, 0xe5

    const/16 v6, 0xc

    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 1054
    .local v13, "shadowDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 1055
    .local v0, "bgInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v2, Landroid/graphics/drawable/InsetDrawable;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, v13

    invoke-direct/range {v2 .. v7}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 1057
    .local v2, "shadowInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v10, Landroid/graphics/drawable/LayerDrawable;

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    const/4 v4, 0x2

    .line 1058
    aput-object v8, v3, v4

    .line 1057
    invoke-direct {v10, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 1059
    .local v10, "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    invoke-virtual {v11, v10}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1061
    return-object v11
.end method

.method private eraserSizeButtonSeekbar()Landroid/view/ViewGroup;
    .locals 14

    .prologue
    const/high16 v13, 0x41000000    # 8.0f

    const/high16 v12, 0x40a00000    # 5.0f

    const/high16 v11, 0x40000000    # 2.0f

    const/16 v5, 0x24

    const/high16 v10, 0x42100000    # 36.0f

    .line 1070
    new-instance v9, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v9, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1071
    .local v9, "eraserSettingSizeDisplayLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    .line 1072
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1073
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 1074
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1072
    invoke-direct {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1075
    .local v8, "eraserPlusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v8, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1076
    const/16 v0, 0xb

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1077
    const/16 v0, 0x8

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1079
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    const/4 v1, 0x0

    .line 1080
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1079
    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1082
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1083
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1085
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    const-string v2, "snote_popup_progress_btn_plus_normal"

    const-string v3, "snote_popup_progress_btn_plus_press"

    .line 1086
    const-string v4, "snote_popup_progress_btn_plus_focus"

    move v6, v5

    .line 1085
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1088
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    .line 1089
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1090
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 1091
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1089
    invoke-direct {v7, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1092
    .local v7, "eraserMinusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1093
    const/16 v0, 0x9

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1094
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1096
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    const/4 v1, 0x0

    .line 1097
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1096
    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1099
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1100
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1102
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    const-string v2, "snote_popup_progress_btn_minus_normal"

    const-string v3, "snote_popup_progress_btn_minus_press"

    .line 1103
    const-string v4, "snote_popup_progress_btn_minus_focus"

    move v6, v5

    .line 1102
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1107
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->eraserSettingSizeSeekBar()Landroid/widget/SeekBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    .line 1109
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1111
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1112
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1114
    return-object v9
.end method

.method private eraserSizeLayout()Landroid/view/ViewGroup;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 988
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 989
    .local v0, "eraserSettingSizeLayout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 990
    const/4 v3, -0x1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42780000    # 62.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 989
    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 993
    .local v1, "eraserSettingSizeLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41300000    # 11.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v1, v7, v3, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 995
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 997
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    .line 998
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 999
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    const/16 v4, 0x56

    const/16 v5, 0x57

    const/16 v6, 0x5b

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1000
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41400000    # 12.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v7, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1004
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1005
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42480000    # 50.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41500000    # 13.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1004
    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1006
    .local v2, "eraserSizeTextParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1007
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1008
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1010
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->eraserSizeButtonSeekbar()Landroid/view/ViewGroup;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

    .line 1011
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1012
    return-object v0
.end method

.method private eraserTypeLayout()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 930
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 931
    .local v1, "localLinearLayout":Landroid/widget/LinearLayout;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 933
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 934
    const/4 v2, -0x1

    const/4 v3, -0x2

    .line 933
    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 935
    .local v0, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 937
    new-instance v2, Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType01:Landroid/widget/ImageButton;

    .line 938
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType01:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v4, "snote_eraser_popup_draw"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 939
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType01:Landroid/widget/ImageButton;

    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$13;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 955
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType01:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 957
    new-instance v2, Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType02:Landroid/widget/ImageButton;

    .line 958
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType02:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v4, "snote_eraser_popup_text"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 959
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType02:Landroid/widget/ImageButton;

    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$14;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 976
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType02:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 978
    return-object v1
.end method

.method private eraserTypeSetting(Landroid/view/View;)V
    .locals 5
    .param p1, "selectedView"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1214
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 1255
    invoke-virtual {p1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 1256
    return-void

    .line 1215
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    .line 1216
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2, v4}, Landroid/view/View;->setSelected(Z)V

    .line 1218
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1220
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    .line 1221
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    .line 1223
    if-nez v0, :cond_3

    .line 1224
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    .line 1229
    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 1214
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1225
    :cond_3
    if-ne v0, v3, :cond_1

    .line 1226
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    goto :goto_1

    .line 1231
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_2

    .line 1232
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-result-object v1

    .line 1233
    .local v1, "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    if-eqz v1, :cond_2

    .line 1234
    iput v4, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    .line 1235
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto :goto_2

    .line 1241
    .end local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_2

    .line 1242
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-result-object v1

    .line 1243
    .restart local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    if-eqz v1, :cond_2

    .line 1244
    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    .line 1245
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto :goto_2

    .line 1229
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getMovableRect()Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 684
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 685
    .local v0, "location":[I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 687
    .local v1, "r":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    .line 689
    aget v2, v0, v4

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLeftMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 690
    aget v2, v0, v5

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTopMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 691
    aget v2, v0, v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 692
    aget v2, v0, v5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 694
    return-object v1
.end method

.method private playScrollAnimationForBottomBar(III)V
    .locals 8
    .param p1, "delay"    # I
    .param p2, "from"    # I
    .param p3, "to"    # I

    .prologue
    .line 1163
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScrollTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1164
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1166
    :cond_0
    move v6, p2

    .line 1167
    .local v6, "fromFinal":I
    move v7, p3

    .line 1168
    .local v7, "toFinal":I
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScrollTimer:Ljava/util/Timer;

    .line 1169
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCount:I

    .line 1170
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScrollTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$15;

    invoke-direct {v1, p0, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;II)V

    .line 1204
    const-wide/16 v2, 0xa

    int-to-long v4, p1

    .line 1170
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 1205
    return-void
.end method

.method private popupMaxButton()Landroid/view/View;
    .locals 8

    .prologue
    const/16 v5, 0x20

    const/high16 v3, 0x42280000    # 42.0f

    .line 889
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 890
    .local v1, "popupMaxButton":Landroid/widget/ImageButton;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 891
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 890
    invoke-direct {v7, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 892
    .local v7, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v0, 0xb

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 894
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 895
    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 896
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 897
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 899
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v2, "snote_popup_arrow_max_normal"

    const-string v3, "snote_popup_arrow_max_normal"

    const-string v4, "snote_popup_arrow_max_normal"

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 901
    return-object v1
.end method

.method private popupMinButton()Landroid/view/View;
    .locals 8

    .prologue
    const/16 v5, 0x20

    const/high16 v3, 0x42280000    # 42.0f

    .line 908
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 909
    .local v1, "popupMinButton":Landroid/widget/ImageButton;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 910
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 909
    invoke-direct {v7, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 911
    .local v7, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v0, 0xb

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 913
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 914
    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 915
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 916
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 918
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v2, "snote_popup_arrow_min_normal"

    const-string v3, "snote_popup_arrow_min_normal"

    const-string v4, "snote_popup_arrow_min_normal"

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 920
    return-object v1
.end method

.method private rotatePosition()V
    .locals 15

    .prologue
    const v14, 0x3f7d70a4    # 0.99f

    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 1259
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1261
    .local v4, "newMovableRect":Landroid/graphics/Rect;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalTopMargin:I

    if-eq v10, v11, :cond_2

    .line 1262
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalTopMargin:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalLeftMargin:I

    add-int/2addr v10, v11

    iput v10, v4, Landroid/graphics/Rect;->left:I

    .line 1266
    :goto_0
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalLeftMargin:I

    if-eq v10, v11, :cond_3

    .line 1267
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalLeftMargin:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalTopMargin:I

    add-int/2addr v10, v11

    iput v10, v4, Landroid/graphics/Rect;->top:I

    .line 1272
    :goto_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iput v10, v4, Landroid/graphics/Rect;->right:I

    .line 1273
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    iput v10, v4, Landroid/graphics/Rect;->bottom:I

    .line 1281
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 1282
    .local v6, "r":Landroid/graphics/Rect;
    const/4 v10, 0x2

    new-array v3, v10, [I

    .line 1283
    .local v3, "location":[I
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLocationOnScreen([I)V

    .line 1285
    aget v10, v3, v12

    iput v10, v6, Landroid/graphics/Rect;->left:I

    .line 1286
    const/4 v10, 0x1

    aget v10, v3, v10

    iput v10, v6, Landroid/graphics/Rect;->top:I

    .line 1287
    iget v10, v6, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getWidth()I

    move-result v11

    add-int/2addr v10, v11

    iput v10, v6, Landroid/graphics/Rect;->right:I

    .line 1288
    iget v10, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getHeight()I

    move-result v11

    add-int/2addr v10, v11

    iput v10, v6, Landroid/graphics/Rect;->bottom:I

    .line 1292
    iget v10, v6, Landroid/graphics/Rect;->left:I

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    sub-int/2addr v10, v11

    int-to-float v2, v10

    .line 1293
    .local v2, "left":F
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    iget v11, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v10, v11

    int-to-float v7, v10

    .line 1294
    .local v7, "right":F
    iget v10, v6, Landroid/graphics/Rect;->top:I

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    sub-int/2addr v10, v11

    int-to-float v8, v10

    .line 1295
    .local v8, "top":F
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iget v11, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v10, v11

    int-to-float v0, v10

    .line 1297
    .local v0, "bottom":F
    add-float v10, v2, v7

    div-float v1, v2, v10

    .line 1298
    .local v1, "hRatio":F
    add-float v10, v8, v0

    div-float v9, v8, v10

    .line 1304
    .local v9, "vRatio":F
    cmpl-float v10, v1, v14

    if-lez v10, :cond_4

    .line 1305
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1310
    :cond_0
    :goto_2
    cmpl-float v10, v9, v14

    if-lez v10, :cond_5

    .line 1311
    const/high16 v9, 0x3f800000    # 1.0f

    .line 1316
    :cond_1
    :goto_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1318
    .local v5, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v11

    if-ge v10, v11, :cond_6

    .line 1319
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v1

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    iput v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1324
    :goto_4
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v11

    if-ge v10, v11, :cond_7

    .line 1325
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v9

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    iput v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1331
    :goto_5
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1332
    return-void

    .line 1264
    .end local v0    # "bottom":F
    .end local v1    # "hRatio":F
    .end local v2    # "left":F
    .end local v3    # "location":[I
    .end local v5    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v6    # "r":Landroid/graphics/Rect;
    .end local v7    # "right":F
    .end local v8    # "top":F
    .end local v9    # "vRatio":F
    :cond_2
    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalLeftMargin:I

    iput v10, v4, Landroid/graphics/Rect;->left:I

    goto/16 :goto_0

    .line 1269
    :cond_3
    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalTopMargin:I

    iput v10, v4, Landroid/graphics/Rect;->top:I

    goto/16 :goto_1

    .line 1306
    .restart local v0    # "bottom":F
    .restart local v1    # "hRatio":F
    .restart local v2    # "left":F
    .restart local v3    # "location":[I
    .restart local v6    # "r":Landroid/graphics/Rect;
    .restart local v7    # "right":F
    .restart local v8    # "top":F
    .restart local v9    # "vRatio":F
    :cond_4
    cmpg-float v10, v1, v13

    if-gez v10, :cond_0

    .line 1307
    const/4 v1, 0x0

    goto :goto_2

    .line 1312
    :cond_5
    cmpg-float v10, v9, v13

    if-gez v10, :cond_1

    .line 1313
    const/4 v9, 0x0

    goto :goto_3

    .line 1321
    .restart local v5    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_6
    iput v12, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_4

    .line 1327
    :cond_7
    iput v12, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_5
.end method

.method private setListener()V
    .locals 3

    .prologue
    .line 782
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 783
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 786
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 787
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 789
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMinButton:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 790
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMinButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 793
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 795
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 796
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 797
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 800
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    if-eqz v1, :cond_4

    .line 802
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 803
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 804
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 807
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_5

    .line 808
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 811
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    if-eqz v1, :cond_6

    .line 812
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 815
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    if-eqz v1, :cond_7

    .line 816
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x1

    if-le v0, v1, :cond_9

    .line 824
    .end local v0    # "i":I
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_8

    .line 825
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 828
    :cond_8
    return-void

    .line 817
    .restart local v0    # "i":I
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v1, v1, v0

    if-eqz v1, :cond_a

    .line 818
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 816
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private titleBg()Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x1

    const/4 v7, -0x1

    .line 549
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 550
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 552
    new-instance v1, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 553
    .local v1, "titleLeft":Landroid/widget/ImageView;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 555
    .local v2, "titleLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v8, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 556
    const/16 v5, 0x9

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 557
    invoke-virtual {v2, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 560
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 567
    new-instance v3, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 568
    .local v3, "titleRight":Landroid/widget/ImageView;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 569
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v6, 0x43248000    # 164.5f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 568
    invoke-direct {v4, v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 570
    .local v4, "titleRightParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 571
    const/16 v5, 0xb

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 572
    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 573
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 575
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v6, "vienna_popup_title_bg"

    invoke-virtual {v5, v1, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 580
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 584
    return-object v0
.end method

.method private titleLayout()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 525
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 526
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 527
    const/high16 v4, 0x42280000    # 42.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 526
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 529
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->popupMaxButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    .line 530
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->popupMinButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMinButton:Landroid/view/View;

    .line 531
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->titleBg()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 532
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->titleText()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 535
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 536
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 537
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 539
    return-object v0
.end method

.method private titleText()Landroid/view/View;
    .locals 9

    .prologue
    const/16 v8, 0x13

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 589
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 590
    .local v1, "titleView":Landroid/widget/TextView;
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 591
    const/high16 v4, 0x42280000    # 42.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v2, v6, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 590
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 592
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 593
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 594
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 596
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_eraser_settings"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 597
    .local v0, "eraserText":Ljava/lang/String;
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 598
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 599
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 602
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x1c

    if-le v2, v3, :cond_0

    .line 603
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 622
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_eraser_settings"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 623
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41500000    # 13.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v5, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 624
    return-object v1

    .line 604
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v8, :cond_1

    .line 605
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41600000    # 14.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 607
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method

.method private totalLayout()V
    .locals 3

    .prologue
    .line 509
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v2, 0x43a48000    # 329.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 510
    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 509
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 511
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setOrientation(I)V

    .line 512
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->titleLayout()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    .line 513
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->bodyLayout()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->addView(Landroid/view/View;)V

    .line 515
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->addView(Landroid/view/View;)V

    .line 516
    return-void
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 702
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 703
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 704
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserContext:Landroid/content/Context;

    .line 706
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    if-nez v1, :cond_0

    .line 758
    :goto_0
    return-void

    .line 710
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 711
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    .line 712
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 713
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

    .line 718
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 719
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    .line 720
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 721
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    .line 722
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 723
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    .line 724
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 725
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    .line 726
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType01:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 727
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType01:Landroid/widget/ImageButton;

    .line 728
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType02:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 729
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType02:Landroid/widget/ImageButton;

    .line 730
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 731
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_3

    .line 736
    .end local v0    # "i":I
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    .line 737
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 738
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    .line 739
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 740
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    .line 741
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 742
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    .line 744
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 745
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-eqz v1, :cond_2

    .line 746
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    .line 747
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    const/4 v2, 0x1

    aput-object v3, v1, v2

    .line 748
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 752
    :cond_2
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;

    .line 753
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 754
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 756
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->close()V

    .line 757
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    goto/16 :goto_0

    .line 732
    .restart local v0    # "i":I
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 733
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aput-object v3, v1, v0

    .line 731
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 1

    .prologue
    .line 1452
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    return-object v0
.end method

.method public getViewMode()I
    .locals 1

    .prologue
    .line 1436
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    return v0
.end method

.method protected initView()V
    .locals 5

    .prologue
    .line 762
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->totalLayout()V

    .line 764
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/LinearLayout;

    .line 765
    .local v0, "eraserTypeViewGroup":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    new-array v2, v2, [Landroid/view/View;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    .line 766
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 770
    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 772
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-nez v2, :cond_0

    .line 773
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 774
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    const/4 v3, 0x0

    new-instance v4, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    aput-object v4, v2, v3

    .line 775
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    const/4 v3, 0x1

    new-instance v4, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    aput-object v4, v2, v3

    .line 778
    :cond_0
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setVisibility(I)V

    .line 779
    return-void

    .line 767
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v1

    .line 766
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mFirstLongPress:Z

    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 57
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 58
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->rotatePosition()V

    .line 61
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 62
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 5
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 1371
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43640000    # 228.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1372
    .local v2, "minWidth":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43010000    # 129.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1374
    .local v1, "minHeight":I
    if-lt p1, v2, :cond_0

    if-ge p2, v1, :cond_1

    .line 1375
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1376
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->checkPosition()V

    .line 1379
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 1380
    .local v0, "handler":Landroid/os/Handler;
    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$16;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    invoke-virtual {v0, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1388
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 1390
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 10
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1395
    if-ne p1, p0, :cond_1

    if-nez p2, :cond_1

    .line 1397
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 1398
    .local v0, "location":[I
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLocationOnScreen([I)V

    .line 1400
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1402
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    new-instance v3, Landroid/graphics/Rect;

    aget v4, v0, v9

    aget v5, v0, v8

    aget v6, v0, v9

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getWidth()I

    move-result v7

    add-int/2addr v6, v7

    aget v7, v0, v8

    .line 1403
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1402
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v2

    .line 1403
    if-nez v2, :cond_0

    .line 1404
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->checkPosition()V

    .line 1407
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1408
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42240000    # 41.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1409
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43480000    # 200.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    .line 1410
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    .line 1409
    mul-float/2addr v3, v4

    .line 1410
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1411
    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    .line 1409
    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 1408
    add-int v1, v2, v3

    .line 1412
    .local v1, "seek_label_pos":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    int-to-float v3, v1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setX(F)V

    .line 1413
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setY(F)V

    .line 1414
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1418
    .end local v0    # "location":[I
    .end local v1    # "seek_label_pos":I
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 1419
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    if-nez v0, :cond_0

    .line 45
    invoke-super {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 48
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    .line 49
    return-void
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$ActionListener;

    .prologue
    .line 1659
    return-void
.end method

.method public setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 0
    .param p1, "canvasView"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .prologue
    .line 1540
    if-eqz p1, :cond_0

    .line 1541
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 1543
    :cond_0
    return-void
.end method

.method public setEraserInfoList([Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 0
    .param p1, "list"    # [Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    .line 1613
    if-eqz p1, :cond_0

    .line 1614
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 1616
    :cond_0
    return-void
.end method

.method public setEraserListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;

    .prologue
    .line 1631
    if-eqz p1, :cond_0

    .line 1632
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;

    .line 1634
    :cond_0
    return-void
.end method

.method public setIndicatorPosition(I)V
    .locals 0
    .param p1, "x"    # I

    .prologue
    .line 852
    return-void
.end method

.method public setInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 2
    .param p1, "settingEraserInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    .line 1560
    if-nez p1, :cond_0

    .line 1570
    :goto_0
    return-void

    .line 1564
    :cond_0
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    .line 1565
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v0, v0, v1

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 1567
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1568
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 1569
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    goto :goto_0
.end method

.method public setLayoutHeight(I)V
    .locals 1
    .param p1, "height"    # I

    .prologue
    .line 1156
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1157
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1158
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1159
    return-void
.end method

.method public setPopup(Z)V
    .locals 4
    .param p1, "open"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 194
    if-eqz p1, :cond_0

    .line 195
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 196
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 199
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getHeight()I

    move-result v0

    .line 200
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x433a0000    # 186.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 199
    invoke-direct {p0, v3, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->playScrollAnimationForBottomBar(III)V

    .line 208
    :goto_0
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 206
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42280000    # 42.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-direct {p0, v3, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->playScrollAnimationForBottomBar(III)V

    goto :goto_0
.end method

.method public setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

    .prologue
    .line 1637
    if-eqz p1, :cond_0

    .line 1638
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

    .line 1640
    :cond_0
    return-void
.end method

.method public setPosition(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 871
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 873
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 874
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 876
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 878
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mFirstLongPress:Z

    if-eqz v1, :cond_0

    .line 879
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLeftMargin:I

    sub-int/2addr v1, p1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLeftMargin:I

    .line 880
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTopMargin:I

    sub-int/2addr v1, p2

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTopMargin:I

    .line 883
    :cond_0
    return-void
.end method

.method public setViewMode(I)V
    .locals 5
    .param p1, "viewMode"    # I

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1469
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    .line 1472
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    .line 1473
    .local v0, "tempRequestLayoutDisable":Z
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    .line 1476
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    if-nez v1, :cond_0

    .line 1477
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1478
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1479
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1480
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1505
    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    .line 1507
    return-void

    .line 1481
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1482
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1483
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1484
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1485
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 1486
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 1487
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1488
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1489
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1490
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 1491
    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 1492
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1493
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1497
    :cond_3
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    .line 1498
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1499
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1500
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1501
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 1517
    if-nez p1, :cond_0

    .line 1518
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-nez v0, :cond_0

    .line 1519
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->loadImage()V

    .line 1523
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1524
    return-void
.end method
