.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;
.super Ljava/lang/Object;
.source "SpenStrokeFrame.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays",
        "InlinedApi"
    }
.end annotation


# static fields
.field private static final CAMERA_TYPE:Ljava/lang/String; = "CAMERA_TYPE"

.field private static final CHANGE_BG_DRAWABLE_NAME:Ljava/lang/String; = "selector_change_stroke_frame_bg"

.field private static final CHANGE_BG_DRAWABLE_NAME_LL:Ljava/lang/String; = "snote_save_btn_normal"

.field private static final CHANGE_CAMERA_DRAWABLE_NAME:Ljava/lang/String; = "snote_photoframe_toggle"

.field private static final CHANGE_CAMERA_FRONT_DESCRIPTION_NAME:Ljava/lang/String; = "string_switch_to_front_camera"

.field private static final CHANGE_CAMERA_IMAGE_UX_TABLE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Rect;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final CHANGE_CAMERA_REAR_DESCRIPTION_NAME:Ljava/lang/String; = "string_switch_to_rear_camera"

.field private static final CHANGE_STROKE_FRAME_BEAUTIFY_DESCRIPTION_NAME:Ljava/lang/String; = "string_transform_into_auto_shape"

.field private static final CHANGE_STROKE_FRAME_BEAUTIFY_DRAWABLE_NAME:Ljava/lang/String; = "snote_photoframe_refine"

.field private static final CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Rect;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final CHANGE_STROKE_FRAME_ORIGINAL_DESCRIPTION_NAME:Ljava/lang/String; = "string_transform_back_to_original_shape"

.field private static final CHANGE_STROKE_FRAME_ORIGINAL_DRAWABLE_NAME:Ljava/lang/String; = "snote_photoframe_undo"

.field private static HASH_KEY_IMAGE_MARGIN:I = 0x0

.field private static HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I = 0x0

.field private static HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I = 0x0

.field private static HASH_KEY_IMAGE_SIZE:I = 0x0

.field private static HASH_KEY_PIXEL_1080_1920:I = 0x0

.field private static HASH_KEY_PIXEL_2560_1600:I = 0x0

.field private static HASH_KEY_PIXEL_720_1280:I = 0x0

.field private static HASH_KEY_PIXEL_DEFAULT:I = 0x0

.field private static final PIXEL_TO_ZOOM_STANDARD_VALUE:I = 0x5

.field private static final SIGMA:F = 1.0E-4f

.field private static final STROKE_FRAME_MIN_WIDTH_HEIGHT:I = 0xa

.field private static final TAG:Ljava/lang/String; = "SpenStrokeFrame"

.field private static mIsCameraStart:Z


# instance fields
.field private dm:Landroid/util/DisplayMetrics;

.field private mActivity:Landroid/app/Activity;

.field private mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

.field private mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

.field private mBeautifyStoke:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end field

.field private mBgBitmap:Landroid/graphics/Bitmap;

.field private mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

.field private mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mChangeCameraBgDrawable:Landroid/graphics/drawable/Drawable;

.field private mChangeCameraBgDrawableLL:Landroid/graphics/drawable/Drawable;

.field private mChangeCameraButton:Landroid/widget/ImageButton;

.field private mChangeCameraDrawable:Landroid/graphics/drawable/Drawable;

.field private mChangeCameraFrontDescription:Ljava/lang/String;

.field private mChangeCameraRearDescription:Ljava/lang/String;

.field private mChangeFrameBeautifyDrawable:Landroid/graphics/drawable/Drawable;

.field private mChangeFrameBgDrawable:Landroid/graphics/drawable/Drawable;

.field private mChangeFrameBgDrawableLL:Landroid/graphics/drawable/Drawable;

.field private mChangeFrameButton:Landroid/widget/ImageButton;

.field private mChangeFrameDescriptionBeautify:Ljava/lang/String;

.field private mChangeFrameDescriptionOriginal:Ljava/lang/String;

.field private mChangeFrameOriginalDrawable:Landroid/graphics/drawable/Drawable;

.field private mDeltaX:F

.field private mDeltaY:F

.field private mFrameShapePath:Landroid/graphics/Path;

.field private mIsResourceAvailable:Z

.field private mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

.field private mOffsetButton:I

.field private mOrientation:I

.field private mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

.field private mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

.field private mOriginalStroke:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end field

.field private mPageHeight:I

.field private mPageWidth:I

.field private mPan:Landroid/graphics/PointF;

.field private mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

.field private mPenSize:F

.field private final mPrevTouchPoint:Landroid/graphics/PointF;

.field mPreviewCallback:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;

.field private mPreviewRatio:F

.field private mRatio:F

.field private mReadyCameraPreview:Z

.field private mRect:Landroid/graphics/RectF;

.field private mShapeMaskBitmap:Landroid/graphics/Bitmap;

.field private mShapeMaskLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mShapeMaskView:Landroid/widget/ImageView;

.field private mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

.field private mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

.field private mStartFramePosition:Landroid/graphics/PointF;

.field private mStrokeFrameAction:I

.field private mStrokeFrameType:I

.field private final mTouchCheckPath:Landroid/graphics/Path;

.field private mTouchImage:Landroid/widget/ImageView;

.field private mViewGroup:Landroid/view/ViewGroup;

.field private mWorkBitmap:Landroid/graphics/Bitmap;

.field private mZoomCur:I

.field private mZoomLast:I

.field private mZoomMode:I

.field private mZoomPrev:I

.field private mZoomY:F

.field private updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x5

    const/4 v7, 0x1

    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 72
    sput v5, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    .line 73
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    .line 74
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    .line 75
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I

    .line 76
    const/16 v3, 0xbb8

    sput v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_1080_1920:I

    .line 77
    const/16 v3, 0x1040

    sput v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_2560_1600:I

    .line 78
    const/16 v3, 0x7d0

    sput v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_720_1280:I

    .line 79
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_1080_1920:I

    sput v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_DEFAULT:I

    .line 81
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    .line 83
    new-instance v2, Landroid/graphics/Rect;

    const/16 v3, 0x27

    const/16 v4, 0x27

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 84
    .local v2, "size":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v5, v5, v7, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 85
    .local v1, "margin":Landroid/graphics/Rect;
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 86
    .local v0, "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    const/4 v3, 0x4

    invoke-direct {v1, v5, v5, v3, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 89
    .restart local v1    # "margin":Landroid/graphics/Rect;
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    invoke-direct {v1, v5, v9, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 91
    .restart local v1    # "margin":Landroid/graphics/Rect;
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_1080_1920:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    new-instance v2, Landroid/graphics/Rect;

    .end local v2    # "size":Landroid/graphics/Rect;
    const/16 v3, 0x3a

    const/16 v4, 0x3b

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 95
    .restart local v2    # "size":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    invoke-direct {v1, v5, v5, v7, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 96
    .restart local v1    # "margin":Landroid/graphics/Rect;
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 97
    .restart local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    invoke-direct {v1, v5, v5, v8, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 100
    .restart local v1    # "margin":Landroid/graphics/Rect;
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    invoke-direct {v1, v5, v6, v5, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 102
    .restart local v1    # "margin":Landroid/graphics/Rect;
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_2560_1600:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    new-instance v2, Landroid/graphics/Rect;

    .end local v2    # "size":Landroid/graphics/Rect;
    const/16 v3, 0x23

    const/16 v4, 0x23

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 106
    .restart local v2    # "size":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    invoke-direct {v1, v5, v5, v7, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 107
    .restart local v1    # "margin":Landroid/graphics/Rect;
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 108
    .restart local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    const/4 v3, 0x4

    invoke-direct {v1, v5, v5, v3, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 111
    .restart local v1    # "margin":Landroid/graphics/Rect;
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    invoke-direct {v1, v5, v9, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 113
    .restart local v1    # "margin":Landroid/graphics/Rect;
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_720_1280:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_CAMERA_IMAGE_UX_TABLE:Ljava/util/HashMap;

    .line 119
    new-instance v2, Landroid/graphics/Rect;

    .end local v2    # "size":Landroid/graphics/Rect;
    const/16 v3, 0x27

    const/16 v4, 0x27

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 120
    .restart local v2    # "size":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_1080_1920:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 121
    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x6

    .line 120
    invoke-direct {v1, v5, v5, v3, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 122
    .restart local v1    # "margin":Landroid/graphics/Rect;
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 123
    .restart local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_1080_1920:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 126
    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/lit8 v3, v3, 0x5

    add-int/lit8 v3, v3, -0x6

    .line 125
    invoke-direct {v1, v5, v5, v3, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 127
    .restart local v1    # "margin":Landroid/graphics/Rect;
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_1080_1920:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 129
    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v3, v3, -0x6

    .line 128
    invoke-direct {v1, v5, v9, v3, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 130
    .restart local v1    # "margin":Landroid/graphics/Rect;
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_CAMERA_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_1080_1920:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    new-instance v2, Landroid/graphics/Rect;

    .end local v2    # "size":Landroid/graphics/Rect;
    const/16 v3, 0x3a

    const/16 v4, 0x3b

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 134
    .restart local v2    # "size":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_2560_1600:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 135
    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    .line 134
    invoke-direct {v1, v5, v5, v3, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 136
    .restart local v1    # "margin":Landroid/graphics/Rect;
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 137
    .restart local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_2560_1600:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 140
    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/lit8 v3, v3, 0x6

    .line 139
    invoke-direct {v1, v5, v5, v3, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 141
    .restart local v1    # "margin":Landroid/graphics/Rect;
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_2560_1600:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 143
    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    .line 142
    invoke-direct {v1, v5, v6, v3, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 144
    .restart local v1    # "margin":Landroid/graphics/Rect;
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_CAMERA_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_2560_1600:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    new-instance v2, Landroid/graphics/Rect;

    .end local v2    # "size":Landroid/graphics/Rect;
    const/16 v3, 0x23

    const/16 v4, 0x23

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 148
    .restart local v2    # "size":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_720_1280:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 149
    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    .line 148
    invoke-direct {v1, v5, v5, v3, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 150
    .restart local v1    # "margin":Landroid/graphics/Rect;
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 151
    .restart local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_720_1280:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 154
    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/lit8 v3, v3, 0x5

    .line 153
    invoke-direct {v1, v5, v5, v3, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 155
    .restart local v1    # "margin":Landroid/graphics/Rect;
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_720_1280:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 157
    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    .line 156
    invoke-direct {v1, v5, v9, v3, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 158
    .restart local v1    # "margin":Landroid/graphics/Rect;
    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_CAMERA_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_720_1280:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sput-boolean v5, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mIsCameraStart:Z

    return-void
.end method

.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    .line 220
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchCheckPath:Landroid/graphics/Path;

    .line 221
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPrevTouchPoint:Landroid/graphics/PointF;

    .line 228
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F

    .line 229
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F

    .line 231
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOffsetButton:I

    .line 233
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mReadyCameraPreview:Z

    .line 234
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mIsResourceAvailable:Z

    .line 60
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mReadyCameraPreview:Z

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Z)V
    .locals 0

    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mReadyCameraPreview:Z

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    return-object v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    .prologue
    .line 1694
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setChangeFrameButtonResources()V

    return-void
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;

    return-object v0
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1485
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V
    .locals 0

    .prologue
    .line 955
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createShapeMaskBitmap(Landroid/graphics/Path;)V

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 1

    .prologue
    .line 962
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V
    .locals 0

    .prologue
    .line 1496
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 394
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setRect(Landroid/graphics/RectF;)V

    return-void
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    .prologue
    .line 1648
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateChangeCameraButtonPosition()V

    return-void
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    .prologue
    .line 1718
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateChangeFrameButtonPosition()V

    return-void
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V
    .locals 0

    .prologue
    .line 222
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    return-void
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    return v0
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/widget/RelativeLayout$LayoutParams;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    return-void
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    return-object v0
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F
    .locals 1

    .prologue
    .line 229
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    return-object v0
.end method

.method static synthetic access$30(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F

    return v0
.end method

.method static synthetic access$31(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchCheckPath:Landroid/graphics/Path;

    return-object v0
.end method

.method static synthetic access$32(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPrevTouchPoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$33(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V
    .locals 0

    .prologue
    .line 223
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomMode:I

    return-void
.end method

.method static synthetic access$34(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V
    .locals 0

    .prologue
    .line 227
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomY:F

    return-void
.end method

.method static synthetic access$35(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I

    return v0
.end method

.method static synthetic access$36(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V
    .locals 0

    .prologue
    .line 225
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomLast:I

    return-void
.end method

.method static synthetic access$37(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V
    .locals 0

    .prologue
    .line 226
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomPrev:I

    return-void
.end method

.method static synthetic access$38(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomY:F

    return v0
.end method

.method static synthetic access$39(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomLast:I

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    .prologue
    .line 1618
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setChangeCameraButtonResources()V

    return-void
.end method

.method static synthetic access$40(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V
    .locals 0

    .prologue
    .line 224
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I

    return-void
.end method

.method static synthetic access$41(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    .prologue
    .line 226
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomPrev:I

    return v0
.end method

.method static synthetic access$42(Z)V
    .locals 0

    .prologue
    .line 165
    sput-boolean p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mIsCameraStart:Z

    return-void
.end method

.method static synthetic access$43(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomMode:I

    return v0
.end method

.method static synthetic access$44(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$45(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$46(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$47(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$48(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$49(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    .prologue
    .line 1309
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    return v0
.end method

.method static synthetic access$50(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1394
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createRotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$51(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1425
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createSizeFitableBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$52(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 932
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createStrokeFrameBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$53(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    return-object v0
.end method

.method static synthetic access$54(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    return-object v0
.end method

.method static synthetic access$55(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    return-object v0
.end method

.method static synthetic access$56(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOrientation:I

    return v0
.end method

.method static synthetic access$57(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageWidth:I

    return v0
.end method

.method static synthetic access$58(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageHeight:I

    return v0
.end method

.method static synthetic access$59(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1759
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertAbsolute(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V
    .locals 0

    .prologue
    .line 178
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    return-void
.end method

.method static synthetic access$60(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$61(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Z
    .locals 1

    .prologue
    .line 405
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startCamera()Z

    move-result v0

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 1011
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;

    return-void
.end method

.method private cancel(I)V
    .locals 3
    .param p1, "causeCode"    # I

    .prologue
    .line 1497
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V

    .line 1498
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameAction:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1499
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    or-int/lit8 v1, p1, 0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    .line 1503
    :goto_0
    return-void

    .line 1501
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    or-int/lit8 v1, p1, 0x2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    goto :goto_0
.end method

.method private cleanResource()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1310
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    if-eqz v3, :cond_0

    .line 1311
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1313
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    if-eqz v3, :cond_1

    .line 1315
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;->setResultListener(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1320
    :goto_0
    new-instance v2, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;-><init>(Landroid/content/Context;)V

    .line 1321
    .local v2, "srm":Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->destroyRecognition(Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;)V

    .line 1322
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    .line 1325
    .end local v2    # "srm":Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_2

    .line 1326
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1327
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    .line 1330
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_3

    .line 1331
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1332
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    .line 1335
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    if-eqz v3, :cond_4

    .line 1336
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1337
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    .line 1340
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    if-eqz v3, :cond_5

    .line 1341
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->stop()V

    .line 1342
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1343
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    .line 1346
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    if-eqz v3, :cond_6

    .line 1347
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1348
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    .line 1351
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v3, :cond_7

    .line 1352
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1353
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setReferenceBitmap(Landroid/graphics/Bitmap;)V

    .line 1354
    new-instance v1, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    .line 1355
    .local v1, "penManager":Lcom/samsung/android/sdk/pen/pen/SpenPenManager;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V

    .line 1356
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    .line 1359
    .end local v1    # "penManager":Lcom/samsung/android/sdk/pen/pen/SpenPenManager;
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_8

    .line 1360
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 1361
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    .line 1364
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_9

    .line 1365
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 1366
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    .line 1369
    :cond_9
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_a

    .line 1370
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 1371
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    .line 1374
    :cond_a
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_b

    .line 1375
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 1376
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    .line 1379
    :cond_b
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameOriginalDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_c

    .line 1380
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameOriginalDrawable:Landroid/graphics/drawable/Drawable;

    .line 1383
    :cond_c
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBeautifyDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_d

    .line 1384
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBeautifyDrawable:Landroid/graphics/drawable/Drawable;

    .line 1387
    :cond_d
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBgDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_e

    .line 1388
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBgDrawable:Landroid/graphics/drawable/Drawable;

    .line 1391
    :cond_e
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    .line 1392
    return-void

    .line 1316
    :catch_0
    move-exception v0

    .line 1318
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private convertAbsolute(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 3
    .param p1, "relativeRect"    # Landroid/graphics/RectF;

    .prologue
    .line 1760
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 1762
    .local v0, "dstRect":Landroid/graphics/RectF;
    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1763
    iget v1, p1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1764
    iget v1, p1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1765
    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1767
    return-object v0
.end method

.method private convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 3
    .param p1, "srcRect"    # Landroid/graphics/RectF;

    .prologue
    .line 1486
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 1488
    .local v0, "dstRect":Landroid/graphics/RectF;
    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1489
    iget v1, p1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1490
    iget v1, p1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1491
    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1493
    return-object v0
.end method

.method private createPen()Lcom/samsung/android/sdk/pen/pen/SpenPen;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 1106
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ge v5, v6, :cond_1

    .line 1107
    const-string v5, "SpenStrokeFrame"

    const-string v6, "OriginalStroke Size = 0"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    .line 1151
    :cond_0
    :goto_0
    return-object v1

    .line 1110
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 1112
    .local v3, "s":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    new-instance v2, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v2, v5}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    .line 1113
    .local v2, "penManager":Lcom/samsung/android/sdk/pen/pen/SpenPenManager;
    const/4 v1, 0x0

    .line 1115
    .local v1, "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :try_start_0
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->createPen(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v1

    .line 1134
    invoke-virtual {v1, v7}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1135
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPenSize:F

    .line 1136
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPenSize:F

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V

    .line 1139
    :cond_2
    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1140
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->isCurveEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setCurveEnabled(Z)V

    .line 1143
    :cond_3
    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1144
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getColor()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setColor(I)V

    .line 1147
    :cond_4
    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1148
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getAdvancedPenSetting()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setAdvancedSetting(Ljava/lang/String;)V

    goto :goto_0

    .line 1116
    :catch_0
    move-exception v0

    .line 1118
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    move-object v1, v4

    .line 1119
    goto :goto_0

    .line 1120
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 1122
    .local v0, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    move-object v1, v4

    .line 1123
    goto :goto_0

    .line 1124
    .end local v0    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v0

    .line 1126
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    move-object v1, v4

    .line 1127
    goto :goto_0

    .line 1128
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 1130
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v1, v4

    .line 1131
    goto :goto_0
.end method

.method private createRotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "src"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1395
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 1396
    .local v5, "matrix":Landroid/graphics/Matrix;
    const/4 v0, 0x0

    .line 1397
    .local v0, "rotateBitmap":Landroid/graphics/Bitmap;
    new-instance v13, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v13}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1398
    .local v13, "info":Landroid/hardware/Camera$CameraInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->getCameraType()I

    move-result v1

    invoke-static {v1, v13}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1400
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->getCameraType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 1401
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->getCameraDegree()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {v5, v1, v2, v3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 1402
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    .end local v0    # "rotateBitmap":Landroid/graphics/Bitmap;
    move-result-object v0

    .line 1403
    .restart local v0    # "rotateBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v5}, Landroid/graphics/Matrix;->reset()V

    .line 1405
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    if-le v1, v2, :cond_1

    .line 1406
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {v5, v1, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 1411
    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 1412
    const/4 v6, 0x1

    .line 1411
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1418
    :goto_1
    if-nez v0, :cond_0

    .line 1419
    const-string v1, "SpenStrokeFrame"

    const-string v2, "rotateBitmap is null. out of memory"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1420
    const/4 v0, 0x0

    .line 1422
    .end local v0    # "rotateBitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-object v0

    .line 1408
    .restart local v0    # "rotateBitmap":Landroid/graphics/Bitmap;
    :cond_1
    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v5, v1, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    goto :goto_0

    .line 1414
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->getCameraDegree()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {v5, v1, v2, v3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 1415
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    const/4 v12, 0x1

    move-object v6, p1

    move-object v11, v5

    invoke-static/range {v6 .. v12}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1
.end method

.method private createShapeMaskBitmap(Landroid/graphics/Path;)V
    .locals 4
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    const/4 v3, 0x0

    .line 956
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 957
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 958
    sget-object v1, Landroid/graphics/Region$Op;->XOR:Landroid/graphics/Region$Op;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 959
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 960
    return-void
.end method

.method private createSizeFitableBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "src"    # Landroid/graphics/Bitmap;
    .param p2, "dstRect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v6, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 1426
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 1427
    .local v5, "width":F
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 1429
    .local v1, "height":F
    cmpl-float v7, v5, v10

    if-eqz v7, :cond_0

    cmpl-float v7, v1, v10

    if-nez v7, :cond_1

    .line 1430
    :cond_0
    const-string v7, "SpenStrokeFrame"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "width or height is 0. width = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " height = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v6

    .line 1478
    :goto_0
    return-object v4

    .line 1434
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_5

    .line 1435
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    div-float v8, v1, v5

    cmpl-float v7, v7, v8

    if-lez v7, :cond_4

    .line 1436
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    div-float v8, v5, v1

    mul-float/2addr v7, v8

    mul-float/2addr v7, v1

    float-to-int v7, v7

    int-to-float v1, v7

    .line 1437
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getHeight()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F

    sub-float/2addr v8, v9

    sub-float v2, v7, v8

    .line 1438
    .local v2, "margin":F
    cmpg-float v7, v2, v1

    if-gez v7, :cond_2

    .line 1439
    move v1, v2

    .line 1464
    :cond_2
    :goto_1
    cmpg-float v7, v5, v10

    if-lez v7, :cond_3

    cmpg-float v7, v1, v10

    if-gtz v7, :cond_7

    .line 1465
    :cond_3
    const-string v7, "SpenStrokeFrame"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "width or height is 0. width = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " height = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v6

    .line 1466
    goto :goto_0

    .line 1442
    .end local v2    # "margin":F
    :cond_4
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    div-float v8, v5, v1

    mul-float/2addr v7, v8

    div-float v7, v5, v7

    float-to-int v7, v7

    int-to-float v5, v7

    .line 1443
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getWidth()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F

    sub-float/2addr v8, v9

    sub-float v2, v7, v8

    .line 1444
    .restart local v2    # "margin":F
    cmpg-float v7, v2, v5

    if-gez v7, :cond_2

    .line 1445
    move v5, v2

    .line 1448
    goto :goto_1

    .line 1449
    .end local v2    # "margin":F
    :cond_5
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    div-float v8, v5, v1

    cmpl-float v7, v7, v8

    if-lez v7, :cond_6

    .line 1450
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    div-float v8, v1, v5

    mul-float/2addr v7, v8

    mul-float/2addr v7, v5

    float-to-int v7, v7

    int-to-float v5, v7

    .line 1451
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getWidth()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F

    sub-float/2addr v8, v9

    sub-float v2, v7, v8

    .line 1452
    .restart local v2    # "margin":F
    cmpg-float v7, v2, v5

    if-gez v7, :cond_2

    .line 1453
    move v5, v2

    .line 1455
    goto :goto_1

    .line 1456
    .end local v2    # "margin":F
    :cond_6
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    div-float v8, v1, v5

    mul-float/2addr v7, v8

    div-float v7, v1, v7

    float-to-int v7, v7

    int-to-float v1, v7

    .line 1457
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getHeight()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F

    sub-float/2addr v8, v9

    sub-float v2, v7, v8

    .line 1458
    .restart local v2    # "margin":F
    cmpg-float v7, v2, v1

    if-gez v7, :cond_2

    .line 1459
    move v1, v2

    goto/16 :goto_1

    .line 1469
    :cond_7
    float-to-int v7, v5

    float-to-int v8, v1

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1470
    .local v4, "resultBitmap":Landroid/graphics/Bitmap;
    if-nez v4, :cond_8

    .line 1471
    const-string v7, "SpenStrokeFrame"

    const-string v8, "resultBitmap is null. out of memory"

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v6

    .line 1472
    goto/16 :goto_0

    .line 1474
    :cond_8
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1475
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v3, Landroid/graphics/Rect;

    float-to-int v7, v5

    float-to-int v8, v1

    invoke-direct {v3, v11, v11, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1476
    .local v3, "r":Landroid/graphics/Rect;
    new-instance v7, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-direct {v7, v11, v11, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, p1, v7, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method private createStrokeFrameBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "src"    # Landroid/graphics/Bitmap;
    .param p2, "relativePath"    # Landroid/graphics/Path;

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 933
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 934
    :cond_0
    const-string v4, "SpenStrokeFrame"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "createStrokeFrameBitmap src or rect  is nullsrc "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " relativePath "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    .line 952
    :goto_0
    return-object v2

    .line 938
    :cond_1
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 939
    .local v1, "r":Landroid/graphics/RectF;
    const/4 v4, 0x0

    invoke-virtual {p2, v1, v4}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 941
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-int v5, v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 942
    .local v2, "resultBitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_2

    .line 943
    const-string v4, "SpenStrokeFrame"

    const-string v5, "resultBitmap createBitmap is failed. out fof memory"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    .line 944
    goto :goto_0

    .line 947
    :cond_2
    iget v4, v1, Landroid/graphics/RectF;->left:F

    neg-float v4, v4

    iget v5, v1, Landroid/graphics/RectF;->top:F

    neg-float v5, v5

    invoke-virtual {p2, v4, v5}, Landroid/graphics/Path;->offset(FF)V

    .line 948
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 949
    .local v0, "backCanvas":Landroid/graphics/Canvas;
    sget-object v4, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    invoke-virtual {v0, p2, v4}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 950
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F

    sub-float v4, v7, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F

    sub-float v5, v7, v5

    invoke-virtual {v0, p1, v4, v5, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 30
    .param p1, "dst"    # Landroid/graphics/Bitmap;
    .param p2, "strokeContainer"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .param p3, "absoluteRect"    # Landroid/graphics/RectF;
    .param p4, "relativeRect"    # Landroid/graphics/RectF;

    .prologue
    .line 963
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    .line 964
    const-string v2, "SpenStrokeFrame"

    const-string v4, "workBitmap isn\'t created"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    const/4 v2, 0x0

    .line 1008
    :goto_0
    return v2

    .line 967
    :cond_0
    new-instance v16, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 968
    .local v16, "c":Landroid/graphics/Canvas;
    const/4 v2, 0x0

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 970
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setReferenceBitmap(Landroid/graphics/Bitmap;)V

    .line 971
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 973
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v22

    .line 974
    .local v22, "oList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v29

    :cond_1
    :goto_1
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 996
    new-instance v27, Landroid/graphics/RectF;

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 997
    .local v27, "srcRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPenSize:F

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->increaseRect(Landroid/graphics/RectF;F)V

    .line 999
    new-instance v18, Landroid/graphics/RectF;

    move-object/from16 v0, v18

    move-object/from16 v1, p4

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1000
    .local v18, "dstRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPenSize:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->increaseRect(Landroid/graphics/RectF;F)V

    .line 1002
    new-instance v17, Landroid/graphics/Canvas;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1003
    .local v17, "dstCanvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    new-instance v4, Landroid/graphics/Rect;

    move-object/from16 v0, v27

    iget v5, v0, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    move-object/from16 v0, v27

    iget v6, v0, Landroid/graphics/RectF;->top:F

    float-to-int v6, v6

    move-object/from16 v0, v27

    iget v7, v0, Landroid/graphics/RectF;->right:F

    float-to-int v7, v7

    .line 1004
    move-object/from16 v0, v27

    iget v8, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v8, v8

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v5, Landroid/graphics/Rect;

    move-object/from16 v0, v18

    iget v6, v0, Landroid/graphics/RectF;->left:F

    float-to-int v6, v6

    move-object/from16 v0, v18

    iget v7, v0, Landroid/graphics/RectF;->top:F

    float-to-int v7, v7

    move-object/from16 v0, v18

    iget v8, v0, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    .line 1005
    move-object/from16 v0, v18

    iget v9, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v9, v9

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v6, 0x0

    .line 1003
    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1006
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1007
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setReferenceBitmap(Landroid/graphics/Bitmap;)V

    .line 1008
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 974
    .end local v17    # "dstCanvas":Landroid/graphics/Canvas;
    .end local v18    # "dstRect":Landroid/graphics/RectF;
    .end local v27    # "srcRect":Landroid/graphics/RectF;
    :cond_2
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 975
    .local v21, "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    move-object/from16 v26, v21

    .line 976
    check-cast v26, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 977
    .local v26, "s":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v23

    .line 978
    .local v23, "point":[Landroid/graphics/PointF;
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPressures()[F

    move-result-object v24

    .line 979
    .local v24, "pressures":[F
    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v20, v0

    .line 980
    .local v20, "length":I
    const/4 v2, 0x2

    move/from16 v0, v20

    if-lt v0, v2, :cond_1

    .line 983
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getTimeStamps()[I

    move-result-object v28

    .line 985
    .local v28, "time":[I
    new-instance v25, Landroid/graphics/RectF;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/RectF;-><init>()V

    .line 987
    .local v25, "r":Landroid/graphics/RectF;
    const/4 v2, 0x0

    aget v2, v28, v2

    int-to-long v2, v2

    const/4 v4, 0x0

    aget v4, v28, v4

    int-to-long v4, v4

    const/4 v6, 0x1

    const/4 v7, 0x0

    aget-object v7, v23, v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    const/4 v8, 0x0

    aget-object v8, v23, v8

    iget v8, v8, Landroid/graphics/PointF;->y:F

    .line 988
    const/4 v9, 0x0

    aget v9, v24, v9

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 987
    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v3

    .line 989
    .local v3, "e":Landroid/view/MotionEvent;
    const/16 v19, 0x1

    .local v19, "i":I
    :goto_2
    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_3

    .line 992
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v0, v25

    invoke-virtual {v2, v3, v0}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->redrawPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    goto/16 :goto_1

    .line 990
    :cond_3
    aget v2, v28, v19

    int-to-long v4, v2

    aget-object v2, v23, v19

    iget v6, v2, Landroid/graphics/PointF;->x:F

    aget-object v2, v23, v19

    iget v7, v2, Landroid/graphics/PointF;->y:F

    aget v8, v24, v19

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/view/MotionEvent;->addBatch(JFFFFI)V

    .line 989
    add-int/lit8 v19, v19, 0x1

    goto :goto_2
.end method

.method private getIntValueAppliedDensity(F)I
    .locals 1
    .param p1, "paramFloat"    # F

    .prologue
    .line 1482
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private increaseRect(Landroid/graphics/RectF;F)V
    .locals 1
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "value"    # F

    .prologue
    .line 1771
    iget v0, p1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 1772
    iget v0, p1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 1773
    iget v0, p1, Landroid/graphics/RectF;->right:F

    add-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 1774
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 1775
    return-void
.end method

.method private isMultiwindowMode()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 922
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 923
    .local v0, "display":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 924
    .local v2, "size":Landroid/graphics/Point;
    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 925
    iget v3, v2, Landroid/graphics/Point;->x:I

    .line 926
    .local v3, "width":I
    iget v1, v2, Landroid/graphics/Point;->y:I

    .line 927
    .local v1, "height":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getWidth()I

    move-result v5

    if-ge v5, v3, :cond_1

    .line 929
    :cond_0
    :goto_0
    return v4

    .line 928
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getHeight()I

    move-result v5

    if-lt v5, v1, :cond_0

    .line 929
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private loadChangeFrameButtonResources()Z
    .locals 20

    .prologue
    .line 1506
    const/4 v15, 0x0

    .line 1507
    .local v15, "res":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    .line 1508
    .local v14, "pm":Landroid/content/pm/PackageManager;
    if-nez v14, :cond_0

    .line 1509
    const-string v17, "SpenStrokeFrame"

    const-string v18, "PackageManager is null"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1510
    const/16 v17, 0x0

    .line 1578
    :goto_0
    return v17

    .line 1514
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v15

    .line 1521
    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    .line 1522
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    move-object/from16 v17, v0

    if-nez v17, :cond_1

    .line 1523
    const-string v17, "SpenStrokeFrame"

    const-string v18, "DisplayMetrics Get is failed"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1524
    const/16 v17, 0x0

    goto :goto_0

    .line 1515
    :catch_0
    move-exception v10

    .line 1516
    .local v10, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v17, "SpenStrokeFrame"

    const-string v18, "SpenSDK Resource not found"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1517
    invoke-virtual {v10}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 1518
    const/16 v17, 0x0

    goto :goto_0

    .line 1527
    .end local v10    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    const-string v17, "string_transform_into_auto_shape"

    const-string v18, "string"

    .line 1528
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v19

    .line 1527
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v15, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 1529
    .local v3, "beautifyDescriptionID":I
    const-string v17, "string_transform_back_to_original_shape"

    const-string v18, "string"

    .line 1530
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v19

    .line 1529
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v15, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    .line 1531
    .local v12, "originalDescriptionID":I
    const-string v17, "selector_change_stroke_frame_bg"

    const-string v18, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v15, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 1532
    .local v5, "bgDrawableID":I
    const-string v17, "snote_save_btn_normal"

    const-string v18, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v15, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 1533
    .local v6, "bgDrawableID_LL":I
    const-string v17, "snote_photoframe_refine"

    const-string v18, "drawable"

    .line 1534
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v19

    .line 1533
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v15, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 1535
    .local v4, "beautifyDrawableID":I
    const-string v17, "snote_photoframe_undo"

    const-string v18, "drawable"

    .line 1536
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v19

    .line 1535
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v15, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    .line 1539
    .local v13, "originalDrawableID":I
    const-string v17, "snote_photoframe_toggle"

    const-string v18, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v15, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 1540
    .local v7, "cameraDrawableID":I
    const-string v17, "string_switch_to_front_camera"

    const-string v18, "string"

    .line 1541
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v19

    .line 1540
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v15, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 1542
    .local v8, "cameraFrontDescriptionID":I
    const-string v17, "string_switch_to_rear_camera"

    const-string v18, "string"

    .line 1543
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v19

    .line 1542
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v15, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 1545
    .local v9, "cameraReartDescriptionID":I
    if-eqz v12, :cond_2

    if-eqz v3, :cond_2

    if-eqz v5, :cond_2

    if-eqz v4, :cond_2

    .line 1546
    if-eqz v13, :cond_2

    if-nez v6, :cond_3

    .line 1547
    :cond_2
    const-string v17, "SpenStrokeFrame"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "ChangeFrame Resource not found. Original = "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " Beautify = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1548
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " BgDrawable = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " BeautifyDrawable = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1549
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " OriginalDrawable = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 1547
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1550
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 1553
    :cond_3
    invoke-virtual {v15, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameDescriptionOriginal:Ljava/lang/String;

    .line 1554
    invoke-virtual {v15, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameDescriptionBeautify:Ljava/lang/String;

    .line 1555
    invoke-virtual {v15, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBgDrawable:Landroid/graphics/drawable/Drawable;

    .line 1556
    invoke-virtual {v15, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBgDrawableLL:Landroid/graphics/drawable/Drawable;

    .line 1557
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v18, v0

    add-int v11, v17, v18

    .line 1558
    .local v11, "key":I
    sget-object v17, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_CAMERA_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_4

    .line 1559
    sget v11, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_DEFAULT:I

    .line 1562
    :cond_4
    sget-object v17, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_CAMERA_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/HashMap;

    sget v18, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/graphics/Rect;

    .line 1563
    .local v16, "size":Landroid/graphics/Rect;
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v17

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v15, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameOriginalDrawable:Landroid/graphics/drawable/Drawable;

    .line 1564
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v17

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v15, v4, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBeautifyDrawable:Landroid/graphics/drawable/Drawable;

    .line 1565
    if-eqz v7, :cond_5

    if-eqz v8, :cond_5

    if-eqz v9, :cond_5

    .line 1566
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mIsResourceAvailable:Z

    .line 1567
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v17

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v15, v7, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraDrawable:Landroid/graphics/drawable/Drawable;

    .line 1568
    invoke-virtual {v15, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraFrontDescription:Ljava/lang/String;

    .line 1569
    invoke-virtual {v15, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraRearDescription:Ljava/lang/String;

    .line 1570
    invoke-virtual {v15, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraBgDrawable:Landroid/graphics/drawable/Drawable;

    .line 1571
    invoke-virtual {v15, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraBgDrawableLL:Landroid/graphics/drawable/Drawable;

    .line 1578
    :goto_1
    const/16 v17, 0x1

    goto/16 :goto_0

    .line 1573
    :cond_5
    const-string v17, "SpenStrokeFrame"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "ChangeFrame Resource not found. CameraDrawableID = "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1574
    const-string v19, " cameraFrontDescriptionID = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "cameraReartDescriptionID = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1575
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 1573
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1576
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mIsResourceAvailable:Z

    goto :goto_1
.end method

.method private makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    .locals 24
    .param p1, "container"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .prologue
    .line 1012
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 1013
    .local v16, "strokeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v2

    .line 1014
    .local v2, "containerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v20

    if-lt v5, v0, :cond_1

    .line 1019
    new-instance v13, Landroid/graphics/Path;

    invoke-direct {v13}, Landroid/graphics/Path;-><init>()V

    .line 1020
    .local v13, "path":Landroid/graphics/Path;
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v15

    .line 1021
    .local v15, "size":I
    new-array v0, v15, [I

    move-object/from16 v19, v0

    .line 1022
    .local v19, "used":[I
    const/16 v20, 0x0

    const/16 v21, 0x1

    aput v21, v19, v20

    .line 1023
    const/4 v5, 0x1

    :goto_1
    if-lt v5, v15, :cond_3

    .line 1026
    const/4 v8, 0x0

    .line 1027
    .local v8, "lastX":F
    const/4 v9, 0x0

    .line 1028
    .local v9, "lastY":F
    const/16 v20, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v14

    .line 1029
    .local v14, "pf":[Landroid/graphics/PointF;
    const/16 v20, 0x0

    aget-object v20, v14, v20

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    const/16 v21, 0x0

    aget-object v21, v14, v21

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1030
    const/4 v5, 0x1

    :goto_2
    array-length v0, v14

    move/from16 v20, v0

    move/from16 v0, v20

    if-lt v5, v0, :cond_4

    .line 1035
    const/4 v5, 0x1

    :goto_3
    if-lt v5, v15, :cond_5

    .line 1102
    .end local v13    # "path":Landroid/graphics/Path;
    :cond_0
    :goto_4
    return-object v13

    .line 1015
    .end local v8    # "lastX":F
    .end local v9    # "lastY":F
    .end local v14    # "pf":[Landroid/graphics/PointF;
    .end local v15    # "size":I
    .end local v19    # "used":[I
    :cond_1
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2

    .line 1016
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1014
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1024
    .restart local v13    # "path":Landroid/graphics/Path;
    .restart local v15    # "size":I
    .restart local v19    # "used":[I
    :cond_3
    const/16 v20, 0x0

    aput v20, v19, v5

    .line 1023
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1031
    .restart local v8    # "lastX":F
    .restart local v9    # "lastY":F
    .restart local v14    # "pf":[Landroid/graphics/PointF;
    :cond_4
    aget-object v20, v14, v5

    move-object/from16 v0, v20

    iget v8, v0, Landroid/graphics/PointF;->x:F

    .line 1032
    aget-object v20, v14, v5

    move-object/from16 v0, v20

    iget v9, v0, Landroid/graphics/PointF;->y:F

    .line 1033
    invoke-virtual {v13, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1030
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1036
    :cond_5
    const/4 v12, 0x0

    .line 1037
    .local v12, "nextStroke":I
    const/4 v6, 0x1

    .line 1038
    .local v6, "isStart":Z
    const/high16 v10, 0x447a0000    # 1000.0f

    .line 1039
    .local v10, "minLength":F
    const/4 v7, 0x1

    .local v7, "j":I
    :goto_5
    if-lt v7, v15, :cond_7

    .line 1083
    if-eqz v12, :cond_0

    .line 1086
    const/16 v20, 0x1

    aput v20, v19, v12

    .line 1087
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v11

    .line 1088
    .local v11, "nextPoint":[Landroid/graphics/PointF;
    if-eqz v6, :cond_12

    .line 1089
    const/4 v7, 0x0

    :goto_6
    array-length v0, v11

    move/from16 v20, v0

    move/from16 v0, v20

    if-lt v7, v0, :cond_11

    .line 1035
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 1040
    .end local v11    # "nextPoint":[Landroid/graphics/PointF;
    :cond_7
    aget v20, v19, v7

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_9

    .line 1039
    :cond_8
    :goto_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 1043
    :cond_9
    if-nez v12, :cond_d

    .line 1044
    move v12, v7

    .line 1045
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v11

    .line 1046
    .restart local v11    # "nextPoint":[Landroid/graphics/PointF;
    if-eqz v11, :cond_a

    array-length v0, v11

    move/from16 v20, v0

    if-nez v20, :cond_b

    .line 1047
    :cond_a
    const-string v20, "SpenStrokeFrame"

    const-string v21, "nextPoint is null"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1048
    const/4 v13, 0x0

    goto/16 :goto_4

    .line 1050
    :cond_b
    const/16 v20, 0x0

    aget-object v20, v11, v20

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v17, v0

    .line 1051
    .local v17, "sx":F
    const/16 v20, 0x0

    aget-object v20, v11, v20

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v18, v0

    .line 1052
    .local v18, "sy":F
    array-length v0, v11

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    aget-object v20, v11, v20

    move-object/from16 v0, v20

    iget v3, v0, Landroid/graphics/PointF;->x:F

    .line 1053
    .local v3, "ex":F
    array-length v0, v11

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    aget-object v20, v11, v20

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/PointF;->y:F

    .line 1054
    .local v4, "ey":F
    sub-float v20, v8, v17

    sub-float v21, v8, v17

    mul-float v20, v20, v21

    sub-float v21, v9, v18

    sub-float v22, v9, v18

    mul-float v21, v21, v22

    add-float v20, v20, v21

    sub-float v21, v8, v3

    sub-float v22, v8, v3

    mul-float v21, v21, v22

    .line 1055
    sub-float v22, v9, v4

    sub-float v23, v9, v4

    mul-float v22, v22, v23

    add-float v21, v21, v22

    cmpl-float v20, v20, v21

    if-lez v20, :cond_c

    .line 1056
    sub-float v20, v8, v3

    sub-float v21, v8, v3

    mul-float v20, v20, v21

    sub-float v21, v9, v4

    sub-float v22, v9, v4

    mul-float v21, v21, v22

    add-float v10, v20, v21

    .line 1057
    const/4 v6, 0x0

    .line 1058
    goto :goto_7

    .line 1059
    :cond_c
    sub-float v20, v8, v17

    sub-float v21, v8, v17

    mul-float v20, v20, v21

    sub-float v21, v9, v18

    sub-float v22, v9, v18

    mul-float v21, v21, v22

    add-float v10, v20, v21

    .line 1060
    const/4 v6, 0x1

    .line 1062
    goto/16 :goto_7

    .line 1063
    .end local v3    # "ex":F
    .end local v4    # "ey":F
    .end local v11    # "nextPoint":[Landroid/graphics/PointF;
    .end local v17    # "sx":F
    .end local v18    # "sy":F
    :cond_d
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v11

    .line 1064
    .restart local v11    # "nextPoint":[Landroid/graphics/PointF;
    if-eqz v11, :cond_e

    array-length v0, v11

    move/from16 v20, v0

    if-nez v20, :cond_f

    .line 1065
    :cond_e
    const-string v20, "SpenStrokeFrame"

    const-string v21, "nextPoint is null"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    const/4 v13, 0x0

    goto/16 :goto_4

    .line 1068
    :cond_f
    const/16 v20, 0x0

    aget-object v20, v11, v20

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v17, v0

    .line 1069
    .restart local v17    # "sx":F
    const/16 v20, 0x0

    aget-object v20, v11, v20

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v18, v0

    .line 1070
    .restart local v18    # "sy":F
    array-length v0, v11

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    aget-object v20, v11, v20

    move-object/from16 v0, v20

    iget v3, v0, Landroid/graphics/PointF;->x:F

    .line 1071
    .restart local v3    # "ex":F
    array-length v0, v11

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    aget-object v20, v11, v20

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/PointF;->y:F

    .line 1072
    .restart local v4    # "ey":F
    sub-float v20, v8, v17

    sub-float v21, v8, v17

    mul-float v20, v20, v21

    sub-float v21, v9, v18

    sub-float v22, v9, v18

    mul-float v21, v21, v22

    add-float v20, v20, v21

    cmpl-float v20, v10, v20

    if-lez v20, :cond_10

    .line 1073
    sub-float v20, v8, v17

    sub-float v21, v8, v17

    mul-float v20, v20, v21

    sub-float v21, v9, v18

    sub-float v22, v9, v18

    mul-float v21, v21, v22

    add-float v10, v20, v21

    .line 1074
    move v12, v7

    .line 1075
    const/4 v6, 0x1

    .line 1076
    goto/16 :goto_7

    :cond_10
    sub-float v20, v8, v3

    sub-float v21, v8, v3

    mul-float v20, v20, v21

    sub-float v21, v9, v4

    sub-float v22, v9, v4

    mul-float v21, v21, v22

    add-float v20, v20, v21

    cmpl-float v20, v10, v20

    if-lez v20, :cond_8

    .line 1077
    sub-float v20, v8, v3

    sub-float v21, v8, v3

    mul-float v20, v20, v21

    sub-float v21, v9, v4

    sub-float v22, v9, v4

    mul-float v21, v21, v22

    add-float v10, v20, v21

    .line 1078
    move v12, v7

    .line 1079
    const/4 v6, 0x0

    goto/16 :goto_7

    .line 1090
    .end local v3    # "ex":F
    .end local v4    # "ey":F
    .end local v17    # "sx":F
    .end local v18    # "sy":F
    :cond_11
    aget-object v20, v11, v7

    move-object/from16 v0, v20

    iget v8, v0, Landroid/graphics/PointF;->x:F

    .line 1091
    aget-object v20, v11, v7

    move-object/from16 v0, v20

    iget v9, v0, Landroid/graphics/PointF;->y:F

    .line 1092
    invoke-virtual {v13, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1089
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_6

    .line 1095
    :cond_12
    array-length v0, v11

    move/from16 v20, v0

    add-int/lit8 v7, v20, -0x1

    :goto_8
    if-ltz v7, :cond_6

    .line 1096
    aget-object v20, v11, v7

    move-object/from16 v0, v20

    iget v8, v0, Landroid/graphics/PointF;->x:F

    .line 1097
    aget-object v20, v11, v7

    move-object/from16 v0, v20

    iget v9, v0, Landroid/graphics/PointF;->y:F

    .line 1098
    invoke-virtual {v13, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1095
    add-int/lit8 v7, v7, -0x1

    goto :goto_8
.end method

.method private setChangeCameraButtonResources()V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1619
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    if-nez v1, :cond_0

    .line 1620
    const-string v1, "SpenStrokeFrame"

    const-string v2, "ChangeCameraButton isn\'t created yet"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1646
    :goto_0
    return-void

    .line 1624
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_1

    .line 1625
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraBgDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1634
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1636
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    if-nez v1, :cond_3

    .line 1637
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraFrontDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1626
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-le v1, v2, :cond_2

    .line 1627
    new-instance v0, Landroid/content/res/ColorStateList;

    .line 1628
    new-array v1, v5, [[I

    new-array v2, v5, [I

    const v3, 0x10100a7

    aput v3, v2, v4

    aput-object v2, v1, v4

    new-array v2, v5, [I

    const/16 v3, 0x1e

    invoke-static {v3, v4, v4, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    aput v3, v2, v4

    .line 1627
    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 1629
    .local v0, "colorStateList":Landroid/content/res/ColorStateList;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraBgDrawableLL:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    invoke-direct {v2, v0, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1631
    .end local v0    # "colorStateList":Landroid/content/res/ColorStateList;
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraBgDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1639
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->getCameraType()I

    move-result v1

    if-ne v1, v5, :cond_4

    .line 1640
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraRearDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1642
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraFrontDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setChangeFrameButtonResources()V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1695
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    if-nez v1, :cond_0

    .line 1696
    const-string v1, "SpenStrokeFrame"

    const-string v2, "ChangeFrameButton isn\'t created yet"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1716
    :goto_0
    return-void

    .line 1700
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_1

    .line 1701
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBgDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1709
    :goto_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    if-nez v1, :cond_3

    .line 1710
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBeautifyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1711
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameDescriptionBeautify:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1702
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-le v1, v2, :cond_2

    .line 1703
    new-instance v0, Landroid/content/res/ColorStateList;

    .line 1704
    new-array v1, v5, [[I

    new-array v2, v5, [I

    const v3, 0x10100a7

    aput v3, v2, v4

    aput-object v2, v1, v4

    new-array v2, v5, [I

    const/16 v3, 0x1e

    invoke-static {v3, v4, v4, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    aput v3, v2, v4

    .line 1703
    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 1705
    .local v0, "colorStateList":Landroid/content/res/ColorStateList;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBgDrawableLL:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    invoke-direct {v2, v0, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1707
    .end local v0    # "colorStateList":Landroid/content/res/ColorStateList;
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBgDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1713
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameOriginalDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1714
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameDescriptionOriginal:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setRect(Landroid/graphics/RectF;)V
    .locals 4
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v3, 0x0

    .line 395
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    .line 397
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 398
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 400
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 403
    :cond_0
    return-void
.end method

.method private startCamera()Z
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v4, 0x1

    const/high16 v10, 0x40800000    # 4.0f

    const/high16 v9, 0x40400000    # 3.0f

    const/4 v3, 0x0

    .line 406
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->loadChangeFrameButtonResources()Z

    move-result v5

    if-nez v5, :cond_0

    .line 407
    const-string v4, "SpenStrokeFrame"

    const-string v5, "ChangeFrame Resource Loading is failed"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    :goto_0
    return v3

    .line 410
    :cond_0
    sput-boolean v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mIsCameraStart:Z

    .line 411
    new-instance v5, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v5, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    .line 412
    new-instance v5, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    const-string v8, "CAMERA_TYPE"

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getExtraDataInt(Ljava/lang/String;)I

    move-result v7

    invoke-direct {v5, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;-><init>(Landroid/app/Activity;I)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    .line 414
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->isFrontCamera()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mIsResourceAvailable:Z

    if-eqz v5, :cond_1

    .line 415
    new-instance v5, Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v5, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    .line 416
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setChangeCameraButtonResources()V

    .line 417
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateChangeCameraButtonPosition()V

    .line 418
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    new-instance v6, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;

    invoke-direct {v6, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 438
    :cond_1
    new-instance v5, Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v5, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    .line 439
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setChangeFrameButtonResources()V

    .line 440
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateChangeFrameButtonPosition()V

    .line 441
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    new-instance v6, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;

    invoke-direct {v6, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 483
    new-instance v5, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v5, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    .line 484
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getHeight()I

    move-result v7

    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 485
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 486
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 487
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    new-instance v6, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;

    invoke-direct {v6, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 495
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    if-nez v5, :cond_3

    .line 496
    :cond_2
    const-string v4, "SpenStrokeFrame"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "CameraView or TouchImage is null. CameraView = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "TouchImage = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 500
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->requestFocusFromTouch()Z

    .line 501
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 502
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    float-to-int v6, v6

    invoke-virtual {v0, v5, v6, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 503
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 505
    iput v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F

    .line 506
    iput v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F

    .line 507
    const/4 v5, 0x2

    new-array v2, v5, [I

    .line 508
    .local v2, "screenLocation":[I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v5, v2}, Landroid/view/ViewGroup;->getLocationOnScreen([I)V

    .line 509
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v4, :cond_7

    .line 510
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v9

    div-float/2addr v6, v10

    cmpl-float v5, v5, v6

    if-lez v5, :cond_5

    .line 511
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v10

    div-float/2addr v6, v9

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getHeight()I

    move-result v6

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_b

    .line 512
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v10

    div-float/2addr v6, v9

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getHeight()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F

    .line 513
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 514
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 516
    aget v5, v2, v4

    if-gtz v5, :cond_4

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->isMultiwindowMode()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 517
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    if-gez v5, :cond_4

    .line 519
    const-string v4, "SpenStrokeFrame"

    const-string v5, "Camera view is out of parent\'s view"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 524
    :cond_4
    aget v5, v2, v4

    if-lez v5, :cond_b

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->isMultiwindowMode()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 525
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v5, :cond_b

    .line 527
    const-string v4, "SpenStrokeFrame"

    const-string v5, "Camera view is out of parent\'s view"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 533
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v9

    div-float/2addr v6, v10

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getWidth()I

    move-result v6

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_b

    .line 534
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v9

    div-float/2addr v6, v10

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F

    .line 535
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 536
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 538
    aget v5, v2, v3

    if-gtz v5, :cond_6

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->isMultiwindowMode()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 539
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    if-gez v5, :cond_6

    .line 541
    const-string v4, "SpenStrokeFrame"

    const-string v5, "Camera view is out of parent\'s view"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 546
    :cond_6
    aget v5, v2, v3

    if-lez v5, :cond_b

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->isMultiwindowMode()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 547
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v5, :cond_b

    .line 549
    const-string v4, "SpenStrokeFrame"

    const-string v5, "Camera view is out of parent\'s view"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 556
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v9

    div-float/2addr v6, v10

    cmpl-float v5, v5, v6

    if-lez v5, :cond_9

    .line 557
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v10

    div-float/2addr v6, v9

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getWidth()I

    move-result v6

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_b

    .line 558
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v10

    div-float/2addr v6, v9

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F

    .line 559
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 560
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 562
    aget v5, v2, v3

    if-gtz v5, :cond_8

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->isMultiwindowMode()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 563
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    if-gez v5, :cond_8

    .line 565
    const-string v4, "SpenStrokeFrame"

    const-string v5, "Camera view is out of parent\'s view"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 570
    :cond_8
    aget v5, v2, v3

    if-lez v5, :cond_b

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->isMultiwindowMode()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 571
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v5, :cond_b

    .line 573
    const-string v4, "SpenStrokeFrame"

    const-string v5, "Camera view is out of parent\'s view"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 579
    :cond_9
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v9

    div-float/2addr v6, v10

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getHeight()I

    move-result v6

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_b

    .line 580
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v9

    div-float/2addr v6, v10

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getHeight()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F

    .line 581
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 582
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 584
    aget v5, v2, v4

    if-gtz v5, :cond_a

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->isMultiwindowMode()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 585
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    if-gez v5, :cond_a

    .line 587
    const-string v4, "SpenStrokeFrame"

    const-string v5, "Camera view is out of parent\'s view"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 592
    :cond_a
    aget v5, v2, v4

    if-lez v5, :cond_b

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->isMultiwindowMode()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 593
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v5, :cond_b

    .line 595
    const-string v4, "SpenStrokeFrame"

    const-string v5, "Camera view is out of parent\'s view"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 603
    :cond_b
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setZOrderMediaOverlay(Z)V

    .line 604
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewCallback:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;

    .line 666
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewCallback:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setOnPreviewCallback(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;)V

    .line 667
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 669
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    new-instance v5, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;

    invoke-direct {v5, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 806
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    .line 909
    .local v1, "listener":Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setOnCompleteCameraFrameListener(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;)V

    .line 910
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 911
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 912
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 913
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 914
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_c

    .line 915
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_c
    move v3, v4

    .line 918
    goto/16 :goto_0
.end method

.method private startRecognition()Z
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1155
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;-><init>(Landroid/content/Context;)V

    .line 1156
    .local v4, "srm":Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;
    const/4 v5, 0x4

    invoke-virtual {v4, v6, v5}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->getInfoList(II)Ljava/util/List;

    move-result-object v2

    .line 1158
    .local v2, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;>;"
    const/4 v3, 0x0

    .line 1159
    .local v3, "isNRRShapeRecognition":Z
    :try_start_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_2

    .line 1166
    :goto_0
    if-nez v3, :cond_1

    .line 1167
    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->createRecognition(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;)Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    .line 1170
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    new-instance v8, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;

    invoke-direct {v8, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;->setResultListener(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;)V

    .line 1291
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;->request(Ljava/util/List;)V

    move v5, v6

    .line 1306
    :goto_1
    return v5

    .line 1159
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;

    .line 1160
    .local v1, "info":Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    iget-object v8, v1, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->name:Ljava/lang/String;

    const-string v9, "NRRShape"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1161
    invoke-virtual {v4, v1}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->createRecognition(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;)Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;
    :try_end_0
    .catch Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1162
    const/4 v3, 0x1

    .line 1163
    goto :goto_0

    .line 1292
    .end local v1    # "info":Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    :catch_0
    move-exception v0

    .line 1294
    .local v0, "e":Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;->printStackTrace()V

    move v5, v7

    .line 1295
    goto :goto_1

    .line 1296
    .end local v0    # "e":Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;
    :catch_1
    move-exception v0

    .line 1298
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    move v5, v7

    .line 1299
    goto :goto_1

    .line 1300
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_2
    move-exception v0

    .line 1302
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v5, v7

    .line 1303
    goto :goto_1
.end method

.method private updateChangeCameraButtonPosition()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 1649
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    if-nez v8, :cond_0

    .line 1650
    const-string v8, "SpenStrokeFrame"

    const-string v9, "ChangeFrameButton isn\'t created yet"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1691
    :goto_0
    return-void

    .line 1654
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    .line 1655
    .local v1, "h":I
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 1657
    .local v5, "r":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    iget v8, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    iget v9, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    add-int v2, v8, v9

    .line 1658
    .local v2, "key":I
    sget-object v8, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_CAMERA_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1659
    sget v2, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_DEFAULT:I

    .line 1662
    :cond_1
    sget-object v8, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_CAMERA_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    sget v9, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/Rect;

    .line 1663
    .local v7, "topMargin":Landroid/graphics/Rect;
    sget-object v8, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_CAMERA_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    sget v9, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 1664
    .local v0, "bottomMargin":Landroid/graphics/Rect;
    sget-object v8, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_CAMERA_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    sget v9, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Rect;

    .line 1666
    .local v6, "size":Landroid/graphics/Rect;
    int-to-float v8, v1

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v8, v9

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    mul-int/lit8 v9, v9, 0x2

    iget v10, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_2

    .line 1667
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget v10, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    sub-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->left:I

    .line 1668
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    float-to-int v8, v8

    iget v9, v0, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->top:I

    .line 1669
    iget v8, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->right:I

    .line 1670
    iget v8, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->bottom:I

    .line 1683
    :goto_1
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-direct {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1684
    .local v3, "layout":Landroid/widget/RelativeLayout$LayoutParams;
    iget v8, v5, Landroid/graphics/Rect;->left:I

    if-gez v8, :cond_4

    .line 1685
    iget v8, v5, Landroid/graphics/Rect;->left:I

    rsub-int/lit8 v8, v8, 0x0

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOffsetButton:I

    .line 1689
    :goto_2
    iget v8, v5, Landroid/graphics/Rect;->left:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOffsetButton:I

    add-int/2addr v8, v9

    iget v9, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3, v8, v9, v11, v11}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1690
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;

    invoke-virtual {v8, v3}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 1671
    .end local v3    # "layout":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    iget v10, v7, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_3

    .line 1672
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget v10, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    sub-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->left:I

    .line 1673
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    float-to-int v8, v8

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    iget v10, v7, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    sub-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->top:I

    .line 1674
    iget v8, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->right:I

    .line 1675
    iget v8, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_1

    .line 1677
    :cond_3
    sget-object v8, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_CAMERA_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    sget v9, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    .line 1678
    .local v4, "margin":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget v10, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    sub-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->left:I

    .line 1679
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getHeight()I

    move-result v8

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    iget v10, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    sub-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->top:I

    .line 1680
    iget v8, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->right:I

    .line 1681
    iget v8, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_1

    .line 1687
    .end local v4    # "margin":Landroid/graphics/Rect;
    .restart local v3    # "layout":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    iput v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOffsetButton:I

    goto/16 :goto_2
.end method

.method private updateChangeFrameButtonPosition()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 1719
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    if-nez v8, :cond_0

    .line 1720
    const-string v8, "SpenStrokeFrame"

    const-string v9, "ChangeFrameButton isn\'t created yet"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1757
    :goto_0
    return-void

    .line 1724
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    .line 1725
    .local v1, "h":I
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 1727
    .local v5, "r":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    iget v8, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    iget v9, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    add-int v2, v8, v9

    .line 1728
    .local v2, "key":I
    sget-object v8, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1729
    sget v2, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_DEFAULT:I

    .line 1732
    :cond_1
    sget-object v8, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    sget v9, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/Rect;

    .line 1733
    .local v7, "topMargin":Landroid/graphics/Rect;
    sget-object v8, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    sget v9, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 1734
    .local v0, "bottomMargin":Landroid/graphics/Rect;
    sget-object v8, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    sget v9, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Rect;

    .line 1736
    .local v6, "size":Landroid/graphics/Rect;
    int-to-float v8, v1

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v8, v9

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    mul-int/lit8 v9, v9, 0x2

    iget v10, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_2

    .line 1737
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget v10, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    sub-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->left:I

    .line 1738
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    float-to-int v8, v8

    iget v9, v0, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->top:I

    .line 1739
    iget v8, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->right:I

    .line 1740
    iget v8, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->bottom:I

    .line 1753
    :goto_1
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-direct {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1754
    .local v3, "layout":Landroid/widget/RelativeLayout$LayoutParams;
    iget v8, v5, Landroid/graphics/Rect;->left:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOffsetButton:I

    add-int/2addr v8, v9

    iget v9, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3, v8, v9, v11, v11}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1755
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    invoke-virtual {v8, v3}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1756
    iput v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOffsetButton:I

    goto/16 :goto_0

    .line 1741
    .end local v3    # "layout":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    iget v10, v7, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_3

    .line 1742
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget v10, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    sub-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->left:I

    .line 1743
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    float-to-int v8, v8

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    iget v10, v7, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    sub-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->top:I

    .line 1744
    iget v8, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->right:I

    .line 1745
    iget v8, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 1747
    :cond_3
    sget-object v8, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    sget v9, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    .line 1748
    .local v4, "margin":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget v10, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    sub-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->left:I

    .line 1749
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getHeight()I

    move-result v8

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    iget v10, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    sub-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->top:I

    .line 1750
    iget v8, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->right:I

    .line 1751
    iget v8, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_1
.end method


# virtual methods
.method public cancelStrokeFrame()Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .locals 1

    .prologue
    .line 389
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mIsCameraStart:Z

    .line 390
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V

    .line 391
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    return-object v0
.end method

.method resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;
    .locals 14
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I
    .param p3, "iconWidth"    # I
    .param p4, "iconHeight"    # I

    .prologue
    .line 1584
    invoke-virtual/range {p1 .. p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v13

    .line 1585
    .local v13, "stream":Ljava/io/InputStream;
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1588
    .local v1, "BitmapOrg":Landroid/graphics/Bitmap;
    if-nez v1, :cond_0

    .line 1589
    const/4 v2, 0x0

    .line 1613
    :goto_0
    return-object v2

    .line 1591
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 1592
    .local v4, "width":I
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 1593
    .local v5, "height":I
    move/from16 v0, p3

    int-to-float v2, v0

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v9

    .line 1594
    .local v9, "newWidth":I
    move/from16 v0, p4

    int-to-float v2, v0

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1597
    .local v8, "newHeight":I
    int-to-float v2, v9

    int-to-float v3, v4

    div-float v12, v2, v3

    .line 1598
    .local v12, "scaleWidth":F
    int-to-float v2, v8

    int-to-float v3, v5

    div-float v11, v2, v3

    .line 1601
    .local v11, "scaleHeight":F
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 1603
    .local v6, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v6, v12, v11}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1609
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x1

    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 1613
    .local v10, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, p1, v10}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public retakeStrokeFrame(Landroid/app/Activity;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIILcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V
    .locals 9
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "spenSurfaceViewBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "bgBitmap"    # Landroid/graphics/Bitmap;
    .param p4, "pageWidth"    # I
    .param p5, "pageHeight"    # I
    .param p6, "frameType"    # I
    .param p7, "objectContainer"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .param p8, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    .param p9, "pan"    # Landroid/graphics/PointF;
    .param p10, "zoomRatio"    # F
    .param p11, "frameStartPosition"    # Landroid/graphics/PointF;
    .param p12, "layout"    # Landroid/view/ViewGroup;

    .prologue
    .line 289
    sget-boolean v6, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mIsCameraStart:Z

    if-eqz v6, :cond_1

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 292
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    .line 293
    iput p6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    .line 294
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 295
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    .line 296
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    .line 297
    move/from16 v0, p10

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    .line 298
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    .line 299
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    .line 300
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageWidth:I

    .line 301
    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageHeight:I

    .line 302
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 303
    const/4 v6, 0x2

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameAction:I

    .line 304
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v6

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOrientation:I

    .line 305
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    const/16 v7, 0xe

    invoke-virtual {v6, v7}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 307
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageWidth:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageHeight:I

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    .line 308
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    if-nez v6, :cond_2

    .line 309
    const-string v6, "SpenStrokeFrame"

    const-string v7, "workBitmap create is failed"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    const/16 v6, 0x20

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    goto :goto_0

    .line 314
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 315
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 314
    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    .line 316
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    if-nez v6, :cond_3

    .line 317
    const-string v6, "SpenStrokeFrame"

    const-string v7, "ShapeMaskBitmap create is failed"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    const/16 v6, 0x20

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    goto :goto_0

    .line 322
    :cond_3
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    .line 323
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 324
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 323
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 325
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_6

    .line 333
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyStoke:Ljava/util/ArrayList;

    .line 334
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 335
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 334
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 336
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_8

    .line 344
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createPen()Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    .line 345
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-nez v6, :cond_a

    .line 346
    const-string v6, "SpenStrokeFrame"

    const-string v7, "Pen Create is failed"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    const/16 v6, 0x20

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    goto/16 :goto_0

    .line 325
    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 326
    .local v2, "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_7

    .line 327
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    .end local v2    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    goto :goto_1

    .line 328
    .restart local v2    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_7
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_4

    .line 329
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 336
    .end local v2    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_8
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 337
    .restart local v2    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_9

    .line 338
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    .end local v2    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    goto :goto_2

    .line 339
    .restart local v2    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_9
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_5

    .line 340
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyStoke:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 351
    .end local v2    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_a
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    if-nez v6, :cond_b

    .line 352
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;

    .line 357
    :goto_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 358
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 359
    .local v3, "relativePath":Landroid/graphics/Path;
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v4

    .line 360
    .local v4, "relativeRect":Landroid/graphics/RectF;
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 361
    .local v1, "m":Landroid/graphics/Matrix;
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    sget-object v7, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v6, v4, v7}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 362
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;

    invoke-virtual {v6, v1, v3}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 363
    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createShapeMaskBitmap(Landroid/graphics/Path;)V

    .line 365
    const/4 v5, 0x0

    .line 366
    .local v5, "resultValue":Z
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    if-nez v6, :cond_c

    .line 367
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-direct {p0, v6, v7, v8, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v5

    .line 372
    :goto_4
    if-nez v5, :cond_d

    .line 373
    const-string v6, "SpenStrokeFrame"

    const-string v7, "drawPen is failed"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    const/16 v6, 0x20

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    goto/16 :goto_0

    .line 354
    .end local v1    # "m":Landroid/graphics/Matrix;
    .end local v3    # "relativePath":Landroid/graphics/Path;
    .end local v4    # "relativeRect":Landroid/graphics/RectF;
    .end local v5    # "resultValue":Z
    :cond_b
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;

    goto :goto_3

    .line 369
    .restart local v1    # "m":Landroid/graphics/Matrix;
    .restart local v3    # "relativePath":Landroid/graphics/Path;
    .restart local v4    # "relativeRect":Landroid/graphics/RectF;
    .restart local v5    # "resultValue":Z
    :cond_c
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-direct {p0, v6, v7, v8, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v5

    goto :goto_4

    .line 378
    :cond_d
    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setRect(Landroid/graphics/RectF;)V

    .line 380
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startCamera()Z

    move-result v6

    if-nez v6, :cond_0

    .line 381
    const-string v6, "SpenStrokeFrame"

    const-string v7, "startCamera is failed"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    const/16 v6, 0x20

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    goto/16 :goto_0
.end method

.method public takeStrokeFrame(Landroid/app/Activity;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIILcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "spenSurfaceViewBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "bgBitmap"    # Landroid/graphics/Bitmap;
    .param p4, "pageWidth"    # I
    .param p5, "pageHeight"    # I
    .param p6, "frameType"    # I
    .param p7, "objectContainer"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .param p8, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    .param p9, "pan"    # Landroid/graphics/PointF;
    .param p10, "zoomRatio"    # F
    .param p11, "frameStartPosition"    # Landroid/graphics/PointF;
    .param p12, "layout"    # Landroid/view/ViewGroup;

    .prologue
    .line 242
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    .line 243
    iput p6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    .line 244
    iput-object p7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 245
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    .line 246
    iput-object p9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    .line 247
    iput p10, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    .line 248
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    .line 249
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    .line 250
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    .line 251
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageWidth:I

    .line 252
    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageHeight:I

    .line 253
    iput-object p8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 254
    const/4 v2, 0x1

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameAction:I

    .line 255
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOrientation:I

    .line 256
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    const/16 v3, 0xe

    invoke-virtual {v2, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 258
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    .line 259
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 260
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 259
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 261
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 268
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 269
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 268
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 270
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    .line 272
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createPen()Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    .line 273
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-nez v2, :cond_4

    .line 274
    const-string v2, "SpenStrokeFrame"

    const-string v3, "Pen Create is failed"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    const/16 v2, 0x20

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    .line 284
    :cond_1
    :goto_1
    return-void

    .line 261
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 262
    .local v1, "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    .line 263
    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    .end local v1    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    goto :goto_0

    .line 264
    .restart local v1    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_3
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 265
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 279
    .end local v1    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startRecognition()Z

    move-result v2

    if-nez v2, :cond_1

    .line 280
    const-string v2, "SpenStrokeFrame"

    const-string v3, "Recognition is failed"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    const/16 v2, 0x8

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    goto :goto_1
.end method
