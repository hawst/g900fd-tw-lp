.class public Lcom/samsung/android/sdk/pen/pen/SpenPen;
.super Ljava/lang/Object;
.source "SpenPen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/pen/SpenPen$ChangedListener;
    }
.end annotation


# static fields
.field public static final PEN_ATTRIBUTE_ADVANCED_SETTING:I = 0x4

.field public static final PEN_ATTRIBUTE_ALPHA:I = 0x1

.field public static final PEN_ATTRIBUTE_COLOR:I = 0x2

.field public static final PEN_ATTRIBUTE_CURVE:I = 0x3

.field public static final PEN_ATTRIBUTE_SIZE:I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mListener:Lcom/samsung/android/sdk/pen/pen/SpenPen$ChangedListener;

.field private mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "penObject"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    .prologue
    const/4 v0, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    .line 54
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mContext:Landroid/content/Context;

    .line 55
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mListener:Lcom/samsung/android/sdk/pen/pen/SpenPen$ChangedListener;

    .line 81
    if-nez p1, :cond_0

    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_ARG : parameter \'context\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    if-nez p2, :cond_1

    .line 85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_ARG : parameter \'penObject\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mContext:Landroid/content/Context;

    .line 88
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    .line 89
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/pen/SpenPen;)Lcom/samsung/android/sdk/pen/pen/SpenPen$ChangedListener;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mListener:Lcom/samsung/android/sdk/pen/pen/SpenPen$ChangedListener;

    return-object v0
.end method


# virtual methods
.method close()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 96
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mContext:Landroid/content/Context;

    .line 97
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    .line 98
    return-void
.end method

.method public draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 121
    return-void
.end method

.method public getAdvancedSetting()Ljava/lang/String;
    .locals 2

    .prologue
    .line 353
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 354
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 174
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getColor()I
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 282
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getColor()I

    move-result v0

    return v0
.end method

.method public getMaxSettingValue()F
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 252
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMaxSettingValue()F

    move-result v0

    return v0
.end method

.method public getMinSettingValue()F
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 238
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMinSettingValue()F

    move-result v0

    return v0
.end method

.method public getPenAttribute(I)Z
    .locals 2
    .param p1, "attribute"    # I

    .prologue
    .line 433
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 434
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v0

    return v0
.end method

.method getPenObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    return-object v0
.end method

.method public getSize()F
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 224
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getSize()F

    move-result v0

    return v0
.end method

.method public getStrokeRect(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)Landroid/graphics/RectF;
    .locals 7
    .param p1, "stroke"    # Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .prologue
    .line 454
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 455
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPressures()[F

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getTimeStamps()[I

    move-result-object v3

    .line 460
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->isCurveEnabled()Z

    move-result v5

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getAdvancedPenSetting()Ljava/lang/String;

    move-result-object v6

    .line 459
    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getStrokeRect([Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public hideAdvancedSetting()V
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 405
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : Recognition is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 408
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->hideAdvancedSetting()V

    .line 409
    return-void
.end method

.method public isCurveEnabled()Z
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 315
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->isCurveEnabled()Z

    move-result v0

    return v0
.end method

.method public redrawPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 141
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->redrawPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 144
    return-void
.end method

.method public setAdvancedSetting(Ljava/lang/String;)V
    .locals 2
    .param p1, "advancedSetting"    # Ljava/lang/String;

    .prologue
    .line 335
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 336
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setAdvancedSetting(Ljava/lang/String;)V

    .line 339
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 160
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 163
    return-void
.end method

.method public setColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 268
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 269
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setColor(I)V

    .line 272
    return-void
.end method

.method public setCurveEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 299
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 300
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setCurveEnabled(Z)V

    .line 303
    return-void
.end method

.method public setReferenceBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 191
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : pen is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setReferenceBitmap(Landroid/graphics/Bitmap;)V

    .line 194
    return-void
.end method

.method public setSize(F)V
    .locals 2
    .param p1, "size"    # F

    .prologue
    .line 206
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_0

    .line 207
    const/16 v0, 0x8

    const-string v1, "pen is not loaded"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 209
    :cond_0
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 210
    const/4 v0, 0x7

    const-string v1, "size must not be negative value"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    .line 213
    return-void
.end method

.method public showAdvancedSetting(Lcom/samsung/android/sdk/pen/pen/SpenPen$ChangedListener;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/pen/SpenPen$ChangedListener;
    .param p2, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 377
    if-nez p1, :cond_0

    .line 378
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_ARG : parameter \'listener\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    if-nez v0, :cond_1

    .line 381
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : Recognition is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mListener:Lcom/samsung/android/sdk/pen/pen/SpenPen$ChangedListener;

    .line 385
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mPenObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/SpenPen;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/samsung/android/sdk/pen/pen/SpenPen$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/pen/SpenPen$1;-><init>(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V

    invoke-interface {v0, v1, v2, p2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->showAdvancedSetting(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;Landroid/view/ViewGroup;)V

    .line 393
    return-void
.end method
