.class Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;
.super Landroid/widget/RelativeLayout;
.source "SPenSeekBarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;
    }
.end annotation


# static fields
.field private static final LL_VERSION_CODE:I = 0x15

.field private static final REP_DELAY:I = 0x14

.field protected static final SEEKBAR_LAYOUT_HEIGHT:I = 0x32

.field protected static final SPEN_SEEKBAR_TYPE_ALPHA:I = 0x0

.field protected static final SPEN_SEEKBAR_TYPE_CURSIVE:I = 0x2

.field protected static final SPEN_SEEKBAR_TYPE_DUMMY:I = 0x4

.field protected static final SPEN_SEEKBAR_TYPE_MODULATION:I = 0x6

.field protected static final SPEN_SEEKBAR_TYPE_SIZE:I = -0x1

.field protected static final SPEN_SEEKBAR_TYPE_SUSTENANCE:I = 0x3

.field private static final handelFocusPath:Ljava/lang/String;

.field private static final handelPath:Ljava/lang/String;

.field private static final handelPressPath:Ljava/lang/String;

.field private static final lineDivider:Ljava/lang/String;

.field protected static mDefaultPath:Ljava/lang/String;

.field private static final minusBgDimPath:Ljava/lang/String;

.field private static final minusBgFocusPath:Ljava/lang/String;

.field private static final minusBgPath:Ljava/lang/String;

.field private static final minusBgPressPath:Ljava/lang/String;

.field private static final plusBgDimPath:Ljava/lang/String;

.field private static final plusBgFocusPath:Ljava/lang/String;

.field private static final plusBgPath:Ljava/lang/String;

.field private static final plusBgPressPath:Ljava/lang/String;

.field private static final progressShadowPath:Ljava/lang/String;


# instance fields
.field private bgDrawable:Landroid/graphics/drawable/Drawable;

.field private mAutoDecrement:Z

.field private mAutoIncrement:Z

.field protected mContext:Landroid/content/Context;

.field private final mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private final mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field private mMinusButton:Landroid/widget/ImageButton;

.field private final mMinusButtonClickListener:Landroid/view/View$OnClickListener;

.field private final mMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mMinusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

.field private final mMinusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

.field private mPenSeekbarTextView:Landroid/widget/TextView;

.field private mPlusButton:Landroid/widget/ImageButton;

.field private final mPlusButtonClickListener:Landroid/view/View$OnClickListener;

.field private final mPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mPlusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

.field private final mPlusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

.field private final mRptRunnable:Ljava/lang/Runnable;

.field private mSPenSeekBarChangeListner:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

.field private final mSdkVersion:I

.field protected mSeekBar:Landroid/widget/SeekBar;

.field private final mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mSeekBarColor:Landroid/graphics/drawable/GradientDrawable;

.field private final mSeekbarKeyListner:Landroid/view/View$OnKeyListener;

.field private mSeekbarType:I

.field protected mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field private progressBgPath:Ljava/lang/String;

.field private final repeatUpdateHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 536
    const-string v0, ""

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    .line 537
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "snote_popup_line"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->lineDivider:Ljava/lang/String;

    .line 538
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "snote_popup_progress_btn_plus_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPath:Ljava/lang/String;

    .line 539
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "snote_popup_progress_btn_plus_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPressPath:Ljava/lang/String;

    .line 540
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "snote_popup_progress_btn_plus_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgFocusPath:Ljava/lang/String;

    .line 541
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "snote_popup_progress_btn_plus_dim"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgDimPath:Ljava/lang/String;

    .line 543
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "snote_popup_progress_btn_minus_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPath:Ljava/lang/String;

    .line 544
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "snote_popup_progress_btn_minus_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPressPath:Ljava/lang/String;

    .line 545
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "snote_popup_progress_btn_minus_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgFocusPath:Ljava/lang/String;

    .line 546
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "snote_popup_progress_btn_minus_dim"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgDimPath:Ljava/lang/String;

    .line 547
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_handle_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelPath:Ljava/lang/String;

    .line 548
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_handle_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelPressPath:Ljava/lang/String;

    .line 549
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_handle_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelFocusPath:Ljava/lang/String;

    .line 551
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_shadow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->progressShadowPath:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;FLcom/samsung/android/sdk/pen/settingui/SpenImageLoader;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ratio"    # F
    .param p3, "imageLoader"    # Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;
    .param p4, "type"    # I

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 34
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    .line 46
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    .line 49
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoDecrement:Z

    .line 50
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoIncrement:Z

    .line 60
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->repeatUpdateHandler:Landroid/os/Handler;

    .line 335
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 386
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonClickListener:Landroid/view/View$OnClickListener;

    .line 394
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonClickListener:Landroid/view/View$OnClickListener;

    .line 402
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 413
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 424
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 442
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 460
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 477
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 494
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarKeyListner:Landroid/view/View$OnKeyListener;

    .line 522
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mRptRunnable:Ljava/lang/Runnable;

    .line 550
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_bg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->progressBgPath:Ljava/lang/String;

    .line 64
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    .line 65
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    const-string v2, ""

    invoke-direct {v0, v1, v2, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 66
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 67
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 68
    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    .line 70
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->initView()V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;FLcom/samsung/android/sdk/pen/settingui/SpenImageLoader;ILjava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ratio"    # F
    .param p3, "imageLoader"    # Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;
    .param p4, "type"    # I
    .param p5, "bgProgressPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 34
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    .line 46
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    .line 49
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoDecrement:Z

    .line 50
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoIncrement:Z

    .line 60
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->repeatUpdateHandler:Landroid/os/Handler;

    .line 335
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 386
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonClickListener:Landroid/view/View$OnClickListener;

    .line 394
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonClickListener:Landroid/view/View$OnClickListener;

    .line 402
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 413
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 424
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 442
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 460
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 477
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 494
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarKeyListner:Landroid/view/View$OnKeyListener;

    .line 522
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mRptRunnable:Ljava/lang/Runnable;

    .line 550
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_bg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->progressBgPath:Ljava/lang/String;

    .line 75
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    .line 76
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    const-string v2, ""

    invoke-direct {v0, v1, v2, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 77
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 78
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 79
    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    .line 80
    iput-object p5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->progressBgPath:Ljava/lang/String;

    .line 82
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->initView()V

    .line 83
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/SeekBar$OnSeekBarChangeListener;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSPenSeekBarChangeListner:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;Z)V
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoDecrement:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoDecrement:Z

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoIncrement:Z

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;Z)V
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoIncrement:Z

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->repeatUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mRptRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private penSeekbar(Landroid/graphics/drawable/GradientDrawable;)Landroid/widget/SeekBar;
    .locals 19
    .param p1, "mPenSeekbarColor"    # Landroid/graphics/drawable/GradientDrawable;

    .prologue
    .line 237
    new-instance v15, Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v15, v5}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 238
    .local v15, "seekBar":Landroid/widget/SeekBar;
    new-instance v16, Landroid/widget/RelativeLayout$LayoutParams;

    .line 239
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x43440000    # 196.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    add-int/lit8 v5, v5, -0xc

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41b00000    # 22.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 238
    move-object/from16 v0, v16

    invoke-direct {v0, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 240
    .local v16, "seekBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41c00000    # 24.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 241
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41980000    # 19.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41c00000    # 24.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    const/4 v8, 0x0

    .line 240
    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 243
    const/4 v5, 0x1

    move-object/from16 v0, v16

    iput-boolean v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 245
    invoke-virtual/range {v15 .. v16}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 246
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    const/4 v6, 0x0

    .line 247
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x40000000    # 2.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/lit8 v7, v7, 0x2

    const/4 v8, 0x0

    .line 246
    invoke-virtual {v15, v5, v6, v7, v8}, Landroid/widget/SeekBar;->setPadding(IIII)V

    .line 248
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelPressPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelFocusPath:Ljava/lang/String;

    const/16 v6, 0x16

    const/16 v7, 0x16

    const/4 v8, 0x1

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v5

    invoke-virtual {v15, v5}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 249
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x40400000    # 3.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v15, v5}, Landroid/widget/SeekBar;->setThumbOffset(I)V

    .line 251
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 252
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 253
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x40900000    # 4.5f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 255
    new-instance v10, Landroid/graphics/drawable/ClipDrawable;

    const/4 v5, 0x3

    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-direct {v10, v0, v5, v6}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 256
    .local v10, "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->progressBgPath:Ljava/lang/String;

    const/16 v7, 0xbe

    const/16 v8, 0x9

    invoke-virtual {v5, v6, v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->bgDrawable:Landroid/graphics/drawable/Drawable;

    .line 257
    new-instance v2, Landroid/graphics/drawable/InsetDrawable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->bgDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 260
    .local v2, "bgInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    if-lez v5, :cond_1

    .line 261
    new-instance v11, Landroid/graphics/drawable/LayerDrawable;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/graphics/drawable/Drawable;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v6, 0x1

    aput-object v10, v5, v6

    invoke-direct {v11, v5}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 262
    .local v11, "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41200000    # 10.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    const/4 v6, 0x0

    .line 263
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41200000    # 10.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    const/4 v8, 0x0

    .line 262
    invoke-virtual {v15, v5, v6, v7, v8}, Landroid/widget/SeekBar;->setPadding(IIII)V

    .line 272
    :goto_0
    :try_start_0
    const-class v5, Landroid/widget/ProgressBar;

    const-string v6, "mMinHeight"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v14

    .line 273
    .local v14, "minHeight":Ljava/lang/reflect/Field;
    const/4 v5, 0x1

    invoke-virtual {v14, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 274
    const-class v5, Landroid/widget/ProgressBar;

    const-string v6, "mMaxHeight"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v12

    .line 275
    .local v12, "maxHeight":Ljava/lang/reflect/Field;
    const/4 v5, 0x1

    invoke-virtual {v12, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    .line 278
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41100000    # 9.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v14, v15, v5}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 279
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41100000    # 9.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v12, v15, v5}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1

    .line 289
    .end local v12    # "maxHeight":Ljava/lang/reflect/Field;
    .end local v14    # "minHeight":Ljava/lang/reflect/Field;
    :goto_1
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    const/16 v6, 0x15

    if-lt v5, v6, :cond_0

    .line 291
    :try_start_2
    const-class v5, Landroid/widget/AbsSeekBar;

    const-string v6, "setSplitTrack"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    sget-object v17, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v17, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v13

    .line 294
    .local v13, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x1

    :try_start_3
    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v13, v15, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_4

    .line 307
    .end local v13    # "method":Ljava/lang/reflect/Method;
    :goto_2
    new-instance v5, Landroid/graphics/drawable/RippleDrawable;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v7, 0x40

    const/4 v8, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v7, v8, v0, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    const/4 v7, 0x0

    .line 308
    const/4 v8, 0x0

    invoke-direct {v5, v6, v7, v8}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 307
    invoke-virtual {v15, v5}, Landroid/widget/SeekBar;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 311
    :cond_0
    invoke-virtual {v15, v11}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 312
    new-instance v5, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$12;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;Landroid/widget/SeekBar;)V

    invoke-virtual {v15, v5}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 332
    return-object v15

    .line 265
    .end local v11    # "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->progressShadowPath:Ljava/lang/String;

    const/16 v7, 0xbe

    const/16 v8, 0x9

    invoke-virtual {v5, v6, v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 266
    .local v4, "shadowDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v3, Landroid/graphics/drawable/InsetDrawable;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 267
    .local v3, "shadowInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v11, Landroid/graphics/drawable/LayerDrawable;

    const/4 v5, 0x3

    new-array v5, v5, [Landroid/graphics/drawable/Drawable;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v6, 0x1

    aput-object v10, v5, v6

    const/4 v6, 0x2

    .line 268
    aput-object v3, v5, v6

    .line 267
    invoke-direct {v11, v5}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .restart local v11    # "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    goto/16 :goto_0

    .line 280
    .end local v3    # "shadowInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    .end local v4    # "shadowDrawable":Landroid/graphics/drawable/Drawable;
    .restart local v12    # "maxHeight":Ljava/lang/reflect/Field;
    .restart local v14    # "minHeight":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v9

    .line 281
    .local v9, "e":Ljava/lang/IllegalAccessException;
    :try_start_4
    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/NoSuchFieldException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_1

    .line 285
    .end local v9    # "e":Ljava/lang/IllegalAccessException;
    .end local v12    # "maxHeight":Ljava/lang/reflect/Field;
    .end local v14    # "minHeight":Ljava/lang/reflect/Field;
    :catch_1
    move-exception v9

    .line 286
    .local v9, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v9}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto/16 :goto_1

    .line 282
    .end local v9    # "e":Ljava/lang/NoSuchFieldException;
    .restart local v12    # "maxHeight":Ljava/lang/reflect/Field;
    .restart local v14    # "minHeight":Ljava/lang/reflect/Field;
    :catch_2
    move-exception v9

    .line 283
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :try_start_5
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/NoSuchFieldException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_1

    .line 295
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    .end local v12    # "maxHeight":Ljava/lang/reflect/Field;
    .end local v14    # "minHeight":Ljava/lang/reflect/Field;
    .restart local v13    # "method":Ljava/lang/reflect/Method;
    :catch_3
    move-exception v9

    .line 296
    .local v9, "e":Ljava/lang/IllegalAccessException;
    :try_start_6
    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_2

    .line 303
    .end local v9    # "e":Ljava/lang/IllegalAccessException;
    .end local v13    # "method":Ljava/lang/reflect/Method;
    :catch_4
    move-exception v9

    .line 304
    .local v9, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v9}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_2

    .line 297
    .end local v9    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v13    # "method":Ljava/lang/reflect/Method;
    :catch_5
    move-exception v9

    .line 298
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :try_start_7
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 299
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :catch_6
    move-exception v9

    .line 300
    .local v9, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v9}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_7
    .catch Ljava/lang/NoSuchMethodException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_2
.end method

.method private seekbarLayout()V
    .locals 17

    .prologue
    .line 140
    new-instance v14, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v14, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 141
    .local v14, "sizeDisplayLayout":Landroid/widget/RelativeLayout;
    new-instance v15, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    .line 142
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42480000    # 50.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 141
    invoke-direct {v15, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 143
    .local v15, "sizeDisplayLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v14, v15}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 144
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41000000    # 8.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v14, v1, v2, v3, v4}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 147
    new-instance v1, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    .line 148
    new-instance v13, Landroid/widget/RelativeLayout$LayoutParams;

    .line 149
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c00000    # 24.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41c00000    # 24.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 148
    invoke-direct {v13, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 150
    .local v13, "plusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v1, 0x1

    iput-boolean v1, v13, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 151
    const/16 v1, 0xb

    invoke-virtual {v13, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 152
    const/16 v1, 0x8

    invoke-virtual {v13, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 154
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v13}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_plus"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 157
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 158
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPressPath:Ljava/lang/String;

    .line 159
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgFocusPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgDimPath:Ljava/lang/String;

    const/16 v6, 0x18

    const/16 v7, 0x18

    .line 158
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 170
    :goto_0
    new-instance v1, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    .line 171
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 172
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c00000    # 24.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41c00000    # 24.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 171
    invoke-direct {v11, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 173
    .local v11, "minusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v1, 0x1

    iput-boolean v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 174
    const/16 v1, 0x9

    invoke-virtual {v11, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 175
    const/16 v1, 0x8

    invoke-virtual {v11, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 177
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v11}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 178
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_minus"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 180
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_2

    .line 181
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPressPath:Ljava/lang/String;

    .line 182
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgFocusPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgDimPath:Ljava/lang/String;

    const/16 v6, 0x18

    const/16 v7, 0x18

    .line 181
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 193
    :goto_1
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarColor:Landroid/graphics/drawable/GradientDrawable;

    .line 194
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarColor:Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->penSeekbar(Landroid/graphics/drawable/GradientDrawable;)Landroid/widget/SeekBar;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    .line 196
    new-instance v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    .line 197
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 198
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    const/16 v2, 0x56

    const/16 v3, 0x57

    const/16 v4, 0x5b

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 199
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41400000    # 12.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 202
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    if-lez v1, :cond_4

    .line 203
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    const/16 v2, 0x33

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 204
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v12, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 205
    .local v12, "penSizeTextParam":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x420c0000    # 35.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v12, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 226
    :goto_2
    const/4 v1, 0x4

    invoke-virtual {v12, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 227
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 228
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    invoke-virtual {v14, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 229
    const/4 v1, 0x0

    invoke-virtual {v14, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 230
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v14, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 231
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v14, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 232
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v14, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 233
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->addView(Landroid/view/View;)V

    .line 234
    return-void

    .line 160
    .end local v11    # "minusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v12    # "penSizeTextParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 161
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v4, 0x40

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    const/4 v4, 0x0

    .line 162
    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 161
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPath:Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgFocusPath:Ljava/lang/String;

    const/4 v5, 0x0

    .line 164
    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgDimPath:Ljava/lang/String;

    const/16 v7, 0x18

    const/16 v8, 0x18

    .line 163
    invoke-virtual/range {v1 .. v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 166
    :cond_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgFocusPath:Ljava/lang/String;

    .line 167
    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgDimPath:Ljava/lang/String;

    const/16 v6, 0x18

    const/16 v7, 0x18

    .line 166
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 183
    .restart local v11    # "minusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_3

    .line 184
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v4, 0x40

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    .line 185
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 184
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPath:Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgFocusPath:Ljava/lang/String;

    const/4 v5, 0x0

    .line 187
    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgDimPath:Ljava/lang/String;

    const/16 v7, 0x18

    const/16 v8, 0x18

    .line 186
    invoke-virtual/range {v1 .. v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 189
    :cond_3
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgFocusPath:Ljava/lang/String;

    .line 190
    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgDimPath:Ljava/lang/String;

    const/16 v6, 0x18

    const/16 v7, 0x18

    .line 189
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 207
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    const/16 v2, 0x31

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 208
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42780000    # 62.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 209
    const/4 v2, -0x1

    .line 208
    invoke-direct {v12, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 212
    .restart local v12    # "penSizeTextParam":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v10, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v10, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 213
    .local v10, "imgDivider":Landroid/widget/ImageView;
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    .line 214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 213
    invoke-direct {v9, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 215
    .local v9, "dividerParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v1, 0x1

    iput-boolean v1, v9, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 216
    const/4 v1, 0x6

    invoke-virtual {v9, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 217
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 218
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v9, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 219
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v9, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 220
    invoke-virtual {v10, v9}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 221
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->lineDivider:Ljava/lang/String;

    invoke-virtual {v1, v10, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 222
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->addView(Landroid/view/View;)V

    goto/16 :goto_2
.end method

.method private setSeekbarListener()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 121
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarKeyListner:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 127
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 128
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 132
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 134
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 137
    :cond_2
    return-void
.end method


# virtual methods
.method protected close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 568
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 569
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    .line 571
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 572
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    .line 573
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 574
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    .line 576
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 577
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    .line 579
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarColor:Landroid/graphics/drawable/GradientDrawable;

    .line 580
    return-void
.end method

.method public getPenSeekbar()Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method protected getSeekBarColor()Landroid/graphics/drawable/GradientDrawable;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarColor:Landroid/graphics/drawable/GradientDrawable;

    return-object v0
.end method

.method protected getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected initView()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->seekbarLayout()V

    .line 87
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->setSeekbarListener()V

    .line 88
    return-void
.end method

.method protected setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->bgDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 92
    return-void
.end method

.method protected setSPenSeekBarChangeListener(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;)V
    .locals 0
    .param p1, "mListener"    # Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

    .prologue
    .line 554
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSPenSeekBarChangeListner:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

    .line 555
    return-void
.end method

.method protected setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    return-void
.end method

.method protected setTextXPosition(I)V
    .locals 2
    .param p1, "x"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setX(F)V

    .line 112
    return-void
.end method

.method protected setTextYPosition(I)V
    .locals 2
    .param p1, "y"    # I

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setY(F)V

    .line 116
    return-void
.end method
