.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;
.super Ljava/lang/Object;
.source "SpenTextBox.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DoubleTapListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 6673
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;)V
    .locals 0

    .prologue
    .line 6673
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 6677
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$33(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 6739
    :cond_0
    :goto_0
    return v8

    .line 6681
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 6682
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 6685
    :cond_2
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v10, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 6687
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v10

    if-lez v10, :cond_9

    .line 6688
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v10

    if-nez v10, :cond_3

    move v8, v9

    .line 6689
    goto :goto_0

    .line 6691
    :cond_3
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v6

    .line 6692
    .local v6, "str":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 6696
    const/4 v10, 0x2

    new-array v4, v10, [F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    aput v10, v4, v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    aput v10, v4, v8

    .line 6697
    .local v4, "point":[F
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$37(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$38(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 6698
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$38(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v10

    invoke-virtual {v10, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 6700
    aget v10, v4, v8

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J
    invoke-static {v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$39(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)J

    move-result-wide v12

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F
    invoke-static {v11, v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$40(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;J)F

    move-result v11

    add-float/2addr v10, v11

    aput v10, v4, v8

    .line 6702
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    aget v11, v4, v8

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I
    invoke-static {v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)I

    move-result v1

    .line 6703
    .local v1, "line":I
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    aget v11, v4, v9

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getOffsetForHorizontal(IF)I
    invoke-static {v10, v1, v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$41(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I

    move-result v2

    .line 6705
    .local v2, "offset":I
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$42(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/graphics/RectF;

    move-result-object v10

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[I

    move-result-object v10

    if-eqz v10, :cond_0

    .line 6709
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[I

    move-result-object v10

    aget v10, v10, v1

    if-ne v2, v10, :cond_4

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$42(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/graphics/RectF;

    move-result-object v10

    aget-object v10, v10, v2

    iget v10, v10, Landroid/graphics/RectF;->right:F

    aget v11, v4, v9

    cmpg-float v10, v10, v11

    if-gez v10, :cond_4

    .line 6710
    add-int/lit8 v2, v2, 0x1

    .line 6713
    :cond_4
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v10

    if-lt v2, v10, :cond_6

    add-int/lit8 v10, v2, -0x1

    invoke-virtual {v6, v10}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 6715
    .local v3, "oneChar":C
    :goto_1
    const/16 v10, 0x20

    if-eq v3, v10, :cond_5

    const/16 v10, 0x9

    if-eq v3, v10, :cond_5

    const/16 v10, 0xa

    if-eq v3, v10, :cond_5

    const/16 v10, 0xd

    if-ne v3, v10, :cond_7

    .line 6716
    :cond_5
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v10, v2, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 6718
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v9, v2, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    goto/16 :goto_0

    .line 6713
    .end local v3    # "oneChar":C
    :cond_6
    invoke-virtual {v6, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    goto :goto_1

    .line 6720
    .restart local v3    # "oneChar":C
    :cond_7
    new-instance v7, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    const/4 v10, 0x0

    invoke-direct {v7, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)V

    .line 6721
    .local v7, "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    const/4 v5, 0x0

    .local v5, "start":I
    const/4 v0, 0x0

    .line 6722
    .local v0, "end":I
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z
    invoke-static {v10, v2, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$44(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 6723
    iget v5, v7, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    .line 6724
    iget v0, v7, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    .line 6726
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v10, v5, v0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_0

    .line 6728
    :cond_8
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v10, v2, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_0

    .line 6732
    .end local v0    # "end":I
    .end local v1    # "line":I
    .end local v2    # "offset":I
    .end local v3    # "oneChar":C
    .end local v4    # "point":[F
    .end local v5    # "start":I
    .end local v6    # "str":Ljava/lang/String;
    .end local v7    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    :cond_9
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    move-result-object v10

    if-eqz v10, :cond_a

    .line 6733
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v10, v9, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    .line 6735
    :cond_a
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v10, v9, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 6736
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    goto/16 :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 6744
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 6749
    const/4 v0, 0x0

    return v0
.end method
