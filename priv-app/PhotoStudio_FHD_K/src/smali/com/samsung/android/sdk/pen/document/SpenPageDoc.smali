.class public final Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.super Ljava/lang/Object;
.source "SpenPageDoc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;,
        Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectIndexMovedListener;,
        Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectListener;,
        Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectSelectedListener;
    }
.end annotation


# static fields
.field public static final BACKGROUND_IMAGE_MODE_CENTER:I = 0x0

.field public static final BACKGROUND_IMAGE_MODE_FIT:I = 0x2

.field public static final BACKGROUND_IMAGE_MODE_STRETCH:I = 0x1

.field public static final BACKGROUND_IMAGE_MODE_TILE:I = 0x3

.field public static final FIND_TYPE_ALL:I = 0x1f

.field public static final FIND_TYPE_CONTAINER:I = 0x8

.field public static final FIND_TYPE_IMAGE:I = 0x4

.field public static final FIND_TYPE_STROKE:I = 0x1

.field public static final FIND_TYPE_TEXT_BOX:I = 0x2

.field public static final GEO_TAG_STATE_DEFAULT:I = 0x0

.field public static final GEO_TAG_STATE_REMOVED:I = 0x2

.field public static final GEO_TAG_STATE_SET:I = 0x1

.field public static final HISTORY_MANAGER_MODE_MULTIPLE_VIEW:I = 0x1

.field public static final HISTORY_MANAGER_MODE_SINGLE_VIEW:I = 0x0

.field private static final NATIVE_COMMAND_APPEND_OBJECTLIST:I = 0x2

.field private static final NATIVE_COMMAND_DUMMY:I = 0x0

.field private static final NATIVE_COMMAND_SET_MULTI_VIEW_USERID:I = 0x1

.field public static final ORIENTATION_LANDSCAPE:I = 0x1

.field public static final ORIENTATION_PORTRAIT:I


# instance fields
.field private mHandle:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->mHandle:I

    .line 404
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 1
    .param p1, "templateUri"    # Ljava/lang/String;
    .param p2, "templatePage"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->mHandle:I

    .line 407
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_Construct2(Ljava/lang/String;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 408
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 410
    :cond_0
    return-void
.end method

.method private native Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_AddTag(Ljava/lang/String;)Z
.end method

.method private native PageDoc_AppendLayer(I)Z
.end method

.method private native PageDoc_AppendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native PageDoc_ClearChangedFlagOfLayer()V
.end method

.method private native PageDoc_ClearRecordedObject()Z
.end method

.method private native PageDoc_Construct2(Ljava/lang/String;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z
.end method

.method private native PageDoc_Copy(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z
.end method

.method private native PageDoc_EnableLayerEventForward(IZ)Z
.end method

.method private native PageDoc_FindObjectAtPosition(IFF)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IFF)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_FindObjectInClosedCurve(I[Landroid/graphics/PointF;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[",
            "Landroid/graphics/PointF;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_FindObjectInRect(ILandroid/graphics/RectF;Z)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/RectF;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_FindTopObjectAtPosition(IFF)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.end method

.method private native PageDoc_GetBackgroundColor()I
.end method

.method private native PageDoc_GetBackgroundImage()Landroid/graphics/Bitmap;
.end method

.method private native PageDoc_GetBackgroundImageMode()I
.end method

.method private native PageDoc_GetBackgroundImagePath()Ljava/lang/String;
.end method

.method private native PageDoc_GetCurrentLayerId()I
.end method

.method private native PageDoc_GetExtraDataByteArray(Ljava/lang/String;)[B
.end method

.method private native PageDoc_GetExtraDataInt(Ljava/lang/String;)I
.end method

.method private native PageDoc_GetExtraDataString(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native PageDoc_GetExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method private native PageDoc_GetGeoTagLatitude()D
.end method

.method private native PageDoc_GetGeoTagLongitude()D
.end method

.method private native PageDoc_GetGeoTagState()I
.end method

.method private native PageDoc_GetHeight()I
.end method

.method private native PageDoc_GetHistoryUpdateRect()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetId()Ljava/lang/String;
.end method

.method private native PageDoc_GetLastEditedTime()J
.end method

.method private native PageDoc_GetLayerCount()I
.end method

.method private native PageDoc_GetLayerHistoryId(I)I
.end method

.method private native PageDoc_GetLayerIdByIndex(I)I
.end method

.method private native PageDoc_GetLayerIndex(I)I
.end method

.method private native PageDoc_GetLayerName(I)Ljava/lang/String;
.end method

.method private native PageDoc_GetObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.end method

.method private native PageDoc_GetObjectByRuntimeHandle(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.end method

.method private native PageDoc_GetObjectCount(Z)I
.end method

.method private native PageDoc_GetObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)I
.end method

.method private native PageDoc_GetObjectList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetObjectList2(I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetObjectList3(ILjava/lang/String;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetObjectList4(ILjava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetOrientation()I
.end method

.method private native PageDoc_GetRectOfAllObject()Landroid/graphics/RectF;
.end method

.method private native PageDoc_GetSelectedObject()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetSelectedObjectCount()I
.end method

.method private native PageDoc_GetTag()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetTemplateObjectList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetTemplateUri()Ljava/lang/String;
.end method

.method private native PageDoc_GetThumbnail()Landroid/graphics/Bitmap;
.end method

.method private native PageDoc_GetVoiceData()Ljava/lang/String;
.end method

.method private native PageDoc_GetWidth()I
.end method

.method private native PageDoc_GroupObject(Ljava/util/ArrayList;Z)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;Z)",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;"
        }
    .end annotation
.end method

.method private native PageDoc_GroupSelectedObject(Z)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
.end method

.method private native PageDoc_HasExtraDataByteArray(Ljava/lang/String;)Z
.end method

.method private native PageDoc_HasExtraDataInt(Ljava/lang/String;)Z
.end method

.method private native PageDoc_HasExtraDataString(Ljava/lang/String;)Z
.end method

.method private native PageDoc_HasExtraDataStringArray(Ljava/lang/String;)Z
.end method

.method private native PageDoc_HasRecordedObject()Z
.end method

.method private native PageDoc_InsertLayer(II)Z
.end method

.method private native PageDoc_InsertObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;I)Z
.end method

.method private native PageDoc_IsChanged()Z
.end method

.method private native PageDoc_IsHistoryManagerUsed()Z
.end method

.method private native PageDoc_IsLayerChanged()Z
.end method

.method private native PageDoc_IsLayerEventForwardable(I)Z
.end method

.method private native PageDoc_IsObjectLoaded()Z
.end method

.method private native PageDoc_IsRecording()Z
.end method

.method private native PageDoc_IsTextOnly()Z
.end method

.method private native PageDoc_IsValid()Z
.end method

.method private native PageDoc_LoadHeader(Ljava/lang/String;)Z
.end method

.method private native PageDoc_LoadObject()Z
.end method

.method private native PageDoc_MoveLayerIndex(II)Z
.end method

.method private native PageDoc_MoveObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;IZ)Z
.end method

.method private native PageDoc_RemoveAllObject()Z
.end method

.method private native PageDoc_RemoveExtraDataByteArray(Ljava/lang/String;)Z
.end method

.method private native PageDoc_RemoveExtraDataInt(Ljava/lang/String;)Z
.end method

.method private native PageDoc_RemoveExtraDataString(Ljava/lang/String;)Z
.end method

.method private native PageDoc_RemoveExtraDataStringArray(Ljava/lang/String;)Z
.end method

.method private native PageDoc_RemoveGeoTag()Z
.end method

.method private native PageDoc_RemoveLayer(I)Z
.end method

.method private native PageDoc_RemoveObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native PageDoc_RemoveSelectedObject()Z
.end method

.method private native PageDoc_RemoveTag(Ljava/lang/String;)Z
.end method

.method private native PageDoc_Save()Z
.end method

.method private native PageDoc_SelectObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native PageDoc_SelectObject(Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native PageDoc_SetBackgroundColor(I)Z
.end method

.method private native PageDoc_SetBackgroundImage(Ljava/lang/String;)Z
.end method

.method private native PageDoc_SetBackgroundImageMode(I)Z
.end method

.method private native PageDoc_SetCurrentLayer(I)Z
.end method

.method private native PageDoc_SetExtraDataByteArray(Ljava/lang/String;[BI)Z
.end method

.method private native PageDoc_SetExtraDataInt(Ljava/lang/String;I)Z
.end method

.method private native PageDoc_SetExtraDataString(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private native PageDoc_SetExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z
.end method

.method private native PageDoc_SetGeoTag(DD)Z
.end method

.method private native PageDoc_SetLayerName(ILjava/lang/String;)Z
.end method

.method private native PageDoc_SetObjectIndexMovedListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectIndexMovedListener;)Z
.end method

.method private native PageDoc_SetObjectListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectListener;)Z
.end method

.method private native PageDoc_SetObjectSelectedListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectSelectedListener;)Z
.end method

.method private native PageDoc_SetTemplateUri(Ljava/lang/String;)Z
.end method

.method private native PageDoc_SetThumbnail(Landroid/graphics/Bitmap;)Z
.end method

.method private native PageDoc_SetVoiceData(Ljava/lang/String;)V
.end method

.method private native PageDoc_SetVolatileBackgroundImage(Landroid/graphics/Bitmap;)Z
.end method

.method private native PageDoc_StartRecord()Z
.end method

.method private native PageDoc_StopRecord()Z
.end method

.method private native PageDoc_UngroupObject(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Z)Z
.end method

.method private native PageDoc_UngroupSelectedObject(Z)Z
.end method

.method private native PageDoc_UnloadObject()Z
.end method

.method private native PageDoc_UseHistoryManager(Z)V
.end method

.method private native PageDoc_clearHistory()V
.end method

.method private native PageDoc_clearHistory2(I)V
.end method

.method private native PageDoc_clearHistoryTag()Z
.end method

.method private native PageDoc_clearRedoHistory()V
.end method

.method private native PageDoc_commitHistory(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)Z
.end method

.method private native PageDoc_finalize()V
.end method

.method private native PageDoc_getHistoryManagerMode()I
.end method

.method private native PageDoc_getLastHistoryId()I
.end method

.method private native PageDoc_getUndoLimit()I
.end method

.method private native PageDoc_isRedoable()Z
.end method

.method private native PageDoc_isRedoable2(I)Z
.end method

.method private native PageDoc_isUndoable()Z
.end method

.method private native PageDoc_isUndoable2(I)Z
.end method

.method private native PageDoc_redo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_redo2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_redoAll()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_redoAll2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_setHistoryId(I)Z
.end method

.method private native PageDoc_setHistoryListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;)Z
.end method

.method private native PageDoc_setHistoryManagerMode(I)Z
.end method

.method private native PageDoc_setHistoryTag()Z
.end method

.method private native PageDoc_setUndoLimit(I)V
.end method

.method private native PageDoc_startHistoryGroup()Z
.end method

.method private native PageDoc_stopHistoryGroup()Z
.end method

.method private native PageDoc_undo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_undo2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_undoAll()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_undoAll2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_undoToTag()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private static isBuildTypeEngMode()Z
    .locals 2

    .prologue
    .line 431
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private throwUncheckedException(I)V
    .locals 3
    .param p1, "errno"    # I

    .prologue
    .line 413
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    .line 414
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenPageDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 416
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 418
    return-void
.end method


# virtual methods
.method public addTag(Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 1141
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_AddTag(Ljava/lang/String;)Z

    move-result v0

    .line 1142
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1143
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1145
    :cond_0
    return-void
.end method

.method public appendLayer(I)V
    .locals 2
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 1865
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_AppendLayer(I)Z

    move-result v0

    .line 1866
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1867
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1869
    :cond_0
    return-void
.end method

.method public appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 485
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_AppendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 486
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 488
    :cond_0
    return-void
.end method

.method public appendObjectList(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2770
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2771
    .local v2, "tempArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-nez p1, :cond_1

    .line 2791
    :cond_0
    :goto_0
    return-void

    .line 2774
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 2776
    .local v0, "it":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2783
    const/4 v4, 0x2

    invoke-direct {p0, v4, v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2784
    .local v1, "rnt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-nez v1, :cond_4

    .line 2785
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    goto :goto_0

    .line 2777
    .end local v1    # "rnt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 2778
    .local v3, "tmpObject":Ljava/lang/Object;
    if-nez v3, :cond_3

    .line 2779
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "E_INVALID_ARG"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2781
    :cond_3
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2788
    .end local v3    # "tmpObject":Ljava/lang/Object;
    .restart local v1    # "rnt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_4
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2789
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    goto :goto_0
.end method

.method public clearChangedFlagOfLayer()V
    .locals 0

    .prologue
    .line 2092
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_ClearChangedFlagOfLayer()V

    .line 2093
    return-void
.end method

.method public clearHistory()V
    .locals 0

    .prologue
    .line 2501
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_clearHistory()V

    .line 2502
    return-void
.end method

.method public clearHistoryTag()V
    .locals 1

    .prologue
    .line 2580
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_clearHistoryTag()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2581
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2583
    :cond_0
    return-void
.end method

.method public clearRecordedObject()V
    .locals 2

    .prologue
    .line 1205
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_ClearRecordedObject()Z

    move-result v0

    .line 1206
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1207
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1209
    :cond_0
    return-void
.end method

.method public clearRedoHistory()V
    .locals 0

    .prologue
    .line 2510
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_clearRedoHistory()V

    .line 2511
    return-void
.end method

.method public commitHistory(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 2
    .param p1, "userData"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    .prologue
    .line 2526
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_commitHistory(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)Z

    move-result v0

    .line 2527
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2528
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2530
    :cond_0
    return-void
.end method

.method public copy(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 2
    .param p1, "sourcePage"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 2159
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_Copy(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z

    move-result v0

    .line 2160
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2161
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2163
    :cond_0
    return-void
.end method

.method public createObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 2
    .param p1, "type"    # I

    .prologue
    const/4 v1, 0x0

    .line 2696
    packed-switch p1, :pswitch_data_0

    move-object v0, v1

    .line 2711
    :goto_0
    return-object v0

    .line 2698
    :pswitch_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 2701
    :pswitch_1
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 2704
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;-><init>()V

    .line 2705
    .local v0, "i":Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setImage(Ljava/lang/String;)V

    goto :goto_0

    .line 2709
    .end local v0    # "i":Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 2696
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public createObject(IZ)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 1
    .param p1, "type"    # I
    .param p2, "isTemplateObject"    # Z

    .prologue
    .line 2729
    packed-switch p1, :pswitch_data_0

    .line 2742
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2731
    :pswitch_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>(Z)V

    goto :goto_0

    .line 2734
    :pswitch_1
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;-><init>(Z)V

    goto :goto_0

    .line 2737
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;-><init>(Z)V

    goto :goto_0

    .line 2740
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>(Z)V

    goto :goto_0

    .line 2729
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 2800
    instance-of v0, p1, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    .line 2801
    iget v0, p0, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->mHandle:I

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .end local p1    # "o":Ljava/lang/Object;
    iget v1, p1, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->mHandle:I

    if-ne v0, v1, :cond_0

    .line 2802
    const/4 v0, 0x1

    .line 2805
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 425
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;

    .line 426
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_finalize()V

    .line 427
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->mHandle:I

    .line 428
    return-void
.end method

.method public findObjectAtPosition(IFF)Ljava/util/ArrayList;
    .locals 2
    .param p1, "typeFilter"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IFF)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 807
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_FindObjectAtPosition(IFF)Ljava/util/ArrayList;

    move-result-object v0

    .line 808
    .local v0, "rnt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 809
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 811
    :cond_0
    return-object v0
.end method

.method public findObjectInClosedCurve(I[Landroid/graphics/PointF;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "typeFilter"    # I
    .param p2, "points"    # [Landroid/graphics/PointF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[",
            "Landroid/graphics/PointF;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 835
    array-length v1, p2

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_FindObjectInClosedCurve(I[Landroid/graphics/PointF;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 836
    .local v0, "rnt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 837
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 839
    :cond_0
    return-object v0
.end method

.method public findObjectInRect(ILandroid/graphics/RectF;Z)Ljava/util/ArrayList;
    .locals 2
    .param p1, "typeFilter"    # I
    .param p2, "rectf"    # Landroid/graphics/RectF;
    .param p3, "allAreas"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/RectF;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 865
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_FindObjectInRect(ILandroid/graphics/RectF;Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 866
    .local v0, "rnt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 867
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 869
    :cond_0
    return-object v0
.end method

.method public findTopObjectAtPosition(IFF)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 1
    .param p1, "typeFilter"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    .line 784
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_FindTopObjectAtPosition(IFF)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    return-object v0
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 1344
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetBackgroundColor()I

    move-result v0

    return v0
.end method

.method public getBackgroundImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1275
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetBackgroundImage()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getBackgroundImageMode()I
    .locals 1

    .prologue
    .line 1319
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetBackgroundImageMode()I

    move-result v0

    return v0
.end method

.method public getBackgroundImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1285
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetBackgroundImagePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentLayerId()I
    .locals 1

    .prologue
    .line 1946
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetCurrentLayerId()I

    move-result v0

    return v0
.end method

.method public getDrawnRectOfAllObject()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1392
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetRectOfAllObject()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataByteArray(Ljava/lang/String;)[B
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1649
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetExtraDataByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataInt(Ljava/lang/String;)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1625
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetExtraDataInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getExtraDataString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1613
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetExtraDataString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1637
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGeoTagLatitude()D
    .locals 2

    .prologue
    .line 1471
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetGeoTagLatitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getGeoTagLongitude()D
    .locals 2

    .prologue
    .line 1482
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetGeoTagLongitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getGeoTagState()I
    .locals 3

    .prologue
    .line 1495
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetGeoTagState()I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1499
    :goto_0
    return v1

    .line 1496
    :catch_0
    move-exception v0

    .line 1497
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 1498
    const-string v1, "SpenPageDoc"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1499
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 451
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetHeight()I

    move-result v0

    return v0
.end method

.method public getHistoryManagerMode()I
    .locals 1

    .prologue
    .line 2663
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_getHistoryManagerMode()I

    move-result v0

    return v0
.end method

.method public getHistoryUpdateRect()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2556
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetHistoryUpdateRect()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1379
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastEditedTime()J
    .locals 2

    .prologue
    .line 462
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLastEditedTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getLayerCount()I
    .locals 1

    .prologue
    .line 1992
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLayerCount()I

    move-result v0

    return v0
.end method

.method public getLayerIdByIndex(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1978
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLayerIdByIndex(I)I

    move-result v0

    .line 1979
    .local v0, "rnt":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1980
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1982
    :cond_0
    return v0
.end method

.method public getLayerIndex(I)I
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 1960
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLayerIndex(I)I

    move-result v0

    .line 1961
    .local v0, "rnt":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1962
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1964
    :cond_0
    return v0
.end method

.method public getLayerName(I)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 2024
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLayerIndex(I)I

    move-result v0

    .line 2025
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2026
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2029
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLayerName(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 583
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    .line 584
    .local v0, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-nez v0, :cond_0

    .line 585
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 587
    :cond_0
    return-object v0
.end method

.method public getObjectByRuntimeHandle(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 2
    .param p1, "runtimeHandle"    # I

    .prologue
    .line 718
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectByRuntimeHandle(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    .line 719
    .local v0, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-nez v0, :cond_0

    .line 720
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 722
    :cond_0
    return-object v0
.end method

.method public getObjectCount(Z)I
    .locals 1
    .param p1, "includeInvisible"    # Z

    .prologue
    .line 704
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectCount(Z)I

    move-result v0

    return v0
.end method

.method public getObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)I
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 738
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)I

    move-result v0

    .line 739
    .local v0, "rnt":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 740
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 742
    :cond_0
    return v0
.end method

.method public getObjectList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 598
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectList()Ljava/util/ArrayList;

    move-result-object v0

    .line 599
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 600
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 602
    :cond_0
    return-object v0
.end method

.method public getObjectList(I)Ljava/util/ArrayList;
    .locals 2
    .param p1, "typeFilter"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 617
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectList2(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 618
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 619
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 621
    :cond_0
    return-object v0
.end method

.method public getObjectList(ILjava/lang/String;I)Ljava/util/ArrayList;
    .locals 2
    .param p1, "typeFilter"    # I
    .param p2, "extraKey"    # Ljava/lang/String;
    .param p3, "extraValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 645
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectList3(ILjava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 646
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 647
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 649
    :cond_0
    return-object v0
.end method

.method public getObjectList(ILjava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "typeFilter"    # I
    .param p2, "extraKey"    # Ljava/lang/String;
    .param p3, "extraValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 673
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectList4(ILjava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 674
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 675
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 677
    :cond_0
    return-object v0
.end method

.method public getOrientation()I
    .locals 2

    .prologue
    .line 1220
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetOrientation()I

    move-result v0

    .line 1221
    .local v0, "rnt":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1222
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1224
    :cond_0
    return v0
.end method

.method public getSelectedObject()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 951
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetSelectedObject()Ljava/util/ArrayList;

    move-result-object v0

    .line 952
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 953
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 955
    :cond_0
    return-object v0
.end method

.method public getSelectedObjectCount()I
    .locals 1

    .prologue
    .line 966
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetSelectedObjectCount()I

    move-result v0

    return v0
.end method

.method public getTag()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1170
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetTag()Ljava/util/ArrayList;

    move-result-object v0

    .line 1171
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v0, :cond_0

    .line 1172
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1174
    :cond_0
    return-object v0
.end method

.method public getTemplateObjectList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 687
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetTemplateObjectList()Ljava/util/ArrayList;

    move-result-object v0

    .line 688
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 689
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 691
    :cond_0
    return-object v0
.end method

.method public getTemplateUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1369
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetTemplateUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUndoLimit()I
    .locals 1

    .prologue
    .line 2490
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_getUndoLimit()I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 441
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetWidth()I

    move-result v0

    return v0
.end method

.method public groupObject(Ljava/util/ArrayList;Z)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .locals 2
    .param p2, "selectAfterGroup"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;Z)",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;"
        }
    .end annotation

    .prologue
    .line 1029
    .local p1, "groupList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GroupObject(Ljava/util/ArrayList;Z)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v0

    .line 1030
    .local v0, "conatiner":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    if-nez v0, :cond_0

    .line 1031
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1033
    :cond_0
    return-object v0
.end method

.method public groupSelectedObject(Z)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .locals 2
    .param p1, "selectAfterGroup"    # Z

    .prologue
    .line 1055
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GroupSelectedObject(Z)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v0

    .line 1056
    .local v0, "conatiner":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    if-nez v0, :cond_0

    .line 1057
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1059
    :cond_0
    return-object v0
.end method

.method public hasExtraDataByteArray(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1701
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_HasExtraDataByteArray(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataInt(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1675
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_HasExtraDataInt(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataString(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1662
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_HasExtraDataString(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataStringArray(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1688
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_HasExtraDataStringArray(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasRecordedObject()Z
    .locals 1

    .prologue
    .line 1196
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_HasRecordedObject()Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 2815
    iget v0, p0, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->mHandle:I

    return v0
.end method

.method public insertLayer(II)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "index"    # I

    .prologue
    .line 1892
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_InsertLayer(II)Z

    move-result v0

    .line 1893
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1894
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1896
    :cond_0
    return-void
.end method

.method public insertObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;I)V
    .locals 1
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .param p2, "index"    # I

    .prologue
    .line 514
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_InsertObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 515
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 517
    :cond_0
    return-void
.end method

.method public isChanged()Z
    .locals 1

    .prologue
    .line 1403
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsChanged()Z

    move-result v0

    return v0
.end method

.method public isLayerChanged()Z
    .locals 1

    .prologue
    .line 2083
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsLayerChanged()Z

    move-result v0

    return v0
.end method

.method public isLayerEventForwardEnabled(I)Z
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 2066
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLayerIndex(I)I

    move-result v0

    .line 2067
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2068
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2071
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsLayerEventForwardable(I)Z

    move-result v1

    return v1
.end method

.method public isObjectContained(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 755
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)I

    move-result v0

    .line 756
    .local v0, "rnt":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 757
    const/4 v1, 0x0

    .line 759
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isObjectLoaded()Z
    .locals 1

    .prologue
    .line 1843
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsObjectLoaded()Z

    move-result v0

    return v0
.end method

.method public isRecording()Z
    .locals 1

    .prologue
    .line 1439
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsRecording()Z

    move-result v0

    return v0
.end method

.method public isRedoable()Z
    .locals 1

    .prologue
    .line 2206
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_isRedoable()Z

    move-result v0

    return v0
.end method

.method public isRedoable(I)Z
    .locals 1
    .param p1, "userId"    # I

    .prologue
    .line 2254
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_isRedoable2(I)Z

    move-result v0

    return v0
.end method

.method public isTextOnly()Z
    .locals 1

    .prologue
    .line 1185
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsTextOnly()Z

    move-result v0

    return v0
.end method

.method public isUndoable()Z
    .locals 1

    .prologue
    .line 2184
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_isUndoable()Z

    move-result v0

    return v0
.end method

.method public isUndoable(I)Z
    .locals 1
    .param p1, "userId"    # I

    .prologue
    .line 2230
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_isUndoable2(I)Z

    move-result v0

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 2755
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsValid()Z

    move-result v0

    return v0
.end method

.method public loadObject()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1797
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_LoadObject()Z

    move-result v0

    .line 1798
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1799
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    .line 1805
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1808
    :cond_0
    return-void
.end method

.method public moveObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;IZ)V
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .param p2, "step"    # I
    .param p3, "ignoreInvisible"    # Z

    .prologue
    .line 891
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_MoveObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;IZ)Z

    move-result v0

    .line 892
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 893
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 895
    :cond_0
    return-void
.end method

.method public redo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2

    .prologue
    .line 2294
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_redo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2295
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2296
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2298
    :cond_0
    return-object v0
.end method

.method public redo(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2
    .param p1, "userId"    # I

    .prologue
    .line 2342
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_redo2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2343
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2344
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2346
    :cond_0
    return-object v0
.end method

.method public redoAll()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2

    .prologue
    .line 2392
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_redoAll()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2393
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2394
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2396
    :cond_0
    return-object v0
.end method

.method public redoAll(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2
    .param p1, "userId"    # I

    .prologue
    .line 2450
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_redoAll2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2451
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2452
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2454
    :cond_0
    return-object v0
.end method

.method public removeAllObject()V
    .locals 1

    .prologue
    .line 552
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveAllObject()Z

    move-result v0

    if-nez v0, :cond_0

    .line 553
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 555
    :cond_0
    return-void
.end method

.method public removeExtraDataByteArray(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1754
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveExtraDataByteArray(Ljava/lang/String;)Z

    move-result v0

    .line 1755
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1756
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1758
    :cond_0
    return-void
.end method

.method public removeExtraDataInt(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1726
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveExtraDataInt(Ljava/lang/String;)Z

    move-result v0

    .line 1727
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1728
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1730
    :cond_0
    return-void
.end method

.method public removeExtraDataString(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1712
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveExtraDataString(Ljava/lang/String;)Z

    move-result v0

    .line 1713
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1714
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1716
    :cond_0
    return-void
.end method

.method public removeExtraDataStringArray(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1740
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveExtraDataStringArray(Ljava/lang/String;)Z

    move-result v0

    .line 1741
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1742
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1744
    :cond_0
    return-void
.end method

.method public removeGeoTag()V
    .locals 4

    .prologue
    .line 1512
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveGeoTag()Z

    move-result v1

    .line 1513
    .local v1, "rnt":Z
    if-nez v1, :cond_0

    .line 1514
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1520
    .end local v1    # "rnt":Z
    :cond_0
    :goto_0
    return-void

    .line 1516
    :catch_0
    move-exception v0

    .line 1517
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 1518
    const-string v2, "SpenPageDoc"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeLayer(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 1915
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveLayer(I)Z

    move-result v0

    .line 1916
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1917
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1919
    :cond_0
    return-void
.end method

.method public removeObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 538
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 539
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 541
    :cond_0
    return-void
.end method

.method public removeSelectedObject()V
    .locals 1

    .prologue
    .line 567
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveSelectedObject()Z

    move-result v0

    if-nez v0, :cond_0

    .line 568
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 570
    :cond_0
    return-void
.end method

.method public removeTag(Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 1157
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveTag(Ljava/lang/String;)Z

    move-result v0

    .line 1158
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1159
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1161
    :cond_0
    return-void
.end method

.method public save()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1771
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_Save()Z

    move-result v0

    .line 1772
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1773
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    .line 1777
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1780
    :cond_0
    return-void
.end method

.method public selectObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 913
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SelectObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    .line 914
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 915
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 917
    :cond_0
    return-void
.end method

.method public selectObject(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 936
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SelectObject(Ljava/util/ArrayList;)Z

    move-result v0

    .line 937
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 938
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 940
    :cond_0
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 1331
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetBackgroundColor(I)Z

    move-result v0

    .line 1332
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1333
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1335
    :cond_0
    return-void
.end method

.method public setBackgroundImage(Ljava/lang/String;)V
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1262
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetBackgroundImage(Ljava/lang/String;)Z

    move-result v0

    .line 1263
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1264
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1266
    :cond_0
    return-void
.end method

.method public setBackgroundImageMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1302
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetBackgroundImageMode(I)Z

    move-result v0

    .line 1303
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1304
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1306
    :cond_0
    return-void
.end method

.method public setCurrentLayer(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 1933
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetCurrentLayer(I)Z

    move-result v0

    .line 1934
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1935
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1937
    :cond_0
    return-void
.end method

.method public setExtraDataByteArray(Ljava/lang/String;[B)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 1576
    if-nez p2, :cond_1

    .line 1577
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetExtraDataByteArray(Ljava/lang/String;[BI)Z

    move-result v0

    .line 1581
    .local v0, "rnt":Z
    :goto_0
    if-nez v0, :cond_0

    .line 1582
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1584
    :cond_0
    return-void

    .line 1579
    .end local v0    # "rnt":Z
    :cond_1
    array-length v1, p2

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetExtraDataByteArray(Ljava/lang/String;[BI)Z

    move-result v0

    .restart local v0    # "rnt":Z
    goto :goto_0
.end method

.method public setExtraDataInt(Ljava/lang/String;I)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 1598
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetExtraDataInt(Ljava/lang/String;I)Z

    move-result v0

    .line 1599
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1600
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1602
    :cond_0
    return-void
.end method

.method public setExtraDataString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1534
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetExtraDataString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 1535
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1536
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1538
    :cond_0
    return-void
.end method

.method public setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [Ljava/lang/String;

    .prologue
    .line 1553
    if-nez p2, :cond_1

    .line 1554
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z

    move-result v0

    .line 1558
    .local v0, "rnt":Z
    :goto_0
    if-nez v0, :cond_0

    .line 1559
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1561
    :cond_0
    return-void

    .line 1556
    .end local v0    # "rnt":Z
    :cond_1
    array-length v1, p2

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z

    move-result v0

    .restart local v0    # "rnt":Z
    goto :goto_0
.end method

.method public setGeoTag(DD)V
    .locals 3
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 1457
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetGeoTag(DD)Z

    move-result v0

    .line 1458
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1459
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1461
    :cond_0
    return-void
.end method

.method public setHistoryListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;

    .prologue
    .line 2542
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_setHistoryListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;)Z

    move-result v0

    .line 2543
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2544
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2546
    :cond_0
    return-void
.end method

.method public setHistoryManagerMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 2649
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_setHistoryManagerMode(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2650
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2652
    :cond_0
    return-void
.end method

.method public setHistoryTag()V
    .locals 1

    .prologue
    .line 2568
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_setHistoryTag()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2569
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2571
    :cond_0
    return-void
.end method

.method public setLayerEventForwardEnabled(IZ)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "enable"    # Z

    .prologue
    .line 2047
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_EnableLayerEventForward(IZ)Z

    move-result v0

    .line 2048
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2049
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2051
    :cond_0
    return-void
.end method

.method public setLayerName(ILjava/lang/String;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 2007
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetLayerName(ILjava/lang/String;)Z

    move-result v0

    .line 2008
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2009
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2011
    :cond_0
    return-void
.end method

.method public setObjectIndexMovedListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectIndexMovedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectIndexMovedListener;

    .prologue
    .line 2137
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetObjectIndexMovedListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectIndexMovedListener;)Z

    move-result v0

    .line 2138
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2139
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2141
    :cond_0
    return-void
.end method

.method public setObjectListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectListener;

    .prologue
    .line 2105
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetObjectListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectListener;)Z

    move-result v0

    .line 2106
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2107
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2109
    :cond_0
    return-void
.end method

.method public setObjectSelectedListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectSelectedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectSelectedListener;

    .prologue
    .line 2121
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetObjectSelectedListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectSelectedListener;)Z

    move-result v0

    .line 2122
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2123
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2125
    :cond_0
    return-void
.end method

.method public setTemplateUri(Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 1356
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetTemplateUri(Ljava/lang/String;)Z

    move-result v0

    .line 1357
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1358
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1360
    :cond_0
    return-void
.end method

.method public setUndoLimit(I)V
    .locals 0
    .param p1, "undoLimit"    # I

    .prologue
    .line 2473
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_setUndoLimit(I)V

    .line 2474
    return-void
.end method

.method public setUserIdForHistoryListener(I)V
    .locals 3
    .param p1, "userId"    # I

    .prologue
    .line 2676
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2677
    .local v1, "tempInteger":Ljava/lang/Integer;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2679
    .local v0, "tempArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2680
    const/4 v2, 0x1

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;

    .line 2681
    return-void
.end method

.method public setVolatileBackgroundImage(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1239
    if-eqz p1, :cond_0

    .line 1240
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1241
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "bitmap is recyled."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1244
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetVolatileBackgroundImage(Landroid/graphics/Bitmap;)Z

    move-result v0

    .line 1245
    .local v0, "rnt":Z
    if-nez v0, :cond_1

    .line 1246
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1248
    :cond_1
    return-void
.end method

.method public startHistoryGroup()V
    .locals 1

    .prologue
    .line 2614
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_startHistoryGroup()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2615
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2617
    :cond_0
    return-void
.end method

.method public startRecord()V
    .locals 2

    .prologue
    .line 1413
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_StartRecord()Z

    move-result v0

    .line 1414
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1415
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1417
    :cond_0
    return-void
.end method

.method public stopHistoryGroup()V
    .locals 1

    .prologue
    .line 2631
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_stopHistoryGroup()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2632
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2634
    :cond_0
    return-void
.end method

.method public stopRecord()V
    .locals 2

    .prologue
    .line 1425
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_StopRecord()Z

    move-result v0

    .line 1426
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1427
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1429
    :cond_0
    return-void
.end method

.method public undo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2

    .prologue
    .line 2272
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_undo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2273
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2274
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2276
    :cond_0
    return-object v0
.end method

.method public undo(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2
    .param p1, "userId"    # I

    .prologue
    .line 2318
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_undo2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2319
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2320
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2322
    :cond_0
    return-object v0
.end method

.method public undoAll()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2

    .prologue
    .line 2367
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_undoAll()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2368
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2369
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2371
    :cond_0
    return-object v0
.end method

.method public undoAll(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2
    .param p1, "userId"    # I

    .prologue
    .line 2420
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_undoAll2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2421
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2422
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2424
    :cond_0
    return-object v0
.end method

.method public undoToTag()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2

    .prologue
    .line 2596
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_undoToTag()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2597
    .local v0, "ret":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2598
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2601
    :cond_0
    return-object v0
.end method

.method public ungroupObject(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Z)V
    .locals 2
    .param p1, "group"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .param p2, "selectAfterUngroup"    # Z

    .prologue
    .line 1104
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_UngroupObject(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Z)Z

    move-result v0

    .line 1105
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1106
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1108
    :cond_0
    return-void
.end method

.method public ungroupSelectedObject(Z)V
    .locals 2
    .param p1, "selectAfterUngroup"    # Z

    .prologue
    .line 1125
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_UngroupSelectedObject(Z)Z

    move-result v0

    .line 1126
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1127
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1129
    :cond_0
    return-void
.end method

.method public unloadObject()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1829
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_UnloadObject()Z

    move-result v0

    .line 1830
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1831
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1833
    :cond_0
    return-void
.end method
