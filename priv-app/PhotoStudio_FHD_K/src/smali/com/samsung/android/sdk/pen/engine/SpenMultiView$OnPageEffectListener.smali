.class Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;
.super Ljava/lang/Object;
.source "SpenMultiView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenMultiView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnPageEffectListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V
    .locals 0

    .prologue
    .line 2885
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;)V
    .locals 0

    .prologue
    .line 2885
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 1

    .prologue
    .line 2903
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->invalidate()V

    .line 2905
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2906
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;->onFinish()V

    .line 2908
    :cond_0
    return-void
.end method

.method public onUpdate()V
    .locals 1

    .prologue
    .line 2898
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->invalidate()V

    .line 2899
    return-void
.end method

.method public onUpdateCanvasLayer(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2888
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas(Landroid/graphics/Canvas;Z)V
    invoke-static {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/Canvas;Z)V

    .line 2889
    return-void
.end method

.method public onUpdateCanvasLayer2(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2893
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas2(Landroid/graphics/Canvas;)V
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/Canvas;)V

    .line 2894
    return-void
.end method
