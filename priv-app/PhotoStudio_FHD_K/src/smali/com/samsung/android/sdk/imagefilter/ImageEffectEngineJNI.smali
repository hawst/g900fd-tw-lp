.class public Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;
.super Ljava/lang/Object;
.source "ImageEffectEngineJNI.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    const-string v0, "SPenImageFilterLibs"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native applyBluewash([I[IIII)V
.end method

.method public static native applyBlur([I[IIII)V
.end method

.method public static native applyBright([I[IIII)V
.end method

.method public static native applyCartoonize([I[IIII)V
.end method

.method public static native applyClassic([I[IIII)V
.end method

.method public static native applyColorSketch([I[IIII)V
.end method

.method public static native applyColorize([I[IIII)V
.end method

.method public static native applyDark([I[IIII)V
.end method

.method public static native applyDownlight([I[IIII)V
.end method

.method public static native applyFadedColor([I[IIII)V
.end method

.method public static native applyFusain([I[IIII)V
.end method

.method public static native applyGray([I[IIII)V
.end method

.method public static native applyMagicPen([I[IIII)V
.end method

.method public static native applyMosaic([I[IIII)V
.end method

.method public static native applyNegative([I[IIII)V
.end method

.method public static native applyNostalgia([I[IIII)V
.end method

.method public static native applyOilpaint([I[IIII)V
.end method

.method public static native applyOldPhoto([I[IIII)V
.end method

.method public static native applyPastelSketch([I[IIII)V
.end method

.method public static native applyPenSketch([I[IIII)V
.end method

.method public static native applyPencilColorSketch([I[IIII)V
.end method

.method public static native applyPencilPastelSketch([I[IIII)V
.end method

.method public static native applyPencilSketch([I[IIII)V
.end method

.method public static native applyPopArt([I[IIII)V
.end method

.method public static native applyPosterize([I[IIII)V
.end method

.method public static native applyRetro([I[IIII)V
.end method

.method public static native applySepia([I[IIII)V
.end method

.method public static native applySoftglow([I[IIII)V
.end method

.method public static native applySunshine([I[IIII)V
.end method

.method public static native applyVignette([I[IIII)V
.end method

.method public static native applyVintage([I[IIII)V
.end method

.method public static native applyVivid([I[IIII)V
.end method

.method public static native applyYellowglow([I[IIII)V
.end method

.method public static native setCheckSupportingModel([CI)V
.end method
