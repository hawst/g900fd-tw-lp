.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;
.super Ljava/lang/Object;
.source "SpenSettingRemoverLayout.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    .line 668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 9
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/high16 v5, 0x41200000    # 10.0f

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 683
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    aget-object v3, v3, v4

    if-eqz v3, :cond_0

    .line 684
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    aget-object v3, v3, v4

    add-int/lit8 v4, p2, 0x1

    int-to-float v4, v4

    mul-float/2addr v4, v5

    div-float/2addr v4, v5

    .line 685
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    mul-float/2addr v4, v5

    .line 684
    iput v4, v3, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 687
    :cond_0
    const/4 v0, 0x1

    .line 688
    .local v0, "currentType":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iput-boolean p3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 690
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ar"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "fa"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 691
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    add-int/lit8 v5, p2, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->convertToArabicNumber(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 697
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41f00000    # 30.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 698
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43160000    # 150.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    int-to-float v5, p2

    const/high16 v6, 0x41100000    # 9.0f

    div-float/2addr v5, v6

    mul-float/2addr v4, v5

    float-to-int v4, v4

    .line 697
    add-int v2, v3, v4

    .line 699
    .local v2, "seek_label_pos":I
    add-int/lit8 v3, p2, 0x1

    const/16 v4, 0xa

    if-lt v3, v4, :cond_2

    .line 700
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40800000    # 4.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 702
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setX(F)V

    .line 703
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40a00000    # 5.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setY(F)V

    .line 704
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v3, :cond_3

    .line 705
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    .line 706
    .local v1, "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    if-eqz v1, :cond_3

    .line 707
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    add-int/lit8 v4, p2, 0x1

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    mul-float/2addr v4, v5

    iput v4, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iput v4, v3, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 708
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v0, v3, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 709
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 714
    .end local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "\u0000"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 717
    if-eq v0, v8, :cond_5

    .line 718
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getMax()I

    move-result v4

    if-ne v3, v4, :cond_7

    .line 719
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 720
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 721
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 722
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-static {v3, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V

    .line 727
    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    if-nez v3, :cond_8

    .line 728
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 729
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 730
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 731
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-static {v3, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V

    .line 738
    :cond_5
    :goto_2
    return-void

    .line 693
    .end local v2    # "seek_label_pos":I
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    add-int/lit8 v4, p2, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 725
    .restart local v2    # "seek_label_pos":I
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v8}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_1

    .line 734
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v8}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_2
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 678
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 672
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 673
    return-void
.end method
