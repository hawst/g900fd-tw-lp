.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout2.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    .line 516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public deletePresetItem(I)V
    .locals 2
    .param p1, "presetItemIndex"    # I

    .prologue
    .line 521
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 522
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 523
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->close()V

    .line 524
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 525
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviousSelectedPresetIndex:I

    if-ne v0, p1, :cond_2

    .line 530
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    const/4 v1, -0x1

    iput v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCurrentSeleted:I

    .line 537
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->notifyDataSetChanged()V

    .line 539
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetDisplay()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    .line 542
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;

    if-eqz v0, :cond_1

    .line 544
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;->onDeleted(I)V

    .line 547
    :cond_1
    return-void

    .line 531
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviousSelectedPresetIndex:I

    if-le v0, p1, :cond_3

    .line 532
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviousSelectedPresetIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviousSelectedPresetIndex:I

    .line 533
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviousSelectedPresetIndex:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCurrentSeleted:I

    goto :goto_0

    .line 535
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviousSelectedPresetIndex:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCurrentSeleted:I

    goto :goto_0
.end method

.method public selectPresetItem(I)V
    .locals 7
    .param p1, "presetItemIndex"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 552
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_2

    .line 593
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v3, :cond_0

    .line 594
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v1

    .line 595
    .local v1, "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    if-eqz v1, :cond_0

    .line 596
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v4, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v3, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 597
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v4, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v3

    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v3, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 598
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v4, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getColor()I

    move-result v3

    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v3, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 599
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v4, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    .line 600
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v3

    .line 599
    iput-object v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v3, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 601
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 602
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 603
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setPenType(Ljava/lang/String;)V

    .line 604
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeSize(F)V

    .line 605
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeColor(I)V

    .line 606
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeAdvancedSetting(Ljava/lang/String;)V

    .line 607
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->invalidate()V

    .line 610
    .end local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;

    if-eqz v3, :cond_1

    .line 611
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;

    invoke-interface {v3, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;->onSelected(I)V

    .line 614
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->playScrollAnimationChoosePen()V
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    .line 615
    return-void

    .line 553
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 554
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setSelected(Z)V

    .line 555
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v4

    .line 556
    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 557
    move v2, v0

    .line 558
    .local v2, "penNameIdx":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isBeautifyPen(Ljava/lang/String;)Z
    invoke-static {v4, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 559
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    const-string v4, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v2

    .line 563
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iput p1, v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCurrentSeleted:I

    .line 564
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iput p1, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviousSelectedPresetIndex:I

    .line 565
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->notifyDataSetChanged()V

    .line 566
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetDisplay()V
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    .line 571
    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 572
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-static {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Z)V

    .line 573
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 587
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setSelected(Z)V

    .line 552
    .end local v2    # "penNameIdx":I
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 575
    .restart local v2    # "penNameIdx":I
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 576
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-static {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Z)V

    .line 577
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public setScrollPresetGridToIndex(I)V
    .locals 1
    .param p1, "paramInt"    # I

    .prologue
    .line 619
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->smoothScrollToPosition(I)V

    .line 620
    return-void
.end method
