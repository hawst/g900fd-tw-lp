.class Lcom/samsung/android/sdk/pen/engine/SpenInView;
.super Ljava/lang/Object;
.source "SpenInView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$BitmapInfo;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$SetPageDocHandler;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$TouchUpHandler;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateHandler;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static LASTEST_LIB_PATH:Ljava/lang/String; = null

.field private static final NATIVE_COMMAND_CAPTUREPAGE_TRANSPARENT:I = 0x6

.field private static final NATIVE_COMMAND_CROP:I = 0x2

.field private static final NATIVE_COMMAND_PENBUTTON_SELECTION_ENABLE:I = 0x5

.field private static final NATIVE_COMMAND_REQUEST_PAGEDOC:I = 0x4

.field private static final NATIVE_COMMAND_STRETCH:I = 0x1

.field private static final NATIVE_COMMAND_TEXT_BOX_SET_DISPLAY_INFO:I = 0x3

.field public static final PAGE_TRANSITION_EFFECT_LEFT:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_RIGHT:I = 0x1

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SHADOW:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SLIDE:I = 0x1

.field public static final REPLAY_STATE_PAUSED:I = 0x2

.field public static final REPLAY_STATE_PLAYING:I = 0x1

.field public static final REPLAY_STATE_STOPPED:I = 0x0

.field private static final STROKE_FRAME_KEY:Ljava/lang/String; = "STROKE_FRAME"

.field private static final STROKE_FRAME_RETAKE:I = 0x2

.field private static final STROKE_FRAME_TAKE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SpenInView"

.field private static final TEXT_OBJECT_DEFAULT_SIZE_FONT:F = 36.0f

.field private static final penNameBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Brush"

.field private static final penNameChineseBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

.field private static final penNameFountainPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.FountainPen"

.field private static final penNameInkPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.InkPen"

.field private static final penNameMagicPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.MagicPen"

.field private static final penNameMarker:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Marker"

.field private static final penNameObliquePen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

.field private static final penNamePencil:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Pencil"


# instance fields
.field private MIN_STROKE_LENGTH:F

.field private activePen:I

.field private bIsSupport:Z

.field private isEraserCursor:Z

.field private isSkipTouch:Z

.field private isSoundEnabled:Z

.field private mAntiAliasPaint:Landroid/graphics/Paint;

.field private mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private mBlackPaint:Landroid/graphics/Paint;

.field private mCanvasLayer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mCirclePaint:Landroid/graphics/Paint;

.field private mCirclePoint:Landroid/graphics/PointF;

.field private mCircleRadius:F

.field private mClippingPath:Landroid/graphics/Path;

.field private mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

.field private mContext:Landroid/content/Context;

.field private mContextMenu:Landroid/view/ContextMenu;

.field private mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

.field private mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

.field private mDebugPaint:Landroid/graphics/Paint;

.field private mDeltaX:F

.field private mDeltaY:F

.field private mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

.field private mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

.field private mDottedLineEnable:Z

.field private mDottedLineHalfWidth:F

.field private mDottedLineIntervalHeight:I

.field private mDottedLinePaint:Landroid/graphics/Paint;

.field private mDoubleTapZoomable:Z

.field private mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

.field private mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

.field private mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

.field private mFloatingLayer:Landroid/graphics/Bitmap;

.field private mFramebufferHeight:I

.field private mFramebufferWidth:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

.field private mHighlightInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mHighlightPaint:Landroid/graphics/Paint;

.field private mHoverDrawable:Landroid/graphics/drawable/Drawable;

.field private mHoverEnable:Z

.field private mHoverIconID:I

.field private mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

.field private mHoverPoint:Landroid/graphics/Point;

.field private mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

.field private mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

.field private mIndexBrush:I

.field private mIndexEraser:I

.field private mIndexMarker:I

.field private mIndexPencil:I

.field private mIs64:Z

.field private mIsCancelFling:Z

.field private mIsControlSelect:Z

.field private mIsHyperText:Z

.field private mIsLongPressEnable:Z

.field private mIsPenButtonSelectionEnabled:Z

.field private mIsSmartHorizontal:Z

.field private mIsSmartScale:Z

.field private mIsSmartVertical:Z

.field private mIsStretch:Z

.field private mIsStrokeDrawing:Z

.field private mIsToolTip:Z

.field private mIsTouchPre:Z

.field private mLayoutExceptSIP:I

.field private mLayoutHeight:I

.field private mLayoutWidth:I

.field private mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

.field private mMagicPenEnabled:Z

.field private mMaxDeltaX:F

.field private mMaxDeltaY:F

.field private mMeasure:Landroid/graphics/PathMeasure;

.field private mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

.field private mOldX:F

.field private mOldY:F

.field private mOnePT:F

.field private mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

.field private mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

.field private mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

.field private mParentLayout:Landroid/view/ViewGroup;

.field private mPath:Landroid/graphics/Path;

.field private mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

.field private mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

.field private mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

.field private mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

.field private mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

.field private mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private mRatio:F

.field private mRatioBitmapHeight:I

.field private mRatioBitmapWidth:I

.field private mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

.field private mRemoverRadius:F

.field private mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field private mRemoverToastMessage:Landroid/widget/Toast;

.field private mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

.field private mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

.field private mRtoBmpItstScrHeight:I

.field private mRtoBmpItstScrWidth:I

.field private mScreenFB:Landroid/graphics/Bitmap;

.field private mScreenHeight:I

.field private mScreenHeightExceptSIP:I

.field private mScreenStartX:I

.field private mScreenStartY:I

.field private mScreenWidth:I

.field private mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

.field private mSdkResources:Landroid/content/res/Resources;

.field private mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

.field private mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

.field private mSetPageDocHandler:Landroid/os/Handler;

.field private mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

.field private mSmps:Lcom/samsung/audio/SmpsManager;

.field private mSrcPaint:Landroid/graphics/Paint;

.field private mStretchHeight:I

.field private mStretchWidth:I

.field private mStretchXRatio:F

.field private mStretchYRatio:F

.field private mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

.field private mStrokeFrameType:I

.field private mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

.field private mTextPaint:Landroid/graphics/Paint;

.field private mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

.field private mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

.field private mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

.field private mTouchCount:I

.field private mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private mTouchProcessingTime:J

.field private mTouchUpHandler:Landroid/os/Handler;

.field private mTransactionClosingControl:Z

.field private mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

.field private mUpdateHandler:Landroid/os/Handler;

.field private mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

.field private mUseC2D:Z

.field private mView:Landroid/view/View;

.field private mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

.field private mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

.field private mZoomable:Z

.field private nativeCanvas:J

.field private removerTouchX:F

.field private removerTouchY:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    const-string v0, "/system/vendor/lib/libC2D2.so"

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->LASTEST_LIB_PATH:Ljava/lang/String;

    .line 356
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;Z)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "updateCanvasListener"    # Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;
    .param p3, "isUseC2D"    # Z

    .prologue
    const/4 v9, -0x1

    const/4 v2, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContextMenu:Landroid/view/ContextMenu;

    .line 105
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutHeight:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutExceptSIP:I

    .line 106
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    .line 107
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    .line 108
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    .line 109
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    .line 110
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    .line 111
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    .line 112
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    .line 113
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    .line 114
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 116
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    .line 117
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    .line 118
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    .line 119
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    .line 121
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    .line 122
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 123
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .line 124
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    .line 125
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 126
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .line 127
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .line 128
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .line 129
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 130
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 131
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    .line 132
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    .line 133
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 134
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    .line 135
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    .line 136
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    .line 137
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    .line 138
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 139
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 140
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 141
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 142
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 143
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    .line 144
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    .line 145
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    .line 147
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 148
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 149
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 150
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .line 151
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 152
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    .line 157
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    .line 158
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 159
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 160
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    .line 161
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 163
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 164
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 165
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnable:Z

    .line 166
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverIconID:I

    .line 167
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 168
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    .line 169
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    .line 171
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    .line 172
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsLongPressEnable:Z

    .line 173
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    .line 175
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    .line 176
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    .line 177
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    .line 178
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    .line 179
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    .line 180
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    .line 181
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 182
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    .line 183
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    .line 184
    new-instance v4, Landroid/graphics/PointF;

    const/high16 v5, -0x3d380000    # -100.0f

    const/high16 v6, -0x3d380000    # -100.0f

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    .line 185
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    .line 186
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    .line 189
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    .line 190
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    .line 191
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    .line 192
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineHalfWidth:F

    .line 194
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomable:Z

    .line 195
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDoubleTapZoomable:Z

    .line 196
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartScale:Z

    .line 197
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartHorizontal:Z

    .line 198
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartVertical:Z

    .line 199
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsHyperText:Z

    .line 200
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    .line 201
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTransactionClosingControl:Z

    .line 205
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsCancelFling:Z

    .line 207
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    .line 208
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    .line 216
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrameType:I

    .line 218
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    .line 219
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removerTouchX:F

    .line 220
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removerTouchY:F

    .line 222
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    .line 224
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    .line 226
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    .line 227
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchHeight:I

    .line 228
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    .line 236
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

    .line 237
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    .line 238
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsPenButtonSelectionEnabled:Z

    .line 242
    const/high16 v4, 0x41700000    # 15.0f

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->MIN_STROKE_LENGTH:F

    .line 243
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOnePT:F

    .line 246
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 248
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldX:F

    .line 249
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldY:F

    .line 252
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mClippingPath:Landroid/graphics/Path;

    .line 255
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    .line 256
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    .line 257
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 258
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexPencil:I

    .line 259
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexMarker:I

    .line 260
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexBrush:I

    .line 261
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexEraser:I

    .line 262
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    .line 263
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    .line 264
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTouchPre:Z

    .line 358
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    .line 5816
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$TouchUpHandler;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$TouchUpHandler;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    .line 5842
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$SetPageDocHandler;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SetPageDocHandler;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    .line 5863
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateHandler;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateHandler;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateHandler:Landroid/os/Handler;

    .line 384
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    .line 385
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v4

    const/16 v5, 0x20

    if-ne v4, v5, :cond_0

    move v2, v3

    :cond_0
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    .line 386
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_init()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    .line 387
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    .line 388
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->construct()V

    .line 390
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v2, :cond_1

    .line 391
    const-string v2, "/system/vendor/lib64/libC2D2.so"

    sput-object v2, Lcom/samsung/android/sdk/pen/engine/SpenInView;->LASTEST_LIB_PATH:Ljava/lang/String;

    .line 393
    :cond_1
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenInView;->LASTEST_LIB_PATH:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 394
    .local v1, "libFilePath":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 395
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    .line 409
    :cond_2
    :goto_0
    return-void

    .line 396
    :cond_3
    if-eqz p3, :cond_2

    .line 398
    :try_start_0
    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_init_c2dJNI()V

    .line 399
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    .line 400
    const-string v2, "c2d"

    const-string v4, "c2d blit init"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 401
    :catch_0
    move-exception v0

    .line 402
    .local v0, "error":Ljava/lang/UnsatisfiedLinkError;
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    .line 403
    const-string v2, "c2d"

    const-string v3, "c2d blit init fail, lib loading error"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 404
    .end local v0    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 405
    .local v0, "error":Ljava/lang/NoClassDefFoundError;
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    .line 406
    const-string v2, "c2d"

    const-string v3, "c2d blit init fail, lib loading error"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private ExtendRectFromRectF(Landroid/graphics/Rect;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dst"    # Landroid/graphics/Rect;
    .param p2, "src"    # Landroid/graphics/RectF;

    .prologue
    .line 857
    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 858
    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 859
    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 860
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 861
    return-void
.end method

.method private MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;
    .locals 6
    .param p1, "src"    # Landroid/graphics/RectF;

    .prologue
    .line 852
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    iget v2, p1, Landroid/graphics/RectF;->top:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, p1, Landroid/graphics/RectF;->right:F

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 853
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 852
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private Native_cancelStroke(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6434
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6435
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_cancelStroke(J)V

    .line 6440
    :goto_0
    return-void

    .line 6438
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_cancelStroke(I)V

    goto :goto_0
.end method

.method private Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "command"    # I
    .param p5, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6481
    .local p4, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6482
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 6485
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "view"    # Lcom/samsung/android/sdk/pen/engine/SpenInView;
    .param p5, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 5964
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 5965
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z

    move-result v0

    .line 5968
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_construct(ILandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_drawObjectList(JLandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p5, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .line 6453
    .local p4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6454
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_drawObjectList(JLandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z

    move-result v0

    .line 6457
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_drawObjectList(ILandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_enablePenCurve(JZ)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "curve"    # Z

    .prologue
    .line 6253
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6254
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_enablePenCurve(JZ)V

    .line 6259
    :goto_0
    return-void

    .line 6257
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_enablePenCurve(IZ)V

    goto :goto_0
.end method

.method private Native_enableZoom(JZ)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "mode"    # Z

    .prologue
    .line 6073
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6074
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_enableZoom(JZ)V

    .line 6079
    :goto_0
    return-void

    .line 6077
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_enableZoom(IZ)V

    goto :goto_0
.end method

.method private Native_finalize(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 5956
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 5957
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_finalize(J)V

    .line 5961
    :goto_0
    return-void

    .line 5959
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_finalize(I)V

    goto :goto_0
.end method

.method private Native_getAdvancedSetting(J)Ljava/lang/String;
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6298
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6299
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getAdvancedSetting(J)Ljava/lang/String;

    move-result-object v0

    .line 6302
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getAdvancedSetting(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_getEraserSize(J)F
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6280
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6281
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getEraserSize(J)F

    move-result v0

    .line 6284
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getEraserSize(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getMaxZoomRatio(J)F
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6118
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6119
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getMaxZoomRatio(J)F

    move-result v0

    .line 6122
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getMaxZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getMinZoomRatio(J)F
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6136
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6137
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getMinZoomRatio(J)F

    move-result v0

    .line 6140
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getMinZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getPan(JLandroid/graphics/PointF;)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 6154
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6155
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPan(JLandroid/graphics/PointF;)V

    .line 6160
    :goto_0
    return-void

    .line 6158
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPan(ILandroid/graphics/PointF;)V

    goto :goto_0
.end method

.method private Native_getPenColor(J)I
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6226
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6227
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPenColor(J)I

    move-result v0

    .line 6230
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPenColor(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getPenSize(J)F
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6244
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6245
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPenSize(J)F

    move-result v0

    .line 6248
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPenSize(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getPenStyle(J)Ljava/lang/String;
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6208
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6209
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPenStyle(J)Ljava/lang/String;

    move-result-object v0

    .line 6212
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPenStyle(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_getReplayState(J)I
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6362
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6363
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getReplayState(J)I

    move-result v0

    .line 6366
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getReplayState(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getTemporaryStroke(JLjava/util/ArrayList;)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 6407
    .local p3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6408
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getTemporaryStroke(JLjava/util/ArrayList;)V

    .line 6413
    :goto_0
    return-void

    .line 6411
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getTemporaryStroke(ILjava/util/ArrayList;)V

    goto :goto_0
.end method

.method private Native_getToolTypeAction(JI)I
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "toolType"    # I

    .prologue
    .line 6064
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6065
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getToolTypeAction(JI)I

    move-result v0

    .line 6068
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getToolTypeAction(II)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getZoomRatio(J)F
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6100
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6101
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getZoomRatio(J)F

    move-result v0

    .line 6104
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_inVisibleUpdate(JIZZ)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "runtimeHandle"    # I
    .param p4, "isVisible"    # Z
    .param p5, "isClosed"    # Z

    .prologue
    .line 6037
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6038
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_inVisibleUpdate(JIZZ)V

    .line 6043
    :goto_0
    return-void

    .line 6041
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_inVisibleUpdate(IIZZ)V

    goto :goto_0
.end method

.method private Native_init()J
    .locals 2

    .prologue
    .line 5947
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 5948
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_init_64()J

    move-result-wide v0

    .line 5951
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_init()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private Native_isPenCurve(J)Z
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6262
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6263
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_isPenCurve(J)Z

    move-result v0

    .line 6266
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_isPenCurve(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_isZoomable(J)Z
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6082
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6083
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_isZoomable(J)Z

    move-result v0

    .line 6086
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_isZoomable(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onHover(JLandroid/view/MotionEvent;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "toolType"    # I

    .prologue
    .line 6172
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6173
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onHover(JLandroid/view/MotionEvent;I)Z

    move-result v0

    .line 6176
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onHover(ILandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onLongPress(JLandroid/view/MotionEvent;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "toolType"    # I

    .prologue
    .line 6190
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6191
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onLongPress(JLandroid/view/MotionEvent;I)Z

    move-result v0

    .line 6194
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onLongPress(ILandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onSingleTapUp(JLandroid/view/MotionEvent;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "toolType"    # I

    .prologue
    .line 6181
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6182
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onSingleTapUp(JLandroid/view/MotionEvent;I)Z

    move-result v0

    .line 6185
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onSingleTapUp(ILandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onTouch(JLandroid/view/MotionEvent;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "toolType"    # I

    .prologue
    .line 6163
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6164
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onTouch(JLandroid/view/MotionEvent;I)Z

    move-result v0

    .line 6167
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onTouch(ILandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_pauseReplay(J)Z
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6343
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6344
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_pauseReplay(J)Z

    move-result v0

    .line 6347
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_pauseReplay(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_removeCanvasBitmap(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6009
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6010
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_removeCanvasBitmap(J)V

    .line 6015
    :goto_0
    return-void

    .line 6013
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_removeCanvasBitmap(I)V

    goto :goto_0
.end method

.method private Native_resumeReplay(J)Z
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6352
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6353
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_resumeReplay(J)Z

    move-result v0

    .line 6356
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_resumeReplay(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setAdvancedSetting(JLjava/lang/String;)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "advancedSetting"    # Ljava/lang/String;

    .prologue
    .line 6289
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6290
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setAdvancedSetting(JLjava/lang/String;)V

    .line 6295
    :goto_0
    return-void

    .line 6293
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setAdvancedSetting(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private Native_setBitmap(JLandroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 5991
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 5992
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setBitmap(JLandroid/graphics/Bitmap;)V

    .line 5997
    :goto_0
    return-void

    .line 5995
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private Native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "layerId"    # I
    .param p4, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 6000
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6001
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V

    .line 6006
    :goto_0
    return-void

    .line 6004
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setCanvasBitmap(IILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private Native_setEraserSize(JF)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "width"    # F

    .prologue
    .line 6271
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6272
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setEraserSize(JF)V

    .line 6277
    :goto_0
    return-void

    .line 6275
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setEraserSize(IF)V

    goto :goto_0
.end method

.method private Native_setEraserType(JI)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "type"    # I

    .prologue
    .line 6425
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6426
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setEraserType(JI)V

    .line 6431
    :goto_0
    return-void

    .line 6429
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setEraserType(II)V

    goto :goto_0
.end method

.method private Native_setHyperTextViewEnabled(JZ)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "enabled"    # Z

    .prologue
    .line 6443
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6444
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setHyperTextViewEnabled(JZ)V

    .line 6449
    :goto_0
    return-void

    .line 6447
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setHyperTextViewEnabled(IZ)V

    goto :goto_0
.end method

.method private Native_setMaxZoomRatio(JF)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "ratio"    # F

    .prologue
    .line 6109
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6110
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setMaxZoomRatio(JF)Z

    move-result v0

    .line 6113
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setMaxZoomRatio(IF)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setMinZoomRatio(JF)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "ratio"    # F

    .prologue
    .line 6127
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6128
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setMinZoomRatio(JF)Z

    move-result v0

    .line 6131
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setMinZoomRatio(IF)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 1
    .param p1, "nativeCanvaTouchs"    # J
    .param p3, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p4, "isUpdate"    # Z

    .prologue
    .line 6046
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6047
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    .line 6050
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setPan(JFFZ)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "deltaX"    # F
    .param p4, "deltaY"    # F
    .param p5, "isUpdate"    # Z

    .prologue
    .line 6145
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6146
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPan(JFFZ)V

    .line 6151
    :goto_0
    return-void

    .line 6149
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPan(IFFZ)V

    goto :goto_0
.end method

.method private Native_setPenColor(JI)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "color"    # I

    .prologue
    .line 6217
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6218
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenColor(JI)V

    .line 6223
    :goto_0
    return-void

    .line 6221
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenColor(II)V

    goto :goto_0
.end method

.method private Native_setPenSize(JF)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "width"    # F

    .prologue
    .line 6235
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6236
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenSize(JF)V

    .line 6241
    :goto_0
    return-void

    .line 6239
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenSize(IF)V

    goto :goto_0
.end method

.method private Native_setPenStyle(JLjava/lang/String;)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "style"    # Ljava/lang/String;

    .prologue
    .line 6199
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6200
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenStyle(JLjava/lang/String;)Z

    move-result v0

    .line 6203
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenStyle(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setRemoverSize(JF)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "width"    # F

    .prologue
    .line 6471
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6472
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setRemoverSize(JF)V

    .line 6477
    :goto_0
    return-void

    .line 6475
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setRemoverSize(IF)V

    goto :goto_0
.end method

.method private Native_setRemoverType(JI)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "type"    # I

    .prologue
    .line 6462
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6463
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setRemoverType(JI)V

    .line 6468
    :goto_0
    return-void

    .line 6466
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setRemoverType(II)V

    goto :goto_0
.end method

.method private Native_setReplayPosition(JI)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "index"    # I

    .prologue
    .line 6380
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6381
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setReplayPosition(JI)Z

    move-result v0

    .line 6384
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setReplayPosition(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setReplaySpeed(JI)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "speed"    # I

    .prologue
    .line 6371
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6372
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setReplaySpeed(JI)Z

    move-result v0

    .line 6375
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setReplaySpeed(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setScreenFrameBuffer(JLandroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 5982
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 5983
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setScreenFrameBuffer(JLandroid/graphics/Bitmap;)V

    .line 5988
    :goto_0
    return-void

    .line 5986
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setScreenFrameBuffer(ILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private Native_setScreenSize(JIII)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "heightExceptSIP"    # I

    .prologue
    .line 5973
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 5974
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setScreenSize(JIII)V

    .line 5979
    :goto_0
    return-void

    .line 5977
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setScreenSize(IIII)V

    goto :goto_0
.end method

.method private Native_setSelectionType(JI)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "type"    # I

    .prologue
    .line 6416
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6417
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setSelectionType(JI)V

    .line 6422
    :goto_0
    return-void

    .line 6420
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setSelectionType(II)V

    goto :goto_0
.end method

.method private Native_setToolTypeAction(JII)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "toolType"    # I
    .param p4, "action"    # I

    .prologue
    .line 6055
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6056
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setToolTypeAction(JII)V

    .line 6061
    :goto_0
    return-void

    .line 6059
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setToolTypeAction(III)V

    goto :goto_0
.end method

.method private Native_setZoom(JFFF)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "centerX"    # F
    .param p4, "centerY"    # F
    .param p5, "ratio"    # F

    .prologue
    .line 6091
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6092
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setZoom(JFFF)V

    .line 6097
    :goto_0
    return-void

    .line 6095
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setZoom(IFFF)V

    goto :goto_0
.end method

.method private Native_startReplay(J)Z
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6325
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6326
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_startReplay(J)Z

    move-result v0

    .line 6329
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_startReplay(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_startTemporaryStroke(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6389
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6390
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_startTemporaryStroke(J)V

    .line 6395
    :goto_0
    return-void

    .line 6393
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_startTemporaryStroke(I)V

    goto :goto_0
.end method

.method private Native_stopReplay(J)Z
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6334
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6335
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_stopReplay(J)Z

    move-result v0

    .line 6338
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_stopReplay(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_stopTemporaryStroke(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6398
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6399
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_stopTemporaryStroke(J)V

    .line 6404
    :goto_0
    return-void

    .line 6402
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_stopTemporaryStroke(I)V

    goto :goto_0
.end method

.method private Native_update(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6018
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6019
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_update(J)V

    .line 6024
    :goto_0
    return-void

    .line 6022
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_update(I)V

    goto :goto_0
.end method

.method private Native_updateAllScreenFrameBuffer(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6027
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6028
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateAllScreenFrameBuffer(J)V

    .line 6033
    :goto_0
    return-void

    .line 6031
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateAllScreenFrameBuffer(I)V

    goto :goto_0
.end method

.method private Native_updateRedo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .param p4, "length"    # I

    .prologue
    .line 6316
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6317
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateRedo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    .line 6320
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateRedo(I[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_updateUndo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .param p4, "length"    # I

    .prologue
    .line 6307
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6308
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateUndo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    .line 6311
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateUndo(I[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    goto :goto_0
.end method

.method private absoluteCoordinate(Landroid/graphics/Rect;FFFF)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/Rect;
    .param p2, "l"    # F
    .param p3, "t"    # F
    .param p4, "r"    # F
    .param p5, "b"    # F

    .prologue
    .line 834
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float v0, p2, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 835
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float v0, p4, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 836
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float v0, p3, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 837
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float v0, p5, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 838
    return-void
.end method

.method private absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dstRectF"    # Landroid/graphics/RectF;
    .param p2, "srcRectF"    # Landroid/graphics/RectF;

    .prologue
    .line 827
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 828
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 829
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 830
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 831
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    return v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 6153
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getPan(JLandroid/graphics/PointF;)V

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenInView;JFFZ)V
    .locals 1

    .prologue
    .line 6144
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPan(JFFZ)V

    return-void
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    return v0
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    return-object v0
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/view/MotionEvent;I)Z
    .locals 1

    .prologue
    .line 6180
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onSingleTapUp(JLandroid/view/MotionEvent;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    return v0
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsLongPressEnable:Z

    return v0
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 237
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PathMeasure;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMeasure:Landroid/graphics/PathMeasure;

    return-object v0
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    return-object v0
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->MIN_STROKE_LENGTH:F

    return v0
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    return-object v0
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 222
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    return v0
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/engine/SpenInView;Z)V
    .locals 0

    .prologue
    .line 222
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    return-void
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    return-void
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V
    .locals 0

    .prologue
    .line 186
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    return-void
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/engine/SpenInView;Z)V
    .locals 0

    .prologue
    .line 237
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    return-void
.end method

.method static synthetic access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    return-object v0
.end method

.method static synthetic access$31(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartScale:Z

    return v0
.end method

.method static synthetic access$32(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartHorizontal:Z

    return v0
.end method

.method static synthetic access$33(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartVertical:Z

    return v0
.end method

.method static synthetic access$34(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    return v0
.end method

.method static synthetic access$35(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    return v0
.end method

.method static synthetic access$36(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/view/MotionEvent;I)Z
    .locals 1

    .prologue
    .line 6189
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onLongPress(JLandroid/view/MotionEvent;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$37(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 205
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsCancelFling:Z

    return v0
.end method

.method static synthetic access$38(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDoubleTapZoomable:Z

    return v0
.end method

.method static synthetic access$39(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    return-object v0
.end method

.method static synthetic access$40(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    return v0
.end method

.method static synthetic access$41(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    return v0
.end method

.method static synthetic access$42(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;Z)V
    .locals 0

    .prologue
    .line 1261
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    return-void
.end method

.method static synthetic access$43(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V
    .locals 0

    .prologue
    .line 1155
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    return-void
.end method

.method static synthetic access$44(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 1137
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer2(Landroid/graphics/Canvas;)V

    return-void
.end method

.method static synthetic access$45(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 5842
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$46(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    return-object v0
.end method

.method static synthetic access$47(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 2023
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateNotepad()V

    return-void
.end method

.method static synthetic access$48(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    return-object v0
.end method

.method static synthetic access$49(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J
    .locals 2

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    return-wide v0
.end method

.method static synthetic access$50(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    return-object v0
.end method

.method static synthetic access$51(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    return-object v0
.end method

.method static synthetic access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    return v0
.end method

.method static synthetic access$53(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    return v0
.end method

.method static synthetic access$54(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    return-object v0
.end method

.method static synthetic access$55(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/view/MotionEvent;I)Z
    .locals 1

    .prologue
    .line 6162
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onTouch(JLandroid/view/MotionEvent;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$56(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    return-object v0
.end method

.method static synthetic access$57(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    return-object v0
.end method

.method static synthetic access$58(Lcom/samsung/android/sdk/pen/engine/SpenInView;JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 6479
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$59(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenInView;JIZZ)V
    .locals 1

    .prologue
    .line 6035
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_inVisibleUpdate(JIZZ)V

    return-void
.end method

.method static synthetic access$60(Lcom/samsung/android/sdk/pen/engine/SpenInView;I)V
    .locals 0

    .prologue
    .line 216
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrameType:I

    return-void
.end method

.method static synthetic access$61(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    return-object v0
.end method

.method static synthetic access$62(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 1903
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->raiseOnKeyDown()V

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    return v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    return v0
.end method

.method private applyTextSetting(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 14
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 1591
    const/4 v3, 0x0

    .line 1592
    .local v3, "endPos":I
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 1593
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v3

    .line 1596
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getSpan()Ljava/util/ArrayList;

    move-result-object v10

    .line 1597
    .local v10, "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-nez v10, :cond_2

    .line 1598
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    if-nez v12, :cond_1

    .line 1599
    new-instance v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v12}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iput-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 1601
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    const/high16 v13, 0x42100000    # 36.0f

    iput v13, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 1604
    :cond_1
    new-instance v10, Ljava/util/ArrayList;

    .end local v10    # "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1606
    .restart local v10    # "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    new-instance v6, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    invoke-direct {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;-><init>()V

    .line 1607
    .local v6, "fsSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;
    const/4 v12, 0x0

    iput v12, v6, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->startPos:I

    .line 1608
    iput v3, v6, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->endPos:I

    .line 1609
    const/high16 v12, 0x42100000    # 36.0f

    iput v12, v6, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    .line 1611
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1613
    new-instance v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;-><init>()V

    .line 1614
    .local v4, "fcSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;
    const/4 v12, 0x0

    iput v12, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->startPos:I

    .line 1615
    iput v3, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->endPos:I

    .line 1617
    const/high16 v12, -0x1000000

    iput v12, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    .line 1619
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1621
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;-><init>()V

    .line 1622
    .local v1, "bsSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;
    const/4 v12, 0x0

    iput v12, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->startPos:I

    .line 1623
    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->endPos:I

    .line 1626
    const/4 v12, 0x0

    iput-boolean v12, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->isBold:Z

    .line 1628
    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1630
    new-instance v7, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    invoke-direct {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;-><init>()V

    .line 1631
    .local v7, "isSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;
    const/4 v12, 0x0

    iput v12, v7, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->startPos:I

    .line 1632
    iput v3, v7, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->endPos:I

    .line 1635
    const/4 v12, 0x0

    iput-boolean v12, v7, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->isItalic:Z

    .line 1637
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1639
    new-instance v11, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    invoke-direct {v11}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;-><init>()V

    .line 1640
    .local v11, "usSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;
    const/4 v12, 0x0

    iput v12, v11, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->startPos:I

    .line 1641
    iput v3, v11, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->endPos:I

    .line 1644
    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->isUnderline:Z

    .line 1646
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1648
    new-instance v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    invoke-direct {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;-><init>()V

    .line 1649
    .local v5, "fnSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;
    const/4 v12, 0x0

    iput v12, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->startPos:I

    .line 1650
    iput v3, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->endPos:I

    .line 1652
    const-string v12, "Roboto-Regular"

    iput-object v12, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    .line 1654
    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1656
    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;-><init>()V

    .line 1658
    .local v2, "direction":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;
    const/4 v12, 0x0

    iput v12, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->textDirection:I

    .line 1659
    const/4 v12, 0x0

    iput v12, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->startPos:I

    .line 1660
    iput v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->endPos:I

    .line 1662
    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1664
    invoke-virtual {p1, v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setSpan(Ljava/util/ArrayList;)V

    .line 1667
    .end local v1    # "bsSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;
    .end local v2    # "direction":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;
    .end local v4    # "fcSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;
    .end local v5    # "fnSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;
    .end local v6    # "fsSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;
    .end local v7    # "isSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;
    .end local v11    # "usSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v9

    .line 1668
    .local v9, "paragraph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    if-nez v9, :cond_4

    .line 1669
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    if-nez v12, :cond_3

    .line 1670
    new-instance v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v12}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iput-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 1673
    :cond_3
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "paragraph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1675
    .restart local v9    # "paragraph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;-><init>()V

    .line 1676
    .local v0, "align":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v12, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    int-to-char v12, v12

    iput v12, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    .line 1677
    const/4 v12, 0x0

    iput v12, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->startPos:I

    .line 1678
    iput v3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->endPos:I

    .line 1680
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1690
    new-instance v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    invoke-direct {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;-><init>()V

    .line 1691
    .local v8, "lineSpacing":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v12, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    iput v12, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    .line 1692
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v12, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v12, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    .line 1693
    const/4 v12, 0x0

    iput v12, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->startPos:I

    .line 1694
    iput v3, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->endPos:I

    .line 1696
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1698
    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setParagraph(Ljava/util/ArrayList;)V

    .line 1700
    .end local v0    # "align":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;
    .end local v8    # "lineSpacing":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;
    :cond_4
    return-void
.end method

.method private checkMDMCameraLock()Z
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 3827
    const/4 v2, 0x0

    .line 3828
    .local v2, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v4, 0x0

    .line 3830
    .local v4, "result":Ljava/lang/String;
    :try_start_0
    const-string v6, "android.os.SystemProperties"

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v10

    invoke-static {v6, v9, v10}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    .line 3831
    const/4 v6, 0x1

    new-array v5, v6, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v9, Ljava/lang/String;

    aput-object v9, v5, v6

    .line 3832
    .local v5, "s":[Ljava/lang/Class;
    const/4 v6, 0x1

    new-array v1, v6, [Ljava/lang/Object;

    .line 3833
    .local v1, "args":[Ljava/lang/Object;
    const/4 v6, 0x0

    new-instance v9, Ljava/lang/String;

    const-string v10, "persist.sys.camera_lock"

    invoke-direct {v9, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v9, v1, v6

    .line 3834
    const-string v6, "get"

    invoke-virtual {v2, v6, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    const/4 v9, 0x0

    invoke-virtual {v6, v9, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Ljava/lang/String;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4

    .line 3847
    .end local v1    # "args":[Ljava/lang/Object;
    .end local v5    # "s":[Ljava/lang/Class;
    :goto_0
    if-nez v4, :cond_0

    move v6, v7

    .line 3854
    :goto_1
    return v6

    .line 3835
    :catch_0
    move-exception v3

    .line 3836
    .local v3, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v3}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 3837
    .end local v3    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v3

    .line 3838
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 3839
    .end local v3    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v3

    .line 3840
    .local v3, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v3}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 3841
    .end local v3    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v3

    .line 3842
    .local v3, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v3}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    .line 3843
    .end local v3    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_4
    move-exception v3

    .line 3844
    .local v3, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v3}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 3851
    .end local v3    # "e":Ljava/lang/NoSuchMethodException;
    :cond_0
    const-string v6, "camera_lock.enabled"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v6, v8

    .line 3852
    goto :goto_1

    :cond_1
    move v6, v7

    .line 3854
    goto :goto_1
.end method

.method private construct()V
    .locals 15

    .prologue
    .line 412
    const-string v1, "SpenInView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "nativeCanvas = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 414
    const/16 v1, 0x8

    const-string v2, " : nativeCanvas must not be null"

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 550
    :goto_0
    return-void

    .line 417
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 418
    const/16 v1, 0x8

    const-string v2, " : context must not be null"

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 423
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    .line 424
    .local v14, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 429
    .end local v14    # "manager":Landroid/content/pm/PackageManager;
    :goto_1
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 430
    .local v6, "rect":Landroid/graphics/RectF;
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 431
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 434
    :cond_2
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;)V

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 435
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;)V

    invoke-virtual {v1, v2}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 436
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v13

    .line 437
    .local v13, "mDisplayMetrics":Landroid/util/DisplayMetrics;
    if-eqz v13, :cond_4

    .line 438
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

    if-nez v1, :cond_3

    .line 439
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

    .line 441
    :cond_3
    iget v1, v13, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOnePT:F

    .line 442
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->MIN_STROKE_LENGTH:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOnePT:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->MIN_STROKE_LENGTH:F

    .line 444
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

    iget v1, v13, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v13, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v1, v3, :cond_5

    iget v1, v13, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_2
    iput v1, v2, Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;->baseRate:I

    .line 447
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 448
    .local v11, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v10, 0x3

    const/4 v12, 0x0

    move-object v7, p0

    invoke-direct/range {v7 .. v12}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 451
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget v3, v13, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v3, v3

    .line 452
    iget v4, v13, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;-><init>(Landroid/content/Context;FF)V

    .line 451
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 453
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;)V

    .line 456
    .end local v11    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_4
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;)V

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    .line 457
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 459
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    .line 460
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 461
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    const v2, -0xf2c5b1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 463
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    .line 464
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 465
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 467
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    .line 468
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 469
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 470
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 471
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    const v2, -0xf2c5b1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 473
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    .line 474
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 475
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 477
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 478
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 482
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    .line 483
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 485
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    .line 486
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 487
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 488
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 490
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;)V

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 491
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setPaint(Landroid/graphics/Paint;)V

    .line 493
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    .line 494
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 496
    new-instance v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 497
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setEraserSize(JF)V

    .line 498
    new-instance v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 499
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setRemoverSize(JF)V

    .line 501
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    .line 502
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;)V

    .line 503
    new-instance v1, Landroid/widget/Toast;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    .line 505
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->initHapticFeedback()V

    .line 506
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->registerPensoundSolution()V

    .line 508
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 548
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    .line 549
    new-instance v1, Landroid/graphics/PathMeasure;

    invoke-direct {v1}, Landroid/graphics/PathMeasure;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMeasure:Landroid/graphics/PathMeasure;

    goto/16 :goto_0

    .line 425
    .end local v6    # "rect":Landroid/graphics/RectF;
    .end local v13    # "mDisplayMetrics":Landroid/util/DisplayMetrics;
    :catch_0
    move-exception v0

    .line 426
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_1

    .line 445
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v6    # "rect":Landroid/graphics/RectF;
    .restart local v13    # "mDisplayMetrics":Landroid/util/DisplayMetrics;
    :cond_5
    iget v1, v13, Landroid/util/DisplayMetrics;->widthPixels:I

    goto/16 :goto_2
.end method

.method private convertPenNameToMaxThicknessValue(Ljava/lang/String;)I
    .locals 2
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 2101
    const/4 v0, 0x0

    .line 2103
    .local v0, "maxValue":I
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2104
    :cond_0
    const/16 v0, 0x40

    .line 2115
    :cond_1
    :goto_0
    return v0

    .line 2105
    :cond_2
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2106
    :cond_3
    const/16 v0, 0x20

    .line 2107
    goto :goto_0

    :cond_4
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2108
    :cond_5
    const/16 v0, 0x50

    .line 2109
    goto :goto_0

    :cond_6
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2110
    const/16 v0, 0x6c

    .line 2111
    goto :goto_0

    :cond_7
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "Eraser"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2112
    :cond_8
    const/16 v0, 0x64

    goto :goto_0
.end method

.method private createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 11
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    const/4 v10, 0x6

    .line 907
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v5

    if-nez v5, :cond_1

    .line 964
    :cond_0
    :goto_0
    return-void

    .line 911
    :cond_1
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    .line 912
    .local v4, "width":I
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    .line 913
    .local v2, "height":I
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    .line 914
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    .line 915
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    if-nez v5, :cond_2

    .line 916
    const-string v5, "The width of pageDoc is 0"

    invoke-static {v10, v5}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 919
    :cond_2
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    if-nez v5, :cond_3

    .line 920
    const-string v5, "The height of pageDoc is 0"

    invoke-static {v10, v5}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 924
    :cond_3
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    if-ne v4, v5, :cond_4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    if-ne v2, v5, :cond_4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isLayerChanged()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 927
    :cond_4
    const-string v5, "SpenInView"

    .line 928
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "createBitmap Width="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Height="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " IsLayerChanged="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 929
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isLayerChanged()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 928
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 927
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_5

    .line 932
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 933
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    .line 936
    :cond_5
    :try_start_0
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 941
    :goto_1
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-direct {p0, v6, v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setBitmap(JLandroid/graphics/Bitmap;)V

    .line 943
    const-string v5, "SpenInView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Added Layer Count="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getLayerCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Layer Size="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 944
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_7

    .line 945
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_removeCanvasBitmap(J)V

    .line 946
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_8

    .line 951
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 954
    :cond_7
    const/4 v3, 0x0

    .local v3, "pos":I
    :goto_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getLayerCount()I

    move-result v5

    if-lt v3, v5, :cond_9

    .line 963
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->deltaZoomSizeChanged()V

    goto/16 :goto_0

    .line 937
    .end local v3    # "pos":I
    :catch_0
    move-exception v1

    .line 938
    .local v1, "e":Ljava/lang/Throwable;
    const-string v5, "SpenInView"

    const-string v6, "Failed to create bitmap"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 939
    const/4 v5, 0x2

    invoke-static {v5}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_1

    .line 946
    .end local v1    # "e":Ljava/lang/Throwable;
    :cond_8
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 947
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_6

    .line 948
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_2

    .line 955
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v3    # "pos":I
    :cond_9
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 956
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    iget-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-eqz v5, :cond_a

    if-nez v3, :cond_a

    .line 957
    invoke-static {v0}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_reallocBitmapJNI(Landroid/graphics/Bitmap;)V

    .line 959
    :cond_a
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getLayerIdByIndex(I)I

    move-result v5

    invoke-direct {p0, v6, v7, v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V

    .line 960
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 954
    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method private deltaZoomSizeChanged()V
    .locals 5

    .prologue
    .line 1505
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getVariableForOnUpdateCanvas()V

    .line 1507
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    .line 1508
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onZoom(FFF)V

    .line 1509
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setLimitHeight(FF)V

    .line 1510
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    .line 1511
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    .line 1510
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setDrawInformation(IIII)V

    .line 1514
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v0, :cond_1

    .line 1515
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->setRatioBitmapSize(II)V

    .line 1516
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->setDeltaValue(FFFF)V

    .line 1519
    :cond_1
    const-string v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onZoom. dx : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", dy : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", r : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", MaxDx : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1520
    const-string v2, ", MaxDy : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1519
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1523
    return-void
.end method

.method private drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "containerObject"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .prologue
    .line 1063
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v3

    .line 1064
    .local v3, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1066
    .local v2, "objectCount":I
    const/4 v1, 0x0

    .local v1, "cnt":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 1081
    return-void

    .line 1067
    :cond_0
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1068
    .local v0, "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-nez v0, :cond_2

    .line 1066
    .end local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1072
    .restart local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v4

    .line 1073
    .local v4, "type":I
    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 1074
    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .end local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    goto :goto_1

    .line 1075
    .restart local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_3
    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    .line 1076
    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    .end local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectImage;)V

    goto :goto_1

    .line 1077
    .restart local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_4
    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 1078
    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .end local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    goto :goto_1
.end method

.method private drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectImage;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "imageObject"    # Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    .prologue
    const/4 v7, 0x0

    .line 1039
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    .line 1040
    .local v8, "dstRectF":Landroid/graphics/RectF;
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getHintText()Ljava/lang/String;

    move-result-object v9

    .line 1041
    .local v9, "hintText":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->isHintTextEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v9, :cond_0

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1042
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-direct {p0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1043
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    invoke-virtual {v8, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 1044
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getHintTextFontSize()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float v6, v1, v2

    .line 1045
    .local v6, "size":F
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getHintTextVerticalOffset()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float v10, v1, v2

    .line 1047
    .local v10, "verticalOffset":F
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-static {v2, v7}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1048
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getHintTextColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1049
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1050
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v9, v7, v1}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Landroid/text/TextPaint;

    .line 1051
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-direct {v2, v3}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    const/4 v5, 0x0

    .line 1050
    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 1053
    .local v0, "textLayout":Landroid/text/StaticLayout;
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1054
    iget v1, v8, Landroid/graphics/RectF;->left:F

    .line 1055
    iget v2, v8, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v10

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 1054
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1056
    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1057
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1060
    .end local v0    # "textLayout":Landroid/text/StaticLayout;
    .end local v6    # "size":F
    .end local v10    # "verticalOffset":F
    :cond_0
    return-void
.end method

.method private drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "textObject"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 993
    new-instance v9, Landroid/graphics/RectF;

    invoke-direct {v9}, Landroid/graphics/RectF;-><init>()V

    .line 994
    .local v9, "dstRectF":Landroid/graphics/RectF;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v12

    .line 995
    .local v12, "text":Ljava/lang/String;
    if-eqz v12, :cond_0

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 996
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getHintText()Ljava/lang/String;

    move-result-object v11

    .line 997
    .local v11, "hintText":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->isHintTextEnabled()Z

    move-result v1

    if-nez v1, :cond_4

    if-eqz v11, :cond_4

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 998
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-direct {p0, v9, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 999
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    invoke-virtual {v9, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 1000
    iget v1, v9, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, v9, Landroid/graphics/RectF;->left:F

    .line 1001
    iget v1, v9, Landroid/graphics/RectF;->top:F

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, v9, Landroid/graphics/RectF;->top:F

    .line 1002
    iget v1, v9, Landroid/graphics/RectF;->right:F

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, v9, Landroid/graphics/RectF;->right:F

    .line 1003
    iget v1, v9, Landroid/graphics/RectF;->bottom:F

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getBottomMargin()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, v9, Landroid/graphics/RectF;->bottom:F

    .line 1005
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getHintTextFontSize()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float v6, v1, v2

    .line 1006
    .local v6, "size":F
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getFont()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1007
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getHintTextColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1008
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1010
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTextAlignment()I

    move-result v8

    .line 1011
    .local v8, "align":I
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 1012
    .local v4, "alignment":Landroid/text/Layout$Alignment;
    const/4 v1, 0x2

    if-eq v8, v1, :cond_1

    const/4 v1, 0x3

    if-ne v8, v1, :cond_5

    .line 1013
    :cond_1
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    .line 1018
    :cond_2
    :goto_0
    new-instance v0, Landroid/text/StaticLayout;

    const/4 v1, 0x0

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v11, v1, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Landroid/text/TextPaint;

    .line 1019
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-direct {v2, v3}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 1018
    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 1021
    .local v0, "textLayout":Landroid/text/StaticLayout;
    const v1, 0x3e99999a    # 0.3f

    mul-float v13, v6, v1

    .line 1022
    .local v13, "verticalOffset":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getGravity()I

    move-result v10

    .line 1023
    .local v10, "gravity":I
    const/4 v1, 0x1

    if-ne v10, v1, :cond_6

    .line 1024
    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float/2addr v1, v6

    const/high16 v2, 0x40000000    # 2.0f

    div-float v13, v1, v2

    .line 1029
    :cond_3
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1030
    iget v1, v9, Landroid/graphics/RectF;->left:F

    iget v2, v9, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v13

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1031
    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1032
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1036
    .end local v0    # "textLayout":Landroid/text/StaticLayout;
    .end local v4    # "alignment":Landroid/text/Layout$Alignment;
    .end local v6    # "size":F
    .end local v8    # "align":I
    .end local v10    # "gravity":I
    .end local v11    # "hintText":Ljava/lang/String;
    .end local v13    # "verticalOffset":F
    :cond_4
    return-void

    .line 1014
    .restart local v4    # "alignment":Landroid/text/Layout$Alignment;
    .restart local v6    # "size":F
    .restart local v8    # "align":I
    .restart local v11    # "hintText":Ljava/lang/String;
    :cond_5
    const/4 v1, 0x1

    if-ne v8, v1, :cond_2

    .line 1015
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_0

    .line 1025
    .restart local v0    # "textLayout":Landroid/text/StaticLayout;
    .restart local v10    # "gravity":I
    .restart local v13    # "verticalOffset":F
    :cond_6
    const/4 v1, 0x2

    if-ne v10, v1, :cond_3

    .line 1026
    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float v13, v1, v6

    goto :goto_1
.end method

.method private fitControlToObject()V
    .locals 3

    .prologue
    .line 1918
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    .line 1919
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isObjectContained(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1920
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    .line 1921
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    .line 1926
    :cond_0
    :goto_0
    return-void

    .line 1923
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    goto :goto_0
.end method

.method private getResourceString(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "strName"    # Ljava/lang/String;

    .prologue
    .line 5883
    const/4 v1, 0x0

    .line 5884
    .local v1, "string":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    if-eqz v2, :cond_1

    .line 5885
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    const-string v3, "string"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 5886
    .local v0, "resId":I
    if-nez v0, :cond_0

    .line 5887
    const/4 v2, 0x0

    .line 5891
    .end local v0    # "resId":I
    :goto_0
    return-object v2

    .line 5889
    .restart local v0    # "resId":I
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .end local v0    # "resId":I
    :cond_1
    move-object v2, v1

    .line 5891
    goto :goto_0
.end method

.method private getVariableForOnUpdateCanvas()V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 1470
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v1, :cond_2

    .line 1471
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    .line 1475
    :goto_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_0

    .line 1476
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    .line 1479
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    if-ge v1, v2, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    .line 1480
    .local v0, "shortHeight":I
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v1, :cond_4

    .line 1481
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchHeight:I

    int-to-float v1, v1

    int-to-float v2, v0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    .line 1485
    :goto_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_1

    .line 1486
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    .line 1489
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v1, :cond_5

    .line 1490
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    .line 1491
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    .line 1497
    :goto_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    if-ge v1, v2, :cond_6

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    :goto_4
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    .line 1498
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    if-ge v1, v2, :cond_7

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    :goto_5
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    .line 1500
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    .line 1501
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    .line 1502
    return-void

    .line 1473
    .end local v0    # "shortHeight":I
    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    goto :goto_0

    .line 1479
    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    goto :goto_1

    .line 1483
    .restart local v0    # "shortHeight":I
    :cond_4
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v1, v1

    int-to-float v2, v0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    goto :goto_2

    .line 1493
    :cond_5
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    .line 1494
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    goto :goto_3

    .line 1497
    :cond_6
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    goto :goto_4

    .line 1498
    :cond_7
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    goto :goto_5
.end method

.method private initHapticFeedback()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2036
    const-string v2, "SpenInView"

    const-string v3, "initHapticFeedback() - Start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2037
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-nez v2, :cond_0

    .line 2039
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2040
    .local v0, "dm":Landroid/util/DisplayMetrics;
    new-instance v2, Lcom/samsung/hapticfeedback/HapticEffect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/hapticfeedback/HapticEffect;-><init>(Landroid/content/Context;II)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 2049
    .end local v0    # "dm":Landroid/util/DisplayMetrics;
    :cond_0
    :goto_0
    const-string v2, "SpenInView"

    const-string v3, "initHapticFeedback() - End"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2050
    return-void

    .line 2041
    :catch_0
    move-exception v1

    .line 2042
    .local v1, "error":Ljava/lang/UnsatisfiedLinkError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 2043
    const-string v2, "TAG"

    const-string v3, "Haptic Effect UnsatisfiedLinkError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2044
    .end local v1    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v1

    .line 2045
    .local v1, "error":Ljava/lang/NoClassDefFoundError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 2046
    const-string v2, "TAG"

    const-string v3, "Haptic Effect NoClassDefFoundError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isIntersect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 2
    .param p1, "lhs"    # Landroid/graphics/RectF;
    .param p2, "rhs"    # Landroid/graphics/RectF;

    .prologue
    .line 848
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static native native_cancelStroke(I)V
.end method

.method private static native native_cancelStroke(J)V
.end method

.method private static native native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_construct(ILandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z
.end method

.method private static native native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z
.end method

.method private static native native_drawObjectList(ILandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;I)Z"
        }
    .end annotation
.end method

.method private static native native_drawObjectList(JLandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;I)Z"
        }
    .end annotation
.end method

.method private static native native_enablePenCurve(IZ)V
.end method

.method private static native native_enablePenCurve(JZ)V
.end method

.method private static native native_enableZoom(IZ)V
.end method

.method private static native native_enableZoom(JZ)V
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_finalize(J)V
.end method

.method private static native native_getAdvancedSetting(I)Ljava/lang/String;
.end method

.method private static native native_getAdvancedSetting(J)Ljava/lang/String;
.end method

.method private static native native_getEraserSize(I)F
.end method

.method private static native native_getEraserSize(J)F
.end method

.method private static native native_getMaxZoomRatio(I)F
.end method

.method private static native native_getMaxZoomRatio(J)F
.end method

.method private static native native_getMinZoomRatio(I)F
.end method

.method private static native native_getMinZoomRatio(J)F
.end method

.method private static native native_getPan(ILandroid/graphics/PointF;)V
.end method

.method private static native native_getPan(JLandroid/graphics/PointF;)V
.end method

.method private static native native_getPenColor(I)I
.end method

.method private static native native_getPenColor(J)I
.end method

.method private static native native_getPenSize(I)F
.end method

.method private static native native_getPenSize(J)F
.end method

.method private static native native_getPenStyle(I)Ljava/lang/String;
.end method

.method private static native native_getPenStyle(J)Ljava/lang/String;
.end method

.method private static native native_getReplayState(I)I
.end method

.method private static native native_getReplayState(J)I
.end method

.method private static native native_getTemporaryStroke(ILjava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)V"
        }
    .end annotation
.end method

.method private static native native_getTemporaryStroke(JLjava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)V"
        }
    .end annotation
.end method

.method private static native native_getToolTypeAction(II)I
.end method

.method private static native native_getToolTypeAction(JI)I
.end method

.method private static native native_getZoomRatio(I)F
.end method

.method private static native native_getZoomRatio(J)F
.end method

.method private static native native_inVisibleUpdate(IIZZ)V
.end method

.method private static native native_inVisibleUpdate(JIZZ)V
.end method

.method private static native native_init()I
.end method

.method private static native native_init_64()J
.end method

.method private static native native_isPenCurve(I)Z
.end method

.method private static native native_isPenCurve(J)Z
.end method

.method private static native native_isZoomable(I)Z
.end method

.method private static native native_isZoomable(J)Z
.end method

.method private static native native_onHover(ILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onHover(JLandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onLongPress(ILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onLongPress(JLandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onSingleTapUp(ILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onSingleTapUp(JLandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onTouch(ILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onTouch(JLandroid/view/MotionEvent;I)Z
.end method

.method private static native native_pauseReplay(I)Z
.end method

.method private static native native_pauseReplay(J)Z
.end method

.method private static native native_removeCanvasBitmap(I)V
.end method

.method private static native native_removeCanvasBitmap(J)V
.end method

.method private static native native_resumeReplay(I)Z
.end method

.method private static native native_resumeReplay(J)Z
.end method

.method private static native native_setAdvancedSetting(ILjava/lang/String;)V
.end method

.method private static native native_setAdvancedSetting(JLjava/lang/String;)V
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setBitmap(JLandroid/graphics/Bitmap;)V
.end method

.method private static native native_setCanvasBitmap(IILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setEraserSize(IF)V
.end method

.method private static native native_setEraserSize(JF)V
.end method

.method private static native native_setEraserType(II)V
.end method

.method private static native native_setEraserType(JI)V
.end method

.method private static native native_setHyperTextViewEnabled(IZ)V
.end method

.method private static native native_setHyperTextViewEnabled(JZ)V
.end method

.method private static native native_setMaxZoomRatio(IF)Z
.end method

.method private static native native_setMaxZoomRatio(JF)Z
.end method

.method private static native native_setMinZoomRatio(IF)Z
.end method

.method private static native native_setMinZoomRatio(JF)Z
.end method

.method private static native native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
.end method

.method private static native native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
.end method

.method private static native native_setPan(IFFZ)V
.end method

.method private static native native_setPan(JFFZ)V
.end method

.method private static native native_setPenColor(II)V
.end method

.method private static native native_setPenColor(JI)V
.end method

.method private static native native_setPenSize(IF)V
.end method

.method private static native native_setPenSize(JF)V
.end method

.method private static native native_setPenStyle(ILjava/lang/String;)Z
.end method

.method private static native native_setPenStyle(JLjava/lang/String;)Z
.end method

.method private static native native_setRemoverSize(IF)V
.end method

.method private static native native_setRemoverSize(JF)V
.end method

.method private static native native_setRemoverType(II)V
.end method

.method private static native native_setRemoverType(JI)V
.end method

.method private static native native_setReplayPosition(II)Z
.end method

.method private static native native_setReplayPosition(JI)Z
.end method

.method private static native native_setReplaySpeed(II)Z
.end method

.method private static native native_setReplaySpeed(JI)Z
.end method

.method private static native native_setScreenFrameBuffer(ILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setScreenFrameBuffer(JLandroid/graphics/Bitmap;)V
.end method

.method private static native native_setScreenSize(IIII)V
.end method

.method private static native native_setScreenSize(JIII)V
.end method

.method private static native native_setSelectionType(II)V
.end method

.method private static native native_setSelectionType(JI)V
.end method

.method private static native native_setToolTypeAction(III)V
.end method

.method private static native native_setToolTypeAction(JII)V
.end method

.method private static native native_setZoom(IFFF)V
.end method

.method private static native native_setZoom(JFFF)V
.end method

.method private static native native_startReplay(I)Z
.end method

.method private static native native_startReplay(J)Z
.end method

.method private static native native_startTemporaryStroke(I)V
.end method

.method private static native native_startTemporaryStroke(J)V
.end method

.method private static native native_stopReplay(I)Z
.end method

.method private static native native_stopReplay(J)Z
.end method

.method private static native native_stopTemporaryStroke(I)V
.end method

.method private static native native_stopTemporaryStroke(J)V
.end method

.method private static native native_update(I)V
.end method

.method private static native native_update(J)V
.end method

.method private static native native_updateAllScreenFrameBuffer(I)V
.end method

.method private static native native_updateAllScreenFrameBuffer(J)V
.end method

.method private static native native_updateRedo(I[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
.end method

.method private static native native_updateRedo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
.end method

.method private static native native_updateUndo(I[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
.end method

.method private static native native_updateUndo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
.end method

.method private onColorPickerChanged(III)V
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "color"    # I

    .prologue
    .line 1545
    const-string v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onColorPickerChanged color"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1546
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    if-eqz v3, :cond_1

    .line 1547
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v3, :cond_0

    .line 1548
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v3, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v3, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getPixel(II)I

    move-result v1

    .line 1549
    .local v1, "controlColor":I
    if-eqz v1, :cond_0

    .line 1550
    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x437f0000    # 255.0f

    div-float v0, v3, v4

    .line 1551
    .local v0, "alpha":F
    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v2, v3, v0

    .line 1552
    .local v2, "ialpha":F
    const/16 v3, 0xff

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v0

    invoke-static {p3}, Landroid/graphics/Color;->red(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v2

    add-float/2addr v4, v5

    float-to-int v4, v4

    .line 1553
    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v0

    invoke-static {p3}, Landroid/graphics/Color;->green(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    add-float/2addr v5, v6

    float-to-int v5, v5

    .line 1554
    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v0

    invoke-static {p3}, Landroid/graphics/Color;->blue(I)I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v2

    add-float/2addr v6, v7

    float-to-int v6, v6

    .line 1552
    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result p3

    .line 1557
    .end local v0    # "alpha":F
    .end local v1    # "controlColor":I
    .end local v2    # "ialpha":F
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    invoke-interface {v3, p3, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;->onChanged(III)V

    .line 1559
    :cond_1
    return-void
.end method

.method private onCompleted()V
    .locals 2

    .prologue
    .line 1569
    const-string v0, "SpenInView"

    const-string v1, "Replay onCompleted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1570
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    if-eqz v0, :cond_0

    .line 1571
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;->onCompleted()V

    .line 1573
    :cond_0
    return-void
.end method

.method private onHyperText(Ljava/lang/String;II)V
    .locals 2
    .param p1, "hyperText"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "handle"    # I

    .prologue
    .line 1911
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    .line 1912
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObjectByRuntimeHandle(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 1913
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    invoke-interface {v1, p1, p2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;->onSelected(Ljava/lang/String;ILcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 1915
    .end local v0    # "textBox":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    :cond_0
    return-void
.end method

.method private onPageDocCanceled(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 2
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 1584
    const-string v0, "SpenInView"

    const-string v1, "onPageDocCanceled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1585
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    if-eqz v0, :cond_0

    .line 1586
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;->onCanceled(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 1588
    :cond_0
    return-void
.end method

.method private onPageDocCompleted(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 2
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 1576
    const-string v0, "SpenInView"

    const-string v1, "onPageDocCompleted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1577
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    if-eqz v0, :cond_0

    .line 1578
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 1579
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;->onCompleted(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 1581
    :cond_0
    return-void
.end method

.method private onProgressChanged(II)V
    .locals 1
    .param p1, "progress"    # I
    .param p2, "id"    # I

    .prologue
    .line 1563
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    if-eqz v0, :cond_0

    .line 1564
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;->onProgressChanged(II)V

    .line 1566
    :cond_0
    return-void
.end method

.method private onSelectObject(Ljava/util/ArrayList;IIFFI)Z
    .locals 21
    .param p2, "toolType"    # I
    .param p3, "pressType"    # I
    .param p4, "x"    # F
    .param p5, "y"    # F
    .param p6, "userdata"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;IIFFI)Z"
        }
    .end annotation

    .prologue
    .line 1704
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v2, :cond_0

    if-nez p3, :cond_0

    .line 1705
    const/4 v2, 0x0

    .line 1900
    :goto_0
    return v2

    .line 1706
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v2, :cond_2

    .line 1707
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isTouchEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1708
    const/4 v2, 0x0

    goto :goto_0

    .line 1711
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 1714
    :cond_2
    if-nez p1, :cond_3

    .line 1715
    const-string v2, "SpenInView"

    const-string v3, "onSelectObject ObjectList is nulll"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1716
    const/4 v2, 0x0

    goto :goto_0

    .line 1719
    :cond_3
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 1720
    const-string v2, "SpenInView"

    const-string v3, "onSelectObject : selected list size is zero."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1721
    const/4 v2, 0x0

    goto :goto_0

    .line 1724
    :cond_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1725
    .local v4, "relativeObjectRectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1726
    .local v5, "menuList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1727
    .local v6, "styleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v8, Landroid/graphics/PointF;

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-direct {v8, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1729
    .local v8, "point":Landroid/graphics/PointF;
    new-instance v19, Landroid/graphics/RectF;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/RectF;-><init>()V

    .line 1730
    .local v19, "tempRelativeRectF":Landroid/graphics/RectF;
    const/16 v18, 0x0

    .line 1731
    .local v18, "tempObject":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    new-instance v10, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x0

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-direct {v10, v2, v3, v7, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1733
    .local v10, "boundaryRect":Landroid/graphics/Rect;
    const-string v2, "SpenInView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "onSelectObject : objectList.size() ="

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1735
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v14, v2, :cond_5

    .line 1756
    const-string v2, "SpenInView"

    const-string v3, "onSelectObject : boundaryRect finished"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1758
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1761
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_9

    .line 1762
    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1763
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    if-eqz v2, :cond_7

    .line 1764
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-object/from16 v3, p1

    move/from16 v7, p3

    invoke-interface/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlListener;->onCreated(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ILandroid/graphics/PointF;)Z

    move-result v11

    .line 1766
    .local v11, "continueProcess":Z
    if-nez v11, :cond_7

    .line 1767
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1736
    .end local v11    # "continueProcess":Z
    :cond_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "tempObject":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1738
    .restart local v18    # "tempObject":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-nez v18, :cond_6

    .line 1739
    const-string v2, "SpenInView"

    const-string v3, "onSelectObject : object is null."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1740
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1748
    :cond_6
    new-instance v17, Landroid/graphics/Rect;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Rect;-><init>()V

    .line 1749
    .local v17, "relativeRect":Landroid/graphics/Rect;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1750
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 1751
    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1753
    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 1735
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 1771
    .end local v17    # "relativeRect":Landroid/graphics/Rect;
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getSelectedObjectCount()I

    move-result v2

    if-nez v2, :cond_8

    .line 1773
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->selectObject(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1783
    :cond_8
    new-instance v12, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v12, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 1784
    .local v12, "control":Lcom/samsung/android/sdk/pen/engine/SpenControlList;
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v12, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setStyle(I)V

    .line 1787
    invoke-virtual {v12, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setContextMenu(Ljava/util/ArrayList;)V

    .line 1788
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setObject(Ljava/util/ArrayList;)V

    .line 1789
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    .line 1790
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1774
    .end local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlList;
    :catch_0
    move-exception v13

    .line 1775
    .local v13, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "SpenInView"

    const-string v3, "selectObject : object is not in current layer"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1776
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1777
    .end local v13    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v13

    .line 1778
    .local v13, "e":Ljava/lang/IllegalStateException;
    const-string v2, "SpenInView"

    const-string v3, "selectObject : pageDoc is not loaded"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1779
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1794
    .end local v13    # "e":Ljava/lang/IllegalStateException;
    :cond_9
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1795
    .local v16, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    const/4 v12, 0x0

    .line 1797
    .local v12, "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getSelectedObjectCount()I

    move-result v2

    if-nez v2, :cond_a

    .line 1799
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->selectObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1809
    :cond_a
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1869
    .end local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    if-eqz v2, :cond_10

    .line 1872
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-object/from16 v3, p1

    move/from16 v7, p3

    invoke-interface/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlListener;->onCreated(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ILandroid/graphics/PointF;)Z

    move-result v11

    .line 1874
    .restart local v11    # "continueProcess":Z
    if-nez v11, :cond_10

    .line 1875
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1800
    .end local v11    # "continueProcess":Z
    .restart local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :catch_2
    move-exception v13

    .line 1801
    .local v13, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "SpenInView"

    const-string v3, "selectObject : object is not in current layer"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1802
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1803
    .end local v13    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v13

    .line 1804
    .local v13, "e":Ljava/lang/IllegalStateException;
    const-string v2, "SpenInView"

    const-string v3, "selectObject : pageDoc is not loaded"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1805
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1811
    .end local v13    # "e":Ljava/lang/IllegalStateException;
    :pswitch_0
    const-string v2, "SpenInView"

    const-string v3, "Text Selection"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1813
    const/4 v2, 0x1

    move/from16 v0, p6

    if-ne v0, v2, :cond_b

    .line 1815
    new-instance v9, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v9, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 1816
    .local v9, "ControlTextBox":Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setStyle(I)V

    .line 1818
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setTextEraserEnabled(Z)V

    .line 1820
    check-cast v16, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .end local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 1821
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setEditable(Z)V

    .line 1822
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setDimEnabled(Z)V

    .line 1823
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    .line 1824
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1827
    .end local v9    # "ControlTextBox":Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;
    .restart local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_b
    const/4 v15, 0x1

    .line 1828
    .local v15, "isEditable":Z
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v2

    const/4 v3, 0x6

    if-eq v2, v3, :cond_c

    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v2

    const/4 v3, 0x6

    if-eq v2, v3, :cond_c

    .line 1829
    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v2

    const/4 v3, 0x6

    if-eq v2, v3, :cond_c

    .line 1830
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v2

    const/4 v3, 0x6

    if-eq v2, v3, :cond_c

    .line 1831
    const/4 v2, 0x2

    move/from16 v0, p3

    if-ne v0, v2, :cond_d

    .line 1832
    :cond_c
    const/4 v15, 0x0

    .line 1835
    :cond_d
    if-eqz v15, :cond_e

    .line 1836
    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_e
    move-object/from16 v2, v16

    .line 1839
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->applyTextSetting(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 1840
    new-instance v12, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    .end local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v12, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .restart local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object v2, v12

    .line 1841
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    check-cast v16, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .end local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 1842
    if-nez p3, :cond_f

    if-eqz v15, :cond_f

    move-object v2, v12

    .line 1843
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTouchPre:Z

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setShowSoftInputEnable(Z)V

    :cond_f
    move-object v2, v12

    .line 1845
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v2, v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setEditable(Z)V

    move-object v2, v12

    .line 1846
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setDimEnabled(Z)V

    goto/16 :goto_2

    .line 1851
    .end local v15    # "isEditable":Z
    .restart local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :pswitch_1
    const-string v2, "SpenInView"

    const-string v3, "TYPE_IMAGE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1852
    new-instance v12, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;

    .end local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v12, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .restart local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object v2, v12

    .line 1853
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;

    check-cast v16, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    .end local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectImage;)V

    goto/16 :goto_2

    .line 1857
    .restart local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :pswitch_2
    const-string v2, "SpenInView"

    const-string v3, "TYPE_STROKE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1858
    new-instance v12, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;

    .end local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v12, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .restart local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object v2, v12

    .line 1859
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;

    check-cast v16, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .end local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    goto/16 :goto_2

    .line 1863
    .restart local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :pswitch_3
    const-string v2, "SpenInView"

    const-string v3, "TYPE_CONTAINER"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1864
    new-instance v12, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;

    .end local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v12, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .restart local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object v2, v12

    .line 1865
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;

    check-cast v16, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .end local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    goto/16 :goto_2

    .line 1879
    :cond_10
    if-eqz v12, :cond_11

    .line 1880
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v12, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setStyle(I)V

    .line 1881
    invoke-virtual {v12, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setContextMenu(Ljava/util/ArrayList;)V

    .line 1883
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setFocusable(Z)V

    .line 1884
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->requestFocus()Z

    .line 1885
    const v2, 0x3ac90

    invoke-virtual {v12, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setNextFocusDownId(I)V

    .line 1886
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenInView$3;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    invoke-virtual {v12, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1896
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    .line 1897
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1900
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1809
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private onShowRemoverMessage()V
    .locals 3

    .prologue
    .line 5895
    const-string v0, "SpenInView"

    const-string v1, "onShowRemoverMessage"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5897
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 5898
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    const-string v1, "string_unable_to_erase_heavy_lines"

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 5899
    const/4 v2, 0x0

    .line 5898
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    .line 5911
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 5912
    return-void
.end method

.method private onUpdateCanvas(Landroid/graphics/RectF;Z)V
    .locals 5
    .param p1, "rectf"    # Landroid/graphics/RectF;
    .param p2, "isScreenFramebuffer"    # Z

    .prologue
    const/4 v4, 0x0

    .line 1262
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1263
    const/4 p1, 0x0

    .line 1265
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    if-eqz v0, :cond_2

    .line 1266
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 1267
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 1268
    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 1269
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 1270
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 1272
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    if-eqz v0, :cond_3

    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_3

    .line 1273
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1274
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 1275
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1276
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1286
    :cond_2
    :goto_0
    return-void

    .line 1279
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 1280
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    invoke-interface {v0, p1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0

    .line 1282
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method private onZoom(FFF)V
    .locals 2
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 1526
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    .line 1527
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    .line 1528
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    .line 1530
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->deltaZoomSizeChanged()V

    .line 1532
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    if-eqz v0, :cond_0

    .line 1533
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;->onZoom(FFF)V

    .line 1536
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_2

    .line 1537
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_1

    .line 1538
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->fit(Z)V

    .line 1540
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onZoom()V

    .line 1542
    :cond_2
    return-void
.end method

.method private raiseOnKeyDown()V
    .locals 4

    .prologue
    const/16 v3, 0x16

    .line 1904
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    .line 1905
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    .line 1906
    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 1905
    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 1908
    :cond_0
    return-void
.end method

.method private registerPensoundSolution()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 2062
    const-string v1, "SpenInView"

    const-string v2, "registerPensoundSolution() - Start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2064
    :try_start_0
    sget-boolean v1, Lcom/samsung/audio/SmpsManager;->isSupport:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 2074
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-nez v1, :cond_0

    .line 2075
    new-instance v1, Lcom/samsung/audio/SmpsManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/audio/SmpsManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 2076
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v1, :cond_0

    .line 2077
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexPencil:I

    .line 2078
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexMarker:I

    .line 2079
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexBrush:I

    .line 2080
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexEraser:I

    .line 2081
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    if-eq v1, v3, :cond_1

    .line 2082
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 2088
    :cond_0
    :goto_0
    const-string v1, "SpenInView"

    const-string v2, "registerPensoundSolution() - End"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2089
    :goto_1
    return-void

    .line 2065
    :catch_0
    move-exception v0

    .line 2066
    .local v0, "error":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "SpenInView"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2067
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    goto :goto_1

    .line 2069
    .end local v0    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 2070
    .local v0, "error":Ljava/lang/NoClassDefFoundError;
    const-string v1, "SpenInView"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2071
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    goto :goto_1

    .line 2083
    .end local v0    # "error":Ljava/lang/NoClassDefFoundError;
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexPencil:I

    if-eq v1, v3, :cond_0

    .line 2084
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexPencil:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    goto :goto_0
.end method

.method private relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/RectF;

    .prologue
    .line 841
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 842
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 843
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 844
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 845
    return-void
.end method

.method private releaseHapticFeedback()V
    .locals 2

    .prologue
    .line 2053
    const-string v0, "SpenInView"

    const-string v1, "releaseHapticFeedback() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2054
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v0, :cond_0

    .line 2055
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    invoke-virtual {v0}, Lcom/samsung/hapticfeedback/HapticEffect;->closeDevice()V

    .line 2056
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 2058
    :cond_0
    const-string v0, "SpenInView"

    const-string v1, "releaseHapticFeedback() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2059
    return-void
.end method

.method private removeHoveringIcon(I)Z
    .locals 6
    .param p1, "nHoverIconID"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1929
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnable:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-nez v4, :cond_1

    .line 1966
    :cond_0
    :goto_0
    return v2

    .line 1932
    :cond_1
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnable:Z

    .line 1933
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1934
    .local v1, "pm":Landroid/content/pm/PackageManager;
    if-eqz v1, :cond_0

    .line 1935
    const-string v4, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1943
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(I)V

    .line 1944
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-virtual {v4, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->removeHoveringSpenCustomIcon(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    move v2, v3

    .line 1966
    goto :goto_0

    .line 1945
    :catch_0
    move-exception v0

    .line 1946
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "SpenInView"

    const-string v4, "removeCustomHoveringIcon() IllegalArgumentException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1947
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1949
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 1950
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v3, "SpenInView"

    const-string v4, "removeCustomHoveringIcon() ClassNotFoundException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1951
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1953
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_2
    move-exception v0

    .line 1954
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v3, "SpenInView"

    const-string v4, "removeCustomHoveringIcon() NoSuchMethodException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1955
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 1957
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_3
    move-exception v0

    .line 1958
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "SpenInView"

    const-string v4, "removeCustomHoveringIcon() IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1959
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 1961
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 1962
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "SpenInView"

    const-string v4, "removeCustomHoveringIcon() IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1963
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private savePngFile(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 2645
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2648
    .local v1, "file":Ljava/io/File;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2649
    .local v2, "filestream":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2650
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2656
    .end local v2    # "filestream":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 2651
    :catch_0
    move-exception v0

    .line 2652
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 2653
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 2654
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private setCustomHoveringIcon()I
    .locals 8

    .prologue
    const/4 v3, -0x1

    .line 1970
    const/4 v1, -0x1

    .line 1971
    .local v1, "nHoverIconID":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_1

    :cond_0
    move v3, v1

    .line 2012
    :goto_0
    return v3

    .line 1974
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnable:Z

    .line 1976
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1977
    .local v2, "pm":Landroid/content/pm/PackageManager;
    if-eqz v2, :cond_2

    .line 1978
    const-string v4, "com.sec.feature.hovering_ui"

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    move v3, v1

    .line 1979
    goto :goto_0

    :cond_2
    move v3, v1

    .line 1982
    goto :goto_0

    .line 1986
    :cond_3
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(ILandroid/graphics/drawable/Drawable;Landroid/graphics/Point;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    move-result v1

    move v3, v1

    .line 2012
    goto :goto_0

    .line 1987
    :catch_0
    move-exception v0

    .line 1988
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "SpenInView"

    const-string v5, "setCustomHoveringIcon() IllegalArgumentException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1989
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1991
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 1992
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v4, "SpenInView"

    const-string v5, "setCustomHoveringIcon() NotFoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1993
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1995
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    :catch_2
    move-exception v0

    .line 1996
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v4, "SpenInView"

    const-string v5, "setCustomHoveringIcon() ClassNotFoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1997
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1999
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_3
    move-exception v0

    .line 2000
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v4, "SpenInView"

    const-string v5, "setCustomHoveringIcon() NoSuchMethodException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2001
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 2003
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v0

    .line 2004
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "SpenInView"

    const-string v5, "setCustomHoveringIcon() IllegalAccessException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2005
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 2007
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_5
    move-exception v0

    .line 2008
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v4, "SpenInView"

    const-string v5, "setCustomHoveringIcon() InvocationTargetException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2009
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 2016
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2021
    :goto_0
    return-void

    .line 2019
    :cond_0
    const-string v0, "SpenInView"

    const-string v1, "setHoverPointerDrawable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2020
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private setPenHoverPoint(Ljava/lang/String;)V
    .locals 4
    .param p1, "penname"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x3

    const/16 v2, 0x32

    .line 5915
    if-nez p1, :cond_0

    .line 5944
    :goto_0
    return-void

    .line 5918
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    if-nez v0, :cond_1

    .line 5919
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    .line 5921
    :cond_1
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    .line 5922
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.MontblancFountainPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 5923
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 5924
    :cond_3
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    .line 5925
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.MontblancCalligraphyPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 5926
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 5927
    :cond_5
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    .line 5928
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 5929
    :cond_6
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    .line 5930
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 5931
    :cond_7
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_8

    .line 5932
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 5933
    :cond_8
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    .line 5934
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 5935
    :cond_9
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_a

    .line 5936
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 5937
    :cond_a
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_b

    .line 5938
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 5939
    :cond_b
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_c

    .line 5940
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 5942
    :cond_c
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0
.end method

.method private unregisterPensoundSolution()V
    .locals 2

    .prologue
    .line 2092
    const-string v0, "SpenInView"

    const-string v1, "unregisterPensoundSolution() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2093
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v0, :cond_0

    .line 2094
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    invoke-virtual {v0}, Lcom/samsung/audio/SmpsManager;->onDestroy()V

    .line 2095
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 2097
    :cond_0
    const-string v0, "SpenInView"

    const-string v1, "unregisterPensoundSolution() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2098
    return-void
.end method

.method private updateCanvasLayer(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V
    .locals 18
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "re_rectF"    # Landroid/graphics/RectF;
    .param p3, "isScreenFramebuffer"    # Z

    .prologue
    .line 1156
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1259
    :cond_0
    :goto_0
    return-void

    .line 1159
    :cond_1
    if-eqz p2, :cond_2

    .line 1160
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-nez v2, :cond_2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-nez v2, :cond_2

    .line 1161
    const/16 p2, 0x0

    .line 1165
    :cond_2
    if-eqz p1, :cond_0

    .line 1168
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1169
    .local v3, "srcDrawRect":Landroid/graphics/Rect;
    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    .line 1171
    .local v12, "dstDrawRect":Landroid/graphics/Rect;
    if-nez p2, :cond_6

    .line 1172
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v6, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v7, v2

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    .line 1173
    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    invoke-virtual {v12, v2, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 1174
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    invoke-virtual {v12, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 1177
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v2, v2

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-lez v2, :cond_3

    .line 1178
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v7, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v8, v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1179
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    add-int/2addr v2, v4

    int-to-float v5, v2

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutWidth:I

    int-to-float v7, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutHeight:I

    int-to-float v8, v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1181
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-lez v2, :cond_4

    .line 1182
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v7, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v8, v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1183
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    add-int/2addr v2, v4

    int-to-float v6, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutWidth:I

    int-to-float v7, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutHeight:I

    int-to-float v8, v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1191
    :cond_4
    :goto_1
    if-eqz p3, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v4

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v2, :cond_8

    .line 1192
    :cond_5
    new-instance v17, Landroid/graphics/Rect;

    move-object/from16 v0, v17

    invoke-direct {v0, v12}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1193
    .local v17, "srcRectFB":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    neg-int v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    neg-int v4, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 1194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    invoke-virtual {v2, v4, v5, v6}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 1196
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-nez v2, :cond_7

    .line 1197
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v2, v1, v12, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 1186
    .end local v17    # "srcRectFB":Landroid/graphics/Rect;
    :cond_6
    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p2

    iget v6, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p2

    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    .line 1187
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v12, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->ExtendRectFromRectF(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    .line 1188
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    invoke-virtual {v12, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    goto :goto_1

    .line 1199
    .restart local v17    # "srcRectFB":Landroid/graphics/Rect;
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v2, v1, v12, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 1202
    .end local v17    # "srcRectFB":Landroid/graphics/Rect;
    :cond_8
    new-instance v16, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1203
    .local v16, "srcRect":Landroid/graphics/Rect;
    new-instance v13, Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v6, v6

    invoke-direct {v13, v2, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1205
    .local v13, "dstRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v2, :cond_b

    .line 1206
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v2, v4

    iput v2, v13, Landroid/graphics/RectF;->left:F

    .line 1207
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v2, v4

    iput v2, v13, Landroid/graphics/RectF;->right:F

    .line 1208
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v2, v4

    iput v2, v13, Landroid/graphics/RectF;->top:F

    .line 1209
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v2, v4

    iput v2, v13, Landroid/graphics/RectF;->bottom:F

    .line 1217
    :goto_2
    const/4 v15, 0x0

    .local v15, "layer":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v15, v2, :cond_0

    .line 1218
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Bitmap;

    .line 1219
    .local v11, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v11, :cond_a

    .line 1220
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v11, v5, v6}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v5

    invoke-virtual {v11, v2, v4, v5}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 1221
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-nez v2, :cond_10

    .line 1222
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v4

    mul-int/2addr v2, v4

    .line 1223
    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v5

    mul-int/2addr v4, v5

    .line 1222
    add-int v10, v2, v4

    .line 1224
    .local v10, "area":I
    const v2, 0x3f4800

    if-le v10, v2, :cond_9

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-nez v2, :cond_d

    .line 1226
    :cond_9
    if-nez v15, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    :goto_4
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v11, v1, v13, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1217
    .end local v10    # "area":I
    :cond_a
    :goto_5
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 1211
    .end local v11    # "bitmap":Landroid/graphics/Bitmap;
    .end local v15    # "layer":I
    :cond_b
    new-instance v14, Landroid/graphics/RectF;

    invoke-direct {v14}, Landroid/graphics/RectF;-><init>()V

    .line 1212
    .local v14, "dstRelRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v13}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1213
    iget v2, v14, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v2, v4

    iget v4, v14, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, v14, Landroid/graphics/RectF;->right:F

    .line 1214
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget v6, v14, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    .line 1213
    invoke-virtual {v13, v2, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    .line 1226
    .end local v14    # "dstRelRect":Landroid/graphics/RectF;
    .restart local v10    # "area":I
    .restart local v11    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v15    # "layer":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    goto :goto_4

    .line 1232
    :cond_d
    if-nez v15, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    .line 1231
    :goto_6
    move-object/from16 v0, p1

    invoke-static {v0, v11, v3, v12, v2}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_drawBitmapJNI(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)I

    move-result v2

    if-gez v2, :cond_a

    .line 1233
    if-nez v15, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    :goto_7
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v11, v1, v13, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_5

    .line 1232
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    goto :goto_6

    .line 1233
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    goto :goto_7

    .line 1239
    .end local v10    # "area":I
    :cond_10
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v4

    mul-int/2addr v2, v4

    .line 1240
    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v5

    mul-int/2addr v4, v5

    .line 1239
    add-int v10, v2, v4

    .line 1241
    .restart local v10    # "area":I
    const v2, 0x3f4800

    if-le v10, v2, :cond_11

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-nez v2, :cond_12

    .line 1243
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v11, v1, v13, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_5

    .line 1249
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 1248
    move-object/from16 v0, p1

    invoke-static {v0, v11, v3, v12, v2}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_drawBitmapJNI(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)I

    move-result v2

    if-gez v2, :cond_a

    .line 1250
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v11, v1, v13, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_5
.end method

.method private updateCanvasLayer2(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x0

    .line 1138
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1139
    .local v1, "srcRect":Landroid/graphics/Rect;
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v4, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v5, v0

    move-object v0, p0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    .line 1140
    new-instance v7, Landroid/graphics/Rect;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    invoke-direct {v7, v9, v9, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1142
    .local v7, "dstRect":Landroid/graphics/Rect;
    const/4 v8, 0x0

    .local v8, "layer":I
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v8, v0, :cond_0

    .line 1153
    return-void

    .line 1143
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    .line 1144
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_1

    .line 1145
    invoke-virtual {v6, v9, v9}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    invoke-virtual {v6, v9, v9, v0}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 1146
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-nez v0, :cond_3

    .line 1147
    if-nez v8, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    :goto_1
    invoke-virtual {p1, v6, v1, v7, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1142
    :cond_1
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1147
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    goto :goto_1

    .line 1149
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v1, v7, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_2
.end method

.method private updateDottedLine(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "re_rectf"    # Landroid/graphics/RectF;

    .prologue
    const/4 v10, 0x0

    .line 967
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    if-nez v1, :cond_1

    .line 990
    :cond_0
    :goto_0
    return-void

    .line 971
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    if-eqz v1, :cond_0

    .line 972
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 973
    .local v7, "bitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 974
    .local v0, "canvasbuf":Landroid/graphics/Canvas;
    new-instance v6, Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v2, v2

    invoke-direct {v6, v10, v10, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 975
    .local v6, "ab_rectf":Landroid/graphics/RectF;
    invoke-direct {p0, v6, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 977
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    int-to-float v8, v1

    .line 978
    .local v8, "currentLine":F
    :goto_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineHalfWidth:F

    sub-float v1, v8, v1

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    cmpg-float v1, v1, v2

    if-lez v1, :cond_2

    .line 986
    const/4 v1, 0x0

    invoke-virtual {p1, v7, v10, v10, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 987
    const/4 v0, 0x0

    .line 988
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 979
    :cond_2
    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineHalfWidth:F

    add-float/2addr v2, v8

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_3

    .line 980
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float v1, v8, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float v9, v1, v2

    .line 981
    .local v9, "re_line":F
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v2, v9

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    .line 982
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v4, v9

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    .line 981
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 984
    .end local v9    # "re_line":F
    :cond_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    int-to-float v1, v1

    add-float/2addr v8, v1

    goto :goto_1
.end method

.method private updateFloatingLayer(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1113
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-nez v3, :cond_0

    .line 1135
    :goto_0
    return-void

    .line 1117
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5, v5}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v4

    invoke-virtual {v3, v5, v5, v4}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 1119
    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1120
    .local v2, "srcRect":Landroid/graphics/Rect;
    new-instance v0, Landroid/graphics/RectF;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v4, v4

    invoke-direct {v0, v6, v6, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1122
    .local v0, "dstRect":Landroid/graphics/RectF;
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v3, :cond_1

    .line 1123
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float v3, v6, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->left:F

    .line 1124
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->right:F

    .line 1125
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float v3, v6, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->top:F

    .line 1126
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 1134
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v2, v0, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 1128
    :cond_1
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1129
    .local v1, "dstRelRect":Landroid/graphics/RectF;
    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1130
    iget v3, v1, Landroid/graphics/RectF;->left:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget v4, v1, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, v1, Landroid/graphics/RectF;->right:F

    .line 1131
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget v6, v1, Landroid/graphics/RectF;->bottom:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    .line 1130
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_1
.end method

.method private updateHighlight(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "re_rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v9, 0x0

    .line 1084
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    if-nez v6, :cond_0

    .line 1110
    :goto_0
    return-void

    .line 1088
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1089
    .local v2, "count":I
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    .line 1090
    .local v5, "rect":Landroid/graphics/RectF;
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1091
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1092
    .local v1, "canvasbuf":Landroid/graphics/Canvas;
    const/4 v4, 0x0

    .local v4, "pos":I
    :goto_1
    if-lt v4, v2, :cond_1

    .line 1107
    const/4 v6, 0x0

    invoke-virtual {p1, v0, v9, v9, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1108
    const/4 v1, 0x0

    .line 1109
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 1093
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;

    .line 1095
    .local v3, "info":Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;
    iget-object v6, v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->rect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 1096
    iget-object v6, v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->rect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 1097
    iget-object v6, v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->rect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 1098
    iget-object v6, v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->rect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 1099
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v7, v7

    invoke-virtual {v5, v9, v9, v6, v7}, Landroid/graphics/RectF;->intersect(FFFF)Z

    .line 1100
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/RectF;->offset(FF)V

    .line 1101
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v7, v7

    invoke-virtual {v5, v9, v9, v6, v7}, Landroid/graphics/RectF;->intersect(FFFF)Z

    .line 1103
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    iget v7, v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->color:I

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 1104
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    iget v7, v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->size:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v7, v8

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1105
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5, v6}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1092
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method private updateNotepad()V
    .locals 6

    .prologue
    .line 2024
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2025
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setPanAndZoom(FFFFF)V

    .line 2026
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setScreenStart(II)V

    .line 2028
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 2029
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 2030
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 2031
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    .line 2033
    :cond_0
    return-void
.end method


# virtual methods
.method public GetPageEffectWorking()Z
    .locals 1

    .prologue
    .line 1320
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-nez v0, :cond_0

    .line 1321
    const/4 v0, 0x0

    .line 1323
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v0

    goto :goto_0
.end method

.method public GetRtoBmpItstScrHeight()I
    .locals 1

    .prologue
    .line 1347
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    return v0
.end method

.method public GetRtoBmpItstScrWidth()I
    .locals 1

    .prologue
    .line 1343
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    return v0
.end method

.method public GetScreenHeight()I
    .locals 1

    .prologue
    .line 1339
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    return v0
.end method

.method public GetScreenStartX()I
    .locals 1

    .prologue
    .line 1327
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    return v0
.end method

.method public GetScreenStartY()I
    .locals 1

    .prologue
    .line 1331
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    return v0
.end method

.method public GetScreenWidth()I
    .locals 1

    .prologue
    .line 1335
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    return v0
.end method

.method public UpdateCanvas(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V
    .locals 21
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "rectf"    # Landroid/graphics/RectF;
    .param p3, "isScreenFramebuffer"    # Z
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 1356
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    .line 1358
    .local v18, "time":J
    move-wide/from16 v14, v18

    .local v14, "postdraw":J
    move-wide/from16 v10, v18

    .local v10, "engine":J
    move-wide/from16 v16, v18

    .line 1359
    .local v16, "predraw":J
    if-nez p2, :cond_3

    .line 1360
    const-string v2, "SpenInView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Performance onUpdateCanvas start rect = null isScreenFramebuffer = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1368
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mClippingPath:Landroid/graphics/Path;

    if-eqz v2, :cond_0

    .line 1369
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 1370
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mClippingPath:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 1373
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v2, :cond_10

    .line 1374
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1375
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->drawAnimation(Landroid/graphics/Canvas;)V

    .line 1460
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mClippingPath:Landroid/graphics/Path;

    if-eqz v2, :cond_2

    .line 1461
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 1464
    :cond_2
    const-string v2, "SpenInView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Performance onUpdateCanvas end total = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long v4, v4, v18

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms predraw = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1465
    sub-long v4, v16, v18

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms spenview = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v4, v10, v16

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms postdraw = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v4, v14, v10

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1466
    const-string v4, " ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1464
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1467
    return-void

    .line 1362
    :cond_3
    const-string v2, "SpenInView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Performance onUpdateCanvas start rect = ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1363
    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") w = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " h = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1364
    const-string v4, " isScreenFramebuffer = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1362
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1377
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-eqz v2, :cond_5

    .line 1378
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v7, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v8, v3

    move-object/from16 v3, p1

    move-object/from16 v9, p2

    invoke-interface/range {v2 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;->onDraw(Landroid/graphics/Canvas;FFFFFLandroid/graphics/RectF;)V

    .line 1380
    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    .line 1382
    invoke-direct/range {p0 .. p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    .line 1384
    if-nez p2, :cond_b

    .line 1385
    new-instance p2, Landroid/graphics/RectF;

    .end local p2    # "rectf":Landroid/graphics/RectF;
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v5, v5

    move-object/from16 v0, p2

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1386
    .restart local p2    # "rectf":Landroid/graphics/RectF;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v2

    if-eqz v2, :cond_a

    .line 1387
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateFloatingLayer(Landroid/graphics/Canvas;)V

    .line 1395
    :cond_6
    :goto_2
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateHighlight(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V

    .line 1396
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateDottedLine(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V

    .line 1397
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v2

    if-nez v2, :cond_7

    .line 1398
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v2, :cond_c

    .line 1400
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float v13, v2, v3

    .line 1401
    .local v13, "xr":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float v20, v2, v3

    .line 1402
    .local v20, "yr":F
    new-instance v12, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float v3, v3, v20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v13

    .line 1403
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    add-float v5, v5, v20

    .line 1402
    invoke-direct {v12, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1404
    .local v12, "ovalRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1409
    .end local v12    # "ovalRect":Landroid/graphics/RectF;
    .end local v13    # "xr":F
    .end local v20    # "yr":F
    :cond_7
    :goto_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v2

    if-nez v2, :cond_8

    .line 1410
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    instance-of v2, v2, Landroid/view/SurfaceView;

    if-eqz v2, :cond_e

    .line 1411
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    const/high16 v4, 0x41f00000    # 30.0f

    add-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_8

    .line 1412
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v2, :cond_d

    .line 1414
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float v13, v2, v3

    .line 1415
    .restart local v13    # "xr":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float v20, v2, v3

    .line 1416
    .restart local v20    # "yr":F
    new-instance v12, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float v3, v3, v20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    .line 1417
    add-float/2addr v4, v13

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    add-float v5, v5, v20

    .line 1416
    invoke-direct {v12, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1418
    .restart local v12    # "ovalRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1438
    .end local v12    # "ovalRect":Landroid/graphics/RectF;
    .end local v13    # "xr":F
    .end local v20    # "yr":F
    :cond_8
    :goto_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    .line 1440
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-eqz v2, :cond_9

    .line 1441
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v7, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v8, v3

    move-object/from16 v3, p1

    move-object/from16 v9, p2

    invoke-interface/range {v2 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;->onDraw(Landroid/graphics/Canvas;FFFFFLandroid/graphics/RectF;)V

    .line 1443
    :cond_9
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v14

    .line 1445
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->drawScroll(Landroid/graphics/Canvas;)Z

    .line 1447
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1448
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 1388
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isUpdateFloating()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1390
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateFloatingLayer(Landroid/graphics/Canvas;)V

    goto/16 :goto_2

    .line 1393
    :cond_b
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateFloatingLayer(Landroid/graphics/Canvas;)V

    goto/16 :goto_2

    .line 1406
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 1420
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1421
    const-string v2, "SpenInView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Draw remover circle mCirclePoint.x "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mCirclePoint.y "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1422
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mRemoverRadius "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1421
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1426
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v2, :cond_f

    .line 1428
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float v13, v2, v3

    .line 1429
    .restart local v13    # "xr":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float v20, v2, v3

    .line 1430
    .restart local v20    # "yr":F
    new-instance v12, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float v3, v3, v20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v13

    .line 1431
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    add-float v5, v5, v20

    .line 1430
    invoke-direct {v12, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1432
    .restart local v12    # "ovalRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 1434
    .end local v12    # "ovalRect":Landroid/graphics/RectF;
    .end local v13    # "xr":F
    .end local v20    # "yr":F
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 1457
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getColor()I

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_1
.end method

.method public cancelStroke()V
    .locals 4

    .prologue
    .line 4808
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4814
    :goto_0
    return-void

    .line 4812
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    .line 4813
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_cancelStroke(J)V

    goto :goto_0
.end method

.method public cancelStrokeFrame()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 3787
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3788
    :cond_0
    const/16 v1, 0x8

    .line 3789
    const-string v2, "The vies has not SpenPageDoc instance. please use to call setPageDoc."

    .line 3788
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 3791
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    if-eqz v1, :cond_3

    .line 3792
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancelStrokeFrame()Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v0

    .line 3793
    .local v0, "o":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    if-eqz v0, :cond_2

    .line 3794
    const-string v1, "STROKE_FRAME"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getExtraDataInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v3, :cond_4

    .line 3795
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->undoToTag()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    .line 3796
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearHistoryTag()V

    .line 3802
    :cond_2
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    .line 3804
    .end local v0    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    :cond_3
    return-void

    .line 3797
    .restart local v0    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    :cond_4
    const-string v1, "STROKE_FRAME"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getExtraDataInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 3798
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setVisibility(Z)V

    .line 3799
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    goto :goto_0
.end method

.method public captureCurrentView(Z)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "includeBackground"    # Z

    .prologue
    const/4 v3, 0x0

    .line 2678
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    move-object v0, v3

    .line 2704
    :goto_0
    return-object v0

    .line 2681
    :cond_0
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    if-nez v4, :cond_2

    .line 2682
    :cond_1
    const-string v4, "SpenInView"

    const-string v5, "Not yet to create view"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2683
    const/4 v4, 0x4

    const-string v5, " : ScreenWidth or ScreenHeight is zero."

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    move-object v0, v3

    .line 2684
    goto :goto_0

    .line 2686
    :cond_2
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_updateAllScreenFrameBuffer(J)V

    .line 2687
    const/4 v0, 0x0

    .line 2689
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez p1, :cond_4

    .line 2690
    :try_start_0
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2698
    :goto_1
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2699
    .local v1, "canvas":Landroid/graphics/Canvas;
    if-nez p1, :cond_3

    .line 2700
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    neg-int v4, v4

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2702
    :cond_3
    const/4 v4, 0x1

    invoke-direct {p0, v1, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    .line 2703
    const/4 v1, 0x0

    .line 2704
    goto :goto_0

    .line 2692
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    :cond_4
    :try_start_1
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 2694
    :catch_0
    move-exception v2

    .line 2695
    .local v2, "e":Ljava/lang/Throwable;
    const-string v4, "SpenInView"

    const-string v5, "Failed to create bitmap"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2696
    const/4 v4, 0x2

    const-string v5, " : fail createBitmap."

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public capturePage(F)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    const/4 v1, 0x0

    .line 2726
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2727
    :cond_0
    const/4 v0, 0x0

    .line 2730
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    .line 2731
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    const/4 v3, 0x1

    .line 2730
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public capturePage(FZ)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "ratio"    # F
    .param p2, "isTransparent"    # Z

    .prologue
    const/4 v8, 0x0

    .line 2735
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v10, 0x0

    cmp-long v1, v2, v10

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2757
    :cond_0
    :goto_0
    return-object v8

    .line 2738
    :cond_1
    if-nez p2, :cond_2

    .line 2739
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->capturePage(F)Landroid/graphics/Bitmap;

    move-result-object v8

    goto :goto_0

    .line 2741
    :cond_2
    const/4 v0, 0x0

    .line 2743
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2748
    :goto_1
    if-eqz v0, :cond_0

    .line 2749
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2750
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2751
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v4, 0x6

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 2752
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    .line 2753
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    const/4 v3, 0x1

    .line 2752
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 2754
    .local v8, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 2744
    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v8    # "result":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v7

    .line 2745
    .local v7, "e":Ljava/lang/Throwable;
    const-string v1, "SpenInView"

    const-string v2, "Failed to create bitmap"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2746
    const/4 v1, 0x2

    const-string v2, " : fail createBitmap."

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public changeStrokeFrame(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
    .locals 4
    .param p1, "container"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3807
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3808
    :cond_0
    const/16 v0, 0x8

    .line 3809
    const-string v1, "The vies has not SpenPageDoc instance. please use to call setPageDoc."

    .line 3808
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 3812
    :cond_1
    if-nez p1, :cond_2

    .line 3813
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "container is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3816
    :cond_2
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3817
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    .line 3818
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    .line 3823
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    .line 3824
    return-void

    .line 3820
    :cond_3
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    .line 3821
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    goto :goto_0
.end method

.method public clearHighlight()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 4435
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4443
    :cond_0
    :goto_0
    return-void

    .line 4438
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 4439
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    .line 4440
    const/4 v0, 0x1

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public close()V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 561
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    if-eqz v1, :cond_0

    .line 562
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 563
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    .line 565
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 567
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v1

    if-eqz v1, :cond_1

    .line 568
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->stopReplay()V

    .line 571
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_2

    .line 572
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_finalize(J)V

    .line 573
    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    .line 576
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    .line 577
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 578
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    .line 581
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 582
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_12

    .line 587
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 588
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    .line 591
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_6

    .line 592
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 593
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    .line 596
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v1, :cond_7

    .line 597
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->close()V

    .line 598
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 601
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v1, :cond_8

    .line 602
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->close()V

    .line 603
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    .line 605
    :cond_8
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 606
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 607
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    .line 608
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v1, :cond_9

    .line 609
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->close()V

    .line 610
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 612
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v1, :cond_a

    .line 613
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, v4}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 614
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 616
    :cond_a
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v1, :cond_b

    .line 617
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->close()V

    .line 618
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 620
    :cond_b
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    if-eqz v1, :cond_c

    .line 621
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V

    .line 622
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    .line 624
    :cond_c
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    .line 625
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    .line 626
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    .line 627
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    .line 628
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 629
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    .line 630
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    .line 631
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    .line 632
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    .line 634
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 635
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 636
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 637
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 638
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .line 639
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    .line 641
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    .line 642
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .line 643
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    .line 644
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 645
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .line 646
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .line 647
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .line 648
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 649
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 650
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    .line 651
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    .line 652
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 653
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    .line 654
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    .line 655
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    .line 656
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    .line 657
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 658
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 659
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 660
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 661
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 662
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    .line 663
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    .line 664
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 665
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    .line 666
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    if-eqz v1, :cond_d

    .line 667
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 668
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    .line 670
    :cond_d
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    if-eqz v1, :cond_e

    .line 671
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 672
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    .line 674
    :cond_e
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateHandler:Landroid/os/Handler;

    if-eqz v1, :cond_f

    .line 675
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 676
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateHandler:Landroid/os/Handler;

    .line 679
    :cond_f
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v1, :cond_10

    .line 680
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->stop()V

    .line 681
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->close()V

    .line 682
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    .line 684
    :cond_10
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    .line 685
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    .line 686
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    .line 687
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContextMenu:Landroid/view/ContextMenu;

    .line 688
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 689
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    .line 690
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    .line 691
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mClippingPath:Landroid/graphics/Path;

    .line 692
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-eqz v1, :cond_11

    .line 693
    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_deinit_c2dJNI()V

    .line 697
    :cond_11
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->releaseHapticFeedback()V

    .line 698
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->unregisterPensoundSolution()V

    .line 699
    return-void

    .line 582
    :cond_12
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 583
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_4

    .line 584
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0
.end method

.method public closeControl()V
    .locals 6

    .prologue
    .line 4195
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTransactionClosingControl:Z

    if-eqz v1, :cond_1

    .line 4209
    :cond_0
    :goto_0
    return-void

    .line 4199
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTransactionClosingControl:Z

    .line 4201
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v1, :cond_2

    .line 4203
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4208
    :cond_2
    :goto_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTransactionClosingControl:Z

    goto :goto_0

    .line 4204
    :catch_0
    move-exception v0

    .line 4205
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public drawObjectList(Ljava/util/ArrayList;)Landroid/graphics/Bitmap;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v10, 0x0

    .line 4784
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move-object v4, v10

    .line 4804
    :cond_1
    :goto_0
    return-object v4

    .line 4788
    :cond_2
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4789
    .local v0, "dstRect":Landroid/graphics/RectF;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 4793
    const/4 v4, 0x0

    .line 4795
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 4796
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_drawObjectList(JLandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_1

    :goto_2
    move-object v4, v10

    .line 4804
    goto :goto_0

    .line 4789
    .end local v4    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 4790
    .local v8, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getDrawnRect()Landroid/graphics/RectF;

    move-result-object v9

    .line 4791
    .local v9, "srcRect":Landroid/graphics/RectF;
    invoke-virtual {p0, v0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->joinRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_1

    .line 4799
    .end local v8    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .end local v9    # "srcRect":Landroid/graphics/RectF;
    .restart local v4    # "bitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v7

    .line 4800
    .local v7, "e":Ljava/lang/Exception;
    const-string v1, "SpenInView"

    const-string v2, "Failed to create bitmap"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4801
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_2
.end method

.method protected findObjectInRect(ILandroid/graphics/RectF;Z)Ljava/util/ArrayList;
    .locals 1
    .param p1, "typeFilter"    # I
    .param p2, "rectf"    # Landroid/graphics/RectF;
    .param p3, "allAreas"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/RectF;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5220
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    .line 5221
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->findObjectInRect(ILandroid/graphics/RectF;Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 5223
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBlankColor()I
    .locals 4

    .prologue
    .line 3132
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3133
    const/4 v0, 0x0

    .line 3135
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasHeight()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3163
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3166
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasWidth()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3150
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3153
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public getControl()Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    .locals 4

    .prologue
    .line 4181
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4182
    const/4 v0, 0x0

    .line 4184
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    goto :goto_0
.end method

.method public getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 6

    .prologue
    .line 3456
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3457
    const/4 v0, 0x0

    .line 3465
    :cond_0
    :goto_0
    return-object v0

    .line 3459
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    .line 3460
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-eqz v1, :cond_0

    .line 3461
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    .line 3462
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    goto :goto_0
.end method

.method public getFrameStartPosition()Landroid/graphics/PointF;
    .locals 4

    .prologue
    .line 2976
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2977
    const/4 v0, 0x0

    .line 2979
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0
.end method

.method public getMaxZoomRatio()F
    .locals 4

    .prologue
    .line 3011
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3012
    const/4 v0, 0x0

    .line 3014
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getMaxZoomRatio(J)F

    move-result v0

    goto :goto_0
.end method

.method public getMinZoomRatio()F
    .locals 4

    .prologue
    .line 3046
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3047
    const/4 v0, 0x0

    .line 3049
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getMinZoomRatio(J)F

    move-result v0

    goto :goto_0
.end method

.method public getPan()Landroid/graphics/PointF;
    .locals 6

    .prologue
    .line 3091
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 3092
    const/4 v0, 0x0

    .line 3096
    :goto_0
    return-object v0

    .line 3094
    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 3095
    .local v0, "point":Landroid/graphics/PointF;
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v2, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getPan(JLandroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 6

    .prologue
    .line 3368
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3369
    const/4 v0, 0x0

    .line 3380
    :cond_0
    :goto_0
    return-object v0

    .line 3371
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    .line 3372
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v1, :cond_0

    .line 3373
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 3374
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 3375
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 3376
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    .line 3377
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 6

    .prologue
    .line 3541
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3542
    const/4 v0, 0x0

    .line 3550
    :cond_0
    :goto_0
    return-object v0

    .line 3544
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    .line 3545
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-eqz v1, :cond_0

    .line 3546
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 3547
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    goto :goto_0
.end method

.method public getReplayState()I
    .locals 4

    .prologue
    .line 4280
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4281
    const/4 v0, 0x0

    .line 4283
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getReplayState(J)I

    move-result v0

    goto :goto_0
.end method

.method public getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    .locals 6

    .prologue
    .line 3603
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3604
    const/4 v0, 0x0

    .line 3611
    :cond_0
    :goto_0
    return-object v0

    .line 3606
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;-><init>()V

    .line 3607
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    if-eqz v1, :cond_0

    .line 3608
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;->type:I

    goto :goto_0
.end method

.method public getTemporaryStroke()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4481
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 4482
    const/4 v0, 0x0

    .line 4487
    :goto_0
    return-object v0

    .line 4484
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4485
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v2, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getTemporaryStroke(JLjava/util/ArrayList;)V

    goto :goto_0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 6

    .prologue
    .line 3262
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3263
    const/4 v0, 0x0

    .line 3283
    :cond_0
    :goto_0
    return-object v0

    .line 3265
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v1, :cond_2

    .line 3266
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v1, :cond_2

    .line 3267
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    goto :goto_0

    .line 3271
    :cond_2
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    .line 3272
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getCanvasWidth()I

    move-result v2

    int-to-double v2, v2

    const-wide/high16 v4, 0x4069000000000000L    # 200.0

    div-double/2addr v2, v4

    double-to-float v2, v2

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 3273
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    if-eqz v1, :cond_0

    .line 3274
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 3275
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 3276
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 3277
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 3278
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 3279
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 3280
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    goto :goto_0
.end method

.method public getToolTypeAction(I)I
    .locals 4
    .param p1, "toolType"    # I

    .prologue
    .line 2874
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2875
    const/4 v0, 0x0

    .line 2877
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getToolTypeAction(JI)I

    move-result v0

    goto :goto_0
.end method

.method public getZoomPadBoxHeight()F
    .locals 4

    .prologue
    .line 4632
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 4633
    :cond_0
    const/4 v0, 0x0

    .line 4636
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getBoxHeight()F

    move-result v0

    goto :goto_0
.end method

.method public getZoomPadBoxPosition()Landroid/graphics/PointF;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 4649
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v1, :cond_1

    .line 4650
    :cond_0
    const/4 v0, 0x0

    .line 4658
    :goto_0
    return-object v0

    .line 4653
    :cond_1
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v6, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 4654
    .local v0, "point":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRectF()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 4655
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRectF()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 4657
    const-string v1, "ZoomPad"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getZoomPadBoxPosition ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getZoomPadBoxRect()Landroid/graphics/RectF;
    .locals 4

    .prologue
    .line 4597
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 4598
    :cond_0
    const/4 v0, 0x0

    .line 4601
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRectF()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoomPadPosition()Landroid/graphics/PointF;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 4671
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v1, :cond_1

    .line 4672
    :cond_0
    const/4 v0, 0x0

    .line 4680
    :goto_0
    return-object v0

    .line 4675
    :cond_1
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v6, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 4676
    .local v0, "point":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 4677
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 4679
    const-string v1, "ZoomPad"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getZoomPadPosition ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getZoomPadRect()Landroid/graphics/RectF;
    .locals 4

    .prologue
    .line 4605
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 4606
    :cond_0
    const/4 v0, 0x0

    .line 4611
    :goto_0
    return-object v0

    .line 4609
    :cond_1
    const-string v0, "ZoomPad"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getZoomPadRect ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 4610
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4609
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4611
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoomRatio()F
    .locals 4

    .prologue
    .line 2959
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2960
    const/4 v0, 0x0

    .line 2962
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getZoomRatio(J)F

    move-result v0

    goto :goto_0
.end method

.method public isDottedLineEnabled()Z
    .locals 4

    .prologue
    .line 4399
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4400
    const/4 v0, 0x0

    .line 4402
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    goto :goto_0
.end method

.method public isEditableTextBox()Z
    .locals 1

    .prologue
    .line 4590
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isEditable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4591
    const/4 v0, 0x1

    .line 4593
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHorizontalScrollBarEnabled()Z
    .locals 6

    .prologue
    .line 4549
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 4550
    const/4 v0, 0x0

    .line 4556
    :cond_0
    :goto_0
    return v0

    .line 4552
    :cond_1
    const/4 v0, 0x0

    .line 4553
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v1, :cond_0

    .line 4554
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->isHorizontalScroll()Z

    move-result v0

    goto :goto_0
.end method

.method public isHorizontalSmartScrollEnabled()Z
    .locals 1

    .prologue
    .line 4042
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartHorizontal:Z

    return v0
.end method

.method public isHyperTextViewEnabled()Z
    .locals 1

    .prologue
    .line 4106
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsHyperText:Z

    return v0
.end method

.method public isLongPressEnabled()Z
    .locals 4

    .prologue
    .line 2615
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2616
    const/4 v0, 0x0

    .line 2618
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsLongPressEnable:Z

    goto :goto_0
.end method

.method public isPenButtonSelectionEnabled()Z
    .locals 1

    .prologue
    .line 4826
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsPenButtonSelectionEnabled:Z

    return v0
.end method

.method public isScrollBarEnabled()Z
    .locals 6

    .prologue
    .line 4519
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 4520
    const/4 v0, 0x0

    .line 4526
    :cond_0
    :goto_0
    return v0

    .line 4522
    :cond_1
    const/4 v0, 0x0

    .line 4523
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v1, :cond_0

    .line 4524
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->isScroll()Z

    move-result v0

    goto :goto_0
.end method

.method public isSmartScaleEnabled()Z
    .locals 1

    .prologue
    .line 3990
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartScale:Z

    return v0
.end method

.method public isTextCursorEnabled()Z
    .locals 4

    .prologue
    .line 2630
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2631
    const/4 v0, 0x0

    .line 2633
    :goto_0
    return v0

    :cond_0
    sget-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverCursorEnable:Z

    goto :goto_0
.end method

.method public isToolTipEnabled()Z
    .locals 1

    .prologue
    .line 4118
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    return v0
.end method

.method public isVerticalScrollBarEnabled()Z
    .locals 6

    .prologue
    .line 4579
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 4580
    const/4 v0, 0x0

    .line 4586
    :cond_0
    :goto_0
    return v0

    .line 4582
    :cond_1
    const/4 v0, 0x0

    .line 4583
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v1, :cond_0

    .line 4584
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->isVerticalScroll()Z

    move-result v0

    goto :goto_0
.end method

.method public isVerticalSmartScrollEnabled()Z
    .locals 1

    .prologue
    .line 4093
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartVertical:Z

    return v0
.end method

.method public isZoomPadEnabled()Z
    .locals 4

    .prologue
    .line 4718
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 4719
    :cond_0
    const/4 v0, 0x0

    .line 4722
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isZoomPadStroking()Z
    .locals 4

    .prologue
    .line 4615
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 4616
    :cond_0
    const/4 v0, 0x0

    .line 4619
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isStroking()Z

    move-result v0

    goto :goto_0
.end method

.method public isZoomable()Z
    .locals 4

    .prologue
    .line 2911
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2912
    const/4 v0, 0x0

    .line 2914
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_isZoomable(J)Z

    move-result v0

    goto :goto_0
.end method

.method joinRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dst"    # Landroid/graphics/RectF;
    .param p2, "src"    # Landroid/graphics/RectF;

    .prologue
    .line 878
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 904
    :cond_0
    :goto_0
    return-void

    .line 881
    :cond_1
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-gez v0, :cond_0

    .line 885
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-gez v0, :cond_2

    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    .line 886
    :cond_2
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 887
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 888
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 889
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0

    .line 891
    :cond_3
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 892
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 894
    :cond_4
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 895
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 897
    :cond_5
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    .line 898
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 900
    :cond_6
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 901
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2565
    if-eqz p1, :cond_0

    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-nez v10, :cond_1

    .line 2566
    :cond_0
    const/4 v10, 0x1

    .line 2611
    :goto_0
    return v10

    .line 2568
    :cond_1
    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v12, 0x0

    invoke-virtual {p1, v12}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v12

    invoke-direct {p0, v10, v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getToolTypeAction(JI)I

    move-result v1

    .line 2569
    .local v1, "toolTypeAction":I
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isEnabled()Z

    move-result v10

    if-nez v10, :cond_3

    .line 2570
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    const/16 v11, 0x9

    if-ne v10, v11, :cond_5

    const/4 v10, 0x7

    if-eq v1, v10, :cond_5

    .line 2571
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setCustomHoveringIcon()I

    move-result v10

    iput v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverIconID:I

    .line 2576
    :cond_2
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    const/16 v11, 0x9

    if-ne v10, v11, :cond_3

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v10, :cond_3

    .line 2577
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v10

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v10, :cond_3

    .line 2578
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v10

    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v11, v10}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isObjectContained(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 2579
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onCanvasHoverEnter()V

    .line 2583
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 2584
    .local v0, "action":I
    const/16 v10, 0x9

    if-ne v0, v10, :cond_4

    .line 2585
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v12

    sub-long v2, v10, v12

    .line 2586
    .local v2, "diffTime":J
    const-wide/16 v10, 0x64

    iget-wide v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    add-long/2addr v10, v12

    cmp-long v10, v2, v10

    if-lez v10, :cond_6

    const/4 v10, 0x1

    :goto_2
    iput-boolean v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    .line 2587
    iget-boolean v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    if-eqz v10, :cond_4

    .line 2588
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    .line 2589
    .local v6, "eventTime":J
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    .line 2590
    .local v4, "downTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 2591
    .local v8, "systemTime":J
    const-string v10, "SpenInView"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "skiptouch hover action = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " eventTime = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " downTime = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 2592
    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " systemTime = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " diffTime = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 2591
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2596
    .end local v2    # "diffTime":J
    .end local v4    # "downTime":J
    .end local v6    # "eventTime":J
    .end local v8    # "systemTime":J
    :cond_4
    iget-boolean v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    if-eqz v10, :cond_7

    .line 2598
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 2572
    .end local v0    # "action":I
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    const/16 v11, 0xa

    if-ne v10, v11, :cond_2

    .line 2573
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverIconID:I

    invoke-direct {p0, v10}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removeHoveringIcon(I)Z

    goto/16 :goto_1

    .line 2586
    .restart local v0    # "action":I
    .restart local v2    # "diffTime":J
    :cond_6
    const/4 v10, 0x0

    goto :goto_2

    .line 2601
    .end local v2    # "diffTime":J
    :cond_7
    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v12, 0x0

    invoke-virtual {p1, v12}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v12

    invoke-direct {p0, v10, v11, p1, v12}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onHover(JLandroid/view/MotionEvent;I)Z

    .line 2603
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-nez v10, :cond_8

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v10, :cond_8

    .line 2604
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v10, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onHoverEvent(Landroid/view/MotionEvent;)V

    .line 2607
    :cond_8
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    if-eqz v10, :cond_9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    invoke-interface {v10, v11, p1}, Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;->onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 2608
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 2611
    :cond_9
    const/4 v10, 0x1

    goto/16 :goto_0
.end method

.method protected declared-synchronized onLayout(ZIIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 16
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "parentLayoutRect"    # Landroid/graphics/Rect;
    .param p7, "windowVisibleRect"    # Landroid/graphics/Rect;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    .prologue
    .line 739
    monitor-enter p0

    sub-int v6, p4, p2

    .line 740
    .local v6, "w":I
    sub-int v7, p5, p3

    .line 741
    .local v7, "h":I
    if-nez p7, :cond_1

    move v10, v7

    .line 744
    .local v10, "hExceptSIP":I
    :goto_0
    if-eqz v6, :cond_0

    if-nez v7, :cond_2

    .line 824
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 741
    .end local v10    # "hExceptSIP":I
    :cond_1
    :try_start_0
    move-object/from16 v0, p7

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 742
    move-object/from16 v0, p6

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    add-int/2addr v4, v5

    sub-int v10, v3, v4

    goto :goto_0

    .line 746
    .restart local v10    # "hExceptSIP":I
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutWidth:I

    if-ne v6, v3, :cond_3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutHeight:I

    if-ne v7, v3, :cond_3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutExceptSIP:I

    if-eq v10, v3, :cond_0

    .line 750
    :cond_3
    const/16 v2, 0xc8

    .line 751
    .local v2, "delayTime":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutWidth:I

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutHeight:I

    if-nez v3, :cond_5

    .line 752
    :cond_4
    const/4 v2, 0x0

    .line 754
    :cond_5
    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutWidth:I

    .line 755
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutHeight:I

    .line 756
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutExceptSIP:I

    .line 762
    const-string v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onLayout("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 763
    const-string v3, "onLayout. parentLayoutRect : "

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->printRect(Ljava/lang/String;Landroid/graphics/Rect;)V

    .line 764
    if-eqz p7, :cond_6

    .line 765
    const-string v3, "onLayout. windowVisibleRect : "

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->printRect(Ljava/lang/String;Landroid/graphics/Rect;)V

    .line 766
    const-string v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onLayout. hExceptSIP : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    :cond_6
    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    .line 770
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    .line 771
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    .line 773
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v3, :cond_7

    .line 774
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v3, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->setScreenSize(II)V

    .line 776
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v3, :cond_8

    .line 777
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v3, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setScreenResolution(II)V

    .line 779
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v3, :cond_9

    .line 780
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-object/from16 v0, p6

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setParentRect(Landroid/graphics/Rect;)V

    .line 783
    :cond_9
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setScreenSize(JIII)V

    .line 784
    const/4 v11, 0x0

    .line 785
    .local v11, "isScreenFBCreate":Z
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    if-le v3, v4, :cond_d

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    .line 786
    .local v13, "newFramebufferWidth":I
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    if-le v3, v4, :cond_e

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    .line 787
    .local v12, "newFramebufferHeight":I
    :goto_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    if-gt v13, v3, :cond_a

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    if-le v12, v3, :cond_c

    .line 788
    :cond_a
    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    .line 789
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    .line 791
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_b

    .line 792
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795
    :cond_b
    :try_start_1
    const-string v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onLayout. ScreenFB W : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", H : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    .line 797
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setScreenFrameBuffer(JLandroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 798
    const/4 v11, 0x1

    .line 804
    :cond_c
    :goto_4
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEditableTextBox()Z

    move-result v3

    if-eqz v3, :cond_f

    if-nez v11, :cond_f

    .line 805
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 739
    .end local v2    # "delayTime":I
    .end local v10    # "hExceptSIP":I
    .end local v11    # "isScreenFBCreate":Z
    .end local v12    # "newFramebufferHeight":I
    .end local v13    # "newFramebufferWidth":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 785
    .restart local v2    # "delayTime":I
    .restart local v10    # "hExceptSIP":I
    .restart local v11    # "isScreenFBCreate":Z
    :cond_d
    :try_start_3
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    goto/16 :goto_2

    .line 786
    .restart local v13    # "newFramebufferWidth":I
    :cond_e
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    goto/16 :goto_3

    .line 799
    .restart local v12    # "newFramebufferHeight":I
    :catch_0
    move-exception v9

    .line 800
    .local v9, "e":Ljava/lang/Throwable;
    const-string v3, "SpenInView"

    const-string v4, "Failed to create bitmap"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_4

    .line 807
    .end local v9    # "e":Ljava/lang/Throwable;
    :cond_f
    if-nez v2, :cond_10

    .line 808
    const-string v3, "SpenInView"

    const-string v4, "onLayout. delayTime = 0"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    .line 810
    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto/16 :goto_1

    .line 812
    :cond_10
    const-string v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onLayout. delayTime = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    .line 814
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    .line 820
    int-to-long v14, v2

    .line 814
    invoke-virtual {v3, v4, v14, v15}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 36
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2276
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-wide/from16 v32, v0

    const-wide/16 v34, 0x0

    cmp-long v31, v32, v34

    if-nez v31, :cond_1

    .line 2277
    :cond_0
    const/16 v31, 0x1

    .line 2544
    :goto_0
    return v31

    .line 2280
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v31

    move/from16 v0, v31

    and-int/lit16 v6, v0, 0xff

    .line 2281
    .local v6, "action":I
    if-nez v6, :cond_2

    .line 2282
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsCancelFling:Z

    .line 2284
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v31

    const/16 v32, 0x2

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_3

    .line 2285
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsCancelFling:Z

    .line 2287
    :cond_3
    const/16 v31, 0x1

    move/from16 v0, v31

    if-eq v6, v0, :cond_4

    const/16 v31, 0x3

    move/from16 v0, v31

    if-eq v6, v0, :cond_4

    .line 2288
    const/16 v31, 0x5

    move/from16 v0, v31

    if-ne v6, v0, :cond_5

    .line 2289
    :cond_4
    const/high16 v31, -0x3d380000    # -100.0f

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    .line 2290
    const/high16 v31, -0x3d380000    # -100.0f

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    .line 2291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    const/high16 v32, -0x3d380000    # -100.0f

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    const/high16 v32, -0x3d380000    # -100.0f

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2294
    :cond_5
    if-nez v6, :cond_8

    .line 2295
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v32

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v34

    sub-long v8, v32, v34

    .line 2296
    .local v8, "diffTime":J
    const-wide/16 v32, 0x258

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    move-wide/from16 v34, v0

    add-long v32, v32, v34

    cmp-long v31, v8, v32

    if-lez v31, :cond_f

    const/16 v31, 0x1

    :goto_1
    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    .line 2297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-object/from16 v31, v0

    if-eqz v31, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v31

    if-eqz v31, :cond_6

    .line 2298
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    .line 2300
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    move/from16 v31, v0

    if-eqz v31, :cond_7

    .line 2301
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v14

    .line 2302
    .local v14, "eventTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v10

    .line 2303
    .local v10, "downTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v24

    .line 2304
    .local v24, "systemTime":J
    const-string v31, "SpenInView"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "skiptouch action = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " eventTime = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " downTime = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    .line 2305
    const-string v33, " systemTime = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " diffTime = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " mTouchProcessingTime = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    .line 2306
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    move-wide/from16 v34, v0

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 2304
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2308
    .end local v10    # "downTime":J
    .end local v14    # "eventTime":J
    .end local v24    # "systemTime":J
    :cond_7
    const-wide/16 v32, 0x0

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    .line 2310
    .end local v8    # "diffTime":J
    :cond_8
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTouchPre:Z

    .line 2311
    const/16 v31, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v31

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v31

    const/16 v32, 0x7

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_9

    .line 2312
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTouchPre:Z

    .line 2314
    :cond_9
    if-eqz v6, :cond_a

    const/16 v31, 0xd3

    move/from16 v0, v31

    if-ne v6, v0, :cond_b

    .line 2315
    :cond_a
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    .line 2317
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v31, v0

    if-eqz v31, :cond_12

    .line 2318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isTouchEnabled()Z

    move-result v31

    if-eqz v31, :cond_12

    .line 2319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getStyle()I

    move-result v31

    const/16 v32, 0x3

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_12

    .line 2320
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    move/from16 v31, v0

    if-eqz v31, :cond_10

    .line 2321
    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v6, v0, :cond_c

    .line 2322
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    .line 2324
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2326
    const/16 v31, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 2333
    :cond_d
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v31, v0

    if-eqz v31, :cond_e

    .line 2334
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v31, v0

    move/from16 v0, v31

    neg-int v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v32, v0

    move/from16 v0, v32

    neg-int v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v32, v0

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2337
    :cond_e
    const/16 v31, 0x1

    goto/16 :goto_0

    .line 2296
    .restart local v8    # "diffTime":J
    :cond_f
    const/16 v31, 0x0

    goto/16 :goto_1

    .line 2328
    .end local v8    # "diffTime":J
    :cond_10
    const/16 v31, 0x1

    move/from16 v0, v31

    if-eq v6, v0, :cond_11

    const/16 v31, 0xd4

    move/from16 v0, v31

    if-ne v6, v0, :cond_d

    .line 2329
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 2330
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTouchPre:Z

    goto :goto_2

    .line 2341
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    move/from16 v31, v0

    if-nez v31, :cond_3d

    .line 2342
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-wide/from16 v32, v0

    const/16 v31, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v31

    move-object/from16 v0, p0

    move-wide/from16 v1, v32

    move/from16 v3, v31

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getToolTypeAction(JI)I

    move-result v30

    .line 2344
    .local v30, "toolTypeAction":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v17

    .line 2345
    .local v17, "originAction":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    .line 2346
    .local v7, "newX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v16

    .line 2348
    .local v16, "newY":F
    const/16 v31, 0x5

    move/from16 v0, v31

    if-ne v6, v0, :cond_14

    .line 2349
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    move/from16 v31, v0

    add-int/lit8 v31, v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    .line 2350
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_14

    .line 2351
    const/16 v31, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 2352
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    move/from16 v31, v0

    if-eqz v31, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    if-eqz v31, :cond_13

    .line 2353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/audio/SmpsManager;->generateSound(Landroid/view/MotionEvent;)V

    .line 2355
    :cond_13
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 2356
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    move/from16 v31, v0

    add-int/lit8 v31, v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    .line 2360
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    move/from16 v31, v0

    if-nez v31, :cond_1c

    .line 2361
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v31

    if-nez v31, :cond_1c

    .line 2362
    const/16 v31, 0x2

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_15

    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_15

    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_1c

    .line 2363
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    move/from16 v31, v0

    if-eqz v31, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    if-eqz v31, :cond_17

    .line 2364
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_16

    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_17

    .line 2365
    :cond_16
    if-nez v6, :cond_27

    .line 2366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexEraser:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 2367
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_26

    .line 2368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    move/from16 v32, v0

    const-string v33, "Eraser"

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v33

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    div-float v32, v32, v33

    move/from16 v0, v32

    float-to-double v0, v0

    move-wide/from16 v32, v0

    invoke-virtual/range {v31 .. v33}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    .line 2379
    :cond_17
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v26

    .line 2380
    .local v26, "tempX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v27

    .line 2381
    .local v27, "tempY":F
    move/from16 v18, v26

    .line 2382
    .local v18, "originX":F
    move/from16 v19, v27

    .line 2384
    .local v19, "originY":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    cmpg-float v31, v26, v31

    if-gez v31, :cond_28

    .line 2385
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v26, v0

    .line 2386
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    .line 2392
    :cond_18
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    cmpg-float v31, v27, v31

    if-gez v31, :cond_29

    .line 2393
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v27, v0

    .line 2394
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    .line 2400
    :cond_19
    :goto_5
    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 2402
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    move/from16 v31, v0

    if-eqz v31, :cond_2d

    .line 2403
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    move/from16 v31, v0

    if-eqz v31, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    if-eqz v31, :cond_1a

    .line 2404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/audio/SmpsManager;->generateSound(Landroid/view/MotionEvent;)V

    .line 2407
    :cond_1a
    if-nez v6, :cond_2a

    .line 2408
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldX:F

    .line 2409
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldY:F

    .line 2428
    :cond_1b
    :goto_6
    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 2429
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    .line 2432
    .end local v18    # "originX":F
    .end local v19    # "originY":F
    .end local v26    # "tempX":F
    .end local v27    # "tempY":F
    :cond_1c
    const/16 v31, 0x6

    move/from16 v0, v31

    if-eq v6, v0, :cond_1d

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_1e

    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v6, v0, :cond_1e

    .line 2433
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    move/from16 v31, v0

    add-int/lit8 v31, v31, -0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    .line 2436
    :cond_1e
    if-nez v6, :cond_1f

    .line 2437
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    .line 2438
    const/16 v31, 0x2

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_2e

    .line 2439
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    .line 2440
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Path;->reset()V

    .line 2441
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v32

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v33

    invoke-virtual/range {v31 .. v33}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2447
    :cond_1f
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    move/from16 v31, v0

    if-nez v31, :cond_20

    const/16 v31, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v31

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v31

    const/16 v32, 0x3

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_22

    .line 2448
    :cond_20
    const/16 v31, 0x1

    move/from16 v0, v31

    if-eq v6, v0, :cond_21

    const/16 v31, 0x3

    move/from16 v0, v31

    if-eq v6, v0, :cond_21

    .line 2449
    const/16 v31, 0x5

    move/from16 v0, v31

    if-eq v6, v0, :cond_21

    const/16 v31, 0xd3

    move/from16 v0, v31

    if-eq v6, v0, :cond_21

    .line 2450
    const/16 v31, 0xd4

    move/from16 v0, v31

    if-eq v6, v0, :cond_21

    const/16 v31, 0xd5

    move/from16 v0, v31

    if-ne v6, v0, :cond_2f

    .line 2451
    :cond_21
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    .line 2457
    :cond_22
    :goto_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v31, v0

    move/from16 v0, v31

    neg-int v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v32, v0

    move/from16 v0, v32

    neg-int v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v32, v0

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2459
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-object/from16 v31, v0

    if-eqz v31, :cond_23

    .line 2460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 2462
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v31, v0

    if-eqz v31, :cond_24

    .line 2463
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2466
    :cond_24
    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v6, v0, :cond_25

    const/16 v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_25

    .line 2467
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-virtual/range {v31 .. v32}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2470
    :cond_25
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v28

    .line 2472
    .local v28, "time":J
    move-wide/from16 v20, v28

    .local v20, "posttouch":J
    move-wide/from16 v12, v28

    .local v12, "engine":J
    move-wide/from16 v22, v28

    .line 2473
    .local v22, "pretouch":J
    const-string v31, "SpenInView"

    const-string v32, "Performance touch process start"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2474
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-object/from16 v31, v0

    if-eqz v31, :cond_30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    move-object/from16 v32, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, p1

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v31

    if-eqz v31, :cond_30

    .line 2475
    const-string v31, "SpenInView"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "Performance pretouch listener has consumed action = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2476
    const/16 v31, 0x1

    goto/16 :goto_0

    .line 2370
    .end local v12    # "engine":J
    .end local v20    # "posttouch":J
    .end local v22    # "pretouch":J
    .end local v28    # "time":J
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    move/from16 v32, v0

    const-string v33, "Eraser"

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v33

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    div-float v32, v32, v33

    move/from16 v0, v32

    float-to-double v0, v0

    move-wide/from16 v32, v0

    invoke-virtual/range {v31 .. v33}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    goto/16 :goto_3

    .line 2372
    :cond_27
    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v6, v0, :cond_17

    .line 2373
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 2374
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    move/from16 v32, v0

    .line 2375
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v33

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    div-float v32, v32, v33

    move/from16 v0, v32

    float-to-double v0, v0

    move-wide/from16 v32, v0

    .line 2374
    invoke-virtual/range {v31 .. v33}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    goto/16 :goto_3

    .line 2387
    .restart local v18    # "originX":F
    .restart local v19    # "originY":F
    .restart local v26    # "tempX":F
    .restart local v27    # "tempY":F
    :cond_28
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    move/from16 v32, v0

    add-int v31, v31, v32

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    cmpl-float v31, v26, v31

    if-lez v31, :cond_18

    .line 2388
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    move/from16 v32, v0

    add-int v31, v31, v32

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v26, v0

    .line 2389
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    goto/16 :goto_4

    .line 2395
    :cond_29
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    move/from16 v32, v0

    add-int v31, v31, v32

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    cmpl-float v31, v27, v31

    if-lez v31, :cond_19

    .line 2396
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    move/from16 v32, v0

    add-int v31, v31, v32

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v27, v0

    .line 2397
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    goto/16 :goto_5

    .line 2410
    :cond_2a
    const/16 v31, 0x2

    move/from16 v0, v31

    if-ne v6, v0, :cond_2c

    .line 2412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    move-object/from16 v31, v0

    if-eqz v31, :cond_2b

    .line 2413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldX:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldY:F

    move/from16 v33, v0

    move-object/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v16

    invoke-virtual {v0, v1, v2, v7, v3}, Lcom/samsung/hapticfeedback/HapticEffect;->playEffectByDistance(FFFF)V

    .line 2415
    :cond_2b
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldX:F

    .line 2416
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldY:F

    goto/16 :goto_6

    .line 2417
    :cond_2c
    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v6, v0, :cond_1b

    .line 2419
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    move-object/from16 v31, v0

    if-eqz v31, :cond_1b

    .line 2420
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/hapticfeedback/HapticEffect;->stopAllEffect()V

    goto/16 :goto_6

    .line 2423
    :cond_2d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    move/from16 v31, v0

    if-eqz v31, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    if-eqz v31, :cond_1b

    .line 2424
    const/16 v31, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 2425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/audio/SmpsManager;->generateSound(Landroid/view/MotionEvent;)V

    .line 2426
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    goto/16 :goto_6

    .line 2442
    .end local v18    # "originX":F
    .end local v19    # "originY":F
    .end local v26    # "tempX":F
    .end local v27    # "tempY":F
    :cond_2e
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_1f

    .line 2443
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Path;->reset()V

    .line 2444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v32

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v33

    invoke-virtual/range {v31 .. v33}, Landroid/graphics/Path;->moveTo(FF)V

    goto/16 :goto_7

    .line 2452
    :cond_2f
    const/16 v31, 0x2

    move/from16 v0, v31

    if-ne v6, v0, :cond_22

    .line 2453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v32

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v33

    invoke-virtual/range {v31 .. v33}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_8

    .line 2478
    .restart local v12    # "engine":J
    .restart local v20    # "posttouch":J
    .restart local v22    # "pretouch":J
    .restart local v28    # "time":J
    :cond_30
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v22

    .line 2480
    if-nez v6, :cond_37

    .line 2481
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    move/from16 v31, v0

    if-eqz v31, :cond_31

    .line 2482
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_31

    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_31

    .line 2483
    const/16 v31, 0x5

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_31

    .line 2484
    const/16 v31, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 2487
    :cond_31
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_32

    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_34

    .line 2488
    :cond_32
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    .line 2489
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_35

    .line 2490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v32, v0

    mul-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    .line 2498
    :cond_33
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v33, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    add-float v32, v32, v33

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2499
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v33, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    add-float v32, v32, v33

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2500
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v31, v0

    const/high16 v32, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    invoke-virtual/range {v31 .. v32}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2524
    :cond_34
    :goto_a
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removerTouchX:F

    .line 2525
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removerTouchY:F

    .line 2526
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-wide/from16 v32, v0

    const/16 v31, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v31

    move-object/from16 v0, p0

    move-wide/from16 v1, v32

    move-object/from16 v3, p1

    move/from16 v4, v31

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onTouch(JLandroid/view/MotionEvent;I)Z

    .line 2527
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    .line 2529
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-object/from16 v31, v0

    if-eqz v31, :cond_3c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    move-object/from16 v32, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, p1

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v31

    if-eqz v31, :cond_3c

    .line 2530
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v20

    .line 2531
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v32

    sub-long v32, v32, v28

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    .line 2532
    const-string v31, "SpenInView"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "Performance touch process end total = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v34

    sub-long v34, v34, v28

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    .line 2533
    const-string v33, " ms pretouch = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sub-long v34, v22, v28

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " ms spenview = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sub-long v34, v12, v22

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    .line 2534
    const-string v33, " ms posttouch = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sub-long v34, v20, v12

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " ms action = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 2532
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2535
    const/16 v31, 0x1

    goto/16 :goto_0

    .line 2491
    :cond_35
    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_33

    .line 2492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    move/from16 v31, v0

    if-nez v31, :cond_36

    .line 2493
    const/high16 v31, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v32, v0

    mul-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    goto/16 :goto_9

    .line 2494
    :cond_36
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_33

    .line 2495
    const/high16 v31, 0x42200000    # 40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v32, v0

    mul-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    goto/16 :goto_9

    .line 2502
    :cond_37
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    move/from16 v31, v0

    if-eqz v31, :cond_34

    .line 2503
    const/16 v31, 0x2

    move/from16 v0, v31

    if-ne v6, v0, :cond_3b

    .line 2504
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_39

    .line 2505
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v32, v0

    mul-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    .line 2513
    :cond_38
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v33, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    add-float v32, v32, v33

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2514
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v33, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    add-float v32, v32, v33

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2515
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v31, v0

    const/high16 v32, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    invoke-virtual/range {v31 .. v32}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto/16 :goto_a

    .line 2506
    :cond_39
    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_38

    .line 2507
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    move/from16 v31, v0

    if-nez v31, :cond_3a

    .line 2508
    const/high16 v31, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v32, v0

    mul-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    goto :goto_b

    .line 2509
    :cond_3a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_38

    .line 2510
    const/high16 v31, 0x42200000    # 40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v32, v0

    mul-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    goto/16 :goto_b

    .line 2517
    :cond_3b
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    .line 2518
    const/high16 v31, -0x3d380000    # -100.0f

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    .line 2519
    const/high16 v31, -0x3d380000    # -100.0f

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    .line 2520
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    const/high16 v32, -0x3d380000    # -100.0f

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2521
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    const/high16 v32, -0x3d380000    # -100.0f

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->y:F

    goto/16 :goto_a

    .line 2537
    :cond_3c
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v20

    .line 2538
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v32

    sub-long v32, v32, v28

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    .line 2539
    const-string v31, "SpenInView"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "Performance touch process end total = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v34

    sub-long v34, v34, v28

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    .line 2540
    const-string v33, " ms pretouch = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sub-long v34, v22, v28

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " ms spenview = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sub-long v34, v12, v22

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    .line 2541
    const-string v33, " ms posttouch = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sub-long v34, v20, v12

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " ms action = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 2539
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2544
    .end local v7    # "newX":F
    .end local v12    # "engine":J
    .end local v16    # "newY":F
    .end local v17    # "originAction":I
    .end local v20    # "posttouch":J
    .end local v22    # "pretouch":J
    .end local v28    # "time":J
    .end local v30    # "toolTypeAction":I
    :cond_3d
    const/16 v31, 0x1

    goto/16 :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 704
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_0

    .line 705
    const/4 v0, 0x4

    if-ne p2, v0, :cond_2

    .line 706
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->hideSoftInput()V

    .line 712
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-eqz v0, :cond_1

    if-nez p2, :cond_1

    .line 713
    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_deinit_c2dJNI()V

    .line 714
    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_init_c2dJNI()V

    .line 718
    :cond_1
    return-void

    .line 707
    :cond_2
    if-nez p2, :cond_0

    .line 708
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->showSoftInput()V

    goto :goto_0
.end method

.method protected onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 721
    const-string v0, "SpenInView"

    const-string v1, "onWindowFocusChanged() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    if-eqz p1, :cond_1

    .line 723
    const-string v0, "SpenInView"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->registerPensoundSolution()V

    .line 725
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_0

    .line 726
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->showSoftInput()V

    .line 732
    :cond_0
    :goto_0
    const-string v0, "SpenInView"

    const-string v1, "onWindowFocusChanged() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    return-void

    .line 729
    :cond_1
    const-string v0, "SpenInView"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->unregisterPensoundSolution()V

    goto :goto_0
.end method

.method public pauseReplay()V
    .locals 4

    .prologue
    .line 4250
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4256
    :cond_0
    :goto_0
    return-void

    .line 4253
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_pauseReplay(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4254
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method printRect(Ljava/lang/String;Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "srcRect"    # Landroid/graphics/Rect;

    .prologue
    .line 864
    const-string v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 865
    const-string v2, ") w = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 864
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 866
    return-void
.end method

.method printRect(Ljava/lang/String;Landroid/graphics/RectF;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "srcRect"    # Landroid/graphics/RectF;

    .prologue
    .line 869
    if-nez p2, :cond_0

    .line 870
    const-string v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    :goto_0
    return-void

    .line 872
    :cond_0
    const-string v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->right:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 873
    const-string v2, ") w = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 872
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public requestPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 7
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "isUpdate"    # Z

    .prologue
    const/4 v6, 0x0

    .line 3770
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3783
    :goto_0
    return v6

    .line 3773
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3774
    const-string v0, "SpenInView"

    const-string v1, "requestPageDoc is closed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3778
    :cond_1
    const-string v0, "SpenInView"

    const-string v1, "start requestPageDoc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3779
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 3780
    .local v5, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3781
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3782
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v4, 0x4

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 3783
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public resumeReplay()V
    .locals 4

    .prologue
    .line 4265
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4271
    :cond_0
    :goto_0
    return-void

    .line 4268
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_resumeReplay(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4269
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public retakeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V
    .locals 16
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "canvasLayout"    # Landroid/view/ViewGroup;
    .param p3, "strokeFrameContainer"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .param p4, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .prologue
    .line 3905
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3906
    :cond_0
    const/16 v2, 0x8

    .line 3907
    const-string v3, "The vies has not SpenPageDoc instance. please use to call setPageDoc."

    .line 3906
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 3910
    :cond_1
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    if-nez p4, :cond_3

    .line 3911
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Arguments is null. activity = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " viewgroup = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3912
    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " stroke = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " listener = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3911
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3915
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->checkMDMCameraLock()Z

    move-result v2

    if-nez v2, :cond_4

    .line 3916
    const/16 v2, 0x22

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-interface {v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    .line 3944
    :goto_0
    return-void

    .line 3921
    :cond_4
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setVisibility(Z)V

    .line 3922
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    .line 3923
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v2, :cond_5

    .line 3924
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setTouchEnabled(Z)V

    .line 3927
    :cond_5
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    .line 3928
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 3929
    .local v5, "bgBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/16 v4, -0x64

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V

    .line 3930
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->captureCurrentView(Z)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v6

    .line 3931
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrameType:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getPan()Landroid/graphics/PointF;

    move-result-object v11

    .line 3932
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomRatio()F

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getFrameStartPosition()Landroid/graphics/PointF;

    move-result-object v13

    move-object/from16 v3, p1

    move-object/from16 v9, p3

    move-object/from16 v14, p2

    .line 3930
    invoke-virtual/range {v2 .. v14}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->retakeStrokeFrame(Landroid/app/Activity;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIILcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V

    .line 3934
    new-instance v15, Landroid/os/Handler;

    invoke-direct {v15}, Landroid/os/Handler;-><init>()V

    .line 3935
    .local v15, "handler":Landroid/os/Handler;
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenInView$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    .line 3943
    const-wide/16 v6, 0x1e

    .line 3935
    invoke-virtual {v15, v2, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method

.method public setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V
    .locals 3
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "listener"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .prologue
    .line 3177
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    if-eqz v1, :cond_0

    .line 3178
    const-string v1, "SpenInView"

    const-string v2, "setBackgroundColorListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3179
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 3181
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v1, :cond_0

    .line 3182
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v1

    shr-int/lit8 v1, v1, 0x18

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    .line 3183
    .local v0, "enable":Z
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_0

    .line 3184
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 3185
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    .line 3189
    .end local v0    # "enable":Z
    :cond_0
    return-void

    .line 3182
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBlankColor(I)V
    .locals 4
    .param p1, "color"    # I

    .prologue
    .line 3112
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 3119
    :cond_0
    :goto_0
    return-void

    .line 3115
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 3116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v0, :cond_0

    .line 3117
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setPaint(Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setClippingPath(Landroid/graphics/Path;)V
    .locals 0
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    .line 1351
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mClippingPath:Landroid/graphics/Path;

    .line 1352
    return-void
.end method

.method public setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .prologue
    .line 4937
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4941
    :goto_0
    return-void

    .line 4940
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    goto :goto_0
.end method

.method public setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 5
    .param p1, "control"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    .prologue
    const/4 v4, 0x0

    .line 4142
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4167
    .end local p1    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    :cond_0
    :goto_0
    return-void

    .line 4145
    .restart local p1    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    :cond_1
    if-nez p1, :cond_2

    .line 4146
    const/4 v0, 0x7

    const-string v1, " control instance must not be null"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 4148
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_3

    .line 4149
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    .line 4150
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    .line 4153
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    .line 4155
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    .line 4156
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;

    invoke-direct {v1, p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;)V

    .line 4158
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->updateRectList()V

    .line 4160
    instance-of v0, p1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_4

    .line 4161
    check-cast p1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    .end local p1    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;

    invoke-direct {v0, p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;)V

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;)V

    .line 4164
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 4165
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setControlListener(Lcom/samsung/android/sdk/pen/engine/SpenControlListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    .prologue
    .line 5114
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5118
    :goto_0
    return-void

    .line 5117
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    goto :goto_0
.end method

.method public setDottedLineEnabled(ZIII[FF)V
    .locals 5
    .param p1, "enable"    # Z
    .param p2, "intervalHeight"    # I
    .param p3, "color"    # I
    .param p4, "thickness"    # I
    .param p5, "pathIntervals"    # [F
    .param p6, "phase"    # F

    .prologue
    const/4 v4, 0x0

    .line 4365
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4386
    :cond_0
    :goto_0
    return-void

    .line 4368
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    if-ne v0, p1, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    if-ne v0, p2, :cond_3

    .line 4369
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-ne v0, p3, :cond_3

    .line 4370
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    int-to-float v1, p4

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 4371
    :cond_3
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    .line 4372
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    if-eqz v0, :cond_4

    .line 4373
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    .line 4374
    int-to-float v0, p4

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineHalfWidth:F

    .line 4376
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    .line 4377
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 4378
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    int-to-float v1, p4

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4379
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    invoke-direct {v1, p5, p6}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 4380
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 4384
    :goto_1
    const/4 v0, 0x1

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0

    .line 4382
    :cond_4
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    goto :goto_1
.end method

.method public setDoubleTapZoomable(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 5204
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5209
    :goto_0
    return-void

    .line 5208
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDoubleTapZoomable:Z

    goto :goto_0
.end method

.method public setEraserChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .prologue
    .line 5038
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5042
    :goto_0
    return-void

    .line 5041
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    goto :goto_0
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 7
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    const/4 v3, 0x5

    .line 3399
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 3440
    :cond_0
    :goto_0
    return-void

    .line 3402
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 3404
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-eqz v0, :cond_5

    .line 3405
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_2

    .line 3406
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_2

    .line 3407
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    .line 3408
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    if-ne v1, v2, :cond_6

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setTextEraserEnabled(Z)V

    .line 3412
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    cmpg-float v0, v0, v6

    if-gez v0, :cond_3

    .line 3413
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iput v6, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 3417
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_4

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v3, :cond_4

    .line 3418
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v3, :cond_4

    .line 3419
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v3, :cond_4

    .line 3420
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v3, :cond_4

    .line 3421
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_7

    .line 3422
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    .line 3423
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    .line 3422
    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(FFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3429
    :cond_4
    :goto_2
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setEraserType(JI)V

    .line 3430
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setEraserSize(JF)V

    .line 3432
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v0, :cond_5

    .line 3433
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    .line 3437
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    if-eqz v0, :cond_0

    .line 3438
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto/16 :goto_0

    .line 3409
    :cond_6
    const/4 v1, 0x0

    goto :goto_1

    .line 3425
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(F)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method public setFlickListener(Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .prologue
    .line 5133
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5137
    :goto_0
    return-void

    .line 5136
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    goto :goto_0
.end method

.method public setForceStretchView(ZII)Z
    .locals 7
    .param p1, "enable"    # Z
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v4, 0x1

    .line 1295
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;-><init>()V

    .line 1296
    .local v0, "stretchInfo":Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    iput-boolean p1, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;->isStretch:Z

    .line 1297
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchWidth:I

    iput p2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;->stretchWidth:I

    .line 1298
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchHeight:I

    iput p3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;->stretchHeight:I

    .line 1300
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    .line 1301
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v1, :cond_1

    .line 1302
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    if-lez v1, :cond_0

    .line 1303
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    .line 1305
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    if-lez v1, :cond_1

    .line 1306
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    .line 1309
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1310
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1312
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 1313
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->deltaZoomSizeChanged()V

    .line 1314
    const/4 v0, 0x0

    .line 1316
    return v4
.end method

.method public setHighlight(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4418
    .local p1, "highlightInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;>;"
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4426
    :cond_0
    :goto_0
    return-void

    .line 4421
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    .line 4423
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 4424
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public setHorizontalScrollBarEnabled(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 4537
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4543
    :cond_0
    :goto_0
    return-void

    .line 4540
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v0, :cond_0

    .line 4541
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->enableHorizontalScroll(Z)V

    goto :goto_0
.end method

.method public setHorizontalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 6
    .param p1, "enable"    # Z
    .param p2, "leftScrollRegion"    # Landroid/graphics/Rect;
    .param p3, "rightScrollRegion"    # Landroid/graphics/Rect;
    .param p4, "responseTime"    # I
    .param p5, "velocity"    # I

    .prologue
    .line 4021
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4029
    :cond_0
    :goto_0
    return-void

    .line 4024
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    .line 4025
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartHorizontal:Z

    .line 4026
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->enableHorizontalSmartScroll(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V

    goto :goto_0
.end method

.method public setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .prologue
    .line 4880
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4884
    :goto_0
    return-void

    .line 4883
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    goto :goto_0
.end method

.method public setHyperTextListener(Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    .prologue
    .line 5180
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5185
    :goto_0
    return-void

    .line 5184
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    goto :goto_0
.end method

.method public setHyperTextViewEnabled(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 4097
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4103
    :goto_0
    return-void

    .line 4101
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsHyperText:Z

    .line 4102
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsHyperText:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setHyperTextViewEnabled(JZ)V

    goto :goto_0
.end method

.method public setLongPressEnabled(Z)Z
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 2622
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2623
    const/4 v0, 0x0

    .line 2626
    :goto_0
    return v0

    .line 2625
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsLongPressEnable:Z

    .line 2626
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setLongPressListener(Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    .prologue
    .line 4899
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4903
    :goto_0
    return-void

    .line 4902
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    goto :goto_0
.end method

.method public setMaxZoomRatio(F)Z
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 2994
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2995
    const/4 v0, 0x0

    .line 2997
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setMaxZoomRatio(JF)Z

    move-result v0

    goto :goto_0
.end method

.method public setMinZoomRatio(F)Z
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 3029
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3030
    const/4 v0, 0x0

    .line 3032
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setMinZoomRatio(JF)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;IIF)Z
    .locals 9
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "direction"    # I
    .param p3, "type"    # I
    .param p4, "centerY"    # F

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 3712
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3766
    :cond_0
    :goto_0
    return v6

    .line 3715
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-ne p1, v1, :cond_2

    .line 3716
    const-string v1, "SpenInView"

    const-string v2, "setPageDoc is same"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v8

    .line 3717
    goto :goto_0

    .line 3719
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-nez v1, :cond_3

    .line 3720
    const-string v1, "SpenInView"

    const-string v2, "setPageDoc is closed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3723
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3727
    const-string v1, "SpenInView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setPageDoc, direction="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3729
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1, p3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setType(I)V

    .line 3730
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 3731
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setCanvasInformation(IIII)V

    .line 3733
    const/4 v7, 0x1

    .line 3734
    .local v7, "isMemoryEnough":Z
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->saveScreenshot()Z

    move-result v1

    if-nez v1, :cond_4

    .line 3735
    const-string v1, "SpenInView"

    const-string v2, "setPageDoc. No enough memory1. Change to PAGE_TRANSITION_EFFECT_NONE"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3736
    const/4 v7, 0x0

    .line 3739
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 3740
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 3742
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 3743
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v4, 0x0

    move-object v1, p0

    move v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPan(JFFZ)V

    .line 3745
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v1, :cond_5

    .line 3746
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v1

    shr-int/lit8 v1, v1, 0x18

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_7

    move v0, v8

    .line 3747
    .local v0, "enable":Z
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_5

    .line 3748
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 3749
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    .line 3752
    .end local v0    # "enable":Z
    :cond_5
    if-eqz v7, :cond_8

    .line 3753
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v2, v3, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    .line 3754
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->startAnimation(I)Z

    move-result v1

    if-nez v1, :cond_6

    .line 3755
    const-string v1, "SpenInView"

    const-string v2, "setPageDoc. No enough memory2. Change to PAGE_TRANSITION_EFFECT_NONE"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3756
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_6
    :goto_2
    move v6, v8

    .line 3766
    goto/16 :goto_0

    :cond_7
    move v0, v6

    .line 3746
    goto :goto_1

    .line 3759
    :cond_8
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v2, v3, v1, v8}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 3760
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    .line 3762
    :cond_9
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    goto :goto_2
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 8
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "isUpdate"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3639
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 3674
    :cond_0
    :goto_0
    return v2

    .line 3642
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-ne p1, v3, :cond_2

    .line 3643
    const-string v2, "SpenInView"

    const-string v3, "setPageDoc is same"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 3644
    goto :goto_0

    .line 3646
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v3

    if-nez v3, :cond_3

    .line 3647
    const-string v1, "SpenInView"

    const-string v3, "setPageDoc is closed"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3650
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v3

    if-nez v3, :cond_0

    .line 3654
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 3655
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 3657
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 3659
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v3, :cond_5

    .line 3660
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v3

    shr-int/lit8 v3, v3, 0x18

    and-int/lit16 v3, v3, 0xff

    const/16 v4, 0xff

    if-ne v3, v4, :cond_7

    move v0, v1

    .line 3661
    .local v0, "enable":Z
    :goto_1
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    if-eq v0, v3, :cond_5

    .line 3662
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v3, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 3663
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    .line 3666
    .end local v0    # "enable":Z
    :cond_5
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v4, v5, v3, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 3667
    if-eqz p2, :cond_6

    .line 3668
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_6
    :goto_2
    move v2, v1

    .line 3674
    goto :goto_0

    :cond_7
    move v0, v2

    .line 3660
    goto :goto_1

    .line 3671
    :cond_8
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    goto :goto_2
.end method

.method public setPageEffectListener(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .prologue
    .line 4975
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 4976
    return-void
.end method

.method public setPan(Landroid/graphics/PointF;)V
    .locals 7
    .param p1, "position"    # Landroid/graphics/PointF;

    .prologue
    const/4 v4, 0x0

    .line 3065
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3078
    :goto_0
    return-void

    .line 3069
    :cond_0
    iget v0, p1, Landroid/graphics/PointF;->x:F

    cmpl-float v0, v0, v4

    if-gtz v0, :cond_1

    iget v0, p1, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v4

    if-lez v0, :cond_2

    .line 3070
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_2

    .line 3071
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setCheckCursorOnScroll(Z)V

    .line 3075
    :cond_2
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget v4, p1, Landroid/graphics/PointF;->x:F

    iget v5, p1, Landroid/graphics/PointF;->y:F

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPan(JFFZ)V

    .line 3077
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateNotepad()V

    goto :goto_0
.end method

.method public setParent(Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "view"    # Landroid/view/ViewGroup;

    .prologue
    .line 4126
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    .line 4127
    return-void
.end method

.method public setPenButtonSelectionEnabled(Z)V
    .locals 7
    .param p1, "enable"    # Z

    .prologue
    .line 4817
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4823
    :goto_0
    return-void

    .line 4821
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsPenButtonSelectionEnabled:Z

    .line 4822
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v4, 0x5

    const/4 v5, 0x0

    if-eqz p1, :cond_1

    const/4 v6, 0x1

    :goto_1
    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .prologue
    .line 5019
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5023
    :goto_0
    return-void

    .line 5022
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    goto :goto_0
.end method

.method public setPenDetachmentListener(Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    .prologue
    .line 4991
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 5004
    :cond_0
    :goto_0
    return-void

    .line 4994
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    .line 4996
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    if-eqz v1, :cond_0

    .line 4997
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    if-nez v1, :cond_0

    .line 4998
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.samsung.pen.INSERT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 4999
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.pen.INSERT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 5000
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    .line 5001
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 6
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x5

    .line 3302
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 3352
    :cond_0
    :goto_0
    return-void

    .line 3305
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 3307
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v0, :cond_0

    .line 3308
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 3309
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    const/high16 v1, 0x41200000    # 10.0f

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 3311
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v0, :cond_5

    .line 3312
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3313
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3314
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 3315
    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexPencil:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    .line 3322
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    invoke-virtual {v0, v1}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 3323
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    .line 3326
    :cond_5
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_6

    .line 3327
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_6

    .line 3328
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_6

    .line 3329
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_6

    .line 3330
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 3331
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 3330
    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableImage(Ljava/lang/String;IF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3332
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-ne v0, v5, :cond_6

    .line 3333
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenHoverPoint(Ljava/lang/String;)V

    .line 3337
    :cond_6
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPenStyle(JLjava/lang/String;)Z

    .line 3338
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPenColor(JI)V

    .line 3339
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPenSize(JF)V

    .line 3340
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_enablePenCurve(JZ)V

    .line 3341
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setAdvancedSetting(JLjava/lang/String;)V

    .line 3343
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v0, :cond_7

    .line 3344
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 3347
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    if-eqz v0, :cond_0

    .line 3348
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto/16 :goto_0

    .line 3316
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 3317
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 3318
    :cond_9
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexBrush:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    goto/16 :goto_1

    .line 3319
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3320
    :cond_b
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexMarker:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    goto/16 :goto_1
.end method

.method public setPenTooltipImage(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "penName"    # Ljava/lang/String;
    .param p2, "image"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 5212
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-nez v0, :cond_0

    .line 5216
    :goto_0
    return-void

    .line 5215
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->setPenTooltipImage(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setPostDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .prologue
    .line 5172
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5177
    :goto_0
    return-void

    .line 5175
    :cond_0
    const-string v0, "SpenInView"

    const-string v1, "Register setPostDrawListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5176
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    goto :goto_0
.end method

.method public setPreDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .prologue
    .line 5152
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5157
    :goto_0
    return-void

    .line 5155
    :cond_0
    const-string v0, "SpenInView"

    const-string v1, "Register setPreDrawListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5156
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    goto :goto_0
.end method

.method public setPreTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .prologue
    .line 4842
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4846
    :goto_0
    return-void

    .line 4845
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    goto :goto_0
.end method

.method public setRemoverChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    .prologue
    .line 5057
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5061
    :goto_0
    return-void

    .line 5060
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    goto :goto_0
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 6
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x5

    .line 3484
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 3525
    :cond_0
    :goto_0
    return-void

    .line 3487
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 3489
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-eqz v0, :cond_4

    .line 3490
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 3491
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 3494
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_3

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_3

    .line 3495
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_3

    .line 3496
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_3

    .line 3497
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_3

    .line 3498
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-nez v0, :cond_6

    .line 3499
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_5

    .line 3500
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/high16 v1, 0x41a00000    # 20.0f

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(FFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3514
    :cond_3
    :goto_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setRemoverType(JI)V

    .line 3515
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setRemoverSize(JF)V

    .line 3517
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v0, :cond_4

    .line 3518
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 3522
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    if-eqz v0, :cond_0

    .line 3523
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    goto :goto_0

    .line 3502
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 3505
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-ne v0, v5, :cond_3

    .line 3506
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_7

    .line 3507
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/high16 v1, 0x42200000    # 40.0f

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(FFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 3509
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public setReplayListener(Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    .prologue
    .line 4918
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4922
    :goto_0
    return-void

    .line 4921
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    goto :goto_0
.end method

.method public setReplayPosition(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 4322
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4328
    :cond_0
    :goto_0
    return-void

    .line 4325
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setReplayPosition(JI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4326
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public setReplaySpeed(I)V
    .locals 4
    .param p1, "speed"    # I

    .prologue
    .line 4297
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4303
    :cond_0
    :goto_0
    return-void

    .line 4300
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setReplaySpeed(JI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4301
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public setRequestPageDocListener(Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    .prologue
    .line 5196
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5201
    :goto_0
    return-void

    .line 5200
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    goto :goto_0
.end method

.method public setScrollBarEnabled(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 4503
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4510
    :cond_0
    :goto_0
    return-void

    .line 4506
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v0, :cond_0

    .line 4507
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->enableScroll(Z)V

    .line 4508
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreen()V

    goto :goto_0
.end method

.method public setSelectionChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    .prologue
    .line 5095
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5099
    :goto_0
    return-void

    .line 5098
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    goto :goto_0
.end method

.method public setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 5
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .prologue
    const/4 v4, 0x5

    .line 3569
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 3587
    :cond_0
    :goto_0
    return-void

    .line 3572
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .line 3573
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_2

    .line 3574
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_2

    .line 3575
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_2

    .line 3576
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_2

    .line 3577
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3580
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    if-eqz v0, :cond_3

    .line 3581
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;->type:I

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setSelectionType(JI)V

    .line 3584
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    if-eqz v0, :cond_0

    .line 3585
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V

    goto :goto_0
.end method

.method public setSmartScaleEnabled(ZLandroid/graphics/Rect;IIF)V
    .locals 6
    .param p1, "enable"    # Z
    .param p2, "region"    # Landroid/graphics/Rect;
    .param p3, "effectFrame"    # I
    .param p4, "zoomOutResponseTime"    # I
    .param p5, "zoomRatio"    # F

    .prologue
    .line 3970
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 3977
    :cond_0
    :goto_0
    return-void

    .line 3973
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    .line 3974
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartScale:Z

    .line 3975
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->enableSmartScale(ZLandroid/graphics/Rect;IIF)V

    goto :goto_0
.end method

.method public setTextChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    .prologue
    .line 5076
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5080
    :goto_0
    return-void

    .line 5079
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    goto :goto_0
.end method

.method public setTextCursorEnabled(Z)Z
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 2637
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2638
    const/4 v0, 0x0

    .line 2641
    :goto_0
    return v0

    .line 2640
    :cond_0
    sput-boolean p1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverCursorEnable:Z

    .line 2641
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 7
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v4, 0x5

    .line 3208
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 3246
    :cond_0
    :goto_0
    return-void

    .line 3211
    :cond_1
    if-eqz p1, :cond_0

    .line 3214
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_3

    .line 3215
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_2

    .line 3216
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 3219
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    if-eqz v0, :cond_0

    .line 3220
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    invoke-interface {v0, p1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;I)V

    goto :goto_0

    .line 3226
    :cond_3
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 3227
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_4

    .line 3228
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_4

    .line 3229
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_4

    .line 3230
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_4

    .line 3231
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3234
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    if-eqz v0, :cond_6

    .line 3235
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 3236
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getCanvasWidth()I

    move-result v2

    int-to-double v2, v2

    const-wide/high16 v4, 0x4069000000000000L    # 200.0

    div-double/2addr v2, v4

    double-to-float v2, v2

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 3238
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-gez v0, :cond_6

    .line 3239
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    const/4 v1, 0x0

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 3243
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    if-eqz v0, :cond_0

    .line 3244
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-interface {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;I)V

    goto :goto_0
.end method

.method public setToolTipEnabled(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 4110
    const-string v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setToolTipEnabled="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4111
    if-nez p1, :cond_0

    .line 4112
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 4114
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    .line 4115
    return-void
.end method

.method public setToolTypeAction(II)V
    .locals 8
    .param p1, "toolType"    # I
    .param p2, "action"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x5

    .line 2782
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2852
    :goto_0
    return-void

    .line 2785
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_1

    .line 2786
    if-eq p1, v4, :cond_1

    .line 2787
    if-ne p2, v4, :cond_7

    .line 2788
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setTouchEnabled(Z)V

    .line 2795
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_2

    .line 2796
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setToolTypeAction(II)V

    .line 2799
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v0, :cond_3

    .line 2800
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setToolTypeAction(II)V

    .line 2802
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_6

    .line 2803
    if-ne p2, v6, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_8

    .line 2804
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    if-nez v0, :cond_4

    .line 2805
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    .line 2810
    :cond_4
    :goto_2
    if-ne p2, v6, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_9

    .line 2811
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v0, :cond_5

    .line 2812
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 2814
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 2815
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 2814
    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableImage(Ljava/lang/String;IF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2816
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenHoverPoint(Ljava/lang/String;)V

    .line 2851
    :cond_6
    :goto_3
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setToolTypeAction(JII)V

    goto :goto_0

    .line 2790
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setTouchEnabled(Z)V

    goto :goto_1

    .line 2808
    :cond_8
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    goto :goto_2

    .line 2817
    :cond_9
    const/4 v0, 0x3

    if-ne p2, v0, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_c

    .line 2818
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-nez v0, :cond_a

    .line 2819
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 2821
    :cond_a
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_b

    .line 2822
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    .line 2823
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    .line 2822
    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(FFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 2825
    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(F)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 2827
    :cond_c
    const/4 v0, 0x4

    if-ne p2, v0, :cond_11

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_11

    .line 2828
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-nez v0, :cond_d

    .line 2829
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 2831
    :cond_d
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-nez v0, :cond_f

    .line 2832
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_e

    .line 2833
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/high16 v1, 0x41a00000    # 20.0f

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(FFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 2835
    :cond_e
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 2837
    :cond_f
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-ne v0, v5, :cond_6

    .line 2838
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_10

    .line 2839
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/high16 v1, 0x42200000    # 40.0f

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(FFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 2841
    :cond_10
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 2844
    :cond_11
    if-ne p2, v4, :cond_12

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_12

    .line 2845
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableHoverImage()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 2847
    :cond_12
    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3
.end method

.method public setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .prologue
    .line 4861
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4865
    :goto_0
    return-void

    .line 4864
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    goto :goto_0
.end method

.method public setVerticalScrollBarEnabled(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 4567
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4573
    :cond_0
    :goto_0
    return-void

    .line 4570
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v0, :cond_0

    .line 4571
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->enableVerticalScroll(Z)V

    goto :goto_0
.end method

.method public setVerticalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 6
    .param p1, "enable"    # Z
    .param p2, "topScrollRegion"    # Landroid/graphics/Rect;
    .param p3, "bottomScrollRegion"    # Landroid/graphics/Rect;
    .param p4, "responseTime"    # I
    .param p5, "velocity"    # I

    .prologue
    .line 4072
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4080
    :cond_0
    :goto_0
    return-void

    .line 4075
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    .line 4076
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartVertical:Z

    .line 4077
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->enableVerticalSmartScroll(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V

    goto :goto_0
.end method

.method public setView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 4122
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    .line 4123
    return-void
.end method

.method public setZoom(FFF)V
    .locals 7
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 2940
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2946
    :goto_0
    return-void

    .line 2943
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setZoom(JFFF)V

    .line 2945
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateNotepad()V

    goto :goto_0
.end method

.method public setZoomListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .prologue
    .line 4956
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4960
    :goto_0
    return-void

    .line 4959
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    goto :goto_0
.end method

.method public setZoomPadBoxHeight(F)V
    .locals 4
    .param p1, "height"    # F

    .prologue
    .line 4623
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 4629
    :cond_0
    :goto_0
    return-void

    .line 4627
    :cond_1
    const-string v0, "ZoomPad"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setZoomPadBoxHeight ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4628
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setBoxHeight(F)V

    goto :goto_0
.end method

.method public setZoomPadBoxHeightEnabled(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 4733
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 4737
    :cond_0
    :goto_0
    return-void

    .line 4736
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setBoxHeightEnabled(Z)V

    goto :goto_0
.end method

.method public setZoomPadBoxPosition(Landroid/graphics/PointF;)V
    .locals 4
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 4640
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 4646
    :cond_0
    :goto_0
    return-void

    .line 4644
    :cond_1
    const-string v0, "ZoomPad"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setZoomPadBoxPosition ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4645
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setBoxPosition(FF)V

    goto :goto_0
.end method

.method public setZoomPadButtonEnabled(ZZZZZ)V
    .locals 6
    .param p1, "left"    # Z
    .param p2, "right"    # Z
    .param p3, "enter"    # Z
    .param p4, "up"    # Z
    .param p5, "down"    # Z

    .prologue
    .line 4759
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 4763
    :cond_0
    :goto_0
    return-void

    .line 4762
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setButtonEnabled(ZZZZZ)V

    goto :goto_0
.end method

.method public setZoomPadListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    .prologue
    .line 5188
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5193
    :goto_0
    return-void

    .line 5192
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    goto :goto_0
.end method

.method public setZoomPadPosition(Landroid/graphics/PointF;)V
    .locals 4
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 4662
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 4668
    :cond_0
    :goto_0
    return-void

    .line 4666
    :cond_1
    const-string v0, "ZoomPad"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setZoomPadPosition ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4667
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setPadPosition(FF)V

    goto :goto_0
.end method

.method public setZoomable(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 2893
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2898
    :goto_0
    return-void

    .line 2896
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomable:Z

    .line 2897
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_enableZoom(JZ)V

    goto :goto_0
.end method

.method public startReplay()V
    .locals 4

    .prologue
    .line 4219
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 4220
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4226
    :cond_0
    :goto_0
    return-void

    .line 4223
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_startReplay(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4224
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public startTemporaryStroke()V
    .locals 4

    .prologue
    .line 4452
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4456
    :goto_0
    return-void

    .line 4455
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_startTemporaryStroke(J)V

    goto :goto_0
.end method

.method public startZoomPad()V
    .locals 7

    .prologue
    .line 4684
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 4707
    :cond_0
    :goto_0
    return-void

    .line 4688
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4691
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 4693
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 4694
    .local v6, "bgBitmap":Landroid/graphics/Bitmap;
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/16 v2, -0x64

    invoke-direct {p0, v0, v1, v2, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V

    .line 4696
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setScreenStart(II)V

    .line 4697
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setPanAndZoom(FFFFF)V

    .line 4698
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setLayer(Ljava/util/ArrayList;)V

    .line 4699
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->start(Landroid/view/ViewGroup;II)V

    .line 4701
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setReference(Landroid/graphics/Bitmap;)V

    .line 4703
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v0, :cond_2

    .line 4704
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 4706
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_0
.end method

.method public stopReplay()V
    .locals 4

    .prologue
    .line 4235
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4241
    :cond_0
    :goto_0
    return-void

    .line 4238
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_stopReplay(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4239
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public stopTemporaryStroke()V
    .locals 4

    .prologue
    .line 4465
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4469
    :goto_0
    return-void

    .line 4468
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_stopTemporaryStroke(J)V

    goto :goto_0
.end method

.method public stopZoomPad()V
    .locals 4

    .prologue
    .line 4710
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 4715
    :cond_0
    :goto_0
    return-void

    .line 4714
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->stop()V

    goto :goto_0
.end method

.method public takeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Ljava/util/List;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V
    .locals 18
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "canvasLayout"    # Landroid/view/ViewGroup;
    .param p4, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;",
            "Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 3860
    .local p3, "strokeList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3861
    :cond_0
    const/16 v2, 0x8

    .line 3862
    const-string v3, "The vies has not SpenPageDoc instance. please use to call setPageDoc."

    .line 3861
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 3864
    :cond_1
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    if-nez p4, :cond_3

    .line 3865
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Arguments is null. activity = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " viewgroup = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3866
    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " stroke = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " listener = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3865
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3869
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->checkMDMCameraLock()Z

    move-result v2

    if-nez v2, :cond_4

    .line 3870
    const/16 v2, 0x21

    .line 3871
    new-instance v3, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 3870
    move-object/from16 v0, p4

    invoke-interface {v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    .line 3901
    :goto_0
    return-void

    .line 3875
    :cond_4
    new-instance v16, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 3876
    .local v16, "oc":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 3877
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_5

    .line 3881
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    .line 3883
    new-instance v15, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v15}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 3884
    .local v15, "bc":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;-><init>()V

    invoke-virtual {v15, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 3885
    new-instance v9, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 3886
    .local v9, "c":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setOutOfViewEnabled(Z)V

    .line 3887
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setRotatable(Z)V

    .line 3888
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setVisibility(Z)V

    .line 3889
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 3890
    invoke-virtual {v9, v15}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 3891
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setHistoryTag()V

    .line 3892
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 3893
    const-string v2, "STROKE_FRAME"

    const/4 v3, 0x1

    invoke-virtual {v9, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setExtraDataInt(Ljava/lang/String;I)V

    .line 3894
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 3895
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    .line 3896
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 3897
    .local v5, "bgBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/16 v4, -0x64

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V

    .line 3898
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->captureCurrentView(Z)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v6

    .line 3899
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrameType:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getPan()Landroid/graphics/PointF;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomRatio()F

    move-result v12

    .line 3900
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getFrameStartPosition()Landroid/graphics/PointF;

    move-result-object v13

    move-object/from16 v3, p1

    move-object/from16 v14, p2

    .line 3898
    invoke-virtual/range {v2 .. v14}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->takeStrokeFrame(Landroid/app/Activity;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIILcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V

    goto/16 :goto_0

    .line 3877
    .end local v5    # "bgBitmap":Landroid/graphics/Bitmap;
    .end local v9    # "c":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .end local v15    # "bc":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 3878
    .local v17, "s":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 3879
    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto/16 :goto_1
.end method

.method public declared-synchronized update()V
    .locals 6

    .prologue
    .line 2129
    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2147
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2133
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 2134
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_update(J)V

    .line 2136
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v1, :cond_2

    .line 2137
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v1

    shr-int/lit8 v1, v1, 0x18

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_3

    const/4 v0, 0x1

    .line 2138
    .local v0, "enable":Z
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_2

    .line 2139
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 2140
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    .line 2144
    .end local v0    # "enable":Z
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2145
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2129
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 2137
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 4
    .param p1, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    .prologue
    .line 2242
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2256
    :goto_0
    return-void

    .line 2245
    :cond_0
    if-eqz p1, :cond_1

    array-length v0, p1

    if-nez v0, :cond_2

    .line 2246
    :cond_1
    const-string v0, "SpenInView"

    const-string v1, "The parameter \'userDataList\' cannot be null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2250
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 2251
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    array-length v2, p1

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_updateRedo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2252
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2254
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->fitControlToObject()V

    .line 2255
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateNotepad()V

    goto :goto_0
.end method

.method public updateScreen()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2179
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2190
    :cond_0
    :goto_0
    return-void

    .line 2183
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v1

    if-nez v1, :cond_0

    .line 2187
    const-string v1, "SpenInView"

    const-string v2, "updateScreen"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2188
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v2, v2

    invoke-direct {v0, v6, v6, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2189
    .local v0, "rectf":Landroid/graphics/RectF;
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public updateScreenFrameBuffer()V
    .locals 4

    .prologue
    .line 2156
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2170
    :goto_0
    return-void

    .line 2160
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2162
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->pauseReplay()V

    .line 2164
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_updateAllScreenFrameBuffer(J)V

    .line 2166
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resumeReplay()V

    goto :goto_0

    .line 2168
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_updateAllScreenFrameBuffer(J)V

    goto :goto_0
.end method

.method public updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 4
    .param p1, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    .prologue
    .line 2209
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2223
    :goto_0
    return-void

    .line 2212
    :cond_0
    if-eqz p1, :cond_1

    array-length v0, p1

    if-nez v0, :cond_2

    .line 2213
    :cond_1
    const-string v0, "SpenInView"

    const-string v1, "The parameter \'userDataList\' cannot be null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2217
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 2218
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    array-length v2, p1

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_updateUndo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2219
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2221
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->fitControlToObject()V

    .line 2222
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateNotepad()V

    goto :goto_0
.end method
