.class Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;
.super Landroid/widget/AdapterView;
.source "SpenHorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SpenHorizontalListView"


# instance fields
.field protected mAdapter:Landroid/widget/ListAdapter;

.field protected mCurrentX:I

.field private mDataChanged:Z

.field private final mDataObserver:Landroid/database/DataSetObserver;

.field private mDisplayOffset:I

.field private mGesture:Landroid/view/GestureDetector;

.field private mIsEdgeLeft:Z

.field private mLeftEdgeEffect:Landroid/widget/EdgeEffect;

.field private mLeftViewIndex:I

.field private mListener:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

.field private mMaxX:I

.field protected mNextX:I

.field private final mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

.field private final mRemovedViewQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mRightEdgeEffect:Landroid/widget/EdgeEffect;

.field private mRightViewIndex:I

.field private mScrollFlag:Z

.field protected mScroller:Landroid/widget/Scroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0, p1, p2}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I

    .line 24
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightViewIndex:I

    .line 27
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mMaxX:I

    .line 28
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDisplayOffset:I

    .line 31
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    .line 32
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDataChanged:Z

    .line 34
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScrollFlag:Z

    .line 91
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    .line 345
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

    .line 66
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->initView()V

    .line 67
    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;Z)V
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDataChanged:Z

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->reset()V

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;Z)V
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScrollFlag:Z

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;Z)V
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mIsEdgeLeft:Z

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/EdgeEffect;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mMaxX:I

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/EdgeEffect;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

    return-object v0
.end method

.method private addAndMeasureChild(Landroid/view/View;I)V
    .locals 4
    .param p1, "child"    # Landroid/view/View;
    .param p2, "viewPos"    # I

    .prologue
    const/4 v1, -0x1

    const/high16 v3, -0x80000000

    .line 146
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 147
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    if-nez v0, :cond_0

    .line 148
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    .end local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 151
    .restart local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 152
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getWidth()I

    move-result v1

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 153
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getHeight()I

    move-result v2

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 152
    invoke-virtual {p1, v1, v2}, Landroid/view/View;->measure(II)V

    .line 154
    return-void
.end method

.method private checkScroller()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->post(Ljava/lang/Runnable;)Z

    .line 209
    :cond_0
    return-void
.end method

.method private fillList(I)V
    .locals 3
    .param p1, "dx"    # I

    .prologue
    .line 212
    const/4 v1, 0x0

    .line 213
    .local v1, "edge":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 214
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 215
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    .line 217
    :cond_0
    invoke-direct {p0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->fillListRight(II)V

    .line 219
    const/4 v1, 0x0

    .line 220
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 221
    if-eqz v0, :cond_1

    .line 222
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 224
    :cond_1
    invoke-direct {p0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->fillListLeft(II)V

    .line 225
    return-void
.end method

.method private fillListLeft(II)V
    .locals 4
    .param p1, "leftEdge"    # I
    .param p2, "dx"    # I

    .prologue
    .line 243
    :goto_0
    add-int v1, p1, p2

    if-lez v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I

    if-gez v1, :cond_1

    .line 253
    :cond_0
    return-void

    .line 244
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {v2, v3, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 245
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 248
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->addAndMeasureChild(Landroid/view/View;I)V

    .line 249
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr p1, v1

    .line 250
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I

    .line 251
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDisplayOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDisplayOffset:I

    goto :goto_0
.end method

.method private fillListRight(II)V
    .locals 4
    .param p1, "rightEdge"    # I
    .param p2, "dx"    # I

    .prologue
    .line 228
    :goto_0
    add-int v1, p1, p2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getWidth()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightViewIndex:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 240
    :cond_0
    return-void

    .line 230
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightViewIndex:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {v2, v3, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 231
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 234
    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->addAndMeasureChild(Landroid/view/View;I)V

    .line 235
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr p1, v1

    .line 237
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightViewIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightViewIndex:I

    goto :goto_0
.end method

.method private declared-synchronized initView()V
    .locals 3

    .prologue
    .line 70
    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightViewIndex:I

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDisplayOffset:I

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mCurrentX:I

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    .line 75
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mMaxX:I

    .line 76
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScroller:Landroid/widget/Scroller;

    .line 78
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mGesture:Landroid/view/GestureDetector;

    .line 79
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mGesture:Landroid/view/GestureDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mIsEdgeLeft:Z

    .line 82
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    if-nez v0, :cond_1

    .line 87
    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :cond_1
    monitor-exit p0

    return-void

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private positionItems(I)V
    .locals 7
    .param p1, "dx"    # I

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 277
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDisplayOffset:I

    add-int/2addr v4, p1

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDisplayOffset:I

    .line 278
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDisplayOffset:I

    .line 279
    .local v3, "left":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildCount()I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 286
    .end local v2    # "i":I
    .end local v3    # "left":I
    :cond_0
    return-void

    .line 280
    .restart local v2    # "i":I
    .restart local v3    # "left":I
    :cond_1
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 281
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 282
    .local v1, "childWidth":I
    const/4 v4, 0x0

    add-int v5, v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 283
    add-int/2addr v3, v1

    .line 279
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private releaseEdgeEffect()V
    .locals 1

    .prologue
    .line 332
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mIsEdgeLeft:Z

    .line 333
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 334
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 335
    return-void
.end method

.method private removeNonVisibleItems(I)V
    .locals 4
    .param p1, "dx"    # I

    .prologue
    const/4 v3, 0x0

    .line 256
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 257
    .local v0, "child":Landroid/view/View;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, p1

    if-lez v1, :cond_2

    .line 266
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 267
    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getWidth()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 273
    :cond_1
    return-void

    .line 258
    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDisplayOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDisplayOffset:I

    .line 259
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 260
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 261
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I

    .line 262
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 268
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 269
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 270
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightViewIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightViewIndex:I

    .line 271
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method private declared-synchronized reset()V
    .locals 1

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->initView()V

    .line 133
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->removeAllViewsInLayout()V

    .line 134
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->requestLayout()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    monitor-exit p0

    return-void

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 295
    monitor-enter p0

    :try_start_0
    const-string v2, "SpenHorizontalListView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dispatchTouchEvent action = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 297
    .local v0, "handled":Z
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mGesture:Landroid/view/GestureDetector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    move v1, v0

    .line 301
    .end local v0    # "handled":Z
    .local v1, "handled":I
    :goto_0
    monitor-exit p0

    return v1

    .line 300
    .end local v1    # "handled":I
    .restart local v0    # "handled":Z
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mGesture:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    or-int/2addr v0, v2

    move v1, v0

    .line 301
    .restart local v1    # "handled":I
    goto :goto_0

    .line 295
    .end local v0    # "handled":Z
    .end local v1    # "handled":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method protected declared-synchronized getLeftEdgeEffect()Landroid/widget/EdgeEffect;
    .locals 1

    .prologue
    .line 49
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized getRightEdgeEffect()Landroid/widget/EdgeEffect;
    .locals 1

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return-object v0
.end method

.method protected declared-synchronized isEdgeLeft()Z
    .locals 1

    .prologue
    .line 41
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mIsEdgeLeft:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized isEdgeRight()Z
    .locals 1

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mIsEdgeLeft:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized onDown(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 338
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScrollFlag:Z

    .line 339
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->releaseEdgeEffect()V

    .line 340
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScroller:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    monitor-exit p0

    return v2

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 11
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 305
    monitor-enter p0

    .line 306
    :try_start_0
    const-string v0, "SpenHorizontalListView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFling: velocityX = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mMaxX = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mMaxX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mNextX = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScrollFlag:Z

    .line 308
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mIsEdgeLeft:Z

    if-eqz v0, :cond_1

    .line 309
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onPull(F)V

    .line 314
    :goto_0
    const/high16 v10, 0x43af0000    # 350.0f

    .line 315
    .local v10, "minVelocity":F
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    .line 316
    .local v9, "dm":Landroid/util/DisplayMetrics;
    const/4 v0, 0x1

    invoke-static {v0, v10, v9}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v10

    .line 318
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v10

    if-ltz v0, :cond_0

    .line 319
    const/4 v0, 0x0

    cmpg-float v0, p3, v0

    if-gez v0, :cond_2

    .line 320
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mMaxX:I

    mul-int/lit8 v3, v3, 0x5

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mMaxX:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 305
    :cond_0
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->requestLayout()V

    .line 328
    const/4 v0, 0x1

    return v0

    .line 311
    .end local v9    # "dm":Landroid/util/DisplayMetrics;
    .end local v10    # "minVelocity":F
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onPull(F)V

    goto :goto_0

    .line 305
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 322
    .restart local v9    # "dm":Landroid/util/DisplayMetrics;
    .restart local v10    # "minVelocity":F
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mMaxX:I

    mul-int/lit8 v3, v3, 0x5

    neg-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mMaxX:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 403
    const-string v0, "SpenHorizontalListView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onInterceptTouchEvent action = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mScrollFlag = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScrollFlag:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 405
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScrollFlag:Z

    .line 408
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScrollFlag:Z

    if-eqz v0, :cond_1

    .line 409
    const/4 v0, 0x1

    .line 412
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected declared-synchronized onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    .line 160
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v3, :cond_0

    .line 161
    const-string v3, "SpenHorizontalListView"

    const-string v4, "onLayout mAdapter = null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    :goto_0
    monitor-exit p0

    return-void

    .line 165
    :cond_0
    :try_start_1
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDataChanged:Z

    if-eqz v3, :cond_1

    .line 166
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mCurrentX:I

    .line 167
    .local v1, "oldCurrentX":I
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->initView()V

    .line 168
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->removeAllViewsInLayout()V

    .line 169
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    .line 170
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDataChanged:Z

    .line 173
    .end local v1    # "oldCurrentX":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 174
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    .line 175
    .local v2, "scrollx":I
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    .line 178
    .end local v2    # "scrollx":I
    :cond_2
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    if-gtz v3, :cond_3

    .line 179
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    .line 180
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mIsEdgeLeft:Z

    .line 181
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScroller:Landroid/widget/Scroller;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 184
    :cond_3
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mMaxX:I

    if-lt v3, v4, :cond_4

    .line 185
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mMaxX:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    .line 186
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mIsEdgeLeft:Z

    .line 187
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScroller:Landroid/widget/Scroller;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 190
    :cond_4
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mCurrentX:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    sub-int v0, v3, v4

    .line 192
    .local v0, "dx":I
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->removeNonVisibleItems(I)V

    .line 193
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->fillList(I)V

    .line 194
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->positionItems(I)V

    .line 195
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mCurrentX:I

    .line 196
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->checkScroller()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 158
    .end local v0    # "dx":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized scrollTo(I)V
    .locals 6
    .param p1, "x"    # I

    .prologue
    .line 289
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    sub-int v3, p1, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 290
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->requestLayout()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    monitor-exit p0

    return-void

    .line 289
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 126
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 127
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 128
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->reset()V

    .line 129
    return-void
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

    .line 62
    return-void
.end method

.method protected declared-synchronized setMaxX(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 138
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mMaxX:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    monitor-exit p0

    return-void

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setSelection(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 143
    return-void
.end method
