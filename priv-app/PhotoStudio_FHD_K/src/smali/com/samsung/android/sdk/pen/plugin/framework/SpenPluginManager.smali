.class public Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;
.super Ljava/lang/Object;
.source "SpenPluginManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;
    }
.end annotation


# static fields
.field private static final APK_PLUGIN_CHECK_INTENT:Ljava/lang/String; = "com.samsung.android.sdk.pen.plugin.PICK"

.field private static final BINARY_TYPE_INDEX:I = 0x4

.field private static final BINARY_TYPE_JAVA:I = 0x0

.field private static final BINARY_TYPE_NATIVE:I = 0x1

.field private static final BUILTIN_PLUGIN_LIST:[[Ljava/lang/String;

.field private static final CLASS_NAME_INDEX:I = 0xd

.field private static final EXTRA_INFO_INDEX:I = 0xa

.field private static final FOCUSED_ICON_IMAGE_URI_INDEX:I = 0x9

.field private static final HAS_PRIVATE_KEY_INDEX:I = 0x5

.field private static final ICON_IMAGE_URI_INDEX:I = 0x6

.field private static final INTERFACE_NAME_INDEX:I = 0x2

.field private static final INTERFACE_VERSION_INDEX:I = 0x3

.field private static final META_DATA_KEY_SPEN_PLUGIN_INFO:Ljava/lang/String; = "SPEN_PLUGIN_INFO"

.field private static final PACKAGE_NAME_INDEX:I = 0xc

.field private static final PLUGIN_NAME_URI_INDEX:I = 0xb

.field private static final PRESET_ICON_IMAGE_URI_INDEX:I = 0x8

.field private static final SELECTED_ICON_IMAGE_URI_INDEX:I = 0x7

.field private static final TYPE_INDEX:I = 0x0

.field private static final VERSION_INDEX:I = 0x1

.field private static mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;


# instance fields
.field private mAPKPluginList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mBuiltInPluginList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mIs64:Z

.field private mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

.field private final mLoadedPluginList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;",
            ">;"
        }
    .end annotation
.end field

.field private mPackageReceiver:Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1150
    const/16 v0, 0x13

    new-array v0, v0, [[Ljava/lang/String;

    .line 1152
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Pen"

    aput-object v2, v1, v5

    const-string v2, "1"

    aput-object v2, v1, v6

    const-string v2, "SpenPenInterface"

    aput-object v2, v1, v7

    const-string v2, "1"

    aput-object v2, v1, v8

    const-string v2, "native"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "snote_popup_pensetting_montblancpen"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 1153
    const-string v3, "snote_popup_pensetting_montblancpen_select"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "pen_preset_montblancpen"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    .line 1154
    const-string v3, "snote_popup_pensetting_montblancpen_focus"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "MontblancFountainPen"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "MontblancFountainPen"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    .line 1155
    const-string v3, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "MontblancFountainPen"

    aput-object v3, v1, v2

    aput-object v1, v0, v5

    .line 1157
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Pen"

    aput-object v2, v1, v5

    const-string v2, "1"

    aput-object v2, v1, v6

    const-string v2, "SpenPenInterface"

    aput-object v2, v1, v7

    const-string v2, "1"

    aput-object v2, v1, v8

    const-string v2, "native"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "snote_popup_pensetting_calligraphypen"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 1158
    const-string v3, "snote_popup_pensetting_calligraphypen_select"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "pen_preset_calligraphypen"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    .line 1159
    const-string v3, "snote_popup_pensetting_calligraphypen_focus"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "MontblancCalligraphyPen"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    .line 1160
    const-string v3, "MontblancCalligraphyPen"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "MontblancCalligraphyPen"

    aput-object v3, v1, v2

    aput-object v1, v0, v6

    .line 1162
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Pen"

    aput-object v2, v1, v5

    const-string v2, "1"

    aput-object v2, v1, v6

    const-string v2, "SpenPenInterface"

    aput-object v2, v1, v7

    const-string v2, "1"

    aput-object v2, v1, v8

    const-string v2, "native"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "snote_popup_pensetting_fountainpen"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 1163
    const-string v3, "snote_popup_pensetting_fountainpen_select"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "pen_preset_fountainpen"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    .line 1164
    const-string v3, "snote_popup_pensetting_fountainpen_focus"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "FountainPen"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "FountainPen"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    .line 1165
    const-string v3, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "FountainPen"

    aput-object v3, v1, v2

    aput-object v1, v0, v7

    .line 1167
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Pen"

    aput-object v2, v1, v5

    const-string v2, "1"

    aput-object v2, v1, v6

    const-string v2, "SpenPenInterface"

    aput-object v2, v1, v7

    const-string v2, "1"

    aput-object v2, v1, v8

    const-string v2, "native"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "snote_popup_pensetting_calligraphypen"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 1168
    const-string v3, "snote_popup_pensetting_calligraphypen_select"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "pen_preset_calligraphypen"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    .line 1169
    const-string v3, "snote_popup_pensetting_calligraphypen_focus"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "ObliquePen"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "ObliquePen"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    .line 1170
    const-string v3, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "ObliquePen"

    aput-object v3, v1, v2

    aput-object v1, v0, v8

    .line 1172
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Pen"

    aput-object v2, v1, v5

    const-string v2, "1"

    aput-object v2, v1, v6

    const-string v2, "SpenPenInterface"

    aput-object v2, v1, v7

    const-string v2, "1"

    aput-object v2, v1, v8

    const-string v2, "native"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "snote_popup_pensetting_pen"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 1173
    const-string v3, "snote_popup_pensetting_pen_select"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "pen_preset_pen"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "snote_popup_pensetting_pen_focus"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    .line 1174
    const-string v3, "InkPen"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "InkPen"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "InkPen"

    aput-object v3, v1, v2

    aput-object v1, v0, v9

    const/4 v1, 0x5

    .line 1176
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Pen"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenPenInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "native"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "snote_popup_pensetting_pencil"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 1177
    const-string v4, "snote_popup_pensetting_pencil_select"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "pen_preset_pencil"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "snote_popup_pensetting_pencil_focus"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    .line 1178
    const-string v4, "Pencil"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "Pencil"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "Pencil"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 1184
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Pen"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenPenInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "native"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "snote_popup_pensetting_marker"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 1185
    const-string v4, "snote_popup_pensetting_marker_select"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "pen_preset_marker"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "snote_popup_pensetting_marker_focus"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    .line 1186
    const-string v4, "Marker"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "Marker"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "Marker"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 1188
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Pen"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenPenInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "native"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "snote_popup_pensetting_chinabrush"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 1189
    const-string v4, "snote_popup_pensetting_chinabrush_select"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "pen_preset_chinabrush"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    .line 1190
    const-string v4, "snote_popup_pensetting_chinabrush_focus"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "ChineseBrush"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "ChineseBrush"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    .line 1191
    const-string v4, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "ChineseBrush"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 1193
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Pen"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenPenInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "native"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "snote_popup_pensetting_alpha"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 1194
    const-string v4, "snote_popup_pensetting_alpha_select"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "pen_preset_alpha"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "snote_popup_pensetting_alpha_focus"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    .line 1195
    const-string v4, "MagicPen"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "MagicPen"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "MagicPen"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 1197
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Pen"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenPenInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "native"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "snote_popup_pensetting_chinabrush"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 1198
    const-string v4, "snote_popup_pensetting_chinabrush_select"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "pen_preset_chinabrush"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    .line 1199
    const-string v4, "snote_popup_pensetting_chinabrush_focus"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "Beautify"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "Beautify"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    .line 1200
    const-string v4, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "Beautify"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 1202
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Pen"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenPenInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "native"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "snote_popup_pensetting_brush"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 1203
    const-string v4, "snote_popup_pensetting_brush_select"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "pen_preset_brush"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "snote_popup_pensetting_brush_focus"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    .line 1204
    const-string v4, "Brush"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "Brush"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "Brush"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 1206
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "ObjectRuntime"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenObjectRuntimeInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "defaultIconImageURI"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 1207
    const-string v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "extraInfo"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "Video"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    .line 1208
    const-string v4, "com.samsung.android.sdk.pen.objectruntime.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "Video"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 1209
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "recognition"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenLanguageRecognitionInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "uriInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 1210
    const-string v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "extraInfo"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "Text"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    .line 1211
    const-string v4, "com.samsung.text"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "TextRecognitionPlugin"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 1212
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "TextRecognition"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenLanguageRecognitionInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "uriInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 1213
    const-string v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "1/4"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "SpenText"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    .line 1214
    const-string v4, "com.samsung.android.sdk.pen.recognition.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "TextRecognitionPlugin"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 1215
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "EquationRecognition"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenRecognitionInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "uriInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 1216
    const-string v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "1/4"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "SpenEquation"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    .line 1217
    const-string v4, "com.samsung.android.sdk.pen.recognition.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "EquationRecognitionPlugin"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 1218
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "ShapeRecognition"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenRecognitionInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "uriInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    .line 1219
    const-string v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "1/4"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "SpenShape"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    .line 1220
    const-string v4, "com.samsung.android.sdk.pen.recognition.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "ShapeRecognitionPlugin"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 1221
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "ShapeRecognition"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenRecognitionInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "uriInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    .line 1222
    const-string v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "1/4"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "NRRShape"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    .line 1223
    const-string v4, "com.samsung.android.sdk.pen.recognition.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "NRRShapeRecognitionPlugin"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 1224
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "SignatureVerification"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenSignatureVerificationInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "uriInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 1225
    const-string v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "extraInfo"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "SpenSignature"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    .line 1226
    const-string v4, "com.samsung.android.sdk.pen.recognition.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "Signature"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 1227
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "SignatureVerification"

    aput-object v3, v2, v5

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "SpenSignatureVerificationInterface"

    aput-object v3, v2, v7

    const-string v3, "1"

    aput-object v3, v2, v8

    const-string v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "uriInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 1228
    const-string v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "extraInfo"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "NRRSignature"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    .line 1229
    const-string v4, "com.samsung.android.sdk.pen.recognition.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "NRRSignatureRecognitionPlugin"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    .line 1150
    sput-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->BUILTIN_PLUGIN_LIST:[[Ljava/lang/String;

    .line 1231
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mIs64:Z

    .line 1235
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    .line 180
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mIs64:Z

    .line 181
    return-void

    .line 180
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;
    .locals 21
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    const-class v15, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    monitor-enter v15

    if-nez p0, :cond_0

    .line 189
    :try_start_0
    new-instance v14, Ljava/lang/IllegalArgumentException;

    const-string v16, "E_INVALID_ARG : parameter \'context\' is null"

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    :catchall_0
    move-exception v14

    monitor-exit v15

    throw v14

    .line 192
    :cond_0
    :try_start_1
    sget-object v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-nez v14, :cond_8

    .line 193
    new-instance v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    invoke-direct {v14}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;-><init>()V

    sput-object v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 194
    sget-object v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    new-instance v16, Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;-><init>()V

    move-object/from16 v0, v16

    iput-object v0, v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mPackageReceiver:Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;

    .line 195
    sget-object v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v14, v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mPackageReceiver:Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;

    if-nez v14, :cond_1

    .line 196
    new-instance v14, Ljava/lang/IllegalStateException;

    const-string v16, "E_INVALID_STATE : Fail to create PackageReceiver"

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 198
    :cond_1
    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    .line 199
    .local v8, "filter":Landroid/content/IntentFilter;
    const-string v14, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v8, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 200
    const-string v14, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v8, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 201
    const-string v14, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v8, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 202
    const-string v14, "package"

    invoke-virtual {v8, v14}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 203
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    sget-object v16, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mPackageReceiver:Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v8}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 205
    sget-object v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    new-instance v16, Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;-><init>()V

    move-object/from16 v0, v16

    iput-object v0, v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    .line 206
    sget-object v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v14, v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    if-nez v14, :cond_2

    .line 207
    new-instance v14, Ljava/lang/IllegalStateException;

    const-string v16, "E_INVALID_STATE : Fail to create JniPluginManager"

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 210
    :cond_2
    sget-object v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    iput-object v0, v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    .line 211
    sget-object v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v14, v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    if-nez v14, :cond_3

    .line 212
    new-instance v14, Ljava/lang/IllegalStateException;

    const-string v16, "E_INVALID_STATE : Fail to create ArrayList for BuiltInPluginList"

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 215
    :cond_3
    sget-object v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    iput-object v0, v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    .line 216
    sget-object v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v14, v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    if-nez v14, :cond_4

    .line 217
    new-instance v14, Ljava/lang/IllegalStateException;

    const-string v16, "E_INVALID_STATE : Fail to create ArrayList for APKPluginList"

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 220
    :cond_4
    sget-object v16, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->BUILTIN_PLUGIN_LIST:[[Ljava/lang/String;

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/4 v14, 0x0

    :goto_0
    move/from16 v0, v17

    if-lt v14, v0, :cond_9

    .line 254
    const/4 v10, 0x0

    .line 255
    .local v10, "is":Ljava/io/InputStream;
    const/4 v6, 0x1

    .line 256
    .local v6, "checkBuiltIn":Z
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v12

    .line 259
    .local v12, "mngr":Landroid/content/res/AssetManager;
    :try_start_2
    const-string v14, "plugin.ini"

    invoke-virtual {v12, v14}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v10

    .line 265
    :goto_1
    if-eqz v6, :cond_6

    .line 267
    const/4 v11, 0x0

    .line 269
    .local v11, "line":Ljava/lang/String;
    :try_start_3
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v14, Ljava/io/InputStreamReader;

    invoke-direct {v14, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v14}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 271
    .local v2, "br":Ljava/io/BufferedReader;
    :cond_5
    :goto_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_e

    .line 308
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 322
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v11    # "line":Ljava/lang/String;
    :cond_6
    if-eqz v10, :cond_7

    .line 324
    :try_start_4
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 325
    const/4 v10, 0x0

    .line 333
    :cond_7
    :goto_3
    :try_start_5
    const-string v14, "PluginManager"

    const-string v16, "Constructor is completed."

    move-object/from16 v0, v16

    invoke-static {v14, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    .end local v6    # "checkBuiltIn":Z
    .end local v8    # "filter":Landroid/content/IntentFilter;
    .end local v10    # "is":Ljava/io/InputStream;
    .end local v12    # "mngr":Landroid/content/res/AssetManager;
    :cond_8
    sget-object v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit v15

    return-object v14

    .line 220
    .restart local v8    # "filter":Landroid/content/IntentFilter;
    :cond_9
    :try_start_6
    aget-object v13, v16, v14

    .line 221
    .local v13, "plugin":[Ljava/lang/String;
    array-length v0, v13

    move/from16 v18, v0

    const/16 v19, 0xe

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_a

    .line 220
    :goto_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 224
    :cond_a
    new-instance v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    invoke-direct {v5}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;-><init>()V

    .line 225
    .local v5, "builtInPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    const/16 v18, 0x0

    aget-object v18, v13, v18

    move-object/from16 v0, v18

    iput-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    .line 226
    const/16 v18, 0x1

    aget-object v18, v13, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    iput v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    .line 227
    const/16 v18, 0x2

    aget-object v18, v13, v18

    move-object/from16 v0, v18

    iput-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    .line 228
    const/16 v18, 0x3

    aget-object v18, v13, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    iput v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceVersion:I

    .line 229
    const/16 v18, 0x4

    aget-object v4, v13, v18

    .line 230
    .local v4, "builtInBinaryType":Ljava/lang/String;
    const/16 v18, 0x5

    aget-object v18, v13, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v18

    move/from16 v0, v18

    iput-boolean v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    .line 231
    const/16 v18, 0x6

    aget-object v18, v13, v18

    move-object/from16 v0, v18

    iput-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    .line 232
    const/16 v18, 0x7

    aget-object v18, v13, v18

    move-object/from16 v0, v18

    iput-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->selectedIconImageUri:Ljava/lang/String;

    .line 233
    const/16 v18, 0x8

    aget-object v18, v13, v18

    move-object/from16 v0, v18

    iput-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    .line 234
    const/16 v18, 0x9

    aget-object v18, v13, v18

    move-object/from16 v0, v18

    iput-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->focusedIconImageUri:Ljava/lang/String;

    .line 235
    const/16 v18, 0xa

    aget-object v18, v13, v18

    move-object/from16 v0, v18

    iput-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    .line 236
    const/16 v18, 0xb

    aget-object v18, v13, v18

    move-object/from16 v0, v18

    iput-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    .line 237
    const/16 v18, 0xc

    aget-object v18, v13, v18

    move-object/from16 v0, v18

    iput-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    .line 238
    const/16 v18, 0xd

    aget-object v18, v13, v18

    move-object/from16 v0, v18

    iput-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    .line 240
    const-string v18, "native"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 241
    const/16 v18, 0x1

    move/from16 v0, v18

    iput v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    .line 246
    :goto_5
    sget-object v18, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_b
    :goto_6
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_d

    .line 251
    sget-object v18, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 243
    :cond_c
    const/16 v18, 0x0

    move/from16 v0, v18

    iput v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    goto :goto_5

    .line 246
    :cond_d
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 247
    .local v9, "info":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v19, v0

    iget-object v0, v9, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    goto :goto_6

    .line 260
    .end local v4    # "builtInBinaryType":Ljava/lang/String;
    .end local v5    # "builtInPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .end local v9    # "info":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .end local v13    # "plugin":[Ljava/lang/String;
    .restart local v6    # "checkBuiltIn":Z
    .restart local v10    # "is":Ljava/io/InputStream;
    .restart local v12    # "mngr":Landroid/content/res/AssetManager;
    :catch_0
    move-exception v7

    .line 261
    .local v7, "e":Ljava/io/IOException;
    const-string v14, "PluginManager"

    const-string v16, "Fail to read Plugin List of App"

    move-object/from16 v0, v16

    invoke-static {v14, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 262
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 272
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v11    # "line":Ljava/lang/String;
    :cond_e
    :try_start_7
    new-instance v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    invoke-direct {v5}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;-><init>()V

    .line 273
    .restart local v5    # "builtInPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    const-string v14, ";"

    invoke-virtual {v11, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 275
    .local v3, "builtInArray":[Ljava/lang/String;
    array-length v14, v3

    const/16 v16, 0xe

    move/from16 v0, v16

    if-ne v14, v0, :cond_5

    .line 278
    const/4 v14, 0x0

    aget-object v14, v3, v14

    iput-object v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    .line 279
    const/4 v14, 0x1

    aget-object v14, v3, v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    iput v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    .line 280
    const/4 v14, 0x2

    aget-object v14, v3, v14

    iput-object v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    .line 281
    const/4 v14, 0x3

    aget-object v14, v3, v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    iput v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceVersion:I

    .line 282
    const/4 v14, 0x4

    aget-object v4, v3, v14

    .line 283
    .restart local v4    # "builtInBinaryType":Ljava/lang/String;
    const/4 v14, 0x5

    aget-object v14, v3, v14

    invoke-static {v14}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v14

    iput-boolean v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    .line 284
    const/4 v14, 0x6

    aget-object v14, v3, v14

    iput-object v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    .line 285
    const/4 v14, 0x7

    aget-object v14, v3, v14

    iput-object v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->selectedIconImageUri:Ljava/lang/String;

    .line 286
    const/16 v14, 0x8

    aget-object v14, v3, v14

    iput-object v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    .line 287
    const/16 v14, 0x9

    aget-object v14, v3, v14

    iput-object v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->focusedIconImageUri:Ljava/lang/String;

    .line 288
    const/16 v14, 0xa

    aget-object v14, v3, v14

    iput-object v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    .line 289
    const/16 v14, 0xb

    aget-object v14, v3, v14

    iput-object v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    .line 290
    const/16 v14, 0xc

    aget-object v14, v3, v14

    iput-object v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    .line 291
    const/16 v14, 0xd

    aget-object v14, v3, v14

    iput-object v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    .line 293
    const-string v14, "native"

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_10

    .line 294
    const/4 v14, 0x1

    iput v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    .line 299
    :goto_7
    sget-object v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v14, v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_f
    :goto_8
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-nez v16, :cond_11

    .line 305
    sget-object v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v14, v14, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    invoke-interface {v14, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    .line 309
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v3    # "builtInArray":[Ljava/lang/String;
    .end local v4    # "builtInBinaryType":Ljava/lang/String;
    .end local v5    # "builtInPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    :catch_1
    move-exception v7

    .line 310
    .restart local v7    # "e":Ljava/io/IOException;
    :try_start_8
    const-string v14, "PluginManager"

    const-string v16, "I/O error occurs"

    move-object/from16 v0, v16

    invoke-static {v14, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    .line 312
    new-instance v14, Ljava/lang/IllegalStateException;

    .line 313
    const-string v16, "E_INVALID_STATE : Fail to update ArrayList for BuiltInPluginList of App"

    .line 312
    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v14
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 296
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "builtInArray":[Ljava/lang/String;
    .restart local v4    # "builtInBinaryType":Ljava/lang/String;
    .restart local v5    # "builtInPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    :cond_10
    const/4 v14, 0x0

    :try_start_9
    iput v14, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_7

    .line 314
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v3    # "builtInArray":[Ljava/lang/String;
    .end local v4    # "builtInBinaryType":Ljava/lang/String;
    .end local v5    # "builtInPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    :catch_2
    move-exception v7

    .line 315
    .local v7, "e":Ljava/lang/NumberFormatException;
    :try_start_a
    const-string v14, "PluginManager"

    const-string v16, "I/O error occurs"

    move-object/from16 v0, v16

    invoke-static {v14, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 317
    new-instance v14, Ljava/lang/IllegalStateException;

    .line 318
    const-string v16, "E_INVALID_STATE : Fail to update ArrayList for BuiltInPluginList of App"

    .line 317
    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v14
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 299
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "builtInArray":[Ljava/lang/String;
    .restart local v4    # "builtInBinaryType":Ljava/lang/String;
    .restart local v5    # "builtInPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    :cond_11
    :try_start_b
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 300
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v16, v0

    iget-object v0, v9, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result v16

    if-eqz v16, :cond_f

    goto :goto_8

    .line 326
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v3    # "builtInArray":[Ljava/lang/String;
    .end local v4    # "builtInBinaryType":Ljava/lang/String;
    .end local v5    # "builtInPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .end local v9    # "info":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .end local v11    # "line":Ljava/lang/String;
    :catch_3
    move-exception v7

    .line 327
    .local v7, "e":Ljava/io/IOException;
    :try_start_c
    const-string v14, "PluginManager"

    const-string v16, "I/O error occurs"

    move-object/from16 v0, v16

    invoke-static {v14, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_3
.end method


# virtual methods
.method deleteAPKPluginInfo(Ljava/lang/String;)V
    .locals 4
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 1140
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1146
    :goto_0
    return-void

    .line 1140
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 1141
    .local v0, "info":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1142
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getNativeHandle(Ljava/lang/Object;)I
    .locals 10
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 979
    const-string v5, "PluginManager"

    const-string v8, "getNativeHandle()"

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 981
    if-nez p1, :cond_0

    .line 982
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v8, "E_INVALID_ARG : parameter \'object\' is null"

    invoke-direct {v5, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 985
    :cond_0
    const-wide/16 v6, 0x0

    .line 986
    .local v6, "ret":J
    const/4 v2, 0x0

    .line 987
    .local v2, "handler":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    const/4 v0, 0x0

    .line 989
    .local v0, "className":Ljava/lang/String;
    invoke-static {p1}, Ljava/lang/reflect/Proxy;->getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;

    move-result-object v2

    .end local v2    # "handler":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    check-cast v2, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;

    .line 990
    .restart local v2    # "handler":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    iget-object v5, v2, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 992
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    monitor-enter v8

    .line 993
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 992
    :goto_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1006
    const-string v5, "PluginManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "getNativeHandle() is completed. returns "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    long-to-int v5, v6

    return v5

    .line 994
    :cond_2
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    .line 995
    .local v4, "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v9, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "."

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v9, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 997
    iget-object v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/reflect/Proxy;->getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;

    move-result-object v1

    .line 996
    check-cast v1, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;

    .line 998
    .local v1, "h":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    iget-object v5, v2, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    iget-object v9, v1, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    invoke-virtual {v5, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 999
    iget-wide v6, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    .line 1000
    goto :goto_0

    .line 992
    .end local v1    # "h":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    .end local v3    # "it":Ljava/util/Iterator;
    .end local v4    # "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :catchall_0
    move-exception v5

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5
.end method

.method public getNativeHandle_64(Ljava/lang/Object;)J
    .locals 10
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 1011
    const-string v5, "PluginManager"

    const-string v8, "getNativeHandle()"

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1013
    if-nez p1, :cond_0

    .line 1014
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v8, "E_INVALID_ARG : parameter \'object\' is null"

    invoke-direct {v5, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1017
    :cond_0
    const-wide/16 v6, 0x0

    .line 1018
    .local v6, "ret":J
    const/4 v2, 0x0

    .line 1019
    .local v2, "handler":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    const/4 v0, 0x0

    .line 1021
    .local v0, "className":Ljava/lang/String;
    invoke-static {p1}, Ljava/lang/reflect/Proxy;->getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;

    move-result-object v2

    .end local v2    # "handler":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    check-cast v2, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;

    .line 1022
    .restart local v2    # "handler":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    iget-object v5, v2, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1024
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    monitor-enter v8

    .line 1025
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1024
    :goto_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1038
    const-string v5, "PluginManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "getNativeHandle() is completed. returns "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1039
    return-wide v6

    .line 1026
    :cond_2
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    .line 1027
    .local v4, "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v9, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "."

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v9, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1029
    iget-object v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/reflect/Proxy;->getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;

    move-result-object v1

    .line 1028
    check-cast v1, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;

    .line 1030
    .local v1, "h":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    iget-object v5, v2, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    iget-object v9, v1, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    invoke-virtual {v5, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1031
    iget-wide v6, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    .line 1032
    goto :goto_0

    .line 1024
    .end local v1    # "h":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    .end local v3    # "it":Ljava/util/Iterator;
    .end local v4    # "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :catchall_0
    move-exception v5

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5
.end method

.method public getPluginInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .locals 7
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 448
    const-string v5, "PluginManager"

    const-string v6, "getPluginInfo()"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    if-nez p1, :cond_0

    .line 451
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "E_INVALID_ARG : parameter \'uuid\' is null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 454
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 456
    .local v3, "builtIniterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 481
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 483
    .local v1, "apkPluginiterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_4

    .line 508
    const-string v5, "PluginManager"

    const-string v6, "getPluginInfo() returns false"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    const/4 v4, 0x0

    .end local v1    # "apkPluginiterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :goto_0
    return-object v4

    .line 460
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 461
    .local v2, "builtInPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v4, v2

    .line 477
    goto :goto_0

    .line 487
    .end local v2    # "builtInPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .restart local v1    # "apkPluginiterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 488
    .local v0, "apkPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 489
    new-instance v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;-><init>()V

    .line 490
    .local v4, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    iput-object v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    .line 491
    iget v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    iput v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    .line 492
    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    iput-object v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    .line 493
    iget v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceVersion:I

    iput v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceVersion:I

    .line 494
    iget v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    iput v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    .line 495
    iget-boolean v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    iput-boolean v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    .line 496
    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    iput-object v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    .line 497
    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->selectedIconImageUri:Ljava/lang/String;

    iput-object v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->selectedIconImageUri:Ljava/lang/String;

    .line 498
    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    iput-object v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    .line 499
    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->focusedIconImageUri:Ljava/lang/String;

    iput-object v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->focusedIconImageUri:Ljava/lang/String;

    .line 500
    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    iput-object v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    .line 501
    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    iput-object v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    .line 502
    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    iput-object v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    .line 503
    const-string v5, "PluginManager"

    const-string v6, "getPluginInfo() is completed"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public getPluginList(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 395
    const-string v5, "PluginManager"

    const-string v6, "getPluginList()"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    if-nez p1, :cond_0

    .line 398
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "E_INVALID_ARG : parameter \'type\' is null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 401
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 402
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 404
    .local v3, "builtIniterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 414
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 416
    .local v1, "apkPluginiterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_5

    .line 426
    const-string v5, "PluginManager"

    const-string v6, "getPluginList() is completed."

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    return-object v4

    .line 407
    .end local v1    # "apkPluginiterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 409
    .local v2, "builtInPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v5, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "all"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 410
    :cond_4
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 419
    .end local v2    # "builtInPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .restart local v1    # "apkPluginiterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 421
    .local v0, "apkPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "all"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 422
    :cond_6
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getPrivateKeyHint(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;)Ljava/lang/String;
    .locals 6
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .prologue
    .line 348
    if-nez p1, :cond_0

    .line 349
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "E_INVALID_ARG : parameter \'info\' is null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 352
    :cond_0
    const/4 v0, 0x0

    .line 353
    .local v0, "hint":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 354
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 353
    :goto_0
    monitor-exit v4

    .line 362
    return-object v0

    .line 355
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    .line 356
    .local v2, "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    iget-object v3, p1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    iget-object v5, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    iget-object v5, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 357
    iget-object v3, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;->getPrivateKeyHint()Ljava/lang/String;

    move-result-object v0

    .line 358
    goto :goto_0

    .line 353
    .end local v1    # "it":Ljava/util/Iterator;
    .end local v2    # "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public loadPlugin(Landroid/app/Activity;Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;Ljava/lang/String;)Ljava/lang/Object;
    .locals 22
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "info"    # Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .param p3, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 668
    const-string v18, "PluginManager"

    const-string v19, "loadPlugin()"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    if-nez p1, :cond_0

    .line 671
    new-instance v18, Ljava/lang/IllegalStateException;

    const-string v19, "E_INVALID_STATE : Unable to Use loadPlugin by null Activity"

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 674
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    move-object/from16 v18, v0

    if-nez v18, :cond_1

    .line 675
    new-instance v18, Ljava/lang/IllegalStateException;

    const-string v19, "E_INVALID_STATE : Unable to Use loadPlugin by null JniPluginManager"

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 678
    :cond_1
    if-nez p2, :cond_2

    .line 679
    new-instance v18, Ljava/lang/IllegalArgumentException;

    const-string v19, "E_INVALID_ARG : parameter \'info\' is null"

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 682
    :cond_2
    const/4 v12, 0x0

    .line 683
    .local v12, "object":Ljava/lang/Object;
    const/4 v6, 0x0

    .line 685
    .local v6, "checkBuiltIn":Z
    const/4 v15, 0x0

    .line 687
    .local v15, "pluginClassLoader":Ljava/lang/ClassLoader;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    monitor-enter v19

    .line 688
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "it":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_5

    .line 687
    :goto_0
    monitor-exit v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 697
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_4
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_6

    .line 705
    :goto_1
    if-eqz v6, :cond_7

    .line 706
    const/4 v9, 0x0

    .line 707
    .local v9, "interfaceName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 708
    .local v7, "className":Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    const-class v19, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 709
    new-instance v18, Ljava/lang/StringBuilder;

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 710
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v15

    .line 711
    const/16 v18, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v19

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v9, v0, v1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v7, v15}, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder;->build(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v12

    .line 743
    :goto_2
    if-eqz v12, :cond_d

    .line 744
    new-instance v16, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;-><init>()V

    .line 745
    .local v16, "pluginObject":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->type:Ljava/lang/String;

    .line 746
    move-object/from16 v0, p2

    iget v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v16

    iput v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->binaryType:I

    .line 747
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    .line 748
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    .line 749
    move-object/from16 v0, v16

    iput-object v12, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    .line 750
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->dummyObject:Ljava/lang/Object;

    .line 751
    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->binaryType:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_a

    move-object/from16 v18, v12

    .line 752
    check-cast v18, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenNativeHandleInterface;

    invoke-interface/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenNativeHandleInterface;->getNativeHandle()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, v16

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    .line 764
    :goto_3
    move-object/from16 v0, v16

    iput-object v15, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->classLoader:Ljava/lang/ClassLoader;

    move-object/from16 v18, v12

    .line 766
    check-cast v18, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;->unlock(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_c

    .line 767
    new-instance v18, Ljava/lang/IllegalArgumentException;

    const-string v19, "E_INVALID_ARG : parameter \'key\' is wrong"

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 689
    .end local v7    # "className":Ljava/lang/String;
    .end local v9    # "interfaceName":Ljava/lang/String;
    .end local v16    # "pluginObject":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :cond_5
    :try_start_1
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    .line 690
    .local v11, "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    iget-object v0, v11, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v18, v0

    iget-object v0, v11, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 691
    iget-object v15, v11, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->classLoader:Ljava/lang/ClassLoader;

    .line 692
    goto/16 :goto_0

    .line 687
    .end local v10    # "it":Ljava/util/Iterator;
    .end local v11    # "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :catchall_0
    move-exception v18

    monitor-exit v19
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v18

    .line 697
    .restart local v10    # "it":Ljava/util/Iterator;
    :cond_6
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 698
    .local v5, "builtinPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    iget-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 699
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v19, v0

    iget-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 700
    const/4 v6, 0x1

    .line 701
    goto/16 :goto_1

    .line 714
    .end local v5    # "builtinPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    :cond_7
    const/4 v9, 0x0

    .line 715
    .restart local v9    # "interfaceName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 716
    .restart local v7    # "className":Ljava/lang/String;
    const/4 v14, 0x0

    .line 718
    .local v14, "pluginApkSourceDir":Ljava/lang/String;
    const/4 v4, 0x0

    .line 720
    .local v4, "ai":Landroid/content/pm/ApplicationInfo;
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 721
    .local v17, "pm":Landroid/content/pm/PackageManager;
    if-nez v17, :cond_8

    .line 722
    const-string v18, "PluginManager"

    const-string v19, "PackageManager is null"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    const/4 v12, 0x0

    .line 786
    .end local v4    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v12    # "object":Ljava/lang/Object;
    .end local v14    # "pluginApkSourceDir":Ljava/lang/String;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    :goto_4
    return-object v12

    .line 725
    .restart local v4    # "ai":Landroid/content/pm/ApplicationInfo;
    .restart local v12    # "object":Ljava/lang/Object;
    .restart local v14    # "pluginApkSourceDir":Ljava/lang/String;
    .restart local v17    # "pm":Landroid/content/pm/PackageManager;
    :cond_8
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x80

    invoke-virtual/range {v17 .. v19}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v4

    .line 731
    new-instance v18, Ljava/lang/StringBuilder;

    const-class v19, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 732
    iget-object v7, v4, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    .line 733
    iget-object v14, v4, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 735
    if-nez v15, :cond_9

    .line 736
    new-instance v15, Ldalvik/system/PathClassLoader;

    .end local v15    # "pluginClassLoader":Ljava/lang/ClassLoader;
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v15, v14, v0}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 739
    .restart local v15    # "pluginClassLoader":Ljava/lang/ClassLoader;
    :cond_9
    const/16 v18, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v19

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v9, v0, v1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v7, v15}, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder;->build(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v12

    goto/16 :goto_2

    .line 726
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v8

    .line 728
    .local v8, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v8}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 729
    const/4 v12, 0x0

    goto :goto_4

    .line 754
    .end local v4    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v8    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v14    # "pluginApkSourceDir":Ljava/lang/String;
    .restart local v16    # "pluginObject":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :cond_a
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->type:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "Pen"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 755
    new-instance v13, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;

    invoke-direct {v13}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;-><init>()V

    .line 756
    .local v13, "pen":Lcom/samsung/android/sdk/pen/pen/preload/NativePen;
    invoke-virtual {v13}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->construct()V

    .line 757
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->setObject(Ljava/lang/Object;)V

    .line 758
    move-object/from16 v0, v16

    iput-object v13, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->dummyObject:Ljava/lang/Object;

    .line 759
    invoke-virtual {v13}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->getNativeHandle()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, v16

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    goto/16 :goto_3

    .line 761
    .end local v13    # "pen":Lcom/samsung/android/sdk/pen/pen/preload/NativePen;
    :cond_b
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, v16

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    goto/16 :goto_3

    .line 770
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    monitor-enter v19

    .line 771
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 770
    monitor-exit v19
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 774
    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-eqz v18, :cond_f

    .line 775
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mIs64:Z

    move/from16 v18, v0

    if-eqz v18, :cond_e

    .line 776
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    move-object/from16 v3, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;->onLoad(JLandroid/content/Context;)V

    .line 785
    .end local v16    # "pluginObject":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :cond_d
    :goto_5
    const-string v18, "PluginManager"

    const-string v19, "loadPlugin() is completed"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 770
    .restart local v16    # "pluginObject":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :catchall_1
    move-exception v18

    :try_start_4
    monitor-exit v19
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v18

    .line 778
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;->onLoad(ILandroid/content/Context;)V

    goto :goto_5

    :cond_f
    move-object/from16 v18, v12

    .line 781
    check-cast v18, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;->onLoad(Landroid/content/Context;)V

    goto :goto_5
.end method

.method public loadPlugin(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;Ljava/lang/String;)Ljava/lang/Object;
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .param p3, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 529
    const-string v18, "PluginManager"

    const-string v19, "loadPlugin()"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    if-nez p1, :cond_0

    .line 532
    new-instance v18, Ljava/lang/IllegalStateException;

    const-string v19, "E_INVALID_STATE : Unable to Use loadPlugin by null Context"

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 535
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    move-object/from16 v18, v0

    if-nez v18, :cond_1

    .line 536
    new-instance v18, Ljava/lang/IllegalStateException;

    const-string v19, "E_INVALID_STATE : Unable to Use loadPlugin by null JniPluginManager"

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 539
    :cond_1
    if-nez p2, :cond_2

    .line 540
    new-instance v18, Ljava/lang/IllegalArgumentException;

    const-string v19, "E_INVALID_ARG : parameter \'info\' is null"

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 543
    :cond_2
    const/4 v12, 0x0

    .line 544
    .local v12, "object":Ljava/lang/Object;
    const/4 v6, 0x0

    .line 546
    .local v6, "checkBuiltIn":Z
    const/4 v15, 0x0

    .line 548
    .local v15, "pluginClassLoader":Ljava/lang/ClassLoader;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    monitor-enter v19

    .line 549
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "it":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_5

    .line 548
    :goto_0
    monitor-exit v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 558
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_4
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_6

    .line 566
    :goto_1
    if-eqz v6, :cond_7

    .line 567
    const/4 v9, 0x0

    .line 568
    .local v9, "interfaceName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 569
    .local v7, "className":Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    const-class v19, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 570
    new-instance v18, Ljava/lang/StringBuilder;

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 571
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v15

    .line 572
    const/16 v18, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v19

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v9, v0, v1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v7, v15}, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder;->build(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v12

    .line 604
    :goto_2
    if-eqz v12, :cond_d

    .line 605
    new-instance v16, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;-><init>()V

    .line 606
    .local v16, "pluginObject":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->type:Ljava/lang/String;

    .line 607
    move-object/from16 v0, p2

    iget v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v16

    iput v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->binaryType:I

    .line 608
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    .line 609
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    .line 610
    move-object/from16 v0, v16

    iput-object v12, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    .line 611
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->dummyObject:Ljava/lang/Object;

    .line 612
    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->binaryType:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_a

    move-object/from16 v18, v12

    .line 613
    check-cast v18, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenNativeHandleInterface;

    invoke-interface/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenNativeHandleInterface;->getNativeHandle()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, v16

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    .line 625
    :goto_3
    move-object/from16 v0, v16

    iput-object v15, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->classLoader:Ljava/lang/ClassLoader;

    move-object/from16 v18, v12

    .line 627
    check-cast v18, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;->unlock(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_c

    .line 628
    new-instance v18, Ljava/lang/IllegalArgumentException;

    const-string v19, "E_INVALID_ARG : parameter \'key\' is wrong"

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 550
    .end local v7    # "className":Ljava/lang/String;
    .end local v9    # "interfaceName":Ljava/lang/String;
    .end local v16    # "pluginObject":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :cond_5
    :try_start_1
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    .line 551
    .local v11, "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    iget-object v0, v11, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v18, v0

    iget-object v0, v11, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 552
    iget-object v15, v11, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->classLoader:Ljava/lang/ClassLoader;

    .line 553
    goto/16 :goto_0

    .line 548
    .end local v10    # "it":Ljava/util/Iterator;
    .end local v11    # "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :catchall_0
    move-exception v18

    monitor-exit v19
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v18

    .line 558
    .restart local v10    # "it":Ljava/util/Iterator;
    :cond_6
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 559
    .local v5, "builtinPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    iget-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 560
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v19, v0

    iget-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 561
    const/4 v6, 0x1

    .line 562
    goto/16 :goto_1

    .line 575
    .end local v5    # "builtinPluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    :cond_7
    const/4 v9, 0x0

    .line 576
    .restart local v9    # "interfaceName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 577
    .restart local v7    # "className":Ljava/lang/String;
    const/4 v14, 0x0

    .line 579
    .local v14, "pluginApkSourceDir":Ljava/lang/String;
    const/4 v4, 0x0

    .line 581
    .local v4, "ai":Landroid/content/pm/ApplicationInfo;
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 582
    .local v17, "pm":Landroid/content/pm/PackageManager;
    if-nez v17, :cond_8

    .line 583
    const-string v18, "PluginManager"

    const-string v19, "PackageManager is null"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    const/4 v12, 0x0

    .line 647
    .end local v4    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v12    # "object":Ljava/lang/Object;
    .end local v14    # "pluginApkSourceDir":Ljava/lang/String;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    :goto_4
    return-object v12

    .line 586
    .restart local v4    # "ai":Landroid/content/pm/ApplicationInfo;
    .restart local v12    # "object":Ljava/lang/Object;
    .restart local v14    # "pluginApkSourceDir":Ljava/lang/String;
    .restart local v17    # "pm":Landroid/content/pm/PackageManager;
    :cond_8
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x80

    invoke-virtual/range {v17 .. v19}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v4

    .line 592
    new-instance v18, Ljava/lang/StringBuilder;

    const-class v19, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 593
    iget-object v7, v4, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    .line 594
    iget-object v14, v4, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 596
    if-nez v15, :cond_9

    .line 597
    new-instance v15, Ldalvik/system/PathClassLoader;

    .end local v15    # "pluginClassLoader":Ljava/lang/ClassLoader;
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v15, v14, v0}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 600
    .restart local v15    # "pluginClassLoader":Ljava/lang/ClassLoader;
    :cond_9
    const/16 v18, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v19

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v9, v0, v1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v7, v15}, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder;->build(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v12

    goto/16 :goto_2

    .line 587
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v8

    .line 589
    .local v8, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v8}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 590
    const/4 v12, 0x0

    goto :goto_4

    .line 615
    .end local v4    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v8    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v14    # "pluginApkSourceDir":Ljava/lang/String;
    .restart local v16    # "pluginObject":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :cond_a
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->type:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "Pen"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 616
    new-instance v13, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;

    invoke-direct {v13}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;-><init>()V

    .line 617
    .local v13, "pen":Lcom/samsung/android/sdk/pen/pen/preload/NativePen;
    invoke-virtual {v13}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->construct()V

    .line 618
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->setObject(Ljava/lang/Object;)V

    .line 619
    move-object/from16 v0, v16

    iput-object v13, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->dummyObject:Ljava/lang/Object;

    .line 620
    invoke-virtual {v13}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->getNativeHandle()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, v16

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    goto/16 :goto_3

    .line 622
    .end local v13    # "pen":Lcom/samsung/android/sdk/pen/pen/preload/NativePen;
    :cond_b
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, v16

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    goto/16 :goto_3

    .line 631
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    monitor-enter v19

    .line 632
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 631
    monitor-exit v19
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 635
    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-eqz v18, :cond_f

    .line 636
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mIs64:Z

    move/from16 v18, v0

    if-eqz v18, :cond_e

    .line 637
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    move-object/from16 v3, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;->onLoad(JLandroid/content/Context;)V

    .line 646
    .end local v16    # "pluginObject":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :cond_d
    :goto_5
    const-string v18, "PluginManager"

    const-string v19, "loadPlugin() is completed"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 631
    .restart local v16    # "pluginObject":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :catchall_1
    move-exception v18

    :try_start_4
    monitor-exit v19
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v18

    .line 639
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;->onLoad(ILandroid/content/Context;)V

    goto :goto_5

    :cond_f
    move-object/from16 v18, v12

    .line 642
    check-cast v18, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;->onLoad(Landroid/content/Context;)V

    goto :goto_5
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;

    .prologue
    .line 933
    const-string v0, "PluginManager"

    const-string v1, "setListener()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mPackageReceiver:Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;

    if-nez v0, :cond_0

    .line 936
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : Unable to Use setPluginListener by null PackageReceiver"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 939
    :cond_0
    if-nez p1, :cond_1

    .line 940
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_ARG : parameter \'listener\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 943
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mPackageReceiver:Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;->setNotifyListener(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;)V

    .line 945
    const-string v0, "PluginManager"

    const-string v1, "setPluginListener() is completed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 946
    return-void
.end method

.method public unloadPlugin(Ljava/lang/Object;)V
    .locals 12
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 814
    const-string v6, "PluginManager"

    const-string v7, "unloadPlugin()"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    if-nez v6, :cond_0

    .line 817
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "E_INVALID_STATE : Unable to Use unloadPlugin by null JniPluginManager"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 820
    :cond_0
    if-nez p1, :cond_1

    .line 821
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "E_INVALID_ARG : parameter \'object\' is null"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 825
    :cond_1
    const/4 v3, 0x0

    .line 826
    .local v3, "handler":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    const/4 v1, 0x0

    .line 828
    .local v1, "className":Ljava/lang/String;
    invoke-static {p1}, Ljava/lang/reflect/Proxy;->getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;

    move-result-object v3

    .end local v3    # "handler":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    check-cast v3, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;

    .line 829
    .restart local v3    # "handler":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    if-nez v3, :cond_2

    .line 830
    new-instance v6, Ljava/lang/NullPointerException;

    const-string v7, "E_INVALID_ARG : proxy handler of object is null"

    invoke-direct {v6, v7}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 832
    :cond_2
    iget-object v6, v3, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 835
    const/4 v0, 0x0

    .line 837
    .local v0, "checkUnloaded":Z
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    monitor-enter v7

    .line 838
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "it":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_4

    .line 837
    .end local p1    # "object":Ljava/lang/Object;
    :goto_0
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 884
    if-nez v0, :cond_7

    .line 885
    const-string v6, "PluginManager"

    const-string v7, "unloadPlugin() returns false"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 886
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "E_INVALID_ARG : Data to unload does not found"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 839
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_4
    :try_start_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    .line 840
    .local v5, "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v8, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "."

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 841
    const/4 v0, 0x1

    .line 862
    iget-object v6, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/reflect/Proxy;->getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;

    move-result-object v2

    .line 861
    check-cast v2, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;

    .line 863
    .local v2, "h":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    iget-object v6, v3, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    iget-object v8, v2, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    invoke-virtual {v6, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 864
    iget-wide v8, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-eqz v6, :cond_6

    .line 865
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mIs64:Z

    if-eqz v6, :cond_5

    .line 866
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    iget-wide v8, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    invoke-virtual {v6, v8, v9}, Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;->onUnload(J)V

    .line 874
    .end local p1    # "object":Ljava/lang/Object;
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 875
    const/4 v6, 0x0

    iput-object v6, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    .line 876
    const/4 v5, 0x0

    .line 877
    const/4 p1, 0x0

    .line 878
    .restart local p1    # "object":Ljava/lang/Object;
    goto :goto_0

    .line 868
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    iget-wide v8, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:J

    long-to-int v8, v8

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;->onUnload(I)V

    goto :goto_1

    .line 837
    .end local v2    # "h":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    .end local v4    # "it":Ljava/util/Iterator;
    .end local v5    # "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    .end local p1    # "object":Ljava/lang/Object;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .line 871
    .restart local v2    # "h":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    .restart local v4    # "it":Ljava/util/Iterator;
    .restart local v5    # "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_6
    :try_start_2
    check-cast p1, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-interface {p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;->onUnload()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 888
    .end local v2    # "h":Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;
    .end local v5    # "o":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;
    :cond_7
    const-string v6, "PluginManager"

    const-string v7, "unloadPlugin() is completed"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 891
    return-void
.end method

.method updateAPKPluginList(Landroid/content/Context;)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1043
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    .line 1044
    .local v9, "intent":Landroid/content/Intent;
    const-string v18, "com.samsung.android.sdk.pen.plugin.PICK"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1046
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v18

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v9, v1}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 1049
    .local v11, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_1

    .line 1137
    return-void

    .line 1052
    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/pm/ResolveInfo;

    .line 1054
    .local v16, "resolveinfo":Landroid/content/pm/ResolveInfo;
    const/4 v10, 0x0

    .line 1055
    .local v10, "isExist":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_4

    .line 1062
    :goto_1
    if-nez v10, :cond_0

    .line 1066
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_5

    .line 1073
    :goto_2
    if-nez v10, :cond_0

    .line 1077
    const/4 v14, 0x0

    .line 1078
    .local v14, "pluginInfoData":Ljava/lang/String;
    new-instance v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    invoke-direct {v13}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;-><init>()V

    .line 1079
    .local v13, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "\\."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 1081
    .local v17, "split":[Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    .line 1082
    .local v12, "packageName":Ljava/lang/StringBuffer;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_3
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    if-lt v7, v0, :cond_6

    .line 1086
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->length()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 1087
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    .line 1088
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    aget-object v18, v17, v18

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    .line 1090
    const/4 v2, 0x0

    .line 1092
    .local v2, "ai":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    .line 1093
    .local v15, "pm":Landroid/content/pm/PackageManager;
    if-nez v15, :cond_7

    .line 1094
    const-string v18, "PluginManager"

    const-string v19, "PackageManager is null"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1099
    .end local v15    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v6

    .line 1101
    .local v6, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 1055
    .end local v2    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v6    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v7    # "i":I
    .end local v12    # "packageName":Ljava/lang/StringBuffer;
    .end local v13    # "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .end local v14    # "pluginInfoData":Ljava/lang/String;
    .end local v17    # "split":[Ljava/lang/String;
    :cond_4
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 1056
    .local v8, "info":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    iget-object v0, v8, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v21, "."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    .line 1057
    iget-object v0, v8, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1056
    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    .line 1057
    if-eqz v19, :cond_2

    .line 1058
    const/4 v10, 0x1

    .line 1059
    goto/16 :goto_1

    .line 1066
    .end local v8    # "info":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    :cond_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 1067
    .restart local v8    # "info":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    iget-object v0, v8, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v21, "."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    .line 1068
    iget-object v0, v8, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1067
    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    .line 1068
    if-eqz v19, :cond_3

    .line 1069
    const/4 v10, 0x1

    .line 1070
    goto/16 :goto_2

    .line 1083
    .end local v8    # "info":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .restart local v7    # "i":I
    .restart local v12    # "packageName":Ljava/lang/StringBuffer;
    .restart local v13    # "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .restart local v14    # "pluginInfoData":Ljava/lang/String;
    .restart local v17    # "split":[Ljava/lang/String;
    :cond_6
    aget-object v18, v17, v7

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1084
    const-string v18, "."

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1082
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_3

    .line 1097
    .restart local v2    # "ai":Landroid/content/pm/ApplicationInfo;
    .restart local v15    # "pm":Landroid/content/pm/PackageManager;
    :cond_7
    :try_start_1
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 1098
    const/16 v19, 0x80

    .line 1097
    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    .line 1104
    iget-object v5, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 1105
    .local v5, "bundle":Landroid/os/Bundle;
    const-string v18, "SPEN_PLUGIN_INFO"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1106
    if-eqz v14, :cond_0

    .line 1110
    const-string v18, ";"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1112
    .local v3, "array":[Ljava/lang/String;
    array-length v0, v3

    move/from16 v18, v0

    const/16 v19, 0xe

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 1116
    const/16 v18, 0x0

    aget-object v18, v3, v18

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    .line 1117
    const/16 v18, 0x1

    aget-object v18, v3, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    iput v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    .line 1118
    const/16 v18, 0x2

    aget-object v18, v3, v18

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    .line 1119
    const/16 v18, 0x3

    aget-object v18, v3, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    iput v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceVersion:I

    .line 1120
    const/16 v18, 0x4

    aget-object v4, v3, v18

    .line 1121
    .local v4, "binaryType":Ljava/lang/String;
    const/16 v18, 0x5

    aget-object v18, v3, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v18

    move/from16 v0, v18

    iput-boolean v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    .line 1122
    const/16 v18, 0x6

    aget-object v18, v3, v18

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    .line 1123
    const/16 v18, 0x7

    aget-object v18, v3, v18

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->selectedIconImageUri:Ljava/lang/String;

    .line 1124
    const/16 v18, 0x8

    aget-object v18, v3, v18

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    .line 1125
    const/16 v18, 0x9

    aget-object v18, v3, v18

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->focusedIconImageUri:Ljava/lang/String;

    .line 1126
    const/16 v18, 0xa

    aget-object v18, v3, v18

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    .line 1127
    const/16 v18, 0xb

    aget-object v18, v3, v18

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    .line 1129
    const-string v18, "native"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 1130
    const/16 v18, 0x1

    move/from16 v0, v18

    iput v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    .line 1135
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1132
    :cond_8
    const/16 v18, 0x0

    move/from16 v0, v18

    iput v0, v13, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    goto :goto_4
.end method
