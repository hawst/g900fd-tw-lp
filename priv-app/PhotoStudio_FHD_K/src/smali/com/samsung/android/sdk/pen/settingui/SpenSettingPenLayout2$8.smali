.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout2.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    .line 1059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v7, 0x42b20000    # 89.0f

    const/4 v8, 0x1

    .line 1062
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-ne v5, v8, :cond_3

    .line 1064
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isPresetClicked:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$19(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1102
    :cond_0
    :goto_0
    return v8

    .line 1069
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 1070
    .local v3, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 1074
    .local v4, "y":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x42820000    # 65.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    int-to-float v5, v5

    .line 1075
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    int-to-float v6, v6

    .line 1074
    div-float/2addr v5, v6

    .line 1075
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1076
    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    int-to-float v6, v6

    .line 1075
    sub-float/2addr v6, v4

    .line 1074
    mul-float/2addr v5, v6

    .line 1073
    sub-float v5, v3, v5

    .line 1076
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41a80000    # 21.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    int-to-float v6, v6

    .line 1073
    add-float/2addr v5, v6

    .line 1076
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1077
    const/high16 v7, 0x42600000    # 56.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    int-to-float v6, v6

    .line 1073
    div-float/2addr v5, v6

    float-to-int v1, v5

    .line 1081
    .local v1, "index":I
    if-gez v1, :cond_4

    .line 1082
    const/4 v1, 0x0

    .line 1088
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mNumberOfPenExist:I

    if-ge v1, v5, :cond_0

    .line 1092
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$20(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1093
    .local v2, "v1":Landroid/view/View;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v0, v5, :cond_5

    .line 1101
    .end local v0    # "i":I
    .end local v1    # "index":I
    .end local v2    # "v1":Landroid/view/View;
    .end local v3    # "x":F
    .end local v4    # "y":F
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 1083
    .restart local v1    # "index":I
    .restart local v3    # "x":F
    .restart local v4    # "y":F
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-le v1, v5, :cond_2

    .line 1084
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    goto :goto_1

    .line 1094
    .restart local v0    # "i":I
    .restart local v2    # "v1":Landroid/view/View;
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-ne v2, v5, :cond_6

    .line 1095
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penSelectIndex(I)V
    invoke-static {v5, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V

    .line 1093
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method
