.class Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;
.super Ljava/lang/Object;
.source "SpenInView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenInView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnScrollListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 5490
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;)V
    .locals 0

    .prologue
    .line 5490
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    return-void
.end method


# virtual methods
.method public onUpdate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 5493
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5494
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->getState()Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_0

    .line 5495
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEditableTextBox()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5496
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V
    invoke-static {v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$42(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;Z)V

    .line 5506
    :cond_0
    :goto_0
    return-void

    .line 5498
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5499
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V
    invoke-static {v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$42(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;Z)V

    goto :goto_0

    .line 5501
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V
    invoke-static {v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$42(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method
