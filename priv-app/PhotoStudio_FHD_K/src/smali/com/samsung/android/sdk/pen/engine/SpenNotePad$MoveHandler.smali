.class Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
.super Landroid/os/Handler;
.source "SpenNotePad.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MoveHandler"
.end annotation


# static fields
.field private static final MOVING_DELAY:I = 0x258

.field private static final MOVING_MESSAGE:I = 0x1


# instance fields
.field private mIsMoving:Z

.field private mLastX:F

.field private final mSpenNotePad:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenNotePad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V
    .locals 1
    .param p1, "view"    # Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    .prologue
    .line 2058
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2053
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mIsMoving:Z

    .line 2059
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mSpenNotePad:Ljava/lang/ref/WeakReference;

    .line 2060
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x0

    .line 2079
    iget v2, p1, Landroid/os/Message;->what:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 2080
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mSpenNotePad:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    .line 2081
    .local v1, "notePad":Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    if-eqz v1, :cond_0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2102
    .end local v1    # "notePad":Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    :cond_0
    :goto_0
    return-void

    .line 2085
    .restart local v1    # "notePad":Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    :cond_1
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I

    move-result v3

    .line 2086
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    .line 2087
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    .line 2088
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setButtonState(I)V

    goto :goto_0

    .line 2091
    :cond_2
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mLastX:F

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    const/high16 v5, 0x41200000    # 10.0f

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    .line 2092
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v4

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v5

    mul-float/2addr v4, v5

    mul-float/2addr v3, v4

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 2091
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V

    .line 2094
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mIsMoving:Z

    .line 2095
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I

    move-result v3

    int-to-float v3, v3

    sub-float v0, v2, v3

    .line 2096
    .local v0, "dis":F
    const-string v2, "ZoomPad"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "move panning"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2097
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 2098
    invoke-virtual {v1, v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateBox(ZF)V

    .line 2099
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 2100
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    goto/16 :goto_0
.end method

.method public isMovingEnabled()Z
    .locals 1

    .prologue
    .line 2074
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mIsMoving:Z

    return v0
.end method

.method public setMovingEnabled(ZF)V
    .locals 1
    .param p1, "enable"    # Z
    .param p2, "x"    # F

    .prologue
    .line 2063
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mIsMoving:Z

    .line 2064
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mIsMoving:Z

    if-eqz v0, :cond_1

    .line 2065
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mLastX:F

    cmpg-float v0, v0, p2

    if-gez v0, :cond_0

    .line 2066
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mLastX:F

    .line 2071
    :cond_0
    :goto_0
    return-void

    .line 2069
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mLastX:F

    goto :goto_0
.end method
