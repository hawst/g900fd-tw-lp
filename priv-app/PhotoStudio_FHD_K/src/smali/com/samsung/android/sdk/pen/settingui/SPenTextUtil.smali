.class Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;
.super Ljava/lang/Object;
.source "SPenTextUtil.java"


# instance fields
.field protected mSdkResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->mSdkResources:Landroid/content/res/Resources;

    .line 26
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 27
    .local v1, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->mSdkResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    .end local v1    # "manager":Landroid/content/pm/PackageManager;
    :goto_0
    return-void

    .line 28
    :catch_0
    move-exception v0

    .line 30
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected setString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "strName"    # Ljava/lang/String;

    .prologue
    .line 36
    const/4 v2, 0x0

    .line 38
    .local v2, "string":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string v4, "string"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 39
    .local v1, "strID":I
    if-nez v1, :cond_0

    .line 40
    const/4 v3, 0x0

    .line 47
    .end local v1    # "strID":I
    :goto_0
    return-object v3

    .line 42
    .restart local v1    # "strID":I
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .end local v1    # "strID":I
    :goto_1
    move-object v3, v2

    .line 47
    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method
