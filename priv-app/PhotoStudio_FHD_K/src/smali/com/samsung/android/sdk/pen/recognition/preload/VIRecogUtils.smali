.class public Lcom/samsung/android/sdk/pen/recognition/preload/VIRecogUtils;
.super Ljava/lang/Object;
.source "VIRecogUtils.java"


# static fields
.field private static final SUPPORT_DB:Z = true

.field private static final TAG:Ljava/lang/String; = "VIRecogUtils"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyDatabase(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/VIRecogUtils;->copyDatabase(Landroid/content/Context;Z)Z

    move-result v0

    return v0
.end method

.method public static copyDatabase(Landroid/content/Context;Z)Z
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bIgnoreExist"    # Z

    .prologue
    .line 35
    const-string v19, "VIRecogUtils"

    const-string v20, "Start copyDatabase()"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    .line 41
    .local v17, "rootPath":Ljava/lang/String;
    const/4 v2, 0x0

    .line 43
    .local v2, "asset":Landroid/content/res/AssetManager;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v19

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v19

    .line 44
    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 50
    const/4 v15, 0x0

    .line 53
    .local v15, "nDataNum":I
    const/16 v18, 0x0

    .line 55
    .local v18, "strDBname":[Ljava/lang/String;
    :try_start_1
    const-string v19, "vidata"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 56
    move-object/from16 v0, v18

    array-length v15, v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 62
    const/4 v7, 0x0

    .line 63
    .local v7, "fileOut":Ljava/io/File;
    const/4 v14, 0x0

    .line 64
    .local v14, "is":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 65
    .local v9, "fo":Ljava/io/FileOutputStream;
    new-instance v6, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, "/vidata"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 66
    .local v6, "fileDir":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v19

    if-nez v19, :cond_0

    .line 67
    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    move-result v19

    if-nez v19, :cond_0

    .line 68
    const-string v19, "VIRecogUtils"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "Fail to make directory : "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const/16 v19, 0x0

    .line 124
    .end local v6    # "fileDir":Ljava/io/File;
    .end local v7    # "fileOut":Ljava/io/File;
    .end local v9    # "fo":Ljava/io/FileOutputStream;
    .end local v14    # "is":Ljava/io/InputStream;
    .end local v15    # "nDataNum":I
    .end local v18    # "strDBname":[Ljava/lang/String;
    :goto_0
    return v19

    .line 45
    :catch_0
    move-exception v4

    .line 46
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v4}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 47
    const/16 v19, 0x0

    goto :goto_0

    .line 57
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v15    # "nDataNum":I
    .restart local v18    # "strDBname":[Ljava/lang/String;
    :catch_1
    move-exception v5

    .line 58
    .local v5, "e1":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 59
    const/16 v19, 0x0

    goto :goto_0

    .line 74
    .end local v5    # "e1":Ljava/io/IOException;
    .restart local v6    # "fileDir":Ljava/io/File;
    .restart local v7    # "fileOut":Ljava/io/File;
    .restart local v9    # "fo":Ljava/io/FileOutputStream;
    .restart local v14    # "is":Ljava/io/InputStream;
    :cond_0
    const/4 v13, 0x0

    .local v13, "ii":I
    move-object v12, v9

    .local v12, "fo":Ljava/lang/Object;
    move-object v8, v7

    .end local v7    # "fileOut":Ljava/io/File;
    .end local v9    # "fo":Ljava/io/FileOutputStream;
    .local v8, "fileOut":Ljava/lang/Object;
    :goto_1
    if-lt v13, v15, :cond_1

    .line 124
    const/16 v19, 0x1

    goto :goto_0

    .line 75
    :cond_1
    :try_start_2
    new-instance v7, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, "/vidata/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    aget-object v20, v18, v13

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 76
    .restart local v7    # "fileOut":Ljava/io/File;
    :try_start_3
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    .end local v8    # "fileOut":Ljava/lang/Object;
    move-result v19

    if-eqz v19, :cond_2

    if-eqz p1, :cond_5

    .line 77
    :cond_2
    if-nez p1, :cond_3

    .line 78
    const-string v19, "VIRecogUtils"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "[copyDatabase] There is no DB file : "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_3
    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "vidata/"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v20, v18, v13

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v14

    .line 82
    invoke-virtual {v14}, Ljava/io/InputStream;->available()I

    move-result v19

    move/from16 v0, v19

    int-to-long v10, v0

    .line 83
    .local v10, "fileSize":J
    long-to-int v0, v10

    move/from16 v19, v0

    move/from16 v0, v19

    new-array v3, v0, [B

    .line 84
    .local v3, "bufferData":[B
    invoke-virtual {v14, v3}, Ljava/io/InputStream;->read([B)I

    move-result v16

    .line 85
    .local v16, "numberBuffers":I
    const/16 v19, -0x1

    move/from16 v0, v16

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 86
    const/4 v3, 0x0

    .line 87
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 88
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 90
    :cond_4
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 91
    const/4 v14, 0x0

    .line 93
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 94
    .restart local v9    # "fo":Ljava/io/FileOutputStream;
    :try_start_4
    invoke-virtual {v9, v3}, Ljava/io/FileOutputStream;->write([B)V

    .line 95
    .end local v12    # "fo":Ljava/lang/Object;
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    .line 97
    const/4 v3, 0x0

    .line 98
    const/4 v9, 0x0

    .line 102
    .end local v3    # "bufferData":[B
    .end local v9    # "fo":Ljava/io/FileOutputStream;
    .end local v10    # "fileSize":J
    .end local v16    # "numberBuffers":I
    :goto_2
    const/4 v7, 0x0

    .line 74
    add-int/lit8 v13, v13, 0x1

    move-object v12, v9

    .restart local v12    # "fo":Ljava/lang/Object;
    move-object v8, v7

    .restart local v8    # "fileOut":Ljava/lang/Object;
    goto/16 :goto_1

    .line 100
    .end local v8    # "fileOut":Ljava/lang/Object;
    :cond_5
    :try_start_5
    const-string v19, "VIRecogUtils"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "[copyDatabase] DB file exist : "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    move-object v9, v12

    .end local v12    # "fo":Ljava/lang/Object;
    .local v9, "fo":Ljava/lang/Object;
    goto :goto_2

    .line 104
    .end local v7    # "fileOut":Ljava/io/File;
    .end local v9    # "fo":Ljava/lang/Object;
    .restart local v8    # "fileOut":Ljava/lang/Object;
    .restart local v12    # "fo":Ljava/lang/Object;
    :catch_2
    move-exception v4

    move-object v9, v12

    .local v9, "fo":Ljava/io/FileOutputStream;
    move-object v7, v8

    .line 105
    .end local v8    # "fileOut":Ljava/lang/Object;
    .end local v12    # "fo":Ljava/lang/Object;
    .local v4, "e":Ljava/io/IOException;
    .restart local v7    # "fileOut":Ljava/io/File;
    :goto_3
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 106
    if-eqz v14, :cond_6

    .line 108
    :try_start_6
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 115
    :cond_6
    :goto_4
    if-eqz v9, :cond_7

    .line 117
    :try_start_7
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 122
    :cond_7
    :goto_5
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 110
    :catch_3
    move-exception v5

    .line 111
    .restart local v5    # "e1":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 118
    .end local v5    # "e1":Ljava/io/IOException;
    :catch_4
    move-exception v5

    .line 119
    .restart local v5    # "e1":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 104
    .end local v4    # "e":Ljava/io/IOException;
    .end local v5    # "e1":Ljava/io/IOException;
    .end local v9    # "fo":Ljava/io/FileOutputStream;
    .restart local v12    # "fo":Ljava/lang/Object;
    :catch_5
    move-exception v4

    move-object v9, v12

    .restart local v9    # "fo":Ljava/io/FileOutputStream;
    goto :goto_3

    .end local v12    # "fo":Ljava/lang/Object;
    .restart local v3    # "bufferData":[B
    .restart local v10    # "fileSize":J
    .restart local v16    # "numberBuffers":I
    :catch_6
    move-exception v4

    goto :goto_3
.end method
