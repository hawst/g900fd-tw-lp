.class Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;
.super Landroid/view/View;
.source "SpenTextPreView.java"


# static fields
.field protected static final TEXT_PREVIEW_WIDTH:I = 0xde

.field private static final VIENNA_CANVAS_WIDTH:I = 0x640


# instance fields
.field private isCheckUnderLine:Z

.field private mBold:I

.field private mColor:I

.field private mOnePoint:F

.field private final mPaint:Landroid/graphics/Paint;

.field private mPreviewOffset:F

.field private final mTextRect:Landroid/graphics/Rect;

.field private mTextSize:F

.field private mTextSkewValue:F

.field private mTypeFace:Landroid/graphics/Typeface;

.field private mWidth:F

.field private maxFontSize1:F

.field private maxFontSize2:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 47
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 20
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mColor:I

    .line 21
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    .line 22
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mBold:I

    .line 23
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSkewValue:F

    .line 24
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->isCheckUnderLine:Z

    .line 25
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTypeFace:Landroid/graphics/Typeface;

    .line 26
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    .line 27
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    .line 31
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mOnePoint:F

    .line 32
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize1:F

    .line 33
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize2:F

    .line 35
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPreviewOffset:F

    .line 36
    const/high16 v0, 0x44870000    # 1080.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mWidth:F

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 6
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "mCanvasWidth"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 54
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 20
    const/high16 v2, -0x1000000

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mColor:I

    .line 21
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    .line 22
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mBold:I

    .line 23
    const/high16 v2, -0x80000000

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSkewValue:F

    .line 24
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->isCheckUnderLine:Z

    .line 25
    sget-object v2, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTypeFace:Landroid/graphics/Typeface;

    .line 26
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    .line 27
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    .line 31
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mOnePoint:F

    .line 32
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize1:F

    .line 33
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize2:F

    .line 35
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPreviewOffset:F

    .line 36
    const/high16 v2, 0x44870000    # 1080.0f

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mWidth:F

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 57
    .local v1, "localDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v2, v1, Landroid/util/DisplayMetrics;->density:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mOnePoint:F

    .line 58
    int-to-double v2, p2

    const-wide v4, 0x4076800000000000L    # 360.0

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 60
    .local v0, "fontPointPixel":F
    const-string v2, "ABCD abcd"

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getMaxFontSize(Ljava/lang/String;F)F

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize1:F

    .line 61
    const-string v2, "ABC abc"

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getMaxFontSize(Ljava/lang/String;F)F

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize2:F

    .line 63
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v2, v3, :cond_0

    .line 64
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mWidth:F

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    iget v2, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mWidth:F

    goto :goto_0
.end method

.method private getMaxFontSize(Ljava/lang/String;F)F
    .locals 11
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "fontPointPixel"    # F

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 71
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 72
    .local v2, "mFontList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, ""

    .line 73
    .local v3, "maxFontName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 74
    .local v5, "maxLength":F
    const/4 v4, 0x0

    .line 76
    .local v4, "maxFontSize":F
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getFontSizeList()Ljava/util/ArrayList;

    move-result-object v0

    .line 78
    .local v0, "fontSizeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v1, v6, :cond_0

    .line 90
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v1, v6, :cond_2

    .line 103
    :goto_2
    return v4

    .line 79
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    const/high16 v7, 0x41f00000    # 30.0f

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 81
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 82
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v6, p1, v9, v7, v8}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 83
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 84
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    cmpl-float v6, v6, v5

    if-lez v6, :cond_1

    .line 85
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v5, v6

    .line 86
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "maxFontName":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 78
    .restart local v3    # "maxFontName":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 92
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, p2

    invoke-virtual {v7, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 94
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 95
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v6, p1, v9, v7, v8}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 96
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 97
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x43540000    # 212.0f

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mOnePoint:F

    mul-float/2addr v7, v8

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_3

    .line 98
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v6, v6

    mul-float v4, p2, v6

    .line 99
    goto/16 :goto_2

    .line 90
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1
.end method


# virtual methods
.method protected getFontSizeList()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v0, "fontList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v1, 0x8

    .local v1, "txtSize":I
    :goto_0
    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 113
    const/16 v1, 0x16

    :goto_1
    const/16 v2, 0x21

    if-lt v1, v2, :cond_1

    .line 116
    const/16 v1, 0x24

    :goto_2
    const/16 v2, 0x41

    if-lt v1, v2, :cond_2

    .line 120
    return-object v0

    .line 111
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 114
    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 117
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    add-int/lit8 v1, v1, 0x4

    goto :goto_2
.end method

.method public getPreviewTextColor()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mColor:I

    return v0
.end method

.method public getTextStyle()C
    .locals 3

    .prologue
    .line 266
    const/4 v0, 0x0

    .line 267
    .local v0, "nTextStyle":C
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mBold:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_0

    .line 268
    const/4 v1, 0x1

    int-to-char v0, v1

    .line 270
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSkewValue:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    .line 271
    or-int/lit8 v1, v0, 0x2

    int-to-char v0, v1

    .line 273
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->isCheckUnderLine:Z

    if-eqz v1, :cond_2

    .line 274
    or-int/lit8 v1, v0, 0x4

    int-to-char v0, v1

    .line 276
    :cond_2
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 134
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 136
    const-string v0, "ABCD abcd"

    .line 138
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 139
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 140
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 142
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTypeFace:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 143
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mBold:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFlags(I)V

    .line 144
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSkewValue:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 145
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->isCheckUnderLine:Z

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setUnderlineText(Z)V

    .line 146
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 149
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize1:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 150
    const-string v0, "ABC abc"

    .line 151
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 154
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize2:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_1

    .line 155
    const-string v0, "AB ab"

    .line 156
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 159
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    const/high16 v2, 0x43430000    # 195.0f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mWidth:F

    const/high16 v2, 0x44c80000    # 1600.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    .line 160
    const-string v0, "Aa"

    .line 161
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 162
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->isCheckUnderLine:Z

    if-eqz v1, :cond_3

    .line 163
    const/high16 v1, 0x41200000    # 10.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPreviewOffset:F

    .line 168
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 169
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    .line 170
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPreviewOffset:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    .line 169
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 171
    return-void

    .line 165
    :cond_3
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPreviewOffset:F

    goto :goto_0
.end method

.method public setPreviewBold(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 241
    if-eqz p1, :cond_0

    .line 242
    const/16 v0, 0x20

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mBold:I

    .line 246
    :goto_0
    return-void

    .line 244
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mBold:I

    goto :goto_0
.end method

.method public setPreviewTextColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    const/4 v0, 0x0

    .line 190
    if-nez p1, :cond_0

    .line 191
    invoke-static {v0, v0, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result p1

    .line 193
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mColor:I

    .line 194
    return-void
.end method

.method public setPreviewTextSize(F)V
    .locals 0
    .param p1, "textSize"    # F

    .prologue
    .line 215
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    .line 216
    return-void
.end method

.method public setPreviewTextSkewX(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 226
    if-eqz p1, :cond_0

    .line 227
    const v0, -0x41666666    # -0.3f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSkewValue:F

    .line 231
    :goto_0
    return-void

    .line 229
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSkewValue:F

    goto :goto_0
.end method

.method public setPreviewTypeface(Landroid/graphics/Typeface;)V
    .locals 0
    .param p1, "typeface"    # Landroid/graphics/Typeface;

    .prologue
    .line 256
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTypeFace:Landroid/graphics/Typeface;

    .line 257
    return-void
.end method

.method public setPreviewUnderLine(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 204
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->isCheckUnderLine:Z

    .line 205
    return-void
.end method
