.class public interface abstract Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$PresetListener;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PresetListener"
.end annotation


# virtual methods
.method public abstract onAdded(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
.end method

.method public abstract onChanged(I)V
.end method

.method public abstract onDeleted(I)V
.end method

.method public abstract onSelected(I)V
.end method
