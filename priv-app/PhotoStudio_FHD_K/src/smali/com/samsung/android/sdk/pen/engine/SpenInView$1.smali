.class Lcom/samsung/android/sdk/pen/engine/SpenInView$1;
.super Ljava/lang/Object;
.source "SpenInView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenInView;->construct()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    .line 508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "o"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .prologue
    .line 533
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$59(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    .line 534
    and-int/lit8 v0, p1, 0x1

    if-lez v0, :cond_2

    .line 535
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->undoToTag()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    .line 536
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearHistoryTag()V

    .line 543
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$61(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 544
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$61(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    .line 546
    :cond_1
    return-void

    .line 538
    :cond_2
    if-eqz p2, :cond_0

    .line 539
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setVisibility(Z)V

    .line 540
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    goto :goto_0
.end method

.method public onCompleted(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
    .locals 4
    .param p1, "frameType"    # I
    .param p2, "o"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 511
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$59(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    .line 512
    invoke-virtual {p2, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setVisibility(Z)V

    .line 513
    const-string v0, "STROKE_FRAME"

    const/4 v1, 0x2

    invoke-virtual {p2, v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setExtraDataInt(Ljava/lang/String;I)V

    .line 514
    if-nez p1, :cond_2

    .line 515
    invoke-virtual {p2, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    .line 516
    invoke-virtual {p2, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    .line 517
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$60(Lcom/samsung/android/sdk/pen/engine/SpenInView;I)V

    .line 523
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearHistoryTag()V

    .line 524
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    .line 526
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$61(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 527
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$61(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCompleted(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    .line 529
    :cond_1
    return-void

    .line 518
    :cond_2
    if-ne p1, v2, :cond_0

    .line 519
    invoke-virtual {p2, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    .line 520
    invoke-virtual {p2, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    .line 521
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$60(Lcom/samsung/android/sdk/pen/engine/SpenInView;I)V

    goto :goto_0
.end method
