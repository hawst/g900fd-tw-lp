.class Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;
.super Ljava/lang/Object;
.source "SpenInView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenInView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnPageEffectListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 5509
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;)V
    .locals 0

    .prologue
    .line 5509
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5527
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$42(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;Z)V

    .line 5528
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$45(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5529
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$45(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 5531
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$46(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 5532
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$46(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;->onFinish()V

    .line 5534
    :cond_1
    return-void
.end method

.method public onUpdate()V
    .locals 3

    .prologue
    .line 5522
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$42(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;Z)V

    .line 5523
    return-void
.end method

.method public onUpdateCanvasLayer(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 5512
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V
    invoke-static {v0, p1, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    .line 5513
    return-void
.end method

.method public onUpdateCanvasLayer2(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 5517
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer2(Landroid/graphics/Canvas;)V
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$44(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/Canvas;)V

    .line 5518
    return-void
.end method
