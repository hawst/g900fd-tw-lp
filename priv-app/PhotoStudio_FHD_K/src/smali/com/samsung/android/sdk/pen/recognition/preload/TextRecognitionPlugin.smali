.class public Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;
.super Ljava/lang/Object;
.source "TextRecognitionPlugin.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$HandleInfo;,
        Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "recognition-TextRecognitionPlugin"


# instance fields
.field private mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

.field private final mRecogHandler:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    .line 46
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .line 52
    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogHandler:Landroid/os/Handler;

    .line 67
    const-string v0, "recognition-TextRecognitionPlugin"

    const-string v1, "creating text recognition plugin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->getCurrentLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 260
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 244
    return-void
.end method

.method public getSupportedLanguage()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-nez v0, :cond_0

    .line 291
    const/4 v0, 0x0

    .line 294
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->getSupportedLanguage()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public initRecognizer(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 75
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-nez v1, :cond_1

    .line 76
    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    .line 78
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-nez v1, :cond_0

    .line 79
    const-string v1, "recognition-TextRecognitionPlugin"

    const-string v2, "Fail to create TextRecognition instance"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :goto_0
    return v0

    .line 83
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    const-string v2, "eng"

    invoke-virtual {v1, p1, v2}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->init(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 84
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    goto :goto_0

    .line 88
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 170
    const-string v1, "Text Recognition"

    const-string v2, "Load libSPenVITextAll.so."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    :try_start_0
    const-string v1, "SPenVITextAll"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->initRecognizer(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 194
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Fail to load Text recognition engine"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 175
    :catch_0
    move-exception v0

    .line 176
    .local v0, "error":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 177
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "libSPenVITextAll.so is not loaded."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 196
    .end local v0    # "error":Ljava/lang/Exception;
    :cond_0
    return-void
.end method

.method public declared-synchronized onUnload()V
    .locals 1

    .prologue
    .line 232
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->dispose()V

    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    :cond_0
    monitor-exit p0

    return-void

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public request(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 269
    .local p1, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    if-nez v1, :cond_0

    .line 270
    const-string v1, "recognition-TextRecognitionPlugin"

    const-string v2, "The result listener isn\'t set yet!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 275
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-eqz v1, :cond_1

    .line 276
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 277
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 282
    .end local v0    # "thread":Ljava/lang/Thread;
    :goto_0
    return-void

    .line 279
    :cond_1
    const-string v1, "recognition-TextRecognitionPlugin"

    const-string v2, "The recognition engine is null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->setLanguage(Ljava/lang/String;)V

    .line 224
    :cond_0
    return-void
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 252
    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .line 158
    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 303
    const/4 v0, 0x1

    return v0
.end method
