.class public Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;
.super Landroid/widget/LinearLayout;
.source "SpenSettingRemoverLayout.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;
    }
.end annotation


# static fields
.field protected static final ERASER_PROGRESS_MAX:I = 0x9

.field protected static final ERASER_SIZE_MAX:I = 0xa

.field protected static final EXIT_BUTTON_HEIGHT:I = 0x24

.field protected static EXIT_BUTTON_WIDTH:I = 0x0

.field private static final IB_REMOVER_EXIT_ID:I = 0xb82f91

.field private static final IB_REMOVER_SIZE_MINUS_ID:I = 0xb82f93

.field private static final IB_REMOVER_SIZE_PLUS_ID:I = 0xb82f92

.field private static final LL_VERSION_CODE:I = 0x15

.field private static final REP_DELAY:I = 0x14

.field private static final TAG:Ljava/lang/String; = "settingui-settingRemover"

.field protected static final TITLE_LAYOUT_HEIGHT:I = 0x29

.field public static final VIEW_MODE_NORMAL:I = 0x0

.field public static final VIEW_MODE_SIZE:I = 0x2

.field public static final VIEW_MODE_TITLE:I = 0x3

.field public static final VIEW_MODE_TYPE:I = 0x1

.field private static arabicChars:[C = null

.field private static final bodyLeftPath:Ljava/lang/String; = "snote_popup_bg_left"

.field private static final bodyRightPath:Ljava/lang/String; = "snote_popup_bg_right"

.field private static final btnFocusPath:Ljava/lang/String; = "snote_popup_btn_focus"

.field private static final btnNoramlPath:Ljava/lang/String; = "snote_popup_btn_normal"

.field private static final btnPressPath:Ljava/lang/String; = "snote_popup_btn_press"

.field private static final cutterPopupDrawPress:Ljava/lang/String; = "snote_eraser_popup_draw_press"

.field private static final cutterPopupDrawUnselect:Ljava/lang/String; = "snote_eraser_popup_draw"

.field private static final cutterPopupTextPress:Ljava/lang/String; = "snote_eraser_popup_text_press"

.field private static final cutterPopupTextUnselect:Ljava/lang/String; = "snote_eraser_popup_text"

.field private static final exitPath:Ljava/lang/String; = "snote_popup_close"

.field private static final exitPressPath:Ljava/lang/String; = "snote_popup_close_press"

.field private static final exitfocusPath:Ljava/lang/String; = "snote_popup_close_focus"

.field private static final handelFocusPath:Ljava/lang/String; = "progress_handle_focus"

.field private static final handelPath:Ljava/lang/String; = "progress_handle_normal"

.field private static final handelPressPath:Ljava/lang/String; = "progress_handle_press"

.field private static final lineDivider:Ljava/lang/String; = "snote_popup_line"

.field protected static final mDefaultPath:Ljava/lang/String; = ""

.field private static final mSdkVersion:I

.field private static final minusBgDimPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_dim"

.field private static final minusBgFocusPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_focus"

.field private static final minusBgPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_normal"

.field private static final minusBgPressPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_press"

.field private static final plusBgDimPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_dim"

.field private static final plusBgFocusPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_focus"

.field private static final plusBgPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_normal"

.field private static final plusBgPressPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_press"

.field private static final progressBgPath:Ljava/lang/String; = "progress_bg"

.field private static final progressShadowPath:Ljava/lang/String; = "progress_shadow"

.field private static final titleCenterPath:Ljava/lang/String; = "snote_popup_title_center"

.field private static final titleLeftPath:Ljava/lang/String; = "snote_popup_title_left"

.field private static final titleRightIndicatorPath:Ljava/lang/String; = "snote_popup_title_bended"

.field private static final titleRightPath:Ljava/lang/String; = "snote_popup_title_right"


# instance fields
.field private EXIT_BUTTON_RIGHT_MARGIN:I

.field private EXIT_BUTTON_TOP_MARGIN:I

.field private final handlerRotate:Landroid/os/Handler;

.field private isFirstClickChexBox:Z

.field localDisplayMetrics:Landroid/util/DisplayMetrics;

.field private localLinearLayout:Landroid/widget/LinearLayout;

.field protected mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

.field private mAutoDecrement:Z

.field private mAutoIncrement:Z

.field protected mBodyLayout:Landroid/view/View;

.field protected mCanvasLayout:Landroid/widget/RelativeLayout;

.field protected mCanvasSize:I

.field protected mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

.field private final mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field protected mClearAllButton:Landroid/view/View;

.field private mClearAllListener:Landroid/view/View$OnClickListener;

.field protected mCurrentCutterType:I

.field protected mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field private mCutterKeyListener:Landroid/view/View$OnKeyListener;

.field protected mCutterListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

.field protected mCutterMinusButton:Landroid/widget/ImageButton;

.field protected mCutterPlusButton:Landroid/widget/ImageButton;

.field protected mCutterSizeButton:Landroid/view/ViewGroup;

.field private mCutterSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field protected mCutterSizeSeekBar:Landroid/widget/SeekBar;

.field protected mCutterType01:Landroid/widget/ImageButton;

.field protected mCutterType02:Landroid/widget/ImageButton;

.field protected mCutterTypeChekBox:Landroid/view/View;

.field protected mCutterTypeLayout:Landroid/view/ViewGroup;

.field private mCutterTypeListner:Landroid/view/View$OnClickListener;

.field protected mCutterTypeView:[Landroid/view/View;

.field protected mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field protected mEraserSizeTextView:Landroid/widget/TextView;

.field protected mExitButton:Landroid/view/View;

.field private mExitButtonListener:Landroid/view/View$OnClickListener;

.field protected mFirstLongPress:Z

.field protected mGestureDetector:Landroid/view/GestureDetector;

.field private mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

.field private mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field protected mIndicator:Landroid/widget/ImageView;

.field private mIsFirstShown:Z

.field protected mIsRotated:Z

.field private mIsShowing:Z

.field mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field protected mLeftMargin:I

.field private mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

.field private mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

.field private final mMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

.field private mMinusButtonListener:Landroid/view/View$OnClickListener;

.field protected mMovableRect:Landroid/graphics/Rect;

.field protected mMoveSettingLayout:Z

.field protected mNeedCalculateMargin:Z

.field private mNeedRecalculateRotate:Z

.field private mNeedRotateWhenSetPosition:Z

.field protected mOldLocation:[I

.field protected mOldMovableRect:Landroid/graphics/Rect;

.field private final mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

.field private mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

.field private mOnTouchListener:Landroid/view/View$OnTouchListener;

.field private final mPlusButtonKeyListener:Landroid/view/View$OnKeyListener;

.field private mPlusButtonListener:Landroid/view/View$OnClickListener;

.field protected mRemoverContext:Landroid/content/Context;

.field private mRightIndicator:Landroid/widget/ImageView;

.field protected mScale:F

.field protected mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field protected mSettingSizeLayout:Landroid/view/ViewGroup;

.field private mStandardCanvasSize:I

.field protected mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field protected mTitleLayout:Landroid/view/View;

.field private mTitleView:Landroid/widget/TextView;

.field protected mTopMargin:I

.field private mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

.field private mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

.field protected mViewMode:I

.field protected mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

.field protected mXDelta:I

.field protected mYDelta:I

.field private repeatUpdateHandler:Landroid/os/Handler;

.field protected requestLayoutDisable:Z

.field private final runnableRotate:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 137
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    .line 166
    const/16 v0, 0x26

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_WIDTH:I

    .line 2470
    const/16 v0, 0xa

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->arabicChars:[C

    return-void

    :array_0
    .array-data 2
        0x660s
        0x661s
        0x662s
        0x663s
        0x664s
        0x665s
        0x666s
        0x667s
        0x668s
        0x669s
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p3, "relativeLayout"    # Landroid/widget/RelativeLayout;

    .prologue
    const/16 v5, 0x15

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 819
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 73
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    .line 74
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsFirstShown:Z

    .line 75
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsShowing:Z

    .line 101
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 108
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    .line 129
    const/16 v0, 0x438

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    .line 130
    const/16 v0, 0x5a0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    .line 132
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    .line 133
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 134
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 135
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 141
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    .line 143
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedCalculateMargin:Z

    .line 144
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mFirstLongPress:Z

    .line 145
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->isFirstClickChexBox:Z

    .line 151
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsRotated:Z

    .line 155
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 157
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .line 158
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    .line 159
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    .line 172
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 173
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    .line 175
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    .line 310
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 390
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 399
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllListener:Landroid/view/View$OnClickListener;

    .line 411
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 440
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 514
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 523
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 539
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 554
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->repeatUpdateHandler:Landroid/os/Handler;

    .line 555
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z

    .line 556
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z

    .line 586
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 597
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    .line 608
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 627
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 638
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    .line 650
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 668
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 741
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$16;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeListner:Landroid/view/View$OnClickListener;

    .line 748
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$17;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$17;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    .line 759
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$18;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$18;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterKeyListener:Landroid/view/View$OnKeyListener;

    .line 1358
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    .line 1359
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->handlerRotate:Landroid/os/Handler;

    .line 1360
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$19;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$19;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->runnableRotate:Ljava/lang/Runnable;

    .line 1994
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 820
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 821
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, p1, p2, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 822
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 823
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 824
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    .line 825
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 826
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 827
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 828
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v0, v0, v4

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 829
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 830
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    .line 832
    :cond_0
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    if-lt v0, v5, :cond_1

    .line 833
    const/16 v0, 0x24

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_WIDTH:I

    .line 835
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    .line 836
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->initView()V

    .line 837
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setListener()V

    .line 838
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    .line 839
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    .line 840
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    .line 841
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p3, "relativeLayout"    # Landroid/widget/RelativeLayout;
    .param p4, "ratio"    # F

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const v1, 0x3f59999a    # 0.85f

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 880
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 73
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    .line 74
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsFirstShown:Z

    .line 75
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsShowing:Z

    .line 101
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 108
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    .line 129
    const/16 v0, 0x438

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    .line 130
    const/16 v0, 0x5a0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    .line 132
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    .line 133
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 134
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 135
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 141
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    .line 143
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedCalculateMargin:Z

    .line 144
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mFirstLongPress:Z

    .line 145
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->isFirstClickChexBox:Z

    .line 151
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsRotated:Z

    .line 155
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 157
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .line 158
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    .line 159
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    .line 172
    const/16 v0, 0x15

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 173
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    .line 175
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    .line 310
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 390
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 399
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllListener:Landroid/view/View$OnClickListener;

    .line 411
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 440
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 514
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 523
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 539
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 554
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->repeatUpdateHandler:Landroid/os/Handler;

    .line 555
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z

    .line 556
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z

    .line 586
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 597
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    .line 608
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 627
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 638
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    .line 650
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 668
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 741
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$16;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeListner:Landroid/view/View$OnClickListener;

    .line 748
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$17;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$17;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    .line 759
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$18;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$18;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterKeyListener:Landroid/view/View$OnKeyListener;

    .line 1358
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    .line 1359
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->handlerRotate:Landroid/os/Handler;

    .line 1360
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$19;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$19;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->runnableRotate:Ljava/lang/Runnable;

    .line 1994
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 881
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 882
    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    .line 883
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    const/high16 v2, 0x40000000    # 2.0f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    :goto_0
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    .line 884
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    :goto_1
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    .line 885
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 886
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 887
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 888
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    .line 889
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 890
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 891
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 892
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 893
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 894
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    .line 896
    :cond_0
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 897
    const/16 v0, 0x24

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_WIDTH:I

    .line 899
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    .line 900
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->initView()V

    .line 901
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setListener()V

    .line 902
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    .line 903
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    .line 904
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    .line 905
    return-void

    .line 883
    :cond_2
    const/high16 v0, 0x40000000    # 2.0f

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 884
    goto :goto_1
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 2410
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V
    .locals 0

    .prologue
    .line 1632
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setEnableSizeSeekbar(Z)V

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2424
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->convertToArabicNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1856
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->cutterTypeSetting(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V
    .locals 0

    .prologue
    .line 1358
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    return-void
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 1358
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    return v0
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V
    .locals 0

    .prologue
    .line 1901
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->rotatePosition()V

    return-void
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    return v0
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V
    .locals 0

    .prologue
    .line 1960
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->checkPosition()V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsShowing:Z

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V
    .locals 0

    .prologue
    .line 555
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->repeatUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 555
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V
    .locals 0

    .prologue
    .line 556
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 556
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z

    return v0
.end method

.method private addCutterLayout()V
    .locals 3

    .prologue
    .line 1470
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 1471
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    .line 1472
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v2, "snote_eraser_popup_draw"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1473
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$21;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$21;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1490
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localLinearLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1492
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 1494
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    .line 1495
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v2, "snote_eraser_popup_text"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1496
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$22;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$22;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1513
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localLinearLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1515
    :cond_1
    return-void
.end method

.method private bodyBg()Landroid/view/ViewGroup;
    .locals 14

    .prologue
    const/16 v13, 0xa

    const/4 v12, 0x1

    const/high16 v11, 0x43080000    # 136.0f

    const/high16 v10, 0x42e40000    # 114.0f

    const/high16 v9, 0x40a00000    # 5.0f

    .line 1094
    new-instance v4, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v4, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1095
    .local v4, "layout":Landroid/widget/RelativeLayout;
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    .line 1096
    const/4 v7, -0x2

    .line 1095
    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1098
    .local v5, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1100
    new-instance v0, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1101
    .local v0, "bodyLeft":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1102
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1103
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1104
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1103
    sub-int/2addr v7, v8

    add-int/lit8 v7, v7, 0x14

    .line 1101
    invoke-direct {v1, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1105
    .local v1, "bodyLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v12, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1106
    const/16 v6, 0x9

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1107
    invoke-virtual {v1, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1108
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1109
    new-instance v2, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v2, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1110
    .local v2, "bodyRight":Landroid/widget/ImageView;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1111
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x43640000    # 228.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    sub-int/2addr v6, v7

    .line 1112
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1113
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1112
    sub-int/2addr v7, v8

    add-int/lit8 v7, v7, 0x14

    .line 1110
    invoke-direct {v3, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1114
    .local v3, "bodyRightParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v12, v3, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1115
    const/16 v6, 0xb

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1116
    invoke-virtual {v3, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1117
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1119
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v7, "snote_popup_bg_left"

    invoke-virtual {v6, v0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1120
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v7, "snote_popup_bg_right"

    invoke-virtual {v6, v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1121
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1122
    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1123
    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1124
    return-object v4
.end method

.method private bodyLayout()Landroid/view/ViewGroup;
    .locals 11

    .prologue
    const/4 v10, -0x1

    const/high16 v8, 0x40a00000    # 5.0f

    const/4 v9, 0x0

    .line 1041
    new-instance v4, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v4, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1042
    .local v4, "layout":Landroid/widget/RelativeLayout;
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1043
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x43640000    # 228.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    const/4 v7, -0x2

    .line 1042
    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1044
    .local v5, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v6, 0x9

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1045
    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1046
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->bodyBg()Landroid/view/ViewGroup;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1048
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->cutterSettingClearAllButton()Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    .line 1049
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->cutterTypeLayout()Landroid/view/ViewGroup;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    .line 1050
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->cutterSettingSize()Landroid/view/ViewGroup;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    .line 1053
    new-instance v3, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v3, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1054
    .local v3, "imgDivider":Landroid/widget/ImageView;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1055
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1054
    invoke-direct {v2, v10, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1056
    .local v2, "dividerParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v6, 0x1

    iput-boolean v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1057
    const/4 v6, 0x6

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1058
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1059
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1060
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x42480000    # 50.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1062
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1063
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v7, "snote_popup_line"

    invoke-virtual {v6, v3, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1064
    invoke-virtual {v4, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1067
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->eraseByStroke()Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    .line 1068
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1069
    .local v0, "checkBoxLayout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1070
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x421c0000    # 39.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1069
    invoke-direct {v1, v10, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1071
    .local v1, "checkBoxParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x424c0000    # 51.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1072
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41200000    # 10.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1071
    invoke-virtual {v1, v9, v6, v9, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1073
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1074
    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 1075
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1077
    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setEnableSizeSeekbar(Z)V

    .line 1078
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1079
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1080
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1081
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1082
    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1083
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1084
    return-object v4
.end method

.method private checkPosition()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1961
    const/4 v4, 0x2

    new-array v0, v4, [I

    .line 1962
    .local v0, "location":[I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43640000    # 228.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x13

    add-int/lit8 v2, v4, 0x2

    .line 1963
    .local v2, "minWidth":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x432c0000    # 172.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1965
    .local v1, "minHeight":I
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLocationOnScreen([I)V

    .line 1967
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1969
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    aget v4, v0, v6

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-ge v4, v5, :cond_0

    .line 1970
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1972
    :cond_0
    aget v4, v0, v7

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    if-ge v4, v5, :cond_1

    .line 1973
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1976
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    aget v5, v0, v6

    sub-int/2addr v4, v5

    if-ge v4, v2, :cond_2

    .line 1977
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v4, v2

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1979
    iget v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-gez v4, :cond_2

    .line 1980
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1983
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    aget v5, v0, v7

    sub-int/2addr v4, v5

    if-ge v4, v1, :cond_3

    .line 1984
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, v1

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1986
    iget v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-gez v4, :cond_3

    .line 1987
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1991
    :cond_3
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1992
    return-void
.end method

.method private convertToArabicNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 2425
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2426
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 2433
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 2427
    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2428
    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->arabicChars:[C

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/lit8 v3, v3, -0x30

    aget-char v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2426
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2430
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private cutterSettingClearAllButton()Landroid/view/View;
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/high16 v12, -0x1000000

    const/high16 v4, 0x41500000    # 13.0f

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1778
    new-instance v1, Landroid/widget/Button;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1779
    .local v1, "clearAllButton":Landroid/widget/Button;
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 1780
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42040000    # 33.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1779
    invoke-direct {v8, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1781
    .local v8, "clearAllButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 1782
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1783
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1781
    invoke-virtual {v8, v0, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1784
    invoke-virtual {v1, v8}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1785
    invoke-virtual {v1, v11}, Landroid/widget/Button;->setFocusable(Z)V

    .line 1786
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_clear_all"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1787
    new-array v6, v13, [[I

    .line 1788
    .local v6, "arrayOfStates":[[I
    new-array v7, v11, [I

    .line 1789
    .local v7, "arrayOfStates1":[I
    const v0, 0x10100a7

    aput v0, v7, v10

    .line 1790
    aput-object v7, v6, v10

    .line 1791
    new-array v0, v10, [I

    aput-object v0, v6, v11

    .line 1792
    new-array v9, v13, [I

    .line 1793
    .local v9, "textColor":[I
    aput v12, v9, v10

    .line 1794
    aput v12, v9, v11

    .line 1795
    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v6, v9}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1796
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41600000    # 14.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v10, v0}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1798
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v2, 0x15

    if-ge v0, v2, :cond_0

    .line 1799
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 1800
    const-string v2, "snote_popup_btn_normal"

    const-string v3, "snote_popup_btn_press"

    const-string v4, "snote_popup_btn_focus"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1808
    :goto_0
    return-object v1

    .line 1802
    :cond_0
    new-instance v0, Landroid/graphics/drawable/RippleDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v3, 0x40

    invoke-static {v3, v10, v10, v10}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 1803
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1802
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1804
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v2, "snote_popup_btn_normal"

    const-string v3, "snote_popup_btn_press"

    .line 1805
    const-string v4, "snote_popup_btn_focus"

    const/16 v5, 0x3d

    invoke-static {v5, v10, v10, v10}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    .line 1804
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private cutterSettingSize()Landroid/view/ViewGroup;
    .locals 8

    .prologue
    const/high16 v6, 0x40a00000    # 5.0f

    const/4 v7, 0x0

    .line 1524
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1527
    .local v0, "cutterSettingSizeLayout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1528
    const/4 v3, -0x1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42400000    # 48.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1527
    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1529
    .local v1, "cutterSettingSizeLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1530
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41000000    # 8.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1529
    invoke-virtual {v1, v3, v7, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1531
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1533
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    .line 1534
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1535
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    const/16 v4, 0x56

    const/16 v5, 0x57

    const/16 v6, 0x5b

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1536
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41400000    # 12.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v7, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1537
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    const/16 v4, 0x33

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 1539
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1540
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41600000    # 14.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1539
    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1541
    .local v2, "eraserSizeTextParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1543
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1544
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1545
    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 1546
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->cutterSettingSizeDisplay()Landroid/view/ViewGroup;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeButton:Landroid/view/ViewGroup;

    .line 1547
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeButton:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1548
    return-object v0
.end method

.method private cutterSettingSizeDisplay()Landroid/view/ViewGroup;
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1704
    new-instance v9, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v9, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1705
    .local v9, "cutterSettingSizeDisplayLayout":Landroid/widget/RelativeLayout;
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 1706
    const/4 v1, -0x1

    .line 1705
    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1707
    .local v10, "mParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1708
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 1709
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    .line 1710
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1711
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41c00000    # 24.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c00000    # 24.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1710
    invoke-direct {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1712
    .local v8, "cutterPlusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v8, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1713
    const/16 v0, 0xb

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1714
    const/16 v0, 0x8

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1716
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1717
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1718
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 1719
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_progress_btn_plus_normal"

    const-string v2, "snote_popup_progress_btn_plus_press"

    .line 1720
    const-string v3, "snote_popup_progress_btn_plus_focus"

    const-string v4, "snote_popup_progress_btn_plus_dim"

    const/16 v5, 0x18

    const/16 v6, 0x18

    .line 1719
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1731
    :goto_0
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    .line 1732
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1733
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41c00000    # 24.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c00000    # 24.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1732
    invoke-direct {v7, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1734
    .local v7, "cutterMinusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1735
    const/16 v0, 0x9

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1736
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1738
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1739
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1740
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_2

    .line 1741
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_progress_btn_minus_normal"

    const-string v2, "snote_popup_progress_btn_minus_press"

    .line 1742
    const-string v3, "snote_popup_progress_btn_minus_focus"

    const-string v4, "snote_popup_progress_btn_minus_dim"

    const/16 v5, 0x18

    const/16 v6, 0x18

    .line 1741
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1754
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->cutterSettingSizeSeekBar()Landroid/widget/SeekBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    .line 1755
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 1756
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1757
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1758
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1760
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    const v1, 0xb82f93

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setId(I)V

    .line 1761
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    const v1, 0xb82f92

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setId(I)V

    .line 1763
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    const v1, 0xb82f91

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusUpId(I)V

    .line 1764
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    const v1, 0xb82f91

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusUpId(I)V

    .line 1765
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    const v1, 0xb82f91

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setNextFocusUpId(I)V

    .line 1766
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    const v1, 0xb82f93

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setNextFocusLeftId(I)V

    .line 1767
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    const v1, 0xb82f92

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setNextFocusRightId(I)V

    .line 1768
    return-object v9

    .line 1721
    .end local v7    # "cutterMinusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 1722
    new-instance v11, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v0, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v11, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1723
    .local v11, "shapeDrawable":Landroid/graphics/drawable/ShapeDrawable;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    new-instance v1, Landroid/graphics/drawable/RippleDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v3, 0x2d

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 1724
    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v11}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1723
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1725
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_progress_btn_plus_normal"

    const-string v2, "snote_popup_progress_btn_plus_normal"

    const-string v3, "snote_popup_progress_btn_plus_focus"

    .line 1726
    const-string v4, "snote_popup_progress_btn_plus_dim"

    const/16 v5, 0x18

    const/16 v6, 0x18

    .line 1725
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1728
    .end local v11    # "shapeDrawable":Landroid/graphics/drawable/ShapeDrawable;
    :cond_1
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_progress_btn_plus_normal"

    const-string v2, "snote_popup_progress_btn_plus_press"

    .line 1729
    const-string v3, "snote_popup_progress_btn_plus_focus"

    const-string v4, "snote_popup_progress_btn_plus_dim"

    const/16 v5, 0x18

    const/16 v6, 0x18

    .line 1728
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1743
    .restart local v7    # "cutterMinusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 1744
    new-instance v11, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v0, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v11, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1745
    .restart local v11    # "shapeDrawable":Landroid/graphics/drawable/ShapeDrawable;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    new-instance v1, Landroid/graphics/drawable/RippleDrawable;

    .line 1746
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v3, 0x2d

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v11}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1745
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1747
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_progress_btn_minus_normal"

    const-string v2, "snote_popup_progress_btn_minus_normal"

    .line 1748
    const-string v3, "snote_popup_progress_btn_minus_focus"

    const-string v4, "snote_popup_progress_btn_minus_dim"

    const/16 v5, 0x18

    const/16 v6, 0x18

    .line 1747
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 1750
    .end local v11    # "shapeDrawable":Landroid/graphics/drawable/ShapeDrawable;
    :cond_3
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_progress_btn_minus_normal"

    const-string v2, "snote_popup_progress_btn_minus_press"

    .line 1751
    const-string v3, "snote_popup_progress_btn_minus_focus"

    const-string v4, "snote_popup_progress_btn_minus_dim"

    const/16 v5, 0x18

    const/16 v6, 0x18

    .line 1750
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1
.end method

.method private cutterSettingSizeSeekBar()Landroid/widget/SeekBar;
    .locals 20

    .prologue
    .line 1558
    new-instance v16, Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 1562
    .local v16, "seekBar":Landroid/widget/SeekBar;
    new-instance v17, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1563
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x432a0000    # 170.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41b00000    # 22.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1562
    move-object/from16 v0, v17

    invoke-direct {v0, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1565
    .local v17, "seekBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v4, 0x1

    move-object/from16 v0, v17

    iput-boolean v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1566
    const/16 v4, 0xe

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1567
    const/16 v4, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1568
    invoke-virtual/range {v16 .. v17}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1569
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x3fc00000    # 1.5f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    const/4 v5, 0x0

    .line 1570
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x3fc00000    # 1.5f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    const/4 v7, 0x0

    .line 1569
    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/widget/SeekBar;->setPadding(IIII)V

    .line 1572
    const/16 v4, 0x9

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1574
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v2, "progress_handle_normal"

    const-string v3, "progress_handle_press"

    const-string v4, "progress_handle_focus"

    const/16 v5, 0x16

    const/16 v6, 0x16

    const/4 v7, 0x1

    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 1575
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setThumbOffset(I)V

    .line 1577
    new-instance v11, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v11}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 1578
    .local v11, "localGradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    const/4 v4, 0x0

    invoke-virtual {v11, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1579
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40900000    # 4.5f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v11, v4}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 1581
    new-instance v10, Landroid/graphics/drawable/ClipDrawable;

    const/4 v4, 0x3

    const/4 v5, 0x1

    invoke-direct {v10, v11, v4, v5}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 1582
    .local v10, "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v5, "progress_bg"

    const/16 v6, 0xbe

    const/16 v7, 0x9

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1583
    .local v2, "bgDrawable":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v5, "progress_shadow"

    const/16 v6, 0xbe

    const/16 v7, 0x9

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    .line 1584
    .local v18, "shadowDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v1, Landroid/graphics/drawable/InsetDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 1585
    .local v1, "bgInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v3, Landroid/graphics/drawable/InsetDrawable;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v4, v18

    invoke-direct/range {v3 .. v8}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 1587
    .local v3, "shadowInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v12, Landroid/graphics/drawable/LayerDrawable;

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    const/4 v5, 0x2

    .line 1588
    aput-object v3, v4, v5

    .line 1587
    invoke-direct {v12, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 1589
    .local v12, "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1591
    :try_start_0
    const-class v4, Landroid/widget/ProgressBar;

    const-string v5, "mMinHeight"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v15

    .line 1592
    .local v15, "minHeight":Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    invoke-virtual {v15, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 1593
    const-class v4, Landroid/widget/ProgressBar;

    const-string v5, "mMaxHeight"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v13

    .line 1594
    .local v13, "maxHeight":Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    invoke-virtual {v13, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1597
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41100000    # 9.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v4}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 1598
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41100000    # 9.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    move-object/from16 v0, v16

    invoke-virtual {v13, v0, v4}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1608
    .end local v13    # "maxHeight":Ljava/lang/reflect/Field;
    .end local v15    # "minHeight":Ljava/lang/reflect/Field;
    :goto_0
    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_0

    .line 1610
    :try_start_2
    const-class v4, Landroid/widget/AbsSeekBar;

    const-string v5, "setSplitTrack"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v14

    .line 1613
    .local v14, "method":Ljava/lang/reflect/Method;
    const/4 v4, 0x1

    :try_start_3
    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_4

    .line 1626
    .end local v14    # "method":Ljava/lang/reflect/Method;
    :goto_1
    new-instance v4, Landroid/graphics/drawable/RippleDrawable;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v6, 0x40

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-static {v6, v7, v8, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    const/4 v6, 0x0

    .line 1627
    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1626
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1629
    :cond_0
    return-object v16

    .line 1599
    .restart local v13    # "maxHeight":Ljava/lang/reflect/Field;
    .restart local v15    # "minHeight":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v9

    .line 1600
    .local v9, "e":Ljava/lang/IllegalAccessException;
    :try_start_4
    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/NoSuchFieldException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 1604
    .end local v9    # "e":Ljava/lang/IllegalAccessException;
    .end local v13    # "maxHeight":Ljava/lang/reflect/Field;
    .end local v15    # "minHeight":Ljava/lang/reflect/Field;
    :catch_1
    move-exception v9

    .line 1605
    .local v9, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v9}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 1601
    .end local v9    # "e":Ljava/lang/NoSuchFieldException;
    .restart local v13    # "maxHeight":Ljava/lang/reflect/Field;
    .restart local v15    # "minHeight":Ljava/lang/reflect/Field;
    :catch_2
    move-exception v9

    .line 1602
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :try_start_5
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/NoSuchFieldException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 1614
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    .end local v13    # "maxHeight":Ljava/lang/reflect/Field;
    .end local v15    # "minHeight":Ljava/lang/reflect/Field;
    .restart local v14    # "method":Ljava/lang/reflect/Method;
    :catch_3
    move-exception v9

    .line 1615
    .local v9, "e":Ljava/lang/IllegalAccessException;
    :try_start_6
    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_1

    .line 1622
    .end local v9    # "e":Ljava/lang/IllegalAccessException;
    .end local v14    # "method":Ljava/lang/reflect/Method;
    :catch_4
    move-exception v9

    .line 1623
    .local v9, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v9}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 1616
    .end local v9    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v14    # "method":Ljava/lang/reflect/Method;
    :catch_5
    move-exception v9

    .line 1617
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :try_start_7
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 1618
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :catch_6
    move-exception v9

    .line 1619
    .local v9, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v9}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_7
    .catch Ljava/lang/NoSuchMethodException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_1
.end method

.method private cutterTypeLayout()Landroid/view/ViewGroup;
    .locals 3

    .prologue
    .line 1459
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localLinearLayout:Landroid/widget/LinearLayout;

    .line 1460
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localLinearLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1462
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1463
    const/4 v1, -0x1

    const/4 v2, -0x2

    .line 1462
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1464
    .local v0, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1466
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localLinearLayout:Landroid/widget/LinearLayout;

    return-object v1
.end method

.method private cutterTypeSetting(Landroid/view/View;)V
    .locals 5
    .param p1, "selectedView"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1857
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 1898
    invoke-virtual {p1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 1899
    return-void

    .line 1858
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    .line 1859
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2, v4}, Landroid/view/View;->setSelected(Z)V

    .line 1861
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1863
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    .line 1864
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    .line 1866
    if-nez v0, :cond_3

    .line 1867
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    .line 1872
    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 1857
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1868
    :cond_3
    if-ne v0, v3, :cond_1

    .line 1869
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    goto :goto_1

    .line 1874
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_2

    .line 1875
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    .line 1876
    .local v1, "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    if-eqz v1, :cond_2

    .line 1877
    iput v4, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 1878
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    goto :goto_2

    .line 1884
    .end local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_2

    .line 1885
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    .line 1886
    .restart local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    if-eqz v1, :cond_2

    .line 1887
    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 1888
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    goto :goto_2

    .line 1872
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private eraseByStroke()Landroid/view/View;
    .locals 14

    .prologue
    const/16 v7, 0x13

    const/16 v4, 0x10

    const/high16 v3, 0x41500000    # 13.0f

    const/4 v9, 0x1

    const/4 v13, 0x0

    .line 1813
    new-instance v11, Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v11, v0}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 1814
    .local v11, "typeCheckBox":Landroid/widget/CheckBox;
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 1815
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x421c0000    # 39.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1814
    invoke-direct {v12, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1816
    .local v12, "typeCheckBoxParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    add-int/lit8 v0, v0, 0x5

    .line 1817
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1816
    invoke-virtual {v12, v0, v13, v1, v13}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1818
    invoke-virtual {v11, v12}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1819
    invoke-virtual {v11, v9}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 1820
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v10

    .line 1821
    .local v10, "curLang":Ljava/lang/String;
    const-string v0, "ur"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ar"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    if-le v0, v4, :cond_0

    .line 1822
    invoke-virtual {v11, v9}, Landroid/widget/CheckBox;->setSingleLine(Z)V

    .line 1824
    :cond_0
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v11, v0}, Landroid/widget/CheckBox;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1825
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    if-le v0, v4, :cond_1

    .line 1826
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {v11, v0, v13, v13, v13}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 1833
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v1, "string_erase_line_by_line"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 1834
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_2

    .line 1835
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_btn_check_off"

    .line 1836
    const-string v2, "snote_btn_check_on"

    const-string v3, "snote_btn_check_on_focused"

    const-string v4, "snote_btn_check_off_focused"

    .line 1837
    const-string v5, "snote_btn_check_on_pressed"

    const-string v6, "snote_btn_check_off_pressed"

    move v8, v7

    .line 1835
    invoke-virtual/range {v0 .. v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/widget/CheckBox;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1843
    :goto_1
    const/high16 v0, -0x1000000

    invoke-virtual {v11, v0}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 1844
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v11, v13, v0}, Landroid/widget/CheckBox;->setTextSize(IF)V

    .line 1845
    invoke-virtual {v11, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1847
    return-object v11

    .line 1829
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41f80000    # 31.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {v11, v0, v13, v13, v13}, Landroid/widget/CheckBox;->setPadding(IIII)V

    goto :goto_0

    .line 1839
    :cond_2
    new-instance v0, Landroid/graphics/drawable/RippleDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v2, 0x40

    invoke-static {v2, v13, v13, v13}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 1840
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1839
    invoke-virtual {v11, v0}, Landroid/widget/CheckBox;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method private exitButton()Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0x29

    const/16 v6, 0x24

    const/4 v4, -0x1

    const/high16 v3, 0x42240000    # 41.0f

    const/16 v7, 0xff

    .line 1424
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1426
    .local v1, "exitButton":Landroid/widget/ImageButton;
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v2, 0x15

    if-ge v0, v2, :cond_0

    .line 1427
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1428
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    sub-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x26

    div-int/lit8 v0, v0, 0x24

    .line 1427
    invoke-direct {v8, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1430
    .local v8, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v2, "snote_popup_close"

    const-string v3, "snote_popup_close_press"

    const-string v4, "snote_popup_close_focus"

    .line 1431
    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_WIDTH:I

    .line 1430
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1441
    :goto_0
    const/16 v0, 0xb

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1443
    invoke-virtual {v1, v8}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1444
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1445
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1447
    return-object v1

    .line 1433
    .end local v8    # "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1434
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    sub-int/2addr v0, v2

    .line 1433
    invoke-direct {v8, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1436
    .restart local v8    # "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v0, Landroid/graphics/drawable/RippleDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 1437
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1436
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1438
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v2, "snote_popup_close"

    const-string v3, "snote_popup_close_press"

    const-string v4, "snote_popup_close_focus"

    .line 1439
    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_WIDTH:I

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    .line 1438
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    goto :goto_0
.end method

.method private findMinValue(Landroid/widget/TextView;I)V
    .locals 4
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "maxWidth"    # I

    .prologue
    const/4 v3, 0x0

    .line 1216
    const/high16 v0, 0x41800000    # 16.0f

    .line 1218
    .local v0, "currentFloat":F
    :goto_0
    invoke-virtual {p1, v3, v3}, Landroid/widget/TextView;->measure(II)V

    .line 1219
    invoke-virtual {p1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    .line 1221
    .local v1, "width":I
    if-le v1, p2, :cond_0

    .line 1222
    const/high16 v2, 0x3f000000    # 0.5f

    sub-float/2addr v0, v2

    .line 1223
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 1225
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1229
    return-void
.end method

.method private getMovableRect()Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2411
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 2412
    .local v0, "location":[I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2414
    .local v1, "r":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    .line 2416
    aget v2, v0, v4

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLeftMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 2417
    aget v2, v0, v5

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTopMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 2418
    aget v2, v0, v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 2419
    aget v2, v0, v5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 2421
    return-object v1
.end method

.method private rotatePosition()V
    .locals 14

    .prologue
    const v13, 0x3f7d70a4    # 0.99f

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 1903
    const-string v8, "settingui-settingRemover"

    const-string v9, "==== SettingRemover ===="

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1904
    const-string v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "old  = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1905
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1904
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1906
    const-string v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "new  = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1907
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1906
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1909
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1911
    .local v4, "r":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    aget v8, v8, v11

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 1912
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    const/4 v9, 0x1

    aget v8, v8, v9

    iput v8, v4, Landroid/graphics/Rect;->top:I

    .line 1913
    iget v8, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->right:I

    .line 1914
    iget v8, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 1916
    const-string v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "view = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1918
    iget v8, v4, Landroid/graphics/Rect;->left:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v9

    int-to-float v2, v8

    .line 1919
    .local v2, "left":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    iget v9, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v9

    int-to-float v5, v8

    .line 1920
    .local v5, "right":F
    iget v8, v4, Landroid/graphics/Rect;->top:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v9

    int-to-float v6, v8

    .line 1921
    .local v6, "top":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget v9, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v8, v9

    int-to-float v0, v8

    .line 1923
    .local v0, "bottom":F
    add-float v8, v2, v5

    div-float v1, v2, v8

    .line 1924
    .local v1, "hRatio":F
    add-float v8, v6, v0

    div-float v7, v6, v8

    .line 1926
    .local v7, "vRatio":F
    const-string v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "left :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", right :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1927
    const-string v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "top :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", bottom :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1928
    const-string v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "hRatio = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", vRatio = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1930
    cmpl-float v8, v1, v13

    if-lez v8, :cond_2

    .line 1931
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1936
    :cond_0
    :goto_0
    cmpl-float v8, v7, v13

    if-lez v8, :cond_3

    .line 1937
    const/high16 v7, 0x3f800000    # 1.0f

    .line 1942
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1944
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    if-ge v8, v9, :cond_4

    .line 1945
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, v1

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1950
    :goto_2
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    if-ge v8, v9, :cond_5

    .line 1951
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, v7

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1956
    :goto_3
    const-string v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "lMargin = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", tMargin = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1957
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1958
    return-void

    .line 1932
    .end local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_2
    cmpg-float v8, v1, v12

    if-gez v8, :cond_0

    .line 1933
    const/4 v1, 0x0

    goto :goto_0

    .line 1938
    :cond_3
    cmpg-float v8, v7, v12

    if-gez v8, :cond_1

    .line 1939
    const/4 v7, 0x0

    goto :goto_1

    .line 1947
    .restart local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_4
    iput v11, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_2

    .line 1953
    :cond_5
    iput v11, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_3
.end method

.method private setEnableSizeSeekbar(Z)V
    .locals 8
    .param p1, "enabled"    # Z

    .prologue
    const/high16 v7, 0x40a00000    # 5.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1633
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_5

    .line 1634
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v5}, Landroid/widget/SeekBar;->setPressed(Z)V

    .line 1638
    if-eqz p1, :cond_9

    .line 1639
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->isFirstClickChexBox:Z

    if-eqz v1, :cond_1

    .line 1640
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1641
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v2

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    float-to-int v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1643
    :cond_0
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->isFirstClickChexBox:Z

    .line 1645
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v6}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 1646
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1647
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1648
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1649
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "fa"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1650
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    .line 1651
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->convertToArabicNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1655
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1656
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43160000    # 150.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x41100000    # 9.0f

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1655
    add-int v0, v1, v2

    .line 1657
    .local v0, "seek_label_pos":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/16 v2, 0xa

    if-lt v1, v2, :cond_3

    .line 1658
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    sub-int/2addr v0, v1

    .line 1660
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setX(F)V

    .line 1661
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setY(F)V

    .line 1662
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setX(F)V

    .line 1663
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setY(F)V

    .line 1665
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    check-cast v1, Landroid/widget/CompoundButton;

    invoke-virtual {v1, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 1666
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-eq v1, v6, :cond_5

    .line 1667
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    if-ne v1, v2, :cond_7

    .line 1668
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1669
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1670
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z

    if-eqz v1, :cond_4

    .line 1671
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z

    .line 1676
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    if-nez v1, :cond_8

    .line 1677
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1678
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1679
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z

    if-eqz v1, :cond_5

    .line 1680
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z

    .line 1695
    .end local v0    # "seek_label_pos":I
    :cond_5
    :goto_2
    return-void

    .line 1653
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1674
    .restart local v0    # "seek_label_pos":I
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_1

    .line 1683
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_2

    .line 1688
    .end local v0    # "seek_label_pos":I
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v5}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 1689
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1690
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1691
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1692
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    check-cast v1, Landroid/widget/CompoundButton;

    invoke-virtual {v1, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2
.end method

.method private setListener()V
    .locals 4

    .prologue
    .line 1248
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1249
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1252
    :cond_0
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 1254
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1255
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1258
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_2

    .line 1260
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1261
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1262
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1263
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1266
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_3

    .line 1267
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1268
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1269
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1270
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1273
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_4

    .line 1274
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1277
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    if-eqz v1, :cond_5

    .line 1278
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1280
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    if-eqz v1, :cond_6

    .line 1281
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    check-cast v1, Landroid/widget/CompoundButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1284
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    array-length v1, v1

    if-lez v1, :cond_7

    .line 1285
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x1

    if-le v0, v1, :cond_9

    .line 1292
    .end local v0    # "i":I
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_8

    .line 1293
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1296
    :cond_8
    return-void

    .line 1286
    .restart local v0    # "i":I
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v1, v1, v0

    if-eqz v1, :cond_a

    .line 1287
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1285
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private titleBg()Landroid/view/View;
    .locals 14

    .prologue
    const/16 v13, 0x8

    const/4 v12, 0x1

    const/4 v11, -0x2

    const v10, 0x42e46666    # 114.2f

    const/4 v9, -0x1

    .line 960
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v7}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 961
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 963
    new-instance v2, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 964
    .local v2, "titleLeft":Landroid/widget/ImageView;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 965
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 964
    invoke-direct {v3, v7, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 966
    .local v3, "titleLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v12, v3, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 967
    const/16 v7, 0x9

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 968
    const/16 v7, 0xa

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 970
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 972
    new-instance v7, Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    .line 973
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 975
    .local v1, "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 977
    new-instance v4, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v4, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 978
    .local v4, "titleRight":Landroid/widget/ImageView;
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 979
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 978
    invoke-direct {v6, v7, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 980
    .local v6, "titleRightParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v12, v6, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 981
    const/16 v7, 0xb

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 982
    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 983
    const/16 v7, 0x13

    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 984
    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 986
    new-instance v7, Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    .line 987
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 989
    .local v5, "titleRightIndicatorParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v7, 0xb

    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 990
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 992
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v8, "snote_popup_title_left"

    invoke-virtual {v7, v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 993
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    const-string v9, "snote_popup_title_center"

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 994
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    const-string v9, "snote_popup_title_bended"

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 995
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v8, "snote_popup_title_right"

    invoke-virtual {v7, v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 997
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 998
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 999
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1000
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1001
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1003
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1004
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1005
    return-object v0
.end method

.method private titleLayout()Landroid/view/ViewGroup;
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/high16 v6, 0x42240000    # 41.0f

    const/4 v5, 0x0

    .line 931
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 932
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 933
    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v3, v7, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 932
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 934
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->exitButton()Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    .line 935
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    const v4, 0xb82f91

    invoke-virtual {v3, v4}, Landroid/view/View;->setId(I)V

    .line 936
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->titleBg()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 937
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->titleText()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 938
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 939
    .local v1, "mButtonLayout":Landroid/widget/RelativeLayout;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 940
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 939
    invoke-direct {v2, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 941
    .local v2, "mButtonLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 942
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 943
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 944
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    add-int/lit8 v3, v3, 0x13

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 945
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_0

    .line 946
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v1, v5, v5, v3, v5}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 948
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 949
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 950
    return-object v0
.end method

.method private titleText()Landroid/view/View;
    .locals 9

    .prologue
    const/16 v8, 0x13

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 1010
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    .line 1011
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1012
    const/high16 v4, 0x42240000    # 41.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v2, v6, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1011
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1013
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1014
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 1015
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 1016
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_eraser_settings"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1017
    .local v0, "eraserText":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1018
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1019
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1022
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x1c

    if-le v1, v2, :cond_0

    .line 1023
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1030
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_eraser_settings"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1031
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41200000    # 10.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/lit8 v2, v2, 0x9

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    invoke-virtual {v1, v2, v3, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1032
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    return-object v1

    .line 1024
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v8, :cond_1

    .line 1025
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41600000    # 14.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 1027
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method

.method private totalLayout()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 912
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x43640000    # 228.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/lit8 v1, v1, 0x13

    .line 913
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x432c0000    # 172.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x14

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 912
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 914
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setOrientation(I)V

    .line 915
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 916
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setLayoutDirection(I)V

    .line 918
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->titleLayout()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    .line 919
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->bodyLayout()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mBodyLayout:Landroid/view/View;

    .line 920
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->addView(Landroid/view/View;)V

    .line 921
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->addView(Landroid/view/View;)V

    .line 922
    return-void
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1134
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 1135
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 1136
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    .line 1137
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 1138
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 1139
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllListener:Landroid/view/View$OnClickListener;

    .line 1140
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 1141
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 1142
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1143
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1144
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 1145
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    .line 1146
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 1147
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    .line 1148
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1149
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeListner:Landroid/view/View$OnClickListener;

    .line 1150
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterKeyListener:Landroid/view/View$OnKeyListener;

    .line 1152
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->repeatUpdateHandler:Landroid/os/Handler;

    .line 1153
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    if-nez v1, :cond_0

    .line 1213
    :goto_0
    return-void

    .line 1157
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1158
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1159
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    .line 1160
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeButton:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1161
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeButton:Landroid/view/ViewGroup;

    .line 1162
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1163
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    .line 1164
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1165
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    .line 1166
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1167
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    .line 1168
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1169
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    .line 1170
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1171
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    .line 1172
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1173
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    .line 1174
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1175
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    .line 1177
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1178
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    .line 1179
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1180
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    .line 1181
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1182
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    .line 1183
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 1184
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_3

    .line 1189
    .end local v0    # "i":I
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    .line 1190
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    .line 1191
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1192
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    .line 1193
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1194
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    .line 1195
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1196
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mBodyLayout:Landroid/view/View;

    .line 1198
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 1199
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-eqz v1, :cond_2

    .line 1200
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    .line 1201
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v2, 0x1

    aput-object v3, v1, v2

    .line 1202
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 1204
    :cond_2
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 1205
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1206
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .line 1207
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    .line 1208
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    .line 1209
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1210
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 1211
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->close()V

    .line 1212
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    goto/16 :goto_0

    .line 1185
    .restart local v0    # "i":I
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1186
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aput-object v3, v1, v0

    .line 1184
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    .prologue
    .line 2132
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    return-object v0
.end method

.method public getViewMode()I
    .locals 1

    .prologue
    .line 2116
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    return v0
.end method

.method protected initView()V
    .locals 3

    .prologue
    .line 1233
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->totalLayout()V

    .line 1234
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_WIDTH:I

    rsub-int v2, v2, 0xe4

    add-int/lit8 v2, v2, -0xe

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/lit8 v1, v1, -0x12

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->findMinValue(Landroid/widget/TextView;I)V

    .line 1235
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1236
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 1238
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-nez v0, :cond_0

    .line 1239
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 1240
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v1, 0x0

    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    aput-object v2, v0, v1

    .line 1241
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v1, 0x1

    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    aput-object v2, v0, v1

    .line 1244
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 1245
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v1, 0x1

    .line 79
    const-string v2, "settingui-settingRemover"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onConfig remover "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getVisibility()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 83
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 84
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLocationOnScreen([I)V

    .line 93
    :cond_0
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsRotated:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 98
    return-void

    .line 87
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLocationOnScreen([I)V

    .line 88
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsFirstShown:Z

    if-nez v2, :cond_0

    .line 89
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    if-eqz v2, :cond_2

    const/4 v1, 0x0

    :cond_2
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    .line 90
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 11
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 2044
    if-ne p1, p0, :cond_0

    .line 2045
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    if-eqz v4, :cond_0

    .line 2047
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    invoke-interface {v4, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;->onVisibilityChanged(I)V

    .line 2050
    :cond_0
    if-ne p1, p0, :cond_6

    if-nez p2, :cond_6

    .line 2051
    const/4 v4, 0x2

    new-array v1, v4, [I

    .line 2052
    .local v1, "location":[I
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLocationInWindow([I)V

    .line 2053
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsFirstShown:Z

    if-eqz v4, :cond_1

    .line 2054
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsFirstShown:Z

    .line 2056
    :cond_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    if-eqz v4, :cond_4

    .line 2058
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2060
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    if-eqz v4, :cond_2

    .line 2061
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->rotatePosition()V

    .line 2064
    :cond_2
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    .line 2087
    :cond_3
    :goto_0
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsRotated:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2096
    .end local v1    # "location":[I
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 2097
    return-void

    .line 2068
    .restart local v1    # "location":[I
    :cond_4
    :try_start_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedCalculateMargin:Z

    if-eqz v4, :cond_5

    .line 2069
    const/4 v4, 0x2

    new-array v2, v4, [I

    .line 2070
    .local v2, "parentLocation":[I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->getLocationInWindow([I)V

    .line 2072
    const/4 v4, 0x0

    aget v4, v1, v4

    const/4 v5, 0x0

    aget v5, v2, v5

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLeftMargin:I

    .line 2073
    const/4 v4, 0x1

    aget v4, v1, v4

    const/4 v5, 0x1

    aget v5, v2, v5

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTopMargin:I

    .line 2075
    const/4 v4, 0x2

    new-array v3, v4, [I

    .line 2076
    .local v3, "rootLocation":[I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2078
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedCalculateMargin:Z

    .line 2080
    .end local v2    # "parentLocation":[I
    .end local v3    # "rootLocation":[I
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2082
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    aget v6, v1, v6

    const/4 v7, 0x1

    aget v7, v1, v7

    const/4 v8, 0x0

    aget v8, v1, v8

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    const/4 v9, 0x1

    aget v9, v1, v9

    .line 2083
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getHeight()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2082
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v4

    .line 2083
    if-nez v4, :cond_3

    .line 2084
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->checkPosition()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2093
    .end local v1    # "location":[I
    :catch_0
    move-exception v0

    .line 2094
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 2090
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_6
    const/4 v4, 0x1

    :try_start_2
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    .line 2091
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    if-nez v0, :cond_0

    .line 67
    invoke-super {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 70
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 71
    return-void
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    .prologue
    .line 2384
    if-eqz p1, :cond_0

    .line 2385
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    .line 2387
    :cond_0
    return-void
.end method

.method public setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 2
    .param p1, "canvasView"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .prologue
    .line 2234
    if-eqz p1, :cond_1

    .line 2235
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 2236
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    if-eqz v0, :cond_3

    .line 2237
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v0, v1, :cond_2

    .line 2238
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    .line 2243
    :goto_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    const/16 v1, 0x5f0

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    const/16 v1, 0x5fc

    if-ne v0, v1, :cond_1

    .line 2244
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    .line 2250
    :cond_1
    :goto_1
    return-void

    .line 2240
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    goto :goto_0

    .line 2247
    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    goto :goto_1
.end method

.method public setIndicatorPosition(I)V
    .locals 8
    .param p1, "x"    # I

    .prologue
    const/high16 v7, 0x43640000    # 228.0f

    const/high16 v6, 0x41f00000    # 30.0f

    const/4 v5, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1314
    if-gez p1, :cond_1

    .line 1315
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1316
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1317
    const/16 v1, -0x63

    if-ne p1, v1, :cond_0

    .line 1318
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    .line 1319
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mFirstLongPress:Z

    .line 1356
    :goto_0
    return-void

    .line 1321
    :cond_0
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    goto :goto_0

    .line 1324
    :cond_1
    const/16 v1, 0x9

    if-ge p1, v1, :cond_2

    .line 1325
    const/16 p1, 0x9

    .line 1327
    :cond_2
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    .line 1328
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1329
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_4

    .line 1330
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    sub-int/2addr v1, v2

    .line 1331
    add-int/lit8 v1, v1, -0x9

    if-le p1, v1, :cond_3

    .line 1332
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1333
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1354
    :goto_1
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mFirstLongPress:Z

    goto :goto_0

    .line 1335
    :cond_3
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1336
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, -0x1

    .line 1335
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1337
    .local v0, "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, p1, v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1338
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1339
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1342
    .end local v0    # "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/lit8 v1, v1, -0x2d

    add-int/lit8 v1, v1, -0x9

    if-le p1, v1, :cond_5

    .line 1343
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1344
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1346
    :cond_5
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1347
    const/4 v1, -0x2

    const/4 v2, -0x1

    .line 1346
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1348
    .restart local v0    # "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, p1, v3, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1349
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1350
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 4
    .param p1, "settingCutterInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2268
    if-nez p1, :cond_0

    .line 2300
    :goto_0
    return-void

    .line 2272
    :cond_0
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    .line 2273
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-ne v0, v1, :cond_4

    .line 2274
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setEnableSizeSeekbar(Z)V

    .line 2278
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    if-eqz v0, :cond_6

    .line 2279
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v0, v1, :cond_5

    .line 2280
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    .line 2285
    :goto_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    const/16 v1, 0x5f0

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    const/16 v1, 0x5fc

    if-ne v0, v1, :cond_2

    .line 2286
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    .line 2291
    :cond_2
    :goto_3
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    .line 2292
    iput v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 2294
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    aget-object v0, v0, v1

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 2296
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    float-to-int v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 2298
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 2299
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    goto :goto_0

    .line 2276
    :cond_4
    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setEnableSizeSeekbar(Z)V

    goto :goto_1

    .line 2282
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    goto :goto_2

    .line 2289
    :cond_6
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    goto :goto_3
.end method

.method public setPosition(II)V
    .locals 9
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v8, 0x0

    .line 1388
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    .line 1389
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->handlerRotate:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->runnableRotate:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1390
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->handlerRotate:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->runnableRotate:Ljava/lang/Runnable;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1392
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1394
    .local v2, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43640000    # 228.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    add-int/lit8 v1, v3, 0x2

    .line 1395
    .local v1, "minWidth":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x432c0000    # 172.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 1397
    .local v0, "minHeight":I
    if-gez p1, :cond_2

    .line 1398
    const/4 p1, 0x0

    .line 1403
    :cond_0
    :goto_0
    if-gez p2, :cond_3

    .line 1404
    const/4 p2, 0x0

    .line 1409
    :cond_1
    :goto_1
    iput p1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1410
    iput p2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1411
    const-string v3, "settingui-settingRemover"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "set Position x,y : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1412
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1413
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1414
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, p1

    aput v4, v3, v8

    .line 1415
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, p2

    aput v5, v3, v4

    .line 1416
    return-void

    .line 1399
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v3, v1

    if-le p1, v3, :cond_0

    .line 1400
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int p1, v3, v1

    goto :goto_0

    .line 1405
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v3, v0

    if-le p2, v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-le v3, v0, :cond_1

    .line 1406
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int p2, v3, v0

    goto :goto_1
.end method

.method public setRemoverInfoList([Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 0
    .param p1, "list"    # [Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 2343
    if-eqz p1, :cond_0

    .line 2344
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 2346
    :cond_0
    return-void
.end method

.method public setRemoverListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .prologue
    .line 2363
    if-eqz p1, :cond_0

    .line 2364
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .line 2366
    :cond_0
    return-void
.end method

.method public setViewMode(I)V
    .locals 7
    .param p1, "viewMode"    # I

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 2152
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    .line 2155
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 2156
    .local v2, "tempRequestLayoutDisable":Z
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 2159
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    if-nez v3, :cond_0

    .line 2160
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2161
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2162
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2189
    :goto_0
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 2191
    return-void

    .line 2163
    :cond_0
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 2164
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->addCutterLayout()V

    .line 2165
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2166
    .local v0, "cutterTypeViewGroup":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    new-array v3, v3, [Landroid/view/View;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    .line 2167
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 2170
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2171
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2172
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 2168
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    aput-object v4, v3, v1

    .line 2167
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2173
    .end local v0    # "cutterTypeViewGroup":Landroid/widget/LinearLayout;
    .end local v1    # "i":I
    :cond_2
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 2175
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2176
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2177
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 2178
    :cond_3
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 2179
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2180
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2182
    :cond_4
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    .line 2183
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2184
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2185
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 2203
    if-nez p1, :cond_1

    .line 2204
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-nez v1, :cond_0

    .line 2205
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->loadImage()V

    .line 2207
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsShowing:Z

    .line 2212
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2216
    :goto_1
    return-void

    .line 2209
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsShowing:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2213
    :catch_0
    move-exception v0

    .line 2214
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method public setVisibilityChangedListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    .prologue
    .line 2405
    if-eqz p1, :cond_0

    .line 2406
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    .line 2408
    :cond_0
    return-void
.end method
