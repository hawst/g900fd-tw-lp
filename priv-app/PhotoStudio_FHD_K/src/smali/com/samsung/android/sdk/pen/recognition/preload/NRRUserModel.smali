.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;
.super Ljava/lang/Object;
.source "NRRUserModel.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DefaultLocale"
    }
.end annotation


# static fields
.field public static final EMPTY:I = 0x0

.field private static final FILES_DIR:Ljava/lang/String; = "signatures"

.field public static final LOADED_FROM_FILE:I = 0x1

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MODEL_FILE:Ljava/lang/String; = "model.dat"

.field private static final SIGNATURES_FILE:Ljava/lang/String; = "signatures.dat"

.field public static final TRAINED_FROM_FILE:I = 0x2

.field public static final TRAINED_FROM_USER_INPUT:I = 0x3

.field private static final TRANSFORMATION:Ljava/lang/String; = "AES/CBC/PKCS5Padding"


# instance fields
.field private final mDirPath:Ljava/lang/String;

.field private mModel:Lcom/samsung/recognitionengine/UserModel;

.field private final mModelFileName:Ljava/lang/String;

.field private final mSignaturesFileName:Ljava/lang/String;

.field private mState:I

.field private mTrainer:Lcom/samsung/recognitionengine/Trainer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "signatures"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mDirPath:Ljava/lang/String;

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mDirPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "model.dat"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModelFileName:Ljava/lang/String;

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mDirPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "signatures.dat"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mSignaturesFileName:Ljava/lang/String;

    .line 66
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->loadModelFromFile()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->trainFromFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I

    .line 70
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->saveModelToFile()Z

    goto :goto_0
.end method

.method private appendSignatureToFile(Lcom/samsung/recognitionengine/Signature;)Z
    .locals 9
    .param p1, "sig"    # Lcom/samsung/recognitionengine/Signature;

    .prologue
    const/4 v5, 0x0

    .line 252
    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mDirPath:Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 254
    .local v1, "modelDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_1

    .line 255
    sget-object v6, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Failed to create directory "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mDirPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v5

    .line 274
    :cond_0
    :goto_0
    return v2

    .line 259
    :cond_1
    const/4 v2, 0x0

    .line 260
    .local v2, "retValue":Z
    const/4 v3, 0x0

    .line 263
    .local v3, "writer":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
    :try_start_0
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mSignaturesFileName:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-static {v7}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->createCipher(I)Ljavax/crypto/Cipher;

    move-result-object v7

    const/4 v8, 0x2

    invoke-static {v8}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->createCipher(I)Ljavax/crypto/Cipher;

    move-result-object v8

    invoke-direct {v4, v6, v7, v8}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;-><init>(Ljava/lang/String;Ljavax/crypto/Cipher;Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    .end local v3    # "writer":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
    .local v4, "writer":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
    :try_start_1
    invoke-virtual {v4, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->append(Lcom/samsung/recognitionengine/Signature;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    .line 269
    if-eqz v4, :cond_0

    .line 270
    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->close()V

    goto :goto_0

    .line 265
    .end local v4    # "writer":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
    .restart local v3    # "writer":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
    :catch_0
    move-exception v0

    .line 266
    .local v0, "ex":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    sget-object v6, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Can not open file "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mSignaturesFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 269
    if-eqz v3, :cond_2

    .line 270
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->close()V

    :cond_2
    move v2, v5

    .line 267
    goto :goto_0

    .line 268
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    .line 269
    :goto_2
    if-eqz v3, :cond_3

    .line 270
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->close()V

    .line 272
    :cond_3
    throw v5

    .line 268
    .end local v3    # "writer":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
    .restart local v4    # "writer":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "writer":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
    .restart local v3    # "writer":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
    goto :goto_2

    .line 265
    .end local v3    # "writer":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
    .restart local v4    # "writer":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4    # "writer":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
    .restart local v3    # "writer":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
    goto :goto_1
.end method

.method public static convertSpenObjectStrokesToSignature(Ljava/util/List;)Lcom/samsung/recognitionengine/Signature;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)",
            "Lcom/samsung/recognitionengine/Signature;"
        }
    .end annotation

    .prologue
    .line 278
    .local p0, "spenObjectStrokes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    new-instance v10, Lcom/samsung/recognitionengine/Signature;

    invoke-direct {v10}, Lcom/samsung/recognitionengine/Signature;-><init>()V

    .line 281
    .local v10, "signature":Lcom/samsung/recognitionengine/Signature;
    const/high16 v5, -0x80000000

    .line 282
    .local v5, "lastValidTimeStamp":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-lt v3, v0, :cond_0

    .line 316
    return-object v10

    .line 283
    :cond_0
    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 284
    .local v12, "spenObjectStroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v17

    move-object/from16 v0, v17

    array-length v11, v0

    .line 285
    .local v11, "size":I
    new-instance v14, Lcom/samsung/recognitionengine/Stroke;

    invoke-direct {v14}, Lcom/samsung/recognitionengine/Stroke;-><init>()V

    .line 286
    .local v14, "stroke":Lcom/samsung/recognitionengine/Stroke;
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v8

    .line 287
    .local v8, "pointFs":[Landroid/graphics/PointF;
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPressures()[F

    move-result-object v9

    .line 288
    .local v9, "pressures":[F
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getTimeStamps()[I

    move-result-object v16

    .line 289
    .local v16, "timeStamps":[I
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getOrientations()[F

    move-result-object v6

    .line 290
    .local v6, "orientations":[F
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getTilts()[F

    move-result-object v15

    .line 292
    .local v15, "tilts":[F
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-lt v4, v11, :cond_1

    .line 314
    invoke-virtual {v10, v14}, Lcom/samsung/recognitionengine/Signature;->add(Lcom/samsung/recognitionengine/Stroke;)V

    .line 282
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 293
    :cond_1
    new-instance v13, Lcom/samsung/recognitionengine/TouchPoint;

    invoke-direct {v13}, Lcom/samsung/recognitionengine/TouchPoint;-><init>()V

    .line 294
    .local v13, "sshapePoint":Lcom/samsung/recognitionengine/TouchPoint;
    new-instance v7, Lcom/samsung/recognitionengine/PointF;

    aget-object v17, v8, v4

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v17, v0

    aget-object v18, v8, v4

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v7, v0, v1}, Lcom/samsung/recognitionengine/PointF;-><init>(FF)V

    .line 295
    .local v7, "pointF":Lcom/samsung/recognitionengine/PointF;
    invoke-virtual {v13, v7}, Lcom/samsung/recognitionengine/TouchPoint;->setPoint(Lcom/samsung/recognitionengine/PointF;)V

    .line 296
    aget v17, v9, v4

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/samsung/recognitionengine/TouchPoint;->setPressure(F)V

    .line 297
    aget v17, v6, v4

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/samsung/recognitionengine/TouchPoint;->setOrientation(F)V

    .line 298
    aget v17, v15, v4

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/samsung/recognitionengine/TouchPoint;->setTilt(F)V

    .line 300
    aget v2, v16, v4

    .line 302
    .local v2, "currentTimeStamp":I
    if-eqz v4, :cond_2

    sub-int v17, v2, v5

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(I)I

    move-result v17

    const/16 v18, 0x3e8

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_2

    .line 292
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 305
    :cond_2
    if-ge v2, v5, :cond_3

    .line 306
    int-to-double v0, v5

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v13, v0, v1}, Lcom/samsung/recognitionengine/TouchPoint;->setTimestamp(D)V

    .line 312
    :goto_3
    invoke-virtual {v14, v13}, Lcom/samsung/recognitionengine/Stroke;->add(Lcom/samsung/recognitionengine/TouchPoint;)V

    goto :goto_2

    .line 308
    :cond_3
    int-to-double v0, v2

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v13, v0, v1}, Lcom/samsung/recognitionengine/TouchPoint;->setTimestamp(D)V

    .line 309
    move v5, v2

    goto :goto_3
.end method

.method private static createCipher(I)Ljavax/crypto/Cipher;
    .locals 11
    .param p0, "mode"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    const/16 v10, 0x10

    .line 325
    const/4 v0, 0x0

    .line 328
    .local v0, "cipher":Ljavax/crypto/Cipher;
    :try_start_0
    const-string v8, "AES/CBC/PKCS5Padding"

    invoke-static {v8}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 335
    :goto_0
    if-nez v0, :cond_0

    .line 336
    const/4 v8, 0x0

    .line 367
    :goto_1
    return-object v8

    .line 329
    :catch_0
    move-exception v1

    .line 330
    .local v1, "ex":Ljava/security/NoSuchAlgorithmException;
    sget-object v8, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v9, "No such algorithm!"

    invoke-static {v8, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 331
    .end local v1    # "ex":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v1

    .line 332
    .local v1, "ex":Ljavax/crypto/NoSuchPaddingException;
    sget-object v8, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v9, "No such pudding!"

    invoke-static {v8, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 339
    .end local v1    # "ex":Ljavax/crypto/NoSuchPaddingException;
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 340
    .local v5, "keyBuilder":Ljava/lang/StringBuilder;
    sget-object v8, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    sget-object v8, Landroid/os/Build;->BOOTLOADER:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    sget-object v8, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    sget-object v8, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    sget-object v8, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    sget-object v8, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 348
    .local v2, "hashBytes":[B
    invoke-static {v2, v10}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v6

    .line 350
    .local v6, "keyBytes":[B
    new-instance v4, Ljavax/crypto/spec/SecretKeySpec;

    const-string v8, "AES"

    invoke-direct {v4, v6, v8}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 353
    .local v4, "key":Ljavax/crypto/SecretKey;
    new-array v3, v10, [B

    fill-array-data v3, :array_0

    .line 355
    .local v3, "iv":[B
    new-instance v7, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v7, v3}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 358
    .local v7, "params":Ljava/security/spec/AlgorithmParameterSpec;
    :try_start_1
    invoke-virtual {v0, p0, v4, v7}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_1
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    move-object v8, v0

    .line 367
    goto :goto_1

    .line 359
    :catch_2
    move-exception v1

    .line 360
    .local v1, "ex":Ljava/security/InvalidAlgorithmParameterException;
    sget-object v8, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v9, "Invalid Algorithm parameter!"

    invoke-static {v8, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 361
    const/4 v0, 0x0

    goto :goto_2

    .line 362
    .end local v1    # "ex":Ljava/security/InvalidAlgorithmParameterException;
    :catch_3
    move-exception v1

    .line 363
    .local v1, "ex":Ljava/security/InvalidKeyException;
    sget-object v8, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v9, "Invalid key!"

    invoke-static {v8, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 364
    const/4 v0, 0x0

    goto :goto_2

    .line 353
    nop

    :array_0
    .array-data 1
        -0x4et
        0x12t
        -0x2bt
        -0x4et
        0x44t
        0x21t
        -0x3dt
        -0x3dt
        0x58t
        -0x66t
        0x4ft
        -0x33t
        0x37t
        -0x1ct
        0x7bt
        0x29t
    .end array-data
.end method

.method private createTrainer()V
    .locals 2

    .prologue
    .line 247
    new-instance v0, Lcom/samsung/recognitionengine/Trainer;

    invoke-direct {v0}, Lcom/samsung/recognitionengine/Trainer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    .line 248
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/Trainer;->setValidateNextSignature(Z)V

    .line 249
    return-void
.end method

.method private dropFile(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 141
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 142
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 143
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 145
    :cond_0
    return-void
.end method

.method public static getMinimumRequiredCount()I
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x3

    return v0
.end method

.method private loadModelFromFile()Z
    .locals 10

    .prologue
    .line 148
    const/4 v5, 0x0

    .line 149
    .local v5, "return_value":Z
    const/4 v2, 0x0

    .line 152
    .local v2, "fileReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    new-instance v7, Ljavax/crypto/CipherInputStream;

    new-instance v8, Ljava/io/FileInputStream;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModelFileName:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x2

    invoke-static {v9}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->createCipher(I)Ljavax/crypto/Cipher;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    invoke-direct {v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    .end local v2    # "fileReader":Ljava/io/BufferedReader;
    .local v3, "fileReader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "content":Ljava/lang/String;
    new-instance v4, Lcom/samsung/recognitionengine/UserModelStringReader;

    invoke-direct {v4, v0}, Lcom/samsung/recognitionengine/UserModelStringReader;-><init>(Ljava/lang/String;)V

    .line 155
    .local v4, "modelReader":Lcom/samsung/recognitionengine/UserModelStringReader;
    invoke-static {v4}, Lcom/samsung/recognitionengine/UserModel;->readModel(Lcom/samsung/recognitionengine/UserModelReader;)Lcom/samsung/recognitionengine/UserModel;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    .line 157
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-virtual {v6}, Lcom/samsung/recognitionengine/UserModel;->isValid()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v6

    if-eqz v6, :cond_1

    .line 158
    const/4 v5, 0x1

    .line 165
    :goto_0
    if-eqz v3, :cond_3

    .line 167
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    move-object v2, v3

    .line 174
    .end local v0    # "content":Ljava/lang/String;
    .end local v3    # "fileReader":Ljava/io/BufferedReader;
    .end local v4    # "modelReader":Lcom/samsung/recognitionengine/UserModelStringReader;
    .restart local v2    # "fileReader":Ljava/io/BufferedReader;
    :cond_0
    :goto_1
    return v5

    .line 160
    .end local v2    # "fileReader":Ljava/io/BufferedReader;
    .restart local v0    # "content":Ljava/lang/String;
    .restart local v3    # "fileReader":Ljava/io/BufferedReader;
    .restart local v4    # "modelReader":Lcom/samsung/recognitionengine/UserModelStringReader;
    :cond_1
    const/4 v6, 0x0

    :try_start_3
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 162
    .end local v0    # "content":Ljava/lang/String;
    .end local v4    # "modelReader":Lcom/samsung/recognitionengine/UserModelStringReader;
    :catch_0
    move-exception v1

    move-object v2, v3

    .line 163
    .end local v3    # "fileReader":Ljava/io/BufferedReader;
    .local v1, "ex":Ljava/lang/Exception;
    .restart local v2    # "fileReader":Ljava/io/BufferedReader;
    :goto_2
    :try_start_4
    sget-object v6, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Failed to read model from file "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModelFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 165
    if-eqz v2, :cond_0

    .line 167
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 168
    :catch_1
    move-exception v1

    .line 169
    sget-object v6, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v7, "Failed to close reader!"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 164
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 165
    :goto_3
    if-eqz v2, :cond_2

    .line 167
    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    .line 172
    :cond_2
    :goto_4
    throw v6

    .line 168
    :catch_2
    move-exception v1

    .line 169
    .restart local v1    # "ex":Ljava/lang/Exception;
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Failed to close reader!"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 168
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "fileReader":Ljava/io/BufferedReader;
    .restart local v0    # "content":Ljava/lang/String;
    .restart local v3    # "fileReader":Ljava/io/BufferedReader;
    .restart local v4    # "modelReader":Lcom/samsung/recognitionengine/UserModelStringReader;
    :catch_3
    move-exception v1

    .line 169
    .restart local v1    # "ex":Ljava/lang/Exception;
    sget-object v6, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v7, "Failed to close reader!"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_3
    move-object v2, v3

    .end local v3    # "fileReader":Ljava/io/BufferedReader;
    .restart local v2    # "fileReader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 164
    .end local v0    # "content":Ljava/lang/String;
    .end local v2    # "fileReader":Ljava/io/BufferedReader;
    .end local v4    # "modelReader":Lcom/samsung/recognitionengine/UserModelStringReader;
    .restart local v3    # "fileReader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "fileReader":Ljava/io/BufferedReader;
    .restart local v2    # "fileReader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 162
    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method private saveModelToFile()Z
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 178
    const/4 v5, 0x0

    .line 180
    .local v5, "returnValue":Z
    new-instance v3, Ljava/io/File;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mDirPath:Ljava/lang/String;

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 182
    .local v3, "modelDir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-nez v7, :cond_1

    .line 183
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Failed to create directory "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mDirPath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_0
    :goto_0
    return v6

    .line 187
    :cond_1
    const/4 v1, 0x0

    .line 190
    .local v1, "fileWriter":Ljava/io/BufferedWriter;
    :try_start_0
    new-instance v4, Lcom/samsung/recognitionengine/UserModelStringWriter;

    invoke-direct {v4}, Lcom/samsung/recognitionengine/UserModelStringWriter;-><init>()V

    .line 191
    .local v4, "modelWriter":Lcom/samsung/recognitionengine/UserModelStringWriter;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-virtual {v7, v4}, Lcom/samsung/recognitionengine/UserModel;->writeModel(Lcom/samsung/recognitionengine/UserModelWriter;)Z

    move-result v5

    .line 192
    if-eqz v5, :cond_2

    .line 193
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v7, Ljava/io/OutputStreamWriter;

    new-instance v8, Ljavax/crypto/CipherOutputStream;

    new-instance v9, Ljava/io/FileOutputStream;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModelFileName:Ljava/lang/String;

    invoke-direct {v9, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x1

    invoke-static {v10}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->createCipher(I)Ljavax/crypto/Cipher;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V

    invoke-direct {v7, v8}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v2, v7}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    .end local v1    # "fileWriter":Ljava/io/BufferedWriter;
    .local v2, "fileWriter":Ljava/io/BufferedWriter;
    :try_start_1
    invoke-virtual {v4}, Lcom/samsung/recognitionengine/UserModelStringWriter;->getString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v1, v2

    .line 200
    .end local v2    # "fileWriter":Ljava/io/BufferedWriter;
    .restart local v1    # "fileWriter":Ljava/io/BufferedWriter;
    :cond_2
    if-eqz v1, :cond_3

    .line 202
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :cond_3
    :goto_1
    move v6, v5

    .line 209
    goto :goto_0

    .line 196
    .end local v4    # "modelWriter":Lcom/samsung/recognitionengine/UserModelStringWriter;
    :catch_0
    move-exception v0

    .line 197
    .local v0, "ex":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Can not open file "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModelFileName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 200
    if-eqz v1, :cond_0

    .line 202
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 203
    :catch_1
    move-exception v0

    .line 204
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Failed to close writer!"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 199
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 200
    :goto_3
    if-eqz v1, :cond_4

    .line 202
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 207
    :cond_4
    :goto_4
    throw v6

    .line 203
    :catch_2
    move-exception v0

    .line 204
    .restart local v0    # "ex":Ljava/lang/Exception;
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Failed to close writer!"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 203
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v4    # "modelWriter":Lcom/samsung/recognitionengine/UserModelStringWriter;
    :catch_3
    move-exception v0

    .line 204
    .restart local v0    # "ex":Ljava/lang/Exception;
    sget-object v6, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v7, "Failed to close writer!"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 199
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "fileWriter":Ljava/io/BufferedWriter;
    .restart local v2    # "fileWriter":Ljava/io/BufferedWriter;
    :catchall_1
    move-exception v6

    move-object v1, v2

    .end local v2    # "fileWriter":Ljava/io/BufferedWriter;
    .restart local v1    # "fileWriter":Ljava/io/BufferedWriter;
    goto :goto_3

    .line 196
    .end local v1    # "fileWriter":Ljava/io/BufferedWriter;
    .restart local v2    # "fileWriter":Ljava/io/BufferedWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fileWriter":Ljava/io/BufferedWriter;
    .restart local v1    # "fileWriter":Ljava/io/BufferedWriter;
    goto :goto_2
.end method

.method private trainFromFile()Z
    .locals 8

    .prologue
    .line 213
    const/4 v3, 0x0

    .line 214
    .local v3, "return_value":Z
    const/4 v1, 0x0

    .line 217
    .local v1, "reader":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
    :try_start_0
    new-instance v2, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mSignaturesFileName:Ljava/lang/String;

    const/4 v7, 0x2

    invoke-static {v7}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->createCipher(I)Ljavax/crypto/Cipher;

    move-result-object v7

    invoke-direct {v2, v6, v7}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;-><init>(Ljava/lang/String;Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 218
    .end local v1    # "reader":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
    .local v2, "reader":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
    :try_start_1
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;->read()Ljava/util/ArrayList;

    move-result-object v5

    .line 220
    .local v5, "signatures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/recognitionengine/Signature;>;"
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v7, 0x3

    if-lt v6, v7, :cond_0

    .line 221
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->createTrainer()V

    .line 223
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_3

    .line 227
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    invoke-virtual {v6}, Lcom/samsung/recognitionengine/Trainer;->trainModel()Lcom/samsung/recognitionengine/UserModel;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    .line 229
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-virtual {v6}, Lcom/samsung/recognitionengine/UserModel;->isValid()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    if-eqz v6, :cond_4

    .line 230
    const/4 v3, 0x1

    .line 238
    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    .line 239
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;->close()V

    :cond_1
    move-object v1, v2

    .line 243
    .end local v2    # "reader":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
    .end local v5    # "signatures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/recognitionengine/Signature;>;"
    .restart local v1    # "reader":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
    :cond_2
    :goto_2
    return v3

    .line 223
    .end local v1    # "reader":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
    .restart local v2    # "reader":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
    .restart local v5    # "signatures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/recognitionengine/Signature;>;"
    :cond_3
    :try_start_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/recognitionengine/Signature;

    .line 224
    .local v4, "s":Lcom/samsung/recognitionengine/Signature;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    invoke-virtual {v7, v4}, Lcom/samsung/recognitionengine/Trainer;->addSignature(Lcom/samsung/recognitionengine/Signature;)Lcom/samsung/recognitionengine/Trainer$EResult;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 235
    .end local v4    # "s":Lcom/samsung/recognitionengine/Signature;
    .end local v5    # "signatures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/recognitionengine/Signature;>;"
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 236
    .end local v2    # "reader":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
    .local v0, "ex":Ljava/lang/Exception;
    .restart local v1    # "reader":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
    :goto_3
    :try_start_3
    sget-object v6, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v7, ""

    invoke-static {v6, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 238
    if-eqz v1, :cond_2

    .line 239
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;->close()V

    goto :goto_2

    .line 232
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "reader":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
    .restart local v2    # "reader":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
    .restart local v5    # "signatures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/recognitionengine/Signature;>;"
    :cond_4
    const/4 v6, 0x0

    :try_start_4
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 237
    .end local v5    # "signatures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/recognitionengine/Signature;>;"
    :catchall_0
    move-exception v6

    move-object v1, v2

    .line 238
    .end local v2    # "reader":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
    .restart local v1    # "reader":Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
    :goto_4
    if-eqz v1, :cond_5

    .line 239
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;->close()V

    .line 241
    :cond_5
    throw v6

    .line 237
    :catchall_1
    move-exception v6

    goto :goto_4

    .line 235
    :catch_1
    move-exception v0

    goto :goto_3
.end method


# virtual methods
.method public declared-synchronized addSignature(Ljava/util/List;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "spenObjectStrokes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    monitor-enter p0

    const/4 v1, 0x0

    .line 101
    .local v1, "return_value":Z
    if-nez p1, :cond_1

    .line 102
    const/4 v1, 0x0

    .line 129
    .end local v1    # "return_value":Z
    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    .line 105
    .restart local v1    # "return_value":Z
    :cond_1
    :try_start_0
    iget v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->drop()V

    .line 109
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    if-nez v3, :cond_3

    .line 110
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->createTrainer()V

    .line 113
    :cond_3
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->convertSpenObjectStrokesToSignature(Ljava/util/List;)Lcom/samsung/recognitionengine/Signature;

    move-result-object v2

    .line 115
    .local v2, "sig":Lcom/samsung/recognitionengine/Signature;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    invoke-virtual {v3, v2}, Lcom/samsung/recognitionengine/Trainer;->addSignature(Lcom/samsung/recognitionengine/Signature;)Lcom/samsung/recognitionengine/Trainer$EResult;

    move-result-object v0

    .line 117
    .local v0, "res":Lcom/samsung/recognitionengine/Trainer$EResult;
    sget-object v3, Lcom/samsung/recognitionengine/Trainer$EResult;->RES_OK:Lcom/samsung/recognitionengine/Trainer$EResult;

    if-ne v0, v3, :cond_4

    .line 118
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->appendSignatureToFile(Lcom/samsung/recognitionengine/Signature;)Z

    .line 121
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    invoke-virtual {v3}, Lcom/samsung/recognitionengine/Trainer;->getSignaturesNumber()J

    move-result-wide v4

    const-wide/16 v6, 0x3

    cmp-long v3, v4, v6

    if-ltz v3, :cond_0

    .line 122
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    invoke-virtual {v3}, Lcom/samsung/recognitionengine/Trainer;->trainModel()Lcom/samsung/recognitionengine/UserModel;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    .line 123
    const/4 v3, 0x3

    iput v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I

    .line 124
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-virtual {v3}, Lcom/samsung/recognitionengine/UserModel;->isValid()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 125
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->saveModelToFile()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    .end local v0    # "res":Lcom/samsung/recognitionengine/Trainer$EResult;
    .end local v2    # "sig":Lcom/samsung/recognitionengine/Signature;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized drop()V
    .locals 1

    .prologue
    .line 133
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I

    .line 136
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModelFileName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->dropFile(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mSignaturesFileName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->dropFile(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    monitor-exit p0

    return-void

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSignaturesNumber()I
    .locals 2

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-virtual {v0}, Lcom/samsung/recognitionengine/UserModel;->getSignaturesNumber()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    long-to-int v0, v0

    .line 87
    :goto_0
    monitor-exit p0

    return v0

    .line 83
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    invoke-virtual {v0}, Lcom/samsung/recognitionengine/Trainer;->getSignaturesNumber()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0

    .line 87
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getState()I
    .locals 1

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getVerifier()Lcom/samsung/recognitionengine/Verifier;
    .locals 2

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-virtual {v0}, Lcom/samsung/recognitionengine/UserModel;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    new-instance v0, Lcom/samsung/recognitionengine/Verifier;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-direct {v0, v1}, Lcom/samsung/recognitionengine/Verifier;-><init>(Lcom/samsung/recognitionengine/UserModel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
