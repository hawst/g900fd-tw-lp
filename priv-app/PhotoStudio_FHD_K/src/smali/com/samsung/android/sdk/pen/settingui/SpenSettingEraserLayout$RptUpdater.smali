.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;
.super Ljava/lang/Object;
.source "SpenSettingEraserLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RptUpdater"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)V
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x14

    const/high16 v2, 0x3f800000    # 1.0f

    .line 520
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mAutoIncrement:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 521
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 522
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mCurrentEraserType:I

    aget-object v0, v0, v1

    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    add-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 523
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    .line 524
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mCurrentEraserType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 526
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 536
    :cond_1
    :goto_0
    return-void

    .line 527
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mAutoDecrement:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 528
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    if-eqz v0, :cond_3

    .line 529
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mCurrentEraserType:I

    aget-object v0, v0, v1

    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    sub-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 530
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    .line 532
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mCurrentEraserType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 534
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$RptUpdater;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
