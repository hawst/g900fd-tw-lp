.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;
.super Ljava/lang/Object;
.source "SpenTextBox.java"

# interfaces
.implements Landroid/text/SpanWatcher;
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChangeWatcher"
.end annotation


# instance fields
.field private mBeforeText:Ljava/lang/CharSequence;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 6763
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;)V
    .locals 0

    .prologue
    .line 6763
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1, "buffer"    # Landroid/text/Editable;

    .prologue
    .line 6871
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->startBlink()V

    .line 6872
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "buffer"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "after"    # I

    .prologue
    .line 6770
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 6771
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->stopBlink()V

    .line 6773
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$53(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->startInput()V

    .line 6774
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 6776
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initMeasureInfo()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$54(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 6778
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$55(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6779
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->mBeforeText:Ljava/lang/CharSequence;

    .line 6781
    :cond_0
    return-void
.end method

.method public onSpanAdded(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .locals 0
    .param p1, "buf"    # Landroid/text/Spannable;
    .param p2, "what"    # Ljava/lang/Object;
    .param p3, "s"    # I
    .param p4, "e"    # I

    .prologue
    .line 6880
    return-void
.end method

.method public onSpanChanged(Landroid/text/Spannable;Ljava/lang/Object;IIII)V
    .locals 0
    .param p1, "buf"    # Landroid/text/Spannable;
    .param p2, "what"    # Ljava/lang/Object;
    .param p3, "s"    # I
    .param p4, "e"    # I
    .param p5, "st"    # I
    .param p6, "en"    # I

    .prologue
    .line 6876
    return-void
.end method

.method public onSpanRemoved(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .locals 0
    .param p1, "buf"    # Landroid/text/Spannable;
    .param p2, "what"    # Ljava/lang/Object;
    .param p3, "s"    # I
    .param p4, "e"    # I

    .prologue
    .line 6884
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 8
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "after"    # I

    .prologue
    const/4 v7, 0x1

    .line 6786
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$55(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShown()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 6787
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->mBeforeText:Ljava/lang/CharSequence;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->sendAccessibilityEventTypeViewTextChanged(Ljava/lang/CharSequence;III)V
    invoke-static {v5, v6, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$56(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Ljava/lang/CharSequence;III)V

    .line 6788
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->mBeforeText:Ljava/lang/CharSequence;

    .line 6791
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v5

    if-nez v5, :cond_3

    .line 6867
    :cond_2
    :goto_0
    return-void

    .line 6795
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 6797
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$57(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 6798
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 6799
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 6802
    :cond_4
    add-int v5, p2, p4

    invoke-interface {p1, p2, v5}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 6806
    .local v4, "str":Ljava/lang/String;
    if-eqz p3, :cond_d

    .line 6807
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    .line 6808
    .local v3, "objectString":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 6812
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v5, v7, :cond_b

    .line 6813
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCommitText:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$58(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsComposingText:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$59(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$60(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSetEmptyText:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$61(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 6815
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->removeText(II)V

    .line 6816
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$62(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 6817
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$23(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 6839
    .end local v3    # "objectString":Ljava/lang/String;
    :cond_6
    :goto_1
    if-eqz p4, :cond_7

    .line 6840
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 6843
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$65(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 6844
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V
    :try_end_0
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6851
    .end local v4    # "str":Ljava/lang/String;
    :cond_8
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    .line 6852
    .local v0, "desctription":Ljava/lang/String;
    if-nez v0, :cond_9

    .line 6853
    const-string v0, ""

    .line 6855
    :cond_9
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "TextBox "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 6857
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$66(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 6858
    const-string v6, "default_input_method"

    .line 6857
    invoke-static {v5, v6}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 6860
    .local v2, "inputMethodId":Ljava/lang/String;
    const-string v5, "com.google.android.googlequicksearchbox/com.google.android.voicesearch.ime.VoiceInputMethodService"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 6861
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v5, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$67(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V

    .line 6862
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    add-int v6, p2, p4

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$68(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V

    .line 6863
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showDeletePopupWindow()V
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$69(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    goto/16 :goto_0

    .line 6820
    .end local v0    # "desctription":Ljava/lang/String;
    .end local v2    # "inputMethodId":Ljava/lang/String;
    .restart local v3    # "objectString":Ljava/lang/String;
    .restart local v4    # "str":Ljava/lang/String;
    :cond_a
    :try_start_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$63(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v6

    add-int/2addr v6, p3

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$64(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    :try_end_1
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 6847
    .end local v3    # "objectString":Ljava/lang/String;
    .end local v4    # "str":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 6848
    .local v1, "e":Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;->printStackTrace()V

    goto :goto_2

    .line 6823
    .end local v1    # "e":Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;
    .restart local v3    # "objectString":Ljava/lang/String;
    .restart local v4    # "str":Ljava/lang/String;
    :cond_b
    :try_start_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$63(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v5

    if-nez v5, :cond_c

    .line 6824
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v5

    invoke-virtual {v5, v4, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->replaceText(Ljava/lang/String;II)V

    goto/16 :goto_1

    .line 6826
    :cond_c
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$63(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v6

    invoke-virtual {v5, v4, p2, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->replaceText(Ljava/lang/String;II)V

    .line 6827
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$64(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V

    goto/16 :goto_1

    .line 6831
    .end local v3    # "objectString":Ljava/lang/String;
    :cond_d
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$63(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v5

    if-nez v5, :cond_e

    .line 6832
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->insertTextAtCursor(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 6834
    :cond_e
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$63(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v6

    invoke-virtual {v5, v4, p2, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->replaceText(Ljava/lang/String;II)V

    .line 6835
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$64(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    :try_end_2
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 6865
    .end local v4    # "str":Ljava/lang/String;
    .restart local v0    # "desctription":Ljava/lang/String;
    .restart local v2    # "inputMethodId":Ljava/lang/String;
    :cond_f
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeleteTextPopup()V
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    goto/16 :goto_0
.end method
