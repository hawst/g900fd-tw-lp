.class Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SPenFontNameDropdown.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    .line 55
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->items:Ljava/util/List;

    .line 56
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->items:Ljava/util/List;

    .line 57
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->context:Landroid/content/Context;

    .line 58
    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v7, 0x13

    const/high16 v6, 0x40c00000    # 6.0f

    const/4 v5, 0x0

    .line 63
    if-nez p2, :cond_0

    .line 64
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 65
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x1090008

    invoke-virtual {v0, v3, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 67
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v3, 0x1020014

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 69
    .local v2, "tv":Landroid/widget/TextView;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 70
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 71
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 73
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 74
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 75
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v2, v3, v5, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 76
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x42140000    # 37.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 78
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v1

    .line 79
    .local v1, "tf":Landroid/graphics/Typeface;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 81
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSdkVersion:I

    if-lt v3, v7, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_2

    .line 82
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const-string v4, ""

    const-string v5, "tw_spinner_list_pressed_holo_light"

    .line 83
    const-string v6, "tw_spinner_list_focused_holo_light"

    .line 82
    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 86
    :cond_2
    return-object p2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v7, 0x13

    const/high16 v6, 0x40c00000    # 6.0f

    const/4 v5, 0x0

    .line 93
    if-nez p2, :cond_0

    .line 94
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 96
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x1090008

    invoke-virtual {v0, v3, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 98
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v3, 0x1020014

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 100
    .local v2, "tv":Landroid/widget/TextView;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 101
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 102
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 104
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 105
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 106
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v2, v3, v5, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 107
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x42140000    # 37.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 114
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v1

    .line 115
    .local v1, "tf":Landroid/graphics/Typeface;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 117
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSdkVersion:I

    if-lt v3, v7, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_2

    .line 118
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const-string v4, ""

    const-string v5, "tw_spinner_list_pressed_holo_light"

    .line 119
    const-string v6, "tw_spinner_list_focused_holo_light"

    .line 118
    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 121
    :cond_2
    return-object p2
.end method
