.class Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;
.super Ljava/lang/Object;
.source "SpenView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewUpdateCanvasListener"
.end annotation


# instance fields
.field mView:Lcom/samsung/android/sdk/pen/engine/SpenView;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenView;Lcom/samsung/android/sdk/pen/engine/SpenView;)V
    .locals 0
    .param p2, "view"    # Lcom/samsung/android/sdk/pen/engine/SpenView;

    .prologue
    .line 2889
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2890
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->mView:Lcom/samsung/android/sdk/pen/engine/SpenView;

    .line 2891
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 2894
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->mView:Lcom/samsung/android/sdk/pen/engine/SpenView;

    .line 2895
    return-void
.end method

.method public onUpdateCanvas(Landroid/graphics/RectF;Z)V
    .locals 6
    .param p1, "rectf"    # Landroid/graphics/RectF;
    .param p2, "isScreenFramebuffer"    # Z

    .prologue
    .line 2899
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;

    invoke-static {v1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenView;Z)V

    .line 2900
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenView;->mThreadId:J
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenView;)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2901
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenView;->invalidate()V

    .line 2926
    :goto_0
    return-void

    .line 2903
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenView;->postInvalidate()V

    .line 2906
    const-wide/16 v2, 0xf

    const/4 v1, 0x0

    :try_start_0
    invoke-static {v2, v3, v1}, Ljava/lang/Thread;->sleep(JI)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2907
    :catch_0
    move-exception v0

    .line 2909
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
