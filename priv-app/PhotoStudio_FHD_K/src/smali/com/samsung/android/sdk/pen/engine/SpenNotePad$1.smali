.class Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;
.super Ljava/lang/Object;
.source "SpenNotePad.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenNotePad;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdate(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Landroid/view/MotionEvent;)V
    .locals 12
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 216
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    .line 217
    .local v1, "point":Landroid/graphics/PointF;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    .line 218
    .local v8, "action":I
    new-instance v9, Landroid/graphics/RectF;

    invoke-direct {v9}, Landroid/graphics/RectF;-><init>()V

    .line 219
    .local v9, "dstRect":Landroid/graphics/RectF;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRectF()Landroid/graphics/RectF;

    move-result-object v2

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    invoke-static {v0, v9, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 221
    const/4 v6, 0x0

    .local v6, "a":I
    :goto_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v0

    if-lt v6, v0, :cond_6

    .line 236
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getBoxHeight()F

    move-result v2

    div-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    mul-float v10, v0, v2

    .line 237
    .local v10, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getBoxHeight()F

    move-result v2

    div-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    mul-float v11, v0, v2

    .line 238
    .local v11, "y":F
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v0

    mul-float/2addr v0, v10

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    add-float v10, v0, v2

    .line 239
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v0

    mul-float/2addr v0, v11

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    add-float v11, v0, v2

    .line 241
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v0

    div-float v0, v10, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    add-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    div-float v2, v11, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    add-float/2addr v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 242
    iget v0, v1, Landroid/graphics/PointF;->x:F

    iget v2, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v9, v0, v2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    instance-of v0, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 244
    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPressure()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    long-to-int v3, v4

    .line 245
    const/16 v4, 0x19

    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getOrientation()F

    move-result v5

    .line 244
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->addPoint(Landroid/graphics/PointF;FIFF)V

    .line 249
    :cond_0
    const/4 v0, 0x1

    if-eq v8, v0, :cond_1

    const/4 v0, 0x3

    if-ne v8, v0, :cond_2

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 251
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onUpdate(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 262
    :cond_2
    if-nez v8, :cond_3

    .line 263
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->setMovingEnabled(ZF)V

    .line 264
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->removeMessages(I)V

    .line 266
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    div-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    mul-float v7, v0, v2

    .line 267
    .local v7, "absoluteX":F
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v2

    cmpg-float v0, v0, v7

    if-gez v0, :cond_4

    .line 268
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->setMovingEnabled(ZF)V

    .line 270
    :cond_4
    const/4 v0, 0x1

    if-ne v8, v0, :cond_5

    .line 271
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->isMovingEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 272
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    move-result-object v0

    const/4 v2, 0x1

    const-wide/16 v4, 0x258

    invoke-virtual {v0, v2, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 275
    :cond_5
    return-void

    .line 222
    .end local v7    # "absoluteX":F
    .end local v10    # "x":F
    .end local v11    # "y":F
    :cond_6
    invoke-virtual {p2, v6}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getBoxHeight()F

    move-result v2

    div-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    .line 223
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    .line 222
    mul-float v10, v0, v2

    .line 224
    .restart local v10    # "x":F
    invoke-virtual {p2, v6}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getBoxHeight()F

    move-result v2

    div-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    .line 225
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    .line 224
    mul-float v11, v0, v2

    .line 226
    .restart local v11    # "y":F
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v0

    mul-float/2addr v0, v10

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    add-float v10, v0, v2

    .line 227
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v0

    mul-float/2addr v0, v11

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    add-float v11, v0, v2

    .line 229
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v0

    div-float v0, v10, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    add-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    div-float v2, v11, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    add-float/2addr v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 230
    iget v0, v1, Landroid/graphics/PointF;->x:F

    iget v2, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v9, v0, v2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v0, p1

    .line 231
    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {p2, v6}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v2

    .line 232
    invoke-virtual {p2, v6}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v4

    long-to-int v3, v4

    .line 233
    const/16 v4, 0x19

    invoke-virtual {p2, v4, v6}, Landroid/view/MotionEvent;->getHistoricalAxisValue(II)F

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getOrientation()F

    move-result v5

    .line 231
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->addPoint(Landroid/graphics/PointF;FIFF)V

    .line 221
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0
.end method
