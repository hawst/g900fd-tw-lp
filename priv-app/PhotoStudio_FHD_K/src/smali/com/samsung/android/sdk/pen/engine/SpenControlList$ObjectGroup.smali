.class Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
.super Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.source "SpenControlList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ObjectGroup"
.end annotation


# instance fields
.field private mIsFirstTimeSelectListObj:Z

.field private mObjectList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end field

.field private mRotation:F

.field private final mUnionRect:Landroid/graphics/RectF;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)V
    .locals 1

    .prologue
    .line 276
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    .line 277
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mRotation:F

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mIsFirstTimeSelectListObj:Z

    .line 278
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    .line 279
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)F
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mRotation:F

    return v0
.end method


# virtual methods
.method public appendObject(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 512
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    .line 513
    return-void
.end method

.method public clearChangedFlag()V
    .locals 0

    .prologue
    .line 245
    return-void
.end method

.method public copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 0
    .param p1, "base"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 264
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 268
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDrawnRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExtraDataByteArray(Ljava/lang/String;)[B
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 168
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExtraDataInt(Ljava/lang/String;)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 150
    const/4 v0, -0x1

    return v0
.end method

.method public getExtraDataString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 141
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 159
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMinHeight()F
    .locals 5

    .prologue
    .line 617
    const/4 v1, 0x0

    .line 619
    .local v1, "min":F
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 629
    return v1

    .line 619
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 620
    .local v2, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 621
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinHeight()F

    move-result v0

    .line 623
    .local v0, "curMin":F
    const/4 v4, 0x0

    cmpl-float v4, v1, v4

    if-eqz v4, :cond_2

    cmpg-float v4, v1, v0

    if-gez v4, :cond_0

    .line 624
    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public getMinWidth()F
    .locals 5

    .prologue
    .line 600
    const/4 v1, 0x0

    .line 602
    .local v1, "min":F
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 612
    return v1

    .line 602
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 603
    .local v2, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 604
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinWidth()F

    move-result v0

    .line 606
    .local v0, "curMin":F
    const/4 v4, 0x0

    cmpl-float v4, v1, v4

    if-eqz v4, :cond_2

    cmpg-float v4, v1, v0

    if-gez v4, :cond_0

    .line 607
    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public getObjectList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 516
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRect()Landroid/graphics/RectF;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 317
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 319
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v6, :cond_0

    .line 320
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 323
    :cond_0
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mIsFirstTimeSelectListObj:Z

    if-nez v6, :cond_1

    .line 324
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_2

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mRotation:F

    sub-float/2addr v6, v8

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const v7, 0x38d1b717    # 1.0E-4f

    cmpg-float v6, v6, v7

    if-gez v6, :cond_2

    .line 325
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v6, v8, v8, v8, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 327
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v2, v6, :cond_3

    .line 336
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mIsFirstTimeSelectListObj:Z

    .line 338
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    .line 339
    .local v4, "tmpAbsoluteObjRect":Landroid/graphics/RectF;
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    .line 340
    .local v5, "tmpRelativeObjRect":Landroid/graphics/RectF;
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v7

    invoke-virtual {v6, v5, v4, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 342
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Landroid/graphics/PointF;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    sub-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 343
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Landroid/graphics/PointF;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->centerY()F

    move-result v7

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    sub-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 345
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Landroid/graphics/PointF;

    move-result-object v6

    iget v7, v6, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v8

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 346
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Landroid/graphics/PointF;

    move-result-object v6

    iget v7, v6, Landroid/graphics/PointF;->y:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v8

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 348
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V

    .line 349
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V

    .line 351
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectWidth:F
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)F

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v8

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v7, v8

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V

    .line 352
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectHeight:F
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)F

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v8

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v7, v8

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V

    .line 355
    .end local v2    # "i":I
    .end local v4    # "tmpAbsoluteObjRect":Landroid/graphics/RectF;
    .end local v5    # "tmpRelativeObjRect":Landroid/graphics/RectF;
    :cond_2
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 356
    .local v0, "absoluteRect":Landroid/graphics/RectF;
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v8

    invoke-virtual {v6, v0, v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 358
    return-object v0

    .line 328
    .end local v0    # "absoluteRect":Landroid/graphics/RectF;
    .restart local v2    # "i":I
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 329
    .local v3, "rect":Landroid/graphics/RectF;
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v1

    .line 331
    .local v1, "degree":F
    if-eqz v3, :cond_4

    .line 332
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    invoke-virtual {v7, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->getBoundBoxObject(Landroid/graphics/RectF;F)Landroid/graphics/RectF;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 327
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method

.method public getResizeOption()I
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 306
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 312
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 307
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v1

    if-ne v1, v2, :cond_1

    move v1, v2

    .line 308
    goto :goto_1

    .line 306
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getRotation()F
    .locals 1

    .prologue
    .line 283
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mRotation:F

    return v0
.end method

.method public getRuntimeHandle()I
    .locals 1

    .prologue
    .line 254
    const/4 v0, -0x1

    return v0
.end method

.method public getSorDataInt(Ljava/lang/String;)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 222
    const/4 v0, -0x1

    return v0
.end method

.method public getSorDataString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 213
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSorInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSorPackageLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x4

    return v0
.end method

.method public hasExtraDataByteArray(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 188
    const/4 v0, 0x0

    return v0
.end method

.method public hasExtraDataInt(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public hasExtraDataString(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 173
    const/4 v0, 0x0

    return v0
.end method

.method public hasExtraDataStringArray(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 183
    const/4 v0, 0x0

    return v0
.end method

.method public hasSorDataInt(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 232
    const/4 v0, 0x0

    return v0
.end method

.method public hasSorDataString(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 227
    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 273
    const/4 v0, -0x1

    return v0
.end method

.method public isChanged()Z
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x1

    return v0
.end method

.method public isMovable()Z
    .locals 3

    .prologue
    .line 109
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 114
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 109
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 110
    .local v0, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isMovable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 111
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isOutOfViewEnabled()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x1

    return v0
.end method

.method public isRecorded()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public isRotatable()Z
    .locals 2

    .prologue
    .line 292
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 297
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 293
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 294
    const/4 v1, 0x0

    goto :goto_1

    .line 292
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public isSelectable()Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method

.method public removeExtraDataByteArray(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 205
    return-void
.end method

.method public removeExtraDataInt(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 197
    return-void
.end method

.method public removeExtraDataString(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 193
    return-void
.end method

.method public removeExtraDataStringArray(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 201
    return-void
.end method

.method public removeSorDataInt(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 241
    return-void
.end method

.method public removeSorDataString(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 237
    return-void
.end method

.method public setExtraDataByteArray(Ljava/lang/String;[B)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 164
    return-void
.end method

.method public setExtraDataInt(Ljava/lang/String;I)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 146
    return-void
.end method

.method public setExtraDataString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 137
    return-void
.end method

.method public setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [Ljava/lang/String;

    .prologue
    .line 155
    return-void
.end method

.method public setMovable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 105
    return-void
.end method

.method public setOutOfViewEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 87
    return-void
.end method

.method public setRecorded(Z)V
    .locals 0
    .param p1, "record"    # Z

    .prologue
    .line 69
    return-void
.end method

.method public setRect(Landroid/graphics/RectF;Z)V
    .locals 29
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "regionOnly"    # Z

    .prologue
    .line 364
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v25, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v26, v0

    cmpl-float v25, v25, v26

    if-eqz v25, :cond_0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    cmpl-float v25, v25, v26

    if-nez v25, :cond_1

    .line 509
    :cond_0
    :goto_0
    return-void

    .line 368
    :cond_1
    const/4 v7, 0x0

    .line 369
    .local v7, "flipHorizon":Z
    const/4 v8, 0x0

    .line 371
    .local v8, "flipVertical":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static/range {v25 .. v25}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 373
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v25, v0

    if-eqz v25, :cond_2

    .line 374
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v26, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static/range {v26 .. v26}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v26

    invoke-interface/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 377
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->getRect()Landroid/graphics/RectF;

    move-result-object v5

    .line 378
    .local v5, "absolutePrevRect":Landroid/graphics/RectF;
    new-instance v16, Landroid/graphics/RectF;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/RectF;-><init>()V

    .line 380
    .local v16, "prevRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v26, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static/range {v26 .. v26}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v5, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 382
    new-instance v4, Landroid/graphics/RectF;

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v27, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v28, v0

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 383
    .local v4, "absoluteNextRect":Landroid/graphics/RectF;
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    .line 385
    .local v13, "nextRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v26, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static/range {v26 .. v26}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v0, v13, v4, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 387
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v26, v0

    sub-float v25, v25, v26

    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    move-result v25

    const v26, 0x38d1b717    # 1.0E-4f

    cmpg-float v25, v25, v26

    if-gez v25, :cond_3

    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    sub-float v25, v25, v26

    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    move-result v25

    const v26, 0x38d1b717    # 1.0E-4f

    cmpg-float v25, v25, v26

    if-gez v25, :cond_3

    .line 388
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v25, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v26, v0

    sub-float v25, v25, v26

    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    move-result v25

    const v26, 0x38d1b717    # 1.0E-4f

    cmpg-float v25, v25, v26

    if-gez v25, :cond_3

    .line 389
    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v26, v0

    sub-float v25, v25, v26

    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    move-result v25

    const v26, 0x38d1b717    # 1.0E-4f

    cmpg-float v25, v25, v26

    if-ltz v25, :cond_0

    .line 393
    :cond_3
    const/4 v11, 0x0

    .line 395
    .local v11, "isResize":Z
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v25

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->width()F

    move-result v26

    sub-float v25, v25, v26

    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    move-result v25

    const v26, 0x38d1b717    # 1.0E-4f

    cmpl-float v25, v25, v26

    if-gtz v25, :cond_4

    .line 396
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v25

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->height()F

    move-result v26

    sub-float v25, v25, v26

    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    move-result v25

    const v26, 0x38d1b717    # 1.0E-4f

    cmpl-float v25, v25, v26

    if-lez v25, :cond_5

    .line 397
    :cond_4
    const/4 v11, 0x1

    .line 400
    :cond_5
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v26, v0

    cmpl-float v25, v25, v26

    if-lez v25, :cond_6

    .line 401
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    .line 402
    .local v19, "temp":F
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v25, v0

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/RectF;->left:F

    .line 403
    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->right:F

    .line 404
    const/4 v7, 0x1

    .line 407
    .end local v19    # "temp":F
    :cond_6
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v26, v0

    cmpl-float v25, v25, v26

    if-lez v25, :cond_7

    .line 408
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    .line 409
    .restart local v19    # "temp":F
    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/RectF;->top:F

    .line 410
    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->bottom:F

    .line 411
    const/4 v8, 0x1

    .line 414
    .end local v19    # "temp":F
    :cond_7
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v26, v0

    cmpl-float v25, v25, v26

    if-nez v25, :cond_8

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    cmpl-float v25, v25, v26

    if-nez v25, :cond_8

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v25, v0

    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v26, v0

    cmpl-float v25, v25, v26

    if-nez v25, :cond_8

    .line 415
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v26, v0

    cmpl-float v25, v25, v26

    if-nez v25, :cond_8

    if-nez v7, :cond_8

    if-eqz v8, :cond_0

    .line 419
    :cond_8
    if-nez p2, :cond_b

    .line 420
    const/16 v18, 0x0

    .line 421
    .local v18, "rw":F
    const/16 v17, 0x0

    .line 423
    .local v17, "rh":F
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v25, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v26, v0

    cmpl-float v25, v25, v26

    if-eqz v25, :cond_9

    .line 424
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v25, v0

    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v26, v0

    sub-float v25, v25, v26

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v26, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    div-float v18, v25, v26

    .line 427
    :cond_9
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    cmpl-float v25, v25, v26

    if-eqz v25, :cond_a

    .line 428
    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    sub-float v25, v25, v26

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v26, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    div-float v17, v25, v26

    .line 433
    :cond_a
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 439
    .local v15, "objectRect":Landroid/graphics/RectF;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    move/from16 v0, v25

    if-lt v10, v0, :cond_d

    .line 489
    .end local v10    # "i":I
    .end local v15    # "objectRect":Landroid/graphics/RectF;
    .end local v17    # "rh":F
    .end local v18    # "rw":F
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 491
    if-eqz v11, :cond_c

    .line 492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v20

    .line 493
    .local v20, "tmpAbsoluteObjRect":Landroid/graphics/RectF;
    new-instance v21, Landroid/graphics/RectF;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/RectF;-><init>()V

    .line 494
    .local v21, "tmpRelativeObjRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v26, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static/range {v26 .. v26}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    move-object/from16 v2, v20

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 496
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;
    invoke-static/range {v25 .. v25}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Landroid/graphics/PointF;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/RectF;->centerX()F

    move-result v26

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/RectF;->centerX()F

    move-result v27

    sub-float v26, v26, v27

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 497
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;
    invoke-static/range {v25 .. v25}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Landroid/graphics/PointF;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/RectF;->centerY()F

    move-result v26

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/RectF;->centerY()F

    move-result v27

    sub-float v26, v26, v27

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 499
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;
    invoke-static/range {v25 .. v25}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Landroid/graphics/PointF;

    move-result-object v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v27, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static/range {v27 .. v27}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v27

    move-object/from16 v0, v27

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v27, v0

    div-float v26, v26, v27

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 500
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;
    invoke-static/range {v25 .. v25}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Landroid/graphics/PointF;

    move-result-object v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v27, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static/range {v27 .. v27}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v27

    move-object/from16 v0, v27

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v27, v0

    div-float v26, v26, v27

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 502
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/RectF;->width()F

    move-result v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V

    .line 503
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/RectF;->height()F

    move-result v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V

    .line 505
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectWidth:F
    invoke-static/range {v25 .. v25}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)F

    move-result v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v27, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static/range {v27 .. v27}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v27

    move-object/from16 v0, v27

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v27, v0

    div-float v26, v26, v27

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V

    .line 506
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectHeight:F
    invoke-static/range {v25 .. v25}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)F

    move-result v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v27, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static/range {v27 .. v27}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v27

    move-object/from16 v0, v27

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v27, v0

    div-float v26, v26, v27

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V

    .line 508
    .end local v20    # "tmpAbsoluteObjRect":Landroid/graphics/RectF;
    .end local v21    # "tmpRelativeObjRect":Landroid/graphics/RectF;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-super {v0, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    goto/16 :goto_0

    .line 440
    .restart local v10    # "i":I
    .restart local v15    # "objectRect":Landroid/graphics/RectF;
    .restart local v17    # "rh":F
    .restart local v18    # "rw":F
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 441
    .local v14, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v6

    .line 443
    .local v6, "absoluteRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v26, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static/range {v26 .. v26}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v0, v15, v6, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 445
    iget v0, v15, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v26, v0

    sub-float v23, v25, v26

    .line 446
    .local v23, "x":F
    iget v0, v15, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    sub-float v24, v25, v26

    .line 447
    .local v24, "y":F
    iget v0, v15, Landroid/graphics/RectF;->right:F

    move/from16 v25, v0

    iget v0, v15, Landroid/graphics/RectF;->left:F

    move/from16 v26, v0

    sub-float v22, v25, v26

    .line 448
    .local v22, "w":F
    iget v0, v15, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    iget v0, v15, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    sub-float v9, v25, v26

    .line 450
    .local v9, "h":F
    mul-float v23, v23, v18

    .line 451
    mul-float v24, v24, v17

    .line 452
    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v25

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_e

    .line 453
    mul-float v22, v22, v18

    .line 454
    mul-float v9, v9, v17

    .line 457
    :cond_e
    if-eqz v7, :cond_f

    .line 458
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v25, v0

    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v26, v0

    sub-float v25, v25, v26

    sub-float v25, v25, v23

    sub-float v23, v25, v22

    .line 461
    :cond_f
    if-eqz v8, :cond_10

    .line 462
    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    sub-float v25, v25, v26

    sub-float v25, v25, v24

    sub-float v24, v25, v9

    .line 465
    :cond_10
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    add-float v25, v25, v23

    move/from16 v0, v25

    iput v0, v15, Landroid/graphics/RectF;->left:F

    .line 466
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    add-float v25, v25, v24

    move/from16 v0, v25

    iput v0, v15, Landroid/graphics/RectF;->top:F

    .line 467
    iget v0, v15, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    add-float v25, v25, v22

    move/from16 v0, v25

    iput v0, v15, Landroid/graphics/RectF;->right:F

    .line 468
    iget v0, v15, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    add-float v25, v25, v9

    move/from16 v0, v25

    iput v0, v15, Landroid/graphics/RectF;->bottom:F

    .line 470
    if-eqz v8, :cond_11

    .line 471
    iget v0, v15, Landroid/graphics/RectF;->bottom:F

    move/from16 v19, v0

    .line 472
    .restart local v19    # "temp":F
    iget v0, v15, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    move/from16 v0, v25

    iput v0, v15, Landroid/graphics/RectF;->bottom:F

    .line 473
    move/from16 v0, v19

    iput v0, v15, Landroid/graphics/RectF;->top:F

    .line 476
    .end local v19    # "temp":F
    :cond_11
    if-eqz v7, :cond_12

    .line 477
    iget v0, v15, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    .line 478
    .restart local v19    # "temp":F
    iget v0, v15, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    move/from16 v0, v25

    iput v0, v15, Landroid/graphics/RectF;->right:F

    .line 479
    move/from16 v0, v19

    iput v0, v15, Landroid/graphics/RectF;->left:F

    .line 482
    .end local v19    # "temp":F
    :cond_12
    new-instance v12, Landroid/graphics/RectF;

    invoke-direct {v12}, Landroid/graphics/RectF;-><init>()V

    .line 483
    .local v12, "newAbsoluteRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v26, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static/range {v26 .. v26}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v0, v12, v15, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 485
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v14, v12, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 439
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1
.end method

.method public setResizeOption(I)V
    .locals 0
    .param p1, "option"    # I

    .prologue
    .line 302
    return-void
.end method

.method public setRotatable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 288
    return-void
.end method

.method public setRotation(F)V
    .locals 29
    .param p1, "degree"    # F

    .prologue
    .line 521
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 522
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mRotation:F

    move/from16 v16, v0

    .line 523
    .local v16, "curDegree":F
    sub-float v19, p1, v16

    .line 525
    .local v19, "moveDegree":F
    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v3, 0x38d1b717    # 1.0E-4f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 596
    .end local v16    # "curDegree":F
    .end local v19    # "moveDegree":F
    :cond_0
    :goto_0
    return-void

    .line 529
    .restart local v16    # "curDegree":F
    .restart local v19    # "moveDegree":F
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 531
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v2, :cond_2

    .line 532
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 536
    :cond_2
    new-instance v18, Landroid/graphics/RectF;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/RectF;-><init>()V

    .line 537
    .local v18, "currRelativeObjRect":Landroid/graphics/RectF;
    new-instance v20, Landroid/graphics/RectF;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/RectF;-><init>()V

    .line 538
    .local v20, "nextAbsoluteObjRect":Landroid/graphics/RectF;
    new-instance v22, Landroid/graphics/RectF;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/RectF;-><init>()V

    .line 540
    .local v22, "nextRelativeObjRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v27

    .line 541
    .local v27, "unionRectCenterX":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    .line 545
    .local v8, "unionRectCenterY":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :goto_1
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 578
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mRotation:F

    .line 580
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v24

    .line 581
    .local v24, "tmpAbsoluteObjRect":Landroid/graphics/RectF;
    new-instance v25, Landroid/graphics/RectF;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/RectF;-><init>()V

    .line 582
    .local v25, "tmpRelativeObjRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v3

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v2, v0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 584
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Landroid/graphics/PointF;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    sub-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/PointF;->x:F

    .line 585
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Landroid/graphics/PointF;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    sub-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/PointF;->y:F

    .line 587
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Landroid/graphics/PointF;

    move-result-object v2

    iget v3, v2, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v4

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/PointF;->x:F

    .line 588
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Landroid/graphics/PointF;

    move-result-object v2

    iget v3, v2, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v4

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/PointF;->y:F

    .line 590
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V

    .line 591
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V

    .line 593
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectWidth:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v4

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v3, v4

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V

    .line 594
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectHeight:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v4

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v3, v4

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V

    goto/16 :goto_0

    .line 545
    .end local v24    # "tmpAbsoluteObjRect":Landroid/graphics/RectF;
    .end local v25    # "tmpRelativeObjRect":Landroid/graphics/RectF;
    :cond_3
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 546
    .local v23, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v5

    .line 547
    .local v5, "prevObjDegree":F
    add-float v21, v5, v19

    .line 549
    .local v21, "nextObjDegree":F
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v17

    .line 550
    .local v17, "currAbsoluteObjRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v3

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v2, v0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 552
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    .line 553
    move-object/from16 v0, v18

    iget v6, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v18

    iget v7, v0, Landroid/graphics/RectF;->top:F

    .line 552
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->rotatePoint(FFFFF)Landroid/graphics/PointF;
    invoke-static/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenControlList;FFFFF)Landroid/graphics/PointF;

    move-result-object v26

    .line 554
    .local v26, "topLeft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    .line 555
    move-object/from16 v0, v18

    iget v6, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, v18

    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    .line 554
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->rotatePoint(FFFFF)Landroid/graphics/PointF;
    invoke-static/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenControlList;FFFFF)Landroid/graphics/PointF;

    move-result-object v15

    .line 557
    .local v15, "bottomRight":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    neg-float v9, v5

    move-object/from16 v0, v26

    iget v10, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v26

    iget v11, v0, Landroid/graphics/PointF;->y:F

    move/from16 v7, v27

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->rotatePoint(FFFFF)Landroid/graphics/PointF;
    invoke-static/range {v6 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenControlList;FFFFF)Landroid/graphics/PointF;

    move-result-object v26

    .line 558
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    neg-float v9, v5

    iget v10, v15, Landroid/graphics/PointF;->x:F

    .line 559
    iget v11, v15, Landroid/graphics/PointF;->y:F

    move/from16 v7, v27

    .line 558
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->rotatePoint(FFFFF)Landroid/graphics/PointF;
    invoke-static/range {v6 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenControlList;FFFFF)Landroid/graphics/PointF;

    move-result-object v15

    .line 561
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v0, v26

    iget v10, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v26

    iget v11, v0, Landroid/graphics/PointF;->y:F

    move/from16 v7, v27

    move/from16 v9, v21

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->rotatePoint(FFFFF)Landroid/graphics/PointF;
    invoke-static/range {v6 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenControlList;FFFFF)Landroid/graphics/PointF;

    move-result-object v26

    .line 562
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget v10, v15, Landroid/graphics/PointF;->x:F

    .line 563
    iget v11, v15, Landroid/graphics/PointF;->y:F

    move/from16 v7, v27

    move/from16 v9, v21

    .line 562
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->rotatePoint(FFFFF)Landroid/graphics/PointF;
    invoke-static/range {v6 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenControlList;FFFFF)Landroid/graphics/PointF;

    move-result-object v15

    .line 565
    move-object/from16 v0, v26

    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v3, v15, Landroid/graphics/PointF;->x:F

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float v10, v2, v3

    .line 566
    .local v10, "newObjCenterX":F
    move-object/from16 v0, v26

    iget v2, v0, Landroid/graphics/PointF;->y:F

    iget v3, v15, Landroid/graphics/PointF;->y:F

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float v11, v2, v3

    .line 568
    .local v11, "newObjCenterY":F
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move/from16 v0, v21

    neg-float v12, v0

    move-object/from16 v0, v26

    iget v13, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v26

    iget v14, v0, Landroid/graphics/PointF;->y:F

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->rotatePoint(FFFFF)Landroid/graphics/PointF;
    invoke-static/range {v9 .. v14}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenControlList;FFFFF)Landroid/graphics/PointF;

    move-result-object v26

    .line 569
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move/from16 v0, v21

    neg-float v12, v0

    iget v13, v15, Landroid/graphics/PointF;->x:F

    .line 570
    iget v14, v15, Landroid/graphics/PointF;->y:F

    .line 569
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->rotatePoint(FFFFF)Landroid/graphics/PointF;
    invoke-static/range {v9 .. v14}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenControlList;FFFFF)Landroid/graphics/PointF;

    move-result-object v15

    .line 572
    move-object/from16 v0, v26

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v26

    iget v3, v0, Landroid/graphics/PointF;->y:F

    iget v4, v15, Landroid/graphics/PointF;->x:F

    iget v6, v15, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3, v4, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 573
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v3

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v2, v0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 575
    const/4 v2, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 576
    move-object/from16 v0, v23

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRotation(F)V

    goto/16 :goto_1
.end method

.method public setSelectable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 96
    return-void
.end method

.method public setSorDataInt(Ljava/lang/String;I)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 218
    return-void
.end method

.method public setSorDataString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 209
    return-void
.end method

.method public setSorInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "info"    # Ljava/lang/String;

    .prologue
    .line 119
    return-void
.end method

.method public setSorPackageLink(Ljava/lang/String;)V
    .locals 0
    .param p1, "link"    # Ljava/lang/String;

    .prologue
    .line 128
    return-void
.end method

.method public setVisibility(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 78
    return-void
.end method
