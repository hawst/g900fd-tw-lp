.class Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;
.super Ljava/lang/Object;
.source "Beautify.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/pen/preload/Beautify;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    .line 933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 937
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 941
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 946
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$0(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 947
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 948
    .local v0, "s":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "settingIndex":I
    :goto_0
    const/16 v3, 0xa

    if-lt v1, v3, :cond_1

    .line 974
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setAdvancedSetting(Ljava/lang/String;)V

    .line 975
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$0(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;->onChanged(Ljava/lang/String;)V

    .line 977
    .end local v0    # "s":Ljava/lang/StringBuffer;
    .end local v1    # "settingIndex":I
    :cond_0
    return-void

    .line 949
    .restart local v0    # "s":Ljava/lang/StringBuffer;
    .restart local v1    # "settingIndex":I
    :cond_1
    const/4 v2, 0x0

    .line 951
    .local v2, "settingValue":I
    packed-switch v1, :pswitch_data_0

    .line 969
    :pswitch_0
    const/4 v2, -0x1

    .line 971
    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 972
    const/16 v3, 0x3b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 948
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 953
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$4(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    .line 954
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v4

    aget-object v3, v3, v4

    const/4 v4, 0x2

    aput v2, v3, v4

    goto :goto_1

    .line 957
    :pswitch_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$5(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    .line 958
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v4

    aget-object v3, v3, v4

    const/4 v4, 0x3

    aput v2, v3, v4

    goto :goto_1

    .line 961
    :pswitch_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$6(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    .line 962
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v4

    aget-object v3, v3, v4

    const/4 v4, 0x4

    aput v2, v3, v4

    goto :goto_1

    .line 965
    :pswitch_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$7(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    .line 966
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v4

    aget-object v3, v3, v4

    const/4 v4, 0x6

    aput v2, v3, v4

    goto :goto_1

    .line 951
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
