.class Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;
.super Landroid/os/Handler;
.source "SpenPageEffectHandler2.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;


# static fields
.field private static final PAGE_EFFECT_IN:I = 0x1

.field private static final PAGE_EFFECT_OUT:I = 0x2

.field private static final PAGE_EFFECT_TIMER_INTERVAL:I = 0x0

.field private static final TAG:Ljava/lang/String; = "PageEffectHandler"


# instance fields
.field private mBlackPaint:Landroid/graphics/Paint;

.field private mCount:I

.field private mCurrentX:F

.field private mDirection:I

.field private mDistanceTable:[F

.field private mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mScreenshot:Landroid/graphics/Bitmap;

.field private mSrcPaint:Landroid/graphics/Paint;

.field private mWorking:Z


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    .line 20
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    .line 21
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    .line 23
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    .line 24
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    .line 26
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    .line 27
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    .line 53
    const/16 v0, 0x14

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    .line 57
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    .line 58
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 60
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    .line 61
    return-void

    .line 53
    nop

    :array_0
    .array-data 4
        0x3e19999a    # 0.15f
        0x3e4f0414
        0x3e8b8100
        0x3ebc048d
        0x3efd6720
        0x3f2ac35b
        0x3f6625ce
        0x3f9b1788
        0x3fd106d5
        0x400cdbe1
        0x403dd810
        0x407fdd38
        0x40ac6bf6
        0x40e86213
        0x411c992c
        0x41480000    # 12.5f
        0x41480000    # 12.5f
        0x41480000    # 12.5f
        0x41480000    # 12.5f
        0x41480000    # 12.5f
    .end array-data
.end method

.method private endAnimation()V
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    .line 214
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onFinish()V

    .line 215
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 67
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    .line 69
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    .line 70
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    .line 71
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    .line 72
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    .line 73
    return-void
.end method

.method public drawAnimation(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 128
    const-string v0, "PageEffectHandler"

    const-string v1, "Start draw animation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-nez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->LIGHTEN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 134
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 135
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    .line 136
    const-string v0, "PageEffectHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    add-float/2addr v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    add-float/2addr v1, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 142
    :goto_1
    const-string v0, "PageEffectHandler"

    const-string v1, "Start draw animation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_0

    .line 139
    :cond_1
    const-string v0, "PageEffectHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "(0, 0) ("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    int-to-float v5, v0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    move-object v1, p1

    move v3, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x2

    const v8, 0x3c23d70a    # 0.01f

    const-wide/16 v6, 0x0

    const/4 v5, 0x1

    .line 147
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    if-nez v2, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v4, :cond_5

    .line 157
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v2, v2

    const/high16 v3, 0x41000000    # 8.0f

    div-float v1, v2, v3

    .line 159
    .local v1, "distance":F
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    if-nez v2, :cond_3

    .line 160
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    add-float/2addr v2, v1

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    .line 166
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdate()V

    .line 170
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_4

    .line 171
    const/16 v2, 0x13

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    .line 172
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 173
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdateCanvasLayer(Landroid/graphics/Canvas;)V

    .line 174
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    neg-float v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    .line 175
    invoke-virtual {p0, v5, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 161
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    :cond_3
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    if-ne v2, v5, :cond_2

    .line 162
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    sub-float/2addr v2, v1

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    goto :goto_1

    .line 177
    :cond_4
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    .line 178
    invoke-virtual {p0, v4, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 180
    .end local v1    # "distance":F
    :cond_5
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v5, :cond_0

    .line 182
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    array-length v3, v3

    if-lt v2, v3, :cond_7

    .line 183
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget v3, v3, v4

    mul-float/2addr v2, v3

    mul-float v1, v2, v8

    .line 188
    .restart local v1    # "distance":F
    :goto_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    if-nez v2, :cond_8

    .line 189
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    add-float/2addr v2, v1

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    .line 196
    :cond_6
    :goto_3
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    if-nez v2, :cond_9

    .line 198
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    .line 199
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->endAnimation()V

    .line 201
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    .line 202
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 203
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 185
    .end local v1    # "distance":F
    :cond_7
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    aget v3, v3, v4

    mul-float/2addr v2, v3

    mul-float v1, v2, v8

    .restart local v1    # "distance":F
    goto :goto_2

    .line 190
    :cond_8
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    if-ne v2, v5, :cond_6

    .line 191
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    sub-float/2addr v2, v1

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    goto :goto_3

    .line 205
    :cond_9
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdate()V

    .line 206
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    .line 207
    invoke-virtual {p0, v5, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0
.end method

.method public isWorking()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    return v0
.end method

.method public saveScreenshot()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 89
    :try_start_0
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 96
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 97
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdateCanvasLayer(Landroid/graphics/Canvas;)V

    .line 98
    const/4 v0, 0x0

    .line 100
    const/4 v2, 0x1

    .end local v0    # "canvas":Landroid/graphics/Canvas;
    :goto_0
    return v2

    .line 90
    :catch_0
    move-exception v1

    .line 91
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 92
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 93
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    goto :goto_0
.end method

.method public setCanvasInformation(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 106
    return-void
.end method

.method public setPaint(Landroid/graphics/Paint;)V
    .locals 0
    .param p1, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    .line 78
    return-void
.end method

.method public setScreenResolution(II)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 82
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    .line 83
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    .line 84
    return-void
.end method

.method public startAnimation(I)Z
    .locals 4
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 110
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    .line 118
    :goto_0
    return v0

    .line 113
    :cond_0
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    .line 114
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    .line 115
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    .line 116
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    .line 117
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->sendEmptyMessageDelayed(IJ)Z

    move v0, v1

    .line 118
    goto :goto_0
.end method
