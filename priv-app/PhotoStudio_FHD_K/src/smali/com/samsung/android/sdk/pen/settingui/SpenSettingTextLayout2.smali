.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
.super Landroid/widget/LinearLayout;
.source "SpenSettingTextLayout2.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$ActionListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;
    }
.end annotation


# static fields
.field protected static final BODY_LAYOUT_HEIGHT:I = 0x14a

.field protected static final BOTTOM_LAYOUT_HEIGHT:I = 0x11

.field protected static final EXIT_BUTTON_RAW_HEIGHT:I = 0x2a

.field protected static final EXIT_BUTTON_RAW_WIDTH:I = 0x2a

.field protected static final LINE_BUTTON_RAW_HEIGHT:I = 0x11

.field protected static final LINE_BUTTON_RAW_WIDTH:I = 0x1

.field protected static final TEXT_SETTING_BOLD:I = 0x2

.field protected static final TEXT_SETTING_ITALIC:I = 0x3

.field protected static final TEXT_SETTING_UNDERLINE:I = 0x4

.field protected static final TITLE_LAYOUT_HEIGHT:I = 0x2a

.field public static final VIEW_MODE_COLOR:I = 0x4

.field public static final VIEW_MODE_MINIMUM:I = 0x1

.field public static final VIEW_MODE_MINIMUM_WITHOUT_PREVIEW:I = 0x2

.field public static final VIEW_MODE_NORMAL:I = 0x0

.field public static final VIEW_MODE_PARAGRAPH:I = 0x5

.field public static final VIEW_MODE_STYLE:I = 0x3

.field public static final VIEW_MODE_TITLE:I = 0x6

.field private static final grayBodyLeftPath:Ljava/lang/String; = "vienna_popup_bg02"

.field private static final lefBgFocuslPath:Ljava/lang/String; = "snote_popup_arrow_left_focus"

.field private static final lefBgPresslPath:Ljava/lang/String; = "snote_popup_arrow_left_press"

.field private static final leftBgPath:Ljava/lang/String; = "snote_popup_arrow_left_normal"

.field private static final lightBodyLeftPath:Ljava/lang/String; = "vienna_popup_bg"

.field protected static final mBgPath:Ljava/lang/String; = "drawingpad_bg_3"

.field protected static final mBoldIconPath:Ljava/lang/String; = "snote_popup_textoption_bold"

.field protected static final mBoldIconPressPath:Ljava/lang/String; = "snote_text_icon_bold_press"

.field private static final mBoldLeftFocusPath:Ljava/lang/String; = "snote_popup_option_btn_left_focus"

.field private static final mBoldLeftNomarPath:Ljava/lang/String; = "snote_popup_option_btn_left_normal"

.field private static final mBoldLeftPressPath:Ljava/lang/String; = "snote_popup_option_btn_left_press_1"

.field protected static final mButtonBgFocusPath:Ljava/lang/String; = "snote_popup_btn_focus"

.field protected static final mButtonBgPath:Ljava/lang/String; = "snote_popup_btn_normal"

.field protected static final mButtonBgPressPath:Ljava/lang/String; = "snote_popup_btn_press"

.field protected static final mCenterIconPressPath:Ljava/lang/String;

.field protected static final mCenterIocnPath:Ljava/lang/String;

.field protected static mColorPickerShow:Z = false

.field protected static final mDefaultPath:Ljava/lang/String; = ""

.field private static final mDropdownFocusPath:Ljava/lang/String; = "snote_dropdown_focused"

.field private static final mDropdownNormalPath:Ljava/lang/String; = "snote_dropdown_normal"

.field private static final mDropdownPressPath:Ljava/lang/String; = "snote_dropdown_pressed"

.field protected static mFirstTimeColorPickerShow:Z = false

.field protected static final mItalicIconPath:Ljava/lang/String; = "snote_popup_textoption_italic"

.field protected static final mItalicIconPressPath:Ljava/lang/String; = "snote_text_icon_italic_press"

.field private static final mItalicLeftFocusPath:Ljava/lang/String; = "snote_popup_option_btn_center_focus"

.field private static final mItalicLeftNomarPath:Ljava/lang/String; = "snote_popup_option_btn_center_normal"

.field private static final mItalicLeftPressPath:Ljava/lang/String; = "snote_popup_option_btn_center_press"

.field protected static final mLeftIconPath:Ljava/lang/String;

.field protected static final mLeftIconPressPath:Ljava/lang/String;

.field protected static final mLeftIndentIconPath:Ljava/lang/String;

.field private static final mOptionBgPath:Ljava/lang/String;

.field protected static final mPreviewBgPath:Ljava/lang/String; = "snote_popup_preview_bg"

.field protected static final mRightIconPath:Ljava/lang/String;

.field protected static final mRightIconPressPath:Ljava/lang/String;

.field protected static final mRightIndentIconPath:Ljava/lang/String;

.field private static final mScrollHandelNormal:Ljava/lang/String;

.field protected static mSdkVersion:I = 0x0

.field protected static final mSpinnerBgEnablePath:Ljava/lang/String; = "tw_spinner_disabled_holo_light"

.field protected static final mSpinnerBgFocusPath:Ljava/lang/String; = "tw_spinner_focused_holo_light"

.field protected static final mSpinnerBgPath:Ljava/lang/String; = "tw_spinner_default_holo_light"

.field protected static final mSpinnerBgPressPath:Ljava/lang/String; = "tw_spinner_pressed_holo_light"

.field protected static final mSpinnerBgSelectPath:Ljava/lang/String; = "tw_spinner_selected_holo_light"

.field protected static final mSpinnerListBgPath:Ljava/lang/String; = "snote_popup_option_btn_normal"

.field protected static final mSpinnerListFocusBgPath:Ljava/lang/String; = "snote_popup_option_btn_focus"

.field protected static final mSpinnerListPressBgPath:Ljava/lang/String; = "snote_popup_option_btn_press"

.field private static final mUnderLeftFocusPath:Ljava/lang/String; = "snote_popup_option_btn_right_focus"

.field private static final mUnderLeftNomarPath:Ljava/lang/String; = "snote_popup_option_btn_right_normal"

.field private static final mUnderLeftPressPath:Ljava/lang/String; = "snote_popup_option_btn_right_press_1"

.field protected static final mUnderLineIconPath:Ljava/lang/String; = "snote_popup_textoption_underline"

.field protected static final mUnderLineIconPressPath:Ljava/lang/String; = "snote_text_icon_underline_press"

.field private static final popupMaxPath:Ljava/lang/String; = "snote_popup_arrow_max_normal"

.field private static final popupMinPath:Ljava/lang/String; = "snote_popup_arrow_min_normal"

.field private static final rightBgFocuslPath:Ljava/lang/String; = "snote_popup_arrow_right_focus"

.field private static final rightBgPath:Ljava/lang/String; = "snote_popup_arrow_right_normal"

.field private static final rightBgPresslPath:Ljava/lang/String; = "snote_popup_arrow_right_press"

.field private static final titleRightPath:Ljava/lang/String; = "vienna_popup_title_bg"


# instance fields
.field protected EXIT_BUTTON_HEIGHT:I

.field protected EXIT_BUTTON_RIGHT_MARGIN:F

.field protected EXIT_BUTTON_TOP_MARGIN:F

.field protected EXIT_BUTTON_WIDTH:I

.field protected mAlignCenterBtn:Landroid/widget/ImageButton;

.field protected mAlignLeftBtn:Landroid/widget/ImageButton;

.field protected mAlignRightBtn:Landroid/widget/ImageButton;

.field protected mBodyBg:Landroid/view/View;

.field protected mBodyLayout:Landroid/view/View;

.field protected mBodyLayoutHeight:I

.field protected mBoldBtn:Landroid/widget/ImageButton;

.field protected mCanvasLayout:Landroid/widget/RelativeLayout;

.field protected mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

.field protected mCanvasWidth:I

.field protected mColorPickerColor:Landroid/view/View;

.field private final mColorPickerColorChangeListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

.field private final mColorPickerColorListener:Landroid/view/View$OnClickListener;

.field protected mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

.field protected mColorPickerSettingExitButton:Landroid/view/View;

.field protected mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

.field protected mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

.field protected mColorSelectedAndPicker:Landroid/view/View;

.field private mCount:I

.field private mCurrentFontName:Ljava/lang/String;

.field private mCurrentFontSize:Ljava/lang/String;

.field protected mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private final mExitButtonListener:Landroid/view/View$OnClickListener;

.field protected mFirstLongPress:Z

.field protected mFontLineSpaceSpinner:Landroid/widget/Spinner;

.field private mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

.field private mFontSizeButton:Landroid/widget/Button;

.field private mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;

.field private final mFontSizeOnClickListener:Landroid/view/View$OnClickListener;

.field protected mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

.field protected mFontSizeSpinnerView:Landroid/widget/Button;

.field private mFontTypeButton:Landroid/widget/Button;

.field private final mFontTypeOnClickListener:Landroid/view/View$OnClickListener;

.field protected mFontTypeSpinnerView:Landroid/view/ViewGroup;

.field private final mHandler:Landroid/os/Handler;

.field private mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field protected mIndentLeftBtn:Landroid/widget/ImageButton;

.field protected mIndentRightBtn:Landroid/widget/ImageButton;

.field protected mItalicBtn:Landroid/widget/ImageButton;

.field protected mLeftMargin:I

.field protected mMovableRect:Landroid/graphics/Rect;

.field private final mOnColorChangedListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

.field private final mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

.field private final mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

.field protected mPaletteBg:Landroid/view/View;

.field protected mPaletteLeftButton:Landroid/view/View;

.field private final mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

.field protected mPaletteRightButton:Landroid/view/View;

.field protected mPaletteView:Landroid/view/View;

.field protected mParaLineSpinner1stSelect:Z

.field protected mParagraphSetting:Landroid/view/View;

.field protected mPickerView:Landroid/view/View;

.field private final mPopupButtonListener:Landroid/view/View$OnClickListener;

.field private mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

.field protected mPopupMaxButton:Landroid/view/View;

.field protected mPopupMinButton:Landroid/view/View;

.field protected mPreCanvasFingerAction:I

.field protected mPreCanvasPenAction:I

.field protected mScale:F

.field protected mScrollAxis:I

.field protected mScrollHandle:Landroid/widget/ImageView;

.field private mScrollTimer:Ljava/util/Timer;

.field protected mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

.field private final mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

.field protected mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

.field private final mTextAlignSettingListener:Landroid/view/View$OnClickListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation
.end field

.field protected mTextContext:Landroid/content/Context;

.field protected mTextFontSizeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTextIndentSettingListener:Landroid/view/View$OnClickListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation
.end field

.field private final mTextLineSpacingSettingListener:Landroid/widget/AdapterView$OnItemSelectedListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation
.end field

.field private final mTextOptButtonListener:Landroid/view/View$OnClickListener;

.field protected mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

.field protected mTextSettingPreview:Landroid/view/View;

.field protected mTextSizeButtonView:[Landroid/view/View;

.field protected mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field protected mTitleLayout:Landroid/view/View;

.field protected mTopMargin:I

.field protected mTotalLeftMargin:I

.field protected mTotalTopMargin:I

.field protected mUnderlineBtn:Landroid/widget/ImageButton;

.field protected mViewMode:I

.field totalLayoutParams:Landroid/widget/LinearLayout$LayoutParams;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSdkVersion:I

    .line 279
    sput-boolean v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerShow:Z

    .line 280
    sput-boolean v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFirstTimeColorPickerShow:Z

    .line 2816
    const-string v0, "snote_text_left"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mLeftIconPath:Ljava/lang/String;

    .line 2817
    const-string v0, "snote_text_center"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCenterIocnPath:Ljava/lang/String;

    .line 2818
    const-string v0, "snote_text_right"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mRightIconPath:Ljava/lang/String;

    .line 2819
    const-string v0, "snote_text_all_left"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mLeftIndentIconPath:Ljava/lang/String;

    .line 2820
    const-string v0, "snote_text_all_right"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mRightIndentIconPath:Ljava/lang/String;

    .line 2821
    const-string v0, "snote_text_left_press"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mLeftIconPressPath:Ljava/lang/String;

    .line 2822
    const-string v0, "snote_text_center_press"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCenterIconPressPath:Ljava/lang/String;

    .line 2823
    const-string v0, "snote_text_right_press"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mRightIconPressPath:Ljava/lang/String;

    .line 2824
    const-string v0, "snote_option_in_bg"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mOptionBgPath:Ljava/lang/String;

    .line 2825
    const-string v0, "snote_popup_scroll_handle_n"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollHandelNormal:Ljava/lang/String;

    .line 2826
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Landroid/widget/RelativeLayout;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p4, "relativeLayout"    # Landroid/widget/RelativeLayout;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/widget/RelativeLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "fontName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 702
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 98
    const/4 v1, -0x2

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayoutHeight:I

    .line 134
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

    .line 145
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mParaLineSpinner1stSelect:Z

    .line 146
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    .line 148
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollAxis:I

    .line 150
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFirstLongPress:Z

    .line 157
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScale:F

    .line 158
    const/16 v1, 0x404

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    .line 159
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mViewMode:I

    .line 171
    const/high16 v1, 0x40800000    # 4.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->EXIT_BUTTON_RIGHT_MARGIN:F

    .line 179
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mHandler:Landroid/os/Handler;

    .line 258
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerColorChangeListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    .line 282
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mOnColorChangedListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    .line 333
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeOnClickListener:Landroid/view/View$OnClickListener;

    .line 368
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeOnClickListener:Landroid/view/View$OnClickListener;

    .line 400
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextOptButtonListener:Landroid/view/View$OnClickListener;

    .line 410
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 454
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    .line 470
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$8;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerColorListener:Landroid/view/View$OnClickListener;

    .line 495
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$9;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    .line 530
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    .line 550
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$11;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    .line 574
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    .line 601
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$13;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextLineSpacingSettingListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 627
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$14;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 637
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$15;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    .line 703
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    .line 704
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScale:F

    invoke-direct {v1, p1, p2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 705
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 706
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 707
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 708
    new-instance v1, Lcom/samsung/android/sdk/pen/util/SpenFont;

    invoke-direct {v1, p1, p3}, Lcom/samsung/android/sdk/pen/util/SpenFont;-><init>(Landroid/content/Context;Ljava/util/HashMap;)V

    .line 709
    const/16 v0, 0x8

    .local v0, "mTextSize":I
    :goto_0
    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 712
    const/16 v0, 0x16

    :goto_1
    const/16 v1, 0x21

    if-lt v0, v1, :cond_1

    .line 715
    const/16 v0, 0x24

    :goto_2
    const/16 v1, 0x41

    if-lt v0, v1, :cond_2

    .line 718
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->initButtonValue()V

    .line 719
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->initView()V

    .line 720
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setListener()V

    .line 721
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    .line 722
    return-void

    .line 710
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 709
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 713
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 712
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 716
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 715
    add-int/lit8 v0, v0, 0x4

    goto :goto_2
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Landroid/widget/RelativeLayout;F)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p4, "relativeLayout"    # Landroid/widget/RelativeLayout;
    .param p5, "ratio"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/widget/RelativeLayout;",
            "F)V"
        }
    .end annotation

    .prologue
    .local p3, "fontName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 775
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 98
    const/4 v1, -0x2

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayoutHeight:I

    .line 134
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

    .line 145
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mParaLineSpinner1stSelect:Z

    .line 146
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    .line 148
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollAxis:I

    .line 150
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFirstLongPress:Z

    .line 157
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScale:F

    .line 158
    const/16 v1, 0x404

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    .line 159
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mViewMode:I

    .line 171
    const/high16 v1, 0x40800000    # 4.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->EXIT_BUTTON_RIGHT_MARGIN:F

    .line 179
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mHandler:Landroid/os/Handler;

    .line 258
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerColorChangeListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    .line 282
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mOnColorChangedListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    .line 333
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeOnClickListener:Landroid/view/View$OnClickListener;

    .line 368
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeOnClickListener:Landroid/view/View$OnClickListener;

    .line 400
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextOptButtonListener:Landroid/view/View$OnClickListener;

    .line 410
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 454
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    .line 470
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$8;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerColorListener:Landroid/view/View$OnClickListener;

    .line 495
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$9;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    .line 530
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    .line 550
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$11;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    .line 574
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    .line 601
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$13;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextLineSpacingSettingListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 627
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$14;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 637
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$15;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    .line 776
    iput p5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScale:F

    .line 777
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    .line 778
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v1, p1, p2, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 779
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 780
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 781
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 783
    new-instance v1, Lcom/samsung/android/sdk/pen/util/SpenFont;

    invoke-direct {v1, p1, p3}, Lcom/samsung/android/sdk/pen/util/SpenFont;-><init>(Landroid/content/Context;Ljava/util/HashMap;)V

    .line 784
    const/16 v0, 0x8

    .local v0, "mTextSize":I
    :goto_0
    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 787
    const/16 v0, 0x16

    :goto_1
    const/16 v1, 0x21

    if-lt v0, v1, :cond_1

    .line 790
    const/16 v0, 0x24

    :goto_2
    const/16 v1, 0x41

    if-lt v0, v1, :cond_2

    .line 793
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->initButtonValue()V

    .line 794
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->initView()V

    .line 796
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setListener()V

    .line 797
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    .line 798
    return-void

    .line 785
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 784
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 788
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 787
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 791
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 790
    add-int/lit8 v0, v0, 0x4

    goto :goto_2
.end method

.method private ColorPickerSettinginit()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1151
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScale:F

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;FII)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    .line 1152
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSettingExitButton:Landroid/view/View;

    .line 1153
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSettingExitButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mExitButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1154
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerColor:Landroid/view/View;

    .line 1155
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerColor:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerColorListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1156
    return-void
.end method

.method private PaletteView()Landroid/view/ViewGroup;
    .locals 15

    .prologue
    const/16 v14, 0xf

    const/4 v13, 0x1

    const/high16 v12, 0x41d80000    # 27.0f

    const/high16 v3, 0x40800000    # 4.0f

    const/high16 v11, 0x42a60000    # 83.0f

    .line 1750
    new-instance v9, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v9, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1751
    .local v9, "sizeDisplayLayout":Landroid/widget/RelativeLayout;
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 1752
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1751
    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1753
    .local v10, "sizeDisplayLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 1754
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, 0x0

    .line 1753
    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1755
    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1757
    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1758
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteRightButton:Landroid/view/View;

    .line 1759
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1760
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1759
    invoke-direct {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1761
    .local v8, "rightImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v13, v8, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1762
    const/16 v0, 0xb

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1763
    invoke-virtual {v8, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1765
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteRightButton:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1766
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteRightButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_next"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1768
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteRightButton:Landroid/view/View;

    const-string v2, "snote_popup_arrow_right_normal"

    const-string v3, "snote_popup_arrow_right_press"

    .line 1769
    const-string v4, "snote_popup_arrow_right_focus"

    const/16 v5, 0x1b

    const/16 v6, 0x53

    .line 1768
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1771
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteLeftButton:Landroid/view/View;

    .line 1772
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1773
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1772
    invoke-direct {v7, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1774
    .local v7, "leftImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v13, v7, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1775
    const/16 v0, 0x9

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1776
    invoke-virtual {v7, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1778
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteLeftButton:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1779
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteLeftButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_back"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1781
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteLeftButton:Landroid/view/View;

    const-string v2, "snote_popup_arrow_left_normal"

    const-string v3, "snote_popup_arrow_left_press"

    .line 1782
    const-string v4, "snote_popup_arrow_left_focus"

    const/16 v5, 0x1b

    const/16 v6, 0x53

    .line 1781
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1784
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->colorPaletteView()Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    .line 1786
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteLeftButton:Landroid/view/View;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1787
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1788
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteRightButton:Landroid/view/View;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1790
    return-object v9
.end method

.method private PickerView()Landroid/view/ViewGroup;
    .locals 6

    .prologue
    const/high16 v5, 0x42080000    # 34.0f

    .line 1734
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1735
    .local v1, "localLinearLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1736
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v3, 0x43828000    # 261.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42820000    # 65.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1735
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1737
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1738
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1739
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42b00000    # 88.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1740
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1742
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->colorPickerView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    .line 1743
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1745
    return-object v1
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2272
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setTextStyle(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

    return-object v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCount:I

    return v0
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;I)V
    .locals 0

    .prologue
    .line 177
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCount:I

    return-void
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontSize:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontSize:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    return-object v0
.end method

.method private bodyBg()Landroid/view/ViewGroup;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 942
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 943
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 946
    .local v1, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 947
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v3, "vienna_popup_bg"

    invoke-virtual {v2, v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 949
    return-object v0
.end method

.method private bodyLayout()Landroid/view/ViewGroup;
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v11, -0x2

    const/4 v10, -0x1

    const/4 v9, 0x0

    .line 894
    new-instance v6, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    .line 895
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 897
    .local v4, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v4, v9, v9, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 898
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 899
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setVerticalFadingEdgeEnabled(Z)V

    .line 900
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setFadingEdgeLength(I)V

    .line 901
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 902
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setOverScrollMode(I)V

    .line 904
    new-instance v5, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;-><init>(Landroid/content/Context;)V

    .line 905
    .local v5, "palletViewLayout":Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;
    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    .line 906
    invoke-direct {v6, v10, v11}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 905
    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 907
    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->setOrientation(I)V

    .line 909
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->textPreview()Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSettingPreview:Landroid/view/View;

    .line 910
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->fontSizeSpinnerOptButton()Landroid/view/ViewGroup;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    .line 912
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->colorSelectedAndPicker()Landroid/view/ViewGroup;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectedAndPicker:Landroid/view/View;

    .line 914
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v6}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 915
    .local v0, "bottonPaddingLayout":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 916
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41880000    # 17.0f

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScale:F

    div-float/2addr v7, v8

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 915
    invoke-direct {v1, v10, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 917
    .local v1, "bottonPaddingLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 918
    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 920
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 921
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 923
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 925
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->paragraphSetting()Landroid/view/ViewGroup;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mParagraphSetting:Landroid/view/View;

    .line 927
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v6, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->addView(Landroid/view/View;)V

    .line 929
    new-instance v2, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v2, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 930
    .local v2, "layout":Landroid/widget/RelativeLayout;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 932
    .local v3, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 933
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->bodyBg()Landroid/view/ViewGroup;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyBg:Landroid/view/View;

    .line 934
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyBg:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 935
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 938
    return-object v2
.end method

.method private checkPosition()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1972
    const/4 v4, 0x2

    new-array v0, v4, [I

    .line 1973
    .local v0, "location":[I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x437a0000    # 250.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1974
    .local v2, "minWidth":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43050000    # 133.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1976
    .local v1, "minHeight":I
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getLocationOnScreen([I)V

    .line 1978
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1980
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    aget v4, v0, v6

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-ge v4, v5, :cond_0

    .line 1981
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1983
    :cond_0
    aget v4, v0, v7

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    if-ge v4, v5, :cond_1

    .line 1984
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1987
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    aget v5, v0, v6

    sub-int/2addr v4, v5

    if-ge v4, v2, :cond_2

    .line 1988
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v4, v2

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1990
    iget v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-gez v4, :cond_2

    .line 1991
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1994
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    aget v5, v0, v7

    sub-int/2addr v4, v5

    if-ge v4, v1, :cond_3

    .line 1995
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, v1

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1997
    iget v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-gez v4, :cond_3

    .line 1998
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2002
    :cond_3
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2003
    return-void
.end method

.method private colorPaletteView()Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1803
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    .line 1804
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScale:F

    .line 1803
    invoke-direct {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    .line 1807
    .local v0, "colorPaletteView":Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1808
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v3, 0x43828000    # 261.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, -0x2

    .line 1807
    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1810
    .local v1, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1811
    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1812
    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1814
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1815
    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setClickable(Z)V

    .line 1816
    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setFocusable(Z)V

    .line 1820
    return-object v0
.end method

.method private colorPickerView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1834
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    .line 1835
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScale:F

    .line 1834
    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    .line 1836
    .local v1, "localf":Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1837
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v3, 0x43828000    # 261.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42820000    # 65.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1836
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1839
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1840
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setClickable(Z)V

    .line 1841
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_gradation"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1842
    invoke-virtual {v1, v5, v5, v5, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setPadding(IIII)V

    .line 1843
    return-object v1
.end method

.method private colorSelectedAndPicker()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 1706
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1707
    .local v1, "localLinearLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 1708
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43230000    # 163.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1707
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1710
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1711
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->PickerView()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPickerView:Landroid/view/View;

    .line 1712
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->PaletteView()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteView:Landroid/view/View;

    .line 1713
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->paletteBg()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteBg:Landroid/view/View;

    .line 1714
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1715
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1716
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPickerView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1717
    return-object v1
.end method

.method private fontSizeSpinner()Landroid/widget/Button;
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/high16 v7, 0x41100000    # 9.0f

    const/4 v6, 0x0

    .line 1591
    new-instance v1, Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    .line 1592
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1593
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42400000    # 48.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42240000    # 41.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1592
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1594
    .local v0, "fontSizeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40c00000    # 6.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1595
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1598
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 1599
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v3, "snote_dropdown_normal"

    .line 1600
    const-string v4, "snote_dropdown_pressed"

    const-string v5, "snote_dropdown_focused"

    .line 1599
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1609
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setGravity(I)V

    .line 1610
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1611
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1610
    invoke-virtual {v1, v2, v6, v3, v6}, Landroid/widget/Button;->setPadding(IIII)V

    .line 1614
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setSingleLine(Z)V

    .line 1615
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 1616
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v6, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1617
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontSize:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1618
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1620
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1621
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    return-object v1

    .line 1602
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v3, "snote_dropdown_normal"

    const-string v4, "snote_dropdown_pressed"

    .line 1603
    const-string v5, "snote_dropdown_focused"

    .line 1602
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private fontSizeSpinnerOptButton()Landroid/view/ViewGroup;
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/high16 v13, 0x42280000    # 42.0f

    const/high16 v12, 0x42240000    # 41.0f

    const/4 v11, 0x0

    const/16 v10, 0x1e

    .line 1311
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1312
    .local v0, "fontSizeSpinnerOptButtonLayout":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 1313
    const/4 v5, -0x1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x42680000    # 58.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1312
    invoke-direct {v1, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1314
    .local v1, "fontSizeSpinnerOptButtonLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iput v5, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 1316
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1317
    const/16 v5, 0x10

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1318
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1319
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41500000    # 13.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1320
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41000000    # 8.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41500000    # 13.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1321
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x41100000    # 9.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1319
    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1322
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->fontTypeSpinnerView()Landroid/view/ViewGroup;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    .line 1323
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->fontSizeSpinner()Landroid/widget/Button;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerView:Landroid/widget/Button;

    .line 1324
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1325
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1326
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 1327
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1326
    invoke-direct {v2, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1342
    .local v2, "mBoldBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v5, Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    .line 1343
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v2}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1344
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v14}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1345
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v7, "string_bold"

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1348
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    const-string v7, "snote_popup_option_btn_left_normal"

    const-string v8, "snote_popup_option_btn_left_press_1"

    .line 1349
    const-string v9, "snote_popup_option_btn_left_focus"

    .line 1348
    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1351
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v7, "snote_popup_textoption_bold"

    invoke-virtual {v6, v7, v10, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1353
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1360
    new-instance v5, Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    .line 1361
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 1362
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1361
    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1363
    .local v3, "mItalicBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v3}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1364
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v14}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1365
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v7, "string_italic"

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1368
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    const-string v7, "snote_popup_option_btn_center_normal"

    const-string v8, "snote_popup_option_btn_center_press"

    .line 1369
    const-string v9, "snote_popup_option_btn_center_focus"

    .line 1368
    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1371
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v7, "snote_popup_textoption_italic"

    invoke-virtual {v6, v7, v10, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1373
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1378
    new-instance v5, Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    .line 1379
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 1380
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1379
    invoke-direct {v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1381
    .local v4, "mUnderlineBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v4}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1382
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v14}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1383
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v7, "string_underline"

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1384
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v7, "snote_popup_textoption_underline"

    invoke-virtual {v6, v7, v10, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1385
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    invoke-virtual {v5, v11, v11, v11, v6}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 1388
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    const-string v7, "snote_popup_option_btn_right_normal"

    const-string v8, "snote_popup_option_btn_right_press_1"

    .line 1389
    const-string v9, "snote_popup_option_btn_right_focus"

    .line 1388
    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1391
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1408
    return-object v0
.end method

.method private fontTypeSpinner()Landroid/widget/Button;
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/high16 v7, 0x41100000    # 9.0f

    const/4 v6, 0x0

    .line 1663
    new-instance v1, Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    .line 1664
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1665
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42e60000    # 115.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42240000    # 41.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1664
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1666
    .local v0, "fontTypeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1669
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 1670
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v3, "snote_dropdown_normal"

    .line 1671
    const-string v4, "snote_dropdown_pressed"

    const-string v5, "snote_dropdown_focused"

    .line 1670
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1683
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setGravity(I)V

    .line 1684
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1685
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1684
    invoke-virtual {v1, v2, v6, v3, v6}, Landroid/widget/Button;->setPadding(IIII)V

    .line 1688
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setSingleLine(Z)V

    .line 1689
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 1690
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v6, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1691
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1692
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1693
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1694
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    return-object v1

    .line 1673
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v3, "snote_dropdown_normal"

    const-string v4, "snote_dropdown_pressed"

    .line 1674
    const-string v5, "snote_dropdown_focused"

    .line 1673
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private fontTypeSpinnerView()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 1634
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1641
    .local v0, "fontTypeSpinnerLayout":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 1642
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42e60000    # 115.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42240000    # 41.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1641
    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1645
    .local v1, "fontTypeSpinnerLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1646
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1647
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->fontTypeSpinner()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1648
    return-object v0
.end method

.method private getMovableRect()Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2829
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 2830
    .local v0, "location":[I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2832
    .local v1, "r":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    .line 2834
    aget v2, v0, v4

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mLeftMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 2835
    aget v2, v0, v5

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTopMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 2836
    aget v2, v0, v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 2837
    aget v2, v0, v5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 2839
    return-object v1
.end method

.method private initButtonValue()V
    .locals 1

    .prologue
    const/16 v0, 0x2a

    .line 953
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->EXIT_BUTTON_WIDTH:I

    .line 954
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->EXIT_BUTTON_HEIGHT:I

    .line 955
    return-void
.end method

.method private initColorSelecteView()V
    .locals 3

    .prologue
    .line 1145
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    if-eqz v0, :cond_0

    .line 1146
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mOnColorChangedListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->getPreviewTextColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setInitialValue(Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;I)V

    .line 1148
    :cond_0
    return-void
.end method

.method private initView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1073
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->totalLayout()V

    .line 1074
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    new-array v1, v1, [Landroid/view/View;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    .line 1075
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 1079
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    .line 1080
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1081
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1082
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1083
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1084
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1085
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1087
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentLeftBtn:Landroid/widget/ImageButton;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentRightBtn:Landroid/widget/ImageButton;

    if-eqz v1, :cond_1

    .line 1088
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentLeftBtn:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1089
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentRightBtn:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1092
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->initColorSelecteView()V

    .line 1093
    new-instance v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 1094
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->ColorPickerSettinginit()V

    .line 1095
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setVisibility(I)V

    .line 1096
    return-void

    .line 1076
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1075
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private paletteBg()Landroid/view/ViewGroup;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1722
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1723
    .local v1, "paletteBgLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 1724
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43230000    # 163.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1723
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1725
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1726
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1728
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v3, "vienna_popup_bg02"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1729
    return-object v1
.end method

.method private paragraphSetting()Landroid/view/ViewGroup;
    .locals 30

    .prologue
    .line 1423
    new-instance v18, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1424
    .local v18, "paragraphSettingLayout":Landroid/widget/LinearLayout;
    new-instance v19, Landroid/widget/LinearLayout$LayoutParams;

    .line 1425
    const/16 v25, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x42de0000    # 111.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    .line 1424
    move-object/from16 v0, v19

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1426
    .local v19, "paragraphSettingLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual/range {v18 .. v19}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1427
    const/16 v25, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1431
    new-instance v14, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-direct {v14, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1432
    .local v14, "paragraphSettingAlignLayout":Landroid/widget/LinearLayout;
    new-instance v15, Landroid/widget/LinearLayout$LayoutParams;

    .line 1433
    const/16 v25, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x425c0000    # 55.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    .line 1432
    move/from16 v0, v25

    move/from16 v1, v26

    invoke-direct {v15, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1434
    .local v15, "paragraphSettingAlignLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v25, v0

    const/high16 v26, 0x41400000    # 12.0f

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v25

    .line 1435
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x41000000    # 8.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v27, v0

    const/high16 v28, 0x41500000    # 13.0f

    invoke-virtual/range {v27 .. v28}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v27

    .line 1436
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v28, v0

    const/high16 v29, 0x41100000    # 9.0f

    invoke-virtual/range {v28 .. v29}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v28

    .line 1434
    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v25, v0

    const/high16 v26, 0x3f800000    # 1.0f

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v25

    move/from16 v0, v25

    iput v0, v15, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 1440
    const/16 v25, 0x10

    move/from16 v0, v25

    iput v0, v15, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1441
    invoke-virtual {v14, v15}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1442
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1443
    new-instance v11, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-direct {v11, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1444
    .local v11, "mAlignTextView":Landroid/widget/TextView;
    new-instance v12, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v25, -0x1

    .line 1445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x425c0000    # 55.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    .line 1444
    move/from16 v0, v25

    move/from16 v1, v26

    invoke-direct {v12, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1446
    .local v12, "mAlignTextViewParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v25, 0x3f800000    # 1.0f

    move/from16 v0, v25

    iput v0, v12, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1447
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-object/from16 v25, v0

    const-string v26, "string_align"

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1449
    const/high16 v25, -0x1000000

    move/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1450
    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x41900000    # 18.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v11, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1451
    const/16 v25, 0x11

    move/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 1452
    invoke-virtual {v14, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1453
    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    .line 1454
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v25, v0

    const/high16 v26, 0x42400000    # 48.0f

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x42280000    # 42.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    .line 1453
    move/from16 v0, v25

    move/from16 v1, v26

    invoke-direct {v10, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1455
    .local v10, "mAlignLeftBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v25, v0

    const/high16 v26, 0x41300000    # 11.0f

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v25

    move/from16 v0, v25

    iput v0, v10, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1456
    const/16 v25, 0x10

    move/from16 v0, v25

    iput v0, v10, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1457
    new-instance v25, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-direct/range {v25 .. v26}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    .line 1458
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1459
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const-string v27, "snote_text_left"

    sget-object v28, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mLeftIconPressPath:Ljava/lang/String;

    .line 1461
    sget-object v29, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mLeftIconPressPath:Ljava/lang/String;

    .line 1460
    invoke-virtual/range {v26 .. v29}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1462
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1463
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    .line 1464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v25, v0

    const/high16 v26, 0x42300000    # 44.0f

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x41d80000    # 27.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    .line 1463
    move/from16 v0, v25

    move/from16 v1, v26

    invoke-direct {v9, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1465
    .local v9, "mAlignCenterBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v25, 0x10

    move/from16 v0, v25

    iput v0, v9, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1466
    new-instance v25, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-direct/range {v25 .. v26}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    .line 1467
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1468
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1469
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    sget-object v27, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCenterIocnPath:Ljava/lang/String;

    sget-object v28, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCenterIconPressPath:Ljava/lang/String;

    .line 1470
    sget-object v29, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCenterIconPressPath:Ljava/lang/String;

    .line 1469
    invoke-virtual/range {v26 .. v29}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1471
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1472
    new-instance v25, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-direct/range {v25 .. v26}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    .line 1473
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1474
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v26, v0

    const-string v27, "snote_popup_btn_normal"

    const-string v28, "snote_popup_btn_press"

    .line 1477
    const-string v29, "snote_popup_btn_press"

    .line 1476
    invoke-virtual/range {v25 .. v29}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v26, v0

    const-string v27, "snote_popup_btn_normal"

    const-string v28, "snote_popup_btn_press"

    .line 1479
    const-string v29, "snote_popup_btn_press"

    .line 1478
    invoke-virtual/range {v25 .. v29}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1480
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v26, v0

    const-string v27, "snote_popup_btn_normal"

    const-string v28, "snote_popup_btn_press"

    .line 1481
    const-string v29, "snote_popup_btn_press"

    .line 1480
    invoke-virtual/range {v25 .. v29}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1483
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    sget-object v27, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mRightIconPath:Ljava/lang/String;

    sget-object v28, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mRightIconPressPath:Ljava/lang/String;

    .line 1484
    sget-object v29, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mRightIconPressPath:Ljava/lang/String;

    .line 1483
    invoke-virtual/range {v26 .. v29}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1485
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1489
    new-instance v16, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1490
    .local v16, "paragraphSettingIndentLayout":Landroid/widget/LinearLayout;
    new-instance v17, Landroid/widget/LinearLayout$LayoutParams;

    .line 1491
    const/16 v25, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x42580000    # 54.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    .line 1490
    move-object/from16 v0, v17

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1492
    .local v17, "paragraphSettingIndentLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v25, v0

    const/high16 v26, 0x40c00000    # 6.0f

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1493
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v25, v0

    const/high16 v26, 0x40e00000    # 7.0f

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1494
    const/16 v25, 0x10

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1495
    invoke-virtual/range {v16 .. v17}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1496
    const/16 v25, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1498
    new-instance v8, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-direct {v8, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1499
    .local v8, "indentTextView":Landroid/widget/TextView;
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v25, -0x1

    .line 1500
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x42580000    # 54.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    .line 1499
    move/from16 v0, v25

    move/from16 v1, v26

    invoke-direct {v7, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1501
    .local v7, "indentTextParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v25, 0x3f800000    # 1.0f

    move/from16 v0, v25

    iput v0, v7, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1502
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1503
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-object/from16 v25, v0

    const-string v26, "string_indent"

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1504
    const/high16 v25, -0x1000000

    move/from16 v0, v25

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1505
    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x41700000    # 15.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v8, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1506
    const/16 v25, 0x11

    move/from16 v0, v25

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 1507
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1509
    new-instance v25, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-direct/range {v25 .. v26}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentLeftBtn:Landroid/widget/ImageButton;

    .line 1510
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 1511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v25, v0

    const/high16 v26, 0x42300000    # 44.0f

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x41d80000    # 27.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    .line 1510
    move/from16 v0, v25

    move/from16 v1, v26

    invoke-direct {v5, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1512
    .local v5, "indentLeftBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v25, v0

    const/high16 v26, 0x41300000    # 11.0f

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v25

    move/from16 v0, v25

    iput v0, v5, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1513
    const/16 v25, 0x10

    move/from16 v0, v25

    iput v0, v5, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1514
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1515
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    sget-object v27, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mLeftIndentIconPath:Ljava/lang/String;

    sget-object v28, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mLeftIndentIconPath:Ljava/lang/String;

    .line 1516
    sget-object v29, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mLeftIndentIconPath:Ljava/lang/String;

    .line 1515
    invoke-virtual/range {v26 .. v29}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1519
    new-instance v25, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-direct/range {v25 .. v26}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentRightBtn:Landroid/widget/ImageButton;

    .line 1520
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 1521
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v25, v0

    const/high16 v26, 0x42300000    # 44.0f

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x41d80000    # 27.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    .line 1520
    move/from16 v0, v25

    move/from16 v1, v26

    invoke-direct {v6, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1522
    .local v6, "indentRightBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v25, 0x10

    move/from16 v0, v25

    iput v0, v6, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1523
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1524
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1526
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v26, v0

    const-string v27, "snote_popup_btn_normal"

    const-string v28, "snote_popup_btn_press"

    .line 1527
    const-string v29, "snote_popup_btn_press"

    .line 1526
    invoke-virtual/range {v25 .. v29}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v26, v0

    const-string v27, "snote_popup_btn_normal"

    const-string v28, "snote_popup_btn_press"

    .line 1529
    const-string v29, "snote_popup_btn_press"

    .line 1528
    invoke-virtual/range {v25 .. v29}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1531
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    sget-object v27, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mRightIndentIconPath:Ljava/lang/String;

    sget-object v28, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mRightIndentIconPath:Ljava/lang/String;

    .line 1532
    sget-object v29, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mRightIndentIconPath:Ljava/lang/String;

    .line 1531
    invoke-virtual/range {v26 .. v29}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1537
    new-instance v20, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1538
    .local v20, "paragraphSettingSpacingLayout":Landroid/widget/LinearLayout;
    new-instance v21, Landroid/widget/LinearLayout$LayoutParams;

    .line 1539
    const/16 v25, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x42580000    # 54.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    .line 1538
    move-object/from16 v0, v21

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1540
    .local v21, "paragraphSettingSpacingLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v25, v0

    const/high16 v26, 0x40c00000    # 6.0f

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1541
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v25, v0

    const/high16 v26, 0x40e00000    # 7.0f

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1542
    const/16 v25, 0x10

    move/from16 v0, v25

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1543
    invoke-virtual/range {v20 .. v21}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1544
    const/16 v25, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1546
    new-instance v23, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1547
    .local v23, "spacingTextView":Landroid/widget/TextView;
    new-instance v22, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v25, -0x1

    .line 1548
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x42580000    # 54.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    .line 1547
    move-object/from16 v0, v22

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1549
    .local v22, "spacingTextParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v25, 0x3f800000    # 1.0f

    move/from16 v0, v25

    move-object/from16 v1, v22

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1550
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-object/from16 v25, v0

    const-string v26, "string_line_spacing"

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1552
    const/high16 v25, -0x1000000

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1553
    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v26, v0

    const/high16 v27, 0x41700000    # 15.0f

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move-object/from16 v0, v23

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1554
    const/16 v25, 0x11

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 1555
    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1557
    new-instance v25, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-direct/range {v25 .. v26}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    .line 1558
    new-instance v24, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v25, -0x2

    .line 1559
    const/16 v26, -0x2

    .line 1558
    invoke-direct/range {v24 .. v26}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1560
    .local v24, "spinnerBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v25, v0

    const/high16 v26, 0x41300000    # 11.0f

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, v24

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1561
    const/16 v25, 0x10

    move/from16 v0, v25

    move-object/from16 v1, v24

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1563
    const/16 v25, 0x1e

    move/from16 v0, v25

    new-array v13, v0, [Ljava/lang/String;

    const/16 v25, 0x0

    const-string v26, "10"

    aput-object v26, v13, v25

    const/16 v25, 0x1

    const-string v26, "11"

    aput-object v26, v13, v25

    const/16 v25, 0x2

    const-string v26, "12"

    aput-object v26, v13, v25

    const/16 v25, 0x3

    const-string v26, "13"

    aput-object v26, v13, v25

    const/16 v25, 0x4

    const-string v26, "14"

    aput-object v26, v13, v25

    const/16 v25, 0x5

    const-string v26, "15"

    aput-object v26, v13, v25

    const/16 v25, 0x6

    const-string v26, "16"

    aput-object v26, v13, v25

    const/16 v25, 0x7

    const-string v26, "17"

    aput-object v26, v13, v25

    const/16 v25, 0x8

    const-string v26, "18"

    aput-object v26, v13, v25

    const/16 v25, 0x9

    const-string v26, "19"

    aput-object v26, v13, v25

    const/16 v25, 0xa

    const-string v26, "20"

    aput-object v26, v13, v25

    const/16 v25, 0xb

    const-string v26, "22"

    aput-object v26, v13, v25

    const/16 v25, 0xc

    const-string v26, "24"

    aput-object v26, v13, v25

    const/16 v25, 0xd

    const-string v26, "26"

    aput-object v26, v13, v25

    const/16 v25, 0xe

    const-string v26, "28"

    aput-object v26, v13, v25

    const/16 v25, 0xf

    .line 1564
    const-string v26, "30"

    aput-object v26, v13, v25

    const/16 v25, 0x10

    const-string v26, "32"

    aput-object v26, v13, v25

    const/16 v25, 0x11

    const-string v26, "36"

    aput-object v26, v13, v25

    const/16 v25, 0x12

    const-string v26, "40"

    aput-object v26, v13, v25

    const/16 v25, 0x13

    const-string v26, "43"

    aput-object v26, v13, v25

    const/16 v25, 0x14

    const-string v26, "44"

    aput-object v26, v13, v25

    const/16 v25, 0x15

    const-string v26, "48"

    aput-object v26, v13, v25

    const/16 v25, 0x16

    const-string v26, "52"

    aput-object v26, v13, v25

    const/16 v25, 0x17

    const-string v26, "56"

    aput-object v26, v13, v25

    const/16 v25, 0x18

    const-string v26, "60"

    aput-object v26, v13, v25

    const/16 v25, 0x19

    const-string v26, "64"

    aput-object v26, v13, v25

    const/16 v25, 0x1a

    const-string v26, "68"

    aput-object v26, v13, v25

    const/16 v25, 0x1b

    const-string v26, "72"

    aput-object v26, v13, v25

    const/16 v25, 0x1c

    const-string v26, "80"

    aput-object v26, v13, v25

    const/16 v25, 0x1d

    const-string v26, "88"

    aput-object v26, v13, v25

    .line 1565
    .local v13, "numbers":[Ljava/lang/String;
    new-instance v4, Landroid/widget/ArrayAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const v26, 0x1090008

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-direct {v4, v0, v1, v13}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 1567
    .local v4, "adpater":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1568
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v25, v0

    const/16 v26, 0x13

    invoke-virtual/range {v25 .. v26}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1569
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextLineSpacingSettingListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1570
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v25, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1574
    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1575
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1576
    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1577
    return-object v18
.end method

.method private playScrollAnimationForBottomBar(III)V
    .locals 8
    .param p1, "delay"    # I
    .param p2, "from"    # I
    .param p3, "to"    # I

    .prologue
    .line 2690
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 2691
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 2693
    :cond_0
    move v6, p2

    .line 2694
    .local v6, "fromFinal":I
    move v7, p3

    .line 2695
    .local v7, "toFinal":I
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollTimer:Ljava/util/Timer;

    .line 2696
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCount:I

    .line 2697
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$16;

    invoke-direct {v1, p0, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;II)V

    .line 2731
    const-wide/16 v2, 0xa

    int-to-long v4, p1

    .line 2697
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 2732
    return-void
.end method

.method private popupMaxButton()Landroid/view/View;
    .locals 10

    .prologue
    const/16 v5, 0x2a

    .line 1233
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->EXIT_BUTTON_TOP_MARGIN:F

    .line 1234
    .local v9, "topMargin":F
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->EXIT_BUTTON_RIGHT_MARGIN:F

    .line 1236
    .local v8, "rightMargin":F
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1237
    .local v1, "popupMaxButton":Landroid/widget/ImageButton;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1238
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->EXIT_BUTTON_WIDTH:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 1239
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->EXIT_BUTTON_HEIGHT:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1237
    invoke-direct {v7, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1240
    .local v7, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v0, 0xb

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1241
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1242
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1243
    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1244
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1247
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v2, "snote_popup_arrow_max_normal"

    const-string v3, "snote_popup_arrow_max_normal"

    const-string v4, "snote_popup_arrow_max_normal"

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1249
    return-object v1
.end method

.method private popupMinButton()Landroid/view/View;
    .locals 10

    .prologue
    const/16 v5, 0x2a

    .line 1253
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->EXIT_BUTTON_TOP_MARGIN:F

    .line 1254
    .local v9, "topMargin":F
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->EXIT_BUTTON_RIGHT_MARGIN:F

    .line 1256
    .local v8, "rightMargin":F
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1257
    .local v1, "popupMinButton":Landroid/widget/ImageButton;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1258
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->EXIT_BUTTON_WIDTH:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 1259
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->EXIT_BUTTON_HEIGHT:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1257
    invoke-direct {v7, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1260
    .local v7, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v0, 0xb

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1261
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1262
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1263
    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1264
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1267
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v2, "snote_popup_arrow_min_normal"

    const-string v3, "snote_popup_arrow_min_normal"

    const-string v4, "snote_popup_arrow_min_normal"

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1269
    return-object v1
.end method

.method private rotatePosition()V
    .locals 15

    .prologue
    const v14, 0x3f7d70a4    # 0.99f

    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 1896
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1898
    .local v4, "newMovableRect":Landroid/graphics/Rect;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTotalTopMargin:I

    if-eq v10, v11, :cond_2

    .line 1899
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTotalTopMargin:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTotalLeftMargin:I

    add-int/2addr v10, v11

    iput v10, v4, Landroid/graphics/Rect;->left:I

    .line 1903
    :goto_0
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTotalLeftMargin:I

    if-eq v10, v11, :cond_3

    .line 1904
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTotalLeftMargin:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTotalTopMargin:I

    add-int/2addr v10, v11

    iput v10, v4, Landroid/graphics/Rect;->top:I

    .line 1909
    :goto_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iput v10, v4, Landroid/graphics/Rect;->right:I

    .line 1910
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    iput v10, v4, Landroid/graphics/Rect;->bottom:I

    .line 1918
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 1919
    .local v6, "r":Landroid/graphics/Rect;
    const/4 v10, 0x2

    new-array v3, v10, [I

    .line 1920
    .local v3, "location":[I
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getLocationOnScreen([I)V

    .line 1922
    aget v10, v3, v12

    iput v10, v6, Landroid/graphics/Rect;->left:I

    .line 1923
    const/4 v10, 0x1

    aget v10, v3, v10

    iput v10, v6, Landroid/graphics/Rect;->top:I

    .line 1924
    iget v10, v6, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getWidth()I

    move-result v11

    add-int/2addr v10, v11

    iput v10, v6, Landroid/graphics/Rect;->right:I

    .line 1925
    iget v10, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getHeight()I

    move-result v11

    add-int/2addr v10, v11

    iput v10, v6, Landroid/graphics/Rect;->bottom:I

    .line 1929
    iget v10, v6, Landroid/graphics/Rect;->left:I

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    sub-int/2addr v10, v11

    int-to-float v2, v10

    .line 1930
    .local v2, "left":F
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    iget v11, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v10, v11

    int-to-float v7, v10

    .line 1931
    .local v7, "right":F
    iget v10, v6, Landroid/graphics/Rect;->top:I

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    sub-int/2addr v10, v11

    int-to-float v8, v10

    .line 1932
    .local v8, "top":F
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iget v11, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v10, v11

    int-to-float v0, v10

    .line 1934
    .local v0, "bottom":F
    add-float v10, v2, v7

    div-float v1, v2, v10

    .line 1935
    .local v1, "hRatio":F
    add-float v10, v8, v0

    div-float v9, v8, v10

    .line 1941
    .local v9, "vRatio":F
    cmpl-float v10, v1, v14

    if-lez v10, :cond_4

    .line 1942
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1947
    :cond_0
    :goto_2
    cmpl-float v10, v9, v14

    if-lez v10, :cond_5

    .line 1948
    const/high16 v9, 0x3f800000    # 1.0f

    .line 1953
    :cond_1
    :goto_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1955
    .local v5, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v11

    if-ge v10, v11, :cond_6

    .line 1956
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v1

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    iput v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1961
    :goto_4
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v11

    if-ge v10, v11, :cond_7

    .line 1962
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v9

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    iput v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1968
    :goto_5
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1969
    return-void

    .line 1901
    .end local v0    # "bottom":F
    .end local v1    # "hRatio":F
    .end local v2    # "left":F
    .end local v3    # "location":[I
    .end local v5    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v6    # "r":Landroid/graphics/Rect;
    .end local v7    # "right":F
    .end local v8    # "top":F
    .end local v9    # "vRatio":F
    :cond_2
    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTotalLeftMargin:I

    iput v10, v4, Landroid/graphics/Rect;->left:I

    goto/16 :goto_0

    .line 1906
    :cond_3
    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTotalTopMargin:I

    iput v10, v4, Landroid/graphics/Rect;->top:I

    goto/16 :goto_1

    .line 1943
    .restart local v0    # "bottom":F
    .restart local v1    # "hRatio":F
    .restart local v2    # "left":F
    .restart local v3    # "location":[I
    .restart local v6    # "r":Landroid/graphics/Rect;
    .restart local v7    # "right":F
    .restart local v8    # "top":F
    .restart local v9    # "vRatio":F
    :cond_4
    cmpg-float v10, v1, v13

    if-gez v10, :cond_0

    .line 1944
    const/4 v1, 0x0

    goto :goto_2

    .line 1949
    :cond_5
    cmpg-float v10, v9, v13

    if-gez v10, :cond_1

    .line 1950
    const/4 v9, 0x0

    goto :goto_3

    .line 1958
    .restart local v5    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_6
    iput v12, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_4

    .line 1964
    :cond_7
    iput v12, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_5
.end method

.method private setListener()V
    .locals 3

    .prologue
    .line 1099
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTitleLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1100
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTitleLayout:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1103
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    if-eqz v1, :cond_1

    .line 1104
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerColorChangeListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setColorPickerColorChangeListener(Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;)V

    .line 1109
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMaxButton:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 1110
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMaxButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1112
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMinButton:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 1113
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMinButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1115
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    if-eqz v1, :cond_4

    .line 1118
    const/4 v0, 0x0

    .local v0, "mTextOptButton":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_8

    .line 1128
    .end local v0    # "mTextOptButton":I
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteRightButton:Landroid/view/View;

    if-eqz v1, :cond_5

    .line 1129
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteRightButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1132
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteLeftButton:Landroid/view/View;

    if-eqz v1, :cond_6

    .line 1133
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteLeftButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1137
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    if-eqz v1, :cond_7

    .line 1138
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setOnScrollChangedListener(Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;)V

    .line 1142
    :cond_7
    return-void

    .line 1119
    .restart local v0    # "mTextOptButton":I
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v1, v1, v0

    if-eqz v1, :cond_9

    .line 1120
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v1, v1, v0

    instance-of v1, v1, Landroid/widget/ImageButton;

    if-eqz v1, :cond_9

    .line 1121
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextOptButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1118
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setTextStyle(Landroid/view/View;)V
    .locals 9
    .param p1, "paramView"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2274
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    if-nez v3, :cond_0

    .line 2311
    :goto_0
    return-void

    .line 2278
    :cond_0
    const/4 v1, 0x2

    .local v1, "mStyleButton":I
    :goto_1
    if-le v1, v6, :cond_3

    .line 2304
    :cond_1
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->getTextStyle()C

    move-result v2

    .line 2305
    .local v2, "textType":C
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v3, :cond_2

    .line 2306
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    .line 2307
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iput v2, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 2308
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 2310
    .end local v0    # "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->invalidate()V

    goto :goto_0

    .line 2279
    .end local v2    # "textType":C
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2280
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2281
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3, v4}, Landroid/view/View;->setSelected(Z)V

    .line 2282
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 2283
    if-ne v1, v7, :cond_4

    .line 2284
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewBold(Z)V

    goto :goto_2

    .line 2285
    :cond_4
    if-ne v1, v8, :cond_5

    .line 2286
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTextSkewX(Z)V

    goto :goto_2

    .line 2287
    :cond_5
    if-ne v1, v6, :cond_1

    .line 2288
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewUnderLine(Z)V

    goto :goto_2

    .line 2291
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3, v4}, Landroid/view/View;->setSelected(Z)V

    .line 2292
    invoke-virtual {p1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 2293
    if-ne v1, v7, :cond_7

    .line 2294
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewBold(Z)V

    goto :goto_2

    .line 2295
    :cond_7
    if-ne v1, v8, :cond_8

    .line 2296
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTextSkewX(Z)V

    goto :goto_2

    .line 2297
    :cond_8
    if-ne v1, v6, :cond_1

    .line 2298
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewUnderLine(Z)V

    goto :goto_2

    .line 2278
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private textPreview()Landroid/view/View;
    .locals 7

    .prologue
    const/high16 v5, 0x41500000    # 13.0f

    const/4 v6, -0x1

    .line 1281
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1282
    .local v0, "mPreviewLayout":Landroid/widget/LinearLayout;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 1283
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42b80000    # 92.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1282
    invoke-direct {v2, v6, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1284
    .local v2, "previewLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1285
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1286
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1287
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1289
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v4, "snote_popup_preview_bg"

    invoke-virtual {v3, v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1292
    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    .line 1294
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 1295
    const/16 v3, 0x11

    .line 1294
    invoke-direct {v1, v6, v6, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 1296
    .local v1, "mPreviewParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, -0x3f600000    # -5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1297
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1298
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1299
    return-object v0
.end method

.method private titleBg()Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 856
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 857
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 863
    .local v1, "titleBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 865
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v3, "vienna_popup_title_bg"

    invoke-virtual {v2, v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 867
    return-object v0
.end method

.method private titleLayout()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 831
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 832
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 833
    const/high16 v4, 0x42280000    # 42.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 832
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 835
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->popupMaxButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMaxButton:Landroid/view/View;

    .line 836
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->popupMinButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMinButton:Landroid/view/View;

    .line 837
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->titleBg()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 838
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->titleText()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 841
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 842
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 843
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMaxButton:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 844
    return-object v0
.end method

.method private titleText()Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/16 v2, 0xfa

    const/4 v3, 0x0

    .line 871
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 872
    .local v0, "titleView":Landroid/widget/TextView;
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 873
    invoke-static {v2, v2, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 874
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 875
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 876
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 877
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 878
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_text_settings"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 879
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 880
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_text_settings"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 881
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41500000    # 13.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 882
    return-object v0
.end method

.method private totalLayout()V
    .locals 3

    .prologue
    .line 810
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v2, 0x43a48000    # 329.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 811
    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 810
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->totalLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 812
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->totalLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 814
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setOrientation(I)V

    .line 816
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->titleLayout()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTitleLayout:Landroid/view/View;

    .line 817
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->bodyLayout()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayout:Landroid/view/View;

    .line 818
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->addView(Landroid/view/View;)V

    .line 819
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->addView(Landroid/view/View;)V

    .line 820
    return-void
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 964
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    if-nez v1, :cond_0

    .line 1070
    :goto_0
    return-void

    .line 968
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 969
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollHandle:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 970
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollHandle:Landroid/widget/ImageView;

    .line 971
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 972
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    .line 973
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 974
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    .line 975
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 976
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_5

    .line 981
    .end local v0    # "i":I
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    .line 982
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 983
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    .line 984
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 985
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    .line 986
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 987
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerView:Landroid/widget/Button;

    .line 989
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 990
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    .line 991
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 992
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    .line 993
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    if-eqz v1, :cond_2

    .line 994
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->close()V

    .line 995
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 996
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    .line 998
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    if-eqz v1, :cond_3

    .line 999
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->close()V

    .line 1000
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1001
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    .line 1003
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1004
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    .line 1005
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1006
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    .line 1007
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1008
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    .line 1009
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentLeftBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1010
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentLeftBtn:Landroid/widget/ImageButton;

    .line 1011
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentRightBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1012
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentRightBtn:Landroid/widget/ImageButton;

    .line 1016
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1017
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSettingPreview:Landroid/view/View;

    .line 1018
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1019
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectedAndPicker:Landroid/view/View;

    .line 1021
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteRightButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1022
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteRightButton:Landroid/view/View;

    .line 1023
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteLeftButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1024
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteLeftButton:Landroid/view/View;

    .line 1025
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteView:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1026
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteView:Landroid/view/View;

    .line 1027
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPickerView:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1028
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPickerView:Landroid/view/View;

    .line 1029
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1030
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteBg:Landroid/view/View;

    .line 1032
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1033
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mParagraphSetting:Landroid/view/View;

    .line 1034
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1035
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    .line 1036
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1037
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    .line 1038
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1039
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    .line 1040
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerColor:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1041
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerColor:Landroid/view/View;

    .line 1042
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSettingExitButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1043
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSettingExitButton:Landroid/view/View;

    .line 1045
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1046
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTitleLayout:Landroid/view/View;

    .line 1047
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyBg:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1048
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyBg:Landroid/view/View;

    .line 1049
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1050
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayout:Landroid/view/View;

    .line 1052
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->close()V

    .line 1053
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1054
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    .line 1055
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 1056
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    .line 1057
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 1058
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    .line 1059
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1060
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    .line 1063
    :cond_4
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 1064
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1065
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1067
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->close()V

    .line 1068
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 1069
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerShow:Z

    goto/16 :goto_0

    .line 977
    .restart local v0    # "i":I
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 978
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aput-object v3, v1, v0

    .line 976
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method

.method public getInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    .prologue
    .line 2334
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    return-object v0
.end method

.method protected getSizeOption()I
    .locals 1

    .prologue
    .line 2541
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mViewMode:I

    return v0
.end method

.method public getViewMode()I
    .locals 1

    .prologue
    .line 2089
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mViewMode:I

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 50
    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFirstLongPress:Z

    if-nez v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 53
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    .line 54
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->rotatePosition()V

    .line 57
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 65
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    if-eqz v1, :cond_1

    .line 66
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->rotatePosition()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :cond_1
    :goto_0
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onLayoutChange()V
    .locals 1

    .prologue
    .line 2600
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->getScrollY()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->scroll(F)V

    .line 2602
    return-void
.end method

.method protected onScroll(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 2620
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 8
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 2008
    if-ne p1, p0, :cond_0

    if-nez p2, :cond_0

    .line 2010
    const/4 v1, 0x2

    new-array v0, v1, [I

    .line 2011
    .local v0, "location":[I
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getLocationOnScreen([I)V

    .line 2013
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2015
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mMovableRect:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    aget v3, v0, v5

    aget v4, v0, v7

    aget v5, v0, v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getWidth()I

    move-result v6

    add-int/2addr v5, v6

    aget v6, v0, v7

    .line 2016
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getHeight()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2015
    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v1

    .line 2016
    if-nez v1, :cond_0

    .line 2017
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->checkPosition()V

    .line 2020
    .end local v0    # "location":[I
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 2021
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 2737
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    if-nez v1, :cond_1

    .line 2754
    :cond_0
    :goto_0
    return-void

    .line 2740
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 2743
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2744
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->hide()V

    .line 2745
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v1, :cond_2

    .line 2746
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v2, 0x2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPreCanvasPenAction:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 2747
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPreCanvasFingerAction:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2753
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowVisibilityChanged(I)V

    goto :goto_0

    .line 2750
    :catch_0
    move-exception v0

    .line 2751
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method protected scroll(F)V
    .locals 1
    .param p1, "scrollYPosition"    # F

    .prologue
    .line 2583
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-gez v0, :cond_0

    .line 2585
    const/4 p1, 0x0

    .line 2587
    :cond_0
    return-void
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$ActionListener;

    .prologue
    .line 2529
    return-void
.end method

.method public setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 0
    .param p1, "canvasView"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .prologue
    .line 2566
    if-eqz p1, :cond_0

    .line 2567
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 2569
    :cond_0
    return-void
.end method

.method public setColorPickerPosition(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1226
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    if-eqz v0, :cond_0

    .line 1227
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->movePosition(II)V

    .line 1230
    :cond_0
    return-void
.end method

.method setExpandBarPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 2259
    return-void
.end method

.method public setIndicatorPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 1180
    return-void
.end method

.method public setInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 12
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x2

    const-wide v8, 0x4076800000000000L    # 360.0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2362
    if-nez p1, :cond_0

    .line 2510
    :goto_0
    return-void

    .line 2366
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v4, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 2367
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v4, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 2368
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v4, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 2369
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v4, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 2370
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v4, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 2371
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v4, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 2372
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v4, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    .line 2373
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v4, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    .line 2374
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v3, :cond_1

    .line 2375
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v1

    .line 2376
    .local v1, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 2377
    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 2378
    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 2379
    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 2380
    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 2381
    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 2382
    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    .line 2383
    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    .line 2384
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 2387
    .end local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTextColor(I)V

    .line 2388
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTextSize(F)V

    .line 2389
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v4}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTypeface(Landroid/graphics/Typeface;)V

    .line 2390
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->setColorPickerColor(I)V

    .line 2392
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontName:Ljava/lang/String;

    .line 2393
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2394
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    int-to-double v4, v4

    div-double/2addr v4, v8

    double-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    if-gtz v3, :cond_3

    .line 2395
    const-string v3, ""

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontSize:Ljava/lang/String;

    .line 2400
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontSize:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2426
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    if-nez v3, :cond_4

    .line 2427
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2428
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2429
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2440
    :cond_2
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setColor(I)V

    .line 2442
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v3, :cond_6

    .line 2443
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    .line 2460
    :goto_3
    const/4 v2, 0x2

    .local v2, "mStyleButton":I
    :goto_4
    if-le v2, v11, :cond_7

    .line 2502
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getCount()I

    move-result v3

    if-lt v0, v3, :cond_b

    .line 2509
    :goto_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->invalidate()V

    goto/16 :goto_0

    .line 2397
    .end local v0    # "i":I
    .end local v2    # "mStyleButton":I
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    int-to-double v4, v4

    div-double/2addr v4, v8

    double-to-float v4, v4

    div-float/2addr v3, v4

    const/high16 v4, 0x3f000000    # 0.5f

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontSize:Ljava/lang/String;

    goto :goto_1

    .line 2430
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    if-ne v3, v10, :cond_5

    .line 2431
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2432
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2433
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_2

    .line 2434
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    if-ne v3, v7, :cond_2

    .line 2435
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignLeftBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2436
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignCenterBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2437
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mAlignRightBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_2

    .line 2445
    :cond_6
    const/16 v3, 0x404

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    goto :goto_3

    .line 2461
    .restart local v2    # "mStyleButton":I
    :cond_7
    packed-switch v2, :pswitch_data_0

    .line 2460
    :goto_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 2463
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v7, :cond_8

    .line 2464
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v2

    invoke-virtual {v3, v7}, Landroid/view/View;->setSelected(Z)V

    .line 2465
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2466
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewBold(Z)V

    goto :goto_7

    .line 2468
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v2

    invoke-virtual {v3, v6}, Landroid/view/View;->setSelected(Z)V

    .line 2469
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2470
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewBold(Z)V

    goto :goto_7

    .line 2475
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v10, :cond_9

    .line 2476
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v2

    invoke-virtual {v3, v7}, Landroid/view/View;->setSelected(Z)V

    .line 2477
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2478
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTextSkewX(Z)V

    goto :goto_7

    .line 2480
    :cond_9
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v2

    invoke-virtual {v3, v6}, Landroid/view/View;->setSelected(Z)V

    .line 2481
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2482
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTextSkewX(Z)V

    goto :goto_7

    .line 2487
    :pswitch_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v11, :cond_a

    .line 2488
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v2

    invoke-virtual {v3, v7}, Landroid/view/View;->setSelected(Z)V

    .line 2489
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2490
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewUnderLine(Z)V

    goto :goto_7

    .line 2492
    :cond_a
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v2

    invoke-virtual {v3, v6}, Landroid/view/View;->setSelected(Z)V

    .line 2493
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2494
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewUnderLine(Z)V

    goto/16 :goto_7

    .line 2503
    .restart local v0    # "i":I
    :cond_b
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    int-to-float v3, v3

    cmpl-float v3, v4, v3

    if-nez v3, :cond_c

    .line 2504
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_6

    .line 2502
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_5

    .line 2461
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setLayoutHeight(I)V
    .locals 1
    .param p1, "height"    # I

    .prologue
    .line 2683
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2684
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2685
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2686
    return-void
.end method

.method public setPopup(Z)V
    .locals 4
    .param p1, "open"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 438
    if-eqz p1, :cond_0

    .line 439
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 440
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 443
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getHeight()I

    move-result v0

    .line 444
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x43ba0000    # 372.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 443
    invoke-direct {p0, v3, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->playScrollAnimationForBottomBar(III)V

    .line 452
    :goto_0
    return-void

    .line 446
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 447
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 450
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42280000    # 42.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-direct {p0, v3, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->playScrollAnimationForBottomBar(III)V

    goto :goto_0
.end method

.method public setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

    .prologue
    .line 245
    if-eqz p1, :cond_0

    .line 246
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

    .line 248
    :cond_0
    return-void
.end method

.method public setPosition(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1199
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1201
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1202
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1204
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1206
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mLeftMargin:I

    sub-int/2addr v1, p1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mLeftMargin:I

    .line 1207
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTopMargin:I

    sub-int/2addr v1, p2

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTopMargin:I

    .line 1208
    return-void
.end method

.method public setViewMode(I)V
    .locals 8
    .param p1, "viewMode"    # I

    .prologue
    const/high16 v7, 0x421c0000    # 39.0f

    const/high16 v6, 0x41880000    # 17.0f

    const/high16 v5, 0x42280000    # 42.0f

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2117
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mViewMode:I

    .line 2118
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2119
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->hide()V

    .line 2120
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_0

    .line 2121
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x2

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPreCanvasPenAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 2122
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPreCanvasFingerAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 2125
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mViewMode:I

    packed-switch v0, :pswitch_data_0

    .line 2239
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mViewMode:I

    .line 2240
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2241
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2242
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2243
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2244
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 2245
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->invalidate()V

    .line 2246
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2247
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2248
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2250
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2251
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setVisibility(I)V

    .line 2252
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2256
    :cond_1
    :goto_0
    return-void

    .line 2127
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2128
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2129
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2130
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2131
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 2132
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->invalidate()V

    .line 2133
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2134
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2135
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2137
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2138
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setVisibility(I)V

    .line 2139
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2140
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 2141
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x43a50000    # 330.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setExpandBarPosition(I)V

    .line 2143
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayoutHeight:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 2144
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    .line 2145
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScale:F

    div-float v2, v6, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    sub-int/2addr v0, v1

    .line 2146
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    sub-int/2addr v0, v1

    .line 2144
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 2150
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2151
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2152
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2153
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2154
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 2155
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2156
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2157
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2159
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2160
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setVisibility(I)V

    .line 2161
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2162
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 2166
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2167
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2168
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2169
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2170
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 2171
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2172
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2173
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2175
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2176
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setVisibility(I)V

    .line 2177
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2178
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 2182
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2183
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2184
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2185
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2186
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 2187
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2188
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2189
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2191
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2192
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setVisibility(I)V

    .line 2193
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2194
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 2198
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2199
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2200
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2201
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 2202
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2203
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2204
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2206
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2207
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2208
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setVisibility(I)V

    .line 2209
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2210
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x42f00000    # 120.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 2213
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2214
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2215
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2216
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2217
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 2218
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2219
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2220
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2222
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2223
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setVisibility(I)V

    .line 2224
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2225
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 2229
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2230
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2231
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mBodyLayoutHeight:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 2232
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    .line 2233
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScale:F

    div-float v2, v6, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    sub-int/2addr v0, v1

    .line 2234
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    sub-int/2addr v0, v1

    .line 2232
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 2125
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setVisibility(I)V
    .locals 6
    .param p1, "visibility"    # I

    .prologue
    const-wide v4, 0x4076800000000000L    # 360.0

    .line 2639
    const/4 v0, 0x1

    .line 2640
    .local v0, "mIsInit":Z
    if-nez p1, :cond_1

    .line 2641
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-nez v1, :cond_1

    .line 2642
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->loadImage()V

    .line 2645
    if-eqz v0, :cond_1

    .line 2646
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v1, :cond_0

    .line 2647
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    .line 2650
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontName:Ljava/lang/String;

    .line 2651
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2652
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2654
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    int-to-double v2, v2

    div-double/2addr v2, v4

    double-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    if-gtz v1, :cond_2

    .line 2655
    const-string v1, ""

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontSize:Ljava/lang/String;

    .line 2661
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontSize:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2663
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTextSize(F)V

    .line 2664
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTypeface(Landroid/graphics/Typeface;)V

    .line 2665
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->invalidate()V

    .line 2669
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    if-nez v1, :cond_3

    .line 2680
    :goto_1
    return-void

    .line 2657
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    int-to-double v2, v2

    div-double/2addr v2, v4

    double-to-float v2, v2

    div-float/2addr v1, v2

    .line 2658
    const/high16 v2, 0x3f000000    # 0.5f

    sub-float/2addr v1, v2

    .line 2657
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontSize:Ljava/lang/String;

    goto :goto_0

    .line 2672
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2673
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->hide()V

    .line 2674
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v1, :cond_4

    .line 2675
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v2, 0x2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPreCanvasPenAction:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 2676
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPreCanvasFingerAction:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 2679
    :cond_4
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method protected textSettingScroll()Landroid/view/View;
    .locals 12

    .prologue
    const/high16 v11, 0x40f00000    # 7.5f

    const/4 v10, 0x0

    const/4 v9, -0x1

    const/high16 v8, 0x40400000    # 3.0f

    .line 2032
    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;-><init>(Landroid/content/Context;)V

    .line 2033
    .local v3, "localThumbControlBackGround":Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40800000    # 4.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->setTrackWidth(I)V

    .line 2034
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->setTopPadding(I)V

    .line 2035
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2036
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41c80000    # 25.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 2035
    invoke-direct {v0, v4, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2037
    .local v0, "localLayoutParams1":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2038
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41000000    # 8.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2039
    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2040
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 2041
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2042
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 2040
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->setPadding(IIII)V

    .line 2044
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mOptionBgPath:Ljava/lang/String;

    invoke-virtual {v4, v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 2045
    new-instance v2, Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2046
    .local v2, "localRelativeLayout":Landroid/widget/RelativeLayout;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2047
    invoke-direct {v4, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2046
    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2048
    new-instance v4, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollHandle:Landroid/widget/ImageView;

    .line 2049
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2050
    const/4 v4, -0x2

    .line 2049
    invoke-direct {v1, v9, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2051
    .local v1, "localLayoutParams2":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollHandle:Landroid/widget/ImageView;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2052
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollHandle:Landroid/widget/ImageView;

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2053
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollHandle:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v4, v10, v5, v10, v10}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 2054
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollHandle:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollHandelNormal:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2055
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollHandle:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2056
    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->addView(Landroid/view/View;)V

    .line 2057
    return-object v3
.end method
