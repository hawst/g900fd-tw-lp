.class Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;
.super Ljava/lang/Object;
.source "TextRecognitionPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TextRecognitionRunnable"
.end annotation


# instance fields
.field private mInput:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p2, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;->mInput:Ljava/util/List;

    .line 104
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;->mInput:Ljava/util/List;

    .line 105
    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 110
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;->mInput:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_2

    .line 129
    const/4 v3, 0x0

    .line 130
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->access$1(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->process()[Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "candidates":[Ljava/lang/String;
    if-eqz v0, :cond_1

    array-length v10, v0

    if-lez v10, :cond_1

    .line 132
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 134
    .restart local v3    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    new-instance v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    aget-object v10, v0, v13

    invoke-direct {v9, v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;-><init>(Ljava/lang/String;)V

    .line 136
    .local v9, "text":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    .end local v9    # "text":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->access$2(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)Landroid/os/Handler;

    move-result-object v10

    new-instance v11, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$HandleInfo;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;->mInput:Ljava/util/List;

    invoke-direct {v11, v12, v3}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$HandleInfo;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v10, v13, v11}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 140
    .local v4, "msg":Landroid/os/Message;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->access$2(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)Landroid/os/Handler;

    move-result-object v10

    invoke-virtual {v10, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 141
    return-void

    .line 110
    .end local v0    # "candidates":[Ljava/lang/String;
    .end local v3    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    .end local v4    # "msg":Landroid/os/Message;
    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 111
    .local v5, "obj":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .end local v5    # "obj":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v6

    .line 112
    .local v6, "points":[Landroid/graphics/PointF;
    if-eqz v6, :cond_0

    .line 113
    array-length v2, v6

    .line 114
    .local v2, "length":I
    const/16 v11, 0x400

    if-le v2, v11, :cond_3

    .line 115
    const/16 v2, 0x400

    .line 117
    :cond_3
    if-lez v2, :cond_0

    .line 118
    new-array v7, v2, [F

    .line 119
    .local v7, "ptx":[F
    new-array v8, v2, [F

    .line 120
    .local v8, "pty":[F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_4

    .line 124
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->access$1(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    move-result-object v11

    invoke-virtual {v11, v7, v8}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->addStroke([F[F)V

    goto :goto_0

    .line 121
    :cond_4
    aget-object v11, v6, v1

    iget v11, v11, Landroid/graphics/PointF;->x:F

    aput v11, v7, v1

    .line 122
    aget-object v11, v6, v1

    iget v11, v11, Landroid/graphics/PointF;->y:F

    aput v11, v8, v1

    .line 120
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
