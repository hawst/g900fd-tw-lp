.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;
.super Ljava/lang/Object;
.source "NRRSignatureRecognitionPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SignatureRecognitionTask"
.end annotation


# instance fields
.field private final mSpenObjectStrokes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;"
        }
    .end annotation
.end field

.field private final mStrictLevel:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;Ljava/util/List;I)V
    .locals 1
    .param p3, "strictLevel"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p2, "spenObjectStrokes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mSpenObjectStrokes:Ljava/util/List;

    .line 106
    packed-switch p3, :pswitch_data_0

    .line 118
    :pswitch_0
    sget-object v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_Medium:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mStrictLevel:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    .line 120
    :goto_0
    return-void

    .line 108
    :pswitch_1
    sget-object v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_Low:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mStrictLevel:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    goto :goto_0

    .line 112
    :pswitch_2
    sget-object v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_High:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mStrictLevel:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    goto :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;Ljava/util/List;ILcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;Ljava/util/List;I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 124
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mSpenObjectStrokes:Ljava/util/List;

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->convertSpenObjectStrokesToSignature(Ljava/util/List;)Lcom/samsung/recognitionengine/Signature;

    move-result-object v1

    .line 125
    .local v1, "sig":Lcom/samsung/recognitionengine/Signature;
    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mModel:Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->access$0()Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->getVerifier()Lcom/samsung/recognitionengine/Verifier;

    move-result-object v2

    .line 128
    .local v2, "verifier":Lcom/samsung/recognitionengine/Verifier;
    if-nez v2, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 135
    .local v0, "result":Z
    :goto_0
    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->access$1()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "result: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mUiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->access$2(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mSpenObjectStrokes:Ljava/util/List;

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v0, v7}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;Ljava/util/List;ZLcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 138
    return-void

    .line 131
    .end local v0    # "result":Z
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mStrictLevel:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    invoke-virtual {v2, v3}, Lcom/samsung/recognitionengine/Verifier;->setStrictnessLevel(Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;)V

    .line 132
    invoke-virtual {v2, v1}, Lcom/samsung/recognitionengine/Verifier;->isAuthentic(Lcom/samsung/recognitionengine/Signature;)Z

    move-result v0

    .restart local v0    # "result":Z
    goto :goto_0
.end method
