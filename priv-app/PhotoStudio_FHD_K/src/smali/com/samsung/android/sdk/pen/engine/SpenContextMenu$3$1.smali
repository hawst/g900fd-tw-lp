.class Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;
.super Ljava/lang/Object;
.source "SpenContextMenu.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

.field private final synthetic val$lvWidthAct:F

.field private final synthetic val$step:F


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;FF)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->val$lvWidthAct:F

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->val$step:F

    .line 916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 919
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->val$lvWidthAct:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->val$step:F

    mul-float/2addr v2, v3

    sub-float v0, v1, v2

    .line 920
    .local v0, "pos":F
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v2, v3

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;F)V

    .line 921
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 922
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-result-object v1

    float-to-int v2, v0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->scrollTo(I)V

    .line 930
    :cond_0
    :goto_0
    return-void

    .line 924
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->scrollTo(I)V

    .line 925
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Ljava/util/Timer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 926
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 927
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$23(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Ljava/util/Timer;)V

    goto :goto_0
.end method
