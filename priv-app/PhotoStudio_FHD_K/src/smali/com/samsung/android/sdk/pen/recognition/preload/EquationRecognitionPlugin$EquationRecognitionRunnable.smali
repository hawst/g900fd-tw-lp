.class Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;
.super Ljava/lang/Object;
.source "EquationRecognitionPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EquationRecognitionRunnable"
.end annotation


# instance fields
.field private mInput:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p2, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->mInput:Ljava/util/List;

    .line 92
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->mInput:Ljava/util/List;

    .line 93
    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 97
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->mInput:Ljava/util/List;

    if-nez v10, :cond_0

    .line 98
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "E_INVALID_ARG : parameter \'input\' is null"

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 100
    :cond_0
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->mInput:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_3

    .line 121
    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->access$1()Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->process()Ljava/lang/String;

    move-result-object v8

    .line 122
    .local v8, "result":Ljava/lang/String;
    const/4 v4, 0x0

    .line 124
    .local v4, "output":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-eqz v8, :cond_2

    .line 125
    const-string v10, "recognition-EquationRecognitionPlugin"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "result = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "output":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .restart local v4    # "output":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    new-instance v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-direct {v9, v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;-><init>(Ljava/lang/String;)V

    .line 131
    .local v9, "text":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    .end local v9    # "text":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    :cond_2
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->access$2(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x0

    new-instance v12, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->mInput:Ljava/util/List;

    invoke-direct {v12, v13, v4}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v10, v11, v12}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 135
    .local v2, "msg":Landroid/os/Message;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->access$2(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)Landroid/os/Handler;

    move-result-object v10

    invoke-virtual {v10, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 136
    return-void

    .line 100
    .end local v2    # "msg":Landroid/os/Message;
    .end local v4    # "output":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    .end local v8    # "result":Ljava/lang/String;
    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 101
    .local v3, "obj":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .end local v3    # "obj":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v7

    .line 102
    .local v7, "points":[Landroid/graphics/PointF;
    if-eqz v7, :cond_1

    .line 103
    array-length v1, v7

    .line 104
    .local v1, "length":I
    const/16 v11, 0x400

    if-le v1, v11, :cond_4

    .line 105
    const/16 v1, 0x400

    .line 107
    :cond_4
    if-lez v1, :cond_1

    .line 108
    new-array v5, v1, [F

    .line 109
    .local v5, "pointX":[F
    new-array v6, v1, [F

    .line 111
    .local v6, "pointY":[F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_5

    .line 116
    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->access$1()Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    move-result-object v11

    invoke-virtual {v11, v5, v6}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->addStroke([F[F)V

    goto :goto_0

    .line 112
    :cond_5
    aget-object v11, v7, v0

    iget v11, v11, Landroid/graphics/PointF;->x:F

    aput v11, v5, v0

    .line 113
    aget-object v11, v7, v0

    iget v11, v11, Landroid/graphics/PointF;->y:F

    aput v11, v6, v0

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
