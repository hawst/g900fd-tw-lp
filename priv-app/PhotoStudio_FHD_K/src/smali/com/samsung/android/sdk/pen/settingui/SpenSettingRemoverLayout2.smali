.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;
.super Landroid/widget/LinearLayout;
.source "SpenSettingRemoverLayout2.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$ActionListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$RptUpdater;
    }
.end annotation


# static fields
.field protected static final BODY_LAYOUT_HEIGHT:I = 0xc2

.field protected static final ERASER_PROGRESS_MAX:I = 0x9

.field protected static final ERASER_SIZE_MAX:I = 0xa

.field protected static final EXIT_BUTTON_RAW_HEIGHT:I = 0x24

.field protected static final EXIT_BUTTON_RAW_WIDTH:I = 0x26

.field protected static final LINE_BUTTON_RAW_HEIGHT:I = 0x11

.field protected static final LINE_BUTTON_RAW_WIDTH:I = 0x1

.field private static final REP_DELAY:I = 0x14

.field private static final TAG:Ljava/lang/String; = "settingui-settingEraser"

.field protected static final TITLE_LAYOUT_HEIGHT:I = 0x2a

.field public static final VIEW_MODE_NORMAL:I = 0x0

.field public static final VIEW_MODE_SIZE:I = 0x2

.field public static final VIEW_MODE_TITLE:I = 0x3

.field public static final VIEW_MODE_TYPE:I = 0x1

.field private static final bodyLeftPath:Ljava/lang/String; = "vienna_popup_bg"

.field private static final btnFocusPath:Ljava/lang/String; = "snote_popup_btn_focus"

.field private static final btnNoramlPath:Ljava/lang/String; = "snote_popup_btn_normal"

.field private static final btnPressPath:Ljava/lang/String; = "snote_popup_btn_press"

.field private static final eraserPopupDrawPress:Ljava/lang/String; = "snote_eraser_popup_draw_press"

.field private static final eraserPopupDrawUnselect:Ljava/lang/String; = "snote_eraser_popup_draw"

.field private static final eraserPopupTextPress:Ljava/lang/String; = "snote_eraser_popup_text_press"

.field private static final eraserPopupTextUnselect:Ljava/lang/String; = "snote_eraser_popup_text"

.field private static final handelFocusPath:Ljava/lang/String; = "progress_handle_focus"

.field private static final handelPath:Ljava/lang/String; = "progress_handle_normal"

.field private static final handelPressPath:Ljava/lang/String; = "progress_handle_press"

.field private static final mSdkVersion:I

.field private static final minusBgDimPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_dim"

.field private static final minusBgFocusPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_focus"

.field private static final minusBgPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_normal"

.field private static final minusBgPressPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_press"

.field private static final plusBgDimPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_dim"

.field private static final plusBgFocusPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_focus"

.field private static final plusBgPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_normal"

.field private static final plusBgPressPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_press"

.field private static final popupMaxPath:Ljava/lang/String; = "snote_popup_arrow_max_normal"

.field private static final popupMinPath:Ljava/lang/String; = "snote_popup_arrow_min_normal"

.field private static final progressBgPath:Ljava/lang/String; = "progress_bg"

.field private static final progressShadowPath:Ljava/lang/String; = "progress_shadow"

.field private static final titleLeftPath:Ljava/lang/String; = "vienna_popup_title_bg"


# instance fields
.field private mAutoDecrement:Z

.field private mAutoIncrement:Z

.field protected mBodyLayout:Landroid/view/View;

.field protected mCanvasLayout:Landroid/widget/RelativeLayout;

.field protected mCanvasSize:I

.field protected mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

.field private final mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field protected mClearAllButton:Landroid/view/View;

.field private final mClearAllListener:Landroid/view/View$OnClickListener;

.field private mCount:I

.field protected mCurrentCutterType:I

.field protected mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field private mCutterTypeChekBox:Landroid/view/View;

.field protected mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private final mEraserKeyListener:Landroid/view/View$OnKeyListener;

.field protected mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;

.field protected mEraserMinusButton:Landroid/view/View;

.field protected mEraserPlusButton:Landroid/view/View;

.field protected mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

.field private final mEraserSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field protected mEraserSizeSeekBar:Landroid/widget/SeekBar;

.field protected mEraserSizeTextView:Landroid/widget/TextView;

.field protected mEraserType01:Landroid/widget/ImageButton;

.field protected mEraserType02:Landroid/widget/ImageButton;

.field protected mEraserTypeLayout:Landroid/view/ViewGroup;

.field private final mEraserTypeListner:Landroid/view/View$OnClickListener;

.field protected mEraserTypeView:[Landroid/view/View;

.field protected mFirstLongPress:Z

.field private final mHandler:Landroid/os/Handler;

.field private mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field protected mLeftMargin:I

.field private final mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

.field private final mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

.field private final mMinusButtonListener:Landroid/view/View$OnClickListener;

.field protected mMovableRect:Landroid/graphics/Rect;

.field private final mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

.field private final mPlusButtonListener:Landroid/view/View$OnClickListener;

.field private final mPopupButtonListener:Landroid/view/View$OnClickListener;

.field private mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

.field protected mPopupMaxButton:Landroid/view/View;

.field protected mPopupMinButton:Landroid/view/View;

.field protected mRemoverContext:Landroid/content/Context;

.field private mScrollTimer:Ljava/util/Timer;

.field protected mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field protected mSettingSizeLayout:Landroid/view/ViewGroup;

.field protected mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field protected mTitleLayout:Landroid/view/View;

.field private mTitleView:Landroid/widget/TextView;

.field protected mTopMargin:I

.field protected mTotalLeftMargin:I

.field protected mTotalTopMargin:I

.field private final mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

.field private final mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

.field protected mViewMode:I

.field private final repeatUpdateHandler:Landroid/os/Handler;

.field protected requestLayoutDisable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 127
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSdkVersion:I

    .line 1992
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p3, "relativeLayout"    # Landroid/widget/RelativeLayout;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 660
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 71
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

    .line 72
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->requestLayoutDisable:Z

    .line 99
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 100
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 101
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCurrentCutterType:I

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mFirstLongPress:Z

    .line 110
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 112
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;

    .line 118
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mViewMode:I

    .line 119
    const/16 v0, 0x438

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    .line 130
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mHandler:Landroid/os/Handler;

    .line 182
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 191
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mClearAllListener:Landroid/view/View$OnClickListener;

    .line 323
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 396
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    .line 411
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 429
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 451
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->repeatUpdateHandler:Landroid/os/Handler;

    .line 452
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mAutoIncrement:Z

    .line 453
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mAutoDecrement:Z

    .line 481
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 493
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    .line 506
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 519
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    .line 533
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 603
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeListner:Landroid/view/View$OnClickListener;

    .line 611
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserKeyListener:Landroid/view/View$OnKeyListener;

    .line 661
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->requestLayoutDisable:Z

    .line 662
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 663
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 664
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 665
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    .line 666
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 667
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->initView()V

    .line 668
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setListener()V

    .line 669
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    .line 670
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p3, "relativeLayout"    # Landroid/widget/RelativeLayout;
    .param p4, "ratio"    # F

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 698
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 71
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

    .line 72
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->requestLayoutDisable:Z

    .line 99
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 100
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 101
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCurrentCutterType:I

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mFirstLongPress:Z

    .line 110
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 112
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;

    .line 118
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mViewMode:I

    .line 119
    const/16 v0, 0x438

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    .line 130
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mHandler:Landroid/os/Handler;

    .line 182
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 191
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mClearAllListener:Landroid/view/View$OnClickListener;

    .line 323
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 396
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    .line 411
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 429
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 451
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->repeatUpdateHandler:Landroid/os/Handler;

    .line 452
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mAutoIncrement:Z

    .line 453
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mAutoDecrement:Z

    .line 481
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 493
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    .line 506
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 519
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    .line 533
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 603
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeListner:Landroid/view/View$OnClickListener;

    .line 611
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserKeyListener:Landroid/view/View$OnKeyListener;

    .line 699
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->requestLayoutDisable:Z

    .line 700
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, p1, p2, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 701
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 702
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 703
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    .line 704
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 705
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->initView()V

    .line 706
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setListener()V

    .line 707
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    .line 708
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;Z)V
    .locals 0

    .prologue
    .line 280
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setEnableSizeSeekbar(Z)V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;I)V
    .locals 0

    .prologue
    .line 128
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCount:I

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mScrollTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;Z)V
    .locals 0

    .prologue
    .line 452
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mAutoIncrement:Z

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->repeatUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)Z
    .locals 1

    .prologue
    .line 452
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mAutoIncrement:Z

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;Z)V
    .locals 0

    .prologue
    .line 453
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mAutoDecrement:Z

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)Z
    .locals 1

    .prologue
    .line 453
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mAutoDecrement:Z

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1496
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->eraserTypeSetting(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCount:I

    return v0
.end method

.method private bodyBg()Landroid/view/ViewGroup;
    .locals 7

    .prologue
    .line 869
    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 870
    .local v2, "layout":Landroid/widget/LinearLayout;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    .line 871
    const/4 v5, -0x2

    .line 870
    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 873
    .local v3, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 875
    new-instance v0, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 876
    .local v0, "bodyLeft":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 877
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v5, 0x43a48000    # 329.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x43100000    # 144.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 876
    invoke-direct {v1, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 881
    .local v1, "bodyLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v4, 0x1

    iput-boolean v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 882
    const/16 v4, 0x9

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 883
    const/16 v4, 0xa

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 884
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 897
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v5, "vienna_popup_bg"

    invoke-virtual {v4, v0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 900
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 901
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 903
    return-object v2
.end method

.method private bodyLayout()Landroid/view/ViewGroup;
    .locals 4

    .prologue
    .line 842
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 843
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 844
    const/4 v3, -0x2

    .line 843
    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 845
    .local v1, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 846
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->bodyBg()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 848
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->eraserSettingClearAllButton()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mClearAllButton:Landroid/view/View;

    .line 849
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->eraserTypeLayout()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    .line 850
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->eraserSizeLayout()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    .line 851
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 852
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->eraseByStroke()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterTypeChekBox:Landroid/view/View;

    .line 853
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setEnableSizeSeekbar(Z)V

    .line 854
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 855
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 856
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterTypeChekBox:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 857
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 859
    return-object v0
.end method

.method private checkPosition()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1611
    const/4 v4, 0x2

    new-array v0, v4, [I

    .line 1612
    .local v0, "location":[I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43640000    # 228.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1613
    .local v2, "minWidth":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43010000    # 129.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1615
    .local v1, "minHeight":I
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getLocationOnScreen([I)V

    .line 1617
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1619
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    aget v4, v0, v6

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-ge v4, v5, :cond_0

    .line 1620
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1622
    :cond_0
    aget v4, v0, v7

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    if-ge v4, v5, :cond_1

    .line 1623
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1626
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    aget v5, v0, v6

    sub-int/2addr v4, v5

    if-ge v4, v2, :cond_2

    .line 1627
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v4, v2

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1629
    iget v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-gez v4, :cond_2

    .line 1630
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1633
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    aget v5, v0, v7

    sub-int/2addr v4, v5

    if-ge v4, v1, :cond_3

    .line 1634
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, v1

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1636
    iget v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-gez v4, :cond_3

    .line 1637
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1641
    :cond_3
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1642
    return-void
.end method

.method private eraseByStroke()Landroid/view/View;
    .locals 11

    .prologue
    const/16 v5, 0x19

    const/high16 v4, 0x41300000    # 11.0f

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 350
    new-instance v7, Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 351
    .local v7, "typeCheckBox":Landroid/widget/CheckBox;
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 352
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42480000    # 50.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 351
    invoke-direct {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 353
    .local v8, "typeCheckBoxParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 354
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42980000    # 76.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 355
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 356
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 353
    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 358
    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 359
    invoke-virtual {v7, v10}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 360
    invoke-virtual {v7, v10}, Landroid/widget/CheckBox;->setSingleLine(Z)V

    .line 361
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 362
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v1, "string_erase_line_by_line"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 363
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_btn_check_off"

    const-string v2, "snote_btn_check_on"

    .line 364
    const-string v3, "snote_btn_check_on_focused"

    const-string v4, "snote_btn_check_off_focused"

    move v6, v5

    .line 363
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 365
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSdkVersion:I

    const/16 v1, 0x10

    if-le v0, v1, :cond_0

    .line 366
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {v7, v0, v9, v9, v9}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 372
    :goto_0
    const/high16 v0, -0x1000000

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 373
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v7, v9, v0}, Landroid/widget/CheckBox;->setTextSize(IF)V

    .line 374
    invoke-virtual {v7, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 376
    return-object v7

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x42140000    # 37.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {v7, v0, v9, v9, v9}, Landroid/widget/CheckBox;->setPadding(IIII)V

    goto :goto_0
.end method

.method private eraserSettingClearAllButton()Landroid/view/View;
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/high16 v12, -0x1000000

    const/high16 v11, 0x41300000    # 11.0f

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1407
    new-instance v2, Landroid/widget/Button;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v2, v5}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1408
    .local v2, "clearAllButton":Landroid/widget/Button;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1409
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v6, 0x43998000    # 307.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x423c0000    # 47.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1408
    invoke-direct {v3, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1410
    .local v3, "clearAllButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1411
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x43080000    # 136.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1416
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41800000    # 16.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1417
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1410
    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1419
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1420
    invoke-virtual {v2, v10}, Landroid/widget/Button;->setFocusable(Z)V

    .line 1421
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v6, "string_clear_all"

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1422
    new-array v0, v13, [[I

    .line 1423
    .local v0, "arrayOfStates":[[I
    new-array v1, v10, [I

    .line 1424
    .local v1, "arrayOfStates1":[I
    const v5, 0x10100a7

    aput v5, v1, v9

    .line 1425
    aput-object v1, v0, v9

    .line 1426
    new-array v5, v9, [I

    aput-object v5, v0, v10

    .line 1427
    new-array v4, v13, [I

    .line 1428
    .local v4, "textColor":[I
    aput v12, v4, v9

    .line 1429
    aput v12, v4, v10

    .line 1430
    new-instance v5, Landroid/content/res/ColorStateList;

    invoke-direct {v5, v0, v4}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1431
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41880000    # 17.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v2, v9, v5}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1433
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v6, "snote_popup_btn_normal"

    const-string v7, "snote_popup_btn_press"

    const-string v8, "snote_popup_btn_focus"

    invoke-virtual {v5, v2, v6, v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1435
    return-object v2
.end method

.method private eraserSettingSizeSeekBar()Landroid/widget/SeekBar;
    .locals 14

    .prologue
    .line 1294
    new-instance v11, Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v11, v3}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 1297
    .local v11, "seekBar":Landroid/widget/SeekBar;
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1298
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43650000    # 229.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1300
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42040000    # 33.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1297
    invoke-direct {v12, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1301
    .local v12, "seekBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1302
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40400000    # 3.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1303
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x40a00000    # 5.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1301
    invoke-virtual {v12, v3, v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1305
    const/4 v3, 0x1

    iput-boolean v3, v12, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1306
    const/16 v3, 0xe

    invoke-virtual {v12, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1307
    const/16 v3, 0x8

    invoke-virtual {v12, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1308
    invoke-virtual {v11, v12}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1309
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    const/4 v4, 0x0

    .line 1310
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    const/4 v6, 0x0

    .line 1309
    invoke-virtual {v11, v3, v4, v5, v6}, Landroid/widget/SeekBar;->setPadding(IIII)V

    .line 1314
    const/16 v3, 0x9

    invoke-virtual {v11, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1316
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "progress_handle_normal"

    const-string v2, "progress_handle_press"

    const-string v3, "progress_handle_focus"

    const/16 v4, 0x21

    const/16 v5, 0x21

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v3

    invoke-virtual {v11, v3}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 1317
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40400000    # 3.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v11, v3}, Landroid/widget/SeekBar;->setThumbOffset(I)V

    .line 1319
    new-instance v9, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v9}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 1320
    .local v9, "localGradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    const/4 v3, 0x0

    invoke-virtual {v9, v3}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1321
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40e00000    # 7.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v9, v3}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 1323
    new-instance v8, Landroid/graphics/drawable/ClipDrawable;

    const/4 v3, 0x3

    const/4 v4, 0x1

    invoke-direct {v8, v9, v3, v4}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 1324
    .local v8, "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v4, "progress_bg"

    const/16 v5, 0xe5

    const/16 v6, 0xc

    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1325
    .local v1, "bgDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v4, "progress_shadow"

    const/16 v5, 0xe5

    const/16 v6, 0xc

    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 1326
    .local v13, "shadowDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 1327
    .local v0, "bgInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v2, Landroid/graphics/drawable/InsetDrawable;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, v13

    invoke-direct/range {v2 .. v7}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 1329
    .local v2, "shadowInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v10, Landroid/graphics/drawable/LayerDrawable;

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    const/4 v4, 0x2

    .line 1330
    aput-object v8, v3, v4

    .line 1329
    invoke-direct {v10, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 1331
    .local v10, "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    invoke-virtual {v11, v10}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1333
    return-object v11
.end method

.method private eraserSizeButtonSeekbar()Landroid/view/ViewGroup;
    .locals 15
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/high16 v14, 0x41000000    # 8.0f

    const/high16 v13, 0x40a00000    # 5.0f

    const/high16 v12, 0x40000000    # 2.0f

    const/high16 v11, 0x42100000    # 36.0f

    const/16 v5, 0x24

    .line 1343
    new-instance v9, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v9, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1344
    .local v9, "eraserSettingSizeDisplayLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    .line 1345
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1346
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 1347
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1345
    invoke-direct {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1348
    .local v8, "eraserPlusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v8, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1349
    const/16 v0, 0xb

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1350
    const/16 v0, 0x8

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1352
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    const/4 v1, 0x0

    .line 1353
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1352
    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1355
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1356
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1358
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 1359
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_progress_btn_plus_normal"

    const-string v2, "snote_popup_progress_btn_plus_press"

    .line 1360
    const-string v3, "snote_popup_progress_btn_plus_focus"

    const-string v4, "snote_popup_progress_btn_plus_dim"

    move v6, v5

    .line 1359
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1366
    :goto_0
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    .line 1367
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1368
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 1369
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1367
    invoke-direct {v7, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1370
    .local v7, "eraserMinusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1371
    const/16 v0, 0x9

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1372
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1374
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    const/4 v1, 0x0

    .line 1375
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1374
    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1377
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1378
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1380
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 1381
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_progress_btn_minus_normal"

    const-string v2, "snote_popup_progress_btn_minus_press"

    .line 1382
    const-string v3, "snote_popup_progress_btn_minus_focus"

    const-string v4, "snote_popup_progress_btn_minus_dim"

    move v6, v5

    .line 1381
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1390
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->eraserSettingSizeSeekBar()Landroid/widget/SeekBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    .line 1392
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1394
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1395
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1397
    return-object v9

    .line 1362
    .end local v7    # "eraserMinusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_progress_btn_plus_normal"

    const-string v2, "snote_popup_progress_btn_plus_press"

    .line 1363
    const-string v3, "snote_popup_progress_btn_plus_focus"

    const-string v4, "snote_popup_progress_btn_plus_dim"

    move v6, v5

    .line 1362
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1384
    .restart local v7    # "eraserMinusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_progress_btn_minus_normal"

    const-string v2, "snote_popup_progress_btn_minus_press"

    .line 1385
    const-string v3, "snote_popup_progress_btn_minus_focus"

    const-string v4, "snote_popup_progress_btn_minus_dim"

    move v6, v5

    .line 1384
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method private eraserSizeLayout()Landroid/view/ViewGroup;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1260
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1261
    .local v0, "eraserSettingSizeLayout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1262
    const/4 v3, -0x1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42780000    # 62.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1261
    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1265
    .local v1, "eraserSettingSizeLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41300000    # 11.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v1, v7, v3, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1267
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1269
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    .line 1270
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1271
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    const/16 v4, 0x56

    const/16 v5, 0x57

    const/16 v6, 0x5b

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1272
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41400000    # 12.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v7, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1276
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1277
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42480000    # 50.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41500000    # 13.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1276
    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1278
    .local v2, "eraserSizeTextParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1279
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1280
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1281
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1282
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->eraserSizeButtonSeekbar()Landroid/view/ViewGroup;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

    .line 1283
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1284
    return-object v0
.end method

.method private eraserTypeLayout()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 1202
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1203
    .local v1, "localLinearLayout":Landroid/widget/LinearLayout;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1205
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1206
    const/4 v2, -0x1

    const/4 v3, -0x2

    .line 1205
    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1207
    .local v0, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1209
    new-instance v2, Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserType01:Landroid/widget/ImageButton;

    .line 1210
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserType01:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v4, "snote_eraser_popup_draw"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1211
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserType01:Landroid/widget/ImageButton;

    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$14;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1227
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserType01:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1229
    new-instance v2, Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserType02:Landroid/widget/ImageButton;

    .line 1230
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserType02:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v4, "snote_eraser_popup_text"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1231
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserType02:Landroid/widget/ImageButton;

    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$15;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1248
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserType02:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1250
    return-object v1
.end method

.method private eraserTypeSetting(Landroid/view/View;)V
    .locals 5
    .param p1, "selectedView"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1497
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeView:[Landroid/view/View;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 1531
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 1532
    return-void

    .line 1498
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    .line 1499
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    .line 1501
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1503
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    .line 1505
    packed-switch v0, :pswitch_data_0

    .line 1497
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1507
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_1

    .line 1508
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-result-object v1

    .line 1509
    .local v1, "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    if-eqz v1, :cond_1

    .line 1510
    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    .line 1511
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto :goto_1

    .line 1517
    .end local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_1

    .line 1518
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-result-object v1

    .line 1519
    .restart local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    if-eqz v1, :cond_1

    .line 1520
    iput v4, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    .line 1521
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto :goto_1

    .line 1505
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private findMinValue(Landroid/widget/TextView;I)V
    .locals 4
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "maxWidth"    # I

    .prologue
    const/4 v3, 0x0

    .line 984
    const/high16 v0, 0x41800000    # 16.0f

    .line 986
    .local v0, "currentFloat":F
    :goto_0
    invoke-virtual {p1, v3, v3}, Landroid/widget/TextView;->measure(II)V

    .line 987
    invoke-virtual {p1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    .line 989
    .local v1, "width":I
    if-le v1, p2, :cond_0

    .line 990
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v0, v2

    .line 991
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 993
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 997
    return-void
.end method

.method private getMovableRect()Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 907
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 908
    .local v0, "location":[I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 910
    .local v1, "r":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    .line 912
    aget v2, v0, v4

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mLeftMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 913
    aget v2, v0, v5

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTopMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 914
    aget v2, v0, v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 915
    aget v2, v0, v5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 917
    return-object v1
.end method

.method private playScrollAnimationForBottomBar(III)V
    .locals 8
    .param p1, "delay"    # I
    .param p2, "from"    # I
    .param p3, "to"    # I

    .prologue
    .line 1446
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mScrollTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1447
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1449
    :cond_0
    move v6, p2

    .line 1450
    .local v6, "fromFinal":I
    move v7, p3

    .line 1451
    .local v7, "toFinal":I
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mScrollTimer:Ljava/util/Timer;

    .line 1452
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCount:I

    .line 1453
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mScrollTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$16;

    invoke-direct {v1, p0, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;II)V

    .line 1487
    const-wide/16 v2, 0xa

    int-to-long v4, p1

    .line 1453
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 1488
    return-void
.end method

.method private popupMaxButton()Landroid/view/View;
    .locals 8

    .prologue
    const/16 v5, 0x2a

    const/high16 v3, 0x42280000    # 42.0f

    .line 1160
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1161
    .local v1, "popupMaxButton":Landroid/widget/ImageButton;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1162
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1161
    invoke-direct {v7, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1163
    .local v7, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v0, 0xb

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1165
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1166
    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1167
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1168
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1170
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v2, "snote_popup_arrow_max_normal"

    const-string v3, "snote_popup_arrow_max_normal"

    const-string v4, "snote_popup_arrow_max_normal"

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1172
    return-object v1
.end method

.method private popupMinButton()Landroid/view/View;
    .locals 8

    .prologue
    const/16 v5, 0x20

    const/high16 v3, 0x42280000    # 42.0f

    .line 1179
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1180
    .local v1, "popupMinButton":Landroid/widget/ImageButton;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1181
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1180
    invoke-direct {v7, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1182
    .local v7, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v0, 0xb

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1184
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1185
    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1186
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1187
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1190
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v2, "snote_popup_arrow_min_normal"

    const-string v3, "snote_popup_arrow_min_normal"

    const-string v4, "snote_popup_arrow_min_normal"

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1192
    return-object v1
.end method

.method private rotatePosition()V
    .locals 13

    .prologue
    .line 1535
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1537
    .local v4, "newMovableRect":Landroid/graphics/Rect;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTotalTopMargin:I

    if-eq v10, v11, :cond_2

    .line 1538
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTotalTopMargin:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTotalLeftMargin:I

    add-int/2addr v10, v11

    iput v10, v4, Landroid/graphics/Rect;->left:I

    .line 1542
    :goto_0
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTotalLeftMargin:I

    if-eq v10, v11, :cond_3

    .line 1543
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTotalLeftMargin:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTotalTopMargin:I

    add-int/2addr v10, v11

    iput v10, v4, Landroid/graphics/Rect;->top:I

    .line 1548
    :goto_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iput v10, v4, Landroid/graphics/Rect;->right:I

    .line 1549
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    iput v10, v4, Landroid/graphics/Rect;->bottom:I

    .line 1551
    const-string v10, "settingui-settingEraser"

    const-string v11, "==== SettingEraser ===="

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1552
    const-string v10, "settingui-settingEraser"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "old  = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->left:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->top:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->right:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 1553
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1552
    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1554
    const-string v10, "settingui-settingEraser"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "new  = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 1555
    iget v12, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1554
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1557
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 1558
    .local v6, "r":Landroid/graphics/Rect;
    const/4 v10, 0x2

    new-array v3, v10, [I

    .line 1559
    .local v3, "location":[I
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getLocationOnScreen([I)V

    .line 1561
    const/4 v10, 0x0

    aget v10, v3, v10

    iput v10, v6, Landroid/graphics/Rect;->left:I

    .line 1562
    const/4 v10, 0x1

    aget v10, v3, v10

    iput v10, v6, Landroid/graphics/Rect;->top:I

    .line 1563
    iget v10, v6, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getWidth()I

    move-result v11

    add-int/2addr v10, v11

    iput v10, v6, Landroid/graphics/Rect;->right:I

    .line 1564
    iget v10, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getHeight()I

    move-result v11

    add-int/2addr v10, v11

    iput v10, v6, Landroid/graphics/Rect;->bottom:I

    .line 1566
    const-string v10, "settingui-settingEraser"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "view = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, v6, Landroid/graphics/Rect;->left:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v6, Landroid/graphics/Rect;->right:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1568
    iget v10, v6, Landroid/graphics/Rect;->left:I

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    sub-int/2addr v10, v11

    int-to-float v2, v10

    .line 1569
    .local v2, "left":F
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    iget v11, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v10, v11

    int-to-float v7, v10

    .line 1570
    .local v7, "right":F
    iget v10, v6, Landroid/graphics/Rect;->top:I

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    sub-int/2addr v10, v11

    int-to-float v8, v10

    .line 1571
    .local v8, "top":F
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iget v11, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v10, v11

    int-to-float v0, v10

    .line 1573
    .local v0, "bottom":F
    add-float v10, v2, v7

    div-float v1, v2, v10

    .line 1574
    .local v1, "hRatio":F
    add-float v10, v8, v0

    div-float v9, v8, v10

    .line 1576
    .local v9, "vRatio":F
    const-string v10, "settingui-settingEraser"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "left :"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", right :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1577
    const-string v10, "settingui-settingEraser"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "top :"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", bottom :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1578
    const-string v10, "settingui-settingEraser"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "hRatio = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", vRatio = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1580
    const v10, 0x3f7d70a4    # 0.99f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_4

    .line 1581
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1586
    :cond_0
    :goto_2
    const v10, 0x3f7d70a4    # 0.99f

    cmpl-float v10, v9, v10

    if-lez v10, :cond_5

    .line 1587
    const/high16 v9, 0x3f800000    # 1.0f

    .line 1592
    :cond_1
    :goto_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1594
    .local v5, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v11

    if-ge v10, v11, :cond_6

    .line 1595
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v1

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    iput v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1600
    :goto_4
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v11

    if-ge v10, v11, :cond_7

    .line 1601
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v9

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    iput v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1606
    :goto_5
    const-string v10, "settingui-settingEraser"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "lMargin = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", tMargin = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1607
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1608
    return-void

    .line 1540
    .end local v0    # "bottom":F
    .end local v1    # "hRatio":F
    .end local v2    # "left":F
    .end local v3    # "location":[I
    .end local v5    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v6    # "r":Landroid/graphics/Rect;
    .end local v7    # "right":F
    .end local v8    # "top":F
    .end local v9    # "vRatio":F
    :cond_2
    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTotalLeftMargin:I

    iput v10, v4, Landroid/graphics/Rect;->left:I

    goto/16 :goto_0

    .line 1545
    :cond_3
    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTotalTopMargin:I

    iput v10, v4, Landroid/graphics/Rect;->top:I

    goto/16 :goto_1

    .line 1582
    .restart local v0    # "bottom":F
    .restart local v1    # "hRatio":F
    .restart local v2    # "left":F
    .restart local v3    # "location":[I
    .restart local v6    # "r":Landroid/graphics/Rect;
    .restart local v7    # "right":F
    .restart local v8    # "top":F
    .restart local v9    # "vRatio":F
    :cond_4
    const/4 v10, 0x0

    cmpg-float v10, v1, v10

    if-gez v10, :cond_0

    .line 1583
    const/4 v1, 0x0

    goto :goto_2

    .line 1588
    :cond_5
    const/4 v10, 0x0

    cmpg-float v10, v9, v10

    if-gez v10, :cond_1

    .line 1589
    const/4 v9, 0x0

    goto :goto_3

    .line 1597
    .restart local v5    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_6
    const/4 v10, 0x0

    iput v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_4

    .line 1603
    :cond_7
    const/4 v10, 0x0

    iput v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_5
.end method

.method private setEnableSizeSeekbar(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 281
    if-eqz p1, :cond_4

    .line 283
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 284
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 286
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 288
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 289
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterTypeChekBox:Landroid/view/View;

    check-cast v0, Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 290
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-eq v0, v3, :cond_1

    .line 291
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 293
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 294
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 295
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mAutoIncrement:Z

    if-eqz v0, :cond_0

    .line 296
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mAutoIncrement:Z

    .line 302
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    if-nez v0, :cond_3

    .line 304
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 305
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 306
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mAutoDecrement:Z

    if-eqz v0, :cond_1

    .line 307
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mAutoDecrement:Z

    .line 321
    :cond_1
    :goto_1
    return-void

    .line 300
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 311
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    .line 315
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 316
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 317
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 318
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterTypeChekBox:Landroid/view/View;

    check-cast v0, Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1
.end method

.method private setListener()V
    .locals 3

    .prologue
    .line 1022
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1023
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleLayout:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1031
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMaxButton:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1032
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMaxButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1034
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMinButton:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 1036
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMinButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1039
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 1041
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1042
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1043
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1046
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    if-eqz v1, :cond_4

    .line 1048
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1049
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1050
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1053
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_5

    .line 1054
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1057
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mClearAllButton:Landroid/view/View;

    if-eqz v1, :cond_6

    .line 1058
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mClearAllButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mClearAllListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1061
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeView:[Landroid/view/View;

    if-eqz v1, :cond_7

    .line 1062
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x1

    if-le v0, v1, :cond_9

    .line 1070
    .end local v0    # "i":I
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_8

    .line 1071
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1075
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterTypeChekBox:Landroid/view/View;

    check-cast v1, Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1076
    return-void

    .line 1063
    .restart local v0    # "i":I
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v1, v1, v0

    if-eqz v1, :cond_a

    .line 1064
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1062
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private titleBg()Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x1

    const/4 v7, -0x1

    .line 758
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 759
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 761
    new-instance v1, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 762
    .local v1, "titleLeft":Landroid/widget/ImageView;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 764
    .local v2, "titleLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v8, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 765
    const/16 v5, 0x9

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 766
    invoke-virtual {v2, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 769
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 776
    new-instance v3, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 777
    .local v3, "titleRight":Landroid/widget/ImageView;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 778
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v6, 0x43248000    # 164.5f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 777
    invoke-direct {v4, v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 779
    .local v4, "titleRightParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 780
    const/16 v5, 0xb

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 781
    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 782
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 784
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v6, "vienna_popup_title_bg"

    invoke-virtual {v5, v1, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 789
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 793
    return-object v0
.end method

.method private titleLayout()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 734
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 735
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 736
    const/high16 v4, 0x42280000    # 42.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 735
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 738
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->popupMaxButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMaxButton:Landroid/view/View;

    .line 739
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->popupMinButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMinButton:Landroid/view/View;

    .line 740
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->titleBg()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 741
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->titleText()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 744
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 745
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 746
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMaxButton:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 748
    return-object v0
.end method

.method private titleText()Landroid/view/View;
    .locals 9

    .prologue
    const/16 v8, 0x13

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 798
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    .line 799
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 800
    const/high16 v4, 0x42280000    # 42.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v2, v6, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 799
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 801
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 802
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 803
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 805
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_eraser_settings"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 806
    .local v0, "eraserText":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 807
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 808
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 811
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x1c

    if-le v1, v2, :cond_0

    .line 812
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 831
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_eraser_settings"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 832
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41500000    # 13.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v5, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 833
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    return-object v1

    .line 813
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v8, :cond_1

    .line 814
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41600000    # 14.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 816
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method

.method private totalLayout()V
    .locals 3

    .prologue
    .line 717
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v2, 0x43a48000    # 329.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 718
    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 717
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 719
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setOrientation(I)V

    .line 720
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->titleLayout()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleLayout:Landroid/view/View;

    .line 721
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->bodyLayout()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mBodyLayout:Landroid/view/View;

    .line 722
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->addView(Landroid/view/View;)V

    .line 723
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->addView(Landroid/view/View;)V

    .line 725
    return-void
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 925
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 926
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 927
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mRemoverContext:Landroid/content/Context;

    .line 929
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    if-nez v1, :cond_0

    .line 981
    :goto_0
    return-void

    .line 933
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 934
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    .line 935
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 936
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

    .line 941
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 942
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    .line 943
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 944
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserMinusButton:Landroid/view/View;

    .line 945
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 946
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mClearAllButton:Landroid/view/View;

    .line 947
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 948
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    .line 949
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserType01:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 950
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserType01:Landroid/widget/ImageButton;

    .line 951
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserType02:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 952
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserType02:Landroid/widget/ImageButton;

    .line 953
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 954
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_3

    .line 959
    .end local v0    # "i":I
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeView:[Landroid/view/View;

    .line 960
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 961
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    .line 962
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 963
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleLayout:Landroid/view/View;

    .line 964
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 965
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mBodyLayout:Landroid/view/View;

    .line 967
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 968
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-eqz v1, :cond_2

    .line 969
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    .line 970
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v2, 0x1

    aput-object v3, v1, v2

    .line 971
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 975
    :cond_2
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;

    .line 976
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 977
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 979
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->close()V

    .line 980
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    goto/16 :goto_0

    .line 955
    .restart local v0    # "i":I
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 956
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeView:[Landroid/view/View;

    aput-object v3, v1, v0

    .line 954
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    .prologue
    .line 1725
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    return-object v0
.end method

.method public getViewMode()I
    .locals 1

    .prologue
    .line 1709
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mViewMode:I

    return v0
.end method

.method protected initView()V
    .locals 5

    .prologue
    .line 1001
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->totalLayout()V

    .line 1002
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v4, 0x438b8000    # 279.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->findMinValue(Landroid/widget/TextView;I)V

    .line 1003
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1004
    .local v0, "eraserTypeViewGroup":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    new-array v2, v2, [Landroid/view/View;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeView:[Landroid/view/View;

    .line 1005
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 1009
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1010
    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 1012
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-nez v2, :cond_0

    .line 1013
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 1014
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v3, 0x0

    new-instance v4, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    aput-object v4, v2, v3

    .line 1015
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v3, 0x1

    new-instance v4, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    aput-object v4, v2, v3

    .line 1018
    :cond_0
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setVisibility(I)V

    .line 1019
    return-void

    .line 1006
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeView:[Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v1

    .line 1005
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 59
    const-string v0, "settingui-settingEraser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onConfig eraser "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mFirstLongPress:Z

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 63
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 64
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->rotatePosition()V

    .line 67
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 68
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 5
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 1647
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43640000    # 228.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1648
    .local v2, "minWidth":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43010000    # 129.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1650
    .local v1, "minHeight":I
    if-lt p1, v2, :cond_0

    if-ge p2, v1, :cond_1

    .line 1651
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1652
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->checkPosition()V

    .line 1655
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 1656
    .local v0, "handler":Landroid/os/Handler;
    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$17;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$17;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V

    invoke-virtual {v0, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1664
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 1666
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 9
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 1671
    if-ne p1, p0, :cond_1

    if-nez p2, :cond_1

    .line 1673
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 1674
    .local v0, "location":[I
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getLocationOnScreen([I)V

    .line 1676
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1678
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mMovableRect:Landroid/graphics/Rect;

    new-instance v3, Landroid/graphics/Rect;

    aget v4, v0, v6

    aget v5, v0, v8

    aget v6, v0, v6

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getWidth()I

    move-result v7

    add-int/2addr v6, v7

    aget v7, v0, v8

    .line 1679
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1678
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v2

    .line 1679
    if-nez v2, :cond_0

    .line 1680
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->checkPosition()V

    .line 1683
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42240000    # 41.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1685
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43480000    # 200.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    .line 1686
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    .line 1685
    mul-float/2addr v3, v4

    .line 1686
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1687
    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    .line 1685
    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 1684
    add-int v1, v2, v3

    .line 1688
    .local v1, "seek_label_pos":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    int-to-float v3, v1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setX(F)V

    .line 1689
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setY(F)V

    .line 1691
    .end local v0    # "location":[I
    .end local v1    # "seek_label_pos":I
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 1692
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->requestLayoutDisable:Z

    if-nez v0, :cond_0

    .line 51
    invoke-super {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 54
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->requestLayoutDisable:Z

    .line 55
    return-void
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$ActionListener;

    .prologue
    .line 1963
    return-void
.end method

.method public setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 3
    .param p1, "canvasView"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .prologue
    const/16 v2, 0x438

    .line 1813
    if-eqz p1, :cond_0

    .line 1814
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 1816
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_2

    .line 1817
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1818
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    .line 1822
    :goto_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    if-nez v0, :cond_0

    .line 1823
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    .line 1829
    :cond_0
    :goto_1
    return-void

    .line 1820
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    goto :goto_0

    .line 1826
    :cond_2
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    goto :goto_1
.end method

.method public setEraserInfoList([Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 0
    .param p1, "list"    # [Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 1917
    if-eqz p1, :cond_0

    .line 1918
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 1920
    :cond_0
    return-void
.end method

.method public setEraserListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;

    .prologue
    .line 1935
    if-eqz p1, :cond_0

    .line 1936
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;

    .line 1938
    :cond_0
    return-void
.end method

.method public setIndicatorPosition(I)V
    .locals 0
    .param p1, "x"    # I

    .prologue
    .line 1100
    return-void
.end method

.method public setInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 4
    .param p1, "settingCutterInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    const/16 v2, 0x438

    const/4 v1, 0x1

    .line 1846
    if-nez p1, :cond_0

    .line 1874
    :goto_0
    return-void

    .line 1850
    :cond_0
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCurrentCutterType:I

    .line 1851
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-ne v0, v1, :cond_2

    .line 1852
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setEnableSizeSeekbar(Z)V

    .line 1856
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_4

    .line 1857
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 1858
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    .line 1862
    :goto_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    if-nez v0, :cond_1

    .line 1863
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    .line 1868
    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCurrentCutterType:I

    aget-object v0, v0, v1

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    int-to-float v2, v2

    const/high16 v3, 0x44870000    # 1080.0f

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 1870
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    float-to-int v1, v1

    mul-int/lit8 v1, v1, 0xa

    div-int/lit8 v1, v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1872
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 1873
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    goto :goto_0

    .line 1854
    :cond_2
    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setEnableSizeSeekbar(Z)V

    goto :goto_1

    .line 1860
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    goto :goto_2

    .line 1866
    :cond_4
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    goto :goto_3
.end method

.method public setLayoutHeight(I)V
    .locals 1
    .param p1, "height"    # I

    .prologue
    .line 1439
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1440
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1441
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1442
    return-void
.end method

.method public setPopup(Z)V
    .locals 4
    .param p1, "open"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 380
    if-eqz p1, :cond_0

    .line 381
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 382
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 385
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getHeight()I

    move-result v0

    .line 386
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x436c0000    # 236.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 385
    invoke-direct {p0, v3, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->playScrollAnimationForBottomBar(III)V

    .line 394
    :goto_0
    return-void

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 389
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 392
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42280000    # 42.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-direct {p0, v3, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->playScrollAnimationForBottomBar(III)V

    goto :goto_0
.end method

.method public setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

    .prologue
    .line 1941
    if-eqz p1, :cond_0

    .line 1942
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

    .line 1944
    :cond_0
    return-void
.end method

.method public setPosition(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1119
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1121
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1122
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1124
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1126
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mFirstLongPress:Z

    if-eqz v1, :cond_0

    .line 1127
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mLeftMargin:I

    sub-int/2addr v1, p1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mLeftMargin:I

    .line 1128
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTopMargin:I

    sub-int/2addr v1, p2

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTopMargin:I

    .line 1131
    :cond_0
    return-void
.end method

.method public setViewMode(I)V
    .locals 5
    .param p1, "viewMode"    # I

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1742
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mViewMode:I

    .line 1745
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->requestLayoutDisable:Z

    .line 1746
    .local v0, "tempRequestLayoutDisable":Z
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->requestLayoutDisable:Z

    .line 1749
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mViewMode:I

    if-nez v1, :cond_0

    .line 1750
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1751
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1752
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1753
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1778
    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->requestLayoutDisable:Z

    .line 1780
    return-void

    .line 1754
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mViewMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1755
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1756
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1757
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1758
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 1759
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mViewMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 1760
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1761
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1762
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1763
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 1764
    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mViewMode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 1765
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1766
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1770
    :cond_3
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mViewMode:I

    .line 1771
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1772
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1773
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1774
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 1790
    if-nez p1, :cond_0

    .line 1791
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-nez v0, :cond_0

    .line 1792
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->loadImage()V

    .line 1796
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1797
    return-void
.end method
