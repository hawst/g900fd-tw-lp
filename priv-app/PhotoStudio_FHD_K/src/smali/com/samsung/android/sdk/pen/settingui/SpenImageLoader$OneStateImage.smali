.class Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;
.super Ljava/lang/Object;
.source "SpenImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OneStateImage"
.end annotation


# instance fields
.field private mHeightList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mImagePathList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mViewList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mWidthList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mViewList:Ljava/util/ArrayList;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;

    .line 22
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mViewList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mViewList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 15
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;

    return-void
.end method
