.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;
.super Ljava/lang/Object;
.source "SpenSettingEraserLayout2.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    .line 330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 10
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/4 v9, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    const v7, 0x3e4ccccd    # 0.2f

    const/4 v6, 0x0

    .line 345
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    .line 346
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v2, v2, v3

    int-to-float v3, p2

    iput v3, v2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 349
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iput-boolean p3, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    .line 351
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    add-int/lit8 v3, p2, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42240000    # 41.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 355
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43480000    # 200.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, p2

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 356
    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    .line 355
    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 354
    add-int v1, v2, v3

    .line 357
    .local v1, "seek_label_pos":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    int-to-float v3, v1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setX(F)V

    .line 358
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setY(F)V

    .line 359
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_1

    .line 360
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-result-object v0

    .line 361
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    if-eqz v0, :cond_1

    .line 362
    add-int/lit8 v2, p2, 0x1

    int-to-float v2, v2

    iput v2, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 363
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    .line 368
    .end local v0    # "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "\u0000"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 371
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getMax()I

    move-result v3

    if-ne v2, v3, :cond_4

    .line 372
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setAlpha(F)V

    .line 373
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setSelected(Z)V

    .line 374
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 375
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoIncrement:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 376
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    invoke-static {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;Z)V

    .line 382
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    if-nez v2, :cond_5

    .line 383
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setAlpha(F)V

    .line 384
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setSelected(Z)V

    .line 385
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 386
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoDecrement:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 387
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    invoke-static {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;Z)V

    .line 395
    :cond_3
    :goto_1
    return-void

    .line 379
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->setAlpha(F)V

    .line 380
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v2, v9}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 390
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->setAlpha(F)V

    .line 391
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v2, v9}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 340
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 335
    return-void
.end method
