.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;
.super Ljava/lang/Object;
.source "SpenSettingTextLayout2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    .line 530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 533
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentLeftBtn:Landroid/widget/ImageButton;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 534
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v1, :cond_0

    .line 535
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    .line 536
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 537
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 546
    .end local v0    # "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_0
    :goto_0
    return-void

    .line 539
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mIndentRightBtn:Landroid/widget/ImageButton;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 540
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v1, :cond_0

    .line 541
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    .line 542
    .restart local v0    # "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    add-int/lit8 v1, v1, -0xa

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 543
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    goto :goto_0
.end method
