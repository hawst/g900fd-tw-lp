.class Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;
.super Ljava/lang/Object;
.source "SpenObjectRuntime.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;

    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCanceled(ILjava/lang/Object;)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "objectBase"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 272
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;)Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;->onCanceled(ILjava/lang/Object;)V

    .line 273
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$1(Z)V

    .line 274
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;Z)V

    .line 275
    return-void
.end method

.method public onCompleted(Ljava/lang/Object;)V
    .locals 2
    .param p1, "objectBase"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 279
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;)Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;->onCompleted(Ljava/lang/Object;)V

    .line 280
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$1(Z)V

    .line 281
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;Z)V

    .line 282
    return-void
.end method

.method public onObjectUpdated(Landroid/graphics/RectF;Ljava/lang/Object;)V
    .locals 1
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "objectBase"    # Ljava/lang/Object;

    .prologue
    .line 267
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;)Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;->onObjectUpdated(Landroid/graphics/RectF;Ljava/lang/Object;)V

    .line 268
    return-void
.end method
