.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

.field private final synthetic val$fromFinal:I

.field private final synthetic val$toFinal:I


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;II)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->val$fromFinal:I

    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->val$toFinal:I

    .line 6766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6769
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCount:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$43(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I

    move-result v2

    add-int/lit8 v2, v2, 0x5

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$44(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V

    .line 6771
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->val$fromFinal:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->val$toFinal:I

    if-le v1, v2, :cond_1

    .line 6772
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->val$fromFinal:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCount:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$43(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I

    move-result v2

    sub-int v0, v1, v2

    .line 6773
    .local v0, "pos":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setLayoutHeight(I)V

    .line 6775
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->val$toFinal:I

    if-gt v0, v1, :cond_0

    .line 6776
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->val$toFinal:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setLayoutHeight(I)V

    .line 6778
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$45(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6779
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$45(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 6780
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$46(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Ljava/util/Timer;)V

    .line 6796
    :cond_0
    :goto_0
    return-void

    .line 6784
    .end local v0    # "pos":I
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->val$fromFinal:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCount:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$43(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I

    move-result v2

    add-int v0, v1, v2

    .line 6785
    .restart local v0    # "pos":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setLayoutHeight(I)V

    .line 6787
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->val$toFinal:I

    if-lt v0, v1, :cond_0

    .line 6788
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->val$toFinal:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setLayoutHeight(I)V

    .line 6790
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$45(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6791
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$45(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 6792
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$46(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Ljava/util/Timer;)V

    goto :goto_0
.end method
