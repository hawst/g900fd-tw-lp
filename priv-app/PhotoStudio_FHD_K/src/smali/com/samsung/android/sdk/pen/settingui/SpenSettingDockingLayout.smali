.class public Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;
.super Landroid/widget/LinearLayout;
.source "SpenSettingDockingLayout.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;
    }
.end annotation


# static fields
.field protected static mSdkVersion:I


# instance fields
.field protected mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mEnable:Z

.field private mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

.field private mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

.field private mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

.field private mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

.field private mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

.field onEraserClearListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;

.field private onPenPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;

.field private onRemoverPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

.field private onSelectPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;

.field private onTextPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSdkVersion:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/lang/String;Landroid/widget/RelativeLayout;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p4, "imagePath"    # Ljava/lang/String;
    .param p5, "canvasLayout"    # Landroid/widget/RelativeLayout;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/widget/RelativeLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .line 135
    .local p2, "resourceIds":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p3, "fontName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 527
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onPenPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;

    .line 546
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onTextPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

    .line 566
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onRemoverPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

    .line 586
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onSelectPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;

    .line 606
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onEraserClearListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;

    .line 137
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    .line 138
    .local v8, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v7, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 139
    .local v7, "deviceWidth":I
    iget v6, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 140
    .local v6, "deviceHeight":I
    const/16 v0, 0x640

    if-lt v7, v0, :cond_0

    const/16 v0, 0x640

    if-ge v6, v0, :cond_1

    .line 141
    :cond_0
    const-string v0, "It does not support the current resolution."

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    .line 188
    :goto_0
    return-void

    .line 146
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    .line 147
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p4, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 152
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p4, p5, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    .line 153
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v1, p1

    move-object v2, p4

    move-object v3, p3

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Landroid/widget/RelativeLayout;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    .line 154
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p4, p5, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    .line 155
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p4, p5, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    .line 157
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_2

    .line 158
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "vienna_popup_bg"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 162
    :goto_1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setOrientation(I)V

    .line 165
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->addView(Landroid/view/View;)V

    .line 166
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->addView(Landroid/view/View;)V

    .line 167
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->addView(Landroid/view/View;)V

    .line 168
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->addView(Landroid/view/View;)V

    .line 170
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setVisibility(I)V

    .line 172
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setVisibility(I)V

    .line 173
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setVisibility(I)V

    .line 175
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    .line 176
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    .line 177
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    .line 178
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    .line 180
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onPenPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;)V

    .line 181
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onTextPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;)V

    .line 182
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onRemoverPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;)V

    .line 183
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onSelectPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;)V

    .line 185
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onEraserClearListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setEraserListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;)V

    .line 187
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    goto/16 :goto_0

    .line 160
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "vienna_popup_bg"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setTopBottomToZeroMarginItem()V

    return-void
.end method

.method private setTopBottomToZeroMarginItem()V
    .locals 4

    .prologue
    .line 67
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 68
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    .line 92
    const-wide/16 v2, 0x64

    .line 68
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 93
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 197
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    if-eqz v0, :cond_2

    .line 202
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->close()V

    .line 203
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    .line 205
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    if-eqz v0, :cond_3

    .line 206
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->close()V

    .line 207
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    .line 209
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    if-eqz v0, :cond_4

    .line 210
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->close()V

    .line 211
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    .line 213
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->close()V

    .line 215
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 54
    :goto_0
    return-void

    .line 52
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setTopBottomToZeroMarginItem()V

    .line 53
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 44
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    goto :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setTopBottomToZeroMarginItem()V

    .line 63
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public setEraserInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 1
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 498
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 503
    :goto_0
    return-void

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    goto :goto_0
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    .prologue
    .line 350
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_1

    .line 357
    :cond_0
    :goto_0
    return-void

    .line 354
    :cond_1
    if-eqz p1, :cond_0

    .line 355
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    goto :goto_0
.end method

.method public setPenInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 1
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    .line 374
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 379
    :goto_0
    return-void

    .line 378
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_0
.end method

.method public setPenInfoList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 454
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 459
    :goto_0
    return-void

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPenInfoList(Ljava/util/List;)V

    goto :goto_0
.end method

.method public setPopupMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 259
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_1

    .line 311
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 300
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    .line 301
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    .line 302
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    .line 303
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    .line 304
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto :goto_0

    .line 264
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    .line 265
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    .line 266
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    .line 267
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    .line 268
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto :goto_0

    .line 273
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    .line 274
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    .line 275
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    .line 276
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    .line 277
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto :goto_0

    .line 282
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    .line 283
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    .line 284
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    .line 285
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    .line 286
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto :goto_0

    .line 291
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    .line 292
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    .line 293
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    .line 294
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    .line 295
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto/16 :goto_0

    .line 262
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setPosition(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 332
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v1, :cond_0

    .line 340
    :goto_0
    return-void

    .line 336
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 337
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 338
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 339
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public setSelectionInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 1
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .prologue
    .line 520
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 525
    :goto_0
    return-void

    .line 524
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V

    goto :goto_0
.end method

.method public setSpenView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 1
    .param p1, "spenView"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 242
    :goto_0
    return-void

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 239
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 240
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 241
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    goto :goto_0
.end method

.method public setTextInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 1
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    .line 476
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 481
    :goto_0
    return-void

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    goto :goto_0
.end method
