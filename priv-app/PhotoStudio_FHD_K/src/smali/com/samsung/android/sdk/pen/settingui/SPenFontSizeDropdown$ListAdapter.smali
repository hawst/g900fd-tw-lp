.class Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SPenFontSizeDropdown.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    .line 53
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->items:Ljava/util/List;

    .line 54
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->items:Ljava/util/List;

    .line 55
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->context:Landroid/content/Context;

    .line 56
    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v5, 0x13

    const/4 v4, 0x0

    .line 60
    if-nez p2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 62
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x1090008

    invoke-virtual {v0, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 64
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v2, 0x1020014

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 66
    .local v1, "tv":Landroid/widget/TextView;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 67
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 68
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 70
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 71
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 72
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 73
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x42140000    # 37.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 75
    sget v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSdkVersion:I

    if-lt v2, v5, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 76
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const-string v3, ""

    const-string v4, "tw_spinner_list_pressed_holo_light"

    .line 77
    const-string v5, "tw_spinner_list_focused_holo_light"

    .line 76
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 79
    :cond_2
    return-object p2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v5, 0x13

    const/4 v4, 0x0

    .line 86
    if-nez p2, :cond_0

    .line 87
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 88
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x1090008

    invoke-virtual {v0, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 90
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v2, 0x1020014

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 92
    .local v1, "tv":Landroid/widget/TextView;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 93
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 94
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 96
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 97
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 98
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 99
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x42140000    # 37.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 107
    sget v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSdkVersion:I

    if-lt v2, v5, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 108
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const-string v3, ""

    const-string v4, "tw_spinner_list_pressed_holo_light"

    .line 109
    const-string v5, "tw_spinner_list_focused_holo_light"

    .line 108
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 111
    :cond_2
    return-object p2
.end method
