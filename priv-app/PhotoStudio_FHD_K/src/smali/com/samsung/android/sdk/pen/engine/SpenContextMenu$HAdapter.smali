.class Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;
.super Landroid/widget/BaseAdapter;
.source "SpenContextMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HAdapter"
.end annotation


# instance fields
.field private final mOnButtonClicked:Landroid/view/View$OnClickListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V
    .locals 1

    .prologue
    .line 993
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .line 994
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 997
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->mOnButtonClicked:Landroid/view/View$OnClickListener;

    .line 995
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    .locals 1

    .prologue
    .line 991
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 1009
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1014
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1019
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 48
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 1026
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v41

    if-nez v41, :cond_0

    .line 1027
    const-string v41, "SpenContextMenu"

    new-instance v42, Ljava/lang/StringBuilder;

    const-string v43, "mContext is NULL at position = "

    invoke-direct/range {v42 .. v43}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v42

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v41 .. v42}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1028
    const/16 v28, 0x0

    .line 1623
    :goto_0
    return-object v28

    .line 1031
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    .line 1032
    .local v30, "res":Landroid/content/res/Resources;
    invoke-virtual/range {v30 .. v30}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v16

    .line 1034
    .local v16, "dm":Landroid/util/DisplayMetrics;
    new-instance v28, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v41

    move-object/from16 v0, v28

    move-object/from16 v1, v41

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1035
    .local v28, "linearLayout":Landroid/widget/LinearLayout;
    new-instance v27, Landroid/widget/LinearLayout$LayoutParams;

    .line 1036
    const/16 v41, -0x1

    const/16 v42, -0x1

    .line 1035
    move-object/from16 v0, v27

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1037
    .local v27, "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v41

    move/from16 v2, v42

    move/from16 v3, v43

    move/from16 v4, v44

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1038
    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1039
    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v41

    move/from16 v2, v42

    move/from16 v3, v43

    move/from16 v4, v44

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1040
    const/16 v41, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1050
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$8()I

    move-result v41

    const/16 v42, 0x1

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_2

    .line 1051
    const/16 v14, 0x62

    .line 1052
    .local v14, "defaultWidth":I
    const/16 v11, 0x48

    .line 1053
    .local v11, "defaultHeight":I
    const/4 v13, 0x2

    .line 1054
    .local v13, "defaultSeperatorWidth":I
    const/16 v12, 0x5c

    .line 1055
    .local v12, "defaultHeight_2":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_1

    .line 1056
    const/16 v11, 0x50

    .line 1057
    const/16 v12, 0x61

    .line 1066
    :cond_1
    :goto_1
    add-int/lit8 v41, p1, 0x1

    rem-int/lit8 v41, v41, 0x2

    if-nez v41, :cond_8

    .line 1067
    new-instance v22, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v41

    move-object/from16 v0, v22

    move-object/from16 v1, v41

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1068
    .local v22, "imageView":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_3

    .line 1069
    const/16 v41, 0x1

    int-to-float v0, v11

    move/from16 v42, v0

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v17, v0

    .line 1073
    .local v17, "height":I
    :goto_2
    new-instance v37, Landroid/view/ViewGroup$LayoutParams;

    move-object/from16 v0, v37

    move/from16 v1, v17

    invoke-direct {v0, v13, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1074
    .local v37, "viewLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, v22

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1075
    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v41

    move/from16 v2, v42

    move/from16 v3, v43

    move/from16 v4, v44

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 1076
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$8()I

    move-result v41

    const/16 v42, 0x1

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_6

    .line 1077
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_4

    .line 1078
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v42, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static/range {v42 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v42

    .line 1079
    const-string v43, "quick_popup_dv_vienna"

    const-string v44, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v45

    .line 1078
    invoke-virtual/range {v42 .. v45}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v41

    move-object/from16 v0, v22

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1097
    :goto_3
    move-object/from16 v0, v28

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1060
    .end local v11    # "defaultHeight":I
    .end local v12    # "defaultHeight_2":I
    .end local v13    # "defaultSeperatorWidth":I
    .end local v14    # "defaultWidth":I
    .end local v17    # "height":I
    .end local v22    # "imageView":Landroid/widget/ImageView;
    .end local v37    # "viewLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_2
    const/16 v14, 0x48

    .line 1061
    .restart local v14    # "defaultWidth":I
    const/16 v11, 0x3d

    .line 1062
    .restart local v11    # "defaultHeight":I
    const/16 v12, 0x4e

    .line 1063
    .restart local v12    # "defaultHeight_2":I
    const/4 v13, 0x1

    .restart local v13    # "defaultSeperatorWidth":I
    goto/16 :goto_1

    .line 1071
    .restart local v22    # "imageView":Landroid/widget/ImageView;
    :cond_3
    const/16 v41, 0x1

    int-to-float v0, v12

    move/from16 v42, v0

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v17, v0

    .restart local v17    # "height":I
    goto/16 :goto_2

    .line 1081
    .restart local v37    # "viewLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_4
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$12()I

    move-result v41

    const/16 v42, 0x10

    move/from16 v0, v41

    move/from16 v1, v42

    if-ge v0, v1, :cond_5

    .line 1082
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v42, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static/range {v42 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v42

    .line 1083
    const-string v43, "tw_quick_bubble_divider_holo_light"

    const-string v44, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v45

    .line 1082
    invoke-virtual/range {v42 .. v45}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v41

    move-object/from16 v0, v22

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 1085
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v42, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static/range {v42 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v42

    .line 1086
    const-string v43, "tw_quick_bubble_divider_holo_light"

    const-string v44, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v45

    .line 1085
    invoke-virtual/range {v42 .. v45}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v41

    move-object/from16 v0, v22

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 1090
    :cond_6
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$12()I

    move-result v41

    const/16 v42, 0x13

    move/from16 v0, v41

    move/from16 v1, v42

    if-gt v0, v1, :cond_7

    .line 1091
    const v41, -0x777778

    move-object/from16 v0, v22

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto/16 :goto_3

    .line 1093
    :cond_7
    const/16 v41, 0x16

    const/16 v42, 0x25

    const/16 v43, 0x2e

    invoke-static/range {v41 .. v43}, Landroid/graphics/Color;->rgb(III)I

    move-result v41

    move-object/from16 v0, v22

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto/16 :goto_3

    .line 1100
    .end local v17    # "height":I
    .end local v22    # "imageView":Landroid/widget/ImageView;
    .end local v37    # "viewLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_8
    div-int/lit8 v23, p1, 0x2

    .line 1101
    .local v23, "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_15

    .line 1102
    const/16 v41, 0x1

    int-to-float v0, v14

    move/from16 v42, v0

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v39, v0

    .line 1103
    .local v39, "width":I
    const/16 v41, 0x1

    int-to-float v0, v11

    move/from16 v42, v0

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v17, v0

    .line 1108
    .restart local v17    # "height":I
    :goto_4
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    move/from16 v0, v39

    move/from16 v1, v17

    invoke-direct {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1109
    .local v8, "buttonLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v26, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v41

    move-object/from16 v0, v26

    move-object/from16 v1, v41

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1110
    .local v26, "layoutButton":Landroid/widget/RelativeLayout;
    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1111
    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v41

    move/from16 v2, v42

    move/from16 v3, v43

    move/from16 v4, v44

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 1112
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move-object/from16 v0, v26

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 1113
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move-object/from16 v0, v26

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 1114
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move-object/from16 v0, v26

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 1115
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move-object/from16 v0, v26

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 1117
    new-instance v20, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 1119
    .local v20, "imageDrawable":Landroid/graphics/drawable/StateListDrawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundPressed:Landroid/graphics/drawable/Drawable;

    .line 1120
    .local v5, "backgroundImage":Landroid/graphics/drawable/Drawable;
    if-nez v5, :cond_9

    .line 1121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_16

    .line 1122
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v42, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static/range {v42 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v42

    const-string v43, "quick_popup_bg_press_vienna"

    .line 1123
    const-string v44, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v45

    .line 1122
    invoke-virtual/range {v42 .. v45}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 1129
    :cond_9
    :goto_5
    const/16 v41, 0x1

    move/from16 v0, v41

    new-array v0, v0, [I

    move-object/from16 v41, v0

    const/16 v42, 0x0

    const v43, 0x10100a7

    aput v43, v41, v42

    move-object/from16 v0, v20

    move-object/from16 v1, v41

    invoke-virtual {v0, v1, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundNormal:Landroid/graphics/drawable/Drawable;

    .line 1133
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1134
    const/16 v41, -0x1

    const/16 v42, -0x1

    .line 1133
    move/from16 v0, v41

    move/from16 v1, v42

    invoke-direct {v7, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1136
    .local v7, "bgImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/util/ArrayList;->size()I

    move-result v41

    add-int/lit8 v41, v41, -0x1

    mul-int/lit8 v25, v41, 0x2

    .line 1140
    .local v25, "itemNum":I
    new-instance v35, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1141
    const/16 v41, -0x1

    const/16 v42, -0x1

    .line 1140
    move-object/from16 v0, v35

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1142
    .local v35, "transparentLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v36, Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v41

    move-object/from16 v0, v36

    move-object/from16 v1, v41

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1143
    .local v36, "transparentView":Landroid/view/View;
    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1145
    if-eqz p1, :cond_a

    move/from16 v0, p1

    move/from16 v1, v25

    if-ne v0, v1, :cond_27

    .line 1146
    :cond_a
    if-nez p1, :cond_e

    .line 1147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    new-instance v42, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v43, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static/range {v43 .. v43}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v43

    const/16 v44, 0x0

    invoke-direct/range {v42 .. v44}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;-><init>(Landroid/content/Context;I)V

    invoke-static/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;)V

    .line 1148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v41

    if-eqz v41, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v41

    if-nez v41, :cond_17

    .line 1151
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1156
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/util/ArrayList;->size()I

    move-result v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v42, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I
    invoke-static/range {v42 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)I

    move-result v42

    move/from16 v0, v41

    move/from16 v1, v42

    if-le v0, v1, :cond_c

    .line 1157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v42, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;
    invoke-static/range {v42 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getLeftEdgeEffect()Landroid/widget/EdgeEffect;

    move-result-object v42

    .line 1158
    const/16 v43, 0x0

    .line 1157
    invoke-virtual/range {v41 .. v43}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setEdgeEffect(Landroid/widget/EdgeEffect;I)V

    .line 1161
    :cond_c
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$8()I

    move-result v41

    const/16 v42, 0x1

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_1b

    .line 1163
    if-eqz v5, :cond_19

    .line 1164
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$12()I

    move-result v41

    const/16 v42, 0x10

    move/from16 v0, v41

    move/from16 v1, v42

    if-ge v0, v1, :cond_18

    .line 1165
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1173
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_1a

    .line 1174
    const/4 v15, 0x5

    .line 1192
    .local v15, "delta_padding":I
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move-object/from16 v0, v42

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setClickable(Z)V

    .line 1193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move-object/from16 v0, v42

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setFocusable(Z)V

    .line 1194
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move-object/from16 v0, v42

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setLongClickable(Z)V

    .line 1195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move-object/from16 v0, v42

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setEnabled(Z)V

    .line 1197
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$12()I

    move-result v41

    const/16 v42, 0x13

    move/from16 v0, v41

    move/from16 v1, v42

    if-le v0, v1, :cond_1e

    if-nez v5, :cond_1e

    .line 1198
    new-instance v10, Landroid/content/res/ColorStateList;

    .line 1199
    const/16 v41, 0x1

    move/from16 v0, v41

    new-array v0, v0, [[I

    move-object/from16 v41, v0

    const/16 v42, 0x0

    const/16 v43, 0x1

    move/from16 v0, v43

    new-array v0, v0, [I

    move-object/from16 v43, v0

    const/16 v44, 0x0

    const v45, 0x10100a7

    aput v45, v43, v44

    aput-object v43, v41, v42

    const/16 v42, 0x1

    move/from16 v0, v42

    new-array v0, v0, [I

    move-object/from16 v42, v0

    const/16 v43, 0x0

    const/16 v44, 0x1e

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    invoke-static/range {v44 .. v47}, Landroid/graphics/Color;->argb(IIII)I

    move-result v44

    aput v44, v42, v43

    .line 1198
    move-object/from16 v0, v41

    move-object/from16 v1, v42

    invoke-direct {v10, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 1200
    .local v10, "colorStateList":Landroid/content/res/ColorStateList;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    new-instance v42, Landroid/graphics/drawable/RippleDrawable;

    const/16 v43, 0x0

    const/16 v44, 0x0

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    move-object/from16 v2, v44

    invoke-direct {v0, v10, v1, v2}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1205
    .end local v10    # "colorStateList":Landroid/content/res/ColorStateList;
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    neg-int v0, v15

    move/from16 v42, v0

    neg-int v0, v15

    move/from16 v43, v0

    neg-int v0, v15

    move/from16 v44, v0

    neg-int v0, v15

    move/from16 v45, v0

    invoke-virtual/range {v41 .. v45}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setPadding(IIII)V

    .line 1206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    move/from16 v41, v0

    move-object/from16 v0, v42

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setId(I)V

    .line 1207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    if-eqz v41, :cond_d

    .line 1208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->mOnButtonClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1211
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    sget-object v42, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    new-instance v42, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$2;

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)V

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v26

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1239
    new-instance v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$3;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1249
    .end local v15    # "delta_padding":I
    :cond_e
    move/from16 v0, p1

    move/from16 v1, v25

    if-ne v0, v1, :cond_12

    .line 1250
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    new-instance v42, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v43, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static/range {v43 .. v43}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v43

    const/16 v44, 0x2

    invoke-direct/range {v42 .. v44}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;-><init>(Landroid/content/Context;I)V

    invoke-static/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;)V

    .line 1251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v41

    if-eqz v41, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v41

    if-nez v41, :cond_1f

    .line 1254
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1259
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/util/ArrayList;->size()I

    move-result v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v42, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I
    invoke-static/range {v42 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)I

    move-result v42

    move/from16 v0, v41

    move/from16 v1, v42

    if-le v0, v1, :cond_10

    .line 1260
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v42, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;
    invoke-static/range {v42 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getRightEdgeEffect()Landroid/widget/EdgeEffect;

    move-result-object v42

    .line 1261
    const/16 v43, 0x2

    .line 1260
    invoke-virtual/range {v41 .. v43}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setEdgeEffect(Landroid/widget/EdgeEffect;I)V

    .line 1264
    :cond_10
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$8()I

    move-result v41

    const/16 v42, 0x1

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_23

    .line 1265
    if-eqz v5, :cond_21

    .line 1266
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$12()I

    move-result v41

    const/16 v42, 0x10

    move/from16 v0, v41

    move/from16 v1, v42

    if-ge v0, v1, :cond_20

    .line 1267
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1275
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_22

    .line 1276
    const/4 v15, 0x5

    .line 1294
    .restart local v15    # "delta_padding":I
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move-object/from16 v0, v42

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setClickable(Z)V

    .line 1295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move-object/from16 v0, v42

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setFocusable(Z)V

    .line 1296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move-object/from16 v0, v42

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setLongClickable(Z)V

    .line 1297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move-object/from16 v0, v42

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setEnabled(Z)V

    .line 1299
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$12()I

    move-result v41

    const/16 v42, 0x13

    move/from16 v0, v41

    move/from16 v1, v42

    if-le v0, v1, :cond_26

    if-nez v5, :cond_26

    .line 1300
    new-instance v10, Landroid/content/res/ColorStateList;

    .line 1301
    const/16 v41, 0x1

    move/from16 v0, v41

    new-array v0, v0, [[I

    move-object/from16 v41, v0

    const/16 v42, 0x0

    const/16 v43, 0x1

    move/from16 v0, v43

    new-array v0, v0, [I

    move-object/from16 v43, v0

    const/16 v44, 0x0

    const v45, 0x10100a7

    aput v45, v43, v44

    aput-object v43, v41, v42

    const/16 v42, 0x1

    move/from16 v0, v42

    new-array v0, v0, [I

    move-object/from16 v42, v0

    const/16 v43, 0x0

    const/16 v44, 0x1e

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    invoke-static/range {v44 .. v47}, Landroid/graphics/Color;->argb(IIII)I

    move-result v44

    aput v44, v42, v43

    .line 1300
    move-object/from16 v0, v41

    move-object/from16 v1, v42

    invoke-direct {v10, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 1302
    .restart local v10    # "colorStateList":Landroid/content/res/ColorStateList;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    new-instance v42, Landroid/graphics/drawable/RippleDrawable;

    const/16 v43, 0x0

    const/16 v44, 0x0

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    move-object/from16 v2, v44

    invoke-direct {v0, v10, v1, v2}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1307
    .end local v10    # "colorStateList":Landroid/content/res/ColorStateList;
    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    neg-int v0, v15

    move/from16 v42, v0

    neg-int v0, v15

    move/from16 v43, v0

    neg-int v0, v15

    move/from16 v44, v0

    neg-int v0, v15

    move/from16 v45, v0

    invoke-virtual/range {v41 .. v45}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setPadding(IIII)V

    .line 1308
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    move/from16 v41, v0

    move-object/from16 v0, v42

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setId(I)V

    .line 1309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    if-eqz v41, :cond_11

    .line 1310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->mOnButtonClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1313
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    sget-object v42, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    new-instance v42, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$4;

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)V

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1339
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v26

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1341
    new-instance v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$5;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$5;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1450
    .end local v15    # "delta_padding":I
    :cond_12
    :goto_e
    new-instance v19, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v41

    move-object/from16 v0, v19

    move-object/from16 v1, v41

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1452
    .local v19, "image":Landroid/widget/ImageView;
    const/16 v41, 0xa

    move-object/from16 v0, v19

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    .line 1453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    if-nez v41, :cond_32

    .line 1454
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableDisableItem:Landroid/graphics/drawable/Drawable;

    move-object/from16 v41, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1459
    :goto_f
    const/16 v41, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1460
    new-instance v21, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1461
    const/16 v41, -0x2

    const/16 v42, -0x2

    .line 1460
    move-object/from16 v0, v21

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1462
    .local v21, "imageParam":Landroid/widget/RelativeLayout$LayoutParams;
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$8()I

    move-result v41

    const/16 v42, 0x1

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_37

    .line 1463
    const/16 v41, 0x1

    const/high16 v42, 0x42040000    # 33.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_33

    .line 1465
    const/16 v41, 0x1

    const/high16 v42, 0x41500000    # 13.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1469
    :goto_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_35

    .line 1470
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_34

    .line 1471
    const/16 v41, 0x1

    const/high16 v42, 0x420c0000    # 35.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1491
    :goto_11
    const/16 v41, 0x6

    move-object/from16 v0, v21

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1492
    const/16 v41, 0xe

    move-object/from16 v0, v21

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1493
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1494
    move-object/from16 v0, v26

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1496
    new-instance v32, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v41

    move-object/from16 v0, v32

    move-object/from16 v1, v41

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1497
    .local v32, "text":Landroid/widget/TextView;
    new-instance v33, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1498
    const/16 v41, -0x1

    const/16 v42, -0x2

    .line 1497
    move-object/from16 v0, v33

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1499
    .local v33, "textParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v41, 0xe

    move-object/from16 v0, v33

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1500
    const/16 v41, 0xc

    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageView;->getId()I

    move-result v42

    move-object/from16 v0, v33

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1502
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$8()I

    move-result v41

    const/16 v42, 0x1

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_40

    .line 1503
    const/16 v41, 0x1

    const/high16 v42, 0x40400000    # 3.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v33

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1504
    const/16 v41, 0x1

    const/high16 v42, 0x40400000    # 3.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v33

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1505
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_39

    .line 1506
    const/16 v41, 0x1

    const/high16 v42, 0x41000000    # 8.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v33

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1510
    :goto_12
    const-string v41, "#1e1e1e"

    invoke-static/range {v41 .. v41}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v41

    move-object/from16 v0, v32

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1511
    const/16 v41, 0x1

    const/high16 v42, 0x41880000    # 17.0f

    move-object/from16 v0, v32

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1513
    const/16 v24, 0x1

    .line 1514
    .local v24, "isNormalText":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_3a

    .line 1515
    const/16 v41, 0x1

    const/high16 v42, 0x40e00000    # 7.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v33

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1516
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    move-object/from16 v41, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1610
    :cond_13
    :goto_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    if-nez v41, :cond_14

    .line 1611
    const v41, 0x3e99999a    # 0.3f

    move-object/from16 v0, v32

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1613
    :cond_14
    sget-object v41, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v32

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1614
    const/16 v41, 0x2

    move-object/from16 v0, v32

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1615
    invoke-virtual/range {v32 .. v33}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1616
    const/16 v41, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 1617
    move-object/from16 v0, v26

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1619
    move-object/from16 v0, v26

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1621
    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1622
    const v41, 0x3ac90

    add-int v41, v41, v23

    move-object/from16 v0, v28

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setId(I)V

    goto/16 :goto_0

    .line 1105
    .end local v5    # "backgroundImage":Landroid/graphics/drawable/Drawable;
    .end local v7    # "bgImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "buttonLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v17    # "height":I
    .end local v19    # "image":Landroid/widget/ImageView;
    .end local v20    # "imageDrawable":Landroid/graphics/drawable/StateListDrawable;
    .end local v21    # "imageParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v24    # "isNormalText":Z
    .end local v25    # "itemNum":I
    .end local v26    # "layoutButton":Landroid/widget/RelativeLayout;
    .end local v32    # "text":Landroid/widget/TextView;
    .end local v33    # "textParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v35    # "transparentLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v36    # "transparentView":Landroid/view/View;
    .end local v39    # "width":I
    :cond_15
    const/16 v41, 0x1

    int-to-float v0, v14

    move/from16 v42, v0

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v39, v0

    .line 1106
    .restart local v39    # "width":I
    const/16 v41, 0x1

    int-to-float v0, v12

    move/from16 v42, v0

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v17, v0

    .restart local v17    # "height":I
    goto/16 :goto_4

    .line 1125
    .restart local v5    # "backgroundImage":Landroid/graphics/drawable/Drawable;
    .restart local v8    # "buttonLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v20    # "imageDrawable":Landroid/graphics/drawable/StateListDrawable;
    .restart local v26    # "layoutButton":Landroid/widget/RelativeLayout;
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v42, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static/range {v42 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v42

    const-string v43, "quick_popup_bg_press"

    .line 1126
    const-string v44, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v45

    .line 1125
    invoke-virtual/range {v42 .. v45}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    goto/16 :goto_5

    .line 1153
    .restart local v7    # "bgImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v25    # "itemNum":I
    .restart local v35    # "transparentLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v36    # "transparentView":Landroid/view/View;
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    move-object/from16 v41, v0

    move-object/from16 v0, v42

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 1167
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_7

    .line 1170
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    const/16 v42, 0xff

    const/16 v43, 0xff

    const/16 v44, 0xff

    invoke-static/range {v42 .. v44}, Landroid/graphics/Color;->rgb(III)I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackgroundColor(I)V

    goto/16 :goto_7

    .line 1176
    :cond_1a
    const/16 v15, 0x8

    .line 1178
    .restart local v15    # "delta_padding":I
    goto/16 :goto_8

    .line 1179
    .end local v15    # "delta_padding":I
    :cond_1b
    if-eqz v5, :cond_1d

    .line 1180
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$12()I

    move-result v41

    const/16 v42, 0x10

    move/from16 v0, v41

    move/from16 v1, v42

    if-ge v0, v1, :cond_1c

    .line 1181
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1189
    :goto_14
    const/4 v15, 0x5

    .restart local v15    # "delta_padding":I
    goto/16 :goto_8

    .line 1183
    .end local v15    # "delta_padding":I
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_14

    .line 1186
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    const/16 v42, 0xe9

    const/16 v43, 0xeb

    const/16 v44, 0xec

    invoke-static/range {v42 .. v44}, Landroid/graphics/Color;->rgb(III)I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackgroundColor(I)V

    goto :goto_14

    .line 1202
    .restart local v15    # "delta_padding":I
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v41

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_9

    .line 1256
    .end local v15    # "delta_padding":I
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    move-object/from16 v41, v0

    move-object/from16 v0, v42

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_a

    .line 1269
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_b

    .line 1272
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    const/16 v42, 0xff

    const/16 v43, 0xff

    const/16 v44, 0xff

    invoke-static/range {v42 .. v44}, Landroid/graphics/Color;->rgb(III)I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackgroundColor(I)V

    goto/16 :goto_b

    .line 1278
    :cond_22
    const/16 v15, 0x8

    .line 1280
    .restart local v15    # "delta_padding":I
    goto/16 :goto_c

    .line 1281
    .end local v15    # "delta_padding":I
    :cond_23
    if-eqz v5, :cond_25

    .line 1282
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$12()I

    move-result v41

    const/16 v42, 0x10

    move/from16 v0, v41

    move/from16 v1, v42

    if-ge v0, v1, :cond_24

    .line 1283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1291
    :goto_15
    const/4 v15, 0x5

    .restart local v15    # "delta_padding":I
    goto/16 :goto_c

    .line 1285
    .end local v15    # "delta_padding":I
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_15

    .line 1288
    :cond_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    const/16 v42, 0xe9

    const/16 v43, 0xeb

    const/16 v44, 0xec

    invoke-static/range {v42 .. v44}, Landroid/graphics/Color;->rgb(III)I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackgroundColor(I)V

    goto :goto_15

    .line 1304
    .restart local v15    # "delta_padding":I
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v41

    move-object/from16 v0, v41

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_d

    .line 1351
    .end local v15    # "delta_padding":I
    :cond_27
    new-instance v6, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-direct {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;-><init>(Landroid/content/Context;)V

    .line 1352
    .local v6, "bgImage":Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1354
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v41

    if-eqz v41, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v41

    if-nez v41, :cond_2a

    .line 1355
    :cond_28
    const/16 v41, 0x0

    move-object/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1360
    :goto_16
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$8()I

    move-result v41

    const/16 v42, 0x1

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_2e

    .line 1361
    if-eqz v5, :cond_2c

    .line 1362
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$12()I

    move-result v41

    const/16 v42, 0x10

    move/from16 v0, v41

    move/from16 v1, v42

    if-ge v0, v1, :cond_2b

    .line 1363
    invoke-virtual {v6, v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1371
    :goto_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_2d

    .line 1372
    const/4 v15, 0x5

    .line 1390
    .restart local v15    # "delta_padding":I
    :goto_18
    sget-object v41, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    move-object/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1392
    const/16 v41, 0x1

    int-to-float v0, v15

    move/from16 v42, v0

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v15, v0

    .line 1394
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setClickable(Z)V

    .line 1395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setFocusable(Z)V

    .line 1396
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setLongClickable(Z)V

    .line 1397
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    move/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setEnabled(Z)V

    .line 1399
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$12()I

    move-result v41

    const/16 v42, 0x13

    move/from16 v0, v41

    move/from16 v1, v42

    if-le v0, v1, :cond_31

    if-nez v5, :cond_31

    .line 1400
    new-instance v10, Landroid/content/res/ColorStateList;

    .line 1401
    const/16 v41, 0x1

    move/from16 v0, v41

    new-array v0, v0, [[I

    move-object/from16 v41, v0

    const/16 v42, 0x0

    const/16 v43, 0x1

    move/from16 v0, v43

    new-array v0, v0, [I

    move-object/from16 v43, v0

    const/16 v44, 0x0

    const v45, 0x10100a7

    aput v45, v43, v44

    aput-object v43, v41, v42

    const/16 v42, 0x1

    move/from16 v0, v42

    new-array v0, v0, [I

    move-object/from16 v42, v0

    const/16 v43, 0x0

    const/16 v44, 0x1e

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    invoke-static/range {v44 .. v47}, Landroid/graphics/Color;->argb(IIII)I

    move-result v44

    aput v44, v42, v43

    .line 1400
    move-object/from16 v0, v41

    move-object/from16 v1, v42

    invoke-direct {v10, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 1402
    .restart local v10    # "colorStateList":Landroid/content/res/ColorStateList;
    new-instance v41, Landroid/graphics/drawable/RippleDrawable;

    const/16 v42, 0x0

    const/16 v43, 0x0

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    move-object/from16 v2, v43

    invoke-direct {v0, v10, v1, v2}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1407
    .end local v10    # "colorStateList":Landroid/content/res/ColorStateList;
    :goto_19
    neg-int v0, v15

    move/from16 v41, v0

    neg-int v0, v15

    move/from16 v42, v0

    neg-int v0, v15

    move/from16 v43, v0

    neg-int v0, v15

    move/from16 v44, v0

    move/from16 v0, v41

    move/from16 v1, v42

    move/from16 v2, v43

    move/from16 v3, v44

    invoke-virtual {v6, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setPadding(IIII)V

    .line 1408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    move/from16 v41, v0

    move/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setId(I)V

    .line 1409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v41, v0

    if-eqz v41, :cond_29

    .line 1410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->mOnButtonClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1413
    :cond_29
    new-instance v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$6;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$6;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)V

    move-object/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1438
    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1440
    new-instance v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$7;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$7;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_e

    .line 1357
    .end local v15    # "delta_padding":I
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_16

    .line 1365
    :cond_2b
    invoke-virtual {v6, v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_17

    .line 1368
    :cond_2c
    const/16 v41, 0xff

    const/16 v42, 0xff

    const/16 v43, 0xff

    invoke-static/range {v41 .. v43}, Landroid/graphics/Color;->rgb(III)I

    move-result v41

    move/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackgroundColor(I)V

    goto/16 :goto_17

    .line 1374
    :cond_2d
    const/16 v15, 0x8

    .line 1376
    .restart local v15    # "delta_padding":I
    goto/16 :goto_18

    .line 1377
    .end local v15    # "delta_padding":I
    :cond_2e
    if-eqz v5, :cond_30

    .line 1378
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$12()I

    move-result v41

    const/16 v42, 0x10

    move/from16 v0, v41

    move/from16 v1, v42

    if-ge v0, v1, :cond_2f

    .line 1379
    invoke-virtual {v6, v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1387
    :goto_1a
    const/4 v15, 0x5

    .restart local v15    # "delta_padding":I
    goto/16 :goto_18

    .line 1381
    .end local v15    # "delta_padding":I
    :cond_2f
    invoke-virtual {v6, v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1a

    .line 1384
    :cond_30
    const/16 v41, 0xe9

    const/16 v42, 0xeb

    const/16 v43, 0xec

    invoke-static/range {v41 .. v43}, Landroid/graphics/Color;->rgb(III)I

    move-result v41

    move/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setBackgroundColor(I)V

    goto :goto_1a

    .line 1404
    .restart local v15    # "delta_padding":I
    :cond_31
    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_19

    .line 1456
    .end local v6    # "bgImage":Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    .end local v15    # "delta_padding":I
    .restart local v19    # "image":Landroid/widget/ImageView;
    :cond_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableNormalItem:Landroid/graphics/drawable/Drawable;

    move-object/from16 v41, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_f

    .line 1467
    .restart local v21    # "imageParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_33
    const/16 v41, 0x1

    const/high16 v42, 0x41000000    # 8.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto/16 :goto_10

    .line 1473
    :cond_34
    const/16 v41, 0x1

    const/high16 v42, 0x42000000    # 32.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_11

    .line 1476
    :cond_35
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_36

    .line 1477
    const/16 v41, 0x1

    const/high16 v42, 0x42500000    # 52.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_11

    .line 1479
    :cond_36
    const/16 v41, 0x1

    const/high16 v42, 0x42500000    # 52.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_11

    .line 1483
    :cond_37
    const/16 v41, 0x1

    const/high16 v42, 0x41a00000    # 20.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1484
    const/16 v41, 0x1

    const/high16 v42, 0x41000000    # 8.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1485
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_38

    .line 1486
    const/16 v41, 0x1

    const/high16 v42, 0x41a80000    # 21.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_11

    .line 1488
    :cond_38
    const/16 v41, 0x1

    const/high16 v42, 0x42180000    # 38.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_11

    .line 1508
    .restart local v32    # "text":Landroid/widget/TextView;
    .restart local v33    # "textParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_39
    const/16 v41, 0x1

    const/high16 v42, 0x41200000    # 10.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v33

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto/16 :goto_12

    .line 1518
    .restart local v24    # "isNormalText":Z
    :cond_3a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    move-object/from16 v31, v0

    .line 1519
    .local v31, "str":Ljava/lang/String;
    if-eqz v31, :cond_13

    .line 1520
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v41

    move/from16 v0, v41

    new-array v0, v0, [F

    move-object/from16 v40, v0

    .line 1521
    .local v40, "widths":[F
    invoke-virtual/range {v32 .. v32}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v29

    .line 1522
    .local v29, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, v29

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 1523
    const/16 v41, 0x0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v42

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    move/from16 v2, v41

    move/from16 v3, v42

    move-object/from16 v4, v40

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    move-result v9

    .line 1524
    .local v9, "c":I
    const/16 v34, 0x0

    .line 1525
    .local v34, "textWidth":F
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_1b
    move/from16 v0, v18

    if-lt v0, v9, :cond_3b

    .line 1529
    const/16 v41, 0x1

    .line 1530
    const/high16 v42, 0x42c40000    # 98.0f

    .line 1529
    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v38, v0

    .line 1531
    .local v38, "viewWidth":I
    const/16 v41, 0x1

    const/high16 v42, 0x40400000    # 3.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    mul-int/lit8 v41, v41, 0x2

    sub-int v38, v38, v41

    .line 1533
    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v41, v0

    cmpg-float v41, v34, v41

    if-gez v41, :cond_3c

    .line 1534
    const/16 v24, 0x1

    .line 1538
    :goto_1c
    const/16 v41, 0x1

    const/high16 v42, 0x40e00000    # 7.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v33

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1539
    const/16 v41, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_3d

    .line 1541
    const/16 v41, 0x1

    .line 1542
    const/high16 v42, 0x40800000    # 4.0f

    .line 1541
    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    int-to-float v0, v0

    move/from16 v41, v0

    .line 1542
    const/high16 v42, 0x3f800000    # 1.0f

    .line 1541
    move-object/from16 v0, v32

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 1548
    :goto_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_3e

    .line 1549
    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x1

    .line 1550
    const/high16 v45, -0x3f600000    # -5.0f

    .line 1549
    move/from16 v0, v44

    move/from16 v1, v45

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v44

    move/from16 v0, v44

    float-to-int v0, v0

    move/from16 v44, v0

    move-object/from16 v0, v32

    move/from16 v1, v41

    move/from16 v2, v42

    move/from16 v3, v43

    move/from16 v4, v44

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1556
    :goto_1e
    if-eqz v24, :cond_3f

    .line 1557
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v42

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v42, "\n"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v32

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_13

    .line 1526
    .end local v38    # "viewWidth":I
    :cond_3b
    aget v41, v40, v18

    add-float v34, v34, v41

    .line 1525
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_1b

    .line 1536
    .restart local v38    # "viewWidth":I
    :cond_3c
    const/16 v24, 0x0

    goto/16 :goto_1c

    .line 1544
    :cond_3d
    const/16 v41, 0x1

    .line 1545
    const/high16 v42, 0x40a00000    # 5.0f

    .line 1544
    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    int-to-float v0, v0

    move/from16 v41, v0

    .line 1545
    const/high16 v42, 0x3f800000    # 1.0f

    .line 1544
    move-object/from16 v0, v32

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setLineSpacing(FF)V

    goto :goto_1d

    .line 1552
    :cond_3e
    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x1

    .line 1553
    const/high16 v45, -0x3ee00000    # -10.0f

    .line 1552
    move/from16 v0, v44

    move/from16 v1, v45

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v44

    move/from16 v0, v44

    float-to-int v0, v0

    move/from16 v44, v0

    move-object/from16 v0, v32

    move/from16 v1, v41

    move/from16 v2, v42

    move/from16 v3, v43

    move/from16 v4, v44

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1e

    .line 1559
    :cond_3f
    move-object/from16 v0, v32

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_13

    .line 1565
    .end local v9    # "c":I
    .end local v18    # "i":I
    .end local v24    # "isNormalText":Z
    .end local v29    # "paint":Landroid/graphics/Paint;
    .end local v31    # "str":Ljava/lang/String;
    .end local v34    # "textWidth":F
    .end local v38    # "viewWidth":I
    .end local v40    # "widths":[F
    :cond_40
    const/16 v41, 0x1

    const/high16 v42, 0x40400000    # 3.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v33

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1566
    const/16 v41, 0x1

    const/high16 v42, 0x40400000    # 3.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v33

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1567
    const/16 v41, 0x1

    const/high16 v42, 0x40000000    # 2.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v33

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1569
    const/high16 v41, -0x1000000

    move-object/from16 v0, v32

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1570
    const/16 v41, 0x1

    const/high16 v42, 0x41400000    # 12.0f

    move-object/from16 v0, v32

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1571
    const/16 v24, 0x1

    .line 1572
    .restart local v24    # "isNormalText":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v41

    if-eqz v41, :cond_41

    .line 1573
    const/16 v41, 0x1

    const/high16 v42, 0x40a00000    # 5.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v33

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1574
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    move-object/from16 v41, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_13

    .line 1576
    :cond_41
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v41, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v41 .. v41}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    move-object/from16 v31, v0

    .line 1577
    .restart local v31    # "str":Ljava/lang/String;
    if-eqz v31, :cond_13

    .line 1578
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v41

    move/from16 v0, v41

    new-array v0, v0, [F

    move-object/from16 v40, v0

    .line 1579
    .restart local v40    # "widths":[F
    invoke-virtual/range {v32 .. v32}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v29

    .line 1580
    .restart local v29    # "paint":Landroid/graphics/Paint;
    move-object/from16 v0, v29

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 1581
    const/16 v41, 0x0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v42

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    move/from16 v2, v41

    move/from16 v3, v42

    move-object/from16 v4, v40

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    move-result v9

    .line 1582
    .restart local v9    # "c":I
    const/16 v34, 0x0

    .line 1583
    .restart local v34    # "textWidth":F
    const/16 v18, 0x0

    .restart local v18    # "i":I
    :goto_1f
    move/from16 v0, v18

    if-lt v0, v9, :cond_42

    .line 1587
    const/16 v41, 0x1

    .line 1588
    const/high16 v42, 0x42900000    # 72.0f

    .line 1587
    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v38, v0

    .line 1589
    .restart local v38    # "viewWidth":I
    const/16 v41, 0x1

    const/high16 v42, 0x40400000    # 3.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    mul-int/lit8 v41, v41, 0x2

    sub-int v38, v38, v41

    .line 1591
    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v41, v0

    cmpg-float v41, v34, v41

    if-gez v41, :cond_43

    .line 1592
    const/16 v24, 0x1

    .line 1596
    :goto_20
    const/16 v41, 0x1

    const/high16 v42, 0x40a00000    # 5.0f

    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v33

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1597
    const/16 v41, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1598
    const/16 v41, 0x1

    .line 1599
    const/high16 v42, 0x40400000    # 3.0f

    .line 1598
    move/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    int-to-float v0, v0

    move/from16 v41, v0

    .line 1599
    const/high16 v42, 0x3f400000    # 0.75f

    .line 1598
    move-object/from16 v0, v32

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 1600
    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x1

    .line 1601
    const/16 v45, 0x0

    .line 1600
    move/from16 v0, v44

    move/from16 v1, v45

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v44

    move/from16 v0, v44

    float-to-int v0, v0

    move/from16 v44, v0

    move-object/from16 v0, v32

    move/from16 v1, v41

    move/from16 v2, v42

    move/from16 v3, v43

    move/from16 v4, v44

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1602
    if-eqz v24, :cond_44

    .line 1603
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v42

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v42, "\n"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v32

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_13

    .line 1584
    .end local v38    # "viewWidth":I
    :cond_42
    aget v41, v40, v18

    add-float v34, v34, v41

    .line 1583
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_1f

    .line 1594
    .restart local v38    # "viewWidth":I
    :cond_43
    const/16 v24, 0x0

    goto/16 :goto_20

    .line 1605
    :cond_44
    move-object/from16 v0, v32

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_13
.end method
