.class public abstract Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;
.super Ljava/lang/Object;
.source "SpenRecognitionBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;
    }
.end annotation


# instance fields
.field protected mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

.field private mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pluginObject"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    .line 116
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;

    .line 36
    if-nez p1, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_ARG : parameter \'context\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    .line 41
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;)Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;

    return-object v0
.end method


# virtual methods
.method getPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    return-object v0
.end method

.method public request(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : SpenRecognitionBase is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    if-nez p1, :cond_1

    .line 71
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_ARG : input is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;->request(Ljava/util/List;)V

    .line 75
    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : SpenRecognitionBase is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;

    .line 98
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;)V

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;->setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;)V

    .line 112
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;->setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;)V

    goto :goto_0
.end method
