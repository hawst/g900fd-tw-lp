.class Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
.super Landroid/view/View;
.source "SpenColorPaletteView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;
    }
.end annotation


# static fields
.field private static final COLOR_COLUMN_NUM:I = 0x7

.field private static final COLOR_NUM_MAX:I = 0xe

.field private static final COLOR_ROW_NUM:I = 0x2

.field private static final CUSTOM_COLOR_IDX:I = 0xc

.field protected static final DEFAULT_COLOR:I = -0x1000000

.field protected static IS_COLOR_GRADATION_SELECT:Z = false

.field private static ITEM_BORDER_WIDTH:I = 0x0

.field private static ITEM_GAPX:I = 0x0

.field private static ITEM_GAPY:I = 0x0

.field private static final SPOID_ICON:I = -0x1100001

.field private static final USER_COLOR:I = -0x1000002

.field private static final WINDOW_BORDER_WIDTH:I

.field private static final mColorTableSetStringName:[[Ljava/lang/String;

.field private static final mColorTableStringName:[Ljava/lang/String;


# instance fields
.field private currentTableIndex:I

.field isRainbow:Z

.field private mBorderPaint:Landroid/graphics/Paint;

.field private mCanvasRect:Landroid/graphics/Rect;

.field private mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

.field public mColorContentDescritionTable:[Ljava/lang/String;

.field public mColorContentDescritionTableSet:[[Ljava/lang/String;

.field private mColorPaint:Landroid/graphics/Paint;

.field private mColorTable:[I

.field private mColorTableSet:[[I

.field private mCustom_imagepath:Ljava/lang/String;

.field private mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

.field private mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

.field private mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

.field private mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

.field mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mFirstExecuted:Z

.field private mIsColorPickerEnable:Z

.field private mItemRect:Landroid/graphics/Rect;

.field private mSelectRect:Landroid/graphics/Rect;

.field private mSeletedItem:I

.field private final mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field private mcurrentFocus:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 35
    sput-boolean v4, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->IS_COLOR_GRADATION_SELECT:Z

    .line 37
    sput v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    .line 40
    sput v6, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_GAPX:I

    .line 41
    const/4 v0, 0x5

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_GAPY:I

    .line 122
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    .line 123
    const-string v1, "string_color_royal_blue"

    aput-object v1, v0, v4

    const-string v1, "string_color_indigo"

    aput-object v1, v0, v5

    const-string v1, "string_color_permanent_violet"

    aput-object v1, v0, v7

    const-string v1, "string_color_pitch_black"

    aput-object v1, v0, v6

    .line 124
    const-string v1, "string_color_grey"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "string_color_viridian"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "string_color_sap_green"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "string_color_vandyke_brown"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 125
    const-string v2, "string_color_yellow_ochre"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "string_color_burnt_umber"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "string_color_lilac"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "string_color_burgundy"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 126
    const-string v2, "string_color_palette"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "string_color_picker_tts"

    aput-object v2, v0, v1

    .line 122
    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableStringName:[Ljava/lang/String;

    .line 129
    new-array v0, v8, [[Ljava/lang/String;

    .line 130
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "string_color_royal_blue"

    aput-object v2, v1, v4

    const-string v2, "string_color_indigo"

    aput-object v2, v1, v5

    const-string v2, "string_color_permanent_violet"

    aput-object v2, v1, v7

    const-string v2, "string_color_pitch_black"

    aput-object v2, v1, v6

    .line 131
    const-string v2, "string_color_grey"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "string_color_viridian"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "string_color_sap_green"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "string_color_vandyke_brown"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    .line 132
    const-string v3, "string_color_yellow_ochre"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "string_color_burnt_umber"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "string_color_lilac"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "string_color_burgundy"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    .line 133
    const-string v3, "string_color_palette"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "string_color_picker_tts"

    aput-object v3, v1, v2

    aput-object v1, v0, v4

    .line 135
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "string_color_white"

    aput-object v2, v1, v4

    const-string v2, "string_color_yellow"

    aput-object v2, v1, v5

    const-string v2, "string_color_coral"

    aput-object v2, v1, v7

    const-string v2, "string_color_tomato"

    aput-object v2, v1, v6

    .line 136
    const-string v2, "string_color_hot_pink"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "string_color_plum"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "string_color_blue_violet"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "string_color_forest_green"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    .line 137
    const-string v3, "string_color_sky_blue"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "string_color_peacock_blue"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "string_color_dark_grey"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "string_color_black"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    .line 138
    const-string v3, "string_color_palette"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "string_color_picker_tts"

    aput-object v3, v1, v2

    aput-object v1, v0, v5

    .line 140
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "string_color_peach_puff"

    aput-object v2, v1, v4

    const-string v2, "string_color_light_salmon"

    aput-object v2, v1, v5

    const-string v2, "string_color_dark_salmon"

    aput-object v2, v1, v7

    const-string v2, "string_color_light_pink"

    aput-object v2, v1, v6

    .line 141
    const-string v2, "string_color_magenta"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "string_color_crimson"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "string_color_red"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "string_color_maroon"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    .line 142
    const-string v3, "string_color_purple"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "string_color_medium_orchid"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "string_color_dim_grey"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "string_color_saddle_brown"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    .line 143
    const-string v3, "string_color_palette"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "string_color_picker_tts"

    aput-object v3, v1, v2

    aput-object v1, v0, v7

    .line 145
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "string_color_gold"

    aput-object v2, v1, v4

    const-string v2, "string_color_orange"

    aput-object v2, v1, v5

    const-string v2, "string_color_chartreuse"

    aput-object v2, v1, v7

    const-string v2, "string_color_lime_green"

    aput-object v2, v1, v6

    .line 146
    const-string v2, "string_color_sea_green"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "string_color_green"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "string_color_olive_drab"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "string_color_dark_cyan"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    .line 147
    const-string v3, "string_color_teal"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "string_color_steel_blue"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "string_color_blue"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "string_color_dark_blue"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    .line 148
    const-string v3, "string_color_palette"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "string_color_picker_tts"

    aput-object v3, v1, v2

    aput-object v1, v0, v6

    .line 129
    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSetStringName:[[Ljava/lang/String;

    .line 151
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;F)V
    .locals 12
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "custom_imagepath"    # Ljava/lang/String;
    .param p3, "ratio"    # F

    .prologue
    const/16 v11, 0xa

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 179
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 47
    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 50
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->isRainbow:Z

    .line 52
    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    .line 55
    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    .line 58
    const/16 v0, 0xe

    new-array v0, v0, [I

    .line 59
    const/16 v1, 0x38

    const/16 v2, 0xd0

    invoke-static {v9, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v7

    const/16 v1, 0x4c

    const/16 v2, 0x71

    invoke-static {v7, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v8

    const/4 v1, 0x2

    const/16 v2, 0x28

    const/16 v3, 0x10

    const/16 v4, 0x67

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    .line 60
    const/16 v1, 0x9

    const/16 v2, 0xf

    invoke-static {v11, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/16 v1, 0x54

    const/16 v2, 0x54

    const/16 v3, 0x54

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v10

    const/4 v1, 0x5

    const/16 v2, 0x15

    const/16 v3, 0x5d

    const/16 v4, 0x3a

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x45

    const/16 v3, 0x62

    const/16 v4, 0x16

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    .line 61
    const/16 v2, 0x62

    const/16 v3, 0x36

    const/16 v4, 0x17

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0xe5

    const/16 v3, 0x8a

    const/16 v4, 0x17

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0xa5

    const/16 v3, 0x3b

    const/16 v4, 0x11

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa9

    const/16 v2, 0x36

    const/16 v3, 0x4d

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v11

    const/16 v1, 0xb

    .line 62
    const/16 v2, 0x7e

    const/16 v3, 0xe

    const/16 v4, 0x41

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const v2, -0x1000002

    aput v2, v0, v1

    const/16 v1, 0xd

    const v2, -0x1100001

    aput v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    .line 66
    new-array v0, v10, [[I

    .line 67
    const/16 v1, 0xe

    new-array v1, v1, [I

    const/16 v2, 0x38

    const/16 v3, 0xd0

    invoke-static {v9, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v7

    const/16 v2, 0x4c

    const/16 v3, 0x71

    invoke-static {v7, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v8

    const/4 v2, 0x2

    const/16 v3, 0x28

    const/16 v4, 0x10

    const/16 v5, 0x67

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    .line 68
    const/16 v2, 0x9

    const/16 v3, 0xf

    invoke-static {v11, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v9

    const/16 v2, 0x54

    const/16 v3, 0x54

    const/16 v4, 0x54

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v10

    const/4 v2, 0x5

    const/16 v3, 0x15

    const/16 v4, 0x5d

    const/16 v5, 0x3a

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x6

    const/16 v3, 0x45

    const/16 v4, 0x62

    const/16 v5, 0x16

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x7

    .line 69
    const/16 v3, 0x62

    const/16 v4, 0x36

    const/16 v5, 0x17

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x8

    const/16 v3, 0xe5

    const/16 v4, 0x8a

    const/16 v5, 0x17

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x9

    const/16 v3, 0xa5

    const/16 v4, 0x3b

    const/16 v5, 0x11

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0xa9

    const/16 v3, 0x36

    const/16 v4, 0x4d

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v11

    const/16 v2, 0xb

    .line 70
    const/16 v3, 0x7e

    const/16 v4, 0xe

    const/16 v5, 0x41

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0xc

    const v3, -0x1000002

    aput v3, v1, v2

    const/16 v2, 0xd

    const v3, -0x1100001

    aput v3, v1, v2

    aput-object v1, v0, v7

    .line 72
    const/16 v1, 0xe

    new-array v1, v1, [I

    const/16 v2, 0xff

    const/16 v3, 0xff

    const/16 v4, 0xff

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v7

    const/16 v2, 0xfd

    const/16 v3, 0xff

    const/16 v4, 0x2d

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v8

    const/4 v2, 0x2

    const/16 v3, 0xff

    const/16 v4, 0x83

    const/16 v5, 0x5d

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    .line 73
    const/16 v2, 0xff

    const/16 v3, 0x3b

    const/16 v4, 0x5b

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v9

    const/16 v2, 0xff

    const/16 v3, 0x49

    const/16 v4, 0xc9

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v10

    const/4 v2, 0x5

    const/16 v3, 0xca

    const/16 v4, 0x85

    const/16 v5, 0xff

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x6

    const/16 v3, 0x7a

    const/16 v4, 0x37

    const/16 v5, 0xde

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x7

    .line 74
    const/16 v3, 0x94

    const/16 v4, 0x2e

    invoke-static {v8, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x8

    const/16 v3, 0x38

    const/16 v4, 0xa8

    const/16 v5, 0xff

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x9

    const/16 v3, 0x33

    const/16 v4, 0x67

    const/16 v5, 0xfd

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0xa6

    const/16 v3, 0xa5

    const/16 v4, 0xa5

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v11

    const/16 v2, 0xb

    .line 75
    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0xc

    const v3, -0x1000002

    aput v3, v1, v2

    const/16 v2, 0xd

    const v3, -0x1100001

    aput v3, v1, v2

    aput-object v1, v0, v8

    const/4 v1, 0x2

    .line 77
    const/16 v2, 0xe

    new-array v2, v2, [I

    const/16 v3, 0xff

    const/16 v4, 0xcd

    const/16 v5, 0xc1

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v7

    const/16 v3, 0xf9

    const/16 v4, 0xa4

    const/16 v5, 0x8f

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v8

    const/4 v3, 0x2

    const/16 v4, 0xf5

    const/16 v5, 0x7e

    const/16 v6, 0x60

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    .line 78
    const/16 v3, 0xff

    const/16 v4, 0x95

    const/16 v5, 0xa5

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v9

    const/16 v3, 0xf5

    const/16 v4, 0x4d

    const/16 v5, 0x67

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v10

    const/4 v3, 0x5

    const/16 v4, 0xed

    const/16 v5, 0x1a

    const/16 v6, 0x3b

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x6

    const/16 v4, 0xbc

    const/16 v5, 0x21

    invoke-static {v4, v10, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x7

    .line 79
    const/16 v4, 0x8b

    const/16 v5, 0x7c

    invoke-static {v4, v7, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    const/16 v3, 0x8

    const/16 v4, 0xbc

    const/16 v5, 0x5b

    invoke-static {v4, v10, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    const/16 v3, 0x9

    const/16 v4, 0x8c

    const/16 v5, 0x53

    const/16 v6, 0x7a

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    const/16 v3, 0x59

    const/16 v4, 0x42

    const/16 v5, 0x54

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v11

    const/16 v3, 0xb

    .line 80
    const/16 v4, 0x66

    const/16 v5, 0x30

    invoke-static {v4, v5, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    const/16 v3, 0xc

    const v4, -0x1000002

    aput v4, v2, v3

    const/16 v3, 0xd

    const v4, -0x1100001

    aput v4, v2, v3

    aput-object v2, v0, v1

    .line 82
    const/16 v1, 0xe

    new-array v1, v1, [I

    const/16 v2, 0xfc

    const/16 v3, 0xe3

    const/16 v4, 0x3c

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v7

    const/16 v2, 0xfa

    const/16 v3, 0xc7

    const/16 v4, 0x4b

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v8

    const/4 v2, 0x2

    const/16 v3, 0xcb

    const/16 v4, 0xe3

    const/16 v5, 0x6c

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    .line 83
    const/16 v2, 0x5b

    const/16 v3, 0xdd

    const/16 v4, 0x38

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v9

    const/16 v2, 0x41

    const/16 v3, 0xa5

    const/16 v4, 0x26

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v10

    const/4 v2, 0x5

    const/16 v3, 0xe

    const/16 v4, 0x80

    const/16 v5, 0x19

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x6

    const/16 v3, 0x66

    const/16 v4, 0x7f

    const/16 v5, 0x2a

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x7

    .line 84
    const/16 v3, 0xe

    const/16 v4, 0x80

    const/16 v5, 0x64

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x8

    const/16 v3, 0x13

    const/16 v4, 0x9b

    const/16 v5, 0x69

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x9

    const/4 v3, 0x5

    const/16 v4, 0x59

    const/16 v5, 0x75

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x11

    const/16 v3, 0x4b

    const/16 v4, 0x9d

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v11

    const/16 v2, 0xb

    .line 85
    const/16 v3, 0x29

    const/16 v4, 0x6c

    invoke-static {v11, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0xc

    const v3, -0x1000002

    aput v3, v1, v2

    const/16 v2, 0xd

    const v3, -0x1100001

    aput v3, v1, v2

    aput-object v1, v0, v9

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    .line 89
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    .line 90
    const-string v1, "White, Tap to apply"

    aput-object v1, v0, v7

    const-string v1, "Yellow, Tap to apply"

    aput-object v1, v0, v8

    const/4 v1, 0x2

    .line 91
    const-string v2, "Coral, Tap to apply"

    aput-object v2, v0, v1

    const-string v1, "Tomato, Tap to apply"

    aput-object v1, v0, v9

    const-string v1, "Hotpink, Tap to apply"

    aput-object v1, v0, v10

    const/4 v1, 0x5

    const-string v2, "Plum, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 92
    const-string v2, "Blueviolet, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Forestgreen, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Dodgeblue, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 93
    const-string v2, "Royalblue, Tap to apply"

    aput-object v2, v0, v1

    const-string v1, "Darkgray, Tap to apply"

    aput-object v1, v0, v11

    const/16 v1, 0xb

    const-string v2, "Black, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 94
    const-string v2, "Custom colour palette, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Spuit, Button"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    .line 97
    new-array v0, v10, [[Ljava/lang/String;

    .line 98
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "White, Tap to apply"

    aput-object v2, v1, v7

    const-string v2, "Yellow, Tap to apply"

    aput-object v2, v1, v8

    const/4 v2, 0x2

    .line 99
    const-string v3, "Coral, Tap to apply"

    aput-object v3, v1, v2

    const-string v2, "Tomato, Tap to apply"

    aput-object v2, v1, v9

    const-string v2, "Hotpink, Tap to apply"

    aput-object v2, v1, v10

    const/4 v2, 0x5

    const-string v3, "Plum, Tap to apply"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    .line 100
    const-string v3, "Blueviolet, Tap to apply"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "Forestgreen, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "Dodgeblue, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    .line 101
    const-string v3, "Royalblue, Tap to apply"

    aput-object v3, v1, v2

    const-string v2, "Darkgray, Tap to apply"

    aput-object v2, v1, v11

    const/16 v2, 0xb

    const-string v3, "Black, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    .line 102
    const-string v3, "Custom colour palette, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "Spuit, Button"

    aput-object v3, v1, v2

    aput-object v1, v0, v7

    .line 104
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Peachpuff, Tap to apply"

    aput-object v2, v1, v7

    const-string v2, "Lightsalmon, Tap to apply"

    aput-object v2, v1, v8

    const/4 v2, 0x2

    .line 105
    const-string v3, "Darksalmon, Tap to apply"

    aput-object v3, v1, v2

    const-string v2, "linghtpink, Tap to apply"

    aput-object v2, v1, v9

    const-string v2, "Palevioletred, Tap to apply"

    aput-object v2, v1, v10

    const/4 v2, 0x5

    .line 106
    const-string v3, "Crimson, Tap to apply"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "Red, Tap to apply"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "Mediumvioletred, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "Purple, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    .line 107
    const-string v3, "MediumOrchid, Tap to apply"

    aput-object v3, v1, v2

    const-string v2, "Dimgray, Tap to apply"

    aput-object v2, v1, v11

    const/16 v2, 0xb

    const-string v3, "Saddlebrown, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    .line 108
    const-string v3, "Custom colour palette, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "Spuit, Button"

    aput-object v3, v1, v2

    aput-object v1, v0, v8

    const/4 v1, 0x2

    .line 110
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Gold, Tap to apply"

    aput-object v3, v2, v7

    const-string v3, "Orange, Tap to apply"

    aput-object v3, v2, v8

    const/4 v3, 0x2

    .line 111
    const-string v4, "Greenyellow, Tap to apply"

    aput-object v4, v2, v3

    const-string v3, "Limegreen, Tap to apply"

    aput-object v3, v2, v9

    const-string v3, "Seegreen, Tap to apply"

    aput-object v3, v2, v10

    const/4 v3, 0x5

    const-string v4, "Green, Tap to apply"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    .line 112
    const-string v4, "OliverDrab, Tap to apply"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "Teal, Tap to apply"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "DarkCyan, Tap to apply"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "Steelblue, Tap to apply"

    aput-object v4, v2, v3

    .line 113
    const-string v3, "Blue, Tap to apply"

    aput-object v3, v2, v11

    const/16 v3, 0xb

    const-string v4, "Darkblue, Tap to apply"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "Custom colour palette, Tap to apply"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "Spuit, Button"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    .line 115
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "White, Tap to apply"

    aput-object v2, v1, v7

    const-string v2, "Yellow, Tap to apply"

    aput-object v2, v1, v8

    const/4 v2, 0x2

    .line 116
    const-string v3, "Coral, Tap to apply"

    aput-object v3, v1, v2

    const-string v2, "Tomato, Tap to apply"

    aput-object v2, v1, v9

    const-string v2, "Hotpink, Tap to apply"

    aput-object v2, v1, v10

    const/4 v2, 0x5

    const-string v3, "Plum, Tap to apply"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    .line 117
    const-string v3, "Blueviolet, Tap to apply"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "Forestgreen, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "Dodgeblue, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    .line 118
    const-string v3, "Royalblue, Tap to apply"

    aput-object v3, v1, v2

    const-string v2, "Darkgray, Tap to apply"

    aput-object v2, v1, v11

    const/16 v2, 0xb

    const-string v3, "Black, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    .line 119
    const-string v3, "Custom colour palette, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "Spuit, Button"

    aput-object v3, v1, v2

    aput-object v1, v0, v9

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTableSet:[[Ljava/lang/String;

    .line 158
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    .line 161
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mIsColorPickerEnable:Z

    .line 166
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCustom_imagepath:Ljava/lang/String;

    .line 464
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mFirstExecuted:Z

    .line 180
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 181
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCustom_imagepath:Ljava/lang/String;

    .line 182
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCustom_imagepath:Ljava/lang/String;

    invoke-direct {v0, p1, v1, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 183
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->initColorName()V

    .line 184
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->initView()V

    .line 185
    return-void
.end method

.method private drawSeleteBox(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v6, 0x0

    .line 557
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    if-eqz v4, :cond_2

    .line 558
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    rem-int/lit8 v2, v4, 0x7

    .line 559
    .local v2, "m":I
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    div-int/lit8 v3, v4, 0x7

    .line 560
    .local v3, "n":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    if-nez v4, :cond_0

    .line 561
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_GAPX:I

    mul-int/lit8 v5, v5, 0x6

    sub-int/2addr v4, v5

    div-int/lit8 v1, v4, 0x7

    .line 562
    .local v1, "itemW":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    if-eqz v4, :cond_3

    move v0, v1

    .line 563
    .local v0, "itemH":I
    :goto_0
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v6, v6, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    .line 565
    .end local v0    # "itemH":I
    .end local v1    # "itemW":I
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    if-nez v4, :cond_1

    .line 566
    new-instance v4, Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    sub-int/2addr v6, v7

    .line 567
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    sget v8, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    sget v9, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    add-int/2addr v8, v9

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 566
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    .line 569
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 570
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_GAPX:I

    add-int/2addr v5, v6

    mul-int/2addr v5, v2

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_GAPY:I

    add-int/2addr v6, v7

    mul-int/2addr v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->offset(II)V

    .line 572
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 573
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 576
    .end local v2    # "m":I
    .end local v3    # "n":I
    :cond_2
    return-void

    .line 562
    .restart local v1    # "itemW":I
    .restart local v2    # "m":I
    .restart local v3    # "n":I
    :cond_3
    add-int/lit8 v0, v1, -0x1

    goto :goto_0
.end method

.method private getColorIdx(FF)I
    .locals 9
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v8, 0x7

    const/4 v7, 0x2

    const/4 v4, 0x0

    .line 648
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    if-nez v5, :cond_0

    .line 673
    :goto_0
    return v4

    .line 652
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    if-nez v5, :cond_1

    .line 653
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_GAPX:I

    mul-int/lit8 v6, v6, 0x6

    sub-int/2addr v5, v6

    div-int/lit8 v1, v5, 0x7

    .line 654
    .local v1, "itemW":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    if-eqz v5, :cond_6

    move v0, v1

    .line 655
    .local v0, "itemH":I
    :goto_1
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v4, v4, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    .line 657
    .end local v0    # "itemH":I
    .end local v1    # "itemW":I
    :cond_1
    const/4 v2, 0x1

    .local v2, "m":I
    :goto_2
    if-le v2, v8, :cond_7

    .line 662
    :cond_2
    const/4 v3, 0x1

    .local v3, "n":I
    :goto_3
    if-le v3, v7, :cond_8

    .line 667
    :cond_3
    if-le v2, v8, :cond_4

    .line 668
    const/4 v2, 0x7

    .line 670
    :cond_4
    if-le v3, v7, :cond_5

    .line 671
    const/4 v3, 0x2

    .line 673
    :cond_5
    add-int/lit8 v4, v3, -0x1

    mul-int/lit8 v4, v4, 0x7

    add-int/lit8 v5, v2, -0x1

    add-int/2addr v4, v5

    goto :goto_0

    .line 654
    .end local v2    # "m":I
    .end local v3    # "n":I
    .restart local v1    # "itemW":I
    :cond_6
    add-int/lit8 v0, v1, -0x1

    goto :goto_1

    .line 658
    .end local v1    # "itemW":I
    .restart local v2    # "m":I
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    mul-int/2addr v4, v2

    add-int/lit8 v5, v2, -0x1

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_GAPX:I

    mul-int/2addr v5, v6

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v4, p1, v4

    if-lez v4, :cond_2

    .line 657
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 663
    .restart local v3    # "n":I
    :cond_8
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    mul-int/2addr v4, v3

    int-to-float v4, v4

    cmpg-float v4, p2, v4

    if-ltz v4, :cond_3

    .line 662
    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method private initColorName()V
    .locals 6

    .prologue
    .line 188
    const-string v2, "SPenColorPaletteView"

    const-string v3, "initColorName"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 194
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTableSet:[[Ljava/lang/String;

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 199
    return-void

    .line 191
    .end local v1    # "j":I
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableStringName:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v5, "string_color_tap_to_apply"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 195
    .restart local v1    # "j":I
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_2

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 196
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTableSet:[[Ljava/lang/String;

    aget-object v2, v2, v1

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSetStringName:[[Ljava/lang/String;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v5, "string_color_tap_to_apply"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 195
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private initView()V
    .locals 3

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/16 v2, 0x19

    .line 318
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_GAPX:I

    .line 319
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_GAPY:I

    .line 320
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    .line 323
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mBorderPaint:Landroid/graphics/Paint;

    .line 324
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mBorderPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 325
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mBorderPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 326
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mBorderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 328
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorPaint:Landroid/graphics/Paint;

    .line 329
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_colorchip_select_box"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    .line 330
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_colorchip_shadow"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    .line 331
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_colorbox_mini"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    .line 333
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mIsColorPickerEnable:Z

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_color_spoid_normal"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    .line 338
    :goto_0
    return-void

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_color_spoid_dim"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    goto :goto_0
.end method

.method private onDrawColorSet(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 510
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    if-eqz v6, :cond_1

    .line 511
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    if-nez v6, :cond_0

    .line 512
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_GAPX:I

    mul-int/lit8 v7, v7, 0x6

    sub-int/2addr v6, v7

    div-int/lit8 v3, v6, 0x7

    .line 513
    .local v3, "itemW":I
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    if-eqz v6, :cond_2

    move v2, v3

    .line 514
    .local v2, "itemH":I
    :goto_0
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v9, v9, v3, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    .line 516
    .end local v2    # "itemH":I
    .end local v3    # "itemW":I
    :cond_0
    new-instance v5, Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    invoke-direct {v5, v6}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 518
    .local v5, "localRect":Landroid/graphics/Rect;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v6, 0x2

    if-lt v1, v6, :cond_3

    .line 546
    .end local v1    # "i":I
    .end local v5    # "localRect":Landroid/graphics/Rect;
    :cond_1
    return-void

    .line 513
    .restart local v3    # "itemW":I
    :cond_2
    add-int/lit8 v2, v3, -0x1

    goto :goto_0

    .line 519
    .end local v3    # "itemW":I
    .restart local v1    # "i":I
    .restart local v5    # "localRect":Landroid/graphics/Rect;
    :cond_3
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_2
    const/4 v6, 0x7

    if-lt v4, v6, :cond_4

    .line 543
    iget v6, v5, Landroid/graphics/Rect;->left:I

    neg-int v6, v6

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v7

    sget v8, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_GAPY:I

    add-int/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Rect;->offset(II)V

    .line 518
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 520
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    mul-int/lit8 v7, v1, 0x7

    add-int/2addr v7, v4

    aget v0, v6, v7

    .line 521
    .local v0, "color":I
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorPaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 522
    const v6, -0x1000002

    if-ne v0, v6, :cond_6

    .line 523
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->isRainbow:Z

    if-eqz v6, :cond_5

    .line 524
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6, v5}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 525
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 541
    :goto_3
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v6

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_GAPX:I

    add-int/2addr v6, v7

    invoke-virtual {v5, v6, v9}, Landroid/graphics/Rect;->offset(II)V

    .line 519
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 527
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 528
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6, v5}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 529
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_3

    .line 533
    :cond_6
    const v6, -0x1100001

    if-ne v0, v6, :cond_7

    .line 534
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6, v5}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 535
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_3

    .line 537
    :cond_7
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 538
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6, v5}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 539
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_3
.end method


# virtual methods
.method protected close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 244
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    .line 245
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    .line 246
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    .line 247
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mBorderPaint:Landroid/graphics/Paint;

    .line 248
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorPaint:Landroid/graphics/Paint;

    .line 249
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    .line 250
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCustom_imagepath:Ljava/lang/String;

    .line 251
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 253
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    .line 254
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 255
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 258
    :cond_0
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    .line 260
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_3

    .line 261
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 262
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 263
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 265
    :cond_2
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    .line 267
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_5

    .line 268
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 269
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_4

    .line 270
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 272
    :cond_4
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    .line 275
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_7

    .line 276
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 277
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_6

    .line 278
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 280
    :cond_6
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    .line 282
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_7
    return-void
.end method

.method public getColor()I
    .locals 2

    .prologue
    .line 728
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    aget v0, v0, v1

    return v0
.end method

.method protected getCurrentTableIndex()I
    .locals 1

    .prologue
    .line 312
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 475
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 476
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingLeft()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingTop()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    int-to-float v6, v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 478
    iget-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mFirstExecuted:Z

    if-eqz v5, :cond_1

    .line 479
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 480
    .local v1, "localLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getWidth()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    div-int/lit8 v2, v5, 0x7

    .line 481
    .local v2, "m":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    array-length v5, v5

    int-to-float v5, v5

    const/high16 v6, 0x40e00000    # 7.0f

    div-float v4, v5, v6

    .line 482
    .local v4, "rowNumF":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    array-length v5, v5

    div-int/lit8 v3, v5, 0x7

    .line 484
    .local v3, "rowNum":I
    int-to-float v5, v3

    cmpl-float v5, v4, v5

    if-lez v5, :cond_0

    .line 485
    add-int/lit8 v3, v3, 0x1

    .line 487
    :cond_0
    mul-int v5, v2, v3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingBottom()I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 488
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 489
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mFirstExecuted:Z

    .line 493
    .end local v1    # "localLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v2    # "m":I
    .end local v3    # "rowNum":I
    .end local v4    # "rowNumF":F
    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->onDrawColorSet(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    :goto_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->drawSeleteBox(Landroid/graphics/Canvas;)V

    .line 498
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 499
    return-void

    .line 494
    :catch_0
    move-exception v0

    .line 495
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 709
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 710
    if-eqz p1, :cond_0

    .line 711
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 712
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 718
    :goto_0
    return-void

    .line 714
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 715
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v5, 0xe

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 401
    const-string v1, "SpenColorPaletteView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "keycode = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    sparse-switch p1, :sswitch_data_0

    .line 461
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 405
    :sswitch_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x7

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x7

    if-ge v1, v5, :cond_0

    .line 408
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x7

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 409
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    .line 410
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->playSoundEffect(I)V

    .line 412
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    goto :goto_0

    .line 416
    :sswitch_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x7

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x7

    if-ge v1, v5, :cond_0

    .line 419
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x7

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 420
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    .line 421
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->playSoundEffect(I)V

    .line 423
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    goto :goto_0

    .line 427
    :sswitch_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x1

    if-ge v1, v5, :cond_0

    .line 430
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 431
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    .line 432
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->playSoundEffect(I)V

    .line 434
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    goto :goto_0

    .line 438
    :sswitch_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x1

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x1

    if-ge v1, v5, :cond_0

    .line 441
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 442
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    .line 443
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->playSoundEffect(I)V

    .line 445
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    goto :goto_0

    .line 450
    :sswitch_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    if-eqz v1, :cond_1

    .line 451
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    aget v2, v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;->colorChanged(II)V

    .line 454
    :cond_1
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->playSoundEffect(I)V

    goto/16 :goto_0

    .line 403
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x17 -> :sswitch_4
        0x42 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onSizeChanged(IIII)V
    .locals 12
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 368
    invoke-super/range {p0 .. p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 370
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingLeft()I

    move-result v3

    .line 371
    .local v3, "mPaddingLeft":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingRight()I

    move-result v4

    .line 372
    .local v4, "mPaddingRight":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingTop()I

    move-result v5

    .line 373
    .local v5, "mPaddingTop":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingBottom()I

    move-result v2

    .line 375
    .local v2, "mPaddingBottom":I
    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getLeft()I

    move-result v7

    add-int/2addr v7, v3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getTop()I

    move-result v8

    add-int/2addr v8, v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getRight()I

    move-result v9

    sub-int/2addr v9, v4

    .line 376
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getBottom()I

    move-result v10

    sub-int/2addr v10, v2

    invoke-direct {v6, v7, v8, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 375
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    .line 379
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_GAPX:I

    mul-int/lit8 v7, v7, 0x6

    sub-int/2addr v6, v7

    div-int/lit8 v1, v6, 0x7

    .line 380
    .local v1, "itemW":I
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_0

    move v0, v1

    .line 381
    .local v0, "itemH":I
    :goto_0
    new-instance v6, Landroid/graphics/Rect;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    .line 384
    new-instance v6, Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    sget v8, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    sget v9, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    .line 385
    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    add-int/2addr v9, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    sget v11, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    add-int/2addr v10, v11

    invoke-direct {v6, v7, v8, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 384
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    .line 386
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 387
    return-void

    .line 380
    .end local v0    # "itemH":I
    :cond_0
    add-int/lit8 v0, v1, -0x1

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v4, 0xd

    const/4 v3, 0x0

    .line 588
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 594
    .local v0, "colorIdx":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 610
    :cond_0
    :goto_0
    :pswitch_0
    const/16 v1, 0xc

    if-ne v0, v1, :cond_4

    .line 611
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    .line 612
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 613
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->isRainbow:Z

    if-eqz v1, :cond_1

    .line 614
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->isRainbow:Z

    .line 626
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 627
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 633
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 635
    const/4 v1, 0x1

    return v1

    .line 599
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getColorIdx(FF)I

    move-result v0

    .line 600
    if-lt v0, v4, :cond_3

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mIsColorPickerEnable:Z

    if-eqz v1, :cond_0

    .line 601
    :cond_3
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->playSoundEffect(I)V

    goto :goto_0

    .line 617
    :cond_4
    if-ne v0, v4, :cond_5

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mIsColorPickerEnable:Z

    if-eqz v1, :cond_1

    .line 618
    :cond_5
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 619
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    goto :goto_1

    .line 629
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    aget v2, v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;->colorChanged(II)V

    goto :goto_2

    .line 594
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 627
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method protected setBackColorTable(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 300
    add-int/lit8 p1, p1, -0x1

    .line 301
    if-nez p1, :cond_0

    .line 302
    const/4 p1, 0x4

    .line 304
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    .line 305
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 306
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    .line 308
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 309
    return-void
.end method

.method public setColor(I)V
    .locals 8
    .param p1, "color"    # I

    .prologue
    const/4 v7, 0x0

    const/high16 v3, -0x1000000

    const/4 v6, 0x1

    const/16 v5, 0xc

    .line 742
    or-int/2addr p1, v3

    .line 744
    and-int/2addr v3, p1

    const/high16 v4, -0x2000000

    if-eq v3, v4, :cond_7

    .line 745
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    array-length v3, v3

    add-int/lit8 v2, v3, -0x1

    .local v2, "m":I
    :goto_0
    if-gez v2, :cond_1

    .line 752
    :goto_1
    if-gez v2, :cond_0

    .line 753
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    array-length v3, v3

    add-int/lit8 v0, v3, -0x2

    .local v0, "i":I
    :goto_2
    if-gez v0, :cond_3

    .line 760
    :goto_3
    if-gez v0, :cond_0

    .line 761
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 762
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    aput p1, v3, v5

    .line 763
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    array-length v3, v3

    if-lt v1, v3, :cond_6

    .line 766
    sput-boolean v6, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->IS_COLOR_GRADATION_SELECT:Z

    .line 778
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "m":I
    :cond_0
    :goto_5
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 779
    return-void

    .line 746
    .restart local v2    # "m":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    aget v3, v3, v2

    if-ne p1, v3, :cond_2

    .line 747
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 748
    sput-boolean v7, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->IS_COLOR_GRADATION_SELECT:Z

    goto :goto_1

    .line 745
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 754
    .restart local v0    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v3, v3, v7

    aget v3, v3, v0

    if-eq p1, v3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v3, v3, v6

    aget v3, v3, v0

    if-eq p1, v3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    const/4 v4, 0x2

    aget-object v3, v3, v4

    aget v3, v3, v0

    if-eq p1, v3, :cond_4

    .line 755
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    const/4 v4, 0x3

    aget-object v3, v3, v4

    aget v3, v3, v0

    if-ne p1, v3, :cond_5

    .line 756
    :cond_4
    const/4 v3, -0x1

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    goto :goto_3

    .line 753
    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 764
    .restart local v1    # "j":I
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v3, v3, v1

    aput p1, v3, v5

    .line 763
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 770
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "m":I
    :cond_7
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 771
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    aput p1, v3, v5

    .line 772
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    array-length v3, v3

    if-lt v1, v3, :cond_8

    .line 775
    sput-boolean v6, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->IS_COLOR_GRADATION_SELECT:Z

    goto :goto_5

    .line 773
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v3, v3, v1

    aput p1, v3, v5

    .line 772
    add-int/lit8 v1, v1, 0x1

    goto :goto_6
.end method

.method public setColorPickerColor(I)V
    .locals 4
    .param p1, "color"    # I

    .prologue
    const/high16 v3, -0x1000000

    const/16 v2, 0xc

    .line 789
    and-int v1, p1, v3

    if-nez v1, :cond_0

    .line 790
    or-int/2addr p1, v3

    .line 792
    :cond_0
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 793
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    aput p1, v1, v2

    .line 794
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 797
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 798
    return-void

    .line 795
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v1, v1, v0

    aput p1, v1, v2

    .line 794
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected setColorPickerEnable(Z)V
    .locals 3
    .param p1, "isEnable"    # Z

    .prologue
    const/16 v2, 0x19

    .line 231
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mIsColorPickerEnable:Z

    .line 233
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    if-eqz v0, :cond_0

    .line 234
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mIsColorPickerEnable:Z

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_color_spoid_normal"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    .line 239
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 241
    :cond_0
    return-void

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_color_spoid_dim"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    goto :goto_0
.end method

.method public setColorPickerMode()V
    .locals 1

    .prologue
    .line 802
    const/16 v0, 0xc

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 803
    return-void
.end method

.method public setInitialValue(Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;I)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;
    .param p2, "color"    # I

    .prologue
    .line 349
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    .line 350
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setSelectBoxPos(I)V

    .line 351
    return-void
.end method

.method protected setMontblancColorPalette(Z)V
    .locals 8
    .param p1, "isEnabled"    # Z

    .prologue
    const/16 v7, 0xe

    const/4 v6, 0x3

    const/4 v1, 0x1

    const/16 v5, 0xb

    const/4 v4, 0x0

    .line 202
    if-eqz p1, :cond_1

    .line 203
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    if-ne v0, v1, :cond_0

    .line 204
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    const/16 v1, 0x1d

    const/16 v2, 0x2a

    const/16 v3, 0x44

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v4

    .line 205
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    const/16 v1, 0x63

    const/16 v2, 0x2e

    const/16 v3, 0x47

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v5

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v0, v0, v4

    const/16 v1, 0x1d

    const/16 v2, 0x2a

    const/16 v3, 0x44

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v4

    .line 209
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v0, v0, v4

    const/16 v1, 0x63

    const/16 v2, 0x2e

    const/16 v3, 0x47

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v5

    .line 227
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 228
    return-void

    .line 211
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    if-ne v0, v1, :cond_2

    .line 212
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    const/16 v1, 0x38

    const/16 v2, 0xd0

    invoke-static {v6, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v4

    .line 213
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    const/16 v1, 0x7e

    const/16 v2, 0x41

    invoke-static {v1, v7, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v5

    .line 214
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_color_royal_blue"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 215
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_color_tap_to_apply"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 214
    aput-object v1, v0, v4

    .line 216
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_color_burgundy"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 217
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_color_tap_to_apply"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 216
    aput-object v1, v0, v5

    .line 219
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v0, v0, v4

    const/16 v1, 0x38

    const/16 v2, 0xd0

    invoke-static {v6, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v4

    .line 220
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v0, v0, v4

    const/16 v1, 0x7e

    const/16 v2, 0x41

    invoke-static {v1, v7, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v5

    .line 222
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTableSet:[[Ljava/lang/String;

    aget-object v0, v0, v4

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_color_royal_blue"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 223
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_color_tap_to_apply"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 222
    aput-object v1, v0, v4

    .line 224
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTableSet:[[Ljava/lang/String;

    aget-object v0, v0, v4

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_color_burgundy"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 225
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_color_tap_to_apply"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 224
    aput-object v1, v0, v5

    goto/16 :goto_0
.end method

.method protected setNextColorTable(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x5

    .line 286
    add-int/lit8 p1, p1, 0x1

    .line 287
    if-ne p1, v1, :cond_0

    .line 288
    const/4 p1, 0x1

    .line 290
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    .line 291
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    if-ge v0, v1, :cond_1

    .line 292
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    .line 294
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 296
    const/16 v0, 0xc2

    const/16 v1, 0x37

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getHeight()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->onSizeChanged(IIII)V

    .line 297
    return-void
.end method

.method public setSelectBoxPos(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    const/high16 v1, -0x1000000

    .line 685
    if-ne p1, v1, :cond_0

    .line 686
    const/high16 p1, -0x1000000

    .line 690
    :cond_0
    or-int/2addr p1, v1

    .line 691
    const/4 v0, 0x0

    .line 692
    .local v0, "m":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-gez v0, :cond_1

    .line 698
    :goto_1
    if-gez v0, :cond_3

    .line 699
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 704
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 705
    return-void

    .line 693
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    aget v1, v1, v0

    if-ne p1, v1, :cond_2

    .line 694
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    goto :goto_1

    .line 692
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 701
    :cond_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    goto :goto_2
.end method
