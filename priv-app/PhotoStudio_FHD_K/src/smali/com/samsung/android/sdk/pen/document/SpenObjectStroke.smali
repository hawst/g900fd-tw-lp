.class public final Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
.super Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.source "SpenObjectStroke.java"


# static fields
.field public static final TOOL_TYPE_ERASER:I = 0x4

.field public static final TOOL_TYPE_FINGER:I = 0x1

.field public static final TOOL_TYPE_MOUSE:I = 0x3

.field public static final TOOL_TYPE_SPEN:I = 0x2

.field public static final TOOL_TYPE_UNKNOWN:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 86
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 87
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init1(Ljava/lang/String;)Z

    move-result v0

    .line 88
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 89
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 91
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 7
    .param p1, "penName"    # Ljava/lang/String;
    .param p2, "isTemplateObject"    # Z

    .prologue
    const/4 v2, 0x0

    .line 106
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move v5, p2

    .line 107
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init4(Ljava/lang/String;[Landroid/graphics/PointF;[F[IZ)Z

    move-result v6

    .line 108
    .local v6, "rnt":Z
    if-nez v6, :cond_0

    .line 109
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 111
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Landroid/graphics/PointF;[F[I)V
    .locals 2
    .param p1, "penName"    # Ljava/lang/String;
    .param p2, "points"    # [Landroid/graphics/PointF;
    .param p3, "pressures"    # [F
    .param p4, "timestamps"    # [I

    .prologue
    .line 134
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 135
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init3(Ljava/lang/String;[Landroid/graphics/PointF;[F[I)Z

    move-result v0

    .line 136
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 137
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 139
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Landroid/graphics/PointF;[F[IZ)V
    .locals 2
    .param p1, "penName"    # Ljava/lang/String;
    .param p2, "points"    # [Landroid/graphics/PointF;
    .param p3, "pressures"    # [F
    .param p4, "timestamps"    # [I
    .param p5, "isTemplateObject"    # Z

    .prologue
    .line 166
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 167
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init4(Ljava/lang/String;[Landroid/graphics/PointF;[F[IZ)Z

    move-result v0

    .line 168
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 169
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 171
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Landroid/graphics/PointF;[F[I[F[F)V
    .locals 4
    .param p1, "penName"    # Ljava/lang/String;
    .param p2, "points"    # [Landroid/graphics/PointF;
    .param p3, "pressures"    # [F
    .param p4, "timestamps"    # [I
    .param p5, "tilts"    # [F
    .param p6, "orientations"    # [F

    .prologue
    .line 205
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 207
    :try_start_0
    invoke-direct/range {p0 .. p6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init5(Ljava/lang/String;[Landroid/graphics/PointF;[F[I[F[F)Z

    move-result v1

    .line 208
    .local v1, "rnt":Z
    if-nez v1, :cond_0

    .line 209
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 211
    .end local v1    # "rnt":Z
    :catch_0
    move-exception v0

    .line 212
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    const-string v2, "SpenObjectStroke"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init3(Ljava/lang/String;[Landroid/graphics/PointF;[F[I)Z

    move-result v1

    .line 214
    .restart local v1    # "rnt":Z
    if-nez v1, :cond_0

    .line 215
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;[Landroid/graphics/PointF;[F[I[F[FZ)V
    .locals 8
    .param p1, "penName"    # Ljava/lang/String;
    .param p2, "points"    # [Landroid/graphics/PointF;
    .param p3, "pressures"    # [F
    .param p4, "timestamps"    # [I
    .param p5, "tilts"    # [F
    .param p6, "orientations"    # [F
    .param p7, "isTemplateObject"    # Z

    .prologue
    .line 255
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 256
    array-length v0, p2

    array-length v1, p3

    if-ne v0, v1, :cond_0

    array-length v0, p2

    array-length v1, p4

    if-ne v0, v1, :cond_0

    .line 257
    array-length v0, p2

    array-length v1, p5

    if-ne v0, v1, :cond_0

    array-length v0, p2

    array-length v1, p6

    if-eq v0, v1, :cond_1

    .line 258
    :cond_0
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 261
    :cond_1
    :try_start_0
    invoke-direct/range {p0 .. p7}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init6(Ljava/lang/String;[Landroid/graphics/PointF;[F[I[F[FZ)Z

    move-result v7

    .line 263
    .local v7, "rnt":Z
    if-nez v7, :cond_2

    .line 264
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    :cond_2
    :goto_0
    return-void

    .line 266
    .end local v7    # "rnt":Z
    :catch_0
    move-exception v6

    .line 267
    .local v6, "e":Ljava/lang/UnsatisfiedLinkError;
    const-string v0, "SpenObjectStroke"

    const-string v1, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p7

    .line 268
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init4(Ljava/lang/String;[Landroid/graphics/PointF;[F[IZ)Z

    move-result v7

    .line 269
    .restart local v7    # "rnt":Z
    if-nez v7, :cond_2

    .line 270
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    goto :goto_0
.end method

.method public constructor <init>(Z)V
    .locals 7
    .param p1, "isTemplateObject"    # Z

    .prologue
    const/4 v1, 0x0

    .line 68
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v5, p1

    .line 69
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init4(Ljava/lang/String;[Landroid/graphics/PointF;[F[IZ)Z

    move-result v6

    .line 70
    .local v6, "rnt":Z
    if-nez v6, :cond_0

    .line 71
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 73
    :cond_0
    return-void
.end method

.method private native ObjectStroke_addPoint4(Landroid/graphics/PointF;FI)Z
.end method

.method private native ObjectStroke_addPoint5(Landroid/graphics/PointF;FIFF)Z
.end method

.method private native ObjectStroke_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native ObjectStroke_enableCurve(Z)Z
.end method

.method private native ObjectStroke_getAdvancedPenSetting()Ljava/lang/String;
.end method

.method private native ObjectStroke_getColor()I
.end method

.method private native ObjectStroke_getDefaultPenName()Ljava/lang/String;
.end method

.method private native ObjectStroke_getInputType()I
.end method

.method private native ObjectStroke_getOrientations()[F
.end method

.method private native ObjectStroke_getPenName()Ljava/lang/String;
.end method

.method private native ObjectStroke_getPenSize()F
.end method

.method private native ObjectStroke_getPoints()[Landroid/graphics/PointF;
.end method

.method private native ObjectStroke_getPressures()[F
.end method

.method private native ObjectStroke_getTilts()[F
.end method

.method private native ObjectStroke_getTimeStamps()[I
.end method

.method private native ObjectStroke_getToolType()I
.end method

.method private native ObjectStroke_getXPoints()[F
.end method

.method private native ObjectStroke_getYPoints()[F
.end method

.method private native ObjectStroke_init1(Ljava/lang/String;)Z
.end method

.method private native ObjectStroke_init3(Ljava/lang/String;[Landroid/graphics/PointF;[F[I)Z
.end method

.method private native ObjectStroke_init4(Ljava/lang/String;[Landroid/graphics/PointF;[F[IZ)Z
.end method

.method private native ObjectStroke_init5(Ljava/lang/String;[Landroid/graphics/PointF;[F[I[F[F)Z
.end method

.method private native ObjectStroke_init6(Ljava/lang/String;[Landroid/graphics/PointF;[F[I[F[FZ)Z
.end method

.method private native ObjectStroke_isCurvable()Z
.end method

.method private native ObjectStroke_move(FF)Z
.end method

.method private native ObjectStroke_resize(FF)Z
.end method

.method private native ObjectStroke_setAdvancedPenSetting(Ljava/lang/String;)Z
.end method

.method private native ObjectStroke_setColor(I)Z
.end method

.method private native ObjectStroke_setDefaultPenName(Ljava/lang/String;)Z
.end method

.method private native ObjectStroke_setInputType(I)Z
.end method

.method private native ObjectStroke_setPenName(Ljava/lang/String;)Z
.end method

.method private native ObjectStroke_setPenSize(F)Z
.end method

.method private native ObjectStroke_setPoints([Landroid/graphics/PointF;[F[I)Z
.end method

.method private native ObjectStroke_setPoints2([Landroid/graphics/PointF;[F[I[F[F)Z
.end method

.method private native ObjectStroke_setRotation(F)Z
.end method

.method private native ObjectStroke_setToolType(I)Z
.end method

.method private throwUncheckedException(I)V
    .locals 3
    .param p1, "errno"    # I

    .prologue
    .line 829
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    .line 830
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenObjectStroke("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 832
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 834
    return-void
.end method


# virtual methods
.method public addPoint(Landroid/graphics/PointF;FI)V
    .locals 2
    .param p1, "pos"    # Landroid/graphics/PointF;
    .param p2, "pressure"    # F
    .param p3, "timestamp"    # I

    .prologue
    .line 627
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_addPoint4(Landroid/graphics/PointF;FI)Z

    move-result v0

    .line 628
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 629
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 631
    :cond_0
    return-void
.end method

.method public addPoint(Landroid/graphics/PointF;FIFF)V
    .locals 4
    .param p1, "pos"    # Landroid/graphics/PointF;
    .param p2, "pressure"    # F
    .param p3, "timestamp"    # I
    .param p4, "tilt"    # F
    .param p5, "orientation"    # F

    .prologue
    .line 653
    :try_start_0
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_addPoint5(Landroid/graphics/PointF;FIFF)Z

    move-result v1

    .line 654
    .local v1, "rnt":Z
    if-nez v1, :cond_0

    .line 655
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 664
    :cond_0
    :goto_0
    return-void

    .line 657
    .end local v1    # "rnt":Z
    :catch_0
    move-exception v0

    .line 658
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    const-string v2, "SpenObjectStroke"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_addPoint4(Landroid/graphics/PointF;FI)Z

    move-result v1

    .line 660
    .restart local v1    # "rnt":Z
    if-nez v1, :cond_0

    .line 661
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    goto :goto_0
.end method

.method public copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2
    .param p1, "source"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 822
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    .line 823
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 824
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 826
    :cond_0
    return-void
.end method

.method public getAdvancedPenSetting()Ljava/lang/String;
    .locals 1

    .prologue
    .line 378
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getAdvancedPenSetting()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 699
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getColor()I

    move-result v0

    return v0
.end method

.method public getDefaultPenName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 343
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getDefaultPenName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 347
    :goto_0
    return-object v1

    .line 344
    :catch_0
    move-exception v0

    .line 345
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 346
    const-string v1, "SpenObjectStroke"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getOrientations()[F
    .locals 3

    .prologue
    .line 604
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getOrientations()[F
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 608
    :goto_0
    return-object v1

    .line 605
    :catch_0
    move-exception v0

    .line 606
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 607
    const-string v1, "SpenObjectStroke"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getPenName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPenSize()F
    .locals 1

    .prologue
    .line 730
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getPenSize()F

    move-result v0

    return v0
.end method

.method public getPoints()[Landroid/graphics/PointF;
    .locals 9

    .prologue
    .line 489
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getXPoints()[F

    move-result-object v4

    .line 490
    .local v4, "xPoints":[F
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getYPoints()[F

    move-result-object v5

    .line 491
    .local v5, "yPoints":[F
    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    array-length v6, v4

    if-eqz v6, :cond_0

    array-length v6, v5

    if-nez v6, :cond_2

    .line 492
    :cond_0
    const/4 v3, 0x0

    .line 504
    .end local v4    # "xPoints":[F
    .end local v5    # "yPoints":[F
    :cond_1
    :goto_0
    return-object v3

    .line 494
    .restart local v4    # "xPoints":[F
    .restart local v5    # "yPoints":[F
    :cond_2
    array-length v2, v4

    .line 495
    .local v2, "pointCount":I
    new-array v3, v2, [Landroid/graphics/PointF;

    .line 497
    .local v3, "points":[Landroid/graphics/PointF;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 498
    new-instance v6, Landroid/graphics/PointF;

    aget v7, v4, v1

    aget v8, v5, v1

    invoke-direct {v6, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v6, v3, v1
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 501
    .end local v1    # "i":I
    .end local v2    # "pointCount":I
    .end local v3    # "points":[Landroid/graphics/PointF;
    .end local v4    # "xPoints":[F
    .end local v5    # "yPoints":[F
    :catch_0
    move-exception v0

    .line 502
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 503
    const-string v6, "SpenObjectStroke"

    const-string v7, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getPoints()[Landroid/graphics/PointF;

    move-result-object v3

    goto :goto_0
.end method

.method public getPressures()[F
    .locals 1

    .prologue
    .line 558
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getPressures()[F

    move-result-object v0

    return-object v0
.end method

.method public getTilts()[F
    .locals 3

    .prologue
    .line 585
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getTilts()[F
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 589
    :goto_0
    return-object v1

    .line 586
    :catch_0
    move-exception v0

    .line 587
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 588
    const-string v1, "SpenObjectStroke"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTimeStamps()[I
    .locals 1

    .prologue
    .line 571
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getTimeStamps()[I

    move-result-object v0

    return-object v0
.end method

.method public getToolType()I
    .locals 1

    .prologue
    .line 803
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getToolType()I

    move-result v0

    return v0
.end method

.method public getXPoints()[F
    .locals 3

    .prologue
    .line 520
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getXPoints()[F
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 524
    :goto_0
    return-object v1

    .line 521
    :catch_0
    move-exception v0

    .line 522
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 523
    const-string v1, "SpenObjectStroke"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getYPoints()[F
    .locals 3

    .prologue
    .line 540
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getYPoints()[F
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 544
    :goto_0
    return-object v1

    .line 541
    :catch_0
    move-exception v0

    .line 542
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 543
    const-string v1, "SpenObjectStroke"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCurveEnabled()Z
    .locals 1

    .prologue
    .line 761
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_isCurvable()Z

    move-result v0

    return v0
.end method

.method public setAdvancedPenSetting(Ljava/lang/String;)V
    .locals 2
    .param p1, "penStyle"    # Ljava/lang/String;

    .prologue
    .line 362
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setAdvancedPenSetting(Ljava/lang/String;)Z

    move-result v0

    .line 363
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 364
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 366
    :cond_0
    return-void
.end method

.method public setColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 684
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setColor(I)Z

    move-result v0

    .line 685
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 686
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 688
    :cond_0
    return-void
.end method

.method public setCurveEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 745
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_enableCurve(Z)Z

    move-result v0

    .line 746
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 747
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 749
    :cond_0
    return-void
.end method

.method public setDefaultPenName(Ljava/lang/String;)V
    .locals 4
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 322
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setDefaultPenName(Ljava/lang/String;)Z

    move-result v1

    .line 323
    .local v1, "rnt":Z
    if-nez v1, :cond_0

    .line 324
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    .end local v1    # "rnt":Z
    :cond_0
    :goto_0
    return-void

    .line 326
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 328
    const-string v2, "SpenObjectStroke"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setPenName(Ljava/lang/String;)V
    .locals 2
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 291
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setPenName(Ljava/lang/String;)Z

    move-result v0

    .line 292
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 293
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 295
    :cond_0
    return-void
.end method

.method public setPenSize(F)V
    .locals 2
    .param p1, "size"    # F

    .prologue
    .line 715
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setPenSize(F)Z

    move-result v0

    .line 716
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 717
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 719
    :cond_0
    return-void
.end method

.method public setPoints([Landroid/graphics/PointF;[F[I)V
    .locals 6
    .param p1, "points"    # [Landroid/graphics/PointF;
    .param p2, "pressures"    # [F
    .param p3, "timestamps"    # [I

    .prologue
    const/4 v4, 0x0

    .line 404
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPoints([Landroid/graphics/PointF;[F[I[F[F)V

    .line 405
    return-void
.end method

.method public setPoints([Landroid/graphics/PointF;[F[I[F[F)V
    .locals 5
    .param p1, "points"    # [Landroid/graphics/PointF;
    .param p2, "pressures"    # [F
    .param p3, "timestamps"    # [I
    .param p4, "tilts"    # [F
    .param p5, "orientations"    # [F

    .prologue
    const/4 v4, 0x7

    .line 439
    const/4 v1, 0x0

    .line 440
    .local v1, "rnt":Z
    if-nez p1, :cond_1

    .line 441
    :try_start_0
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setPoints2([Landroid/graphics/PointF;[F[I[F[F)Z

    move-result v1

    .line 456
    :goto_0
    if-nez v1, :cond_0

    .line 457
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 477
    :cond_0
    :goto_1
    return-void

    .line 443
    :cond_1
    array-length v2, p1

    array-length v3, p2

    if-ne v2, v3, :cond_2

    array-length v2, p1

    array-length v3, p3

    if-eq v2, v3, :cond_3

    .line 444
    :cond_2
    const/4 v2, 0x7

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 447
    :cond_3
    if-nez p4, :cond_4

    if-nez p5, :cond_6

    .line 448
    :cond_4
    if-eqz p4, :cond_5

    if-eqz p5, :cond_6

    .line 449
    :cond_5
    if-eqz p4, :cond_7

    if-eqz p5, :cond_7

    array-length v2, p1

    array-length v3, p4

    if-ne v2, v3, :cond_6

    array-length v2, p1

    array-length v3, p5

    if-eq v2, v3, :cond_7

    .line 450
    :cond_6
    const/4 v2, 0x7

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 453
    :cond_7
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setPoints2([Landroid/graphics/PointF;[F[I[F[F)Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 459
    :catch_0
    move-exception v0

    .line 460
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    const-string v2, "SpenObjectStroke"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    const/4 v1, 0x0

    .line 462
    if-nez p1, :cond_8

    .line 463
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setPoints([Landroid/graphics/PointF;[F[I)Z

    move-result v1

    .line 472
    :goto_2
    if-nez v1, :cond_0

    .line 473
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    goto :goto_1

    .line 465
    :cond_8
    array-length v2, p1

    array-length v3, p2

    if-ne v2, v3, :cond_9

    array-length v2, p1

    array-length v3, p3

    if-eq v2, v3, :cond_a

    .line 466
    :cond_9
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 469
    :cond_a
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setPoints([Landroid/graphics/PointF;[F[I)Z

    move-result v1

    goto :goto_2
.end method

.method public setRect(Landroid/graphics/RectF;Z)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "regionOnly"    # Z

    .prologue
    .line 671
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 672
    return-void
.end method

.method public setRotation(F)V
    .locals 2
    .param p1, "degree"    # F

    .prologue
    .line 811
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setRotation(F)Z

    move-result v0

    .line 812
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 813
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 815
    :cond_0
    return-void
.end method

.method public setToolType(I)V
    .locals 1
    .param p1, "toolType"    # I

    .prologue
    .line 783
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setToolType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 784
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    .line 786
    :cond_0
    return-void
.end method
