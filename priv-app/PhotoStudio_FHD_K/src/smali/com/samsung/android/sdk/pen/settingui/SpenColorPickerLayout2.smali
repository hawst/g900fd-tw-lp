.class Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;
.super Landroid/view/View;
.source "SpenColorPickerLayout2.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "settingui-colorPicker"


# instance fields
.field private drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field mColorPickerColorImage:Landroid/view/View;

.field mColorPickerCurrentColor:Landroid/view/View;

.field mColorPickerHandle:Landroid/view/View;

.field mColorPickerdExitBtn:Landroid/view/View;

.field private mCurrentColor:I

.field private final mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field private mIsRotated:Z

.field mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private final mOldParentRect:Landroid/graphics/Rect;

.field private mParentRelativeLayout:Landroid/widget/RelativeLayout;

.field mSpoidExitListener:Landroid/view/View$OnTouchListener;

.field mSpoidSettingListener:Landroid/view/View$OnTouchListener;

.field mSpuitSettings:Landroid/view/View;

.field mSpuitdBG:Landroid/view/View;

.field private final mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field private mXDelta:I

.field private mYDelta:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;FII)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "canvasLayout"    # Landroid/widget/RelativeLayout;
    .param p3, "ratio"    # F
    .param p4, "x"    # I
    .param p5, "y"    # I

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    .line 34
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mCurrentColor:I

    .line 39
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mIsRotated:Z

    .line 572
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 601
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpoidSettingListener:Landroid/view/View$OnTouchListener;

    .line 683
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpoidExitListener:Landroid/view/View$OnTouchListener;

    .line 51
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 52
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, ""

    invoke-direct {v0, p1, v1, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 53
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 54
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    .line 55
    invoke-direct {p0, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->initSpuitSetting(II)V

    .line 56
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mOldParentRect:Landroid/graphics/Rect;

    .line 57
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mIsRotated:Z

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;Z)V
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mIsRotated:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)V
    .locals 0

    .prologue
    .line 421
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->checkPosition()V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;I)V
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mXDelta:I

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;I)V
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mYDelta:I

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mXDelta:I

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mYDelta:I

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    return-object v0
.end method

.method private bodyLayout()Landroid/view/ViewGroup;
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 275
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 277
    .local v0, "bodyLayout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 279
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->spuitdBg()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    .line 280
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->spuitdHandle()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    .line 281
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->spuitExitBtn()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    .line 282
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->spuitColorImage()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    .line 283
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->spuitCurrentColor()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    .line 284
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 285
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 286
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 287
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 288
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 289
    return-object v0
.end method

.method private checkPosition()V
    .locals 14

    .prologue
    .line 422
    const/4 v11, 0x2

    new-array v1, v11, [I

    .line 424
    .local v1, "location":[I
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v12, 0x42d40000    # 106.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 425
    .local v3, "minWidth":I
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v12, 0x42180000    # 38.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 427
    .local v2, "minHeight":I
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v11, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 429
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v4

    .line 431
    .local v4, "parentRect":Landroid/graphics/Rect;
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    .line 432
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    .line 431
    check-cast v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 434
    .local v8, "spoidSettinghandleParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v0, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 435
    .local v0, "lMargin":I
    iget v10, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 437
    .local v10, "tMargin":I
    const/4 v11, 0x0

    aget v11, v1, v11

    iget v12, v4, Landroid/graphics/Rect;->left:I

    if-ge v11, v12, :cond_0

    .line 438
    const/4 v11, 0x0

    iput v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 440
    :cond_0
    const/4 v11, 0x1

    aget v11, v1, v11

    iget v12, v4, Landroid/graphics/Rect;->top:I

    if-ge v11, v12, :cond_1

    .line 441
    const/4 v11, 0x0

    iput v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 444
    :cond_1
    iget v11, v4, Landroid/graphics/Rect;->right:I

    const/4 v12, 0x0

    aget v12, v1, v12

    sub-int/2addr v11, v12

    if-ge v11, v3, :cond_2

    .line 445
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int/2addr v11, v3

    iput v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 447
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v11, :cond_2

    .line 448
    const/4 v11, 0x0

    iput v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 451
    :cond_2
    iget v11, v4, Landroid/graphics/Rect;->bottom:I

    const/4 v12, 0x1

    aget v12, v1, v12

    sub-int/2addr v11, v12

    if-ge v11, v2, :cond_3

    .line 452
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v11

    sub-int/2addr v11, v2

    iput v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 454
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v11, :cond_3

    .line 455
    const/4 v11, 0x0

    iput v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 459
    :cond_3
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-ne v0, v11, :cond_4

    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-ne v10, v11, :cond_4

    .line 494
    :goto_0
    return-void

    .line 463
    :cond_4
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 464
    .local v7, "spoidSettingBgParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    .line 465
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 464
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 466
    .local v5, "spoidColorImageParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    .line 467
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 466
    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 468
    .local v6, "spoidExitBtnParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    .line 469
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 468
    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 471
    .local v9, "spuitCurrentColorParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 472
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v13, 0x41b00000    # 22.0f

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v12

    add-int/2addr v11, v12

    .line 471
    iput v11, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 473
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v11, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 475
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 476
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v13, 0x41c80000    # 25.0f

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v12

    add-int/2addr v11, v12

    .line 475
    iput v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 477
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 478
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v13, 0x40e00000    # 7.0f

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v12

    add-int/2addr v11, v12

    .line 477
    iput v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 480
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 481
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v13, 0x42480000    # 50.0f

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v12

    add-int/2addr v11, v12

    .line 480
    iput v11, v9, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 482
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 483
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v13, 0x40e00000    # 7.0f

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v12

    add-int/2addr v11, v12

    .line 482
    iput v11, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 485
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 486
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v13, 0x429c0000    # 78.0f

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v12

    add-int/2addr v11, v12

    .line 485
    iput v11, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 487
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v11, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 489
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v11, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 490
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v11, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 491
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v11, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 492
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v11, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 493
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v11, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0
.end method

.method private getMovableRect()Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 497
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 498
    .local v0, "location":[I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 500
    .local v1, "r":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    .line 502
    aget v2, v0, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 503
    aget v2, v0, v4

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 504
    aget v2, v0, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 505
    aget v2, v0, v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 507
    return-object v1
.end method

.method private initSpuitSetting(II)V
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/16 v6, 0x8

    const/4 v5, -0x2

    const/4 v4, 0x0

    .line 235
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->totalLayout()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    .line 236
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 238
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpoidSettingListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 240
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 243
    .local v1, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 246
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    .line 247
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v2, v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 249
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    :cond_0
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->movePosition(II)V

    .line 255
    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->setVisibility(I)V

    .line 257
    return-void

    .line 251
    :catch_0
    move-exception v0

    .line 252
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method private setCanvasSize()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->invalidate()V

    .line 181
    :cond_0
    return-void
.end method

.method private spuitColorImage()Landroid/view/View;
    .locals 8

    .prologue
    const/16 v5, 0x16

    const/4 v4, 0x0

    const/high16 v3, 0x41b00000    # 22.0f

    .line 372
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 375
    .local v1, "mSpuitColorView":Landroid/widget/ImageView;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 376
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 375
    invoke-direct {v7, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 377
    .local v7, "mSpoidColorViewParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 378
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40e00000    # 7.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 377
    invoke-virtual {v7, v0, v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 379
    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 382
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v2, "snote_toolbar_icon_spoid_hover"

    .line 383
    const-string v3, "snote_color_spoid_press"

    const-string v4, "snote_color_spoid_focus"

    move v6, v5

    .line 382
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 385
    return-object v1
.end method

.method private spuitCurrentColor()Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x41b00000    # 22.0f

    .line 396
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 400
    .local v0, "mSpuitColorFirst":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 401
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 400
    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 402
    .local v1, "mSpuitColorFirstParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42480000    # 50.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 403
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40e00000    # 7.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 402
    invoke-virtual {v1, v3, v4, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 404
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 409
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v4, "snote_colorchip_shadow"

    invoke-virtual {v3, v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 411
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 412
    .local v2, "mSpuitCurrentColor":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 413
    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 414
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 418
    return-object v2
.end method

.method private spuitExitBtn()Landroid/view/View;
    .locals 14

    .prologue
    const/16 v7, 0x12

    const/high16 v13, 0x41b00000    # 22.0f

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 334
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 336
    .local v1, "spuitExitLayout":Landroid/widget/RelativeLayout;
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 337
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41e00000    # 28.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42180000    # 38.0f

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 336
    invoke-direct {v10, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 339
    .local v10, "spuitExitLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v2, "snote_toolbar_bg_right_normal"

    .line 340
    const-string v3, "snote_toolbar_bg_right_selectedl"

    const-string v4, "snote_toolbar_bg_right_focus"

    const/16 v5, 0x1c

    const/16 v6, 0x26

    .line 339
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 342
    new-instance v3, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 344
    .local v3, "exitButton":Landroid/widget/ImageButton;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v4, "snote_popup_icon_close"

    .line 345
    const-string v5, "snote_popup_icon_close"

    const-string v6, "snote_popup_icon_close"

    move v8, v7

    .line 344
    invoke-virtual/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 347
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 348
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 347
    invoke-direct {v9, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 349
    .local v9, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 350
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40e00000    # 7.0f

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 349
    invoke-virtual {v9, v0, v2, v11, v11}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 351
    invoke-virtual {v3, v9}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 352
    invoke-virtual {v3, v12}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 353
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 354
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpoidExitListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 355
    iput-boolean v12, v10, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 356
    const/16 v0, 0x9

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 357
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x429c0000    # 78.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {v10, v0, v11, v11, v11}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 358
    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 359
    invoke-virtual {v1, v12}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 360
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 361
    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 362
    return-object v1
.end method

.method private spuitdBg()Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 293
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 295
    .local v0, "spoidBgButton":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 296
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42600000    # 56.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42180000    # 38.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 295
    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 297
    .local v1, "spoidBgParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 298
    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 299
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41b00000    # 22.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v5, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 300
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 303
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_gradation"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 304
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 306
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v3, "snote_toolbar_bg_center2_normal"

    invoke-virtual {v2, v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 310
    return-object v0
.end method

.method private spuitdHandle()Landroid/view/View;
    .locals 6

    .prologue
    .line 314
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 316
    .local v0, "spoidHandle":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 317
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41b00000    # 22.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42180000    # 38.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 316
    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 318
    .local v1, "spoidHandleParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 319
    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 321
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 323
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v3, "snote_toolbar_handle"

    const/16 v4, 0x16

    const/16 v5, 0x26

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;II)V

    .line 325
    return-object v0
.end method

.method private totalLayout()Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 266
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 268
    .local v0, "totalLayout":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 270
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->bodyLayout()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 271
    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 159
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 160
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    .line 161
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 162
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    .line 163
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 164
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    .line 165
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 166
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    .line 167
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 168
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    .line 169
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 170
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    .line 171
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    .line 172
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 173
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 174
    return-void
.end method

.method public getColorPickerCurrentColor()I
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mCurrentColor:I

    return v0
.end method

.method public getColorPickerSettingVisible()I
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    return v0
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 213
    return-void
.end method

.method movePosition(II)V
    .locals 11
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v8, 0x0

    const/high16 v10, 0x40e00000    # 7.0f

    .line 511
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    if-nez v7, :cond_0

    .line 570
    :goto_0
    return-void

    .line 515
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    .line 516
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 515
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 517
    .local v5, "spoidSettinghandleParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 518
    .local v4, "spoidSettingBgParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    .line 519
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 518
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 520
    .local v2, "spoidColorImageParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    .line 521
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 520
    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 522
    .local v3, "spoidExitBtnParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    .line 523
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 522
    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 525
    .local v6, "spuitCurrentColorParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput p1, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 526
    iput p2, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 528
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v7, :cond_1

    .line 529
    iput v8, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 531
    :cond_1
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v7, :cond_2

    .line 532
    iput v8, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 535
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x42d40000    # 106.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 536
    .local v1, "minWidth":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x42180000    # 38.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 538
    .local v0, "minHeight":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v7

    if-lez v7, :cond_3

    .line 539
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v8

    sub-int/2addr v8, v1

    if-le v7, v8, :cond_3

    .line 540
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v7

    sub-int/2addr v7, v1

    iput v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 542
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    if-lez v7, :cond_4

    .line 543
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v8

    sub-int/2addr v8, v0

    if-le v7, v8, :cond_4

    .line 544
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sub-int/2addr v7, v0

    iput v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 547
    :cond_4
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 548
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x41b00000    # 22.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    add-int/2addr v7, v8

    .line 547
    iput v7, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 549
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v7, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 551
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 552
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x41c80000    # 25.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    add-int/2addr v7, v8

    .line 551
    iput v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 553
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 554
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    add-int/2addr v7, v8

    .line 553
    iput v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 556
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 557
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x42480000    # 50.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    add-int/2addr v7, v8

    .line 556
    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 558
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 559
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    add-int/2addr v7, v8

    .line 558
    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 561
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 562
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x429c0000    # 78.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    add-int/2addr v7, v8

    .line 561
    iput v7, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 563
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v7, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 565
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v7, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 566
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v7, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 567
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v7, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 568
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v7, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 569
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v7, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0
.end method

.method protected rotatePosition()V
    .locals 20

    .prologue
    .line 66
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    if-nez v15, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mIsRotated:Z

    if-eqz v15, :cond_0

    .line 74
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    .line 75
    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    .line 74
    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 76
    .local v11, "spoidSettinghandleParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 77
    .local v10, "spoidSettingBgParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    .line 78
    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    .line 77
    check-cast v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 79
    .local v8, "spoidColorImageParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    .line 80
    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 79
    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 81
    .local v9, "spoidExitBtnParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    .line 82
    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    .line 81
    check-cast v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 84
    .local v12, "spuitCurrentColorParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v5

    .line 86
    .local v5, "newParentRect":Landroid/graphics/Rect;
    const-string v15, "settingui-colorPicker"

    const-string v16, "==== colorPicker ===="

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const-string v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "old  = "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mOldParentRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mOldParentRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mOldParentRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 88
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mOldParentRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 87
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const-string v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "new  = "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v5, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget v0, v5, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget v0, v5, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 90
    iget v0, v5, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 89
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 93
    .local v6, "r":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mOldParentRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->left:I

    iget v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    iput v15, v6, Landroid/graphics/Rect;->left:I

    .line 94
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mOldParentRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->top:I

    iget v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    iput v15, v6, Landroid/graphics/Rect;->top:I

    .line 95
    iget v15, v6, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x42d40000    # 106.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    iput v15, v6, Landroid/graphics/Rect;->right:I

    .line 96
    iget v15, v6, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x42180000    # 38.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    iput v15, v6, Landroid/graphics/Rect;->bottom:I

    .line 98
    const-string v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "view = "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v6, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget v0, v6, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget v15, v6, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mOldParentRect:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    int-to-float v4, v15

    .line 101
    .local v4, "left":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mOldParentRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->right:I

    iget v0, v6, Landroid/graphics/Rect;->right:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    int-to-float v7, v15

    .line 102
    .local v7, "right":F
    iget v15, v6, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mOldParentRect:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    int-to-float v13, v15

    .line 103
    .local v13, "top":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mOldParentRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->bottom:I

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    int-to-float v2, v15

    .line 105
    .local v2, "bottom":F
    add-float v15, v4, v7

    div-float v3, v4, v15

    .line 106
    .local v3, "hRatio":F
    add-float v15, v13, v2

    div-float v14, v13, v15

    .line 108
    .local v14, "vRatio":F
    const-string v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "left :"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", right :"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    const-string v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "top :"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", bottom :"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    const-string v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "hRatio = "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", vRatio = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    float-to-double v0, v3

    move-wide/from16 v16, v0

    const-wide v18, 0x3fefae147ae147aeL    # 0.99

    cmpl-double v15, v16, v18

    if-lez v15, :cond_4

    .line 113
    const/high16 v3, 0x3f800000    # 1.0f

    .line 118
    :cond_2
    :goto_1
    float-to-double v0, v14

    move-wide/from16 v16, v0

    const-wide v18, 0x3fefae147ae147aeL    # 0.99

    cmpl-double v15, v16, v18

    if-lez v15, :cond_5

    .line 119
    const/high16 v14, 0x3f800000    # 1.0f

    .line 124
    :cond_3
    :goto_2
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v15

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v16

    sub-int v15, v15, v16

    int-to-float v15, v15

    mul-float/2addr v15, v3

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v15

    iput v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 125
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v15

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v16

    sub-int v15, v15, v16

    int-to-float v15, v15

    mul-float/2addr v15, v14

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v15

    iput v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 127
    const-string v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "lMargin = "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", tMargin = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 128
    iget v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 127
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x41b00000    # 22.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 130
    iput v15, v10, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 132
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v15, v10, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 134
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 135
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x41c80000    # 25.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 134
    iput v15, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 136
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x40e00000    # 7.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 136
    iput v15, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 139
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x42480000    # 50.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 139
    iput v15, v12, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 141
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x40e00000    # 7.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 141
    iput v15, v12, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 144
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x429c0000    # 78.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 144
    iput v15, v9, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 146
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v15, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 148
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v15, v11}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 149
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v15, v10}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 150
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v15, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 151
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v15, v12}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 152
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v15, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 114
    :cond_4
    const/4 v15, 0x0

    cmpg-float v15, v3, v15

    if-gez v15, :cond_2

    .line 115
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 120
    :cond_5
    const/4 v15, 0x0

    cmpg-float v15, v14, v15

    if-gez v15, :cond_3

    .line 121
    const/4 v14, 0x0

    goto/16 :goto_2
.end method

.method public setColorPickerColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mCurrentColor:I

    .line 192
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mCurrentColor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 193
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 195
    return-void
.end method

.method protected setRotation()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mOldParentRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mIsRotated:Z

    .line 62
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-nez v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->loadImage()V

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 204
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->setCanvasSize()V

    .line 205
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 206
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->checkPosition()V

    .line 207
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 209
    return-void
.end method
