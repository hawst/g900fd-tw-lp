.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRSimpleShapeStrokesBuilder;
.super Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;
.source "NRRSimpleShapeStrokesBuilder.java"


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;Lcom/samsung/recognitionengine/ShapeInfo;)V
    .locals 0
    .param p1, "penSettings"    # Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;
    .param p2, "shapeInfo"    # Lcom/samsung/recognitionengine/ShapeInfo;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;Lcom/samsung/recognitionengine/ShapeInfo;)V

    .line 16
    return-void
.end method


# virtual methods
.method public buildLayoutObject()Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 6

    .prologue
    .line 19
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSimpleShapeStrokesBuilder;->mShapeInfo:Lcom/samsung/recognitionengine/ShapeInfo;

    invoke-virtual {v4}, Lcom/samsung/recognitionengine/ShapeInfo;->getRecognizedPoints()Lcom/samsung/recognitionengine/PointFVector;

    move-result-object v3

    .line 20
    .local v3, "points":Lcom/samsung/recognitionengine/PointFVector;
    invoke-virtual {v3}, Lcom/samsung/recognitionengine/PointFVector;->size()J

    move-result-wide v4

    long-to-int v2, v4

    .line 21
    .local v2, "pointCount":I
    if-nez v2, :cond_1

    .line 22
    const/4 v0, 0x0

    .line 36
    :cond_0
    :goto_0
    return-object v0

    .line 24
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 26
    .local v0, "container":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    add-int/lit8 v4, v2, -0x1

    if-lt v1, v4, :cond_2

    .line 31
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSimpleShapeStrokesBuilder;->mShapeInfo:Lcom/samsung/recognitionengine/ShapeInfo;

    invoke-virtual {v4}, Lcom/samsung/recognitionengine/ShapeInfo;->isClosedShape()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 32
    add-int/lit8 v4, v2, -0x1

    invoke-virtual {v3, v4}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v4

    .line 33
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v5

    .line 32
    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSimpleShapeStrokesBuilder;->createLineStroke(Lcom/samsung/recognitionengine/PointF;Lcom/samsung/recognitionengine/PointF;)Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0

    .line 27
    :cond_2
    invoke-virtual {v3, v1}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v4

    .line 28
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v3, v5}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v5

    .line 27
    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSimpleShapeStrokesBuilder;->createLineStroke(Lcom/samsung/recognitionengine/PointF;Lcom/samsung/recognitionengine/PointF;)Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 26
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
