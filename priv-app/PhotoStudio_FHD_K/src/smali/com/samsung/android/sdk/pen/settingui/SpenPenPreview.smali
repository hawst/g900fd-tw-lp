.class Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;
.super Landroid/view/View;
.source "SpenPenPreview.java"


# static fields
.field private static CANVAS_WIDTH_VEINNA:I

.field private static NUM_POINTS:I


# instance fields
.field private mAdvancedSetting:Ljava/lang/String;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapPaint:Landroid/graphics/Paint;

.field private mCanvasWidth:I

.field private mColor:I

.field private mContext:Landroid/content/Context;

.field private mMax:F

.field private mMin:F

.field private mPenPluginInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

.field private mPenType:Ljava/lang/String;

.field private mPoints:[F

.field private mPressures:[F

.field private mRect:Landroid/graphics/RectF;

.field private mStrokeWidth:F

.field private mbottom:I

.field private mleft:I

.field private mright:I

.field private mtop:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/16 v0, 0xa

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    .line 20
    const/16 v0, 0x640

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->CANVAS_WIDTH_VEINNA:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 47
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 25
    const/high16 v1, 0x41a00000    # 20.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    .line 26
    const/high16 v1, -0x1000000

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    .line 35
    const/16 v1, 0x438

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    .line 37
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mleft:I

    .line 38
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mright:I

    .line 39
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    .line 40
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mbottom:I

    .line 42
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMin:F

    .line 43
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMax:F

    .line 49
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mContext:Landroid/content/Context;

    .line 51
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    .line 52
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    .line 54
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    .line 55
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmapPaint:Landroid/graphics/Paint;

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 57
    .local v0, "localDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v1, v2, :cond_0

    .line 58
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    .line 62
    :goto_0
    return-void

    .line 60
    :cond_0
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    goto :goto_0
.end method

.method private checkResolution()V
    .locals 13

    .prologue
    .line 120
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mright:I

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mleft:I

    sub-int v7, v9, v10

    .line 121
    .local v7, "width":I
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mbottom:I

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    sub-int v3, v9, v10

    .line 122
    .local v3, "height":I
    int-to-float v9, v7

    const/high16 v10, 0x41800000    # 16.0f

    div-float v8, v9, v10

    .line 123
    .local v8, "widthUnit":F
    int-to-float v9, v3

    const/high16 v10, 0x41800000    # 16.0f

    div-float v2, v9, v10

    .line 125
    .local v2, "heighUnit":F
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v10, 0x0

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mleft:I

    int-to-float v11, v11

    const/high16 v12, 0x3f800000    # 1.0f

    mul-float/2addr v12, v8

    add-float/2addr v11, v12

    aput v11, v9, v10

    .line 126
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->CANVAS_WIDTH_VEINNA:I

    if-ne v9, v10, :cond_1

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    const-string v10, "Marker"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    const-string v10, ".Brush"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 127
    :cond_0
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v6

    .line 128
    .local v6, "penPluginIndex":I
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v9

    invoke-interface {v9}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMinSettingValue()F

    move-result v5

    .line 130
    .local v5, "min":F
    const/high16 v9, 0x41200000    # 10.0f

    add-float/2addr v9, v5

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    int-to-float v10, v10

    mul-float/2addr v9, v10

    const/high16 v10, 0x43b40000    # 360.0f

    div-float/2addr v9, v10

    iput v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMax:F

    .line 131
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    int-to-float v9, v9

    mul-float/2addr v9, v5

    const/high16 v10, 0x43b40000    # 360.0f

    div-float/2addr v9, v10

    iput v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMin:F

    .line 132
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMin:F

    sub-float/2addr v9, v10

    const/high16 v10, 0x41200000    # 10.0f

    mul-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMax:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMin:F

    sub-float/2addr v10, v11

    div-float v0, v9, v10

    .line 133
    .local v0, "delta":F
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v10, 0x1

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    int-to-float v11, v11

    sub-float/2addr v11, v0

    const/high16 v12, 0x41600000    # 14.0f

    mul-float/2addr v12, v2

    add-float/2addr v11, v12

    aput v11, v9, v10

    .line 134
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v10, v10, 0x2

    add-int/lit8 v10, v10, -0x1

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    int-to-float v11, v11

    sub-float/2addr v11, v0

    const/high16 v12, 0x41600000    # 14.0f

    mul-float/2addr v12, v2

    add-float/2addr v11, v12

    aput v11, v9, v10

    .line 140
    .end local v0    # "delta":F
    .end local v5    # "min":F
    .end local v6    # "penPluginIndex":I
    :goto_0
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v10, v10, 0x2

    add-int/lit8 v10, v10, -0x2

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mleft:I

    int-to-float v11, v11

    const/high16 v12, 0x41800000    # 16.0f

    mul-float/2addr v12, v8

    add-float/2addr v11, v12

    aput v11, v9, v10

    .line 142
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v10, v10, 0x2

    add-int/lit8 v10, v10, -0x2

    aget v9, v9, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    sub-float/2addr v9, v10

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    add-int/lit8 v10, v10, -0x1

    int-to-float v10, v10

    div-float v1, v9, v10

    .line 144
    .local v1, "dp":F
    const/4 v4, 0x2

    .local v4, "i":I
    :goto_1
    sget v9, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v9, v9, 0x2

    add-int/lit8 v9, v9, -0x2

    if-lt v4, v9, :cond_2

    .line 149
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    const/4 v10, 0x0

    const v11, 0x3f333333    # 0.7f

    aput v11, v9, v10

    .line 151
    const/4 v4, 0x1

    :goto_2
    sget v9, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    if-lt v4, v9, :cond_3

    .line 154
    return-void

    .line 136
    .end local v1    # "dp":F
    .end local v4    # "i":I
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v10, 0x1

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    int-to-float v11, v11

    const/high16 v12, 0x41600000    # 14.0f

    mul-float/2addr v12, v2

    add-float/2addr v11, v12

    aput v11, v9, v10

    .line 137
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v10, v10, 0x2

    add-int/lit8 v10, v10, -0x1

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    int-to-float v11, v11

    const/high16 v12, 0x41600000    # 14.0f

    mul-float/2addr v12, v2

    add-float/2addr v11, v12

    aput v11, v9, v10

    goto :goto_0

    .line 145
    .restart local v1    # "dp":F
    .restart local v4    # "i":I
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    add-int/lit8 v11, v4, -0x2

    aget v10, v10, v11

    add-float/2addr v10, v1

    aput v10, v9, v4

    .line 146
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    add-int/lit8 v10, v4, 0x1

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v12, 0x1

    aget v11, v11, v12

    aput v11, v9, v10

    .line 144
    add-int/lit8 v4, v4, 0x2

    goto :goto_1

    .line 152
    :cond_3
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    add-int/lit8 v11, v4, -0x1

    aget v10, v10, v11

    const v11, 0x3ccccccd    # 0.025f

    sub-float/2addr v10, v11

    aput v10, v9, v4

    .line 151
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mContext:Landroid/content/Context;

    .line 66
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    .line 67
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    .line 69
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    .line 70
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 72
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 76
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    .line 78
    :cond_1
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    .line 79
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmapPaint:Landroid/graphics/Paint;

    .line 81
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 21
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 166
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v19

    .line 167
    .local v19, "penPluginIndex":I
    const/4 v6, -0x1

    move/from16 v0, v19

    if-ne v0, v6, :cond_1

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    if-nez v6, :cond_2

    .line 171
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    .line 172
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 176
    :cond_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 178
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    .line 179
    const/4 v7, 0x4

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 180
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mAdvancedSetting:Ljava/lang/String;

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setAdvancedSetting(Ljava/lang/String;)V

    .line 183
    :cond_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    .line 184
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setColor(I)V

    .line 186
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 187
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->checkResolution()V

    .line 188
    const-wide/16 v2, 0x0

    .line 190
    .local v2, "time":J
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    mul-float v20, v6, v7

    .line 191
    .local v20, "startShift":F
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    sget v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    add-int/lit8 v8, v8, -0x1

    aget v7, v7, v8

    mul-float v16, v6, v7

    .line 192
    .local v16, "endShift":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    const-string v7, "Marker"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 193
    const/16 v20, 0x0

    .line 194
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float v16, v6, v7

    .line 195
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, -0x2

    aget v6, v6, v7

    sub-float v6, v6, v16

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v8, v8, 0x2

    add-int/lit8 v8, v8, -0x4

    aget v7, v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_4

    .line 196
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, -0x4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v9, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v9, v9, 0x2

    add-int/lit8 v9, v9, -0x2

    aget v8, v8, v9

    sub-float v8, v8, v16

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v8, v9

    aput v8, v6, v7

    .line 199
    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    const-string v7, ".Brush"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 200
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    mul-float v16, v6, v7

    .line 203
    :cond_5
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_1
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    move/from16 v0, v18

    if-lt v0, v6, :cond_6

    .line 224
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmapPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 204
    :cond_6
    if-nez v18, :cond_7

    .line 205
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 206
    const/4 v6, 0x0

    .line 207
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v8, v18, 0x2

    aget v7, v7, v8

    add-float v7, v7, v20

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v9, v18, 0x2

    add-int/lit8 v9, v9, 0x1

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    aget v9, v9, v18

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-wide v4, v2

    .line 206
    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v17

    .line 208
    .local v17, "event":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    move-object/from16 v0, v17

    invoke-interface {v6, v0, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 209
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    .line 203
    :goto_2
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 210
    .end local v17    # "event":Landroid/view/MotionEvent;
    :cond_7
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    add-int/lit8 v6, v6, -0x1

    move/from16 v0, v18

    if-ne v0, v6, :cond_8

    .line 211
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    sub-int v6, v6, v18

    mul-int/lit8 v6, v6, 0x5

    int-to-long v6, v6

    add-long v4, v2, v6

    .line 212
    .local v4, "currentTime":J
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v8, v18, 0x2

    aget v7, v7, v8

    .line 213
    sub-float v7, v7, v16

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v9, v18, 0x2

    add-int/lit8 v9, v9, 0x1

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    aget v9, v9, v18

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 212
    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v17

    .line 214
    .restart local v17    # "event":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    move-object/from16 v0, v17

    invoke-interface {v6, v0, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 215
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    goto :goto_2

    .line 217
    .end local v4    # "currentTime":J
    .end local v17    # "event":Landroid/view/MotionEvent;
    :cond_8
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    sub-int v6, v6, v18

    mul-int/lit8 v6, v6, 0x5

    int-to-long v6, v6

    add-long v4, v2, v6

    .line 218
    .restart local v4    # "currentTime":J
    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v8, v18, 0x2

    aget v7, v7, v8

    .line 219
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v9, v18, 0x2

    add-int/lit8 v9, v9, 0x1

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    aget v9, v9, v18

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 218
    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v17

    .line 220
    .restart local v17    # "event":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    move-object/from16 v0, v17

    invoke-interface {v6, v0, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 221
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    goto/16 :goto_2
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 86
    if-eqz p1, :cond_0

    .line 87
    sub-int v4, p4, p2

    .line 88
    .local v4, "width":I
    sub-int v2, p5, p3

    .line 89
    .local v2, "height":I
    int-to-float v6, v4

    const/high16 v7, 0x41800000    # 16.0f

    div-float v5, v6, v7

    .line 90
    .local v5, "widthUnit":F
    int-to-float v6, v2

    const/high16 v7, 0x41800000    # 16.0f

    div-float v1, v6, v7

    .line 92
    .local v1, "heighUnit":F
    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mleft:I

    .line 93
    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    .line 94
    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mright:I

    .line 95
    iput p5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mbottom:I

    .line 97
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v7, 0x0

    int-to-float v8, p2

    const/high16 v9, 0x3f800000    # 1.0f

    mul-float/2addr v9, v5

    add-float/2addr v8, v9

    aput v8, v6, v7

    .line 98
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v7, 0x1

    int-to-float v8, p3

    const/high16 v9, 0x41600000    # 14.0f

    mul-float/2addr v9, v1

    add-float/2addr v8, v9

    aput v8, v6, v7

    .line 100
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, -0x2

    int-to-float v8, p2

    const/high16 v9, 0x41800000    # 16.0f

    mul-float/2addr v9, v5

    add-float/2addr v8, v9

    aput v8, v6, v7

    .line 101
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, -0x1

    int-to-float v8, p3

    const/high16 v9, 0x41600000    # 14.0f

    mul-float/2addr v9, v1

    add-float/2addr v8, v9

    aput v8, v6, v7

    .line 103
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, -0x2

    aget v6, v6, v7

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    sub-float/2addr v6, v7

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    add-int/lit8 v7, v7, -0x1

    int-to-float v7, v7

    div-float v0, v6, v7

    .line 105
    .local v0, "dp":F
    const/4 v3, 0x2

    .local v3, "i":I
    :goto_0
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v6, v6, 0x2

    add-int/lit8 v6, v6, -0x2

    if-lt v3, v6, :cond_1

    .line 110
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    const/4 v7, 0x0

    const v8, 0x3f333333    # 0.7f

    aput v8, v6, v7

    .line 112
    const/4 v3, 0x1

    :goto_1
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    if-lt v3, v6, :cond_2

    .line 116
    .end local v0    # "dp":F
    .end local v1    # "heighUnit":F
    .end local v2    # "height":I
    .end local v3    # "i":I
    .end local v4    # "width":I
    .end local v5    # "widthUnit":F
    :cond_0
    return-void

    .line 106
    .restart local v0    # "dp":F
    .restart local v1    # "heighUnit":F
    .restart local v2    # "height":I
    .restart local v3    # "i":I
    .restart local v4    # "width":I
    .restart local v5    # "widthUnit":F
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    add-int/lit8 v8, v3, -0x2

    aget v7, v7, v8

    add-float/2addr v7, v0

    aput v7, v6, v3

    .line 107
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    add-int/lit8 v7, v3, 0x1

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v9, 0x1

    aget v8, v8, v9

    aput v8, v6, v7

    .line 105
    add-int/lit8 v3, v3, 0x2

    goto :goto_0

    .line 113
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    add-int/lit8 v8, v3, -0x1

    aget v7, v7, v8

    const v8, 0x3ccccccd    # 0.025f

    sub-float/2addr v7, v8

    aput v7, v6, v3

    .line 112
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 161
    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    .line 162
    return-void
.end method

.method public setPenPlugin(Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;)V
    .locals 1
    .param p1, "penPluginManager"    # Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    .line 291
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginInfoList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    .line 292
    return-void
.end method

.method public setPenType(Ljava/lang/String;)V
    .locals 0
    .param p1, "penType"    # Ljava/lang/String;

    .prologue
    .line 269
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    .line 270
    return-void
.end method

.method public setStrokeAdvancedSetting(Ljava/lang/String;)V
    .locals 0
    .param p1, "adnvance"    # Ljava/lang/String;

    .prologue
    .line 279
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mAdvancedSetting:Ljava/lang/String;

    .line 280
    return-void
.end method

.method public setStrokeAlpha(I)V
    .locals 3
    .param p1, "alpha"    # I

    .prologue
    .line 248
    shl-int/lit8 v0, p1, 0x18

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    .line 249
    return-void
.end method

.method public setStrokeColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 258
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    .line 259
    return-void
.end method

.method public setStrokeSize(F)V
    .locals 0
    .param p1, "width"    # F

    .prologue
    .line 237
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    .line 238
    return-void
.end method
