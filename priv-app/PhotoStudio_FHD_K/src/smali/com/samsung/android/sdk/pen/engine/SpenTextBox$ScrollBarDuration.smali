.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;
.super Landroid/os/Handler;
.source "SpenTextBox.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScrollBarDuration"
.end annotation


# instance fields
.field private final mTextBox:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenTextBox;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 1
    .param p1, "textBox"    # Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .prologue
    .line 5900
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 5901
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->mTextBox:Ljava/lang/ref/WeakReference;

    .line 5902
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 5906
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    .line 5919
    :cond_0
    :goto_0
    return-void

    .line 5910
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 5911
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    if-eqz v0, :cond_0

    .line 5915
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5916
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    .line 5918
    invoke-virtual {p0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
