.class public abstract Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.super Landroid/view/View;
.source "SpenControlBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT:[I = null

.field protected static final ALL:I = -0x1

.field private static final ANGLE_RECT_HEIGHT:I = 0x1e

.field private static final ANGLE_RECT_WIDTH:I = 0x3c

.field private static final ANGLE_TEXT_SIZE:I = 0x12

.field protected static final DEFAULT_BORDER_POINT:Ljava/lang/String; = "handler_icon"

.field protected static final DEFAULT_DEGREE_STRING:Ljava/lang/String; = "\u00b0"

.field private static final DEFAULT_DENSITY_DPI:I = 0xa0

.field protected static final DEFAULT_HANDLE_RESIZE_ICON_SIZE:I = 0x16

.field protected static final DEFAULT_HANDLE_RESIZE_ICON_SIZE_N1:I = 0x21

.field protected static final DEFAULT_HANDLE_ROTATE_ICON_SIZE:I = 0x1e

.field protected static final DEFAULT_HANDLE_ROTATE_ICON_SIZE_N1:I = 0x2d

.field protected static final DEFAULT_RESIZE_ZONE_SIZE:I = 0x7

.field protected static final DEFAULT_RESIZE_ZONE_SIZE_N1:I = 0xc

.field protected static final DEFAULT_ROTATE_POINT_BORDER:Ljava/lang/String; = "handler_icon_rotate"

.field protected static final DEFAULT_ROTATION_ZONE_SIZE:I = 0x19

.field protected static final DEFAULT_ROTATION_ZONE_SIZE_N1:I = 0x26

.field protected static final DIMMING_BG_COLOR:I = 0x40000000

.field protected static final FLIP_DIRECTION_HORIZONTAL:I = 0x2

.field protected static final FLIP_DIRECTION_NONE:I = 0x0

.field protected static final FLIP_DIRECTION_VERTICAL:I = 0x1

.field protected static final MIN_RESIZE_ZONE_SIZE:I = 0x5

.field protected static final NO_OBJECT:I = -0x1

.field protected static final SIGMA:F = 1.0E-4f

.field public static final STYLE_BORDER_NONE:I = 0x1

.field public static final STYLE_BORDER_NONE_ACTION_NONE:I = 0x3

.field public static final STYLE_BORDER_OBJECT:I = 0x0

.field public static final STYLE_BORDER_STATIC:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SpenControlBase"

.field protected static final TOUCH_ZONE_BOTTOM:I = 0x7

.field protected static final TOUCH_ZONE_BOTTOM_LEFT:I = 0x6

.field protected static final TOUCH_ZONE_BOTTOM_RIGHT:I = 0x8

.field protected static final TOUCH_ZONE_CENTER:I = 0x9

.field protected static final TOUCH_ZONE_LEFT:I = 0x4

.field protected static final TOUCH_ZONE_MAX:I = 0xa

.field protected static final TOUCH_ZONE_NONE:I = -0x1

.field protected static final TOUCH_ZONE_RIGHT:I = 0x5

.field protected static final TOUCH_ZONE_ROTATE:I = 0x0

.field protected static final TOUCH_ZONE_TOP:I = 0x2

.field protected static final TOUCH_ZONE_TOP_LEFT:I = 0x1

.field protected static final TOUCH_ZONE_TOP_RIGHT:I = 0x3

.field private static final TRIVIAL_MOVING_CRITERIA:I = 0x14

.field private static final mDeltaEdge:I = 0x1

.field private static mObjectOutlineEnable:Z


# instance fields
.field protected mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

.field private final mControlBaseContext:Landroid/content/Context;

.field private mDensityDpi:I

.field protected mIsClosed:Z

.field private mIsDim:Z

.field private mIsFirstTouch:Z

.field private mIsFlipDirectionHorizontal:Z

.field private mIsFlipDirectionVertical:Z

.field private mIsObjectChange:Z

.field protected mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

.field protected mMaximumResizeRect:Landroid/graphics/RectF;

.field protected mMinimumResizeRect:Landroid/graphics/RectF;

.field private mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

.field private mObjectBaseList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end field

.field private mOnePT:F

.field private mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

.field private mOrgRectList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field protected mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

.field private mPanBackup:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation
.end field

.field private mPointDown:Landroid/graphics/PointF;

.field private mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

.field private mRectList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private mResourceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field protected mRotateAngle:F

.field private final mSelectContextMenuListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

.field private mStyle:I

.field private mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

.field protected mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

.field private mTempRect:Landroid/graphics/Rect;

.field private mTempRectF:Landroid/graphics/RectF;

.field private mTmpMatrix:Landroid/graphics/Matrix;

.field private mTouchEnable:Z

.field protected mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

.field private mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

.field protected mTransactionTouchEvent:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

.field private mTrivialMovingEn:Z


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT()[I
    .locals 3

    .prologue
    .line 59
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->values()[Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_1:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_2:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_3:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_4:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_MAX:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectOutlineEnable:Z

    .line 331
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 3563
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 285
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    .line 291
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 295
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    .line 302
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    .line 303
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    .line 304
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    .line 306
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    .line 309
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    .line 310
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRect:Landroid/graphics/Rect;

    .line 313
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    .line 314
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsClosed:Z

    .line 319
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    .line 323
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFirstTouch:Z

    .line 327
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    .line 328
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    .line 705
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mSelectContextMenuListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    .line 3564
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mControlBaseContext:Landroid/content/Context;

    .line 3565
    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->initialize(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 3566
    return-void
.end method

.method private Increase2MinimumRect(Landroid/graphics/RectF;)Z
    .locals 3
    .param p1, "srcRect"    # Landroid/graphics/RectF;

    .prologue
    .line 2000
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v1, :cond_0

    .line 2001
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 2002
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2005
    :cond_0
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 2006
    .local v0, "rect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2008
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    .line 2009
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 2010
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 2013
    :cond_2
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    .line 2014
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 2016
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2018
    const/4 v1, 0x1

    .line 2020
    :goto_0
    return v1

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mControlBaseContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)I
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    return v0
.end method

.method private adjustObjectRect(I)V
    .locals 16
    .param p1, "index"    # I

    .prologue
    .line 3613
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v14

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-nez v14, :cond_1

    .line 3675
    :cond_0
    :goto_0
    return-void

    .line 3617
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 3619
    .local v11, "obj":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    const/high16 v8, 0x41200000    # 10.0f

    .line 3620
    .local v8, "defaultMinWidth":F
    const/high16 v7, 0x41200000    # 10.0f

    .line 3621
    .local v7, "defaultMinHeight":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v14

    mul-int/lit8 v14, v14, 0x2

    int-to-float v6, v14

    .line 3622
    .local v6, "defaultMaxWidht":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v14

    mul-int/lit8 v14, v14, 0x2

    int-to-float v5, v14

    .line 3624
    .local v5, "defaultMaxHeight":F
    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinWidth()F

    move-result v4

    .line 3625
    .local v4, "currMinWidth":F
    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMaxWidth()F

    move-result v2

    .line 3626
    .local v2, "currMaxWidth":F
    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinHeight()F

    move-result v3

    .line 3627
    .local v3, "currMinHeight":F
    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMaxHeight()F

    move-result v1

    .line 3629
    .local v1, "currMaxHeight":F
    const/4 v14, 0x0

    cmpg-float v14, v4, v14

    if-lez v14, :cond_0

    const/4 v14, 0x0

    cmpg-float v14, v3, v14

    if-lez v14, :cond_0

    const/4 v14, 0x0

    cmpg-float v14, v2, v14

    if-lez v14, :cond_0

    const/4 v14, 0x0

    cmpg-float v14, v1, v14

    if-lez v14, :cond_0

    .line 3635
    sub-float v14, v8, v4

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    const v15, 0x38d1b717    # 1.0E-4f

    cmpg-float v14, v14, v15

    if-gez v14, :cond_2

    sub-float v14, v6, v2

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    const v15, 0x38d1b717    # 1.0E-4f

    cmpg-float v14, v14, v15

    if-gez v14, :cond_2

    .line 3636
    sub-float v14, v7, v3

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    const v15, 0x38d1b717    # 1.0E-4f

    cmpg-float v14, v14, v15

    if-gez v14, :cond_2

    .line 3637
    sub-float v14, v5, v1

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    const v15, 0x38d1b717    # 1.0E-4f

    cmpg-float v14, v14, v15

    if-ltz v14, :cond_0

    .line 3641
    :cond_2
    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v12

    .line 3642
    .local v12, "objectRect":Landroid/graphics/RectF;
    invoke-virtual {v12}, Landroid/graphics/RectF;->width()F

    move-result v13

    .line 3643
    .local v13, "width":F
    invoke-virtual {v12}, Landroid/graphics/RectF;->height()F

    move-result v9

    .line 3644
    .local v9, "height":F
    const/4 v10, 0x0

    .line 3646
    .local v10, "isRectChange":Z
    cmpg-float v14, v13, v4

    if-gez v14, :cond_3

    .line 3647
    move v13, v4

    .line 3648
    const/4 v10, 0x1

    .line 3651
    :cond_3
    cmpg-float v14, v9, v3

    if-gez v14, :cond_4

    .line 3652
    move v9, v3

    .line 3653
    const/4 v10, 0x1

    .line 3655
    :cond_4
    const/4 v14, 0x0

    cmpl-float v14, v2, v14

    if-lez v14, :cond_5

    cmpl-float v14, v2, v4

    if-lez v14, :cond_5

    cmpl-float v14, v13, v2

    if-lez v14, :cond_5

    .line 3656
    move v13, v2

    .line 3657
    const/4 v10, 0x1

    .line 3659
    :cond_5
    const/4 v14, 0x0

    cmpl-float v14, v1, v14

    if-lez v14, :cond_6

    cmpl-float v14, v1, v3

    if-lez v14, :cond_6

    cmpl-float v14, v9, v1

    if-lez v14, :cond_6

    .line 3660
    move v9, v1

    .line 3661
    const/4 v10, 0x1

    .line 3664
    :cond_6
    if-eqz v10, :cond_0

    .line 3668
    iget v14, v12, Landroid/graphics/RectF;->left:F

    add-float/2addr v14, v13

    iput v14, v12, Landroid/graphics/RectF;->right:F

    .line 3669
    iget v14, v12, Landroid/graphics/RectF;->top:F

    add-float/2addr v14, v9

    iput v14, v12, Landroid/graphics/RectF;->bottom:F

    .line 3671
    const/4 v14, 0x0

    invoke-virtual {v11, v12, v14}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 3672
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v14, :cond_0

    .line 3673
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-interface {v14, v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onObjectChanged(Ljava/util/ArrayList;)V

    goto/16 :goto_0
.end method

.method private calculateContextMenuPosition(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 24
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 1073
    new-instance v13, Landroid/graphics/Rect;

    invoke-direct {v13}, Landroid/graphics/Rect;-><init>()V

    .line 1074
    .local v13, "retRect":Landroid/graphics/Rect;
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v12

    .line 1075
    .local v12, "rectBorder":Landroid/graphics/RectF;
    if-nez v12, :cond_0

    .line 1076
    const/4 v13, 0x0

    .line 1181
    .end local v13    # "retRect":Landroid/graphics/Rect;
    :goto_0
    return-object v13

    .line 1079
    .restart local v13    # "retRect":Landroid/graphics/Rect;
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    invoke-virtual {v12}, Landroid/graphics/RectF;->height()F

    move-result v20

    const/high16 v21, 0x40000000    # 2.0f

    div-float v20, v20, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    move-object/from16 v21, v0

    .line 1080
    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v1, v12}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/RectF;->height()F

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    move-object/from16 v22, v0

    const/16 v23, 0x2

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v0, v1, v12}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v22

    .line 1081
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/RectF;->height()F

    move-result v22

    const/high16 v23, 0x40000000    # 2.0f

    div-float v22, v22, v23

    add-float v21, v21, v22

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    .line 1079
    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v8, v0

    .line 1083
    .local v8, "knopHeight":I
    invoke-virtual {v12}, Landroid/graphics/RectF;->centerY()F

    move-result v18

    int-to-float v0, v8

    move/from16 v19, v0

    sub-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v9, v0

    .line 1085
    .local v9, "knopY":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 1087
    .local v11, "parentLayout":Landroid/view/ViewGroup;
    if-nez v11, :cond_1

    .line 1088
    const/4 v13, 0x0

    goto :goto_0

    .line 1091
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v18, v0

    if-nez v18, :cond_2

    .line 1092
    const/4 v13, 0x0

    goto :goto_0

    .line 1095
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getRect()Landroid/graphics/Rect;

    move-result-object v10

    .line 1096
    .local v10, "menuRect":Landroid/graphics/Rect;
    if-nez v10, :cond_3

    .line 1097
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 1100
    :cond_3
    const/16 v16, 0x0

    .local v16, "y1":I
    const/16 v17, 0x0

    .local v17, "y2":I
    const/4 v15, 0x0

    .local v15, "y":I
    const/4 v14, 0x0

    .line 1101
    .local v14, "x":I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v18

    if-nez v18, :cond_c

    .line 1102
    const/4 v6, 0x5

    .line 1103
    .local v6, "delta_padding":I
    const/16 v18, 0x1

    int-to-float v0, v6

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    .line 1104
    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v20

    .line 1103
    invoke-static/range {v18 .. v20}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v18

    move/from16 v0, v18

    float-to-int v6, v0

    .line 1106
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    sub-int v18, v18, v6

    move/from16 v0, v18

    iput v0, v10, Landroid/graphics/Rect;->left:I

    .line 1107
    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    sub-int v18, v18, v6

    move/from16 v0, v18

    iput v0, v10, Landroid/graphics/Rect;->top:I

    .line 1108
    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    add-int v18, v18, v6

    move/from16 v0, v18

    iput v0, v10, Landroid/graphics/Rect;->right:I

    .line 1109
    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    add-int v18, v18, v6

    move/from16 v0, v18

    iput v0, v10, Landroid/graphics/Rect;->bottom:I

    .line 1111
    const/16 v18, 0x1

    const/high16 v19, 0x40e00000    # 7.0f

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    .line 1112
    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v20

    .line 1111
    invoke-static/range {v18 .. v20}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v18

    move/from16 v0, v18

    float-to-int v7, v0

    .line 1113
    .local v7, "gap":I
    const/16 v18, 0x1

    const/high16 v19, 0x40a00000    # 5.0f

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    .line 1114
    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v20

    .line 1113
    invoke-static/range {v18 .. v20}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v18

    move/from16 v0, v18

    float-to-int v5, v0

    .line 1115
    .local v5, "delta":I
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v3, v0

    .line 1117
    .local v3, "centerTopX":I
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v0, v9, :cond_6

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 1118
    .local v4, "centerTopY":I
    :goto_1
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v0, v9, :cond_7

    move-object/from16 v0, p1

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 1120
    .local v2, "centerBottomY":I
    :goto_2
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v18

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v18, v0

    sub-int v14, v3, v18

    .line 1121
    if-gez v14, :cond_8

    .line 1122
    const/4 v14, 0x0

    .line 1127
    :cond_4
    :goto_3
    sub-int v18, v4, v7

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v19

    sub-int v16, v18, v19

    .line 1128
    add-int v18, v2, v7

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v19

    add-int v18, v18, v19

    sub-int v17, v18, v5

    .line 1130
    move/from16 v15, v16

    .line 1131
    if-gez v16, :cond_a

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_a

    .line 1132
    invoke-virtual {v12}, Landroid/graphics/RectF;->centerY()F

    move-result v18

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    cmpl-float v18, v18, v19

    if-lez v18, :cond_9

    .line 1133
    const/4 v15, 0x0

    .line 1142
    :cond_5
    :goto_4
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v18

    add-int v18, v18, v14

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v19

    add-int v19, v19, v15

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v13, v14, v15, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .end local v2    # "centerBottomY":I
    .end local v4    # "centerTopY":I
    :cond_6
    move v4, v9

    .line 1117
    goto :goto_1

    .restart local v4    # "centerTopY":I
    :cond_7
    move v2, v9

    .line 1118
    goto :goto_2

    .line 1123
    .restart local v2    # "centerBottomY":I
    :cond_8
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v18

    add-int v18, v18, v14

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getWidth()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_4

    .line 1124
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getWidth()I

    move-result v18

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v19

    sub-int v14, v18, v19

    goto :goto_3

    .line 1135
    :cond_9
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v18

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v19

    sub-int v18, v18, v19

    sub-int v15, v18, v5

    .line 1137
    goto :goto_4

    :cond_a
    if-gez v16, :cond_b

    .line 1138
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v18

    sub-int v15, v17, v18

    .line 1139
    goto :goto_4

    :cond_b
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_5

    .line 1140
    sub-int v15, v16, v5

    goto :goto_4

    .line 1144
    .end local v2    # "centerBottomY":I
    .end local v3    # "centerTopX":I
    .end local v4    # "centerTopY":I
    .end local v5    # "delta":I
    .end local v6    # "delta_padding":I
    .end local v7    # "gap":I
    :cond_c
    const/16 v18, 0x1

    const/high16 v19, 0x40e00000    # 7.0f

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    .line 1145
    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v20

    .line 1144
    invoke-static/range {v18 .. v20}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v18

    move/from16 v0, v18

    float-to-int v7, v0

    .line 1146
    .restart local v7    # "gap":I
    const/16 v18, 0x1

    const/high16 v19, 0x40a00000    # 5.0f

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    .line 1147
    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v20

    .line 1146
    invoke-static/range {v18 .. v20}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v18

    move/from16 v0, v18

    float-to-int v5, v0

    .line 1148
    .restart local v5    # "delta":I
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v3, v0

    .line 1150
    .restart local v3    # "centerTopX":I
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v0, v9, :cond_f

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 1151
    .restart local v4    # "centerTopY":I
    :goto_5
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v0, v9, :cond_10

    move-object/from16 v0, p1

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 1153
    .restart local v2    # "centerBottomY":I
    :goto_6
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v18

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v18, v0

    sub-int v14, v3, v18

    .line 1154
    if-gez v14, :cond_11

    .line 1155
    const/4 v14, 0x0

    .line 1159
    :cond_d
    :goto_7
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v18

    sub-int v18, v4, v18

    mul-int/lit8 v19, v7, 0x2

    add-int v16, v18, v19

    .line 1160
    add-int v18, v2, v7

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v19

    add-int v18, v18, v19

    sub-int v17, v18, v5

    .line 1162
    move/from16 v15, v16

    .line 1163
    if-gez v16, :cond_13

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_13

    .line 1164
    invoke-virtual {v12}, Landroid/graphics/RectF;->centerY()F

    move-result v18

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    cmpl-float v18, v18, v19

    if-lez v18, :cond_12

    .line 1165
    const/4 v15, 0x0

    .line 1179
    :cond_e
    :goto_8
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v18

    add-int v18, v18, v14

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v19

    add-int v19, v19, v15

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v13, v14, v15, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .end local v2    # "centerBottomY":I
    .end local v4    # "centerTopY":I
    :cond_f
    move v4, v9

    .line 1150
    goto :goto_5

    .restart local v4    # "centerTopY":I
    :cond_10
    move v2, v9

    .line 1151
    goto :goto_6

    .line 1156
    .restart local v2    # "centerBottomY":I
    :cond_11
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v18

    add-int v18, v18, v14

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getWidth()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_d

    .line 1157
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getWidth()I

    move-result v18

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v19

    sub-int v14, v18, v19

    goto :goto_7

    .line 1167
    :cond_12
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v18

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v19

    sub-int v18, v18, v19

    mul-int/lit8 v19, v7, 0x2

    sub-int v15, v18, v19

    .line 1169
    goto :goto_8

    :cond_13
    if-gez v16, :cond_15

    .line 1170
    if-ne v2, v9, :cond_14

    .line 1171
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v18

    sub-int v18, v17, v18

    mul-int/lit8 v19, v7, 0x3

    sub-int v15, v18, v19

    .line 1172
    goto :goto_8

    .line 1173
    :cond_14
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v18

    sub-int v15, v17, v18

    .line 1176
    goto :goto_8

    :cond_15
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_e

    .line 1177
    move/from16 v15, v16

    goto :goto_8
.end method

.method private checkAllRectOutOfCanvas(Landroid/graphics/RectF;IF)Z
    .locals 22
    .param p1, "rectF"    # Landroid/graphics/RectF;
    .param p2, "index"    # I
    .param p3, "angle"    # F

    .prologue
    .line 1251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 1252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 1253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 1256
    :cond_0
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v8

    .line 1257
    .local v8, "pivotX":F
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v9

    .line 1260
    .local v9, "pivotY":F
    new-instance v15, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v15, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1261
    .local v15, "topleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v8, v9, v1, v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v15

    .line 1263
    new-instance v16, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1264
    .local v16, "topright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p3

    move-object/from16 v2, v16

    invoke-direct {v0, v8, v9, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v16

    .line 1266
    new-instance v5, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v5, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1267
    .local v5, "bottomleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v8, v9, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 1269
    new-instance v6, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v6, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1270
    .local v6, "bottomright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v8, v9, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 1272
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v15, v1, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v10

    .line 1273
    .local v10, "points":[F
    const/16 v17, 0x0

    aget v7, v10, v17

    .line 1274
    .local v7, "left":F
    const/16 v17, 0x1

    aget v14, v10, v17

    .line 1275
    .local v14, "top":F
    const/16 v17, 0x2

    aget v11, v10, v17

    .line 1276
    .local v11, "right":F
    const/16 v17, 0x3

    aget v4, v10, v17

    .line 1278
    .local v4, "bottom":F
    new-instance v12, Landroid/graphics/RectF;

    invoke-direct {v12, v7, v14, v11, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1279
    .local v12, "tmpBoundBoxRect":Landroid/graphics/RectF;
    new-instance v13, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v17, v0

    const/high16 v18, 0x3f800000    # 1.0f

    add-float v17, v17, v18

    .line 1280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    .line 1281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    sub-float v20, v20, v21

    .line 1279
    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v13, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1283
    .local v13, "tmpFrameRect":Landroid/graphics/RectF;
    invoke-virtual {v13, v12}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    move-result v17

    if-nez v17, :cond_1

    invoke-virtual {v12, v13}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 1284
    :cond_1
    const/16 v17, 0x0

    .line 1287
    :goto_0
    return v17

    :cond_2
    const/16 v17, 0x1

    goto :goto_0
.end method

.method private drawDimmingWindow(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 799
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    if-eqz v0, :cond_0

    .line 800
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 802
    :cond_0
    return-void
.end method

.method private drawHighlightRect(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 4168
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectOutlineEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 4188
    :goto_0
    return-void

    .line 4171
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v1

    .line 4173
    .local v1, "paint":Landroid/graphics/Paint;
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4174
    .local v0, "dstRect":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 4176
    .local v2, "srcRect":Landroid/graphics/RectF;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 4178
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v3, :cond_1

    .line 4179
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 4181
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 4183
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 4184
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 4186
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 4187
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method private drawHighlightStroke(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V
    .locals 31
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "objectStroke"    # Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .prologue
    .line 4247
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectOutlineEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 4487
    :cond_0
    :goto_0
    return-void

    .line 4250
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getType()I

    move-result v3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 4255
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenName()Ljava/lang/String;

    move-result-object v23

    .line 4256
    .local v23, "penName":Ljava/lang/String;
    if-eqz v23, :cond_0

    .line 4260
    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 4264
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 4266
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v3, :cond_2

    .line 4267
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 4270
    :cond_2
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getXPoints()[F

    move-result-object v26

    .line 4271
    .local v26, "pointXs":[F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getYPoints()[F

    move-result-object v27

    .line 4272
    .local v27, "pointYs":[F
    if-eqz v26, :cond_0

    if-eqz v27, :cond_0

    .line 4275
    move-object/from16 v0, v26

    array-length v13, v0

    .line 4276
    .local v13, "lenX":I
    move-object/from16 v0, v27

    array-length v14, v0

    .line 4277
    .local v14, "lenY":I
    if-le v13, v14, :cond_6

    move v12, v14

    .line 4278
    .local v12, "len":I
    :goto_1
    new-array v0, v12, [Landroid/graphics/PointF;

    move-object/from16 v16, v0

    .line 4279
    .local v16, "myPointList":[Landroid/graphics/PointF;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    move-object/from16 v0, v16

    array-length v3, v0

    if-lt v11, v3, :cond_7

    .line 4284
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v22

    .line 4285
    .local v22, "paint":Landroid/graphics/Paint;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4286
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 4288
    .local v2, "myPath":Landroid/graphics/Path;
    const/4 v11, 0x0

    :goto_3
    if-lt v11, v12, :cond_8

    .line 4295
    const/16 v3, 0x64

    if-gt v12, v3, :cond_1e

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->isCurveEnabled()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 4296
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 4297
    .local v20, "orgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    const/4 v11, 0x0

    :goto_4
    if-lt v11, v12, :cond_9

    .line 4303
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v21

    .line 4305
    .local v21, "orgSize":I
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 4306
    .local v17, "newList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 4307
    const/4 v11, 0x0

    :goto_5
    add-int/lit8 v3, v21, -0x1

    if-lt v11, v3, :cond_b

    .line 4322
    :cond_3
    :goto_6
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v18

    .line 4324
    .local v18, "newSize":I
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    const/4 v4, 0x0

    aget-object v4, v16, v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 4326
    new-instance v30, Landroid/graphics/PointF;

    invoke-direct/range {v30 .. v30}, Landroid/graphics/PointF;-><init>()V

    .line 4327
    .local v30, "startPoint":Landroid/graphics/PointF;
    new-instance v10, Landroid/graphics/PointF;

    invoke-direct {v10}, Landroid/graphics/PointF;-><init>()V

    .line 4328
    .local v10, "controlPoint":Landroid/graphics/PointF;
    new-instance v19, Landroid/graphics/PointF;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/PointF;-><init>()V

    .line 4330
    .local v19, "nextPoint":Landroid/graphics/PointF;
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4332
    const/4 v3, 0x3

    move/from16 v0, v21

    if-lt v0, v3, :cond_1c

    .line 4333
    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Marker"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 4334
    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Brush"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 4335
    :cond_4
    new-instance v9, Landroid/graphics/PointF;

    invoke-direct {v9}, Landroid/graphics/PointF;-><init>()V

    .line 4336
    .local v9, "calcPoint":Landroid/graphics/PointF;
    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v9, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getStartEndBitmapCalcPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;F)V

    .line 4337
    new-instance v28, Landroid/graphics/PointF;

    invoke-direct/range {v28 .. v28}, Landroid/graphics/PointF;-><init>()V

    .line 4339
    .local v28, "prevMiddlePoint":Landroid/graphics/PointF;
    iget v4, v9, Landroid/graphics/PointF;->x:F

    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v28

    iput v3, v0, Landroid/graphics/PointF;->x:F

    .line 4340
    iget v4, v9, Landroid/graphics/PointF;->y:F

    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v28

    iput v3, v0, Landroid/graphics/PointF;->y:F

    .line 4342
    iget v3, v9, Landroid/graphics/PointF;->x:F

    iget v4, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 4344
    iget v3, v9, Landroid/graphics/PointF;->x:F

    iget v4, v9, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4345
    iget v3, v9, Landroid/graphics/PointF;->x:F

    iget v4, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4346
    move-object/from16 v0, v28

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v28

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4348
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_e

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_e

    .line 4349
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4350
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4355
    :goto_7
    const/16 v29, 0x0

    .line 4356
    .local v29, "prevPoint":Landroid/graphics/PointF;
    const/4 v11, 0x1

    :goto_8
    add-int/lit8 v3, v21, -0x1

    if-lt v11, v3, :cond_f

    .line 4378
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4379
    move-object/from16 v0, v29

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v29

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4380
    add-int/lit8 v3, v21, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v21, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4382
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_11

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_11

    .line 4383
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4384
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4457
    .end local v9    # "calcPoint":Landroid/graphics/PointF;
    .end local v28    # "prevMiddlePoint":Landroid/graphics/PointF;
    .end local v29    # "prevPoint":Landroid/graphics/PointF;
    :cond_5
    :goto_9
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 4458
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4459
    const/4 v3, -0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 4460
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 4461
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .end local v2    # "myPath":Landroid/graphics/Path;
    .end local v10    # "controlPoint":Landroid/graphics/PointF;
    .end local v11    # "i":I
    .end local v12    # "len":I
    .end local v16    # "myPointList":[Landroid/graphics/PointF;
    .end local v17    # "newList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    .end local v18    # "newSize":I
    .end local v19    # "nextPoint":Landroid/graphics/PointF;
    .end local v20    # "orgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    .end local v21    # "orgSize":I
    .end local v22    # "paint":Landroid/graphics/Paint;
    .end local v30    # "startPoint":Landroid/graphics/PointF;
    :cond_6
    move v12, v13

    .line 4277
    goto/16 :goto_1

    .line 4280
    .restart local v11    # "i":I
    .restart local v12    # "len":I
    .restart local v16    # "myPointList":[Landroid/graphics/PointF;
    :cond_7
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    aput-object v3, v16, v11

    .line 4281
    aget-object v3, v16, v11

    aget v4, v26, v11

    iput v4, v3, Landroid/graphics/PointF;->x:F

    .line 4282
    aget-object v3, v16, v11

    aget v4, v27, v11

    iput v4, v3, Landroid/graphics/PointF;->y:F

    .line 4279
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_2

    .line 4289
    .restart local v2    # "myPath":Landroid/graphics/Path;
    .restart local v22    # "paint":Landroid/graphics/Paint;
    :cond_8
    aget-object v3, v16, v11

    aget-object v4, v16, v11

    iget v4, v4, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v4, v5

    .line 4290
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v5

    .line 4289
    iput v4, v3, Landroid/graphics/PointF;->x:F

    .line 4291
    aget-object v3, v16, v11

    aget-object v4, v16, v11

    iget v4, v4, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v4, v5

    .line 4292
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v5

    .line 4291
    iput v4, v3, Landroid/graphics/PointF;->y:F

    .line 4288
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    .line 4298
    .restart local v20    # "orgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    :cond_9
    aget-object v3, v16, v11

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isContained(Ljava/util/ArrayList;Landroid/graphics/PointF;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 4299
    aget-object v3, v16, v11

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4297
    :cond_a
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_4

    .line 4308
    .restart local v17    # "newList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    .restart local v21    # "orgSize":I
    :cond_b
    new-instance v4, Landroid/graphics/PointF;

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v5, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v3, v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v6, v3, Landroid/graphics/PointF;->y:F

    .line 4309
    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v3, v6

    invoke-direct {v4, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 4308
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4307
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_5

    .line 4312
    :cond_c
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4314
    const/4 v11, 0x1

    :goto_a
    add-int/lit8 v3, v21, -0x2

    if-lt v11, v3, :cond_d

    .line 4318
    const/4 v3, 0x1

    move/from16 v0, v21

    if-le v0, v3, :cond_3

    .line 4319
    add-int/lit8 v3, v21, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 4315
    :cond_d
    new-instance v4, Landroid/graphics/PointF;

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v5, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v11, 0x2

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v3, v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v6, v3, Landroid/graphics/PointF;->y:F

    .line 4316
    add-int/lit8 v3, v11, 0x2

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v3, v6

    invoke-direct {v4, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 4315
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4314
    add-int/lit8 v11, v11, 0x2

    goto :goto_a

    .line 4352
    .restart local v9    # "calcPoint":Landroid/graphics/PointF;
    .restart local v10    # "controlPoint":Landroid/graphics/PointF;
    .restart local v18    # "newSize":I
    .restart local v19    # "nextPoint":Landroid/graphics/PointF;
    .restart local v28    # "prevMiddlePoint":Landroid/graphics/PointF;
    .restart local v30    # "startPoint":Landroid/graphics/PointF;
    :cond_e
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto/16 :goto_7

    .line 4357
    .restart local v29    # "prevPoint":Landroid/graphics/PointF;
    :cond_f
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    .end local v29    # "prevPoint":Landroid/graphics/PointF;
    check-cast v29, Landroid/graphics/PointF;

    .line 4359
    .restart local v29    # "prevPoint":Landroid/graphics/PointF;
    new-instance v15, Landroid/graphics/PointF;

    invoke-direct {v15}, Landroid/graphics/PointF;-><init>()V

    .line 4360
    .local v15, "middlePoint":Landroid/graphics/PointF;
    move-object/from16 v0, v29

    iget v4, v0, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    float-to-double v4, v3

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    double-to-float v3, v4

    iput v3, v15, Landroid/graphics/PointF;->x:F

    .line 4361
    move-object/from16 v0, v29

    iget v4, v0, Landroid/graphics/PointF;->y:F

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v4

    float-to-double v4, v3

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    double-to-float v3, v4

    iput v3, v15, Landroid/graphics/PointF;->y:F

    .line 4363
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4364
    move-object/from16 v0, v29

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v29

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4365
    iget v3, v15, Landroid/graphics/PointF;->x:F

    iget v4, v15, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4367
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_10

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_10

    .line 4368
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4369
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4374
    :goto_b
    move-object/from16 v29, v15

    .line 4356
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_8

    .line 4371
    :cond_10
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto :goto_b

    .line 4386
    .end local v15    # "middlePoint":Landroid/graphics/PointF;
    :cond_11
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto/16 :goto_9

    .line 4389
    .end local v9    # "calcPoint":Landroid/graphics/PointF;
    .end local v28    # "prevMiddlePoint":Landroid/graphics/PointF;
    .end local v29    # "prevPoint":Landroid/graphics/PointF;
    :cond_12
    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 4390
    const/4 v11, 0x0

    :goto_c
    move/from16 v0, v18

    if-lt v11, v0, :cond_13

    .line 4404
    add-int/lit8 v3, v21, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v21, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_9

    .line 4391
    :cond_13
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4392
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4394
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_14

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_14

    .line 4395
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4396
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4401
    :goto_d
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4390
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_c

    .line 4398
    :cond_14
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto :goto_d

    .line 4405
    :cond_15
    const-string v3, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 4406
    rem-int/lit8 v3, v21, 0x2

    const/4 v4, 0x1

    if-ne v3, v4, :cond_17

    .line 4407
    const/4 v11, 0x0

    :goto_e
    add-int/lit8 v3, v18, -0x1

    if-ge v11, v3, :cond_5

    .line 4408
    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4409
    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4411
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_16

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_16

    .line 4412
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4413
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4418
    :goto_f
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4407
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_e

    .line 4415
    :cond_16
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto :goto_f

    .line 4421
    :cond_17
    const/4 v11, 0x0

    :goto_10
    add-int/lit8 v3, v18, -0x2

    if-lt v11, v3, :cond_18

    .line 4434
    new-instance v25, Landroid/graphics/PointF;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/PointF;-><init>()V

    .line 4435
    .local v25, "point3":Landroid/graphics/PointF;
    add-int/lit8 v3, v18, -0x2

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/graphics/PointF;

    .line 4436
    .local v24, "point2":Landroid/graphics/PointF;
    add-int/lit8 v3, v21, -0x3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    .end local v25    # "point3":Landroid/graphics/PointF;
    check-cast v25, Landroid/graphics/PointF;

    .line 4438
    .restart local v25    # "point3":Landroid/graphics/PointF;
    move-object/from16 v0, v24

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v25

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/PointF;->y:F

    add-int/lit8 v7, v18, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    .line 4439
    add-int/lit8 v8, v18, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    .line 4438
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto/16 :goto_9

    .line 4422
    .end local v24    # "point2":Landroid/graphics/PointF;
    .end local v25    # "point3":Landroid/graphics/PointF;
    :cond_18
    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4423
    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4425
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_19

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_19

    .line 4426
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4427
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4432
    :goto_11
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4421
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_10

    .line 4429
    :cond_19
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto :goto_11

    .line 4442
    :cond_1a
    const/4 v11, 0x0

    :goto_12
    add-int/lit8 v3, v18, -0x1

    if-ge v11, v3, :cond_5

    .line 4443
    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4444
    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4446
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1b

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1b

    .line 4447
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4448
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4453
    :goto_13
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4442
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_12

    .line 4450
    :cond_1b
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto :goto_13

    .line 4462
    :cond_1c
    const/4 v3, 0x2

    move/from16 v0, v21

    if-ne v0, v3, :cond_1d

    .line 4463
    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4464
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 4466
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4467
    const/4 v3, -0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 4468
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 4469
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 4470
    :cond_1d
    const/4 v3, 0x1

    move/from16 v0, v21

    if-ne v0, v3, :cond_0

    .line 4471
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4472
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    const/4 v4, 0x0

    aget-object v4, v16, v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v5

    .line 4473
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    float-to-int v5, v5

    int-to-float v5, v5

    .line 4472
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v3, v4, v5, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 4476
    .end local v10    # "controlPoint":Landroid/graphics/PointF;
    .end local v17    # "newList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    .end local v18    # "newSize":I
    .end local v19    # "nextPoint":Landroid/graphics/PointF;
    .end local v20    # "orgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    .end local v21    # "orgSize":I
    .end local v30    # "startPoint":Landroid/graphics/PointF;
    :cond_1e
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    const/4 v4, 0x0

    aget-object v4, v16, v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->setLastPoint(FF)V

    .line 4477
    const/4 v11, 0x0

    :goto_14
    if-lt v11, v12, :cond_1f

    .line 4480
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 4482
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4483
    const/4 v3, -0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 4484
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 4485
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 4478
    :cond_1f
    aget-object v3, v16, v11

    iget v3, v3, Landroid/graphics/PointF;->x:F

    aget-object v4, v16, v11

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4477
    add-int/lit8 v11, v11, 0x1

    goto :goto_14
.end method

.method private findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F
    .locals 7
    .param p1, "p1"    # Landroid/graphics/PointF;
    .param p2, "oppo_p1"    # Landroid/graphics/PointF;

    .prologue
    .line 2112
    iget v5, p1, Landroid/graphics/PointF;->x:F

    iget v6, p2, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_0

    iget v1, p1, Landroid/graphics/PointF;->x:F

    .line 2113
    .local v1, "left":F
    :goto_0
    iget v5, p1, Landroid/graphics/PointF;->x:F

    iget v6, p2, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    iget v3, p1, Landroid/graphics/PointF;->x:F

    .line 2114
    .local v3, "right":F
    :goto_1
    iget v5, p1, Landroid/graphics/PointF;->y:F

    iget v6, p2, Landroid/graphics/PointF;->y:F

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_2

    iget v4, p1, Landroid/graphics/PointF;->y:F

    .line 2115
    .local v4, "top":F
    :goto_2
    iget v5, p1, Landroid/graphics/PointF;->y:F

    iget v6, p2, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_3

    iget v0, p1, Landroid/graphics/PointF;->y:F

    .line 2117
    .local v0, "bottom":F
    :goto_3
    const/4 v5, 0x4

    new-array v2, v5, [F

    const/4 v5, 0x0

    aput v1, v2, v5

    const/4 v5, 0x1

    aput v4, v2, v5

    const/4 v5, 0x2

    aput v3, v2, v5

    const/4 v5, 0x3

    aput v0, v2, v5

    .line 2119
    .local v2, "ret":[F
    return-object v2

    .line 2112
    .end local v0    # "bottom":F
    .end local v1    # "left":F
    .end local v2    # "ret":[F
    .end local v3    # "right":F
    .end local v4    # "top":F
    :cond_0
    iget v1, p2, Landroid/graphics/PointF;->x:F

    goto :goto_0

    .line 2113
    .restart local v1    # "left":F
    :cond_1
    iget v3, p2, Landroid/graphics/PointF;->x:F

    goto :goto_1

    .line 2114
    .restart local v3    # "right":F
    :cond_2
    iget v4, p2, Landroid/graphics/PointF;->y:F

    goto :goto_2

    .line 2115
    .restart local v4    # "top":F
    :cond_3
    iget v0, p2, Landroid/graphics/PointF;->y:F

    goto :goto_3
.end method

.method private findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F
    .locals 7
    .param p1, "topleft"    # Landroid/graphics/PointF;
    .param p2, "topright"    # Landroid/graphics/PointF;
    .param p3, "bottomleft"    # Landroid/graphics/PointF;
    .param p4, "bottomright"    # Landroid/graphics/PointF;

    .prologue
    .line 2088
    iget v5, p1, Landroid/graphics/PointF;->x:F

    iget v6, p2, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_8

    iget v1, p1, Landroid/graphics/PointF;->x:F

    .line 2089
    .local v1, "left":F
    :goto_0
    iget v5, p3, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v5, v1

    if-gtz v5, :cond_0

    iget v1, p3, Landroid/graphics/PointF;->x:F

    .line 2090
    :cond_0
    iget v5, p4, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v5, v1

    if-gtz v5, :cond_1

    iget v1, p4, Landroid/graphics/PointF;->x:F

    .line 2092
    :cond_1
    iget v5, p1, Landroid/graphics/PointF;->x:F

    iget v6, p2, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_9

    iget v3, p1, Landroid/graphics/PointF;->x:F

    .line 2093
    .local v3, "right":F
    :goto_1
    iget v5, p3, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v3

    if-ltz v5, :cond_2

    iget v3, p3, Landroid/graphics/PointF;->x:F

    .line 2094
    :cond_2
    iget v5, p4, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v3

    if-ltz v5, :cond_3

    iget v3, p4, Landroid/graphics/PointF;->x:F

    .line 2096
    :cond_3
    iget v5, p1, Landroid/graphics/PointF;->y:F

    iget v6, p2, Landroid/graphics/PointF;->y:F

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_a

    iget v4, p1, Landroid/graphics/PointF;->y:F

    .line 2097
    .local v4, "top":F
    :goto_2
    iget v5, p3, Landroid/graphics/PointF;->y:F

    cmpg-float v5, v5, v4

    if-gtz v5, :cond_4

    iget v4, p3, Landroid/graphics/PointF;->y:F

    .line 2098
    :cond_4
    iget v5, p4, Landroid/graphics/PointF;->y:F

    cmpg-float v5, v5, v4

    if-gtz v5, :cond_5

    iget v4, p4, Landroid/graphics/PointF;->y:F

    .line 2100
    :cond_5
    iget v5, p1, Landroid/graphics/PointF;->y:F

    iget v6, p2, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_b

    iget v0, p1, Landroid/graphics/PointF;->y:F

    .line 2101
    .local v0, "bottom":F
    :goto_3
    iget v5, p3, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v0

    if-ltz v5, :cond_6

    iget v0, p3, Landroid/graphics/PointF;->y:F

    .line 2102
    :cond_6
    iget v5, p4, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v0

    if-ltz v5, :cond_7

    iget v0, p4, Landroid/graphics/PointF;->y:F

    .line 2104
    :cond_7
    const/4 v5, 0x4

    new-array v2, v5, [F

    const/4 v5, 0x0

    aput v1, v2, v5

    const/4 v5, 0x1

    aput v4, v2, v5

    const/4 v5, 0x2

    aput v3, v2, v5

    const/4 v5, 0x3

    aput v0, v2, v5

    .line 2106
    .local v2, "ret":[F
    return-object v2

    .line 2088
    .end local v0    # "bottom":F
    .end local v1    # "left":F
    .end local v2    # "ret":[F
    .end local v3    # "right":F
    .end local v4    # "top":F
    :cond_8
    iget v1, p2, Landroid/graphics/PointF;->x:F

    goto :goto_0

    .line 2092
    .restart local v1    # "left":F
    :cond_9
    iget v3, p2, Landroid/graphics/PointF;->x:F

    goto :goto_1

    .line 2096
    .restart local v3    # "right":F
    :cond_a
    iget v4, p2, Landroid/graphics/PointF;->y:F

    goto :goto_2

    .line 2100
    .restart local v4    # "top":F
    :cond_b
    iget v0, p2, Landroid/graphics/PointF;->y:F

    goto :goto_3
.end method

.method private findResizeRate(Landroid/graphics/PointF;Landroid/graphics/RectF;)Landroid/graphics/PointF;
    .locals 5
    .param p1, "diff_rotated"    # Landroid/graphics/PointF;
    .param p2, "rectStart"    # Landroid/graphics/RectF;

    .prologue
    const v4, 0x38d1b717    # 1.0E-4f

    .line 959
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    .line 960
    .local v1, "resizeRate":Landroid/graphics/PointF;
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 961
    .local v2, "width":F
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 962
    const/high16 v2, 0x3f800000    # 1.0f

    .line 964
    :cond_0
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v0

    .line 965
    .local v0, "height":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    .line 966
    const/high16 v0, 0x3f800000    # 1.0f

    .line 969
    :cond_1
    iget v3, p1, Landroid/graphics/PointF;->x:F

    div-float/2addr v3, v2

    iput v3, v1, Landroid/graphics/PointF;->x:F

    .line 970
    iget v3, p1, Landroid/graphics/PointF;->y:F

    div-float/2addr v3, v0

    iput v3, v1, Landroid/graphics/PointF;->y:F

    .line 971
    return-object v1
.end method

.method private fit(ILcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 8
    .param p1, "index"    # I
    .param p2, "coordinateInfo"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .prologue
    .line 718
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v3

    if-nez v3, :cond_0

    .line 728
    :goto_0
    return-void

    .line 722
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 723
    .local v0, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 725
    .local v1, "objectRect":Landroid/graphics/RectF;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    .line 726
    .local v2, "rect":Landroid/graphics/RectF;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v2, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 727
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v4, v2, Landroid/graphics/RectF;->left:F

    iget v5, v2, Landroid/graphics/RectF;->top:F

    iget v6, v2, Landroid/graphics/RectF;->right:F

    iget v7, v2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0
.end method

.method private getBorderAngle(I)F
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 783
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 784
    :cond_0
    const/4 v0, 0x0

    .line 787
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v0

    goto :goto_0
.end method

.method private getBorderRect(I)Landroid/graphics/RectF;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 767
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 768
    const/4 v0, 0x0

    .line 779
    :goto_0
    return-object v0

    .line 771
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 773
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v1, :cond_1

    .line 774
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 777
    :cond_1
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 778
    .local v0, "relativeRect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    goto :goto_0
.end method

.method private getFixedPoint(ILandroid/graphics/RectF;)Landroid/graphics/PointF;
    .locals 2
    .param p1, "touchZone"    # I
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 1714
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1715
    .local v0, "result":Landroid/graphics/PointF;
    packed-switch p1, :pswitch_data_0

    .line 1754
    :goto_0
    :pswitch_0
    return-object v0

    .line 1717
    :pswitch_1
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1718
    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1721
    :pswitch_2
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1722
    iget v1, p2, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1725
    :pswitch_3
    iget v1, p2, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1726
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1729
    :pswitch_4
    iget v1, p2, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1730
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1733
    :pswitch_5
    iget v1, p2, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1734
    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1737
    :pswitch_6
    iget v1, p2, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1738
    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1741
    :pswitch_7
    iget v1, p2, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1742
    iget v1, p2, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1745
    :pswitch_8
    iget v1, p2, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1746
    iget v1, p2, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1749
    :pswitch_9
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1750
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1715
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_9
        :pswitch_0
        :pswitch_6
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_8
        :pswitch_2
        :pswitch_7
    .end packed-switch
.end method

.method private getOffsetWhenRotation(ILandroid/graphics/RectF;Landroid/graphics/RectF;F)Landroid/graphics/PointF;
    .locals 14
    .param p1, "touchZone"    # I
    .param p2, "srcRect"    # Landroid/graphics/RectF;
    .param p3, "dscRect"    # Landroid/graphics/RectF;
    .param p4, "degree"    # F

    .prologue
    .line 1697
    new-instance v11, Landroid/graphics/PointF;

    invoke-direct {v11}, Landroid/graphics/PointF;-><init>()V

    .line 1699
    .local v11, "offset":Landroid/graphics/PointF;
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getFixedPoint(ILandroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v13

    .line 1700
    .local v13, "srcTouchPoint":Landroid/graphics/PointF;
    iget v3, v13, Landroid/graphics/PointF;->x:F

    float-to-int v4, v3

    iget v3, v13, Landroid/graphics/PointF;->y:F

    float-to-int v5, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v6

    .line 1701
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerY()F

    move-result v7

    move/from16 v0, p4

    float-to-double v8, v0

    move-object v3, p0

    .line 1700
    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v10

    .line 1703
    .local v10, "newTouchPoint":Landroid/graphics/PointF;
    move-object/from16 v0, p3

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getFixedPoint(ILandroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v12

    .line 1704
    .local v12, "resizeTouchPoint":Landroid/graphics/PointF;
    iget v3, v12, Landroid/graphics/PointF;->x:F

    float-to-int v4, v3

    iget v3, v12, Landroid/graphics/PointF;->y:F

    float-to-int v5, v3

    .line 1705
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->centerX()F

    move-result v6

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->centerY()F

    move-result v7

    move/from16 v0, p4

    float-to-double v8, v0

    move-object v3, p0

    .line 1704
    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v2

    .line 1707
    .local v2, "newResizeTouchPoint":Landroid/graphics/PointF;
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    iput v3, v11, Landroid/graphics/PointF;->x:F

    .line 1708
    iget v3, v10, Landroid/graphics/PointF;->y:F

    iget v4, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    iput v3, v11, Landroid/graphics/PointF;->y:F

    .line 1710
    return-object v11
.end method

.method private getRotateable(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 791
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 792
    :cond_0
    const/4 v0, 0x0

    .line 795
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v0

    goto :goto_0
.end method

.method private hideMovingView()V
    .locals 2

    .prologue
    .line 1457
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    if-eqz v0, :cond_0

    .line 1458
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->setVisibility(I)V

    .line 1460
    :cond_0
    return-void
.end method

.method private initialize(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 10
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    const/16 v9, 0xa0

    const/4 v8, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 805
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 806
    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 807
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    .line 808
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    .line 810
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    .line 811
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    .line 812
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    .line 813
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    .line 814
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    .line 815
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRect:Landroid/graphics/Rect;

    .line 816
    new-instance v4, Ljava/util/Hashtable;

    invoke-direct {v4}, Ljava/util/Hashtable;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mResourceMap:Ljava/util/Map;

    .line 817
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .line 818
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTransactionTouchEvent:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

    .line 819
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTrivialMovingEn:Z

    .line 820
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchEnable:Z

    .line 821
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    .line 822
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 823
    .local v0, "localDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    .line 825
    const/high16 v4, 0x41200000    # 10.0f

    invoke-static {v6, v4, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    .line 826
    .local v3, "minResizeEdge":F
    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    invoke-static {v6, v4, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    mul-float v2, v4, v7

    .line 829
    .local v2, "maxResizeWidth":F
    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    .line 828
    invoke-static {v6, v4, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    mul-float v1, v4, v7

    .line 830
    .local v1, "maxResizeHeight":F
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v5, v5, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    .line 831
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v5, v5, v2, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMaximumResizeRect:Landroid/graphics/RectF;

    .line 833
    iget v4, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v4, v9, :cond_0

    .line 834
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mDensityDpi:I

    .line 839
    :goto_0
    const/4 v4, 0x0

    invoke-virtual {p0, v6, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setLayerType(ILandroid/graphics/Paint;)V

    .line 841
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;-><init>(F)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    .line 842
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsObjectChange:Z

    .line 843
    return-void

    .line 836
    :cond_0
    const/16 v4, 0x140

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mDensityDpi:I

    goto :goto_0
.end method

.method private isClippedObjectMovingOutsideFrameRect(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 5
    .param p1, "topleft"    # Landroid/graphics/PointF;
    .param p2, "topright"    # Landroid/graphics/PointF;
    .param p3, "bottomleft"    # Landroid/graphics/PointF;
    .param p4, "bottomright"    # Landroid/graphics/PointF;

    .prologue
    const v4, 0x3951b717    # 2.0E-4f

    const v3, 0x38d1b717    # 1.0E-4f

    const/4 v1, 0x1

    .line 2125
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 2126
    .local v0, "outsideFrameRect":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 2127
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 2128
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v4

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 2129
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, v4

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 2131
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    packed-switch v2, :pswitch_data_0

    .line 2179
    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1

    .line 2133
    :pswitch_0
    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2134
    iget v2, p3, Landroid/graphics/PointF;->x:F

    iget v3, p3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 2139
    :pswitch_1
    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2140
    iget v2, p4, Landroid/graphics/PointF;->x:F

    iget v3, p4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 2145
    :pswitch_2
    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 2150
    :pswitch_3
    iget v2, p3, Landroid/graphics/PointF;->x:F

    iget v3, p3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2151
    iget v2, p4, Landroid/graphics/PointF;->x:F

    iget v3, p4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 2156
    :pswitch_4
    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 2161
    :pswitch_5
    iget v2, p3, Landroid/graphics/PointF;->x:F

    iget v3, p3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 2166
    :pswitch_6
    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 2171
    :pswitch_7
    iget v2, p4, Landroid/graphics/PointF;->x:F

    iget v3, p4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 2131
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_7
    .end packed-switch
.end method

.method private isContained(Ljava/util/ArrayList;Landroid/graphics/PointF;)Z
    .locals 5
    .param p2, "point"    # Landroid/graphics/PointF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;",
            "Landroid/graphics/PointF;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "arr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    const v4, 0x38d1b717    # 1.0E-4f

    .line 4490
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 4495
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 4490
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 4491
    .local v0, "pointF":Landroid/graphics/PointF;
    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_0

    iget v2, v0, Landroid/graphics/PointF;->y:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_0

    .line 4492
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isMovable()Z
    .locals 3

    .prologue
    .line 866
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 871
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 866
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 867
    .local v0, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isMovable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 868
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isObjectAvailable()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 731
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 737
    :cond_0
    :goto_0
    return v0

    .line 734
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 737
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isOutOfViewEnabled()Z
    .locals 3

    .prologue
    .line 857
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 862
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 857
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 858
    .local v0, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 859
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isRotatable()Z
    .locals 3

    .prologue
    .line 846
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    if-nez v1, :cond_1

    .line 847
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 853
    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 847
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 848
    .local v0, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 849
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private normalizeDegree(F)F
    .locals 4
    .param p1, "degrees"    # F

    .prologue
    const/high16 v3, -0x3c4c0000    # -360.0f

    const/high16 v2, 0x43b40000    # 360.0f

    .line 925
    move v0, p1

    .line 926
    .local v0, "result":F
    cmpl-float v1, v3, p1

    if-ltz v1, :cond_0

    .line 927
    add-float v0, p1, v2

    .line 929
    :cond_0
    cmpg-float v1, v2, p1

    if-gtz v1, :cond_1

    .line 930
    sub-float v0, p1, v2

    .line 932
    :cond_1
    cmpl-float v1, v3, v0

    if-gez v1, :cond_2

    cmpg-float v1, v2, v0

    if-gtz v1, :cond_3

    .line 933
    :cond_2
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v0

    .line 935
    :cond_3
    return v0
.end method

.method private resize(ILandroid/graphics/PointF;)V
    .locals 13
    .param p1, "index"    # I
    .param p2, "diff_rotated"    # Landroid/graphics/PointF;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    .line 975
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    .line 976
    .local v4, "tmp":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 977
    .local v2, "preRectF":Landroid/graphics/RectF;
    iget v6, v4, Landroid/graphics/RectF;->left:F

    iget v7, v4, Landroid/graphics/RectF;->top:F

    iget v8, v4, Landroid/graphics/RectF;->right:F

    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 984
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    packed-switch v6, :pswitch_data_0

    .line 1066
    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1067
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v6

    invoke-direct {p0, v4, p1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->checkAllRectOutOfCanvas(Landroid/graphics/RectF;IF)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1068
    iget v6, v2, Landroid/graphics/RectF;->left:F

    iget v7, v2, Landroid/graphics/RectF;->top:F

    iget v8, v2, Landroid/graphics/RectF;->right:F

    iget v9, v2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1070
    :cond_1
    return-void

    .line 986
    :pswitch_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeL2R(ILandroid/graphics/PointF;)V

    .line 987
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeT2B(ILandroid/graphics/PointF;)V

    goto :goto_0

    .line 991
    :pswitch_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    .line 992
    .local v3, "rectf":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    .line 993
    .local v0, "center":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 994
    .local v1, "height":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 995
    .local v5, "width":F
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeT2B(ILandroid/graphics/PointF;)V

    .line 996
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v6

    if-ne v6, v12, :cond_0

    .line 997
    cmpl-float v6, v1, v11

    if-eqz v6, :cond_0

    .line 1000
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v5

    mul-float v7, v10, v1

    div-float/2addr v6, v7

    sub-float v6, v0, v6

    iput v6, v3, Landroid/graphics/RectF;->left:F

    .line 1001
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v5

    mul-float v7, v10, v1

    div-float/2addr v6, v7

    add-float/2addr v6, v0

    iput v6, v3, Landroid/graphics/RectF;->right:F

    goto :goto_0

    .line 1006
    .end local v0    # "center":F
    .end local v1    # "height":F
    .end local v3    # "rectf":Landroid/graphics/RectF;
    .end local v5    # "width":F
    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeR2L(ILandroid/graphics/PointF;)V

    .line 1007
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeT2B(ILandroid/graphics/PointF;)V

    goto :goto_0

    .line 1011
    :pswitch_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    .line 1012
    .restart local v3    # "rectf":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    .line 1013
    .restart local v0    # "center":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 1014
    .restart local v1    # "height":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 1015
    .restart local v5    # "width":F
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeL2R(ILandroid/graphics/PointF;)V

    .line 1016
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v6

    if-ne v6, v12, :cond_0

    .line 1017
    cmpl-float v6, v5, v11

    if-eqz v6, :cond_0

    .line 1020
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v1

    mul-float v7, v10, v5

    div-float/2addr v6, v7

    sub-float v6, v0, v6

    iput v6, v3, Landroid/graphics/RectF;->top:F

    .line 1021
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v1

    mul-float v7, v10, v5

    div-float/2addr v6, v7

    add-float/2addr v6, v0

    iput v6, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 1025
    .end local v0    # "center":F
    .end local v1    # "height":F
    .end local v3    # "rectf":Landroid/graphics/RectF;
    .end local v5    # "width":F
    :pswitch_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    .line 1026
    .restart local v3    # "rectf":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    .line 1027
    .restart local v0    # "center":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 1028
    .restart local v1    # "height":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 1029
    .restart local v5    # "width":F
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeR2L(ILandroid/graphics/PointF;)V

    .line 1030
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v6

    if-ne v6, v12, :cond_0

    .line 1031
    cmpl-float v6, v5, v11

    if-eqz v6, :cond_0

    .line 1034
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v1

    mul-float v7, v10, v5

    div-float/2addr v6, v7

    sub-float v6, v0, v6

    iput v6, v3, Landroid/graphics/RectF;->top:F

    .line 1035
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v1

    mul-float v7, v10, v5

    div-float/2addr v6, v7

    add-float/2addr v6, v0

    iput v6, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 1039
    .end local v0    # "center":F
    .end local v1    # "height":F
    .end local v3    # "rectf":Landroid/graphics/RectF;
    .end local v5    # "width":F
    :pswitch_5
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeL2R(ILandroid/graphics/PointF;)V

    .line 1040
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeB2T(ILandroid/graphics/PointF;)V

    goto/16 :goto_0

    .line 1044
    :pswitch_6
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    .line 1045
    .restart local v3    # "rectf":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    .line 1046
    .restart local v0    # "center":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 1047
    .restart local v1    # "height":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 1048
    .restart local v5    # "width":F
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeB2T(ILandroid/graphics/PointF;)V

    .line 1049
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v6

    if-ne v6, v12, :cond_0

    .line 1050
    cmpl-float v6, v1, v11

    if-eqz v6, :cond_0

    .line 1053
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v5

    mul-float v7, v10, v1

    div-float/2addr v6, v7

    sub-float v6, v0, v6

    iput v6, v3, Landroid/graphics/RectF;->left:F

    .line 1054
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v5

    mul-float v7, v10, v1

    div-float/2addr v6, v7

    add-float/2addr v6, v0

    iput v6, v3, Landroid/graphics/RectF;->right:F

    goto/16 :goto_0

    .line 1058
    .end local v0    # "center":F
    .end local v1    # "height":F
    .end local v3    # "rectf":Landroid/graphics/RectF;
    .end local v5    # "width":F
    :pswitch_7
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeR2L(ILandroid/graphics/PointF;)V

    .line 1059
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeB2T(ILandroid/graphics/PointF;)V

    goto/16 :goto_0

    .line 984
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private resize2Threshold(I)Z
    .locals 21
    .param p1, "objID"    # I

    .prologue
    .line 1547
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/RectF;

    .line 1549
    .local v11, "rectf":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v13

    .line 1551
    .local v13, "srcRect":Landroid/graphics/RectF;
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const v19, 0x38d1b717    # 1.0E-4f

    cmpg-float v18, v18, v19

    if-ltz v18, :cond_0

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const v19, 0x38d1b717    # 1.0E-4f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_1

    .line 1552
    :cond_0
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->left:F

    .line 1553
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->right:F

    .line 1554
    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    .line 1555
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->top:F

    .line 1556
    const/16 v18, 0x0

    .line 1693
    :goto_0
    return v18

    .line 1559
    :cond_1
    const/4 v4, 0x0

    .line 1560
    .local v4, "isNotKeepRatio":Z
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v18

    const v19, 0x3f800347    # 1.0001f

    cmpg-float v18, v18, v19

    if-lez v18, :cond_2

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v18

    const v19, 0x3f800347    # 1.0001f

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_3

    .line 1561
    :cond_2
    const/4 v4, 0x1

    .line 1564
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->Increase2MinimumRect(Landroid/graphics/RectF;)Z

    .line 1566
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v19

    div-float v10, v18, v19

    .line 1567
    .local v10, "ratio":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMaximumResizeRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v19, v0

    mul-float v6, v18, v19

    .line 1568
    .local v6, "maxWidth":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMaximumResizeRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v19, v0

    mul-float v5, v18, v19

    .line 1570
    .local v5, "maxHeight":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v19, v0

    mul-float v8, v18, v19

    .line 1571
    .local v8, "minWidth":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v19, v0

    mul-float v7, v18, v19

    .line 1573
    .local v7, "minHeight":F
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpg-float v18, v18, v6

    if-gez v18, :cond_4

    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v8

    if-lez v18, :cond_4

    .line 1574
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v7

    if-lez v18, :cond_4

    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpg-float v18, v18, v5

    if-gez v18, :cond_4

    .line 1575
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 1578
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_5

    .line 1579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v18

    if-eqz v18, :cond_6

    if-nez v4, :cond_6

    .line 1580
    :cond_5
    mul-float v18, v10, v5

    cmpl-float v18, v6, v18

    if-lez v18, :cond_c

    mul-float v16, v10, v5

    .line 1581
    .local v16, "tmpMaxWidth":F
    :goto_1
    cmpg-float v18, v16, v6

    if-gez v18, :cond_d

    .line 1582
    move/from16 v6, v16

    .line 1587
    :goto_2
    mul-float v18, v10, v7

    cmpg-float v18, v8, v18

    if-gez v18, :cond_e

    mul-float v17, v10, v7

    .line 1588
    .local v17, "tmpMinWidth":F
    :goto_3
    cmpl-float v18, v17, v8

    if-lez v18, :cond_f

    .line 1589
    move/from16 v8, v17

    .line 1595
    .end local v16    # "tmpMaxWidth":F
    .end local v17    # "tmpMinWidth":F
    :cond_6
    :goto_4
    const/4 v12, 0x1

    .line 1596
    .local v12, "sign":I
    const/4 v3, 0x0

    .line 1598
    .local v3, "isEdgeThreshold":Z
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v6

    if-lez v18, :cond_10

    move v15, v6

    .line 1599
    .local v15, "thresholdW":F
    :goto_5
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v5

    if-lez v18, :cond_11

    move v14, v5

    .line 1601
    .local v14, "thresholdH":F
    :goto_6
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v6

    if-gtz v18, :cond_7

    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpg-float v18, v18, v8

    if-gez v18, :cond_8

    .line 1602
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_0

    .line 1630
    :goto_7
    :pswitch_0
    const/4 v3, 0x1

    .line 1632
    :cond_8
    const/4 v12, 0x1

    .line 1633
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v5

    if-gtz v18, :cond_9

    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpg-float v18, v18, v7

    if-gez v18, :cond_a

    .line 1634
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_1

    .line 1662
    :goto_8
    :pswitch_1
    const/4 v3, 0x1

    .line 1665
    :cond_a
    if-eqz v3, :cond_b

    .line 1666
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_b

    .line 1667
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_2

    .line 1689
    :cond_b
    :goto_9
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1690
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v18

    .line 1689
    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-direct {v0, v1, v13, v11, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getOffsetWhenRotation(ILandroid/graphics/RectF;Landroid/graphics/RectF;F)Landroid/graphics/PointF;

    move-result-object v9

    .line 1691
    .local v9, "offset":Landroid/graphics/PointF;
    iget v0, v9, Landroid/graphics/PointF;->x:F

    move/from16 v18, v0

    iget v0, v9, Landroid/graphics/PointF;->y:F

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 1693
    const/16 v18, 0x1

    goto/16 :goto_0

    .end local v3    # "isEdgeThreshold":Z
    .end local v9    # "offset":Landroid/graphics/PointF;
    .end local v12    # "sign":I
    .end local v14    # "thresholdH":F
    .end local v15    # "thresholdW":F
    :cond_c
    move/from16 v16, v6

    .line 1580
    goto/16 :goto_1

    .line 1584
    .restart local v16    # "tmpMaxWidth":F
    :cond_d
    div-float v5, v6, v10

    goto/16 :goto_2

    :cond_e
    move/from16 v17, v8

    .line 1587
    goto/16 :goto_3

    .line 1591
    .restart local v17    # "tmpMinWidth":F
    :cond_f
    div-float v7, v8, v10

    goto/16 :goto_4

    .end local v16    # "tmpMaxWidth":F
    .end local v17    # "tmpMinWidth":F
    .restart local v3    # "isEdgeThreshold":Z
    .restart local v12    # "sign":I
    :cond_10
    move v15, v8

    .line 1598
    goto/16 :goto_5

    .restart local v15    # "thresholdW":F
    :cond_11
    move v14, v7

    .line 1599
    goto/16 :goto_6

    .line 1606
    .restart local v14    # "thresholdH":F
    :pswitch_3
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_12

    .line 1607
    const/4 v12, -0x1

    .line 1609
    :cond_12
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v18, v0

    int-to-float v0, v12

    move/from16 v19, v0

    mul-float v19, v19, v15

    sub-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->left:F

    .line 1610
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->right:F

    goto/16 :goto_7

    .line 1615
    :pswitch_4
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_13

    .line 1616
    const/4 v12, -0x1

    .line 1618
    :cond_13
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    int-to-float v0, v12

    move/from16 v19, v0

    mul-float v19, v19, v15

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->right:F

    .line 1619
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->left:F

    goto/16 :goto_7

    .line 1622
    :pswitch_5
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_14

    .line 1623
    const/4 v12, -0x1

    .line 1625
    :cond_14
    iget v0, v11, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    int-to-float v0, v12

    move/from16 v19, v0

    mul-float v19, v19, v15

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->right:F

    goto/16 :goto_7

    .line 1636
    :pswitch_6
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_15

    .line 1637
    const/4 v12, -0x1

    .line 1639
    :cond_15
    iget v0, v11, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    int-to-float v0, v12

    move/from16 v19, v0

    mul-float v19, v19, v14

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_8

    .line 1644
    :pswitch_7
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_16

    .line 1645
    const/4 v12, -0x1

    .line 1647
    :cond_16
    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    .line 1648
    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    int-to-float v0, v12

    move/from16 v19, v0

    mul-float v19, v19, v14

    sub-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->top:F

    goto/16 :goto_8

    .line 1653
    :pswitch_8
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_17

    .line 1654
    const/4 v12, -0x1

    .line 1656
    :cond_17
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    int-to-float v0, v12

    move/from16 v19, v0

    mul-float v19, v19, v14

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    .line 1657
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->top:F

    goto/16 :goto_8

    .line 1669
    :pswitch_9
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    add-float v18, v18, v19

    const/high16 v19, 0x40000000    # 2.0f

    div-float v19, v15, v19

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->right:F

    .line 1670
    iget v0, v11, Landroid/graphics/RectF;->right:F

    move/from16 v18, v0

    sub-float v18, v18, v15

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    .line 1673
    :pswitch_a
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    add-float v18, v18, v19

    const/high16 v19, 0x40000000    # 2.0f

    div-float v19, v14, v19

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    .line 1674
    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    sub-float v18, v18, v14

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    .line 1677
    :pswitch_b
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    add-float v18, v18, v19

    const/high16 v19, 0x40000000    # 2.0f

    div-float v19, v14, v19

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    .line 1678
    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    sub-float v18, v18, v14

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    .line 1681
    :pswitch_c
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    add-float v18, v18, v19

    const/high16 v19, 0x40000000    # 2.0f

    div-float v19, v15, v19

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->right:F

    .line 1682
    iget v0, v11, Landroid/graphics/RectF;->right:F

    move/from16 v18, v0

    sub-float v18, v18, v15

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    .line 1602
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 1634
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_6
        :pswitch_1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_1
        :pswitch_1
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch

    .line 1667
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_9
        :pswitch_2
        :pswitch_a
        :pswitch_b
        :pswitch_2
        :pswitch_c
    .end packed-switch
.end method

.method private resizeB2T(ILandroid/graphics/PointF;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "diff_rotated"    # Landroid/graphics/PointF;

    .prologue
    .line 954
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 955
    .local v0, "rectF":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, p2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 956
    return-void
.end method

.method private resizeImage(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;
    .locals 19
    .param p1, "drawableName"    # Ljava/lang/String;
    .param p2, "iconWidth"    # I
    .param p3, "iconHeight"    # I

    .prologue
    .line 3232
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    .line 3233
    .local v13, "manager":Landroid/content/pm/PackageManager;
    const/4 v11, 0x0

    .line 3235
    .local v11, "mApk1Resources":Landroid/content/res/Resources;
    :try_start_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v11

    .line 3236
    const-string v3, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    .line 3238
    .local v12, "mDrawableResID":I
    if-eqz v12, :cond_1

    .line 3240
    invoke-static {v11, v12}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 3241
    .local v2, "BitmapOrg":Landroid/graphics/Bitmap;
    if-nez v2, :cond_0

    .line 3242
    const/4 v3, 0x0

    .line 3272
    .end local v2    # "BitmapOrg":Landroid/graphics/Bitmap;
    .end local v12    # "mDrawableResID":I
    :goto_0
    return-object v3

    .line 3245
    .restart local v2    # "BitmapOrg":Landroid/graphics/Bitmap;
    .restart local v12    # "mDrawableResID":I
    :cond_0
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 3246
    .local v5, "width":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 3248
    .local v6, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    .line 3249
    .local v9, "dm":Landroid/util/DisplayMetrics;
    const/4 v3, 0x1

    move/from16 v0, p2

    int-to-float v4, v0

    invoke-static {v3, v4, v9}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v15, v3

    .line 3250
    .local v15, "newWidth":I
    const/4 v3, 0x1

    move/from16 v0, p3

    int-to-float v4, v0

    invoke-static {v3, v4, v9}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v14, v3

    .line 3253
    .local v14, "newHeight":I
    int-to-float v3, v15

    int-to-float v4, v5

    div-float v18, v3, v4

    .line 3254
    .local v18, "scaleWidth":F
    int-to-float v3, v14

    int-to-float v4, v6

    div-float v17, v3, v4

    .line 3257
    .local v17, "scaleHeight":F
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 3259
    .local v7, "matrix":Landroid/graphics/Matrix;
    move/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 3262
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 3266
    .local v16, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, v16

    invoke-direct {v3, v11, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3268
    .end local v2    # "BitmapOrg":Landroid/graphics/Bitmap;
    .end local v5    # "width":I
    .end local v6    # "height":I
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v9    # "dm":Landroid/util/DisplayMetrics;
    .end local v12    # "mDrawableResID":I
    .end local v14    # "newHeight":I
    .end local v15    # "newWidth":I
    .end local v16    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v17    # "scaleHeight":F
    .end local v18    # "scaleWidth":F
    :catch_0
    move-exception v10

    .line 3269
    .local v10, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v10}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 3272
    .end local v10    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private resizeL2R(ILandroid/graphics/PointF;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "diff_rotated"    # Landroid/graphics/PointF;

    .prologue
    .line 939
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 940
    .local v0, "rectF":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 941
    return-void
.end method

.method private resizeR2L(ILandroid/graphics/PointF;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "diff_rotated"    # Landroid/graphics/PointF;

    .prologue
    .line 944
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 945
    .local v0, "rectF":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget v2, p2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 946
    return-void
.end method

.method private resizeT2B(ILandroid/graphics/PointF;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "diff_rotated"    # Landroid/graphics/PointF;

    .prologue
    .line 949
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 950
    .local v0, "rectF":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget v2, p2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 951
    return-void
.end method

.method private rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 4
    .param p1, "px"    # F
    .param p2, "py"    # F
    .param p3, "angle"    # F
    .param p4, "input"    # Landroid/graphics/PointF;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2069
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    if-nez v1, :cond_0

    .line 2070
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    .line 2073
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, p3, p1, p2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 2075
    const/4 v1, 0x2

    new-array v0, v1, [F

    .line 2076
    .local v0, "pts":[F
    iget v1, p4, Landroid/graphics/PointF;->x:F

    aput v1, v0, v2

    .line 2077
    iget v1, p4, Landroid/graphics/PointF;->y:F

    aput v1, v0, v3

    .line 2079
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 2081
    new-instance v1, Landroid/graphics/PointF;

    aget v2, v0, v2

    aget v3, v0, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v1
.end method


# virtual methods
.method protected absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/RectF;
    .param p3, "coordinateInfo"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .prologue
    .line 741
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 751
    :cond_0
    :goto_0
    return-void

    .line 745
    :cond_1
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 746
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    .line 747
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    .line 746
    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 748
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 749
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    .line 750
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    .line 749
    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method protected applyChange()V
    .locals 8

    .prologue
    .line 3395
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-nez v6, :cond_1

    .line 3424
    :cond_0
    return-void

    .line 3399
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 3400
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3401
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 3403
    .local v4, "objectRect":Landroid/graphics/RectF;
    const/4 v5, 0x0

    .line 3404
    .local v5, "rotateDiff":F
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    .line 3405
    .local v1, "index":I
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    .line 3406
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v7

    sub-float v5, v6, v7

    .line 3409
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 3410
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v4, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3411
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 3412
    .local v3, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    const/4 v6, 0x0

    invoke-virtual {v3, v4, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 3414
    const/4 v2, 0x0

    .line 3415
    .local v2, "objAng":F
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 3417
    if-eq v0, v1, :cond_3

    .line 3418
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v6

    add-float/2addr v6, v5

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v2

    .line 3420
    :cond_3
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotateable(I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 3421
    float-to-int v6, v2

    int-to-float v6, v6

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRotation(F)V

    .line 3409
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 3743
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 3744
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 3745
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->close()Z

    .line 3748
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3749
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 3750
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 3753
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_2

    .line 3754
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onClosed(Ljava/util/ArrayList;)V

    .line 3757
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsClosed:Z

    .line 3758
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    .line 3759
    return-void
.end method

.method protected draw8Points(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V
    .locals 12
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "rect"    # Landroid/graphics/RectF;
    .param p4, "isKeepRatio"    # Z

    .prologue
    .line 3104
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    .line 3105
    .local v7, "w":I
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 3107
    .local v3, "h":I
    int-to-float v8, v7

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 3108
    .local v4, "halfX":I
    int-to-float v8, v3

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 3109
    .local v5, "halfY":I
    invoke-virtual {p3}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 3110
    .local v0, "CenterX":I
    invoke-virtual {p3}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 3112
    .local v1, "CenterY":I
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 3113
    .local v6, "tmpRect":Landroid/graphics/Rect;
    invoke-virtual {p3, v6}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 3115
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 3117
    .local v2, "bounds":Landroid/graphics/Rect;
    iget v8, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v4

    iget v9, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v9, v5

    iget v10, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v10, v4

    iget v11, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v11, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3118
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3119
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3122
    iget v8, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v4

    iget v9, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v9, v5

    iget v10, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v10, v4

    iget v11, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v11, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3123
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3124
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3127
    iget v8, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v4

    iget v9, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v5

    iget v10, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v10, v4

    iget v11, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v11, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3128
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3129
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3132
    iget v8, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v4

    iget v9, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v5

    iget v10, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v10, v4

    iget v11, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v11, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3133
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3134
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3136
    instance-of v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    if-nez v8, :cond_1

    .line 3137
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;)F

    move-result v9

    const/high16 v10, 0x40c00000    # 6.0f

    mul-float/2addr v9, v10

    cmpl-float v8, v8, v9

    if-lez v8, :cond_0

    .line 3139
    sub-int v8, v0, v4

    iget v9, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v9, v5

    add-int v10, v0, v4

    iget v11, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v11, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3140
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3141
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3144
    sub-int v8, v0, v4

    iget v9, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v5

    add-int v10, v0, v4

    iget v11, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v11, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3145
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3146
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3149
    :cond_0
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;)F

    move-result v9

    const/high16 v10, 0x40c00000    # 6.0f

    mul-float/2addr v9, v10

    cmpl-float v8, v8, v9

    if-lez v8, :cond_1

    .line 3151
    iget v8, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v4

    sub-int v9, v1, v5

    iget v10, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v10, v4

    add-int v11, v1, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3152
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3153
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3156
    iget v8, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v4

    sub-int v9, v1, v5

    iget v10, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v10, v4

    add-int v11, v1, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3157
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3158
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3161
    :cond_1
    return-void
.end method

.method protected drawHighlightObject(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 4156
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectOutlineEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4165
    .end local p2    # "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :goto_0
    return-void

    .line 4160
    .restart local p2    # "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_0
    instance-of v0, p2, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v0, :cond_1

    .line 4161
    check-cast p2, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .end local p2    # "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->drawHighlightStroke(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    goto :goto_0

    .line 4163
    .restart local p2    # "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->drawHighlightRect(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0
.end method

.method public fit()V
    .locals 3

    .prologue
    .line 3592
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3610
    :cond_0
    :goto_0
    return-void

    .line 3596
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 3597
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v1, :cond_2

    .line 3598
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3601
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_4

    .line 3606
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFirstTouch:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 3607
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->updateContextMenu()V

    .line 3608
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    goto :goto_0

    .line 3602
    :cond_4
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->adjustObjectRect(I)V

    .line 3603
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit(ILcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3601
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected fitRotateAngle2BorderAngle()V
    .locals 2

    .prologue
    .line 3475
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 3476
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 3480
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 3477
    .restart local v0    # "i":I
    :cond_1
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 3476
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected getBoundBox(I)Landroid/graphics/RectF;
    .locals 20
    .param p1, "index"    # I

    .prologue
    .line 1185
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1186
    const/16 v17, 0x0

    .line 1233
    :cond_0
    :goto_0
    return-object v17

    .line 1189
    :cond_1
    const/4 v3, -0x1

    move/from16 v0, p1

    if-ne v0, v3, :cond_2

    .line 1190
    new-instance v17, Landroid/graphics/RectF;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1193
    .local v17, "sumRect":Landroid/graphics/RectF;
    const/4 v10, 0x1

    .local v10, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v10, v3, :cond_0

    .line 1194
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v18

    .line 1195
    .local v18, "tempRect":Landroid/graphics/RectF;
    invoke-virtual/range {v17 .. v18}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 1193
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 1200
    .end local v10    # "i":I
    .end local v17    # "sumRect":Landroid/graphics/RectF;
    .end local v18    # "tempRect":Landroid/graphics/RectF;
    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v6

    .line 1201
    .local v6, "centerX":F
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v7

    .line 1202
    .local v7, "centerY":F
    const/4 v3, 0x4

    new-array v0, v3, [Landroid/graphics/PointF;

    move-object/from16 v16, v0

    .line 1203
    .local v16, "rotatePoint":[Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/graphics/RectF;

    .line 1204
    .local v15, "rectF":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v2

    .line 1205
    .local v2, "degree":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    move/from16 v0, p1

    if-ne v0, v3, :cond_3

    .line 1206
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 1208
    :cond_3
    const/16 v19, 0x0

    iget v3, v15, Landroid/graphics/RectF;->left:F

    float-to-int v4, v3

    iget v3, v15, Landroid/graphics/RectF;->top:F

    float-to-int v5, v3

    float-to-double v8, v2

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v3

    aput-object v3, v16, v19

    .line 1209
    const/16 v19, 0x1

    iget v3, v15, Landroid/graphics/RectF;->right:F

    float-to-int v4, v3

    iget v3, v15, Landroid/graphics/RectF;->top:F

    float-to-int v5, v3

    float-to-double v8, v2

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v3

    aput-object v3, v16, v19

    .line 1210
    const/16 v19, 0x2

    iget v3, v15, Landroid/graphics/RectF;->left:F

    float-to-int v4, v3

    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v3

    float-to-double v8, v2

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v3

    aput-object v3, v16, v19

    .line 1211
    const/16 v19, 0x3

    iget v3, v15, Landroid/graphics/RectF;->right:F

    float-to-int v4, v3

    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v3

    float-to-double v8, v2

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v3

    aput-object v3, v16, v19

    .line 1214
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v12, v3, Landroid/graphics/PointF;->x:F

    .line 1215
    .local v12, "pointX_min":F
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v14, v3, Landroid/graphics/PointF;->y:F

    .line 1216
    .local v14, "pointY_min":F
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v11, v3, Landroid/graphics/PointF;->x:F

    .line 1217
    .local v11, "pointX_max":F
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v13, v3, Landroid/graphics/PointF;->y:F

    .line 1218
    .local v13, "pointY_max":F
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_2
    move-object/from16 v0, v16

    array-length v3, v0

    if-lt v10, v3, :cond_4

    .line 1233
    new-instance v17, Landroid/graphics/RectF;

    move-object/from16 v0, v17

    invoke-direct {v0, v12, v14, v11, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto/16 :goto_0

    .line 1219
    :cond_4
    aget-object v3, v16, v10

    iget v3, v3, Landroid/graphics/PointF;->x:F

    cmpl-float v3, v12, v3

    if-ltz v3, :cond_5

    .line 1220
    aget-object v3, v16, v10

    iget v12, v3, Landroid/graphics/PointF;->x:F

    .line 1222
    :cond_5
    aget-object v3, v16, v10

    iget v3, v3, Landroid/graphics/PointF;->x:F

    cmpg-float v3, v11, v3

    if-gtz v3, :cond_6

    .line 1223
    aget-object v3, v16, v10

    iget v11, v3, Landroid/graphics/PointF;->x:F

    .line 1225
    :cond_6
    aget-object v3, v16, v10

    iget v3, v3, Landroid/graphics/PointF;->y:F

    cmpl-float v3, v14, v3

    if-ltz v3, :cond_7

    .line 1226
    aget-object v3, v16, v10

    iget v14, v3, Landroid/graphics/PointF;->y:F

    .line 1228
    :cond_7
    aget-object v3, v16, v10

    iget v3, v3, Landroid/graphics/PointF;->y:F

    cmpg-float v3, v13, v3

    if-gtz v3, :cond_8

    .line 1229
    aget-object v3, v16, v10

    iget v13, v3, Landroid/graphics/PointF;->y:F

    .line 1218
    :cond_8
    add-int/lit8 v10, v10, 0x1

    goto :goto_2
.end method

.method public getContextMenu()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3887
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getControlPivotX(I)F
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 2054
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2055
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    .line 2058
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    goto :goto_0
.end method

.method protected getControlPivotY(I)F
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 2062
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2063
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    .line 2065
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    goto :goto_0
.end method

.method protected getDrawableImage(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 18
    .param p1, "drawableName"    # Ljava/lang/String;

    .prologue
    .line 1900
    const/4 v8, 0x0

    .line 1901
    .local v8, "drawable":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mResourceMap:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "drawable":Landroid/graphics/drawable/Drawable;
    check-cast v8, Landroid/graphics/drawable/Drawable;

    .line 1902
    .restart local v8    # "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v8, :cond_0

    move-object v1, v8

    .line 1964
    :goto_0
    return-object v1

    .line 1906
    :cond_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 1907
    .local v17, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v15

    .line 1909
    .local v15, "mApk1Resources":Landroid/content/res/Resources;
    const-string v2, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v15, v0, v2, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v16

    .line 1911
    .local v16, "mDrawableResID":I
    if-eqz v16, :cond_2

    .line 1912
    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 1913
    if-eqz v8, :cond_1

    .line 1914
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mResourceMap:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .end local v15    # "mApk1Resources":Landroid/content/res/Resources;
    .end local v16    # "mDrawableResID":I
    .end local v17    # "manager":Landroid/content/pm/PackageManager;
    :cond_1
    :goto_1
    move-object v1, v8

    .line 1964
    goto :goto_0

    .line 1917
    .restart local v15    # "mApk1Resources":Landroid/content/res/Resources;
    .restart local v16    # "mDrawableResID":I
    .restart local v17    # "manager":Landroid/content/pm/PackageManager;
    :cond_2
    const/4 v12, 0x0

    .line 1918
    .local v12, "localInputStream":Ljava/io/InputStream;
    const/4 v1, 0x0

    .line 1919
    .local v1, "localObject":Ljava/lang/Object;
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 1921
    .local v5, "localRect":Landroid/graphics/Rect;
    const-class v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v14

    .line 1923
    .local v14, "localURL":Ljava/net/URL;
    if-eqz v14, :cond_3

    .line 1924
    :try_start_1
    invoke-virtual {v14}, Ljava/net/URL;->openStream()Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v12

    .line 1930
    :cond_3
    :try_start_2
    new-instance v13, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v13}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1931
    .local v13, "localOptions":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x0

    .line 1932
    .local v3, "localBitmap":Landroid/graphics/Bitmap;
    if-eqz v12, :cond_4

    .line 1933
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mDensityDpi:I

    iput v2, v13, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 1934
    invoke-static {v12, v5, v13}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v3

    .line 1936
    :try_start_3
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_2

    .line 1944
    if-eqz v3, :cond_1

    .line 1945
    :try_start_4
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v4

    .line 1946
    .local v4, "arrayOfByte":[B
    invoke-static {v4}, Landroid/graphics/NinePatch;->isNinePatchChunk([B)Z

    move-result v7

    .line 1947
    .local v7, "bool":Z
    if-eqz v7, :cond_5

    .line 1948
    new-instance v1, Landroid/graphics/drawable/NinePatchDrawable;

    .end local v1    # "localObject":Ljava/lang/Object;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1949
    const/4 v6, 0x0

    .line 1948
    invoke-direct/range {v1 .. v6}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Ljava/lang/String;)V

    .line 1954
    :goto_2
    if-eqz v1, :cond_1

    .line 1955
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mResourceMap:Ljava/util/Map;

    move-object v0, v1

    check-cast v0, Landroid/graphics/drawable/Drawable;

    move-object v2, v0

    move-object/from16 v0, p1

    invoke-interface {v6, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1956
    check-cast v1, Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 1926
    .end local v3    # "localBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "arrayOfByte":[B
    .end local v7    # "bool":Z
    .end local v13    # "localOptions":Landroid/graphics/BitmapFactory$Options;
    .restart local v1    # "localObject":Ljava/lang/Object;
    :catch_0
    move-exception v10

    .line 1927
    .local v10, "localIOException2":Ljava/io/IOException;
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1937
    .end local v10    # "localIOException2":Ljava/io/IOException;
    .restart local v3    # "localBitmap":Landroid/graphics/Bitmap;
    .restart local v13    # "localOptions":Landroid/graphics/BitmapFactory$Options;
    :catch_1
    move-exception v11

    .line 1938
    .local v11, "localIOException3":Ljava/io/IOException;
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1941
    .end local v11    # "localIOException3":Ljava/io/IOException;
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1951
    .restart local v4    # "arrayOfByte":[B
    .restart local v7    # "bool":Z
    :cond_5
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "localObject":Ljava/lang/Object;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_2

    .local v1, "localObject":Landroid/graphics/drawable/BitmapDrawable;
    goto :goto_2

    .line 1961
    .end local v1    # "localObject":Landroid/graphics/drawable/BitmapDrawable;
    .end local v3    # "localBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "arrayOfByte":[B
    .end local v5    # "localRect":Landroid/graphics/Rect;
    .end local v7    # "bool":Z
    .end local v12    # "localInputStream":Ljava/io/InputStream;
    .end local v13    # "localOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v14    # "localURL":Ljava/net/URL;
    .end local v15    # "mApk1Resources":Landroid/content/res/Resources;
    .end local v16    # "mDrawableResID":I
    .end local v17    # "manager":Landroid/content/pm/PackageManager;
    :catch_2
    move-exception v9

    .line 1962
    .local v9, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v9}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method public getMinResizeRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 3986
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    .line 3987
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    .line 3989
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getObjectList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4517
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getPanKey(II)Ljava/lang/String;
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 3471
    new-instance v0, Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public getPixel(II)I
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 4538
    const/4 v0, 0x0

    return v0
.end method

.method public getPopupMenu()Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 4593
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 4594
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->getPopupMenu()Landroid/widget/PopupWindow;

    move-result-object v0

    .line 4597
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRect()Landroid/graphics/RectF;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 3781
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 3782
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    .line 3789
    :goto_0
    return-object v2

    .line 3784
    :cond_0
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v4, v4, v4, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 3785
    .local v1, "rect":Landroid/graphics/RectF;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    move-object v2, v1

    .line 3789
    goto :goto_0

    .line 3786
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 3785
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected getRectList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2050
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getRotatePoint(IIFFD)Landroid/graphics/PointF;
    .locals 21
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "pivotX"    # F
    .param p4, "pivotY"    # F
    .param p5, "degrees"    # D

    .prologue
    .line 1238
    invoke-static/range {p5 .. p6}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    .line 1239
    .local v4, "dSetDegree":D
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 1240
    .local v2, "cosq":D
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 1241
    .local v10, "sinq":D
    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v16, v16, p3

    move/from16 v0, v16

    float-to-double v12, v0

    .line 1242
    .local v12, "sx":D
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v16, v16, p4

    move/from16 v0, v16

    float-to-double v14, v0

    .line 1243
    .local v14, "sy":D
    mul-double v16, v12, v2

    mul-double v18, v14, v10

    sub-double v16, v16, v18

    move/from16 v0, p3

    float-to-double v0, v0

    move-wide/from16 v18, v0

    add-double v6, v16, v18

    .line 1244
    .local v6, "rx":D
    mul-double v16, v12, v10

    mul-double v18, v14, v2

    add-double v16, v16, v18

    move/from16 v0, p4

    float-to-double v0, v0

    move-wide/from16 v18, v0

    add-double v8, v16, v18

    .line 1246
    .local v8, "ry":D
    new-instance v16, Landroid/graphics/PointF;

    double-to-float v0, v6

    move/from16 v17, v0

    double-to-float v0, v8

    move/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v16
.end method

.method getStartEndBitmapCalcPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;F)V
    .locals 16
    .param p1, "curPoint"    # Landroid/graphics/PointF;
    .param p2, "prevPoint"    # Landroid/graphics/PointF;
    .param p3, "calcPoint"    # Landroid/graphics/PointF;
    .param p4, "distance"    # F

    .prologue
    .line 4191
    move-object/from16 v0, p1

    iget v9, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/PointF;->x:F

    sub-float v5, v9, v10

    .line 4192
    .local v5, "originalDx":F
    move-object/from16 v0, p1

    iget v9, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/PointF;->y:F

    sub-float v6, v9, v10

    .line 4194
    .local v6, "originalDy":F
    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_MAX:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    .line 4195
    .local v7, "quadrant":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;
    const/4 v9, 0x0

    cmpl-float v9, v5, v9

    if-ltz v9, :cond_0

    const/4 v9, 0x0

    cmpl-float v9, v6, v9

    if-ltz v9, :cond_0

    .line 4196
    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_1:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    .line 4205
    :goto_0
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 4206
    .local v3, "dx":F
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 4208
    .local v4, "dy":F
    float-to-double v10, v4

    float-to-double v12, v3

    div-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->atan(D)D

    move-result-wide v10

    const-wide v12, 0x4066800000000000L    # 180.0

    mul-double/2addr v10, v12

    const-wide v12, 0x400921fb60000000L    # 3.1415927410125732

    div-double/2addr v10, v12

    double-to-float v2, v10

    .line 4209
    .local v2, "degree":F
    const/4 v8, 0x0

    .line 4211
    .local v8, "radian":F
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT()[I

    move-result-object v9

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 4244
    :goto_1
    return-void

    .line 4197
    .end local v2    # "degree":F
    .end local v3    # "dx":F
    .end local v4    # "dy":F
    .end local v8    # "radian":F
    :cond_0
    const/4 v9, 0x0

    cmpg-float v9, v5, v9

    if-gez v9, :cond_1

    const/4 v9, 0x0

    cmpl-float v9, v6, v9

    if-ltz v9, :cond_1

    .line 4198
    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_2:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    .line 4199
    goto :goto_0

    :cond_1
    const/4 v9, 0x0

    cmpl-float v9, v5, v9

    if-ltz v9, :cond_2

    const/4 v9, 0x0

    cmpg-float v9, v6, v9

    if-gez v9, :cond_2

    .line 4200
    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_3:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    .line 4201
    goto :goto_0

    .line 4202
    :cond_2
    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_4:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    goto :goto_0

    .line 4213
    .restart local v2    # "degree":F
    .restart local v3    # "dx":F
    .restart local v4    # "dy":F
    .restart local v8    # "radian":F
    :pswitch_0
    const/high16 v9, 0x42b40000    # 90.0f

    sub-float/2addr v9, v2

    const v10, 0x3c8efa35

    mul-float v8, v9, v10

    .line 4215
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->x:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->x:F

    .line 4216
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->y:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->y:F

    goto :goto_1

    .line 4220
    :pswitch_1
    const v9, 0x3c8efa35

    mul-float v8, v2, v9

    .line 4222
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->x:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->x:F

    .line 4223
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->y:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->y:F

    goto :goto_1

    .line 4227
    :pswitch_2
    const v9, 0x3c8efa35

    mul-float v8, v2, v9

    .line 4229
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->x:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->x:F

    .line 4230
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->y:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->y:F

    goto/16 :goto_1

    .line 4234
    :pswitch_3
    const/high16 v9, 0x42b40000    # 90.0f

    sub-float/2addr v9, v2

    const v10, 0x3c8efa35

    mul-float v8, v9, v10

    .line 4236
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->x:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->x:F

    .line 4237
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->y:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->y:F

    goto/16 :goto_1

    .line 4211
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getStyle()I
    .locals 1

    .prologue
    .line 3825
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    return v0
.end method

.method protected handleMoveControl(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 10
    .param p1, "pos"    # Landroid/graphics/Point;
    .param p2, "pos_rotated"    # Landroid/graphics/Point;

    .prologue
    const/high16 v7, 0x41a00000    # 20.0f

    const/4 v9, 0x0

    .line 1389
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1454
    :cond_0
    :goto_0
    return-void

    .line 1393
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isMovable()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1397
    iget v5, p1, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->x:F

    sub-float/2addr v5, v6

    float-to-int v5, v5

    int-to-float v0, v5

    .line 1398
    .local v0, "changeX":F
    iget v5, p1, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->y:F

    sub-float/2addr v5, v6

    float-to-int v5, v5

    int-to-float v1, v5

    .line 1400
    .local v1, "changeY":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpg-float v5, v5, v7

    if-gez v5, :cond_2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpg-float v5, v5, v7

    if-gez v5, :cond_2

    .line 1401
    iget-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTrivialMovingEn:Z

    if-eqz v5, :cond_0

    .line 1405
    :cond_2
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTrivialMovingEn:Z

    .line 1408
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 1410
    .local v3, "preRectF":Landroid/graphics/RectF;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_5

    .line 1427
    instance-of v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;

    if-eqz v5, :cond_8

    .line 1428
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    if-nez v5, :cond_3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 1429
    new-instance v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mControlBaseContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    .line 1431
    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v6, "SM-P90"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1432
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->setMovingPaint(Landroid/graphics/Paint;)V

    .line 1437
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1440
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    if-eqz v5, :cond_4

    .line 1441
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->setVisibility(I)V

    .line 1442
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    invoke-virtual {v6, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->setRect(Landroid/graphics/RectF;)V

    .line 1443
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->setRotatedAngle(F)V

    .line 1444
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->invalidate()V

    .line 1449
    :cond_4
    :goto_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v6, p1, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    iput v6, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->x:F

    .line 1450
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v6, p1, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    iput v6, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->y:F

    .line 1452
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v6, p2, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    iput v6, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_x:F

    .line 1453
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v6, p2, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    iput v6, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_y:F

    goto/16 :goto_0

    .line 1411
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    .line 1412
    .local v4, "rectF":Landroid/graphics/RectF;
    iget v5, v4, Landroid/graphics/RectF;->left:F

    iget v6, v4, Landroid/graphics/RectF;->top:F

    iget v7, v4, Landroid/graphics/RectF;->right:F

    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1413
    iget v5, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v5, v0

    iget v6, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v6, v1

    iget v7, v4, Landroid/graphics/RectF;->right:F

    add-float/2addr v7, v0

    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v8, v1

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1415
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1416
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v5

    invoke-direct {p0, v4, v2, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->checkAllRectOutOfCanvas(Landroid/graphics/RectF;IF)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1417
    iget v5, v3, Landroid/graphics/RectF;->left:F

    iget v6, v3, Landroid/graphics/RectF;->top:F

    iget v7, v3, Landroid/graphics/RectF;->right:F

    iget v8, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1420
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1422
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1410
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 1434
    .end local v4    # "rectF":Landroid/graphics/RectF;
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMovingView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->setMovingPaint(Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1447
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    goto/16 :goto_3
.end method

.method protected handleResizeControl(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 19
    .param p1, "pos"    # Landroid/graphics/PointF;
    .param p2, "pos_rotated"    # Landroid/graphics/PointF;

    .prologue
    .line 1291
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1292
    const/4 v1, 0x0

    .line 1385
    :goto_0
    return v1

    .line 1295
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    .line 1297
    .local v2, "index":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 1298
    const/4 v1, 0x0

    goto :goto_0

    .line 1301
    :cond_1
    new-instance v11, Landroid/graphics/PointF;

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {v11, v1, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1303
    .local v11, "diff_rotated":Landroid/graphics/PointF;
    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_x:F

    sub-float/2addr v1, v3

    iput v1, v11, Landroid/graphics/PointF;->x:F

    .line 1304
    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_y:F

    sub-float/2addr v1, v3

    iput v1, v11, Landroid/graphics/PointF;->y:F

    .line 1306
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findResizeRate(Landroid/graphics/PointF;Landroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 1308
    .local v18, "resizeRate":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v14

    .line 1309
    .local v14, "objRect":Landroid/graphics/RectF;
    invoke-virtual {v14}, Landroid/graphics/RectF;->width()F

    move-result v1

    const v3, 0x3f800347    # 1.0001f

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v1

    const v3, 0x3f800347    # 1.0001f

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    .line 1310
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    packed-switch v1, :pswitch_data_0

    .line 1346
    :cond_2
    :goto_1
    :pswitch_0
    new-instance v16, Landroid/graphics/RectF;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/RectF;-><init>()V

    .line 1347
    .local v16, "prevRect":Landroid/graphics/RectF;
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 1348
    .local v15, "postRect":Landroid/graphics/RectF;
    new-instance v12, Landroid/graphics/PointF;

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {v12, v1, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1351
    .local v12, "diff_rotated_rated":Landroid/graphics/PointF;
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v13, v1, :cond_7

    .line 1383
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    .line 1385
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 1312
    .end local v12    # "diff_rotated_rated":Landroid/graphics/PointF;
    .end local v13    # "i":I
    .end local v15    # "postRect":Landroid/graphics/RectF;
    .end local v16    # "prevRect":Landroid/graphics/RectF;
    :pswitch_1
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_3

    .line 1313
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_1

    .line 1315
    :cond_3
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/PointF;->x:F

    goto :goto_1

    .line 1320
    :pswitch_2
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/PointF;->x:F

    neg-float v1, v1

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_4

    .line 1321
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/PointF;->x:F

    neg-float v1, v1

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_1

    .line 1323
    :cond_4
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/PointF;->y:F

    neg-float v1, v1

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/PointF;->x:F

    goto :goto_1

    .line 1328
    :pswitch_3
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/PointF;->x:F

    neg-float v1, v1

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->y:F

    neg-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_5

    .line 1329
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_1

    .line 1331
    :cond_5
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/PointF;->x:F

    goto :goto_1

    .line 1336
    :pswitch_4
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->y:F

    neg-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_6

    .line 1337
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/PointF;->x:F

    neg-float v1, v1

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto/16 :goto_1

    .line 1339
    :cond_6
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/PointF;->y:F

    neg-float v1, v1

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/PointF;->x:F

    goto/16 :goto_1

    .line 1352
    .restart local v12    # "diff_rotated_rated":Landroid/graphics/PointF;
    .restart local v13    # "i":I
    .restart local v15    # "postRect":Landroid/graphics/RectF;
    .restart local v16    # "prevRect":Landroid/graphics/RectF;
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1354
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v17

    .line 1355
    .local v17, "rect":Landroid/graphics/RectF;
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/RectF;->width()F

    move-result v1

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, v3

    iput v1, v12, Landroid/graphics/PointF;->x:F

    .line 1356
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/RectF;->height()F

    move-result v1

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->y:F

    mul-float/2addr v1, v3

    iput v1, v12, Landroid/graphics/PointF;->y:F

    .line 1358
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v12}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resize(ILandroid/graphics/PointF;)V

    .line 1359
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v1

    if-nez v1, :cond_8

    .line 1360
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v8

    .line 1361
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v1

    const/4 v9, 0x1

    if-ne v1, v9, :cond_9

    const/4 v9, 0x1

    .line 1362
    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v10

    div-float v10, v1, v10

    move-object/from16 v1, p0

    .line 1360
    invoke-virtual/range {v1 .. v10}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObject(IZZZFFFZF)Z

    move-result v1

    .line 1362
    if-eqz v1, :cond_8

    .line 1363
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1366
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1368
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v1

    if-nez v1, :cond_a

    .line 1369
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 1361
    :cond_9
    const/4 v9, 0x0

    goto :goto_3

    .line 1372
    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    invoke-virtual {v15, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1374
    move-object/from16 v0, v16

    iget v1, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    iget v3, v15, Landroid/graphics/RectF;->right:F

    iget v4, v15, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v3

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_b

    .line 1375
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1377
    :cond_b
    move-object/from16 v0, v16

    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    iget v4, v15, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v3

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_c

    .line 1378
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1351
    :cond_c
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2

    .line 1310
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected handleRotationControl(Landroid/graphics/Point;)V
    .locals 33
    .param p1, "pos"    # Landroid/graphics/Point;

    .prologue
    .line 1463
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1543
    :cond_0
    :goto_0
    return-void

    .line 1467
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move/from16 v22, v0

    .line 1468
    .local v22, "preDegree":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    move/from16 v17, v0

    .line 1469
    .local v17, "index":I
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v2

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    sub-float v15, v2, v4

    .line 1470
    .local v15, "dx":F
    move-object/from16 v0, p1

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v4

    sub-float v16, v2, v4

    .line 1472
    .local v16, "dy":F
    move/from16 v0, v16

    float-to-double v4, v0

    float-to-double v6, v15

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v24

    .line 1473
    .local v24, "radian":D
    const-wide v4, 0x4066800000000000L    # 180.0

    mul-double v4, v4, v24

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v4, v6

    double-to-float v14, v4

    .line 1475
    .local v14, "degree":F
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v14

    .line 1477
    const/16 v18, 0x1

    .line 1478
    .local v18, "isClippable":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v3, v2, :cond_9

    .line 1484
    :goto_2
    if-nez v18, :cond_2

    .line 1485
    const/4 v3, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v3, v2, :cond_b

    .line 1492
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v28

    .line 1493
    .local v28, "rotateRect":Landroid/graphics/RectF;
    invoke-virtual/range {v28 .. v28}, Landroid/graphics/RectF;->centerX()F

    move-result v29

    .line 1494
    .local v29, "startX":F
    invoke-virtual/range {v28 .. v28}, Landroid/graphics/RectF;->centerY()F

    move-result v32

    .line 1496
    .local v32, "startY":F
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v12

    .line 1497
    .local v12, "centerPointX":F
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v13

    .line 1499
    .local v13, "centerPointY":F
    sub-float v2, v12, v29

    sub-float v4, v12, v29

    mul-float/2addr v2, v4

    sub-float v4, v13, v32

    .line 1500
    sub-float v5, v13, v32

    mul-float/2addr v4, v5

    .line 1499
    add-float/2addr v2, v4

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v30

    .line 1501
    .local v30, "rotationRad":D
    move-object/from16 v0, p1

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float v2, v12, v2

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    sub-float v4, v12, v4

    mul-float/2addr v2, v4

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    sub-float v4, v13, v4

    .line 1502
    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    sub-float v5, v13, v5

    mul-float/2addr v4, v5

    .line 1501
    add-float/2addr v2, v4

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v20

    .line 1504
    .local v20, "pos2Center":D
    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v2, v14

    float-to-int v2, v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 1506
    const-wide v4, 0x4051800000000000L    # 70.0

    add-double v4, v4, v30

    cmpg-double v2, v20, v4

    if-gez v2, :cond_5

    .line 1507
    const/high16 v2, 0x43320000    # 178.0f

    cmpl-float v2, v14, v2

    if-lez v2, :cond_3

    const/high16 v2, 0x43380000    # 184.0f

    cmpg-float v2, v14, v2

    if-gez v2, :cond_3

    .line 1508
    const/high16 v14, 0x43340000    # 180.0f

    .line 1510
    :cond_3
    const/high16 v2, -0x3cc80000    # -184.0f

    cmpl-float v2, v14, v2

    if-lez v2, :cond_4

    const/high16 v2, -0x3cce0000    # -178.0f

    cmpg-float v2, v14, v2

    if-gez v2, :cond_4

    .line 1511
    const/high16 v14, -0x3ccc0000    # -180.0f

    .line 1514
    :cond_4
    float-to-int v2, v14

    div-int/lit8 v2, v2, 0x5

    mul-int/lit8 v2, v2, 0x5

    int-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 1517
    :cond_5
    if-eqz v18, :cond_6

    .line 1519
    new-instance v23, Landroid/graphics/RectF;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/RectF;-><init>()V

    .line 1521
    .local v23, "preRectF":Landroid/graphics/RectF;
    const/4 v3, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v3, v2, :cond_c

    .line 1532
    .end local v23    # "preRectF":Landroid/graphics/RectF;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    .line 1533
    const/4 v3, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 1534
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotateable(I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1535
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move/from16 v19, v0

    .line 1536
    .local v19, "objAng":F
    move/from16 v0, v17

    if-eq v3, v0, :cond_7

    const/4 v2, -0x1

    move/from16 v0, v17

    if-eq v0, v2, :cond_7

    .line 1537
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v4

    sub-float v27, v2, v4

    .line 1538
    .local v27, "rotateDiff":F
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v2

    add-float v2, v2, v27

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v19

    .line 1540
    .end local v27    # "rotateDiff":F
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1533
    .end local v19    # "objAng":F
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1479
    .end local v12    # "centerPointX":F
    .end local v13    # "centerPointY":F
    .end local v20    # "pos2Center":D
    .end local v28    # "rotateRect":Landroid/graphics/RectF;
    .end local v29    # "startX":F
    .end local v30    # "rotationRad":D
    .end local v32    # "startY":F
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v2

    if-nez v2, :cond_a

    .line 1480
    const/16 v18, 0x0

    .line 1481
    goto/16 :goto_2

    .line 1478
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 1486
    :cond_b
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    float-to-int v2, v14

    int-to-float v9, v2

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObject(IZZZFFFZF)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1485
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    .line 1522
    .restart local v12    # "centerPointX":F
    .restart local v13    # "centerPointY":F
    .restart local v20    # "pos2Center":D
    .restart local v23    # "preRectF":Landroid/graphics/RectF;
    .restart local v28    # "rotateRect":Landroid/graphics/RectF;
    .restart local v29    # "startX":F
    .restart local v30    # "rotationRad":D
    .restart local v32    # "startY":F
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/graphics/RectF;

    .line 1523
    .local v26, "rectF":Landroid/graphics/RectF;
    move-object/from16 v0, v26

    iget v2, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v26

    iget v4, v0, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, v26

    iget v5, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, v26

    iget v6, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1525
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->checkAllRectOutOfCanvas(Landroid/graphics/RectF;IF)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1526
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    goto/16 :goto_0

    .line 1521
    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4
.end method

.method protected handleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 27
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1758
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1759
    const/4 v4, 0x0

    .line 1896
    :goto_0
    return v4

    .line 1762
    :cond_0
    const/4 v5, 0x0

    .line 1763
    .local v5, "activeRect":Landroid/graphics/RectF;
    new-instance v22, Landroid/graphics/Point;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    move-object/from16 v0, v22

    invoke-direct {v0, v4, v6}, Landroid/graphics/Point;-><init>(II)V

    .line 1765
    .local v22, "pos":Landroid/graphics/Point;
    new-instance v23, Landroid/graphics/Point;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Point;-><init>()V

    .line 1767
    .local v23, "pos_rotated":Landroid/graphics/Point;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v4, v4, 0xff

    packed-switch v4, :pswitch_data_0

    .line 1896
    :goto_1
    :pswitch_0
    const/4 v4, 0x0

    goto :goto_0

    .line 1769
    :pswitch_1
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    .line 1770
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    .line 1772
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, v22

    iget v7, v0, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    iput v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->x:F

    iput v7, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->x:F

    .line 1773
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, v22

    iget v7, v0, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    iput v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->y:F

    iput v7, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->y:F

    .line 1774
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 1776
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v17

    if-lt v0, v4, :cond_2

    .line 1811
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, v23

    iget v7, v0, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    iput v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_x:F

    iput v7, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_x:F

    .line 1812
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, v23

    iget v7, v0, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    iput v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_y:F

    iput v7, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_y:F

    .line 1814
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isPressed()Z

    move-result v4

    if-nez v4, :cond_6

    .line 1815
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1777
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1778
    .local v21, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object/from16 v0, v22

    iget v6, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v22

    iget v7, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v8

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v9

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v4

    neg-float v4, v4

    float-to-double v10, v4

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    .end local v5    # "activeRect":Landroid/graphics/RectF;
    move-result-object v25

    .line 1779
    .local v25, "tempPoint":Landroid/graphics/PointF;
    new-instance v23, Landroid/graphics/Point;

    .end local v23    # "pos_rotated":Landroid/graphics/Point;
    move-object/from16 v0, v25

    iget v4, v0, Landroid/graphics/PointF;->x:F

    float-to-int v4, v4

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/PointF;->y:F

    float-to-int v6, v6

    move-object/from16 v0, v23

    invoke-direct {v0, v4, v6}, Landroid/graphics/Point;-><init>(II)V

    .line 1781
    .restart local v23    # "pos_rotated":Landroid/graphics/Point;
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v5

    .line 1782
    .restart local v5    # "activeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->Increase2MinimumRect(Landroid/graphics/RectF;)Z

    .line 1784
    move-object/from16 v0, v23

    iget v6, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v23

    iget v7, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v4, p0

    move-object/from16 v8, v21

    invoke-virtual/range {v4 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onCheckTouchPosition(Landroid/graphics/RectF;IILcom/samsung/android/sdk/pen/document/SpenObjectBase;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;)V

    .line 1785
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isPressed()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1786
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move/from16 v0, v17

    iput v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    .line 1787
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 1789
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1790
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resize2Threshold(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1791
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v4, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 1793
    move-object/from16 v0, p0

    instance-of v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    if-eqz v4, :cond_4

    .line 1794
    const/4 v4, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 1800
    :cond_3
    :goto_3
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_1

    .line 1801
    new-instance v4, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    .line 1803
    const/16 v19, 0x0

    .local v19, "j":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v19

    if-ge v0, v4, :cond_1

    .line 1804
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    new-instance v7, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    invoke-direct {v7, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1803
    add-int/lit8 v19, v19, 0x1

    goto :goto_4

    .line 1796
    .end local v19    # "j":I
    :cond_4
    const/4 v4, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    goto :goto_3

    .line 1776
    :cond_5
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_2

    .line 1817
    .end local v21    # "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .end local v25    # "tempPoint":Landroid/graphics/PointF;
    :cond_6
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1820
    .end local v17    # "i":I
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    goto/16 :goto_1

    .line 1824
    :pswitch_3
    const/16 v18, 0x0

    .line 1825
    .local v18, "index":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    if-ltz v4, :cond_7

    .line 1826
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    move/from16 v18, v0

    .line 1828
    :cond_7
    move-object/from16 v0, v22

    iget v8, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v22

    iget v9, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v10

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v11

    .line 1829
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v4

    neg-float v4, v4

    float-to-double v12, v4

    move-object/from16 v7, p0

    .line 1828
    invoke-virtual/range {v7 .. v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v25

    .line 1830
    .restart local v25    # "tempPoint":Landroid/graphics/PointF;
    new-instance v23, Landroid/graphics/Point;

    .end local v23    # "pos_rotated":Landroid/graphics/Point;
    move-object/from16 v0, v25

    iget v4, v0, Landroid/graphics/PointF;->x:F

    float-to-int v4, v4

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/PointF;->y:F

    float-to-int v6, v6

    move-object/from16 v0, v23

    invoke-direct {v0, v4, v6}, Landroid/graphics/Point;-><init>(II)V

    .line 1832
    .restart local v23    # "pos_rotated":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v4

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1833
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleRotationControl(Landroid/graphics/Point;)V

    goto/16 :goto_1

    .line 1837
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1838
    new-instance v4, Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    invoke-direct {v4, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v6, Landroid/graphics/PointF;

    move-object/from16 v0, v25

    iget v7, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v25

    iget v8, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v6, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleResizeControl(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v4

    goto/16 :goto_0

    .line 1841
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1842
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleMoveControl(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 1843
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1845
    :cond_a
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1848
    .end local v18    # "index":I
    .end local v25    # "tempPoint":Landroid/graphics/PointF;
    :pswitch_4
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v4

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    instance-of v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTrivialMovingEn:Z

    if-eqz v4, :cond_b

    .line 1849
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1850
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1851
    const-string v4, "SpenControlBase"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Up-Moving Rect = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1854
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v4

    if-nez v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v4

    if-nez v4, :cond_c

    .line 1855
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1856
    :cond_c
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v17

    if-lt v0, v4, :cond_f

    .line 1868
    .end local v17    # "i":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1869
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v17

    if-lt v0, v4, :cond_10

    .line 1887
    .end local v17    # "i":I
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onObjectChanged()V

    .line 1889
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 1890
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFirstTouch:Z

    .line 1891
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1857
    .restart local v17    # "i":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/graphics/RectF;

    .line 1858
    .local v24, "rectf":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v14

    .line 1859
    .local v14, "boundBox":Landroid/graphics/RectF;
    if-eqz v14, :cond_d

    if-eqz v24, :cond_d

    .line 1862
    invoke-virtual {v14}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v6

    sub-float/2addr v4, v6

    float-to-int v15, v4

    .line 1863
    .local v15, "deltaX":I
    invoke-virtual {v14}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v6

    sub-float/2addr v4, v6

    float-to-int v0, v4

    move/from16 v16, v0

    .line 1864
    .local v16, "deltaY":I
    int-to-float v4, v15

    move/from16 v0, v16

    int-to-float v6, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v6}, Landroid/graphics/RectF;->offset(FF)V

    .line 1856
    add-int/lit8 v17, v17, 0x1

    goto :goto_5

    .line 1870
    .end local v14    # "boundBox":Landroid/graphics/RectF;
    .end local v15    # "deltaX":I
    .end local v16    # "deltaY":I
    .end local v24    # "rectf":Landroid/graphics/RectF;
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/graphics/RectF;

    .line 1873
    .local v20, "modifiedRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    if-eqz v4, :cond_11

    .line 1874
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v26, v0

    .line 1875
    .local v26, "tmpEdge":F
    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v20

    iput v4, v0, Landroid/graphics/RectF;->right:F

    .line 1876
    move/from16 v0, v26

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 1878
    .end local v26    # "tmpEdge":F
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    if-eqz v4, :cond_12

    .line 1879
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    .line 1880
    .restart local v26    # "tmpEdge":F
    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, v20

    iput v4, v0, Landroid/graphics/RectF;->top:F

    .line 1881
    move/from16 v0, v26

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 1869
    .end local v26    # "tmpEdge":F
    :cond_12
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_6

    .line 1767
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected isClippedObject(IZZZFFFZF)Z
    .locals 24
    .param p1, "index"    # I
    .param p2, "isRotating"    # Z
    .param p3, "isMoving"    # Z
    .param p4, "isResizing"    # Z
    .param p5, "changeX"    # F
    .param p6, "changeY"    # F
    .param p7, "angle"    # F
    .param p8, "isKeepRatio"    # Z
    .param p9, "ratio"    # F

    .prologue
    .line 2187
    const/4 v14, 0x0

    .line 2189
    .local v14, "rect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 2190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    .line 2191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v21, v0

    invoke-interface/range {v20 .. v21}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2194
    :cond_0
    if-nez p3, :cond_1

    if-eqz p4, :cond_3

    .line 2195
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "rect":Landroid/graphics/RectF;
    check-cast v14, Landroid/graphics/RectF;

    .line 2199
    .restart local v14    # "rect":Landroid/graphics/RectF;
    :cond_2
    :goto_0
    if-nez v14, :cond_4

    .line 2200
    const/16 v20, 0x0

    .line 3095
    :goto_1
    return v20

    .line 2196
    :cond_3
    if-eqz p2, :cond_2

    .line 2197
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v14

    goto :goto_0

    .line 2203
    :cond_4
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v12

    .line 2204
    .local v12, "px":F
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v13

    .line 2206
    .local v13, "py":F
    if-eqz p3, :cond_b

    .line 2207
    const/4 v8, 0x0

    .local v8, "deltaY":F
    const/4 v7, 0x0

    .line 2208
    .local v7, "deltaX":F
    new-instance v18, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2209
    .local v18, "topleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2211
    new-instance v19, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-direct/range {v19 .. v21}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2212
    .local v19, "topright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2214
    new-instance v5, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v5, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2215
    .local v5, "bottomleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2217
    new-instance v6, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v6, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2218
    .local v6, "bottomright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2220
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2221
    .local v11, "points":[F
    const/16 v20, 0x0

    aget v9, v11, v20

    .line 2222
    .local v9, "left":F
    const/16 v20, 0x1

    aget v17, v11, v20

    .line 2223
    .local v17, "top":F
    const/16 v20, 0x2

    aget v15, v11, v20

    .line 2224
    .local v15, "right":F
    const/16 v20, 0x3

    aget v4, v11, v20

    .line 2226
    .local v4, "bottom":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    cmpg-float v20, v9, v20

    if-gez v20, :cond_5

    .line 2227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    sub-float v7, v20, v9

    .line 2229
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    cmpl-float v20, v15, v20

    if-lez v20, :cond_6

    .line 2230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    sub-float v7, v20, v15

    .line 2232
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    cmpg-float v20, v17, v20

    if-gez v20, :cond_7

    .line 2233
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v8, v20, v17

    .line 2235
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    cmpl-float v20, v4, v20

    if-lez v20, :cond_8

    .line 2236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    sub-float v8, v20, v4

    .line 2239
    :cond_8
    const/16 v20, 0x0

    cmpl-float v20, v7, v20

    if-nez v20, :cond_9

    const/16 v20, 0x0

    cmpl-float v20, v8, v20

    if-eqz v20, :cond_a

    .line 2240
    :cond_9
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2241
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2242
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    iput v0, v5, Landroid/graphics/PointF;->x:F

    .line 2243
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/PointF;->x:F

    .line 2244
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2245
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2246
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    iput v0, v5, Landroid/graphics/PointF;->y:F

    .line 2247
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/PointF;->y:F

    .line 2249
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2250
    const/16 v20, 0x0

    aget v9, v11, v20

    .line 2251
    const/16 v20, 0x1

    aget v17, v11, v20

    .line 2252
    const/16 v20, 0x2

    aget v15, v11, v20

    .line 2253
    const/16 v20, 0x3

    aget v4, v11, v20

    .line 2255
    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    add-float v22, v9, v15

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v12, v0

    .line 2256
    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    add-float v22, v17, v4

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v13, v0

    .line 2257
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2258
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2259
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2260
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2262
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2263
    const/16 v20, 0x0

    aget v9, v11, v20

    .line 2264
    const/16 v20, 0x1

    aget v17, v11, v20

    .line 2265
    const/16 v20, 0x2

    aget v15, v11, v20

    .line 2266
    const/16 v20, 0x3

    aget v4, v11, v20

    .line 2267
    move/from16 v0, v17

    invoke-virtual {v14, v9, v0, v15, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2269
    :cond_a
    const/16 v20, 0x1

    goto/16 :goto_1

    .line 2270
    .end local v4    # "bottom":F
    .end local v5    # "bottomleft":Landroid/graphics/PointF;
    .end local v6    # "bottomright":Landroid/graphics/PointF;
    .end local v7    # "deltaX":F
    .end local v8    # "deltaY":F
    .end local v9    # "left":F
    .end local v11    # "points":[F
    .end local v15    # "right":F
    .end local v17    # "top":F
    .end local v18    # "topleft":Landroid/graphics/PointF;
    .end local v19    # "topright":Landroid/graphics/PointF;
    :cond_b
    if-eqz p4, :cond_c7

    .line 2271
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v10

    .line 2273
    .local v10, "originalRect":Landroid/graphics/RectF;
    const/4 v8, 0x0

    .restart local v8    # "deltaY":F
    const/4 v7, 0x0

    .line 2274
    .restart local v7    # "deltaX":F
    new-instance v18, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2275
    .restart local v18    # "topleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2277
    new-instance v19, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-direct/range {v19 .. v21}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2278
    .restart local v19    # "topright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2280
    new-instance v5, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v5, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2281
    .restart local v5    # "bottomleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2283
    new-instance v6, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v6, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2284
    .restart local v6    # "bottomright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2286
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObjectMovingOutsideFrameRect(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 2287
    const/16 v20, 0x1

    goto/16 :goto_1

    .line 2290
    :cond_c
    const/4 v11, 0x0

    .line 2292
    .restart local v11    # "points":[F
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2293
    const/16 v20, 0x0

    aget v9, v11, v20

    .line 2294
    .restart local v9    # "left":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    cmpg-float v20, v9, v20

    if-gez v20, :cond_12

    .line 2296
    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_d

    .line 2297
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    .line 2299
    :cond_d
    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_e

    .line 2300
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    .line 2303
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    sub-float v7, v20, v9

    .line 2305
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v9, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_24

    .line 2306
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2308
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2309
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2311
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2335
    :cond_f
    :goto_2
    const/16 v20, 0x0

    aget v20, v11, v20

    const/16 v21, 0x1

    aget v21, v11, v21

    const/16 v22, 0x2

    aget v22, v11, v22

    const/16 v23, 0x3

    aget v23, v11, v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2338
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2339
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2341
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2343
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2345
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2347
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2349
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2350
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-eqz v20, :cond_36

    .line 2353
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_10

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_2e

    .line 2354
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_28

    .line 2355
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_27

    .line 2356
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 2408
    :cond_11
    :goto_3
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2409
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2411
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2412
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2414
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2415
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2417
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2418
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2494
    :cond_12
    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2495
    const/16 v20, 0x1

    aget v17, v11, v20

    .line 2496
    .restart local v17    # "top":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    cmpg-float v20, v17, v20

    if-gez v20, :cond_18

    .line 2497
    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_13

    .line 2498
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    .line 2500
    :cond_13
    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_14

    .line 2501
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    .line 2504
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v8, v20, v17

    .line 2505
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v17, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_4d

    .line 2506
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2508
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2509
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2511
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2536
    :cond_15
    :goto_5
    const/16 v20, 0x0

    aget v20, v11, v20

    const/16 v21, 0x1

    aget v21, v11, v21

    const/16 v22, 0x2

    aget v22, v11, v22

    const/16 v23, 0x3

    aget v23, v11, v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2539
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2540
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2542
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2544
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2546
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2548
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2550
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2551
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2553
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-eqz v20, :cond_5f

    .line 2554
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_16

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_57

    .line 2555
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_51

    .line 2556
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_50

    .line 2557
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 2609
    :cond_17
    :goto_6
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2610
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2612
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2613
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2615
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2616
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2618
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2619
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2695
    :cond_18
    :goto_7
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2696
    const/16 v20, 0x2

    aget v15, v11, v20

    .line 2698
    .restart local v15    # "right":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    cmpl-float v20, v15, v20

    if-lez v20, :cond_1e

    .line 2699
    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_19

    .line 2700
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    .line 2702
    :cond_19
    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_1a

    .line 2703
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    .line 2706
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    sub-float v7, v20, v15

    .line 2707
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v15, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_76

    .line 2708
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2710
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2711
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2713
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2737
    :cond_1b
    :goto_8
    const/16 v20, 0x0

    aget v20, v11, v20

    const/16 v21, 0x1

    aget v21, v11, v21

    const/16 v22, 0x2

    aget v22, v11, v22

    const/16 v23, 0x3

    aget v23, v11, v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2740
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2741
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2743
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2744
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2746
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2748
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2750
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2752
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2754
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-eqz v20, :cond_88

    .line 2755
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_1c

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_80

    .line 2756
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_7a

    .line 2757
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_79

    .line 2758
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 2810
    :cond_1d
    :goto_9
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2811
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2813
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2814
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2816
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2817
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2819
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2820
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2895
    :cond_1e
    :goto_a
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2896
    const/16 v20, 0x3

    aget v4, v11, v20

    .line 2898
    .restart local v4    # "bottom":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    cmpl-float v20, v4, v20

    if-lez v20, :cond_23

    .line 2899
    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_1f

    .line 2900
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    .line 2902
    :cond_1f
    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_20

    .line 2903
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    .line 2906
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    sub-float v8, v20, v4

    .line 2907
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v4, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_9f

    .line 2909
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2911
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2912
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2914
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2940
    :cond_21
    :goto_b
    const/16 v20, 0x0

    aget v20, v11, v20

    const/16 v21, 0x1

    aget v21, v11, v21

    const/16 v22, 0x2

    aget v22, v11, v22

    const/16 v23, 0x3

    aget v23, v11, v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2943
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2944
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2946
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2947
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2949
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2950
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2952
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2953
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2955
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-eqz v20, :cond_b1

    .line 2956
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_22

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a9

    .line 2957
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_a3

    .line 2958
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a2

    .line 2959
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 3095
    .end local v4    # "bottom":F
    .end local v5    # "bottomleft":Landroid/graphics/PointF;
    .end local v6    # "bottomright":Landroid/graphics/PointF;
    .end local v7    # "deltaX":F
    .end local v8    # "deltaY":F
    .end local v9    # "left":F
    .end local v10    # "originalRect":Landroid/graphics/RectF;
    .end local v11    # "points":[F
    .end local v15    # "right":F
    .end local v17    # "top":F
    .end local v18    # "topleft":Landroid/graphics/PointF;
    .end local v19    # "topright":Landroid/graphics/PointF;
    :cond_23
    :goto_c
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 2312
    .restart local v5    # "bottomleft":Landroid/graphics/PointF;
    .restart local v6    # "bottomright":Landroid/graphics/PointF;
    .restart local v7    # "deltaX":F
    .restart local v8    # "deltaY":F
    .restart local v9    # "left":F
    .restart local v10    # "originalRect":Landroid/graphics/RectF;
    .restart local v11    # "points":[F
    .restart local v18    # "topleft":Landroid/graphics/PointF;
    .restart local v19    # "topright":Landroid/graphics/PointF;
    :cond_24
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v9, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_25

    .line 2313
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2315
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2316
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2318
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2319
    goto/16 :goto_2

    :cond_25
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v9, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_26

    .line 2320
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    iput v0, v5, Landroid/graphics/PointF;->x:F

    .line 2322
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2323
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2325
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2326
    goto/16 :goto_2

    :cond_26
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v9, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_f

    .line 2327
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/PointF;->x:F

    .line 2329
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2330
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2332
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    goto/16 :goto_2

    .line 2358
    :cond_27
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_3

    .line 2360
    :cond_28
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2a

    .line 2361
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_29

    .line 2362
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_3

    .line 2364
    :cond_29
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_3

    .line 2366
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2c

    .line 2367
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_2b

    .line 2368
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_3

    .line 2370
    :cond_2b
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_3

    .line 2372
    :cond_2c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_11

    .line 2373
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_2d

    .line 2374
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_3

    .line 2376
    :cond_2d
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_3

    .line 2380
    :cond_2e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_30

    .line 2381
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_2f

    .line 2382
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_3

    .line 2384
    :cond_2f
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_3

    .line 2386
    :cond_30
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_32

    .line 2387
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_31

    .line 2388
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_3

    .line 2390
    :cond_31
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_3

    .line 2392
    :cond_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_34

    .line 2393
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_33

    .line 2394
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_3

    .line 2396
    :cond_33
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_3

    .line 2398
    :cond_34
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_11

    .line 2399
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_35

    .line 2400
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_3

    .line 2402
    :cond_35
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_3

    .line 2419
    :cond_36
    if-eqz p8, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-nez v20, :cond_12

    .line 2420
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_37

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_3c

    .line 2421
    :cond_37
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_39

    .line 2422
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 2479
    :cond_38
    :goto_d
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2480
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2482
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2483
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2485
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2486
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2488
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2489
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    goto/16 :goto_4

    .line 2423
    :cond_39
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3a

    .line 2424
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    .line 2425
    :cond_3a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3b

    .line 2426
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    .line 2427
    :cond_3b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_38

    .line 2428
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    .line 2431
    :cond_3c
    move/from16 v16, p7

    .line 2432
    .local v16, "tmp_angle":F
    const/16 v20, 0x0

    cmpg-float v20, v16, v20

    if-gez v20, :cond_3d

    .line 2433
    const/high16 v20, 0x43b40000    # 360.0f

    add-float v16, v16, v20

    .line 2436
    :cond_3d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_41

    .line 2437
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_3e

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_3e

    .line 2438
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    .line 2439
    :cond_3e
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_3f

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_3f

    .line 2440
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    .line 2441
    :cond_3f
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_40

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_40

    .line 2442
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_d

    .line 2443
    :cond_40
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_38

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_38

    .line 2444
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_d

    .line 2446
    :cond_41
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_45

    .line 2447
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_42

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_42

    .line 2448
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    .line 2449
    :cond_42
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_43

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_43

    .line 2450
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    .line 2451
    :cond_43
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_44

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_44

    .line 2452
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    .line 2453
    :cond_44
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_38

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_38

    .line 2454
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_d

    .line 2456
    :cond_45
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_49

    .line 2457
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_46

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_46

    .line 2458
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    .line 2459
    :cond_46
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_47

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_47

    .line 2460
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    .line 2461
    :cond_47
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_48

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_48

    .line 2462
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    .line 2463
    :cond_48
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_38

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_38

    .line 2464
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    .line 2466
    :cond_49
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_38

    .line 2467
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_4a

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_4a

    .line 2468
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    .line 2469
    :cond_4a
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_4b

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_4b

    .line 2470
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    .line 2471
    :cond_4b
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_4c

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_4c

    .line 2472
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_d

    .line 2473
    :cond_4c
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_38

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_38

    .line 2474
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    .line 2512
    .end local v16    # "tmp_angle":F
    .restart local v17    # "top":F
    :cond_4d
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v17, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_4e

    .line 2513
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2515
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2516
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2518
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2519
    goto/16 :goto_5

    :cond_4e
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v17, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_4f

    .line 2521
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    iput v0, v5, Landroid/graphics/PointF;->y:F

    .line 2523
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2524
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2526
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2527
    goto/16 :goto_5

    :cond_4f
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v17, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_15

    .line 2528
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/PointF;->y:F

    .line 2530
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2531
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2533
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    goto/16 :goto_5

    .line 2559
    :cond_50
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_6

    .line 2561
    :cond_51
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_53

    .line 2562
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_52

    .line 2563
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_6

    .line 2565
    :cond_52
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_6

    .line 2567
    :cond_53
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_55

    .line 2568
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_54

    .line 2569
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_6

    .line 2571
    :cond_54
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_6

    .line 2573
    :cond_55
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_17

    .line 2574
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_56

    .line 2575
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_6

    .line 2577
    :cond_56
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_6

    .line 2581
    :cond_57
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_59

    .line 2582
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_58

    .line 2583
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_6

    .line 2585
    :cond_58
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_6

    .line 2587
    :cond_59
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5b

    .line 2588
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_5a

    .line 2589
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_6

    .line 2591
    :cond_5a
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_6

    .line 2593
    :cond_5b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5d

    .line 2594
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_5c

    .line 2595
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_6

    .line 2597
    :cond_5c
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_6

    .line 2599
    :cond_5d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_17

    .line 2600
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_5e

    .line 2601
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_6

    .line 2603
    :cond_5e
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_6

    .line 2620
    :cond_5f
    if-eqz p8, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-nez v20, :cond_18

    .line 2621
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_60

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_65

    .line 2622
    :cond_60
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_62

    .line 2623
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 2680
    :cond_61
    :goto_e
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2681
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2683
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2684
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2686
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2687
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2689
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2690
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    goto/16 :goto_7

    .line 2624
    :cond_62
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_63

    .line 2625
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_e

    .line 2626
    :cond_63
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_64

    .line 2627
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_e

    .line 2628
    :cond_64
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_61

    .line 2629
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_e

    .line 2632
    :cond_65
    move/from16 v16, p7

    .line 2633
    .restart local v16    # "tmp_angle":F
    const/16 v20, 0x0

    cmpg-float v20, v16, v20

    if-gez v20, :cond_66

    .line 2634
    const/high16 v20, 0x43b40000    # 360.0f

    add-float v16, v16, v20

    .line 2637
    :cond_66
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_6a

    .line 2638
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_67

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_67

    .line 2639
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_e

    .line 2640
    :cond_67
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_68

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_68

    .line 2641
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_e

    .line 2642
    :cond_68
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_69

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_69

    .line 2643
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_e

    .line 2644
    :cond_69
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_61

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_61

    .line 2645
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_e

    .line 2647
    :cond_6a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_6e

    .line 2648
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_6b

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_6b

    .line 2649
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_e

    .line 2650
    :cond_6b
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_6c

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_6c

    .line 2651
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_e

    .line 2652
    :cond_6c
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_6d

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_6d

    .line 2653
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_e

    .line 2654
    :cond_6d
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_61

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_61

    .line 2655
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_e

    .line 2657
    :cond_6e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_72

    .line 2658
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_6f

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_6f

    .line 2659
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_e

    .line 2660
    :cond_6f
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_70

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_70

    .line 2661
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_e

    .line 2662
    :cond_70
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_71

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_71

    .line 2663
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_e

    .line 2664
    :cond_71
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_61

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_61

    .line 2665
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_e

    .line 2667
    :cond_72
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_61

    .line 2668
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_73

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_73

    .line 2669
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_e

    .line 2670
    :cond_73
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_74

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_74

    .line 2671
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_e

    .line 2672
    :cond_74
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_75

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_75

    .line 2673
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_e

    .line 2674
    :cond_75
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_61

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_61

    .line 2675
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_e

    .line 2714
    .end local v16    # "tmp_angle":F
    .restart local v15    # "right":F
    :cond_76
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v15, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_77

    .line 2715
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2717
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2718
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2720
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2721
    goto/16 :goto_8

    :cond_77
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v15, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_78

    .line 2722
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    iput v0, v5, Landroid/graphics/PointF;->x:F

    .line 2724
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2725
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2727
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2728
    goto/16 :goto_8

    :cond_78
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v15, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_1b

    .line 2729
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/PointF;->x:F

    .line 2731
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2732
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2734
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    goto/16 :goto_8

    .line 2760
    :cond_79
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_9

    .line 2762
    :cond_7a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_7c

    .line 2763
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_7b

    .line 2764
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_9

    .line 2766
    :cond_7b
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    .line 2768
    :cond_7c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_7e

    .line 2769
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_7d

    .line 2770
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    .line 2772
    :cond_7d
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    .line 2774
    :cond_7e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1d

    .line 2775
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_7f

    .line 2776
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    .line 2778
    :cond_7f
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_9

    .line 2782
    :cond_80
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_82

    .line 2783
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_81

    .line 2784
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_9

    .line 2786
    :cond_81
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_9

    .line 2788
    :cond_82
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_84

    .line 2789
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_83

    .line 2790
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    .line 2792
    :cond_83
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_9

    .line 2794
    :cond_84
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_86

    .line 2795
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_85

    .line 2796
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    .line 2798
    :cond_85
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    .line 2800
    :cond_86
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1d

    .line 2801
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_87

    .line 2802
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_9

    .line 2804
    :cond_87
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    .line 2821
    :cond_88
    if-eqz p8, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-nez v20, :cond_1e

    .line 2822
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_89

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_8e

    .line 2823
    :cond_89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_8b

    .line 2824
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 2881
    :cond_8a
    :goto_f
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2882
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2884
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2885
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2887
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2888
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2890
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2891
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    goto/16 :goto_a

    .line 2825
    :cond_8b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_8c

    .line 2826
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    .line 2827
    :cond_8c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_8d

    .line 2828
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    .line 2829
    :cond_8d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_8a

    .line 2830
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    .line 2833
    :cond_8e
    move/from16 v16, p7

    .line 2834
    .restart local v16    # "tmp_angle":F
    const/16 v20, 0x0

    cmpg-float v20, v16, v20

    if-gez v20, :cond_8f

    .line 2835
    const/high16 v20, 0x43b40000    # 360.0f

    add-float v16, v16, v20

    .line 2838
    :cond_8f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_93

    .line 2839
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_90

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_90

    .line 2840
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_f

    .line 2841
    :cond_90
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_91

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_91

    .line 2842
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_f

    .line 2843
    :cond_91
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_92

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_92

    .line 2844
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    .line 2845
    :cond_92
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_8a

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_8a

    .line 2846
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    .line 2848
    :cond_93
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_97

    .line 2849
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_94

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_94

    .line 2850
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    .line 2851
    :cond_94
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_95

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_95

    .line 2852
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_f

    .line 2853
    :cond_95
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_96

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_96

    .line 2854
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    .line 2855
    :cond_96
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_8a

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_8a

    .line 2856
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    .line 2858
    :cond_97
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_9b

    .line 2859
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_98

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_98

    .line 2860
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    .line 2861
    :cond_98
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_99

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_99

    .line 2862
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    .line 2863
    :cond_99
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_9a

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_9a

    .line 2864
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    .line 2865
    :cond_9a
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_8a

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_8a

    .line 2866
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    .line 2868
    :cond_9b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_8a

    .line 2869
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_9c

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_9c

    .line 2870
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_f

    .line 2871
    :cond_9c
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_9d

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_9d

    .line 2872
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    .line 2873
    :cond_9d
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_9e

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_9e

    .line 2874
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    .line 2875
    :cond_9e
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_8a

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_8a

    .line 2876
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    .line 2915
    .end local v16    # "tmp_angle":F
    .restart local v4    # "bottom":F
    :cond_9f
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v4, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a0

    .line 2916
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2918
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2919
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2921
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2922
    goto/16 :goto_b

    :cond_a0
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v4, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a1

    .line 2924
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    iput v0, v5, Landroid/graphics/PointF;->y:F

    .line 2926
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2927
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2929
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2931
    goto/16 :goto_b

    :cond_a1
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v4, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_21

    .line 2932
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/PointF;->y:F

    .line 2934
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2935
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2937
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    goto/16 :goto_b

    .line 2961
    :cond_a2
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 2963
    :cond_a3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_a5

    .line 2964
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a4

    .line 2965
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 2967
    :cond_a4
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 2969
    :cond_a5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_a7

    .line 2970
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a6

    .line 2971
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 2973
    :cond_a6
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 2975
    :cond_a7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_23

    .line 2976
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a8

    .line 2977
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 2979
    :cond_a8
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 2983
    :cond_a9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_ab

    .line 2984
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_aa

    .line 2985
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 2987
    :cond_aa
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 2989
    :cond_ab
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_ad

    .line 2990
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_ac

    .line 2991
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 2993
    :cond_ac
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 2995
    :cond_ad
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_af

    .line 2996
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_ae

    .line 2997
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 2999
    :cond_ae
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3001
    :cond_af
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_23

    .line 3002
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_b0

    .line 3003
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3005
    :cond_b0
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3009
    :cond_b1
    if-eqz p8, :cond_23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-nez v20, :cond_23

    .line 3010
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_b2

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_b6

    .line 3011
    :cond_b2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_b3

    .line 3012
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3013
    :cond_b3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_b4

    .line 3014
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3015
    :cond_b4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_b5

    .line 3016
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3017
    :cond_b5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_23

    .line 3018
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3021
    :cond_b6
    move/from16 v16, p7

    .line 3022
    .restart local v16    # "tmp_angle":F
    const/16 v20, 0x0

    cmpg-float v20, v16, v20

    if-gez v20, :cond_b7

    .line 3023
    const/high16 v20, 0x43b40000    # 360.0f

    add-float v16, v16, v20

    .line 3026
    :cond_b7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_bb

    .line 3027
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_b8

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_b8

    .line 3028
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3029
    :cond_b8
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_b9

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_b9

    .line 3030
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3031
    :cond_b9
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_ba

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_ba

    .line 3032
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3033
    :cond_ba
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_23

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_23

    .line 3034
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3036
    :cond_bb
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_bf

    .line 3037
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_bc

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_bc

    .line 3038
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3039
    :cond_bc
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_bd

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_bd

    .line 3040
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3041
    :cond_bd
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_be

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_be

    .line 3042
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3043
    :cond_be
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_23

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_23

    .line 3044
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3046
    :cond_bf
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_c3

    .line 3047
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_c0

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_c0

    .line 3048
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3049
    :cond_c0
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_c1

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_c1

    .line 3050
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3051
    :cond_c1
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_c2

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_c2

    .line 3052
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3053
    :cond_c2
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_23

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_23

    .line 3054
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3056
    :cond_c3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_23

    .line 3057
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_c4

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_c4

    .line 3058
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3059
    :cond_c4
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_c5

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_c5

    .line 3060
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3061
    :cond_c5
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_c6

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_c6

    .line 3062
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3063
    :cond_c6
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_23

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_23

    .line 3064
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3070
    .end local v4    # "bottom":F
    .end local v5    # "bottomleft":Landroid/graphics/PointF;
    .end local v6    # "bottomright":Landroid/graphics/PointF;
    .end local v7    # "deltaX":F
    .end local v8    # "deltaY":F
    .end local v9    # "left":F
    .end local v10    # "originalRect":Landroid/graphics/RectF;
    .end local v11    # "points":[F
    .end local v15    # "right":F
    .end local v16    # "tmp_angle":F
    .end local v17    # "top":F
    .end local v18    # "topleft":Landroid/graphics/PointF;
    .end local v19    # "topright":Landroid/graphics/PointF;
    :cond_c7
    if-eqz p2, :cond_23

    .line 3071
    new-instance v18, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 3072
    .restart local v18    # "topleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 3074
    new-instance v19, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-direct/range {v19 .. v21}, Landroid/graphics/PointF;-><init>(FF)V

    .line 3075
    .restart local v19    # "topright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 3077
    new-instance v5, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v5, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 3078
    .restart local v5    # "bottomleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 3080
    new-instance v6, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v6, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 3081
    .restart local v6    # "bottomright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 3083
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 3084
    .restart local v11    # "points":[F
    const/16 v20, 0x0

    aget v9, v11, v20

    .line 3085
    .restart local v9    # "left":F
    const/16 v20, 0x1

    aget v17, v11, v20

    .line 3086
    .restart local v17    # "top":F
    const/16 v20, 0x2

    aget v15, v11, v20

    .line 3087
    .restart local v15    # "right":F
    const/16 v20, 0x3

    aget v4, v11, v20

    .line 3089
    .restart local v4    # "bottom":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    cmpg-float v20, v9, v20

    if-ltz v20, :cond_c8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    cmpl-float v20, v15, v20

    if-gtz v20, :cond_c8

    .line 3090
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    cmpg-float v20, v17, v20

    if-ltz v20, :cond_c8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    cmpl-float v20, v4, v20

    if-lez v20, :cond_23

    .line 3091
    :cond_c8
    const/16 v20, 0x1

    goto/16 :goto_1
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 3770
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsClosed:Z

    return v0
.end method

.method public isContextMenuItemEnabled(I)Z
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 3872
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-nez v0, :cond_0

    .line 3873
    const/4 v0, 0x0

    .line 3876
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getItemEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public isContextMenuVisible()Z
    .locals 1

    .prologue
    .line 3923
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    return v0
.end method

.method public isDimEnabled()Z
    .locals 1

    .prologue
    .line 4018
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    return v0
.end method

.method public isObjectOutlineEnabled()Z
    .locals 1

    .prologue
    .line 4563
    sget-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectOutlineEnable:Z

    return v0
.end method

.method public isTouchEnabled()Z
    .locals 1

    .prologue
    .line 3952
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchEnable:Z

    return v0
.end method

.method protected onCanvasHoverEnter()V
    .locals 1

    .prologue
    .line 4567
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4568
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onObjectChanged()V

    .line 4570
    :cond_1
    return-void
.end method

.method protected onCheckTouchPosition(Landroid/graphics/RectF;IILcom/samsung/android/sdk/pen/document/SpenObjectBase;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;)V
    .locals 6
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .param p5, "touchState"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    .prologue
    .line 3099
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->checkTouchPosition(Landroid/graphics/RectF;IILcom/samsung/android/sdk/pen/document/SpenObjectBase;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;)V

    .line 3101
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 17
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 4059
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v12, 0x3

    if-ne v11, v12, :cond_1

    .line 4153
    :cond_0
    :goto_0
    return-void

    .line 4063
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mFirstDraw:Z

    if-eqz v11, :cond_2

    .line 4064
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mFirstDraw:Z

    .line 4066
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onPrepareDraw(Landroid/graphics/Canvas;)V

    .line 4069
    :cond_2
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v11

    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 4071
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 4078
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    if-eqz v11, :cond_3

    move-object/from16 v0, p0

    instance-of v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    if-nez v11, :cond_3

    .line 4079
    const/high16 v11, 0x40000000    # 2.0f

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 4082
    :cond_3
    const/4 v2, 0x0

    .line 4083
    .local v2, "activeRect":Landroid/graphics/RectF;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lt v6, v11, :cond_5

    .line 4105
    const/4 v6, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lt v6, v11, :cond_8

    .line 4146
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsObjectChange:Z

    if-eqz v11, :cond_0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v11, :cond_0

    .line 4147
    move-object/from16 v0, p0

    instance-of v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;

    if-eqz v11, :cond_4

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTrivialMovingEn:Z

    if-eqz v11, :cond_4

    .line 4148
    const-string v11, "SpenControlBase"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Up-Drawing Rect = "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4150
    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-interface {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onObjectChanged(Ljava/util/ArrayList;)V

    .line 4151
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsObjectChange:Z

    goto/16 :goto_0

    .line 4084
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v2

    .line 4085
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    .line 4086
    .local v3, "centerX":F
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    .line 4087
    .local v4, "centerY":F
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->Increase2MinimumRect(Landroid/graphics/RectF;)Z

    .line 4089
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    if-eqz v11, :cond_6

    move-object/from16 v0, p0

    instance-of v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    if-nez v11, :cond_6

    .line 4090
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 4091
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 4092
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->drawDimmingWindow(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V

    .line 4093
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 4095
    :cond_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    instance-of v11, v11, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v11, :cond_7

    .line 4096
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->drawHighlightStroke(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    .line 4099
    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 4100
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 4101
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDrawBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 4102
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 4083
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 4106
    .end local v3    # "centerX":F
    .end local v4    # "centerY":F
    :cond_8
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 4107
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v11

    float-to-int v11, v11

    int-to-float v3, v11

    .line 4108
    .restart local v3    # "centerX":F
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v11

    float-to-int v11, v11

    int-to-float v4, v11

    .line 4109
    .restart local v4    # "centerY":F
    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 4111
    .local v8, "objAng":F
    float-to-int v5, v8

    .line 4112
    .local v5, "displayAngle":I
    if-gez v5, :cond_9

    .line 4113
    add-int/lit16 v5, v5, 0x168

    .line 4115
    :cond_9
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v11

    if-eqz v11, :cond_a

    .line 4116
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    const/high16 v13, 0x42700000    # 60.0f

    mul-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float v12, v3, v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    const/high16 v14, 0x41f00000    # 30.0f

    mul-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    sub-float v13, v4, v13

    .line 4117
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    const/high16 v15, 0x42700000    # 60.0f

    mul-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    add-float/2addr v14, v3

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    const/high16 v16, 0x41f00000    # 30.0f

    mul-float v15, v15, v16

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    add-float/2addr v15, v4

    .line 4116
    invoke-virtual {v11, v12, v13, v14, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 4118
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    const/high16 v12, 0x40800000    # 4.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    mul-float/2addr v12, v13

    const/high16 v13, 0x40800000    # 4.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    mul-float/2addr v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v15, 0x4

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12, v13, v14}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 4119
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v12, 0x5

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v9

    .line 4120
    .local v9, "paintText":Landroid/graphics/Paint;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "\u00b0"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 4121
    invoke-virtual {v9}, Landroid/graphics/Paint;->descent()F

    move-result v12

    invoke-virtual {v9}, Landroid/graphics/Paint;->ascent()F

    move-result v13

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float v12, v4, v12

    .line 4120
    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v3, v12, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 4124
    .end local v9    # "paintText":Landroid/graphics/Paint;
    :cond_a
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    .line 4125
    .local v7, "index":I
    if-eq v6, v7, :cond_b

    const/4 v11, -0x1

    if-eq v7, v11, :cond_b

    .line 4126
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotateable(I)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 4127
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v12

    sub-float v10, v11, v12

    .line 4128
    .local v10, "rotateDiff":F
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v11

    add-float/2addr v11, v10

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v8

    .line 4134
    .end local v10    # "rotateDiff":F
    :cond_b
    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 4135
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v11

    if-nez v11, :cond_c

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v11

    if-nez v11, :cond_c

    .line 4136
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v11

    if-eqz v11, :cond_d

    .line 4138
    :cond_c
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v11, v12}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 4139
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v12, v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDrawObject(Landroid/graphics/Canvas;Landroid/graphics/Rect;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 4140
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/RectF;

    iget v13, v11, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/RectF;

    iget v14, v11, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/RectF;

    iget v15, v11, Landroid/graphics/RectF;->right:F

    .line 4141
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    .line 4140
    invoke-virtual {v12, v13, v14, v15, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 4143
    :cond_d
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 4105
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2

    .line 4130
    :cond_e
    const/4 v8, 0x0

    goto/16 :goto_3
.end method

.method protected onDrawBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 25
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "rect"    # Landroid/graphics/RectF;
    .param p3, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 3164
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v8, 0x1

    if-ne v3, v8, :cond_1

    .line 3228
    :cond_0
    :goto_0
    return-void

    .line 3168
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v3

    const/4 v8, 0x2

    if-eq v3, v8, :cond_5

    const/4 v13, 0x1

    .line 3169
    .local v13, "isEightPoint":Z
    :goto_1
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v15

    .line 3171
    .local v15, "isRotatable":Z
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_6

    const/4 v14, 0x1

    .line 3173
    .local v14, "isKeepRatio":Z
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v8, 0x2

    if-ne v3, v8, :cond_2

    .line 3174
    const/4 v13, 0x0

    .line 3175
    const/4 v15, 0x0

    .line 3178
    :cond_2
    if-eqz v15, :cond_4

    .line 3179
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    const/4 v8, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v3, v8, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v20

    .line 3180
    .local v20, "rotateRect":Landroid/graphics/RectF;
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    .line 3181
    .local v4, "startX":F
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    .line 3182
    .local v5, "startY":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v6

    .line 3183
    .local v6, "stopX":F
    move-object/from16 v0, p2

    iget v7, v0, Landroid/graphics/RectF;->top:F

    .line 3185
    .local v7, "stopY":F
    const-string v3, "handler_icon_rotate"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getDrawableImage(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 3186
    .local v11, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v11, :cond_4

    .line 3187
    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v8, 0x40000000    # 2.0f

    div-float v12, v3, v8

    .line 3188
    .local v12, "hx":F
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_3

    .line 3189
    const/high16 v3, 0x42340000    # 45.0f

    mul-float/2addr v3, v12

    const/high16 v8, 0x41f00000    # 30.0f

    div-float v12, v3, v8

    .line 3192
    :cond_3
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerX()F

    move-result v9

    .line 3193
    .local v9, "CenterX":F
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerY()F

    move-result v10

    .line 3194
    .local v10, "CenterY":F
    const/high16 v3, 0x40800000    # 4.0f

    sub-float v19, v12, v3

    .line 3195
    .local v19, "radius":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move/from16 v21, v0

    .line 3196
    new-instance v22, Landroid/graphics/PointF;

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerX()F

    move-result v23

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerY()F

    move-result v24

    invoke-direct/range {v22 .. v24}, Landroid/graphics/PointF;-><init>(FF)V

    .line 3195
    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v3, v8, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v16

    .line 3197
    .local v16, "knobPos":Landroid/graphics/PointF;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getTemplateProperty()Z

    .line 3198
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v18

    check-cast v18, Landroid/view/ViewGroup;

    .line 3199
    .local v18, "parentLayout":Landroid/view/ViewGroup;
    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/PointF;->x:F

    add-float v3, v3, v19

    const/4 v8, 0x0

    cmpl-float v3, v3, v8

    if-lez v3, :cond_4

    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/PointF;->x:F

    sub-float v3, v3, v19

    invoke-virtual/range {v18 .. v18}, Landroid/view/ViewGroup;->getWidth()I

    move-result v8

    int-to-float v8, v8

    cmpg-float v3, v3, v8

    if-gez v3, :cond_4

    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/PointF;->y:F

    add-float v3, v3, v19

    const/4 v8, 0x0

    cmpl-float v3, v3, v8

    if-lez v3, :cond_4

    .line 3200
    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/PointF;->y:F

    sub-float v3, v3, v19

    invoke-virtual/range {v18 .. v18}, Landroid/view/ViewGroup;->getHeight()I

    move-result v8

    int-to-float v8, v8

    cmpg-float v3, v3, v8

    if-gez v3, :cond_4

    .line 3201
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v8

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 3202
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/16 v8, 0x9

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v9, v10, v1, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 3207
    .end local v4    # "startX":F
    .end local v5    # "startY":F
    .end local v6    # "stopX":F
    .end local v7    # "stopY":F
    .end local v9    # "CenterX":F
    .end local v10    # "CenterY":F
    .end local v11    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v12    # "hx":F
    .end local v16    # "knobPos":Landroid/graphics/PointF;
    .end local v18    # "parentLayout":Landroid/view/ViewGroup;
    .end local v19    # "radius":F
    .end local v20    # "rotateRect":Landroid/graphics/RectF;
    :cond_4
    if-eqz v13, :cond_8

    .line 3208
    const/4 v11, 0x0

    .line 3209
    .restart local v11    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_7

    .line 3210
    const-string v3, "handler_icon"

    const/16 v8, 0x21

    .line 3211
    const/16 v21, 0x21

    .line 3210
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v3, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeImage(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 3217
    :goto_3
    if-eqz v11, :cond_0

    .line 3218
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 3219
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v11, v1, v2, v14}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->draw8Points(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    goto/16 :goto_0

    .line 3168
    .end local v11    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v13    # "isEightPoint":Z
    .end local v14    # "isKeepRatio":Z
    .end local v15    # "isRotatable":Z
    :cond_5
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 3171
    .restart local v13    # "isEightPoint":Z
    .restart local v15    # "isRotatable":Z
    :cond_6
    const/4 v14, 0x0

    goto/16 :goto_2

    .line 3213
    .restart local v11    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v14    # "isKeepRatio":Z
    :cond_7
    const-string v3, "handler_icon"

    const/16 v8, 0x16

    .line 3214
    const/16 v21, 0x16

    .line 3213
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v3, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeImage(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    goto :goto_3

    .line 3222
    .end local v11    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v17

    .line 3223
    .local v17, "paint":Landroid/graphics/Paint;
    const/high16 v3, 0x40800000    # 4.0f

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 3224
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/RectF;->left:F

    const/high16 v8, 0x40800000    # 4.0f

    sub-float/2addr v3, v8

    const/high16 v8, 0x3f800000    # 1.0f

    add-float/2addr v3, v8

    move-object/from16 v0, p2

    iget v8, v0, Landroid/graphics/RectF;->top:F

    const/high16 v21, 0x40800000    # 4.0f

    sub-float v8, v8, v21

    const/high16 v21, 0x3f800000    # 1.0f

    add-float v8, v8, v21

    .line 3225
    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    const/high16 v22, 0x40800000    # 4.0f

    add-float v21, v21, v22

    const/high16 v22, 0x3f800000    # 1.0f

    sub-float v21, v21, v22

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v22, v0

    const/high16 v23, 0x40800000    # 4.0f

    add-float v22, v22, v23

    const/high16 v23, 0x3f800000    # 1.0f

    sub-float v22, v22, v23

    .line 3224
    move-object/from16 v0, p2

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v3, v8, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 3226
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method protected onDrawObject(Landroid/graphics/Canvas;Landroid/graphics/Rect;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "rect"    # Landroid/graphics/Rect;
    .param p3, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 3445
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 3446
    .local v0, "tmpRect":Landroid/graphics/RectF;
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 3447
    iget v1, p2, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 3448
    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 3450
    :cond_0
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 3451
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 3452
    iget v1, p2, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 3455
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3456
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->Increase2MinimumRect(Landroid/graphics/RectF;)Z

    .line 3459
    :cond_2
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SM-P90"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3460
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 3465
    :goto_0
    return-void

    .line 3464
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 0
    .param p1, "flipDirection"    # I
    .param p2, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 3468
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 3582
    const/4 v0, 0x0

    return v0
.end method

.method protected onMenuSelected(I)V
    .locals 2
    .param p1, "itemId"    # I

    .prologue
    .line 3427
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 3428
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onMenuSelected(Ljava/util/ArrayList;I)V

    .line 3430
    :cond_0
    return-void
.end method

.method protected onObjectChanged()V
    .locals 24

    .prologue
    .line 3282
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mFirstDraw:Z

    if-eqz v2, :cond_1

    .line 3392
    :cond_0
    :goto_0
    return-void

    .line 3286
    :cond_1
    const/16 v17, 0x1

    .line 3287
    .local v17, "isTransaction":Z
    new-instance v20, Landroid/graphics/RectF;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/RectF;-><init>()V

    .line 3288
    .local v20, "objectRect":Landroid/graphics/RectF;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v3, v2, :cond_2

    .line 3322
    if-nez v17, :cond_9

    .line 3323
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    .line 3324
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v2, :cond_0

    .line 3325
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->updateContextMenu()V

    .line 3326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    goto :goto_0

    .line 3289
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v2

    if-nez v2, :cond_3

    .line 3290
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/graphics/RectF;

    .line 3292
    .local v21, "rectF":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3293
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObject(IZZZFFFZF)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3294
    const/16 v17, 0x0

    .line 3295
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 3288
    .end local v21    # "rectF":Landroid/graphics/RectF;
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 3297
    .restart local v21    # "rectF":Landroid/graphics/RectF;
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 3298
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObject(IZZZFFFZF)Z

    .line 3300
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3301
    const/4 v13, 0x0

    .line 3302
    .local v13, "deltaX":F
    const/4 v14, 0x0

    .line 3304
    .local v14, "deltaY":F
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->left:F

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-gez v2, :cond_7

    .line 3305
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->left:F

    neg-float v13, v2

    .line 3310
    :cond_5
    :goto_3
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->top:F

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-gez v2, :cond_8

    .line 3311
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->top:F

    neg-float v14, v2

    .line 3316
    :cond_6
    :goto_4
    move-object/from16 v0, v20

    invoke-virtual {v0, v13, v14}, Landroid/graphics/RectF;->offset(FF)V

    .line 3317
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    goto :goto_2

    .line 3306
    :cond_7
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_5

    .line 3307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/RectF;->right:F

    sub-float v13, v2, v4

    goto :goto_3

    .line 3312
    :cond_8
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_6

    .line 3313
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    sub-float v14, v2, v4

    goto :goto_4

    .line 3331
    .end local v13    # "deltaX":F
    .end local v14    # "deltaY":F
    .end local v21    # "rectF":Landroid/graphics/RectF;
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v2, :cond_b

    .line 3332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 3333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3335
    const/16 v22, 0x0

    .line 3336
    .local v22, "rotateDiff":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v15, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    .line 3337
    .local v15, "index":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_a

    .line 3338
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v4

    sub-float v22, v2, v4

    .line 3341
    :cond_a
    const/16 v16, 0x0

    .line 3342
    .local v16, "isResize2Threshold":Z
    const/4 v3, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v3, v2, :cond_c

    .line 3386
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsObjectChange:Z

    .line 3389
    .end local v15    # "index":I
    .end local v16    # "isResize2Threshold":Z
    .end local v22    # "rotateDiff":F
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    .line 3391
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    goto/16 :goto_0

    .line 3343
    .restart local v15    # "index":I
    .restart local v16    # "isResize2Threshold":Z
    .restart local v22    # "rotateDiff":F
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v2

    if-nez v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_e

    .line 3344
    :cond_d
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resize2Threshold(I)Z

    move-result v16

    .line 3347
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3348
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 3349
    .local v19, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isFlipEnabled()Z

    move-result v2

    if-nez v2, :cond_10

    .line 3351
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_f

    .line 3352
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v23, v0

    .line 3353
    .local v23, "temp":F
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v20

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 3354
    move/from16 v0, v23

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 3356
    .end local v23    # "temp":F
    :cond_f
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_10

    .line 3357
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    .line 3358
    .restart local v23    # "temp":F
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, v20

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 3359
    move/from16 v0, v23

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 3363
    .end local v23    # "temp":F
    :cond_10
    move-object/from16 v0, p0

    instance-of v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;

    if-nez v2, :cond_11

    move-object/from16 v0, p0

    instance-of v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    if-eqz v2, :cond_15

    .line 3364
    :cond_11
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v12

    .line 3365
    .local v12, "currObjectRect":Landroid/graphics/RectF;
    if-eqz v16, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_14

    .line 3366
    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 3367
    const/4 v2, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 3375
    .end local v12    # "currObjectRect":Landroid/graphics/RectF;
    :goto_6
    const/16 v18, 0x0

    .line 3376
    .local v18, "objAng":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move/from16 v18, v0

    .line 3378
    if-eq v3, v15, :cond_12

    .line 3379
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v2

    add-float v2, v2, v22

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v18

    .line 3381
    :cond_12
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotateable(I)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 3382
    move/from16 v0, v18

    float-to-int v2, v0

    int-to-float v2, v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRotation(F)V

    .line 3342
    :cond_13
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    .line 3369
    .end local v18    # "objAng":F
    .restart local v12    # "currObjectRect":Landroid/graphics/RectF;
    :cond_14
    const/4 v2, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    goto :goto_6

    .line 3372
    .end local v12    # "currObjectRect":Landroid/graphics/RectF;
    :cond_15
    const/4 v2, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    goto :goto_6
.end method

.method protected onPrepareDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 4022
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    if-eqz v0, :cond_0

    .line 4023
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    .line 4025
    :cond_0
    return-void
.end method

.method protected onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 3433
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 3434
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 3436
    :cond_0
    return-void
.end method

.method protected onRequestScroll(FF)V
    .locals 1
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 3276
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 3277
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestScroll(FF)V

    .line 3279
    :cond_0
    return-void
.end method

.method protected onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1
    .param p1, "angle"    # F
    .param p2, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 3439
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 3440
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 3442
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 20
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 3484
    invoke-super/range {p0 .. p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 3486
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-nez v15, :cond_1

    .line 3549
    :cond_0
    :goto_0
    return-void

    .line 3490
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v15

    if-eqz v15, :cond_0

    .line 3494
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 3495
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    invoke-interface/range {v15 .. v16}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3497
    const/4 v14, 0x0

    .line 3499
    .local v14, "scrollDirty":Z
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->setDirty()V

    .line 3500
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    .line 3502
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v15, v15, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v6, v15, Landroid/graphics/PointF;->x:F

    .line 3503
    .local v6, "curPanX":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v15, v15, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v7, v15, Landroid/graphics/PointF;->y:F

    .line 3505
    .local v7, "curPanY":F
    invoke-virtual/range {p0 .. p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getPanKey(II)Ljava/lang/String;

    move-result-object v9

    .line 3506
    .local v9, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    invoke-virtual {v15, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Point;

    .line 3507
    .local v11, "pan":Landroid/graphics/Point;
    if-eqz v11, :cond_2

    .line 3508
    iget v15, v11, Landroid/graphics/Point;->x:I

    int-to-float v15, v15

    sub-float/2addr v15, v6

    iget v0, v11, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v16, v16, v7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRequestScroll(FF)V

    .line 3509
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    invoke-virtual {v15, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3510
    const/4 v14, 0x1

    .line 3513
    :cond_2
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 3514
    .local v4, "absoluteScreenRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v15, v15, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v15, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3516
    const/4 v15, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v10

    .line 3517
    .local v10, "objectBoundBox":Landroid/graphics/RectF;
    if-eqz v10, :cond_0

    .line 3520
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 3521
    .local v3, "absoluteObjectBoundBox":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10, v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3523
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    move-object/from16 v0, p0

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getPanKey(II)Ljava/lang/String;

    move-result-object v16

    new-instance v17, Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    .line 3524
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v19, v0

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-direct/range {v17 .. v19}, Landroid/graphics/Point;-><init>(II)V

    .line 3523
    invoke-virtual/range {v15 .. v17}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3526
    invoke-virtual {v4, v3}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v5

    .line 3527
    .local v5, "containFlag":Z
    invoke-virtual {v3, v4}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    move-result v8

    .line 3528
    .local v8, "intersectFlag":Z
    if-eqz v8, :cond_3

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isOutOfViewEnabled()Z

    move-result v15

    if-nez v15, :cond_6

    if-nez v5, :cond_6

    .line 3529
    :cond_3
    const/4 v12, 0x0

    .local v12, "panX":F
    const/4 v13, 0x0

    .line 3530
    .local v13, "panY":F
    iget v15, v10, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v16, v0

    cmpl-float v15, v15, v16

    if-lez v15, :cond_7

    .line 3531
    iget v15, v10, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v16, v0

    div-float v13, v15, v16

    .line 3536
    :cond_4
    :goto_1
    iget v15, v10, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v16, v0

    cmpg-float v15, v15, v16

    if-gez v15, :cond_8

    .line 3537
    iget v15, v10, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v16, v0

    div-float v12, v15, v16

    .line 3542
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRequestScroll(FF)V

    .line 3543
    const/4 v14, 0x1

    .line 3546
    .end local v12    # "panX":F
    .end local v13    # "panY":F
    :cond_6
    if-eqz v14, :cond_0

    .line 3547
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    goto/16 :goto_0

    .line 3532
    .restart local v12    # "panX":F
    .restart local v13    # "panY":F
    :cond_7
    iget v15, v10, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v16, v0

    cmpg-float v15, v15, v16

    if-gez v15, :cond_4

    .line 3533
    iget v15, v10, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v16, v0

    div-float v13, v15, v16

    goto/16 :goto_1

    .line 3538
    :cond_8
    iget v15, v10, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v16, v0

    cmpl-float v15, v15, v16

    if-lez v15, :cond_5

    .line 3539
    iget v15, v10, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v16, v0

    div-float v12, v15, v16

    goto/16 :goto_2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 3687
    if-eqz p1, :cond_0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 3733
    :cond_0
    :goto_0
    return v0

    .line 3690
    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchEnable:Z

    if-eqz v2, :cond_0

    .line 3694
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v1, :cond_2

    .line 3695
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    move v0, v1

    .line 3696
    goto :goto_0

    .line 3698
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTransactionTouchEvent:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->check(Landroid/view/MotionEvent;)V

    .line 3700
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 3729
    :cond_3
    :goto_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 3730
    goto :goto_0

    .line 3702
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    if-nez v0, :cond_4

    .line 3703
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    .line 3705
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_1

    .line 3709
    :pswitch_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFirstTouch:Z

    if-eqz v0, :cond_5

    .line 3710
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    mul-float/2addr v0, v2

    .line 3711
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    .line 3710
    add-float/2addr v0, v2

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 3711
    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    cmpl-double v0, v2, v4

    if-ltz v0, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    if-eq v0, v1, :cond_3

    .line 3712
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->hide()V

    goto :goto_1

    .line 3716
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->hideMovingView()V

    goto :goto_1

    .line 3720
    :pswitch_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->hideMovingView()V

    .line 3721
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsClosed:Z

    if-nez v0, :cond_3

    .line 3722
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onCanvasHoverEnter()V

    goto :goto_1

    .line 3733
    :cond_6
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 3700
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 4574
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 4575
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4576
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->hide()V

    .line 4577
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->setDirty()V

    .line 4578
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    .line 4580
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 4038
    if-eqz p1, :cond_0

    .line 4039
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fitRotateAngle2BorderAngle()V

    .line 4040
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    .line 4041
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 4042
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    .line 4044
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    .line 4045
    return-void
.end method

.method protected onZoom()V
    .locals 0

    .prologue
    .line 713
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    .line 714
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    .line 715
    return-void
.end method

.method protected relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/RectF;
    .param p3, "coordinateInfo"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .prologue
    .line 754
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 764
    :cond_0
    :goto_0
    return-void

    .line 758
    :cond_1
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 759
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    .line 760
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    .line 759
    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 761
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 762
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    .line 763
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    .line 762
    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method public setContextMenu(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3838
    .local p1, "menuItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iput-object p1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    .line 3839
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->setDirty()V

    .line 3840
    return-void
.end method

.method public setContextMenuItemEnabled(IZ)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "enable"    # Z

    .prologue
    .line 3853
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-nez v0, :cond_0

    .line 3858
    :goto_0
    return-void

    .line 3857
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->setItemEnabled(IZ)V

    goto :goto_0
.end method

.method public setContextMenuVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 3902
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    if-eq v0, p1, :cond_0

    .line 3903
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iput-boolean p1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    .line 3904
    if-eqz p1, :cond_1

    .line 3905
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    .line 3910
    :cond_0
    :goto_0
    return-void

    .line 3907
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->hide()V

    goto :goto_0
.end method

.method public setDimEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 4005
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    .line 4006
    return-void
.end method

.method protected setListener(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    .prologue
    .line 4506
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    .line 4507
    return-void
.end method

.method public setMinResizeRect(Landroid/graphics/RectF;)V
    .locals 3
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v2, 0x0

    .line 3966
    if-nez p1, :cond_0

    .line 3967
    const/4 v0, 0x7

    const-string v1, " rect must be not null"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 3970
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    if-eqz v0, :cond_1

    .line 3971
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_1

    .line 3972
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_1

    .line 3973
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 3976
    :cond_1
    return-void
.end method

.method protected setObjectList(Ljava/util/ArrayList;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const v13, 0x38d1b717    # 1.0E-4f

    const/high16 v12, 0x40000000    # 2.0f

    const/4 v11, 0x0

    .line 1968
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    .line 1970
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v0, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1971
    .local v0, "defaultMaxHeight":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v1, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1973
    .local v1, "defaultMaxWidth":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    .line 1974
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6, v11, v11, v11, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1975
    .local v6, "unionRectF":Landroid/graphics/RectF;
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v11, v11, v11, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1977
    .local v5, "unionMaxRectF":Landroid/graphics/RectF;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 1994
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v7, v6}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 1995
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMaximumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v7, v5}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1997
    .end local v5    # "unionMaxRectF":Landroid/graphics/RectF;
    .end local v6    # "unionRectF":Landroid/graphics/RectF;
    :cond_0
    return-void

    .line 1977
    .restart local v5    # "unionMaxRectF":Landroid/graphics/RectF;
    .restart local v6    # "unionRectF":Landroid/graphics/RectF;
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1979
    .local v4, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    new-instance v8, Landroid/graphics/RectF;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinWidth()F

    move-result v9

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinHeight()F

    move-result v10

    invoke-direct {v8, v11, v11, v9, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v6, v8}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 1981
    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMaxWidth()F

    move-result v3

    .line 1982
    .local v3, "maxWidth":F
    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMaxHeight()F

    move-result v2

    .line 1983
    .local v2, "maxHeight":F
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpg-float v8, v8, v13

    if-ltz v8, :cond_2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v8

    int-to-float v9, v1

    mul-float/2addr v9, v12

    cmpl-float v8, v8, v9

    if-lez v8, :cond_3

    .line 1984
    :cond_2
    int-to-float v8, v1

    mul-float v3, v8, v12

    .line 1986
    :cond_3
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpg-float v8, v8, v13

    if-ltz v8, :cond_4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v8

    int-to-float v9, v0

    mul-float/2addr v9, v12

    cmpl-float v8, v8, v9

    if-lez v8, :cond_5

    .line 1987
    :cond_4
    int-to-float v8, v0

    mul-float v2, v8, v12

    .line 1989
    :cond_5
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8, v11, v11, v3, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v5, v8}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 1991
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    new-instance v9, Landroid/graphics/RectF;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    invoke-direct {v9, v10}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setObjectOutlineEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 4550
    sput-boolean p1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectOutlineEnable:Z

    .line 4551
    return-void
.end method

.method public setStyle(I)V
    .locals 1
    .param p1, "style"    # I

    .prologue
    .line 3806
    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-gt p1, v0, :cond_0

    .line 3807
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    .line 3811
    :goto_0
    return-void

    .line 3809
    :cond_0
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public setTouchEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 3938
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchEnable:Z

    .line 3939
    return-void
.end method

.method protected updateContextMenu()V
    .locals 10

    .prologue
    .line 875
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    if-nez v5, :cond_1

    .line 922
    :cond_0
    :goto_0
    return-void

    .line 879
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 880
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mDirtyFlag:Z

    if-eqz v5, :cond_3

    .line 881
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v5, :cond_2

    .line 882
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->close()Z

    .line 883
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    const/4 v6, 0x0

    iput-object v6, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .line 886
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 887
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    new-instance v6, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    .line 888
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mSelectContextMenuListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    invoke-direct {v6, v7, p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;-><init>(Landroid/content/Context;Landroid/view/View;Ljava/util/ArrayList;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;)V

    .line 887
    iput-object v6, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .line 892
    :cond_3
    const/4 v5, -0x1

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v3

    .line 893
    .local v3, "rectF":Landroid/graphics/RectF;
    if-eqz v3, :cond_0

    .line 896
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 897
    .local v2, "rect":Landroid/graphics/Rect;
    const/16 v4, 0x19

    .line 898
    .local v4, "top_diff":I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    .line 899
    const/16 v4, 0x26

    .line 902
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isRotatable()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 903
    mul-int/lit8 v4, v4, 0x3

    .line 906
    :cond_5
    iget v5, v3, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    iget v6, v3, Landroid/graphics/RectF;->top:F

    int-to-float v7, v4

    sub-float/2addr v6, v7

    float-to-int v6, v6

    iget v7, v3, Landroid/graphics/RectF;->right:F

    float-to-int v7, v7

    iget v8, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v8, v8

    .line 907
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;)F

    move-result v9

    float-to-int v9, v9

    add-int/2addr v8, v9

    .line 906
    invoke-virtual {v2, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 909
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v5, :cond_0

    .line 912
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->calculateContextMenuPosition(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 913
    .local v0, "contextMenuPositionRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_6

    .line 914
    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 917
    :cond_6
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 918
    .local v1, "offsetRect":Landroid/graphics/Rect;
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 919
    iget v5, v1, Landroid/graphics/Rect;->left:I

    iget v6, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2, v5, v6}, Landroid/graphics/Rect;->offset(II)V

    .line 920
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v5, v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->setRect(Landroid/graphics/Rect;)V

    goto/16 :goto_0
.end method

.method protected updateRectList()V
    .locals 9

    .prologue
    .line 2024
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 2025
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2027
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 2028
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2047
    :cond_0
    return-void

    .line 2028
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 2030
    .local v1, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 2031
    .local v0, "dstRect":Landroid/graphics/RectF;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 2033
    .local v2, "srcRect":Landroid/graphics/RectF;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 2035
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v4, :cond_2

    .line 2036
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2038
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v0, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2040
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->Increase2MinimumRect(Landroid/graphics/RectF;)Z

    .line 2042
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2043
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v5, v0, Landroid/graphics/RectF;->left:F

    iget v6, v0, Landroid/graphics/RectF;->top:F

    iget v7, v0, Landroid/graphics/RectF;->right:F

    iget v8, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0
.end method
