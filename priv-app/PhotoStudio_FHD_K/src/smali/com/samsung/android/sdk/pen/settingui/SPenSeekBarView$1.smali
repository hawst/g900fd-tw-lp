.class Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;
.super Ljava/lang/Object;
.source "SPenSeekBarView.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    .line 335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6
    .param p1, "seekbar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const v4, 0x3ee66666    # 0.45f

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 352
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSPenSeekBarChangeListner:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$2(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)I

    move-result v1

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;->onProgressChanged(Landroid/widget/SeekBar;IZI)V

    .line 354
    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoDecrement:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoIncrement:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSPenSeekBarChangeListner:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-interface {v0, v3, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;->onUpdate(ZI)V

    .line 360
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 361
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 362
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoIncrement:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 363
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 371
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    if-nez v0, :cond_4

    .line 372
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$6(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 373
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoDecrement:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 374
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$6(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 383
    :goto_2
    return-void

    .line 357
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSPenSeekBarChangeListner:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-interface {v0, v2, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;->onUpdate(ZI)V

    goto :goto_0

    .line 365
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setAlpha(F)V

    goto :goto_1

    .line 368
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 369
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_1

    .line 376
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$6(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setAlpha(F)V

    goto :goto_2

    .line 379
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$6(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 380
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$6(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_2
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekbar"    # Landroid/widget/SeekBar;

    .prologue
    .line 346
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSPenSeekBarChangeListner:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$2(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;->onStartTrackingTouch(Landroid/widget/SeekBar;I)V

    .line 347
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4
    .param p1, "seekbar"    # Landroid/widget/SeekBar;

    .prologue
    .line 339
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$0(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/SeekBar$OnSeekBarChangeListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    .line 340
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSPenSeekBarChangeListner:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$2(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;->onStopTrackingTouch(Landroid/widget/SeekBar;I)V

    .line 341
    return-void
.end method
