.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$2;
.super Ljava/lang/Object;
.source "SpenSettingSelectionLayout2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->mPopupMaxButton:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;->onPopup(Z)V

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->mPopupMinButton:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;->onPopup(Z)V

    goto :goto_0
.end method
