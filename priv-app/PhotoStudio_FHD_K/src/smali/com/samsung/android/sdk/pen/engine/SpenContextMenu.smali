.class public Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
.super Ljava/lang/Object;
.source "SpenContextMenu.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;,
        Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;,
        Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;
    }
.end annotation


# static fields
.field private static final DEFAULT_HEIGHT:I = 0x3d

.field private static final DEFAULT_HEIGHT_2:I = 0x4e

.field private static final DEFAULT_HEIGHT_2_N1:I = 0x5c

.field private static final DEFAULT_HEIGHT_2_VIENNA:I = 0x61

.field private static final DEFAULT_HEIGHT_N1:I = 0x48

.field private static final DEFAULT_HEIGHT_VIENNA:I = 0x50

.field protected static final DEFAULT_ITEM_WIDTH:I = 0x48

.field protected static final DEFAULT_ITEM_WIDTH_N1:I = 0x62

.field private static final DEFAULT_MINIMUM_DELAY:I = 0x3e8

.field protected static final DEFAULT_SEPERATOR_WIDTH:I = 0x1

.field protected static final DEFAULT_SEPERATOR_WIDTH_N1:I = 0x2

.field private static final DEFAULT_TEXT_SIZE:I = 0xc

.field private static final DEFAULT_TEXT_SIZE_N1:I = 0x11

.field private static final DEFAULT_WIDTH:I = 0x136

.field private static final MAX_RATE_PER_WIDTH:F = 0.9f

.field protected static final QUICK_POPUP_BG:Ljava/lang/String; = "quick_popup_bg"

.field protected static final QUICK_POPUP_BG_PRESS:Ljava/lang/String; = "quick_popup_bg_press"

.field protected static final QUICK_POPUP_BG_PRESS_VIENNA:Ljava/lang/String; = "quick_popup_bg_press_vienna"

.field protected static final QUICK_POPUP_DV:Ljava/lang/String; = "quick_popup_dv"

.field protected static final QUICK_POPUP_DV_VIENNA:Ljava/lang/String; = "quick_popup_dv_vienna"

.field private static final SDK_VERSION:I

.field protected static final SHADOW_BORDER:Ljava/lang/String; = "shadow_border"

.field protected static final SHADOW_BORDER_N1:Ljava/lang/String; = "shadow_border_n1"

.field protected static final SHADOW_BORDER_VIENNA:Ljava/lang/String; = "shadow_border_vienna"

.field protected static final TW_QUICK_BUBBLE_DIVIDER_HOLO_LIGHT:Ljava/lang/String; = "tw_quick_bubble_divider_holo_light"

.field public static final TYPE_DEFAULT:I = 0x0

.field public static final TYPE_WIDE:I = 0x1

.field private static mType:I


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

.field private mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

.field private final mContext:Landroid/content/Context;

.field private mCount:F

.field private mDelay:I

.field private final mHandler:Landroid/os/Handler;

.field private mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

.field private mIsNormalMode:Z

.field private mIsShowFullMenuItem:Z

.field private mIsViennaModel:Z

.field mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

.field private mNumItem:I

.field private mParent:Landroid/view/View;

.field private mPopupView:Landroid/view/View;

.field private mPopupWindow:Landroid/widget/PopupWindow;

.field private mPrevRect:Landroid/graphics/Rect;

.field private mRect:Landroid/graphics/Rect;

.field private mResourceSDK:Landroid/content/res/Resources;

.field private mScrollFlag:Z

.field private mScrollTimer:Ljava/util/Timer;

.field private mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

.field private final mSpenHorizontalListViewListener:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

.field private mTimer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I

    .line 117
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Ljava/util/ArrayList;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;)V
    .locals 34
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/view/View;
    .param p4, "selectListener"    # Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;",
            ">;",
            "Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 452
    .local p3, "menu":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/16 v29, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    .line 109
    const/16 v29, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    .line 110
    new-instance v29, Landroid/os/Handler;

    invoke-direct/range {v29 .. v29}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHandler:Landroid/os/Handler;

    .line 111
    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mDelay:I

    .line 112
    const/16 v29, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    .line 113
    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollFlag:Z

    .line 114
    const/16 v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z

    .line 115
    const/16 v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsShowFullMenuItem:Z

    .line 420
    new-instance v29, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSpenHorizontalListViewListener:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

    .line 883
    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F

    .line 454
    if-nez p3, :cond_0

    .line 455
    const/16 v29, 0x7

    const-string v30, " The list of menu items must be not null"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 458
    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    .line 459
    if-eqz p1, :cond_8

    .line 460
    const/16 v22, 0x0

    .line 461
    .local v22, "res":Landroid/content/res/Resources;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    .line 462
    invoke-virtual/range {v22 .. v22}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    .line 464
    .local v10, "dm":Landroid/util/DisplayMetrics;
    const-string v29, "accessibility"

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/view/accessibility/AccessibilityManager;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 467
    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z

    .line 470
    iget v0, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v29, v0

    iget v0, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v30, v0

    move/from16 v0, v29

    move/from16 v1, v30

    if-ge v0, v1, :cond_9

    .line 471
    iget v5, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 476
    .local v5, "canvasWidth":I
    :goto_0
    const/16 v29, 0x640

    move/from16 v0, v29

    if-ne v5, v0, :cond_1

    iget v0, v10, Landroid/util/DisplayMetrics;->density:F

    move/from16 v29, v0

    const/high16 v30, 0x40000000    # 2.0f

    cmpl-float v29, v29, v30

    if-nez v29, :cond_1

    .line 477
    const/16 v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z

    .line 480
    :cond_1
    new-instance v29, Landroid/graphics/Rect;

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x136

    const/16 v33, 0x3d

    invoke-direct/range {v29 .. v33}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    .line 481
    new-instance v29, Landroid/graphics/Rect;

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x136

    const/16 v33, 0x3d

    invoke-direct/range {v29 .. v33}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    .line 483
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v18

    .line 485
    .local v18, "manager":Landroid/content/pm/PackageManager;
    :try_start_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v18

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 490
    :goto_1
    new-instance v29, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    const/16 v30, 0x0

    invoke-direct/range {v29 .. v30}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)V

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    .line 491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;Ljava/util/ArrayList;)V

    .line 492
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->isNormalMode()Z

    move-result v29

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z

    .line 494
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    .line 496
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    .line 497
    const/16 v29, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    .line 499
    new-instance v16, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 500
    .local v16, "linearLayout":Landroid/widget/RelativeLayout;
    const/16 v24, 0x0

    .line 501
    .local v24, "shadowImage":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v29, v0

    if-eqz v29, :cond_2

    .line 502
    sget v29, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    const/16 v30, 0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_b

    .line 504
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z

    move/from16 v29, v0

    if-eqz v29, :cond_a

    .line 505
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v29, v0

    const-string v30, "quick_popup_bg"

    const-string v31, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v29 .. v32}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    .line 510
    .local v12, "id":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v12}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v24

    .line 516
    .end local v12    # "id":I
    :cond_2
    :goto_3
    sget v29, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I

    const/16 v30, 0x10

    move/from16 v0, v29

    move/from16 v1, v30

    if-ge v0, v1, :cond_c

    if-eqz v24, :cond_c

    .line 517
    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 522
    :cond_3
    :goto_4
    const/16 v27, 0x136

    .line 523
    .local v27, "width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v29, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v29 .. v29}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v25

    .line 524
    .local v25, "size":I
    const/16 v19, 0x0

    .line 531
    .local v19, "numItem":I
    sget v29, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    const/16 v30, 0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_d

    .line 532
    const/16 v15, 0x62

    .line 533
    .local v15, "itemWidth":I
    const/16 v23, 0x2

    .line 534
    .local v23, "seperatorWidth":I
    const/16 v13, 0x48

    .line 535
    .local v13, "itemHeight":I
    const/16 v14, 0x5c

    .line 536
    .local v14, "itemHeight_2":I
    const/16 v9, 0x8

    .line 538
    .local v9, "delta_padding":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z

    move/from16 v29, v0

    if-eqz v29, :cond_4

    .line 539
    const/16 v13, 0x50

    .line 540
    const/16 v14, 0x61

    .line 541
    const/4 v9, 0x5

    .line 552
    :cond_4
    :goto_5
    const/16 v29, 0x4

    move/from16 v0, v25

    move/from16 v1, v29

    if-ge v0, v1, :cond_e

    .line 553
    move/from16 v19, v25

    .line 554
    mul-int v27, v15, v19

    .line 574
    :goto_6
    const/16 v29, 0x1

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v30, v0

    move/from16 v0, v29

    move/from16 v1, v30

    invoke-static {v0, v1, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v29

    move/from16 v0, v29

    float-to-int v0, v0

    move/from16 v27, v0

    .line 575
    add-int/lit8 v29, v19, -0x1

    mul-int v29, v29, v23

    add-int v27, v27, v29

    .line 577
    new-instance v20, Landroid/graphics/Rect;

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x438

    const/16 v32, 0x438

    move-object/from16 v0, v20

    move/from16 v1, v29

    move/from16 v2, v30

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 578
    .local v20, "outRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    move-object/from16 v29, v0

    if-eqz v29, :cond_5

    .line 579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 582
    :cond_5
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->width()I

    move-result v21

    .line 583
    .local v21, "pWidth":I
    if-eqz v21, :cond_7

    .line 584
    move/from16 v28, v27

    .line 585
    .local v28, "widthAdapt":I
    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v29, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v30, v0

    div-float v29, v29, v30

    const v30, 0x3f666666    # 0.9f

    cmpl-float v29, v29, v30

    if-lez v29, :cond_7

    .line 587
    :cond_6
    if-gtz v19, :cond_12

    .line 597
    :goto_7
    if-lez v19, :cond_13

    .line 598
    move/from16 v27, v28

    .line 606
    .end local v28    # "widthAdapt":I
    :cond_7
    :goto_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z

    move/from16 v29, v0

    if-eqz v29, :cond_14

    .line 607
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v29, v0

    move/from16 v0, v27

    move-object/from16 v1, v29

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 608
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v29, v0

    const/16 v30, 0x1

    int-to-float v0, v13

    move/from16 v31, v0

    move/from16 v0, v30

    move/from16 v1, v31

    invoke-static {v0, v1, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v30

    move/from16 v0, v30

    float-to-int v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    move-object/from16 v1, v29

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 614
    :goto_9
    new-instance v29, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    const/16 v30, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, p1

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    .line 616
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSpenHorizontalListViewListener:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

    move-object/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;)V

    .line 618
    new-instance v17, Landroid/view/ViewGroup$LayoutParams;

    const/16 v29, -0x1

    .line 619
    const/16 v30, -0x1

    .line 618
    move-object/from16 v0, v17

    move/from16 v1, v29

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 620
    .local v17, "listLayoutParam":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 621
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    invoke-virtual/range {v29 .. v33}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setPadding(IIII)V

    .line 623
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setFadingEdgeLength(I)V

    .line 624
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setVerticalScrollBarEnabled(Z)V

    .line 625
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setOverScrollMode(I)V

    .line 626
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-object/from16 v29, v0

    new-instance v30, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    invoke-virtual/range {v29 .. v30}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 627
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setScrollBarStyle(I)V

    .line 629
    mul-int v26, v15, v25

    .line 630
    .local v26, "totalWidth":I
    const/16 v29, 0x1

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v30, v0

    move/from16 v0, v29

    move/from16 v1, v30

    invoke-static {v0, v1, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v29

    move/from16 v0, v29

    float-to-int v0, v0

    move/from16 v26, v0

    .line 631
    add-int/lit8 v29, v25, -0x1

    mul-int v29, v29, v23

    add-int v26, v26, v29

    .line 632
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-object/from16 v29, v0

    sub-int v30, v26, v27

    invoke-virtual/range {v29 .. v30}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setMaxX(I)V

    .line 635
    new-instance v7, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-direct {v7, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 636
    .local v7, "contentLayout":Landroid/widget/RelativeLayout;
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 637
    const/16 v29, -0x1

    const/16 v30, -0x1

    .line 636
    move/from16 v0, v29

    move/from16 v1, v30

    invoke-direct {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 638
    .local v8, "contentLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 639
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 640
    const/16 v29, 0xfa

    const/16 v30, 0xfa

    const/16 v31, 0xfa

    invoke-static/range {v29 .. v31}, Landroid/graphics/Color;->rgb(III)I

    move-result v29

    move/from16 v0, v29

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 641
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 643
    const/16 v29, 0x1

    int-to-float v0, v9

    move/from16 v30, v0

    move/from16 v0, v29

    move/from16 v1, v30

    invoke-static {v0, v1, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v29

    move/from16 v0, v29

    float-to-int v9, v0

    .line 644
    move-object/from16 v0, v16

    invoke-virtual {v0, v9, v9, v9, v9}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 646
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    .line 648
    new-instance v29, Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Rect;->width()I

    move-result v31

    mul-int/lit8 v32, v9, 0x2

    add-int v31, v31, v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/graphics/Rect;->height()I

    move-result v32

    .line 649
    mul-int/lit8 v33, v9, 0x2

    add-int v32, v32, v33

    invoke-direct/range {v29 .. v32}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    .line 648
    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 651
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v29, v0

    const/16 v30, -0x1

    invoke-virtual/range {v29 .. v30}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 652
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v29, v0

    new-instance v30, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual/range {v29 .. v30}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 653
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 654
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 656
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v29, v0

    new-instance v30, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    invoke-virtual/range {v29 .. v30}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 678
    .end local v5    # "canvasWidth":I
    .end local v7    # "contentLayout":Landroid/widget/RelativeLayout;
    .end local v8    # "contentLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v9    # "delta_padding":I
    .end local v10    # "dm":Landroid/util/DisplayMetrics;
    .end local v13    # "itemHeight":I
    .end local v14    # "itemHeight_2":I
    .end local v15    # "itemWidth":I
    .end local v16    # "linearLayout":Landroid/widget/RelativeLayout;
    .end local v17    # "listLayoutParam":Landroid/view/ViewGroup$LayoutParams;
    .end local v18    # "manager":Landroid/content/pm/PackageManager;
    .end local v19    # "numItem":I
    .end local v20    # "outRect":Landroid/graphics/Rect;
    .end local v21    # "pWidth":I
    .end local v22    # "res":Landroid/content/res/Resources;
    .end local v23    # "seperatorWidth":I
    .end local v24    # "shadowImage":Landroid/graphics/drawable/Drawable;
    .end local v25    # "size":I
    .end local v26    # "totalWidth":I
    .end local v27    # "width":I
    :cond_8
    return-void

    .line 473
    .restart local v10    # "dm":Landroid/util/DisplayMetrics;
    .restart local v22    # "res":Landroid/content/res/Resources;
    :cond_9
    iget v5, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    .restart local v5    # "canvasWidth":I
    goto/16 :goto_0

    .line 486
    .restart local v18    # "manager":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v11

    .line 487
    .local v11, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/16 v29, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    goto/16 :goto_1

    .line 507
    .end local v11    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v16    # "linearLayout":Landroid/widget/RelativeLayout;
    .restart local v24    # "shadowImage":Landroid/graphics/drawable/Drawable;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v29, v0

    const-string v30, "shadow_border_n1"

    const-string v31, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v29 .. v32}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    .restart local v12    # "id":I
    goto/16 :goto_2

    .line 512
    .end local v12    # "id":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v30, v0

    const-string v31, "shadow_border"

    const-string v32, "drawable"

    .line 513
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v33

    .line 512
    invoke-virtual/range {v30 .. v33}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v30

    invoke-virtual/range {v29 .. v30}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v24

    goto/16 :goto_3

    .line 518
    :cond_c
    if-eqz v24, :cond_3

    .line 519
    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 545
    .restart local v19    # "numItem":I
    .restart local v25    # "size":I
    .restart local v27    # "width":I
    :cond_d
    const/16 v15, 0x48

    .line 546
    .restart local v15    # "itemWidth":I
    const/16 v23, 0x1

    .line 547
    .restart local v23    # "seperatorWidth":I
    const/16 v13, 0x3d

    .line 548
    .restart local v13    # "itemHeight":I
    const/16 v14, 0x4e

    .line 549
    .restart local v14    # "itemHeight_2":I
    const/4 v9, 0x5

    .restart local v9    # "delta_padding":I
    goto/16 :goto_5

    .line 555
    :cond_e
    const/16 v29, 0x6

    move/from16 v0, v25

    move/from16 v1, v29

    if-ge v0, v1, :cond_10

    .line 556
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    .line 557
    .local v6, "config":Landroid/content/res/Configuration;
    iget v0, v6, Landroid/content/res/Configuration;->orientation:I

    move/from16 v29, v0

    const/16 v30, 0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_f

    .line 558
    const/16 v19, 0x4

    .line 559
    mul-int v27, v15, v19

    .line 560
    goto/16 :goto_6

    .line 561
    :cond_f
    move/from16 v19, v25

    .line 562
    mul-int v27, v15, v19

    .line 564
    goto/16 :goto_6

    .line 565
    .end local v6    # "config":Landroid/content/res/Configuration;
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    .line 566
    .restart local v6    # "config":Landroid/content/res/Configuration;
    iget v0, v6, Landroid/content/res/Configuration;->orientation:I

    move/from16 v29, v0

    const/16 v30, 0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_11

    .line 567
    const/16 v19, 0x4

    .line 568
    mul-int v27, v15, v19

    .line 569
    goto/16 :goto_6

    .line 570
    :cond_11
    const/16 v19, 0x6

    .line 571
    mul-int v27, v15, v19

    goto/16 :goto_6

    .line 588
    .end local v6    # "config":Landroid/content/res/Configuration;
    .restart local v20    # "outRect":Landroid/graphics/Rect;
    .restart local v21    # "pWidth":I
    .restart local v28    # "widthAdapt":I
    :cond_12
    add-int/lit8 v19, v19, -0x1

    .line 589
    mul-int v28, v15, v19

    .line 590
    const/16 v29, 0x1

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v30, v0

    move/from16 v0, v29

    move/from16 v1, v30

    invoke-static {v0, v1, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v29

    move/from16 v0, v29

    float-to-int v0, v0

    move/from16 v28, v0

    .line 591
    add-int/lit8 v29, v19, -0x1

    mul-int v29, v29, v23

    add-int v28, v28, v29

    .line 592
    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v29, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v30, v0

    div-float v29, v29, v30

    const v30, 0x3f666666    # 0.9f

    cmpg-float v29, v29, v30

    if-gtz v29, :cond_6

    goto/16 :goto_7

    .line 600
    :cond_13
    const/16 v19, 0x1

    .line 601
    move/from16 v27, v15

    .line 602
    const/16 v29, 0x1

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v30, v0

    move/from16 v0, v29

    move/from16 v1, v30

    invoke-static {v0, v1, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v29

    move/from16 v0, v29

    float-to-int v0, v0

    move/from16 v27, v0

    goto/16 :goto_8

    .line 610
    .end local v28    # "widthAdapt":I
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v29, v0

    move/from16 v0, v27

    move-object/from16 v1, v29

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 611
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v29, v0

    const/16 v30, 0x1

    int-to-float v0, v14

    move/from16 v31, v0

    move/from16 v0, v30

    move/from16 v1, v31

    invoke-static {v0, v1, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v30

    move/from16 v0, v30

    float-to-int v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    move-object/from16 v1, v29

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_9
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsShowFullMenuItem:Z

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Z)V
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollFlag:Z

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z

    return v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$12()I
    .locals 1

    .prologue
    .line 99
    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I

    return v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    return v0
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mDelay:I

    return v0
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;I)V
    .locals 0

    .prologue
    .line 936
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->updateTimer(I)V

    return-void
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)F
    .locals 1

    .prologue
    .line 883
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F

    return v0
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;F)V
    .locals 0

    .prologue
    .line 883
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F

    return-void
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Ljava/util/Timer;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollFlag:Z

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$8()I
    .locals 1

    .prologue
    .line 117
    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    return v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z

    return v0
.end method

.method public static getType()I
    .locals 1

    .prologue
    .line 276
    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    return v0
.end method

.method private isNormalMode()Z
    .locals 15

    .prologue
    .line 686
    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    if-nez v13, :cond_0

    .line 687
    const/4 v13, 0x1

    .line 742
    :goto_0
    return v13

    .line 689
    :cond_0
    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v13

    if-nez v13, :cond_1

    .line 690
    const/4 v13, 0x1

    goto :goto_0

    .line 692
    :cond_1
    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    if-nez v13, :cond_2

    .line 693
    const/4 v13, 0x1

    goto :goto_0

    .line 695
    :cond_2
    new-instance v9, Landroid/widget/TextView;

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-direct {v9, v13}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 697
    .local v9, "textView":Landroid/widget/TextView;
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 698
    const/4 v13, -0x2

    const/4 v14, -0x2

    .line 697
    invoke-direct {v8, v13, v14}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 699
    .local v8, "textParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v13, 0xe

    invoke-virtual {v8, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 701
    sget v13, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_3

    .line 702
    const-string v13, "#1e1e1e"

    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 703
    const/4 v13, 0x1

    const/high16 v14, 0x41880000    # 17.0f

    invoke-virtual {v9, v13, v14}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 708
    :goto_1
    invoke-virtual {v9}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    .line 709
    .local v5, "paint":Landroid/graphics/Paint;
    invoke-virtual {v5, v5}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 714
    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 715
    .local v6, "res":Landroid/content/res/Resources;
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 716
    .local v1, "dm":Landroid/util/DisplayMetrics;
    sget v13, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_4

    .line 717
    const/4 v13, 0x1

    const/high16 v14, 0x42c40000    # 98.0f

    invoke-static {v13, v14, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v13

    float-to-int v10, v13

    .line 722
    .local v10, "viewWidth":I
    :goto_2
    const/4 v13, 0x1

    const/high16 v14, 0x40400000    # 3.0f

    invoke-static {v13, v14, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v13

    float-to-int v13, v13

    mul-int/lit8 v13, v13, 0x2

    sub-int/2addr v10, v13

    .line 724
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lt v2, v13, :cond_5

    .line 742
    const/4 v13, 0x1

    goto :goto_0

    .line 705
    .end local v1    # "dm":Landroid/util/DisplayMetrics;
    .end local v2    # "i":I
    .end local v5    # "paint":Landroid/graphics/Paint;
    .end local v6    # "res":Landroid/content/res/Resources;
    .end local v10    # "viewWidth":I
    :cond_3
    const/high16 v13, -0x1000000

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 706
    const/4 v13, 0x1

    const/high16 v14, 0x41400000    # 12.0f

    invoke-virtual {v9, v13, v14}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_1

    .line 719
    .restart local v1    # "dm":Landroid/util/DisplayMetrics;
    .restart local v5    # "paint":Landroid/graphics/Paint;
    .restart local v6    # "res":Landroid/content/res/Resources;
    :cond_4
    const/4 v13, 0x1

    const/high16 v14, 0x42900000    # 72.0f

    invoke-static {v13, v14, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v13

    float-to-int v10, v13

    .restart local v10    # "viewWidth":I
    goto :goto_2

    .line 725
    .restart local v2    # "i":I
    :cond_5
    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-object v7, v13, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    .line 726
    .local v7, "str":Ljava/lang/String;
    if-nez v7, :cond_6

    .line 727
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 729
    :cond_6
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v4

    .line 731
    .local v4, "length":I
    new-array v12, v4, [F

    .line 732
    .local v12, "widths":[F
    const/4 v13, 0x0

    invoke-virtual {v5, v7, v13, v4, v12}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    move-result v0

    .line 733
    .local v0, "count":I
    const/4 v11, 0x0

    .line 734
    .local v11, "width":F
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_4
    if-lt v3, v0, :cond_7

    .line 738
    int-to-float v13, v10

    cmpg-float v13, v13, v11

    if-gez v13, :cond_8

    .line 739
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 735
    :cond_7
    aget v13, v12, v3

    add-float/2addr v11, v13

    .line 734
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 724
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method private playScrollAnimation(I)V
    .locals 11
    .param p1, "delay"    # I

    .prologue
    const/4 v1, 0x1

    .line 887
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 888
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 890
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 934
    :goto_0
    return-void

    .line 894
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 895
    .local v9, "res":Landroid/content/res/Resources;
    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    .line 898
    .local v6, "dm":Landroid/util/DisplayMetrics;
    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    if-ne v0, v1, :cond_2

    .line 899
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x62

    int-to-float v7, v0

    .line 900
    .local v7, "lvWidth":F
    invoke-static {v1, v7, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    .line 901
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    add-float/2addr v7, v0

    .line 907
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v7, v0

    .line 909
    move v8, v7

    .line 910
    .local v8, "lvWidthAct":F
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v1, v0, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v10

    .line 912
    .local v10, "step":F
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    .line 913
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    invoke-direct {v1, p0, v8, v10}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;FF)V

    .line 933
    const-wide/16 v2, 0x64

    int-to-long v4, p1

    .line 913
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    goto :goto_0

    .line 903
    .end local v7    # "lvWidth":F
    .end local v8    # "lvWidthAct":F
    .end local v10    # "step":F
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x48

    int-to-float v7, v0

    .line 904
    .restart local v7    # "lvWidth":F
    invoke-static {v1, v7, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    .line 905
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x1

    int-to-float v0, v0

    add-float/2addr v7, v0

    goto :goto_1
.end method

.method public static setType(I)V
    .locals 0
    .param p0, "type"    # I

    .prologue
    .line 288
    sput p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    .line 289
    return-void
.end method

.method private updateContextMenuLocation()V
    .locals 22

    .prologue
    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v18

    if-eqz v18, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 294
    .local v12, "res":Landroid/content/res/Resources;
    invoke-virtual {v12}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    .line 296
    .local v6, "dm":Landroid/util/DisplayMetrics;
    const/16 v16, 0x136

    .line 297
    .local v16, "width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 305
    .local v14, "size":I
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsShowFullMenuItem:Z

    .line 307
    sget v18, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_6

    .line 308
    const/16 v9, 0x62

    .line 309
    .local v9, "itemWidth":I
    const/4 v13, 0x2

    .line 310
    .local v13, "seperatorWidth":I
    const/16 v7, 0x48

    .line 311
    .local v7, "itemHeight":I
    const/16 v8, 0x5c

    .line 312
    .local v8, "itemHeight_2":I
    const/16 v5, 0x8

    .line 314
    .local v5, "delta_padding":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z

    move/from16 v18, v0

    if-eqz v18, :cond_0

    .line 315
    const/16 v7, 0x50

    .line 316
    const/16 v8, 0x61

    .line 317
    const/4 v5, 0x5

    .line 327
    :cond_0
    :goto_0
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    .line 328
    const/16 v18, 0x4

    move/from16 v0, v18

    if-ge v14, v0, :cond_7

    .line 329
    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    .line 330
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    move/from16 v18, v0

    mul-int v16, v9, v18

    .line 355
    :goto_1
    const/16 v18, 0x6

    move/from16 v0, v18

    if-le v14, v0, :cond_1

    .line 356
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsShowFullMenuItem:Z

    .line 359
    :cond_1
    const/16 v18, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v0, v1, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v18

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v16, v0

    .line 360
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    mul-int v18, v18, v13

    add-int v16, v16, v18

    .line 362
    new-instance v10, Landroid/graphics/Rect;

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x438

    const/16 v21, 0x438

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v10, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 363
    .local v10, "outRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    .line 364
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 366
    :cond_2
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v11

    .line 367
    .local v11, "pWidth":I
    if-eqz v11, :cond_4

    .line 368
    move/from16 v17, v16

    .line 369
    .local v17, "widthAdapt":I
    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v18, v0

    int-to-float v0, v11

    move/from16 v19, v0

    div-float v18, v18, v19

    const v19, 0x3f666666    # 0.9f

    cmpl-float v18, v18, v19

    if-lez v18, :cond_4

    .line 370
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsShowFullMenuItem:Z

    .line 373
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    move/from16 v18, v0

    if-gtz v18, :cond_b

    .line 383
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    move/from16 v18, v0

    if-lez v18, :cond_c

    .line 384
    move/from16 v16, v17

    .line 393
    .end local v17    # "widthAdapt":I
    :cond_4
    :goto_3
    mul-int v15, v9, v14

    .line 394
    .local v15, "totalWidth":I
    const/16 v18, 0x1

    int-to-float v0, v15

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v0, v1, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v18

    move/from16 v0, v18

    float-to-int v15, v0

    .line 395
    add-int/lit8 v18, v14, -0x1

    mul-int v18, v18, v13

    add-int v15, v15, v18

    .line 396
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-object/from16 v18, v0

    sub-int v19, v15, v16

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setMaxX(I)V

    .line 398
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z

    move/from16 v18, v0

    if-eqz v18, :cond_d

    .line 399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    add-int v19, v19, v16

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 400
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    const/16 v20, 0x1

    int-to-float v0, v7

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v0, v1, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 407
    :goto_4
    const/16 v18, 0x1

    int-to-float v0, v5

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v0, v1, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v18

    move/from16 v0, v18

    float-to-int v5, v0

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->width()I

    move-result v19

    mul-int/lit8 v20, v5, 0x2

    add-int v19, v19, v20

    invoke-virtual/range {v18 .. v19}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v19

    mul-int/lit8 v20, v5, 0x2

    add-int v19, v19, v20

    invoke-virtual/range {v18 .. v19}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 418
    .end local v5    # "delta_padding":I
    .end local v6    # "dm":Landroid/util/DisplayMetrics;
    .end local v7    # "itemHeight":I
    .end local v8    # "itemHeight_2":I
    .end local v9    # "itemWidth":I
    .end local v10    # "outRect":Landroid/graphics/Rect;
    .end local v11    # "pWidth":I
    .end local v12    # "res":Landroid/content/res/Resources;
    .end local v13    # "seperatorWidth":I
    .end local v14    # "size":I
    .end local v15    # "totalWidth":I
    .end local v16    # "width":I
    :cond_5
    return-void

    .line 320
    .restart local v6    # "dm":Landroid/util/DisplayMetrics;
    .restart local v12    # "res":Landroid/content/res/Resources;
    .restart local v14    # "size":I
    .restart local v16    # "width":I
    :cond_6
    const/16 v9, 0x48

    .line 321
    .restart local v9    # "itemWidth":I
    const/4 v13, 0x1

    .line 322
    .restart local v13    # "seperatorWidth":I
    const/16 v7, 0x3d

    .line 323
    .restart local v7    # "itemHeight":I
    const/16 v8, 0x4e

    .line 324
    .restart local v8    # "itemHeight_2":I
    const/4 v5, 0x5

    .restart local v5    # "delta_padding":I
    goto/16 :goto_0

    .line 331
    :cond_7
    const/16 v18, 0x6

    move/from16 v0, v18

    if-ge v14, v0, :cond_9

    .line 332
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    .line 333
    .local v4, "config":Landroid/content/res/Configuration;
    iget v0, v4, Landroid/content/res/Configuration;->orientation:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 334
    const/16 v18, 0x4

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    .line 335
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    move/from16 v18, v0

    mul-int v16, v9, v18

    .line 337
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsShowFullMenuItem:Z

    goto/16 :goto_1

    .line 339
    :cond_8
    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    .line 340
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    move/from16 v18, v0

    mul-int v16, v9, v18

    .line 342
    goto/16 :goto_1

    .line 343
    .end local v4    # "config":Landroid/content/res/Configuration;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    .line 344
    .restart local v4    # "config":Landroid/content/res/Configuration;
    iget v0, v4, Landroid/content/res/Configuration;->orientation:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_a

    .line 345
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsShowFullMenuItem:Z

    .line 347
    const/16 v18, 0x4

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    .line 348
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    move/from16 v18, v0

    mul-int v16, v9, v18

    .line 349
    goto/16 :goto_1

    .line 350
    :cond_a
    const/16 v18, 0x6

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    .line 351
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    move/from16 v18, v0

    mul-int v16, v9, v18

    goto/16 :goto_1

    .line 374
    .end local v4    # "config":Landroid/content/res/Configuration;
    .restart local v10    # "outRect":Landroid/graphics/Rect;
    .restart local v11    # "pWidth":I
    .restart local v17    # "widthAdapt":I
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    .line 375
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    move/from16 v18, v0

    mul-int v17, v9, v18

    .line 376
    const/16 v18, 0x1

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v0, v1, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v18

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v17, v0

    .line 377
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    mul-int v18, v18, v13

    add-int v17, v17, v18

    .line 378
    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v18, v0

    int-to-float v0, v11

    move/from16 v19, v0

    div-float v18, v18, v19

    const v19, 0x3f666666    # 0.9f

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_3

    goto/16 :goto_2

    .line 386
    :cond_c
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    .line 387
    move/from16 v16, v9

    .line 388
    const/16 v18, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v0, v1, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v18

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v16, v0

    goto/16 :goto_3

    .line 402
    .end local v17    # "widthAdapt":I
    .restart local v15    # "totalWidth":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    add-int v19, v19, v16

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 403
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    .line 404
    const/16 v20, 0x1

    int-to-float v0, v8

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v0, v1, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    add-int v19, v19, v20

    .line 403
    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_4
.end method

.method private updateTimer(I)V
    .locals 4
    .param p1, "delay"    # I

    .prologue
    .line 937
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 938
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 941
    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    .line 942
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    .line 953
    int-to-long v2, p1

    .line 942
    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 954
    return-void
.end method


# virtual methods
.method public close()Z
    .locals 1

    .prologue
    .line 754
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->hide()V

    .line 755
    const/4 v0, 0x1

    return v0
.end method

.method public getItemEnabled(I)Z
    .locals 4
    .param p1, "id"    # I

    .prologue
    const/4 v1, 0x0

    .line 816
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    if-nez v2, :cond_1

    .line 826
    :cond_0
    :goto_0
    return v1

    .line 820
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    .line 821
    .local v0, "contextMenuItemInfo":Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;
    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    if-ne v3, p1, :cond_2

    .line 822
    iget-boolean v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    goto :goto_0
.end method

.method protected getPopupHeight()F
    .locals 1

    .prologue
    .line 1628
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method protected getPopupMenu()Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 1632
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 1633
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 1636
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 766
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 978
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 980
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 981
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 983
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F

    .line 984
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 986
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 987
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    .line 989
    :cond_1
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 877
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 878
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 880
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setItemEnabled(IZ)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "enable"    # Z

    .prologue
    .line 792
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_1

    .line 803
    :cond_0
    :goto_0
    return-void

    .line 795
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 800
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 801
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Landroid/widget/BaseAdapter;

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 795
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    .line 796
    .local v0, "contextMenuItemInfo":Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;
    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    if-ne v2, p1, :cond_2

    .line 797
    iput-boolean p2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    goto :goto_1
.end method

.method public setRect(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 778
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 779
    return-void
.end method

.method public show()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 835
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    if-nez v1, :cond_1

    .line 865
    :cond_0
    :goto_0
    return-void

    .line 839
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->updateContextMenuLocation()V

    .line 841
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 842
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-ne v1, v2, :cond_2

    .line 843
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-eq v1, v2, :cond_0

    .line 846
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->hide()V

    .line 849
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 851
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    if-le v1, v2, :cond_4

    .line 852
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 853
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2, v6, v3, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 864
    :goto_1
    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->playScrollAnimation(I)V

    goto/16 :goto_0

    .line 855
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v2

    sub-int v0, v1, v2

    .line 856
    .local v0, "left":I
    if-gez v0, :cond_5

    .line 857
    const/4 v0, 0x0

    .line 860
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 861
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2, v6, v0, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_1
.end method

.method public show(I)V
    .locals 0
    .param p1, "delay"    # I

    .prologue
    .line 965
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->show()V

    .line 967
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mDelay:I

    .line 969
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->updateTimer(I)V

    .line 970
    return-void
.end method
