.class Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;
.super Ljava/lang/Object;
.source "SpenControlTextBox.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V
    .locals 0

    .prologue
    .line 1235
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;)V
    .locals 0

    .prologue
    .line 1235
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1238
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 1243
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1248
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 1252
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1257
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 1261
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mReceiveActionDown:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1287
    :cond_0
    :goto_0
    return v4

    .line 1265
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 1266
    .local v0, "objectTextBox":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1267
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->isReadOnlyEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1268
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->bringToFront()V

    .line 1269
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->enableTextInput(Z)V

    .line 1270
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 1271
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setEditable(Z)V

    .line 1273
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setStyle(I)V

    .line 1274
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v1, :cond_2

    .line 1275
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->hide()V

    .line 1280
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getStyle()I

    move-result v1

    if-eq v1, v5, :cond_0

    .line 1281
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v1, :cond_0

    .line 1282
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->updateContextMenu()V

    .line 1283
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    goto :goto_0
.end method
