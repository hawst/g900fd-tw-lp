.class public Lcom/samsung/android/sdk/pen/pen/preload/NativePen;
.super Ljava/lang/Object;
.source "NativePen.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;


# instance fields
.field private mIs64:Z

.field public final nativeNativePen:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->mIs64:Z

    .line 35
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->mIs64:Z

    .line 36
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->Native_init()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->nativeNativePen:J

    .line 38
    return-void

    .line 35
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private Native_construct(J)Z
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 497
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 498
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->native_construct(J)Z

    move-result v0

    .line 500
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->native_construct(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_init()J
    .locals 2

    .prologue
    .line 490
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 491
    invoke-static {}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->native_init_64()J

    move-result-wide v0

    .line 493
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->native_init()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private Native_setObject(JLjava/lang/Object;)V
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 504
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 505
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->native_setobject(JLjava/lang/Object;)V

    .line 508
    :goto_0
    return-void

    .line 507
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->native_construct(I)Z

    goto :goto_0
.end method

.method private static native native_command(IILjava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_command(JILjava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_construct(I)Z
.end method

.method private static native native_construct(J)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_finalize(J)V
.end method

.method private static native native_init()I
.end method

.method private static native native_init_64()J
.end method

.method private static native native_setobject(ILjava/lang/Object;)V
.end method

.method private static native native_setobject(JLjava/lang/Object;)V
.end method


# virtual methods
.method public construct()V
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->nativeNativePen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->Native_construct(J)Z

    .line 50
    return-void
.end method

.method public draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 184
    return-void
.end method

.method public getAdvancedSetting()Ljava/lang/String;
    .locals 1

    .prologue
    .line 400
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    return-object v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 336
    const/4 v0, 0x0

    return v0
.end method

.method public getMaxSettingValue()F
    .locals 1

    .prologue
    .line 304
    const/4 v0, 0x0

    return v0
.end method

.method public getMinSettingValue()F
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x0

    return v0
.end method

.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->nativeNativePen:J

    return-wide v0
.end method

.method public getPenAttribute(I)Z
    .locals 1
    .param p1, "attribute"    # I

    .prologue
    .line 466
    const/4 v0, 0x0

    return v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 122
    return-void
.end method

.method public getSize()F
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x0

    return v0
.end method

.method public getStrokeRect([Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
    .locals 1
    .param p1, "points"    # [Landroid/graphics/PointF;
    .param p2, "pressures"    # [F
    .param p3, "timestamps"    # [I
    .param p4, "size"    # F
    .param p5, "isCurvable"    # Z
    .param p6, "advanced"    # Ljava/lang/String;

    .prologue
    .line 450
    const/4 v0, 0x0

    return-object v0
.end method

.method public hideAdvancedSetting()V
    .locals 0

    .prologue
    .line 434
    return-void
.end method

.method public isCurveEnabled()Z
    .locals 1

    .prologue
    .line 368
    const/4 v0, 0x0

    return v0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    return-void
.end method

.method public onUnload()V
    .locals 0

    .prologue
    .line 92
    return-void
.end method

.method public redrawPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "allEvent"    # Landroid/view/MotionEvent;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 196
    return-void
.end method

.method public setAdvancedSetting(Ljava/lang/String;)V
    .locals 0
    .param p1, "advancedSetting"    # Ljava/lang/String;

    .prologue
    .line 386
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 226
    return-void
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 322
    return-void
.end method

.method public setCurveEnabled(Z)V
    .locals 0
    .param p1, "curve"    # Z

    .prologue
    .line 354
    return-void
.end method

.method public setObject(Ljava/lang/Object;)V
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->nativeNativePen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->Native_setObject(JLjava/lang/Object;)V

    .line 62
    return-void
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 138
    return-void
.end method

.method public setReferenceBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 242
    return-void
.end method

.method public setSize(F)V
    .locals 0
    .param p1, "size"    # F

    .prologue
    .line 258
    return-void
.end method

.method public showAdvancedSetting(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 418
    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 168
    const/4 v0, 0x1

    return v0
.end method
