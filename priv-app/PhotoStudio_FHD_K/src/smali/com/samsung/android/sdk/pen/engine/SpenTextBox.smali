.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.super Landroid/view/View;
.source "SpenTextBox.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    }
.end annotation


# static fields
.field private static final BITMAP_CUE_BOTTOM:I = 0x1

.field private static final BITMAP_CUE_MAX:I = 0x2

.field private static final BITMAP_CUE_TOP:I = 0x0

.field private static final BITMAP_HANDLE_CENTER_BOTTOM:I = 0x5

.field private static final BITMAP_HANDLE_CENTER_TOP:I = 0x4

.field private static final BITMAP_HANDLE_LEFT_BOTTOM:I = 0x1

.field private static final BITMAP_HANDLE_LEFT_TOP:I = 0x0

.field private static final BITMAP_HANDLE_MAX:I = 0x6

.field private static final BITMAP_HANDLE_RIGHT_BOTTOM:I = 0x3

.field private static final BITMAP_HANDLE_RIGHT_TOP:I = 0x2

.field private static final BORDER_STATIC_LINE_WIDTH:I = 0x4

.field private static final CR_CHAR:C = '\r'

.field private static final CUE_BOTTOM:I = 0x1

.field private static final CUE_MAX:I = 0x2

.field private static final CUE_TOP:I = 0x0

.field private static final CURSOR_WIDTH:I = 0x2

.field private static final DBG:Z = false

.field private static final DEFAULT_CONTEXTMENU_DELAY:I = 0x1388

.field private static final DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

.field private static final DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

.field private static final DEFAULT_LINE_SPACING:F = 1.3f

.field private static final DEFAULT_PADDING:I = 0x5

.field private static final DEFAULT_SIZE_FONT:F = 36.0f

.field private static final DEFAULT_SPAN:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

.field private static final DRAGTEXT_MAX_LENGTH:I = 0x14

.field private static final EMOTION_CHAR_BEGIN:C = '\udc00'

.field private static final EMOTION_CHAR_END:C = '\udff0'

.field private static final EMOTION_CHAR_PREFIX_1:C = '\ud83c'

.field private static final EMOTION_CHAR_PREFIX_2:C = '\ud83d'

.field private static final EPSILON:F = 1.0E-7f

.field private static final HANDLE_END:I = 0x2

.field private static final HANDLE_MAX:I = 0x3

.field private static final HANDLE_MIDDLE:I = 0x1

.field private static final HANDLE_START:I = 0x0

.field static final ID_COPY:I = 0x1020021

.field static final ID_CUT:I = 0x1020020

.field static final ID_PASTE:I = 0x1020022

.field static final ID_SELECT_ALL:I = 0x102001f

.field private static final ITALIC_ANGLE_IN_DEGREE:F = 15.0f

.field private static final LF_CHAR:C = '\n'

.field private static final PRESS_AREA_COLOR_DARK:I = -0xe69570

.field private static final PRESS_AREA_COLOR_LIGHT:I = -0x7f86350e

.field private static final SAMSUNG_INPUT_METHOD_ID:Ljava/lang/String; = "com.sec.android.inputmethod/.SamsungKeypad"

.field private static final SCROLL_BAR_COLOR:I = -0x7fa0a0a1

.field private static final SCROLL_BAR_WIDTH:I = 0x4

.field private static final SIN_15_DEGREE:F

.field private static final SPACE_CHAR:C = ' '

.field private static final TAB_CHAR:C = '\t'

.field private static final TAG:Ljava/lang/String; = "SpenTextBox"

.field private static final TEXT_INPUT_LIMITED:I = 0x1388

.field private static final VOICE_INPUT_METHOD_ID:Ljava/lang/String; = "com.google.android.googlequicksearchbox/com.google.android.voicesearch.ime.VoiceInputMethodService"

.field private static mContextMenuItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected static mHoverCursorEnable:Z


# instance fields
.field private CONTROL_BOTTOM_MARGIN:I

.field private CONTROL_LEFT_MARGIN:I

.field private CONTROL_RIGHT_MARGIN:I

.field private CONTROL_TOP_MARGIN:I

.field private final MAX_OBJECT_HEIGHT:F

.field private final MAX_OBJECT_WIDTH:F

.field private final hideDeletePopupRunable:Ljava/lang/Runnable;

.field private isAltPressed:Z

.field private isCtrlPressed:Z

.field private isShiftPressed:Z

.field private lastText:Ljava/lang/String;

.field private last_index:I

.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mActionDownTime:J

.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

.field private mCanvasScroll:F

.field private mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

.field private mClipboardMessage:Landroid/widget/Toast;

.field private mContext:Landroid/content/Context;

.field private final mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

.field private mContextMenuVisible:Z

.field private mCueBitmap:[Landroid/graphics/Bitmap;

.field private mCueButton:[Landroid/widget/ImageButton;

.field private mCurrentOrientation:I

.field private mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

.field private mCursorHandleVisible:Z

.field private mCursorVisible:Z

.field private mDeletePopupHandler:Landroid/os/Handler;

.field private mDeleteTextPopup:Landroid/widget/TextView;

.field private mDeltaY:F

.field private mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

.field private mDragText:Landroid/widget/TextView;

.field private mEditable:Landroid/text/Editable;

.field private mEndCurCut:I

.field private mEndIndex:[I

.field private mEndIndexInput:I

.field protected mFirstDraw:Z

.field protected mGestureDetector:Landroid/view/GestureDetector;

.field private mHandleBitmap:[Landroid/graphics/Bitmap;

.field private mHandleButton:[Landroid/widget/ImageButton;

.field private mHandleButtonRect:[Landroid/graphics/RectF;

.field mHandleListener:[Landroid/view/View$OnTouchListener;

.field private mHandlePressed:Z

.field private mHardKeyboardHidden:I

.field private mHasWindowFocus:Z

.field private mHighLightPaint:Landroid/graphics/Paint;

.field private mHighlightPathBogus:Z

.field private mHoverDrawable:Landroid/graphics/drawable/Drawable;

.field private mHoverIconID:I

.field private mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

.field private mInputConnection:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

.field private final mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mIs64:Z

.field private mIsCanvasScroll:Z

.field private mIsChangedByKeyShortCut:Z

.field private mIsCheckCursorOnScroll:Z

.field private mIsCommitText:Z

.field private mIsComposingText:Z

.field private mIsDeletedText:Z

.field private mIsEditable:Z

.field private mIsEditableClear:Z

.field private mIsFirstCharLF:Z

.field private mIsFitOnSizeChanged:Z

.field mIsHandleRevese:[Z

.field private mIsHardKeyboardConnected:Z

.field private mIsMoveText:Z

.field private mIsSelectByKey:Z

.field private mIsSetEmptyText:Z

.field private mIsShowSoftInputEnable:Z

.field private mIsSwipeOnKeyboard:Z

.field private mIsTyping:Z

.field private mIsViewMode:Z

.field private mKeyListener:Landroid/text/method/KeyListener;

.field private mKeypadFilter:Landroid/content/IntentFilter;

.field private mLineCount:I

.field private mLineL2R:[Z

.field private mLinePosition:[Landroid/graphics/PointF;

.field private mMetricsRect:Landroid/graphics/Rect;

.field private mMoveEnd:I

.field private mMoveIndex:I

.field private mMoveSpanList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mMoveStart:I

.field private mMoveText:Ljava/lang/String;

.field private mNativeTextView:J

.field private mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

.field private mPrevCurPos:I

.field private mPrevLineIndex:I

.field private mPrevRelativeWidth:I

.field private mPreventShowSoftInput:Z

.field private mRequestObjectChange:Ljava/lang/Runnable;

.field private mScaleMatrix:Landroid/graphics/Matrix;

.field private mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

.field private mScrollBarPaint:Landroid/graphics/Paint;

.field private mScrollBarVisible:Z

.field private mSelectPaint:Landroid/graphics/Paint;

.field private mShowCursor:J

.field private mStartIndex:[I

.field private mStartIndexInput:I

.field private mSurroundingTextLength:I

.field private mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

.field private mTempMatrix:Landroid/graphics/Matrix;

.field private mTempRectF:Landroid/graphics/RectF;

.field private mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

.field protected mTextEraserEnable:Z

.field private mTextItalic:[Z

.field private mTextLimit:I

.field private mTextRect:[Landroid/graphics/RectF;

.field private mTextSize:[F

.field private mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

.field private mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

.field private mTouchEnable:Z

.field private final mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

.field private mUpdateHandler:Landroid/os/Handler;

.field private nextText:Ljava/lang/String;

.field private recordedDelete:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 96
    const-wide/high16 v0, 0x402e000000000000L    # 15.0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->SIN_15_DEGREE:F

    .line 167
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    .line 177
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

    .line 183
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_SPAN:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 393
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverCursorEnable:Z

    .line 395
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuItemList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;II)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # Landroid/view/ViewGroup;
    .param p3, "parentWidth"    # I
    .param p4, "ParentHeight"    # I

    .prologue
    .line 425
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 143
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    .line 144
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    .line 145
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    .line 146
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    .line 160
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    .line 161
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    .line 162
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    .line 165
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsShowSoftInputEnable:Z

    .line 169
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/String;

    const-string v6, "text_select_handle_left_2_browser"

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v1, v2

    .line 170
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/String;

    const-string v6, "text_select_handle_left_browser"

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v1, v2

    .line 171
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v2, 0x2

    new-instance v3, Ljava/lang/String;

    const-string v6, "text_select_handle_right_2_browser"

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v1, v2

    .line 172
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v2, 0x3

    new-instance v3, Ljava/lang/String;

    const-string v6, "text_select_handle_right_browser"

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v1, v2

    .line 173
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v2, 0x4

    new-instance v3, Ljava/lang/String;

    const-string v6, "text_select_handle_reverse"

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v1, v2

    .line 174
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v2, 0x5

    new-instance v3, Ljava/lang/String;

    const-string v6, "text_select_handle_middle"

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v1, v2

    .line 179
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/String;

    const-string v6, ""

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v1, v2

    .line 180
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/String;

    const-string v6, ""

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v1, v2

    .line 191
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 192
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    .line 195
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevRelativeWidth:I

    .line 196
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCurrentOrientation:I

    .line 199
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    .line 200
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    .line 201
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    .line 220
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z

    .line 223
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeypadFilter:Landroid/content/IntentFilter;

    .line 226
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHardKeyboardHidden:I

    .line 227
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHardKeyboardConnected:Z

    .line 228
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSwipeOnKeyboard:Z

    .line 229
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPreventShowSoftInput:Z

    .line 232
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditable:Z

    .line 233
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    .line 234
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    .line 235
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarVisible:Z

    .line 236
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTouchEnable:Z

    .line 237
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandlePressed:Z

    .line 238
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    .line 239
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHasWindowFocus:Z

    .line 240
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    .line 241
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mFirstDraw:Z

    .line 242
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    .line 243
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCanvasScroll:Z

    .line 244
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFitOnSizeChanged:Z

    .line 246
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    .line 247
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 250
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    .line 252
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    .line 253
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    .line 255
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    .line 256
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCommitText:Z

    .line 257
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 258
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsComposingText:Z

    .line 259
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSetEmptyText:Z

    .line 260
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    .line 261
    const/16 v1, 0x1388

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    .line 262
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I

    .line 263
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    .line 266
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCheckCursorOnScroll:Z

    .line 267
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    .line 271
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    .line 272
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    .line 273
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    .line 274
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    .line 275
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    .line 277
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    .line 278
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mActionDownTime:J

    .line 281
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    .line 282
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevLineIndex:I

    .line 283
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 284
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 285
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeltaY:F

    .line 286
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    .line 287
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    .line 288
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    .line 289
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineL2R:[Z

    .line 290
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    .line 291
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextSize:[F

    .line 292
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextItalic:[Z

    .line 293
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFirstCharLF:Z

    .line 296
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .line 297
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    .line 298
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    .line 299
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    .line 300
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMetricsRect:Landroid/graphics/Rect;

    .line 303
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    .line 306
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mUpdateHandler:Landroid/os/Handler;

    .line 307
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mRequestObjectChange:Ljava/lang/Runnable;

    .line 308
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    .line 311
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    .line 313
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 315
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    .line 316
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->nextText:Ljava/lang/String;

    .line 317
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    .line 319
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    .line 320
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndexInput:I

    .line 321
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndexInput:I

    .line 322
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeletePopupHandler:Landroid/os/Handler;

    .line 324
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 326
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    .line 328
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    .line 389
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 390
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 391
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 392
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverIconID:I

    .line 537
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 1500
    const/4 v1, 0x3

    new-array v1, v1, [Landroid/view/View$OnTouchListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    .line 1502
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    const/4 v2, 0x0

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    aput-object v3, v1, v2

    .line 1587
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    const/4 v2, 0x1

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    aput-object v3, v1, v2

    .line 1681
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    const/4 v2, 0x2

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$4;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    aput-object v3, v1, v2

    .line 6963
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$5;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeletePopupRunable:Ljava/lang/Runnable;

    .line 427
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    .line 429
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    const-string v2, "accessibility"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 431
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_4

    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    .line 433
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_init()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    .line 435
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    .line 436
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 437
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    const v2, -0x7f86350e

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 438
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 440
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    .line 441
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    const v2, -0xff6901

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 443
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    .line 444
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 445
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    const v2, -0x7fa0a0a1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 446
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 448
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    .line 449
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    .line 451
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    .line 453
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    .line 454
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-nez v1, :cond_0

    .line 455
    invoke-static {}, Landroid/text/Editable$Factory;->getInstance()Landroid/text/Editable$Factory;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/text/Editable$Factory;->newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    .line 456
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 458
    const/4 v1, 0x1

    new-array v0, v1, [Landroid/text/InputFilter;

    .line 459
    .local v0, "arrayOfInputFilter":[Landroid/text/InputFilter;
    const/4 v1, 0x0

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v6, 0x1388

    invoke-direct {v2, v3, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;-><init>(Landroid/content/Context;I)V

    aput-object v2, v0, v1

    .line 461
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1, v0}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    .line 464
    .end local v0    # "arrayOfInputFilter":[Landroid/text/InputFilter;
    :cond_0
    const/4 v1, 0x0

    sget-object v2, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v1, v2}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    .line 466
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    .line 468
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mUpdateHandler:Landroid/os/Handler;

    .line 470
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$6;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mRequestObjectChange:Ljava/lang/Runnable;

    .line 477
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_construct(JLandroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 478
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 481
    :cond_1
    int-to-float v1, p3

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    .line 482
    int-to-float v1, p4

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    .line 484
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    .line 485
    .local v7, "displayMetrics":Landroid/util/DisplayMetrics;
    if-eqz v7, :cond_3

    .line 486
    const/4 v4, 0x1

    .line 487
    .local v4, "textOnCanvasCommand":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    if-nez v1, :cond_2

    .line 488
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    .line 491
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    iget v1, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v1, v3, :cond_5

    iget v1, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_1
    iput v1, v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;->baseRate:I

    .line 494
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 495
    .local v5, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 496
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 498
    iget v1, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v1, v2, :cond_6

    iget v8, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 500
    .local v8, "width":I
    :goto_2
    new-instance v1, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v8, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMetricsRect:Landroid/graphics/Rect;

    .line 503
    .end local v4    # "textOnCanvasCommand":I
    .end local v5    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v8    # "width":I
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCurrentOrientation:I

    .line 504
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHardKeyboardHidden:I

    .line 506
    iput-object p0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 508
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 509
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 510
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const-string v2, "hover_pointer_text"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 511
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$7;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$7;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 525
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeypadFilter:Landroid/content/IntentFilter;

    .line 526
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeypadFilter:Landroid/content/IntentFilter;

    const-string v2, "ResponseAxT9Info"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 527
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeypadFilter:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 529
    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initTextBox(Landroid/view/ViewGroup;)V

    .line 531
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->makeDeletePopup()Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    .line 533
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeletePopupHandler:Landroid/os/Handler;

    .line 535
    return-void

    .line 431
    .end local v7    # "displayMetrics":Landroid/util/DisplayMetrics;
    :cond_4
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 492
    .restart local v4    # "textOnCanvasCommand":I
    .restart local v7    # "displayMetrics":Landroid/util/DisplayMetrics;
    :cond_5
    iget v1, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    goto/16 :goto_1

    .line 499
    .restart local v5    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_6
    iget v8, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    goto/16 :goto_2
.end method

.method private Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "command"    # I
    .param p5, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7469
    .local p4, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7470
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 7473
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_construct(JLandroid/content/Context;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 7342
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7343
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_construct(JLandroid/content/Context;)Z

    move-result v0

    .line 7346
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_construct(ILandroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_finalize(J)V
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 7333
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7334
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_finalize(J)V

    .line 7339
    :goto_0
    return-void

    .line 7337
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_finalize(I)V

    goto :goto_0
.end method

.method private Native_getHeight(J)I
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 7387
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7388
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(J)I

    move-result v0

    .line 7391
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getHintTextWidth(J)F
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 7432
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7433
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHintTextWidth(J)F

    move-result v0

    .line 7436
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHintTextWidth(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getLineCount(J)I
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 7378
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7379
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineCount(J)I

    move-result v0

    .line 7382
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineCount(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getLineEndIndex(JI)I
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "lineNumber"    # I

    .prologue
    .line 7423
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7424
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineEndIndex(JI)I

    move-result v0

    .line 7427
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineEndIndex(II)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getLinePosition(JILandroid/graphics/PointF;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "lineNumber"    # I
    .param p4, "position"    # Landroid/graphics/PointF;

    .prologue
    .line 7396
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7397
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLinePosition(JILandroid/graphics/PointF;)Z

    move-result v0

    .line 7400
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLinePosition(IILandroid/graphics/PointF;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_getLineStartIndex(JI)I
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "lineNumber"    # I

    .prologue
    .line 7414
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7415
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineStartIndex(JI)I

    move-result v0

    .line 7418
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineStartIndex(II)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getPan(J)F
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 7450
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7451
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(J)F

    move-result v0

    .line 7454
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getTextRect(JILandroid/graphics/RectF;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "textIndex"    # I
    .param p4, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 7405
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7406
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getTextRect(JILandroid/graphics/RectF;)Z

    move-result v0

    .line 7409
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getTextRect(IILandroid/graphics/RectF;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_init()J
    .locals 2

    .prologue
    .line 7315
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7316
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_init_64()J

    move-result-wide v0

    .line 7319
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_init()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private Native_measure(JI)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "width"    # I

    .prologue
    .line 7369
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7370
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_measure(JI)Z

    move-result v0

    .line 7373
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_measure(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setBitmap(JLandroid/graphics/Bitmap;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 7351
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7352
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setBitmap(JLandroid/graphics/Bitmap;)Z

    move-result v0

    .line 7355
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setBitmap(ILandroid/graphics/Bitmap;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setObjectText(JLcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "ObjectTextBox"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 7360
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7361
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setObjectText(JLcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z

    move-result v0

    .line 7364
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setObjectText(ILcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setPan(JF)V
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "deltaY"    # F

    .prologue
    .line 7441
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7442
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setPan(JF)V

    .line 7447
    :goto_0
    return-void

    .line 7445
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setPan(IF)V

    goto :goto_0
.end method

.method private Native_update(J)Z
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 7459
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7460
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_update(J)Z

    move-result v0

    .line 7463
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_update(I)Z

    move-result v0

    goto :goto_0
.end method

.method private absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/RectF;
    .param p3, "coordinateInfo"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .prologue
    .line 5335
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 5336
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    .line 5337
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    .line 5336
    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 5338
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 5339
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    .line 5340
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    .line 5339
    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 5341
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;FF)[F
    .locals 1

    .prologue
    .line 5922
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getAbsolutePoint(FF)[F

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 4631
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)F
    .locals 1

    .prologue
    .line 3370
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkForVerticalScroll(I)F

    move-result v0

    return v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 3843
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawHandle()V

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 1775
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    return-void
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 5811
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V

    return-void
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    return-object v0
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 6970
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeleteTextPopup()V

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/method/KeyListener;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    return-object v0
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 257
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 256
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCommitText:Z

    return-void
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 258
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsComposingText:Z

    return-void
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    return-void
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 6999
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->selectAll()Z

    move-result v0

    return v0
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/content/ClipboardManager;II)Z
    .locals 1

    .prologue
    .line 7004
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->cut(Landroid/content/ClipboardManager;II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/content/ClipboardManager;II)Z
    .locals 1

    .prologue
    .line 7063
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->copy(Landroid/content/ClipboardManager;II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/content/ClipboardManager;II)Z
    .locals 1

    .prologue
    .line 7117
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->paste(Landroid/content/ClipboardManager;II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z

    return v0
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 4608
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSettingInfo()V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 4308
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$30(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 255
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    return-void
.end method

.method static synthetic access$31(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 234
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    return-void
.end method

.method static synthetic access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 235
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarVisible:Z

    return-void
.end method

.method static synthetic access$33(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    return v0
.end method

.method static synthetic access$34(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 255
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    return v0
.end method

.method static synthetic access$35(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 324
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    return-void
.end method

.method static synthetic access$36(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I
    .locals 1

    .prologue
    .line 281
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    return v0
.end method

.method static synthetic access$37(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$38(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$39(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)J
    .locals 2

    .prologue
    .line 326
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    return-wide v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    return-object v0
.end method

.method static synthetic access$40(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;J)F
    .locals 1

    .prologue
    .line 7449
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v0

    return v0
.end method

.method static synthetic access$41(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I
    .locals 1

    .prologue
    .line 5166
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getOffsetForHorizontal(IF)I

    move-result v0

    return v0
.end method

.method static synthetic access$42(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$43(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[I
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    return-object v0
.end method

.method static synthetic access$44(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z
    .locals 1

    .prologue
    .line 4497
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$45(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I
    .locals 1

    .prologue
    .line 3044
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I

    move-result v0

    return v0
.end method

.method static synthetic access$46(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)F
    .locals 1

    .prologue
    .line 267
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    return v0
.end method

.method static synthetic access$47(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)V
    .locals 0

    .prologue
    .line 267
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    return-void
.end method

.method static synthetic access$48(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$49(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 5982
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueTopButtonDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    return-object v0
.end method

.method static synthetic access$50(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 6011
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueBottomButtonDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$51(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 229
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPreventShowSoftInput:Z

    return-void
.end method

.method static synthetic access$52(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 5833
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V

    return-void
.end method

.method static synthetic access$53(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    return-object v0
.end method

.method static synthetic access$54(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 3012
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initMeasureInfo()V

    return-void
.end method

.method static synthetic access$55(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method static synthetic access$56(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 6753
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->sendAccessibilityEventTypeViewTextChanged(Ljava/lang/CharSequence;III)V

    return-void
.end method

.method static synthetic access$57(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 260
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    return v0
.end method

.method static synthetic access$58(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 256
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCommitText:Z

    return v0
.end method

.method static synthetic access$59(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsComposingText:Z

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 220
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z

    return-void
.end method

.method static synthetic access$60(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    return v0
.end method

.method static synthetic access$61(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSetEmptyText:Z

    return v0
.end method

.method static synthetic access$62(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    return v0
.end method

.method static synthetic access$63(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I

    return v0
.end method

.method static synthetic access$64(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    .locals 0

    .prologue
    .line 262
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I

    return-void
.end method

.method static synthetic access$65(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    return v0
.end method

.method static synthetic access$66(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$67(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    .locals 0

    .prologue
    .line 320
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndexInput:I

    return-void
.end method

.method static synthetic access$68(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    .locals 0

    .prologue
    .line 321
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndexInput:I

    return-void
.end method

.method static synthetic access$69(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 6924
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showDeletePopupWindow()V

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 237
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandlePressed:Z

    return-void
.end method

.method static synthetic access$70(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I
    .locals 1

    .prologue
    .line 7261
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCustomHoveringIcon()I

    move-result v0

    return v0
.end method

.method static synthetic access$71(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    .locals 0

    .prologue
    .line 392
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverIconID:I

    return-void
.end method

.method static synthetic access$72(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I
    .locals 1

    .prologue
    .line 392
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverIconID:I

    return v0
.end method

.method static synthetic access$73(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)Z
    .locals 1

    .prologue
    .line 7221
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeHoveringIcon(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$74(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 6976
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->deleteLastSpeechInput()V

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)I
    .locals 1

    .prologue
    .line 5147
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v0

    return v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I
    .locals 1

    .prologue
    .line 5219
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v0

    return v0
.end method

.method private adjustCursorSize(Landroid/graphics/Rect;II)V
    .locals 12
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 3205
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v9, :cond_1

    .line 3257
    :cond_0
    :goto_0
    return-void

    .line 3209
    :cond_1
    const/4 v0, 0x0

    .local v0, "currentFontSize":F
    const/4 v6, 0x0

    .line 3211
    .local v6, "maxFontSize":F
    move v7, p2

    .line 3213
    .local v7, "pos":I
    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineCount(J)I

    move-result v3

    .line 3214
    .local v3, "lineCount":I
    const/4 v5, 0x0

    .local v5, "lineStart":I
    const/4 v4, 0x0

    .line 3216
    .local v4, "lineEnd":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-lt v2, v3, :cond_6

    .line 3225
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9, v5, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v8

    .line 3226
    .local v8, "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v8, :cond_3

    .line 3227
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v2, v9, :cond_8

    .line 3238
    :cond_3
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9, v7, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v8

    .line 3239
    if-eqz v8, :cond_4

    .line 3240
    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v2, v9, :cond_a

    .line 3248
    :cond_4
    const/4 v9, 0x0

    cmpl-float v9, v6, v9

    if-eqz v9, :cond_5

    .line 3249
    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v9, v9

    sub-float v10, v6, v0

    const/high16 v11, 0x40800000    # 4.0f

    div-float/2addr v10, v11

    sub-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, p1, Landroid/graphics/Rect;->bottom:I

    .line 3252
    :cond_5
    const/4 v9, 0x0

    cmpl-float v9, v0, v9

    if-lez v9, :cond_0

    .line 3253
    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    const v10, 0x3fa66666    # 1.3f

    mul-float/2addr v10, v0

    float-to-int v10, v10

    sub-int/2addr v9, v10

    iput v9, p1, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 3217
    .end local v8    # "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    :cond_6
    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v10, v11, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineStartIndex(JI)I

    move-result v5

    .line 3218
    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v10, v11, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineEndIndex(JI)I

    move-result v4

    .line 3219
    if-gt v5, v7, :cond_7

    add-int/lit8 v9, v4, 0x1

    if-ge v9, v7, :cond_2

    .line 3216
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3228
    .restart local v8    # "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    :cond_8
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    instance-of v9, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v9, :cond_9

    .line 3229
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v1, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    .line 3230
    .local v1, "fontSize":F
    cmpg-float v9, v6, v1

    if-gtz v9, :cond_9

    .line 3231
    move v6, v1

    .line 3227
    .end local v1    # "fontSize":F
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 3241
    :cond_a
    if-eqz v7, :cond_b

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-eq v9, v7, :cond_c

    .line 3242
    :cond_b
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    instance-of v9, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v9, :cond_c

    .line 3243
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    .line 3240
    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method private adjustTextBox()V
    .locals 15

    .prologue
    const/4 v14, 0x3

    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 3104
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v8, :cond_1

    .line 3196
    :cond_0
    :goto_0
    return-void

    .line 3108
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 3110
    .local v5, "prevObjectWidth":F
    iget-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v8, :cond_6

    .line 3111
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 3116
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v1

    .line 3117
    .local v1, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v1, :cond_0

    .line 3132
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    .line 3134
    .local v4, "objectRect":Landroid/graphics/RectF;
    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    .line 3135
    .local v7, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0, v7, v4, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3137
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v0

    .line 3138
    .local v0, "absoluteHeight":I
    const/4 v6, 0x0

    .line 3139
    .local v6, "relativeHeight":I
    if-nez v0, :cond_7

    .line 3140
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultHeight()I

    move-result v6

    .line 3145
    :goto_2
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v8

    const/4 v9, 0x2

    if-eq v8, v9, :cond_2

    .line 3146
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v8

    if-ne v8, v14, :cond_3

    .line 3147
    :cond_2
    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v8

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v8, v8

    if-ge v6, v8, :cond_a

    .line 3148
    if-lez v6, :cond_3

    .line 3149
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinHeight()F

    move-result v3

    .line 3150
    .local v3, "minHeight":F
    cmpg-float v8, v3, v12

    if-gtz v8, :cond_8

    .line 3151
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v8

    int-to-float v9, v6

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    .line 3183
    .end local v3    # "minHeight":F
    :cond_3
    :goto_3
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v8

    const/4 v9, 0x1

    if-eq v8, v9, :cond_4

    .line 3184
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v8

    if-ne v8, v14, :cond_5

    .line 3185
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    cmpg-float v8, v5, v8

    if-gez v8, :cond_5

    .line 3186
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onObjectChanged()V

    .line 3190
    :cond_5
    iget-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCanvasScroll:Z

    if-eqz v8, :cond_e

    iget-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFitOnSizeChanged:Z

    if-eqz v8, :cond_e

    .line 3191
    iput-boolean v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCanvasScroll:Z

    .line 3192
    iput-boolean v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFitOnSizeChanged:Z

    goto/16 :goto_0

    .line 3113
    .end local v0    # "absoluteHeight":I
    .end local v1    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .end local v4    # "objectRect":Landroid/graphics/RectF;
    .end local v6    # "relativeHeight":I
    .end local v7    # "relativeRect":Landroid/graphics/RectF;
    :cond_6
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    move-result v10

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v10, v10

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_measure(JI)Z

    goto/16 :goto_1

    .line 3142
    .restart local v0    # "absoluteHeight":I
    .restart local v1    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .restart local v4    # "objectRect":Landroid/graphics/RectF;
    .restart local v6    # "relativeHeight":I
    .restart local v7    # "relativeRect":Landroid/graphics/RectF;
    :cond_7
    int-to-float v8, v0

    iget v9, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v8, v9

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v6, v8

    goto/16 :goto_2

    .line 3153
    .restart local v3    # "minHeight":F
    :cond_8
    iget v8, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v8

    .line 3154
    int-to-float v8, v6

    cmpg-float v8, v3, v8

    if-gez v8, :cond_9

    .line 3155
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v8

    int-to-float v9, v6

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto :goto_3

    .line 3157
    :cond_9
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v8

    invoke-direct {p0, v8, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto :goto_3

    .line 3161
    .end local v3    # "minHeight":F
    :cond_a
    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v8

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v8, v8

    if-le v6, v8, :cond_3

    .line 3162
    int-to-float v8, v0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v9

    iget v9, v9, Landroid/graphics/RectF;->top:F

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v10, v10

    sub-float/2addr v9, v10

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_b

    .line 3163
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v9, v9

    sub-float/2addr v8, v9

    .line 3164
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v9

    iget v9, v9, Landroid/graphics/RectF;->top:F

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 3163
    sub-float/2addr v8, v9

    .line 3164
    iget v9, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    .line 3163
    mul-float/2addr v8, v9

    float-to-int v6, v8

    .line 3166
    :cond_b
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxHeight()F

    move-result v2

    .line 3167
    .local v2, "maxHeight":F
    cmpg-float v8, v2, v12

    if-gtz v8, :cond_c

    .line 3168
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v8

    int-to-float v9, v6

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto/16 :goto_3

    .line 3170
    :cond_c
    iget v8, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v2, v8

    .line 3171
    int-to-float v8, v6

    cmpl-float v8, v2, v8

    if-lez v8, :cond_d

    .line 3172
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v8

    int-to-float v9, v6

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto/16 :goto_3

    .line 3174
    :cond_d
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v8

    invoke-direct {p0, v8, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto/16 :goto_3

    .line 3194
    .end local v2    # "maxHeight":F
    :cond_e
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkCursorPosition()V

    goto/16 :goto_0
.end method

.method private adjustTextBoxRect()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 3058
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v5, :cond_1

    .line 3098
    :cond_0
    :goto_0
    return-void

    .line 3062
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    .line 3063
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v0, :cond_0

    .line 3067
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 3068
    .local v3, "objectRect":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v4

    .line 3069
    .local v4, "width":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 3071
    .local v1, "height":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinWidth()F

    move-result v5

    cmpg-float v5, v4, v5

    if-gez v5, :cond_2

    .line 3072
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinWidth()F

    move-result v4

    .line 3074
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinHeight()F

    move-result v5

    cmpg-float v5, v1, v5

    if-gez v5, :cond_3

    .line 3075
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinHeight()F

    move-result v1

    .line 3077
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxWidth()F

    move-result v5

    cmpl-float v5, v5, v7

    if-lez v5, :cond_4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxWidth()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinWidth()F

    move-result v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_4

    .line 3078
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxWidth()F

    move-result v5

    cmpl-float v5, v4, v5

    if-lez v5, :cond_4

    .line 3079
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxWidth()F

    move-result v4

    .line 3081
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxHeight()F

    move-result v5

    cmpl-float v5, v5, v7

    if-lez v5, :cond_5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxHeight()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinHeight()F

    move-result v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_5

    .line 3082
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxHeight()F

    move-result v5

    cmpl-float v5, v1, v5

    if-lez v5, :cond_5

    .line 3083
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxHeight()F

    move-result v1

    .line 3085
    :cond_5
    iget v5, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v5, v4

    iput v5, v3, Landroid/graphics/RectF;->right:F

    .line 3086
    iget v5, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v5, v1

    iput v5, v3, Landroid/graphics/RectF;->bottom:F

    .line 3088
    const/4 v2, 0x0

    .line 3089
    .local v2, "isRectChange":Z
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    cmpl-float v5, v5, v6

    if-nez v5, :cond_6

    .line 3090
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_7

    .line 3091
    :cond_6
    const/4 v2, 0x1

    .line 3093
    :cond_7
    if-eqz v2, :cond_0

    .line 3094
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setRect(Landroid/graphics/RectF;Z)V

    .line 3095
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onObjectChanged()V

    .line 3096
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto/16 :goto_0
.end method

.method private applyScaledRect(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 4116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 4117
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 4118
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 4119
    return-void
.end method

.method private checkCursorPosition()V
    .locals 24

    .prologue
    .line 3483
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v20, v0

    if-nez v20, :cond_1

    .line 3640
    :cond_0
    :goto_0
    return-void

    .line 3488
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    move/from16 v20, v0

    if-nez v20, :cond_0

    .line 3492
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v4

    .line 3493
    .local v4, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v4, :cond_0

    .line 3498
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    .line 3499
    .local v10, "objectRect":Landroid/graphics/RectF;
    if-eqz v10, :cond_0

    .line 3504
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v12

    .line 3506
    .local v12, "pos":I
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v16

    .line 3507
    .local v16, "temp":Landroid/graphics/Rect;
    if-eqz v16, :cond_0

    .line 3512
    new-instance v14, Landroid/graphics/RectF;

    invoke-direct {v14}, Landroid/graphics/RectF;-><init>()V

    .line 3513
    .local v14, "rect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2, v12, v14}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 3514
    invoke-virtual {v14}, Landroid/graphics/RectF;->width()F

    move-result v20

    const/high16 v21, 0x3f800000    # 1.0f

    add-float v17, v20, v21

    .line 3515
    .local v17, "width":F
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    add-float v20, v20, v17

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 3517
    new-instance v6, Landroid/graphics/RectF;

    move-object/from16 v0, v16

    invoke-direct {v6, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 3519
    .local v6, "cursorRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v20

    move/from16 v0, v20

    int-to-float v9, v0

    .line 3520
    .local v9, "minHeight":F
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v7

    .line 3521
    .local v7, "deltaY":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getBottomMargin()F

    move-result v21

    add-float v8, v20, v21

    .line 3522
    .local v8, "margin":F
    const/16 v20, 0x0

    cmpl-float v20, v8, v20

    if-lez v20, :cond_2

    .line 3523
    const/4 v8, 0x0

    .line 3526
    :cond_2
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v20

    cmpl-float v20, v9, v20

    if-lez v20, :cond_d

    .line 3527
    iget v0, v6, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    cmpg-float v20, v20, v7

    if-gez v20, :cond_c

    .line 3528
    iget v0, v6, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    const/high16 v21, 0x40a00000    # 5.0f

    sub-float v7, v20, v21

    .line 3533
    :cond_3
    :goto_1
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v20

    add-float v20, v20, v8

    add-float v20, v20, v7

    cmpg-float v20, v9, v20

    if-gez v20, :cond_4

    .line 3534
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v20

    add-float v20, v20, v8

    add-float v20, v20, v7

    sub-float v20, v20, v9

    sub-float v7, v7, v20

    .line 3540
    :cond_4
    :goto_2
    const/16 v20, 0x0

    cmpg-float v20, v7, v20

    if-gez v20, :cond_5

    .line 3541
    const/4 v7, 0x0

    .line 3544
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCurrnetLineIndex(I)I

    move-result v5

    .line 3545
    .local v5, "currentLineIndex":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevLineIndex:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ne v0, v5, :cond_6

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v20

    sub-float v20, v7, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x33d6bf95    # 1.0E-7f

    cmpl-float v20, v20, v21

    if-lez v20, :cond_7

    .line 3546
    :cond_6
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setPan(JF)V

    .line 3548
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v20

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    float-to-double v0, v9

    move-wide/from16 v22, v0

    cmpg-double v20, v20, v22

    if-gez v20, :cond_7

    .line 3549
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showScrollBar()V

    .line 3553
    :cond_7
    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v22

    sub-float v21, v21, v22

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 3555
    iget v0, v6, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRelativePoint(FF)[F

    move-result-object v13

    .line 3556
    .local v13, "pts":[F
    if-eqz v13, :cond_0

    .line 3561
    const/16 v20, 0x1

    aget v20, v13, v20

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/RectF;->top:F

    .line 3563
    iget v0, v6, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRelativePoint(FF)[F

    move-result-object v13

    .line 3564
    if-eqz v13, :cond_0

    .line 3569
    const/16 v20, 0x1

    aget v20, v13, v20

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/RectF;->bottom:F

    .line 3571
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 3572
    .local v15, "relativeCursorRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v6, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3582
    const/16 v18, 0x0

    .line 3583
    .local v18, "xDiff":F
    const/16 v19, 0x0

    .line 3585
    .local v19, "yDiff":F
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 3586
    .local v11, "parentLayout":Landroid/view/ViewGroup;
    if-eqz v11, :cond_0

    .line 3590
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v20

    if-eqz v20, :cond_0

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getWidth()I

    move-result v20

    if-eqz v20, :cond_0

    .line 3594
    iget v0, v15, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_e

    .line 3596
    iget v0, v15, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    sub-float v19, v20, v21

    .line 3597
    iget v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v20, v0

    div-float v19, v19, v20

    .line 3598
    const/high16 v20, 0x41100000    # 9.0f

    add-float v19, v19, v20

    .line 3610
    :cond_8
    :goto_3
    iget v0, v15, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_f

    .line 3612
    iget v0, v15, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    sub-float v18, v20, v21

    .line 3613
    iget v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v20, v0

    div-float v18, v18, v20

    .line 3614
    iget v0, v6, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    const/high16 v22, 0x40a00000    # 5.0f

    sub-float v21, v21, v22

    cmpg-float v20, v20, v21

    if-gez v20, :cond_9

    .line 3615
    const/high16 v20, 0x40a00000    # 5.0f

    add-float v18, v18, v20

    .line 3626
    :cond_9
    :goto_4
    const/16 v20, 0x0

    cmpl-float v20, v18, v20

    if-nez v20, :cond_a

    const/16 v20, 0x0

    cmpl-float v20, v19, v20

    if-eqz v20, :cond_b

    .line 3627
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCheckCursorOnScroll:Z

    move/from16 v20, v0

    if-eqz v20, :cond_10

    .line 3628
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onRequestScroll(FF)V

    .line 3639
    :cond_b
    :goto_5
    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevLineIndex:I

    goto/16 :goto_0

    .line 3529
    .end local v5    # "currentLineIndex":I
    .end local v11    # "parentLayout":Landroid/view/ViewGroup;
    .end local v13    # "pts":[F
    .end local v15    # "relativeCursorRect":Landroid/graphics/RectF;
    .end local v18    # "xDiff":F
    .end local v19    # "yDiff":F
    :cond_c
    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v21

    add-float v21, v21, v7

    cmpl-float v20, v20, v21

    if-lez v20, :cond_3

    .line 3530
    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v21

    sub-float v20, v20, v21

    const/high16 v21, 0x40a00000    # 5.0f

    add-float v7, v20, v21

    goto/16 :goto_1

    .line 3537
    :cond_d
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 3599
    .restart local v5    # "currentLineIndex":I
    .restart local v11    # "parentLayout":Landroid/view/ViewGroup;
    .restart local v13    # "pts":[F
    .restart local v15    # "relativeCursorRect":Landroid/graphics/RectF;
    .restart local v18    # "xDiff":F
    .restart local v19    # "yDiff":F
    :cond_e
    iget v0, v15, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    const/16 v21, 0x0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_8

    .line 3601
    iget v0, v15, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v15, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gtz v20, :cond_8

    .line 3602
    iget v0, v15, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    .line 3603
    iget v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v20, v0

    div-float v19, v19, v20

    .line 3604
    iget v0, v6, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    const/high16 v21, 0x40a00000    # 5.0f

    cmpl-float v20, v20, v21

    if-lez v20, :cond_8

    .line 3605
    const/high16 v20, 0x41100000    # 9.0f

    sub-float v19, v19, v20

    goto/16 :goto_3

    .line 3617
    :cond_f
    iget v0, v15, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    const/16 v21, 0x0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_9

    .line 3619
    iget v0, v15, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    .line 3620
    iget v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v20, v0

    div-float v18, v18, v20

    .line 3621
    iget v0, v6, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    const/high16 v21, 0x420c0000    # 35.0f

    cmpl-float v20, v20, v21

    if-lez v20, :cond_9

    .line 3622
    const/high16 v20, 0x420c0000    # 35.0f

    sub-float v18, v18, v20

    goto/16 :goto_4

    .line 3630
    :cond_10
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V

    .line 3631
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aget-object v20, v20, v21

    const/16 v21, 0x8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3632
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    move-object/from16 v20, v0

    const/16 v21, 0x2

    aget-object v20, v20, v21

    const/16 v21, 0x8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3633
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v20, v0

    if-eqz v20, :cond_b

    .line 3634
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    goto/16 :goto_5
.end method

.method private checkForHorizontalScroll()Z
    .locals 10
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3330
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_1

    .line 3367
    :cond_0
    :goto_0
    return v4

    .line 3335
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v6, :cond_0

    .line 3339
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandlePressed:Z

    if-nez v6, :cond_0

    .line 3340
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v6

    const/4 v7, 0x3

    if-eq v6, v7, :cond_2

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v6

    if-ne v6, v5, :cond_0

    .line 3345
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 3347
    .local v2, "objectRect":Landroid/graphics/RectF;
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    .line 3348
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v0, :cond_0

    .line 3353
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 3354
    .local v3, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0, v3, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3356
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMaximumWidth()F

    move-result v1

    .line 3357
    .local v1, "lineRelativeWidth":F
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevRelativeWidth:I

    int-to-float v4, v4

    cmpl-float v4, v4, v1

    if-eqz v4, :cond_3

    .line 3358
    float-to-int v4, v1

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevRelativeWidth:I

    .line 3360
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-direct {p0, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    .line 3362
    iget v4, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v1

    iput v4, v3, Landroid/graphics/RectF;->right:F

    :cond_3
    move v4, v5

    .line 3367
    goto :goto_0
.end method

.method private checkForVerticalScroll(I)F
    .locals 14
    .param p1, "index"    # I

    .prologue
    .line 3371
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3372
    .local v4, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    if-nez v4, :cond_0

    .line 3374
    const/4 v2, 0x0

    .line 3475
    :goto_0
    return v2

    .line 3377
    :cond_0
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v12, :cond_1

    .line 3378
    const/4 v2, 0x0

    goto :goto_0

    .line 3381
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    .line 3382
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v0, :cond_2

    .line 3384
    const/4 v2, 0x0

    goto :goto_0

    .line 3387
    :cond_2
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v7

    .line 3388
    .local v7, "objectRect":Landroid/graphics/RectF;
    if-nez v7, :cond_3

    .line 3390
    const/4 v2, 0x0

    goto :goto_0

    .line 3393
    :cond_3
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v12

    invoke-direct {p0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v11

    .line 3394
    .local v11, "temp":Landroid/graphics/Rect;
    if-nez v11, :cond_4

    .line 3396
    const/4 v2, 0x0

    goto :goto_0

    .line 3399
    :cond_4
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v11}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 3401
    .local v1, "cursorRect":Landroid/graphics/RectF;
    iget-wide v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v3

    .line 3402
    .local v3, "deltaY":F
    iget-wide v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v12

    int-to-float v6, v12

    .line 3403
    .local v6, "minHeight":F
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v12

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v13}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getBottomMargin()F

    move-result v13

    add-float v5, v12, v13

    .line 3404
    .local v5, "margin":F
    const/4 v12, 0x0

    cmpl-float v12, v5, v12

    if-lez v12, :cond_5

    .line 3405
    const/4 v5, 0x0

    .line 3408
    :cond_5
    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v12

    cmpl-float v12, v6, v12

    if-lez v12, :cond_a

    .line 3409
    iget v12, v1, Landroid/graphics/RectF;->top:F

    cmpg-float v12, v12, v3

    if-gez v12, :cond_9

    .line 3410
    iget v3, v1, Landroid/graphics/RectF;->top:F

    .line 3415
    :cond_6
    :goto_1
    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v12

    add-float/2addr v12, v5

    add-float/2addr v12, v3

    cmpg-float v12, v6, v12

    if-gez v12, :cond_7

    .line 3416
    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v12

    add-float/2addr v12, v5

    add-float/2addr v12, v3

    sub-float/2addr v12, v6

    sub-float/2addr v3, v12

    .line 3422
    :cond_7
    :goto_2
    const/4 v12, 0x0

    cmpg-float v12, v3, v12

    if-gez v12, :cond_8

    .line 3423
    const/4 v3, 0x0

    .line 3426
    :cond_8
    iget v12, v7, Landroid/graphics/RectF;->left:F

    iget v13, v7, Landroid/graphics/RectF;->top:F

    sub-float/2addr v13, v3

    invoke-virtual {v1, v12, v13}, Landroid/graphics/RectF;->offset(FF)V

    .line 3428
    iget v12, v1, Landroid/graphics/RectF;->left:F

    iget v13, v1, Landroid/graphics/RectF;->top:F

    invoke-direct {p0, v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRelativePoint(FF)[F

    move-result-object v9

    .line 3429
    .local v9, "pts":[F
    if-nez v9, :cond_b

    .line 3431
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3411
    .end local v9    # "pts":[F
    :cond_9
    iget v12, v1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v13

    add-float/2addr v13, v3

    cmpl-float v12, v12, v13

    if-lez v12, :cond_6

    .line 3412
    iget v12, v1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v13

    sub-float v3, v12, v13

    goto :goto_1

    .line 3419
    :cond_a
    const/4 v3, 0x0

    goto :goto_2

    .line 3434
    .restart local v9    # "pts":[F
    :cond_b
    const/4 v12, 0x1

    aget v12, v9, v12

    iput v12, v1, Landroid/graphics/RectF;->top:F

    .line 3436
    iget v12, v1, Landroid/graphics/RectF;->left:F

    iget v13, v1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {p0, v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRelativePoint(FF)[F

    move-result-object v9

    .line 3437
    if-nez v9, :cond_c

    .line 3439
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3442
    :cond_c
    const/4 v12, 0x1

    aget v12, v9, v12

    iput v12, v1, Landroid/graphics/RectF;->bottom:F

    .line 3444
    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10}, Landroid/graphics/RectF;-><init>()V

    .line 3445
    .local v10, "relativeCursorRect":Landroid/graphics/RectF;
    invoke-direct {p0, v10, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3447
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    .line 3448
    .local v8, "parentLayout":Landroid/view/ViewGroup;
    if-nez v8, :cond_d

    .line 3449
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3452
    :cond_d
    invoke-virtual {v8}, Landroid/view/ViewGroup;->getHeight()I

    move-result v12

    if-eqz v12, :cond_e

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getWidth()I

    move-result v12

    if-nez v12, :cond_f

    .line 3453
    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3456
    :cond_f
    const/4 v2, 0x0

    .line 3457
    .local v2, "delta":F
    iget v12, v10, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getHeight()I

    move-result v13

    int-to-float v13, v13

    cmpl-float v12, v12, v13

    if-lez v12, :cond_11

    .line 3459
    iget v12, v10, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getHeight()I

    move-result v13

    int-to-float v13, v13

    sub-float v2, v12, v13

    .line 3460
    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v2, v12

    .line 3461
    const/high16 v12, 0x41100000    # 9.0f

    add-float/2addr v2, v12

    .line 3471
    :cond_10
    :goto_3
    const/4 v12, 0x1

    invoke-virtual {p0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    goto/16 :goto_0

    .line 3462
    :cond_11
    iget v12, v10, Landroid/graphics/RectF;->top:F

    const/4 v13, 0x0

    cmpg-float v12, v12, v13

    if-gez v12, :cond_10

    .line 3464
    iget v2, v10, Landroid/graphics/RectF;->top:F

    .line 3465
    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v2, v12

    .line 3466
    iget v12, v1, Landroid/graphics/RectF;->top:F

    const/high16 v13, 0x40a00000    # 5.0f

    cmpl-float v12, v12, v13

    if-lez v12, :cond_10

    .line 3467
    const/high16 v12, 0x41100000    # 9.0f

    sub-float/2addr v2, v12

    goto :goto_3
.end method

.method private checkObjectBounds()V
    .locals 9
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 3644
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v5, :cond_1

    .line 3697
    :cond_0
    :goto_0
    return-void

    .line 3648
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 3650
    .local v3, "objectRect":Landroid/graphics/RectF;
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    .line 3651
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v0, :cond_0

    .line 3656
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 3657
    .local v4, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0, v4, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3659
    iget v5, v3, Landroid/graphics/RectF;->left:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    add-int/lit8 v6, v6, 0x0

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    .line 3660
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->right:F

    .line 3661
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    int-to-float v5, v5

    iput v5, v3, Landroid/graphics/RectF;->left:F

    .line 3663
    :cond_2
    iget v5, v3, Landroid/graphics/RectF;->top:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    add-int/lit8 v6, v6, 0x0

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_3

    .line 3664
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->bottom:F

    .line 3665
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    int-to-float v5, v5

    iput v5, v3, Landroid/graphics/RectF;->top:F

    .line 3667
    :cond_3
    iget v5, v3, Landroid/graphics/RectF;->right:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_6

    .line 3668
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v5

    if-eqz v5, :cond_4

    .line 3669
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    .line 3670
    :cond_4
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v6

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->left:F

    .line 3672
    :cond_5
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->right:F

    .line 3674
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultWidth()I

    move-result v2

    .line 3675
    .local v2, "minWidth":I
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    int-to-float v6, v2

    cmpg-float v5, v5, v6

    if-gez v5, :cond_6

    .line 3676
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    int-to-float v6, v2

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->left:F

    .line 3679
    .end local v2    # "minWidth":I
    :cond_6
    iget v5, v3, Landroid/graphics/RectF;->bottom:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_9

    .line 3680
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v5

    if-eqz v5, :cond_7

    .line 3681
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v5

    if-ne v5, v8, :cond_8

    .line 3682
    :cond_7
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->top:F

    .line 3684
    :cond_8
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->bottom:F

    .line 3686
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultHeight()I

    move-result v1

    .line 3687
    .local v1, "minHeight":I
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    int-to-float v6, v1

    cmpg-float v5, v5, v6

    if-gez v5, :cond_9

    .line 3688
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    int-to-float v6, v1

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->top:F

    .line 3692
    .end local v1    # "minHeight":I
    :cond_9
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5, v3, v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setRect(Landroid/graphics/RectF;Z)V

    .line 3694
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v5

    if-nez v5, :cond_0

    .line 3695
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onObjectChanged()V

    goto/16 :goto_0
.end method

.method private copy(Landroid/content/ClipboardManager;II)Z
    .locals 12
    .param p1, "clipboard"    # Landroid/content/ClipboardManager;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/16 v11, 0x96

    const/16 v10, 0x50

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 7064
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v6}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 7065
    .local v5, "str":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 7066
    invoke-virtual {p1}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    .line 7067
    .local v0, "clip":Landroid/content/ClipData;
    if-eqz v0, :cond_0

    .line 7068
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v3

    .line 7069
    .local v3, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v3, :cond_2

    .line 7093
    .end local v1    # "i":I
    .end local v3    # "length":I
    :cond_0
    if-eqz v5, :cond_1

    .line 7094
    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 7095
    const-string v6, "clipData"

    invoke-static {v6, v5}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 7096
    invoke-virtual {p1, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 7098
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v6, :cond_5

    .line 7099
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 7100
    const-string v7, "string_copied_to_clipboard"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 7099
    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    .line 7105
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v10, v8, v11}, Landroid/widget/Toast;->setGravity(III)V

    .line 7106
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 7109
    if-le p2, p3, :cond_6

    .end local p2    # "start":I
    :goto_2
    invoke-virtual {p0, p2, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 7113
    .end local v0    # "clip":Landroid/content/ClipData;
    :cond_1
    const/4 v5, 0x0

    .line 7114
    :goto_3
    return v9

    .line 7070
    .restart local v0    # "clip":Landroid/content/ClipData;
    .restart local v1    # "i":I
    .restart local v3    # "length":I
    .restart local p2    # "start":I
    :cond_2
    invoke-virtual {v0, v8}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v2

    .line 7071
    .local v2, "item":Landroid/content/ClipData$Item;
    if-eqz v2, :cond_4

    .line 7072
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    .line 7073
    .local v4, "sequence":Ljava/lang/CharSequence;
    if-eqz v4, :cond_4

    .line 7074
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_4

    .line 7075
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v6, :cond_3

    .line 7076
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 7077
    const-string v7, "string_already_exists"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 7076
    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    .line 7082
    :goto_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v10, v8, v11}, Landroid/widget/Toast;->setGravity(III)V

    .line 7083
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 7084
    const/4 v0, 0x0

    .line 7085
    const/4 v5, 0x0

    .line 7086
    goto :goto_3

    .line 7079
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string v7, "string_already_exists"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 7080
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v8}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_4

    .line 7069
    .end local v4    # "sequence":Ljava/lang/CharSequence;
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 7102
    .end local v1    # "i":I
    .end local v2    # "item":Landroid/content/ClipData$Item;
    .end local v3    # "length":I
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string v7, "string_copied_to_clipboard"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 7103
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v8}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_1

    :cond_6
    move p2, p3

    .line 7109
    goto :goto_2
.end method

.method private cut(Landroid/content/ClipboardManager;II)Z
    .locals 12
    .param p1, "clipboard"    # Landroid/content/ClipboardManager;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/16 v11, 0x96

    const/16 v10, 0x50

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 7005
    const/4 v5, 0x0

    .line 7006
    .local v5, "str":Ljava/lang/String;
    if-eq p2, p3, :cond_2

    .line 7007
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    .line 7009
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v6}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 7010
    if-eqz v5, :cond_1

    .line 7011
    invoke-virtual {p1}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    .line 7012
    .local v0, "clip":Landroid/content/ClipData;
    if-eqz v0, :cond_0

    .line 7013
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v3

    .line 7014
    .local v3, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v3, :cond_3

    .line 7042
    .end local v1    # "i":I
    .end local v3    # "length":I
    :cond_0
    const-string v6, "clipData"

    invoke-static {v6, v5}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 7043
    invoke-virtual {p1, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 7045
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v6, :cond_6

    .line 7046
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "string_copied_to_clipboard"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    .line 7052
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v10, v8, v11}, Landroid/widget/Toast;->setGravity(III)V

    .line 7053
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 7055
    .end local v0    # "clip":Landroid/content/ClipData;
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v6}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v6

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 7056
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v6}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v6

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 7057
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    .line 7059
    :cond_2
    const/4 v5, 0x0

    .line 7060
    :goto_2
    return v9

    .line 7015
    .restart local v0    # "clip":Landroid/content/ClipData;
    .restart local v1    # "i":I
    .restart local v3    # "length":I
    :cond_3
    invoke-virtual {v0, v8}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v2

    .line 7016
    .local v2, "item":Landroid/content/ClipData$Item;
    if-eqz v2, :cond_5

    .line 7017
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    .line 7018
    .local v4, "sequence":Ljava/lang/CharSequence;
    if-eqz v4, :cond_5

    .line 7019
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_5

    .line 7020
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v6, :cond_4

    .line 7021
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 7022
    const-string v7, "string_already_exists"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 7021
    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    .line 7028
    :goto_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v10, v8, v11}, Landroid/widget/Toast;->setGravity(III)V

    .line 7029
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 7030
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v6}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v6

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 7031
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v6}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v6

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 7032
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    .line 7033
    const/4 v0, 0x0

    .line 7034
    const/4 v5, 0x0

    .line 7035
    goto :goto_2

    .line 7024
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string v7, "string_already_exists"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 7025
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v8}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_3

    .line 7014
    .end local v4    # "sequence":Ljava/lang/CharSequence;
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 7049
    .end local v1    # "i":I
    .end local v2    # "item":Landroid/content/ClipData$Item;
    .end local v3    # "length":I
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string v7, "string_copied_to_clipboard"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 7050
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v8}, Landroid/widget/Toast;->setDuration(I)V

    goto/16 :goto_1
.end method

.method private delete(II)Z
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v1, 0x1

    .line 7159
    if-eq p1, p2, :cond_1

    .line 7160
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 7161
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    if-nez v0, :cond_0

    .line 7162
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    .line 7163
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    .line 7165
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 7166
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 7167
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    .line 7169
    :cond_1
    return v1
.end method

.method private deleteLastSpeechInput()V
    .locals 3

    .prologue
    .line 6977
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 6978
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndexInput:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndexInput:I

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 6979
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndexInput:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 6980
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 6981
    return-void
.end method

.method private drawSelectRect(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "startIndex"    # I
    .param p3, "endIndex"    # I
    .param p4, "result"    # Landroid/graphics/RectF;
    .param p5, "drawRect"    # Landroid/graphics/Rect;

    .prologue
    .line 4154
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-nez v5, :cond_1

    .line 4181
    :cond_0
    return-void

    .line 4158
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v5, :cond_0

    .line 4162
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v2

    .line 4164
    .local v2, "deltaY":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 4165
    .local v3, "objectRect":Landroid/graphics/RectF;
    if-eqz v3, :cond_0

    .line 4170
    move v1, p2

    .local v1, "cnt":I
    :goto_0
    if-ge v1, p3, :cond_0

    .line 4171
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v5, v2

    float-to-int v4, v5

    .line 4172
    .local v4, "top":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v5, v2

    float-to-int v0, v5

    .line 4174
    .local v0, "bottom":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->left:F

    float-to-int v6, v5

    if-gez v4, :cond_2

    const/4 v4, 0x0

    .end local v4    # "top":I
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->right:F

    float-to-int v7, v5

    .line 4175
    int-to-float v5, v0

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v8

    cmpl-float v5, v5, v8

    if-lez v5, :cond_3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    :goto_1
    float-to-int v5, v5

    .line 4174
    invoke-virtual {p5, v6, v4, v7, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 4177
    invoke-direct {p0, p5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->applyScaledRect(Landroid/graphics/Rect;)V

    .line 4179
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p5, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 4170
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4175
    :cond_3
    int-to-float v5, v0

    goto :goto_1
.end method

.method private drawSelectedLine(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "startIndex"    # I
    .param p3, "endIndex"    # I
    .param p4, "result"    # Landroid/graphics/RectF;
    .param p5, "drawRect"    # Landroid/graphics/Rect;

    .prologue
    .line 4122
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-nez v5, :cond_1

    .line 4151
    :cond_0
    :goto_0
    return-void

    .line 4126
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, p2

    invoke-virtual {p4, v5}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 4127
    add-int/lit8 v1, p2, 0x1

    .local v1, "cnt":I
    :goto_1
    if-le v1, p3, :cond_3

    .line 4134
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v2

    .line 4136
    .local v2, "deltaY":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 4137
    .local v3, "objectRect":Landroid/graphics/RectF;
    if-eqz v3, :cond_0

    .line 4142
    iget v5, p4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v5, v2

    float-to-int v4, v5

    .line 4143
    .local v4, "top":I
    iget v5, p4, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v5, v2

    float-to-int v0, v5

    .line 4145
    .local v0, "bottom":I
    iget v5, p4, Landroid/graphics/RectF;->left:F

    float-to-int v6, v5

    if-gez v4, :cond_2

    const/4 v4, 0x0

    .end local v4    # "top":I
    :cond_2
    iget v5, p4, Landroid/graphics/RectF;->right:F

    float-to-int v7, v5

    .line 4146
    int-to-float v5, v0

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v8

    cmpl-float v5, v5, v8

    if-lez v5, :cond_8

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    :goto_2
    float-to-int v5, v5

    .line 4145
    invoke-virtual {p5, v6, v4, v7, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 4148
    invoke-direct {p0, p5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->applyScaledRect(Landroid/graphics/Rect;)V

    .line 4150
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p5, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 4128
    .end local v0    # "bottom":I
    .end local v2    # "deltaY":F
    .end local v3    # "objectRect":Landroid/graphics/RectF;
    :cond_3
    iget v5, p4, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, v1

    iget v6, v6, Landroid/graphics/RectF;->left:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_4

    iget v5, p4, Landroid/graphics/RectF;->left:F

    :goto_3
    iput v5, p4, Landroid/graphics/RectF;->left:F

    .line 4129
    iget v5, p4, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, v1

    iget v6, v6, Landroid/graphics/RectF;->top:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_5

    iget v5, p4, Landroid/graphics/RectF;->top:F

    :goto_4
    iput v5, p4, Landroid/graphics/RectF;->top:F

    .line 4130
    iget v5, p4, Landroid/graphics/RectF;->right:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, v1

    iget v6, v6, Landroid/graphics/RectF;->right:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_6

    iget v5, p4, Landroid/graphics/RectF;->right:F

    :goto_5
    iput v5, p4, Landroid/graphics/RectF;->right:F

    .line 4131
    iget v5, p4, Landroid/graphics/RectF;->bottom:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, v1

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_7

    iget v5, p4, Landroid/graphics/RectF;->bottom:F

    :goto_6
    iput v5, p4, Landroid/graphics/RectF;->bottom:F

    .line 4127
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4128
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->left:F

    goto :goto_3

    .line 4129
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->top:F

    goto :goto_4

    .line 4130
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->right:F

    goto :goto_5

    .line 4131
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    goto :goto_6

    .line 4146
    .restart local v0    # "bottom":I
    .restart local v2    # "deltaY":F
    .restart local v3    # "objectRect":Landroid/graphics/RectF;
    :cond_8
    int-to-float v5, v0

    goto :goto_2
.end method

.method private findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z
    .locals 10
    .param p1, "index"    # I
    .param p2, "word"    # Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    .prologue
    const/16 v9, 0x20

    const/16 v8, 0xd

    const/16 v7, 0xa

    const/16 v6, 0x9

    const/4 v4, 0x0

    .line 4498
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v5, :cond_1

    .line 4545
    :cond_0
    :goto_0
    return v4

    .line 4501
    :cond_1
    iget-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v5, :cond_2

    .line 4502
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 4505
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I

    move-result v5

    if-ge p1, v5, :cond_0

    if-ltz p1, :cond_0

    .line 4509
    iput v4, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    iput v4, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    .line 4510
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineFromIndex(I)I

    move-result v1

    .line 4511
    .local v1, "line":I
    if-ltz v1, :cond_0

    .line 4515
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v5, :cond_0

    .line 4521
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    .line 4522
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    iput v4, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    .line 4523
    add-int/lit8 v0, p1, -0x1

    .local v0, "cnt":I
    :goto_1
    iget v4, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    if-ge v0, v4, :cond_4

    .line 4531
    :goto_2
    invoke-virtual {v3, p1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 4532
    .local v2, "oneChar":C
    if-eq v2, v9, :cond_3

    if-eq v2, v6, :cond_3

    if-eq v2, v7, :cond_3

    if-ne v2, v8, :cond_7

    .line 4533
    :cond_3
    iput p1, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    .line 4534
    const/4 v4, 0x1

    goto :goto_0

    .line 4524
    .end local v2    # "oneChar":C
    :cond_4
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 4525
    .restart local v2    # "oneChar":C
    if-eq v2, v9, :cond_5

    if-eq v2, v6, :cond_5

    if-eq v2, v7, :cond_5

    if-ne v2, v8, :cond_6

    .line 4526
    :cond_5
    add-int/lit8 v4, v0, 0x1

    iput v4, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    goto :goto_2

    .line 4523
    :cond_6
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 4537
    :cond_7
    add-int/lit8 v0, p1, 0x1

    :goto_3
    iget v4, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    if-lt v0, v4, :cond_8

    .line 4545
    :goto_4
    const/4 v4, 0x1

    goto :goto_0

    .line 4538
    :cond_8
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 4539
    if-eq v2, v9, :cond_9

    if-eq v2, v6, :cond_9

    if-eq v2, v7, :cond_9

    if-ne v2, v8, :cond_a

    .line 4540
    :cond_9
    iput v0, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    goto :goto_4

    .line 4537
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private getAbsolutePoint(FF)[F
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v4, 0x0

    .line 5923
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v5, :cond_0

    move-object v2, v4

    .line 5946
    :goto_0
    return-object v2

    .line 5926
    :cond_0
    const/4 v5, 0x2

    new-array v2, v5, [F

    const/4 v5, 0x0

    aput p1, v2, v5

    const/4 v5, 0x1

    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v6

    add-float/2addr v6, p2

    aput v6, v2, v5

    .line 5927
    .local v2, "point":[F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 5928
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 5930
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 5932
    .local v3, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    .line 5933
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v0, :cond_1

    move-object v2, v4

    .line 5935
    goto :goto_0

    .line 5938
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    invoke-direct {p0, v3, v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 5940
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 5941
    .local v1, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 5942
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v5

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v6

    invoke-virtual {v1, v4, v5, v6}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 5943
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, v4}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 5944
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    goto :goto_0
.end method

.method private getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .locals 2

    .prologue
    .line 3024
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 3025
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    .line 3026
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3028
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    return-object v0
.end method

.method private getCurrnetLineIndex(I)I
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 4567
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineCount(J)I

    move-result v1

    .line 4569
    .local v1, "lineCount":I
    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_1

    .line 4575
    const/4 v0, 0x0

    .end local v0    # "i":I
    :cond_0
    return v0

    .line 4570
    .restart local v0    # "i":I
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v2, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineStartIndex(JI)I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 4569
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private getCursorIndex(IF)I
    .locals 12
    .param p1, "line"    # I
    .param p2, "h"    # F

    .prologue
    const/4 v11, 0x0

    const/4 v1, 0x0

    .line 5222
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v9, :cond_1

    .line 5329
    :cond_0
    :goto_0
    return v1

    .line 5226
    :cond_1
    iget-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v9, :cond_2

    .line 5227
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 5230
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v9, :cond_0

    .line 5235
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lez v9, :cond_0

    .line 5237
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lt p1, v9, :cond_3

    .line 5238
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    add-int/lit8 v10, v10, -0x1

    aget v1, v9, v10

    goto :goto_0

    .line 5239
    :cond_3
    if-ltz p1, :cond_0

    .line 5244
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v8

    .line 5245
    .local v8, "str":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 5249
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v10, v10, p1

    if-ne v9, v10, :cond_4

    iget-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFirstCharLF:Z

    if-eqz v9, :cond_4

    .line 5250
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    add-int/lit8 v1, v9, 0x1

    goto :goto_0

    .line 5253
    :cond_4
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 5254
    .local v4, "oneChar":C
    const/16 v9, 0xa

    if-eq v4, v9, :cond_5

    const/16 v9, 0xd

    if-ne v4, v9, :cond_6

    .line 5255
    :cond_5
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    add-int/lit8 v5, v9, 0x1

    .line 5259
    .local v5, "start":I
    :goto_1
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v0, v9, p1

    .line 5261
    .local v0, "end":I
    if-le v5, v0, :cond_7

    move v1, v5

    .line 5262
    goto :goto_0

    .line 5257
    .end local v0    # "end":I
    .end local v5    # "start":I
    :cond_6
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v5, v9, p1

    .restart local v5    # "start":I
    goto :goto_1

    .line 5268
    .restart local v0    # "end":I
    :cond_7
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v6, v9, p1

    .local v6, "startIndex":I
    move v2, v6

    .line 5269
    .local v2, "lastIndex":I
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v10, v10, p1

    aget-object v9, v9, v10

    iget v7, v9, Landroid/graphics/RectF;->left:F

    .local v7, "startX":F
    move v3, v7

    .line 5271
    .local v3, "lastX":F
    move v1, v5

    .local v1, "index":I
    :goto_2
    if-le v1, v0, :cond_8

    .line 5319
    cmpg-float v9, p2, v7

    if-gez v9, :cond_10

    .line 5320
    invoke-direct {p0, v8, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_f

    move v1, v6

    .line 5321
    goto/16 :goto_0

    .line 5272
    :cond_8
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->left:F

    cmpl-float v9, p2, v9

    if-ltz v9, :cond_c

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->right:F

    cmpg-float v9, p2, v9

    if-gtz v9, :cond_c

    .line 5274
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    invoke-virtual {v9}, Landroid/graphics/RectF;->centerX()F

    move-result v9

    cmpl-float v9, p2, v9

    if-ltz v9, :cond_a

    .line 5275
    invoke-direct {p0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 5276
    if-gt v1, v0, :cond_9

    .line 5277
    add-int/lit8 v1, v1, 0x1

    .line 5280
    :cond_9
    :goto_3
    if-gt v1, v0, :cond_0

    .line 5281
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v9

    cmpl-float v9, v9, v11

    if-nez v9, :cond_0

    .line 5280
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 5291
    :cond_a
    invoke-direct {p0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v9

    if-nez v9, :cond_0

    .line 5294
    if-gt v1, v0, :cond_b

    .line 5295
    add-int/lit8 v1, v1, 0x1

    .line 5298
    :cond_b
    :goto_4
    if-gt v1, v0, :cond_0

    .line 5299
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v9

    cmpl-float v9, v9, v11

    if-nez v9, :cond_0

    .line 5298
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 5308
    :cond_c
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->left:F

    cmpg-float v9, v3, v9

    if-gez v9, :cond_d

    .line 5309
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v3, v9, Landroid/graphics/RectF;->left:F

    .line 5310
    move v2, v1

    .line 5312
    :cond_d
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->left:F

    cmpl-float v9, v7, v9

    if-lez v9, :cond_e

    .line 5313
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v7, v9, Landroid/graphics/RectF;->left:F

    .line 5314
    move v6, v1

    .line 5271
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 5323
    :cond_f
    add-int/lit8 v1, v6, 0x1

    goto/16 :goto_0

    .line 5326
    :cond_10
    invoke-direct {p0, v8, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 5327
    add-int/lit8 v1, v2, 0x1

    goto/16 :goto_0

    :cond_11
    move v1, v2

    .line 5329
    goto/16 :goto_0
.end method

.method private getCursorRect(I)Landroid/graphics/Rect;
    .locals 12
    .param p1, "pos"    # I

    .prologue
    .line 4310
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_1

    .line 4312
    const/4 v4, 0x0

    .line 4466
    :cond_0
    :goto_0
    return-object v4

    .line 4315
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v8, :cond_2

    .line 4316
    const/4 v4, 0x0

    goto :goto_0

    .line 4319
    :cond_2
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 4321
    .local v4, "rect":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 4323
    .local v6, "str":Ljava/lang/String;
    if-eqz v6, :cond_3

    if-ltz p1, :cond_3

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-le p1, v8, :cond_4

    .line 4324
    :cond_3
    const/4 v4, 0x0

    goto :goto_0

    .line 4327
    :cond_4
    if-lez p1, :cond_e

    .line 4328
    const/4 v7, 0x0

    .line 4329
    .local v7, "textRect":Landroid/graphics/RectF;
    iget-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v8, :cond_8

    .line 4332
    new-instance v7, Landroid/graphics/RectF;

    .end local v7    # "textRect":Landroid/graphics/RectF;
    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    .line 4333
    .restart local v7    # "textRect":Landroid/graphics/RectF;
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFrontPosition(I)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 4334
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9, p1, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 4336
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getGravity()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDeltaY(I)F

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/RectF;->offset(FF)V

    .line 4338
    add-int/lit8 v8, p1, -0x1

    invoke-direct {p0, v6, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 4339
    iget v8, v7, Landroid/graphics/RectF;->left:F

    float-to-int v8, v8

    add-int/lit8 v8, v8, -0x2

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->left:F

    float-to-int v10, v10

    .line 4340
    add-int/lit8 v10, v10, 0x2

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4339
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 4342
    :cond_5
    iget v8, v7, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    add-int/lit8 v8, v8, -0x2

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->right:F

    float-to-int v10, v10

    .line 4343
    add-int/lit8 v10, v10, 0x2

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4342
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 4346
    :cond_6
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    add-int/lit8 v10, p1, -0x1

    invoke-direct {p0, v8, v9, v10, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 4348
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getGravity()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDeltaY(I)F

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/RectF;->offset(FF)V

    .line 4350
    add-int/lit8 v8, p1, -0x1

    invoke-direct {p0, v6, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 4351
    iget v8, v7, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    add-int/lit8 v8, v8, -0x2

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->right:F

    float-to-int v10, v10

    .line 4352
    add-int/lit8 v10, v10, 0x2

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4351
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4354
    :cond_7
    iget v8, v7, Landroid/graphics/RectF;->left:F

    float-to-int v8, v8

    add-int/lit8 v8, v8, -0x2

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->left:F

    float-to-int v10, v10

    .line 4355
    add-int/lit8 v10, v10, 0x2

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4354
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4360
    :cond_8
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-nez v8, :cond_a

    .line 4361
    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 4363
    :cond_a
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFrontPosition(I)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 4364
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v7, v8, p1

    .line 4366
    add-int/lit8 v8, p1, -0x1

    invoke-direct {p0, v6, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 4367
    iget v8, v7, Landroid/graphics/RectF;->left:F

    float-to-int v8, v8

    add-int/lit8 v8, v8, -0x2

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->left:F

    float-to-int v10, v10

    .line 4368
    add-int/lit8 v10, v10, 0x2

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4367
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4370
    :cond_b
    iget v8, v7, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    add-int/lit8 v8, v8, -0x2

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->right:F

    float-to-int v10, v10

    .line 4371
    add-int/lit8 v10, v10, 0x2

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4370
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4374
    :cond_c
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    add-int/lit8 v9, p1, -0x1

    aget-object v7, v8, v9

    .line 4375
    add-int/lit8 v8, p1, -0x1

    invoke-direct {p0, v6, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 4376
    iget v8, v7, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    add-int/lit8 v8, v8, -0x2

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->right:F

    float-to-int v10, v10

    .line 4377
    add-int/lit8 v10, v10, 0x2

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4376
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4379
    :cond_d
    iget v8, v7, Landroid/graphics/RectF;->left:F

    float-to-int v8, v8

    add-int/lit8 v8, v8, -0x2

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->left:F

    float-to-int v10, v10

    .line 4380
    add-int/lit8 v10, v10, 0x2

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4379
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4385
    .end local v7    # "textRect":Landroid/graphics/RectF;
    :cond_e
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_11

    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v10, 0x0

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineStartIndex(JI)I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_11

    .line 4386
    const/4 v7, 0x0

    .line 4387
    .restart local v7    # "textRect":Landroid/graphics/RectF;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-nez v8, :cond_f

    .line 4388
    new-instance v7, Landroid/graphics/RectF;

    .end local v7    # "textRect":Landroid/graphics/RectF;
    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    .line 4389
    .restart local v7    # "textRect":Landroid/graphics/RectF;
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v10, 0x0

    invoke-direct {p0, v8, v9, v10, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 4395
    :goto_1
    invoke-direct {p0, v6, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 4396
    iget v8, v7, Landroid/graphics/RectF;->left:F

    float-to-int v8, v8

    add-int/lit8 v8, v8, -0x2

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    .line 4397
    iget v10, v7, Landroid/graphics/RectF;->left:F

    float-to-int v10, v10

    add-int/lit8 v10, v10, 0x2

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4396
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4392
    :cond_f
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    const/4 v9, 0x0

    aget-object v7, v8, v9

    goto :goto_1

    .line 4399
    :cond_10
    iget v8, v7, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    add-int/lit8 v8, v8, -0x2

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->right:F

    float-to-int v10, v10

    .line 4400
    add-int/lit8 v10, v10, 0x2

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4399
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4405
    .end local v7    # "textRect":Landroid/graphics/RectF;
    :cond_11
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v5

    .line 4406
    .local v5, "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v5, :cond_12

    .line 4407
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v0, v8, -0x1

    .local v0, "cnt":I
    :goto_2
    if-gez v0, :cond_17

    .line 4419
    .end local v0    # "cnt":I
    :cond_12
    :goto_3
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v3

    .line 4420
    .local v3, "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    if-eqz v3, :cond_13

    .line 4421
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .restart local v0    # "cnt":I
    :goto_4
    if-lt v1, v0, :cond_19

    .line 4451
    .end local v0    # "cnt":I
    .end local v1    # "i":I
    :cond_13
    if-eqz v3, :cond_14

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-gtz v8, :cond_15

    .line 4452
    :cond_14
    iget v8, v4, Landroid/graphics/Rect;->right:I

    iget v9, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x33d6bf95    # 1.0E-7f

    cmpl-float v8, v8, v9

    if-lez v8, :cond_15

    iget v8, v4, Landroid/graphics/Rect;->bottom:I

    iget v9, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x33d6bf95    # 1.0E-7f

    cmpl-float v8, v8, v9

    if-lez v8, :cond_15

    .line 4453
    iget v8, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    const v10, 0x3fa66666    # 1.3f

    mul-float/2addr v9, v10

    float-to-int v9, v9

    add-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 4457
    :cond_15
    iget v8, v4, Landroid/graphics/Rect;->right:I

    iget v9, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x33d6bf95    # 1.0E-7f

    cmpg-float v8, v8, v9

    if-ltz v8, :cond_16

    iget v8, v4, Landroid/graphics/Rect;->bottom:I

    iget v9, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x33d6bf95    # 1.0E-7f

    cmpg-float v8, v8, v9

    if-gez v8, :cond_0

    .line 4458
    :cond_16
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v8

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->top:I

    .line 4459
    iget v8, v4, Landroid/graphics/Rect;->top:I

    int-to-float v8, v8

    const v9, 0x423b3333    # 46.8f

    add-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 4460
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v8

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 4461
    iget v8, v4, Landroid/graphics/Rect;->left:I

    add-int/lit8 v8, v8, 0x4

    iput v8, v4, Landroid/graphics/Rect;->right:I

    goto/16 :goto_0

    .line 4408
    .end local v3    # "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    .restart local v0    # "cnt":I
    :cond_17
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v8, :cond_18

    .line 4409
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v8

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->top:I

    .line 4410
    iget v9, v4, Landroid/graphics/Rect;->top:I

    .line 4411
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    float-to-int v8, v8

    add-int/2addr v8, v9

    .line 4410
    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 4412
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v8

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 4413
    iget v8, v4, Landroid/graphics/Rect;->left:I

    add-int/lit8 v8, v8, 0x4

    iput v8, v4, Landroid/graphics/Rect;->right:I

    goto/16 :goto_3

    .line 4407
    :cond_18
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_2

    .line 4422
    .restart local v1    # "i":I
    .restart local v3    # "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    :cond_19
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    if-eqz v8, :cond_1a

    .line 4423
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    if-nez v8, :cond_1b

    .line 4424
    iget v9, v4, Landroid/graphics/Rect;->top:I

    .line 4425
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    float-to-int v8, v8

    add-int/2addr v8, v9

    .line 4424
    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 4433
    :cond_1a
    :goto_5
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    if-eqz v8, :cond_1d

    .line 4434
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 4435
    .local v2, "objectRect":Landroid/graphics/RectF;
    if-nez v2, :cond_1c

    .line 4437
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 4427
    .end local v2    # "objectRect":Landroid/graphics/RectF;
    :cond_1b
    iget v9, v4, Landroid/graphics/Rect;->top:I

    .line 4428
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v10, v8

    .line 4429
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    .line 4428
    mul-float/2addr v8, v10

    float-to-int v8, v8

    add-int/2addr v8, v9

    .line 4427
    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    goto :goto_5

    .line 4439
    .restart local v2    # "objectRect":Landroid/graphics/RectF;
    :cond_1c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_1e

    .line 4440
    iget v8, v4, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 4441
    iget v8, v4, Landroid/graphics/Rect;->right:I

    int-to-float v8, v8

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->right:I

    .line 4421
    .end local v2    # "objectRect":Landroid/graphics/RectF;
    :cond_1d
    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_4

    .line 4442
    .restart local v2    # "objectRect":Landroid/graphics/RectF;
    :cond_1e
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1d

    .line 4443
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v9

    sub-float/2addr v8, v9

    .line 4444
    const/high16 v9, 0x40800000    # 4.0f

    sub-float/2addr v8, v9

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    sub-float/2addr v8, v9

    float-to-int v8, v8

    .line 4443
    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 4445
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v9

    sub-float/2addr v8, v9

    const/high16 v9, 0x40800000    # 4.0f

    sub-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->right:I

    goto :goto_6
.end method

.method private getDeltaY(I)F
    .locals 9
    .param p1, "gravity"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 4269
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 4270
    .local v3, "objectRect":Landroid/graphics/RectF;
    if-nez v3, :cond_1

    .line 4305
    :cond_0
    :goto_0
    return v2

    .line 4275
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v1

    .line 4276
    .local v1, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v1, :cond_0

    .line 4281
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    .line 4282
    .local v5, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0, v5, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 4284
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v0

    .line 4285
    .local v0, "absoluteHeight":I
    const/4 v4, 0x0

    .line 4286
    .local v4, "relativeHeight":I
    if-nez v0, :cond_4

    .line 4287
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultHeight()I

    move-result v4

    .line 4292
    :goto_1
    const/4 v2, 0x0

    .line 4294
    .local v2, "diffY":F
    if-eq p1, v8, :cond_2

    const/4 v6, 0x2

    if-ne p1, v6, :cond_3

    .line 4295
    :cond_2
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v6

    int-to-float v7, v4

    sub-float v2, v6, v7

    .line 4296
    if-ne p1, v8, :cond_3

    .line 4297
    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    .line 4301
    :cond_3
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v6

    int-to-float v7, v4

    cmpg-float v6, v6, v7

    if-gez v6, :cond_0

    .line 4302
    const/4 v2, 0x0

    goto :goto_0

    .line 4289
    .end local v2    # "diffY":F
    :cond_4
    int-to-float v6, v0

    iget v7, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v6, v7

    float-to-int v4, v6

    goto :goto_1
.end method

.method private getDrawableImage(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "drawableName"    # Ljava/lang/String;

    .prologue
    .line 5109
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 5110
    .local v3, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    .line 5112
    .local v1, "mApk1Resources":Landroid/content/res/Resources;
    const-string v5, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, p1, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 5114
    .local v2, "mDrawableResID":I
    const/4 v4, 0x0

    .line 5116
    .local v4, "tmp":Landroid/graphics/Bitmap;
    :try_start_1
    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 5125
    .end local v1    # "mApk1Resources":Landroid/content/res/Resources;
    .end local v2    # "mDrawableResID":I
    .end local v3    # "manager":Landroid/content/pm/PackageManager;
    .end local v4    # "tmp":Landroid/graphics/Bitmap;
    :goto_0
    return-object v4

    .line 5117
    .restart local v1    # "mApk1Resources":Landroid/content/res/Resources;
    .restart local v2    # "mDrawableResID":I
    .restart local v3    # "manager":Landroid/content/pm/PackageManager;
    .restart local v4    # "tmp":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v0

    .line 5118
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 5122
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    .end local v1    # "mApk1Resources":Landroid/content/res/Resources;
    .end local v2    # "mDrawableResID":I
    .end local v3    # "manager":Landroid/content/pm/PackageManager;
    .end local v4    # "tmp":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v0

    .line 5123
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 5125
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private getInvMatrix()Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 4928
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 4929
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private getLineForVertical(F)I
    .locals 3
    .param p1, "v"    # F

    .prologue
    const/4 v1, 0x0

    .line 5149
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v2, :cond_0

    .line 5150
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 5153
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    if-nez v2, :cond_2

    move v0, v1

    .line 5163
    :cond_1
    :goto_0
    return v0

    .line 5157
    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    add-int/lit8 v0, v2, -0x1

    .local v0, "line":I
    :goto_1
    if-gez v0, :cond_3

    move v0, v1

    .line 5163
    goto :goto_0

    .line 5158
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/graphics/PointF;->y:F

    cmpl-float v2, p1, v2

    if-gez v2, :cond_1

    .line 5157
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method private getLineFromIndex(I)I
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 4549
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-nez v2, :cond_1

    move v0, v1

    .line 4563
    :cond_0
    :goto_0
    return v0

    .line 4553
    :cond_1
    if-gez p1, :cond_2

    move v0, v1

    .line 4554
    goto :goto_0

    .line 4557
    :cond_2
    const/4 v0, 0x0

    .local v0, "line":I
    :goto_1
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lt v0, v2, :cond_3

    move v0, v1

    .line 4563
    goto :goto_0

    .line 4558
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v2, v2, v0

    if-le p1, v2, :cond_0

    .line 4557
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "strName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 5131
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 5132
    .local v1, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    .line 5134
    .local v2, "sdkResources":Landroid/content/res/Resources;
    const-string v5, "string"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, p1, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 5135
    .local v3, "strID":I
    if-nez v3, :cond_0

    .line 5143
    .end local v1    # "manager":Landroid/content/pm/PackageManager;
    .end local v2    # "sdkResources":Landroid/content/res/Resources;
    .end local v3    # "strID":I
    :goto_0
    return-object v4

    .line 5138
    .restart local v1    # "manager":Landroid/content/pm/PackageManager;
    .restart local v2    # "sdkResources":Landroid/content/res/Resources;
    .restart local v3    # "strID":I
    :cond_0
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 5139
    .local v4, "string":Ljava/lang/String;
    goto :goto_0

    .line 5141
    .end local v1    # "manager":Landroid/content/pm/PackageManager;
    .end local v2    # "sdkResources":Landroid/content/res/Resources;
    .end local v3    # "strID":I
    .end local v4    # "string":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 5142
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private getOffsetForHorizontal(IF)I
    .locals 11
    .param p1, "line"    # I
    .param p2, "h"    # F

    .prologue
    const/4 v1, 0x0

    .line 5167
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v9, :cond_1

    .line 5216
    :cond_0
    :goto_0
    return v1

    .line 5170
    :cond_1
    iget-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v9, :cond_2

    .line 5171
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 5174
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v9, :cond_0

    .line 5178
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-ge p1, v9, :cond_0

    if-ltz p1, :cond_0

    .line 5182
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v10, v10, p1

    if-ne v9, v10, :cond_3

    .line 5183
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    add-int/lit8 v1, v9, 0x1

    goto :goto_0

    .line 5187
    :cond_3
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v8

    .line 5188
    .local v8, "str":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 5189
    .local v4, "oneChar":C
    const/16 v9, 0xa

    if-eq v4, v9, :cond_4

    const/16 v9, 0xd

    if-ne v4, v9, :cond_5

    .line 5190
    :cond_4
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    add-int/lit8 v5, v9, 0x1

    .line 5194
    .local v5, "start":I
    :goto_1
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v0, v9, p1

    .line 5199
    .local v0, "end":I
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v6, v9, p1

    .local v6, "startIndex":I
    move v2, v6

    .line 5200
    .local v2, "lastIndex":I
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v10, v10, p1

    aget-object v9, v9, v10

    iget v7, v9, Landroid/graphics/RectF;->left:F

    .local v7, "startX":F
    move v3, v7

    .line 5202
    .local v3, "lastX":F
    move v1, v5

    .local v1, "index":I
    :goto_2
    if-le v1, v0, :cond_6

    .line 5216
    cmpg-float v9, p2, v7

    if-gez v9, :cond_a

    .end local v6    # "startIndex":I
    :goto_3
    move v1, v6

    goto :goto_0

    .line 5192
    .end local v0    # "end":I
    .end local v1    # "index":I
    .end local v2    # "lastIndex":I
    .end local v3    # "lastX":F
    .end local v5    # "start":I
    .end local v7    # "startX":F
    :cond_5
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v5, v9, p1

    .restart local v5    # "start":I
    goto :goto_1

    .line 5203
    .restart local v0    # "end":I
    .restart local v1    # "index":I
    .restart local v2    # "lastIndex":I
    .restart local v3    # "lastX":F
    .restart local v6    # "startIndex":I
    .restart local v7    # "startX":F
    :cond_6
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->left:F

    cmpl-float v9, p2, v9

    if-ltz v9, :cond_7

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->right:F

    cmpg-float v9, p2, v9

    if-lez v9, :cond_0

    .line 5206
    :cond_7
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->left:F

    cmpg-float v9, v3, v9

    if-gez v9, :cond_8

    .line 5207
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v3, v9, Landroid/graphics/RectF;->left:F

    .line 5208
    move v2, v1

    .line 5210
    :cond_8
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->left:F

    cmpl-float v9, v7, v9

    if-lez v9, :cond_9

    .line 5211
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v7, v9, Landroid/graphics/RectF;->left:F

    .line 5212
    move v6, v1

    .line 5202
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_a
    move v6, v2

    .line 5216
    goto :goto_3
.end method

.method private getRelativePoint(FF)[F
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v3, 0x0

    .line 5963
    const/4 v4, 0x2

    new-array v2, v4, [F

    const/4 v4, 0x0

    aput p1, v2, v4

    const/4 v4, 0x1

    aput p2, v2, v4

    .line 5964
    .local v2, "pts":[F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v4, :cond_0

    move-object v2, v3

    .line 5979
    .end local v2    # "pts":[F
    :goto_0
    return-object v2

    .line 5968
    .restart local v2    # "pts":[F
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 5969
    .local v1, "objectRect":Landroid/graphics/RectF;
    if-nez v1, :cond_1

    move-object v2, v3

    .line 5970
    goto :goto_0

    .line 5973
    :cond_1
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 5974
    .local v0, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 5975
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 5977
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    goto :goto_0
.end method

.method private getRotatePoint(IIFFD)Landroid/graphics/PointF;
    .locals 21
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "pivotX"    # F
    .param p4, "pivotY"    # F
    .param p5, "degrees"    # D

    .prologue
    .line 5951
    invoke-static/range {p5 .. p6}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    .line 5952
    .local v4, "dSetDegree":D
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 5953
    .local v2, "cosq":D
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 5954
    .local v10, "sinq":D
    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v16, v16, p3

    move/from16 v0, v16

    float-to-double v12, v0

    .line 5955
    .local v12, "sx":D
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v16, v16, p4

    move/from16 v0, v16

    float-to-double v14, v0

    .line 5956
    .local v14, "sy":D
    mul-double v16, v12, v2

    mul-double v18, v14, v10

    sub-double v16, v16, v18

    move/from16 v0, p3

    float-to-double v0, v0

    move-wide/from16 v18, v0

    add-double v6, v16, v18

    .line 5957
    .local v6, "rx":D
    mul-double v16, v12, v10

    mul-double v18, v14, v2

    add-double v16, v16, v18

    move/from16 v0, p4

    float-to-double v0, v0

    move-wide/from16 v18, v0

    add-double v8, v16, v18

    .line 5959
    .local v8, "ry":D
    new-instance v16, Landroid/graphics/PointF;

    double-to-float v0, v6

    move/from16 v17, v0

    double-to-float v0, v8

    move/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v16
.end method

.method private getScrollBarRect()Landroid/graphics/Rect;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 3782
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_1

    .line 3802
    :cond_0
    :goto_0
    return-object v2

    .line 3786
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 3787
    .local v1, "objectRect":Landroid/graphics/RectF;
    if-eqz v1, :cond_0

    .line 3792
    iget v3, v1, Landroid/graphics/RectF;->left:F

    neg-float v3, v3

    iget v4, v1, Landroid/graphics/RectF;->top:F

    neg-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/RectF;->offset(FF)V

    .line 3794
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v0

    .line 3796
    .local v0, "deltaY":F
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 3797
    .local v2, "rect":Landroid/graphics/Rect;
    iget v3, v1, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    add-int/lit8 v3, v3, -0xd

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 3798
    iget v3, v1, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    add-int/lit8 v3, v3, -0x5

    add-int/lit8 v3, v3, 0x4

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 3799
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v4

    int-to-float v4, v4

    div-float v4, v0, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 3800
    iget v3, v2, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    goto :goto_0
.end method

.method private getSelectionRect(II)Landroid/graphics/RectF;
    .locals 6
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v4, 0x0

    .line 4470
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    move-object v3, v4

    .line 4494
    :goto_0
    return-object v3

    .line 4474
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v2

    .line 4475
    .local v2, "len":I
    if-ge p1, p2, :cond_2

    if-ltz p1, :cond_2

    if-ltz p2, :cond_2

    if-gt p1, v2, :cond_2

    if-le p2, v2, :cond_3

    :cond_2
    move-object v3, v4

    .line 4476
    goto :goto_0

    .line 4479
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-nez v5, :cond_4

    move-object v3, v4

    .line 4480
    goto :goto_0

    .line 4483
    :cond_4
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 4484
    .local v3, "rect":Landroid/graphics/RectF;
    move v1, p1

    .local v1, "i":I
    :goto_1
    if-lt v1, p2, :cond_5

    .line 4488
    invoke-virtual {v3}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_6

    move-object v3, v4

    .line 4489
    goto :goto_0

    .line 4485
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    invoke-virtual {v3, v5}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 4484
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4492
    :cond_6
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v0

    .line 4493
    .local v0, "deltaY":F
    const/4 v4, 0x0

    neg-float v5, v0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_0
.end method

.method private getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 17
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 4933
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v15, :cond_1

    .line 4934
    const/4 v14, 0x0

    .line 5104
    :cond_0
    return-object v14

    .line 4937
    :cond_1
    new-instance v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v14}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    .line 4939
    .local v14, "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    const/high16 v15, 0x42100000    # 36.0f

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 4941
    const/4 v12, 0x0

    .local v12, "sLength":I
    const/4 v4, 0x0

    .local v4, "cLength":I
    const/4 v2, 0x0

    .local v2, "bgLength":I
    const/4 v6, 0x0

    .line 4942
    .local v6, "fLength":I
    const/4 v13, 0x0

    .local v13, "sMatch":Z
    const/4 v5, 0x0

    .local v5, "cMatch":Z
    const/4 v3, 0x0

    .local v3, "bgMatch":Z
    const/4 v7, 0x0

    .line 4944
    .local v7, "fMatch":Z
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v15, v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v11

    .line 4945
    .local v11, "sInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v11, :cond_2

    .line 4946
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v15

    add-int/lit8 v8, v15, -0x1

    .local v8, "i":I
    :goto_0
    if-gez v8, :cond_4

    .line 5088
    .end local v8    # "i":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v10

    .line 5089
    .local v10, "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    if-eqz v10, :cond_0

    .line 5090
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_3
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;

    .line 5091
    .local v9, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    if-eqz v15, :cond_20

    .line 5092
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    goto :goto_1

    .line 4947
    .end local v10    # "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    .restart local v8    # "i":I
    :cond_4
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 4949
    .local v9, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v15, :cond_b

    if-nez v13, :cond_b

    .line 4950
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_8

    .line 4951
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    .line 4952
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 4953
    const/4 v13, 0x1

    .line 4946
    :cond_5
    :goto_2
    add-int/lit8 v8, v8, -0x1

    goto :goto_0

    .line 4957
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_6
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-ne v15, v0, :cond_7

    if-nez p2, :cond_5

    .line 4958
    :cond_7
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 4959
    add-int/lit8 v12, v12, 0x1

    .line 4962
    goto :goto_2

    .line 4963
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_8
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-gt v15, v0, :cond_9

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-lt v15, v0, :cond_9

    .line 4964
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 4965
    const/4 v13, 0x1

    .line 4967
    goto :goto_2

    .line 4969
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_9
    if-nez v12, :cond_a

    .line 4970
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 4971
    add-int/lit8 v12, v12, 0x1

    .line 4972
    goto :goto_2

    .line 4973
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_a
    iget v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    move/from16 v16, v0

    cmpl-float v15, v15, v16

    if-eqz v15, :cond_5

    .line 4974
    const/4 v15, 0x0

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    goto :goto_2

    .line 4978
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_b
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    if-eqz v15, :cond_10

    if-nez v5, :cond_10

    .line 4979
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_d

    .line 4980
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_c

    .line 4981
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 4982
    const/4 v5, 0x1

    .line 4984
    goto :goto_2

    .line 4986
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_c
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-eq v15, v0, :cond_5

    .line 4987
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 4988
    add-int/lit8 v4, v4, 0x1

    .line 4991
    goto :goto_2

    .line 4992
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_d
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-gt v15, v0, :cond_e

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-lt v15, v0, :cond_e

    .line 4993
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 4994
    const/4 v5, 0x1

    .line 4996
    goto/16 :goto_2

    .line 4998
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_e
    if-nez v4, :cond_f

    .line 4999
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 5000
    add-int/lit8 v4, v4, 0x1

    .line 5001
    goto/16 :goto_2

    .line 5002
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_f
    iget v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-eq v15, v0, :cond_5

    .line 5003
    const/4 v15, 0x0

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    goto/16 :goto_2

    .line 5007
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_10
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    if-eqz v15, :cond_15

    if-nez v3, :cond_15

    .line 5008
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_12

    .line 5009
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 5010
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    .line 5011
    const/4 v3, 0x1

    .line 5013
    goto/16 :goto_2

    .line 5015
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_11
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-eq v15, v0, :cond_5

    .line 5016
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    .line 5017
    add-int/lit8 v2, v2, 0x1

    .line 5020
    goto/16 :goto_2

    .line 5021
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_12
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-gt v15, v0, :cond_13

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-lt v15, v0, :cond_13

    .line 5022
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    .line 5023
    const/4 v3, 0x1

    .line 5025
    goto/16 :goto_2

    .line 5027
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_13
    if-nez v2, :cond_14

    .line 5028
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    .line 5029
    add-int/lit8 v2, v2, 0x1

    .line 5030
    goto/16 :goto_2

    .line 5031
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_14
    iget v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-eq v15, v0, :cond_5

    .line 5032
    const/4 v15, 0x0

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    goto/16 :goto_2

    .line 5036
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_15
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    if-eqz v15, :cond_1a

    if-nez v7, :cond_1a

    .line 5037
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_17

    .line 5038
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 5039
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget-object v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iput-object v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 5040
    const/4 v7, 0x1

    .line 5042
    goto/16 :goto_2

    .line 5044
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_16
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-eq v15, v0, :cond_5

    .line 5045
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget-object v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iput-object v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 5046
    add-int/lit8 v6, v6, 0x1

    .line 5049
    goto/16 :goto_2

    .line 5050
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_17
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-gt v15, v0, :cond_18

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-lt v15, v0, :cond_18

    .line 5051
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget-object v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iput-object v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 5052
    const/4 v7, 0x1

    .line 5054
    goto/16 :goto_2

    .line 5056
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_18
    if-nez v6, :cond_19

    .line 5057
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget-object v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iput-object v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 5058
    add-int/lit8 v6, v6, 0x1

    .line 5059
    goto/16 :goto_2

    .line 5060
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_19
    iget-object v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget-object v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v15

    if-eqz v15, :cond_5

    .line 5061
    const-string v15, ""

    iput-object v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    goto/16 :goto_2

    .line 5065
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_1a
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    if-eqz v15, :cond_1c

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-ne v15, v0, :cond_1b

    if-nez p2, :cond_1c

    :cond_1b
    move-object v15, v9

    .line 5066
    check-cast v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    iget-boolean v15, v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->isBold:Z

    if-eqz v15, :cond_5

    .line 5067
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-lt v0, v15, :cond_5

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-gt v0, v15, :cond_5

    .line 5068
    iget v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    or-int/lit8 v15, v15, 0x1

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    goto/16 :goto_2

    .line 5071
    :cond_1c
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    if-eqz v15, :cond_1e

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-ne v15, v0, :cond_1d

    if-nez p2, :cond_1e

    :cond_1d
    move-object v15, v9

    .line 5072
    check-cast v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    iget-boolean v15, v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->isItalic:Z

    if-eqz v15, :cond_5

    .line 5073
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-lt v0, v15, :cond_5

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-gt v0, v15, :cond_5

    .line 5074
    iget v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    or-int/lit8 v15, v15, 0x2

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    goto/16 :goto_2

    .line 5077
    :cond_1e
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    if-eqz v15, :cond_5

    .line 5078
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-ne v15, v0, :cond_1f

    if-nez p2, :cond_5

    :cond_1f
    move-object v15, v9

    .line 5079
    check-cast v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    iget-boolean v15, v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->isUnderline:Z

    if-eqz v15, :cond_5

    .line 5080
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-lt v0, v15, :cond_5

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-gt v0, v15, :cond_5

    .line 5081
    iget v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    or-int/lit8 v15, v15, 0x4

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    goto/16 :goto_2

    .line 5093
    .end local v8    # "i":I
    .local v9, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    .restart local v10    # "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    :cond_20
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    if-eqz v15, :cond_3

    move-object v15, v9

    .line 5094
    check-cast v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v15, v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    .line 5095
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    goto/16 :goto_1
.end method

.method private getTextLength()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3045
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v2, :cond_1

    .line 3054
    :cond_0
    :goto_0
    return v1

    .line 3049
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    .line 3050
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 3054
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_0
.end method

.method private hideCursorHandle()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5834
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v0, v0, v2

    if-nez v0, :cond_1

    .line 5841
    :cond_0
    :goto_0
    return-void

    .line 5838
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5839
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    .line 5840
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v0, v0, v2

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private hideDeleteTextPopup()V
    .locals 2

    .prologue
    .line 6971
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 6972
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 6974
    :cond_0
    return-void
.end method

.method private initCue()V
    .locals 11
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, -0x2

    const/4 v8, 0x2

    .line 2781
    new-array v5, v8, [Landroid/graphics/Bitmap;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    .line 2782
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v8, :cond_1

    .line 2796
    new-array v5, v8, [Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    .line 2798
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 2799
    .local v3, "parentLayout":Landroid/view/ViewGroup;
    const/4 v0, 0x0

    .line 2800
    .local v0, "bitmapIndex":I
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v8, :cond_2

    .line 2822
    .end local v0    # "bitmapIndex":I
    .end local v3    # "parentLayout":Landroid/view/ViewGroup;
    :cond_0
    return-void

    .line 2783
    :cond_1
    sget-object v5, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDrawableImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2784
    .local v4, "temp":Landroid/graphics/Bitmap;
    if-eqz v4, :cond_0

    .line 2787
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    invoke-static {v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    aput-object v6, v5, v1

    .line 2788
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v6, v6, v1

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v6, v7, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    aput-object v6, v5, v1

    .line 2789
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 2790
    const/4 v4, 0x0

    .line 2791
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 2782
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2801
    .end local v4    # "temp":Landroid/graphics/Bitmap;
    .restart local v0    # "bitmapIndex":I
    .restart local v3    # "parentLayout":Landroid/view/ViewGroup;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    new-instance v6, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    aput-object v6, v5, v1

    .line 2802
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    invoke-virtual {v5, v10}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 2804
    packed-switch v1, :pswitch_data_0

    .line 2812
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v0

    if-eqz v5, :cond_3

    .line 2813
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2814
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2817
    .local v2, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    invoke-virtual {v3, v5, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2820
    .end local v2    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2800
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2806
    :pswitch_0
    const/4 v0, 0x0

    .line 2807
    goto :goto_2

    .line 2809
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_2

    .line 2804
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private initDragText()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2877
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 2878
    .local v1, "parentLayout":Landroid/view/ViewGroup;
    new-instance v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    .line 2879
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 2880
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    .line 2881
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    .line 2880
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2882
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2883
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 2884
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setPivotX(F)V

    .line 2885
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setPivotY(F)V

    .line 2886
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v2, v4, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2887
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 2888
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2889
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2890
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2891
    return-void
.end method

.method private initEditable()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2949
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v5, :cond_0

    .line 3010
    :goto_0
    return-void

    .line 2953
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    .line 2957
    .local v3, "strBefore":Ljava/lang/String;
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    .line 2959
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v5}, Landroid/text/method/TextKeyListener;->clear(Landroid/text/Editable;)V

    .line 2960
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_7

    .line 2961
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xa

    if-eq v5, v6, :cond_6

    .line 2962
    sget-object v5, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v7, v5}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    .line 2970
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setChangeWatcher()V

    .line 2972
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 2973
    .local v1, "str":Ljava/lang/String;
    const/4 v2, 0x0

    .line 2974
    .local v2, "strAfter":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 2975
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v0

    .line 2976
    .local v0, "pos":I
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v0, v5, :cond_1

    .line 2977
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .line 2979
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 2982
    :cond_1
    invoke-virtual {v1, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 2983
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 2986
    .end local v0    # "pos":I
    :cond_2
    const-string v4, ""

    .line 2987
    .local v4, "strContent":Ljava/lang/String;
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_3

    .line 2988
    move-object v4, v3

    .line 2990
    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_4

    .line 2991
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2997
    :cond_4
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_8

    .line 2998
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v6

    invoke-interface {v5, v7, v6, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 2999
    if-eqz v3, :cond_5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v5, v6, :cond_5

    .line 3000
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-static {v5, v6}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 3007
    :cond_5
    :goto_2
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I

    .line 3009
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    goto/16 :goto_0

    .line 2964
    .end local v1    # "str":Ljava/lang/String;
    .end local v2    # "strAfter":Ljava/lang/String;
    .end local v4    # "strContent":Ljava/lang/String;
    :cond_6
    sget-object v5, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v7, v5}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    goto/16 :goto_1

    .line 2967
    :cond_7
    sget-object v5, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v7, v5}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    goto/16 :goto_1

    .line 3003
    .restart local v1    # "str":Ljava/lang/String;
    .restart local v2    # "strAfter":Ljava/lang/String;
    .restart local v4    # "strContent":Ljava/lang/String;
    :cond_8
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v5}, Landroid/text/Editable;->clear()V

    .line 3004
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v5, v7}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    goto :goto_2
.end method

.method private initHandle()V
    .locals 12

    .prologue
    const/4 v11, 0x6

    const/4 v10, 0x0

    const/4 v9, -0x2

    const/4 v8, 0x3

    .line 2825
    new-array v5, v11, [Landroid/graphics/Bitmap;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    .line 2826
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v11, :cond_1

    .line 2840
    new-array v5, v8, [Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    .line 2841
    new-array v5, v8, [Landroid/graphics/RectF;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    .line 2842
    new-array v5, v8, [Z

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    .line 2844
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 2845
    .local v3, "parentLayout":Landroid/view/ViewGroup;
    const/4 v0, 0x0

    .line 2846
    .local v0, "bitmapIndex":I
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v8, :cond_2

    .line 2874
    .end local v0    # "bitmapIndex":I
    .end local v3    # "parentLayout":Landroid/view/ViewGroup;
    :cond_0
    return-void

    .line 2827
    :cond_1
    sget-object v5, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDrawableImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2828
    .local v4, "temp":Landroid/graphics/Bitmap;
    if-eqz v4, :cond_0

    .line 2831
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    invoke-static {v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    aput-object v6, v5, v1

    .line 2832
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v6, v6, v1

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v6, v7, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    aput-object v6, v5, v1

    .line 2833
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 2834
    const/4 v4, 0x0

    .line 2835
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 2826
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2847
    .end local v4    # "temp":Landroid/graphics/Bitmap;
    .restart local v0    # "bitmapIndex":I
    .restart local v3    # "parentLayout":Landroid/view/ViewGroup;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    aput-object v6, v5, v1

    .line 2849
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    new-instance v6, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    aput-object v6, v5, v1

    .line 2850
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    invoke-virtual {v5, v10}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 2852
    packed-switch v1, :pswitch_data_0

    .line 2863
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v0

    if-eqz v5, :cond_3

    .line 2864
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2865
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2868
    .local v2, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    invoke-virtual {v3, v5, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2871
    .end local v2    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2872
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2846
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 2854
    :pswitch_0
    const/4 v0, 0x0

    .line 2855
    goto :goto_2

    .line 2857
    :pswitch_1
    const/4 v0, 0x5

    .line 2858
    goto :goto_2

    .line 2860
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_2

    .line 2852
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initMeasureInfo()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3013
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    .line 3014
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    .line 3016
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    .line 3017
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    .line 3018
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    .line 3019
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextSize:[F

    .line 3020
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextItalic:[Z

    .line 3021
    return-void
.end method

.method private initTextBox(Landroid/view/ViewGroup;)V
    .locals 6
    .param p1, "layout"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v2, -0x2

    .line 2750
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .line 2751
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    .line 2752
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    .line 2754
    if-eqz p1, :cond_0

    .line 2755
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2761
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p1, p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2764
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    new-instance v1, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;

    invoke-direct {v3, p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;)V

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    .line 2765
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;

    invoke-direct {v2, p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;)V

    invoke-virtual {v1, v2}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 2766
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 2768
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusableInTouchMode(Z)V

    .line 2769
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    .line 2770
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusable(Z)V

    .line 2774
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initHandle()V

    .line 2776
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initDragText()V

    .line 2777
    return-void
.end method

.method private isCursorVisible()Z
    .locals 1

    .prologue
    .line 5663
    const/4 v0, 0x1

    return v0
.end method

.method private isFrontPosition(I)Z
    .locals 4
    .param p1, "pos"    # I

    .prologue
    const/4 v2, 0x0

    .line 6984
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v3, :cond_0

    .line 6985
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-lt v0, v3, :cond_1

    .line 6996
    .end local v0    # "index":I
    :cond_0
    :goto_1
    return v2

    .line 6986
    .restart local v0    # "index":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v3, v3, v0

    add-int/lit8 v3, v3, 0x1

    if-ne v3, p1, :cond_2

    .line 6987
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 6988
    .local v1, "oneChar":C
    const/16 v3, 0xa

    if-eq v1, v3, :cond_0

    const/16 v3, 0xd

    if-eq v1, v3, :cond_0

    .line 6991
    const/4 v2, 0x1

    goto :goto_1

    .line 6985
    .end local v1    # "oneChar":C
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isHapticEnabled()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7324
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "spen_feedback_haptic"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 7325
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 7326
    const-string v3, "spen_feedback_haptic_pen_gesture"

    .line 7325
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 7329
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private isHighSurrogateByte(I)Z
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 2746
    const v0, 0xfc00

    and-int/2addr v0, p1

    const v1, 0xd800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isItalicAttribute(I)Z
    .locals 6
    .param p1, "pos"    # I

    .prologue
    const/4 v4, 0x0

    .line 5353
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    move v3, v4

    .line 5374
    :goto_0
    return v3

    .line 5357
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3, p1, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 5358
    .local v2, "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v2, :cond_1

    .line 5359
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .local v0, "cnt":I
    :goto_1
    if-gez v0, :cond_2

    .end local v0    # "cnt":I
    :cond_1
    move v3, v4

    .line 5374
    goto :goto_0

    .line 5360
    .restart local v0    # "cnt":I
    :cond_2
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 5361
    .local v1, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    instance-of v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    if-eqz v3, :cond_5

    .line 5362
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ne v3, p1, :cond_3

    if-eqz p1, :cond_3

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v5, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-ne v3, v5, :cond_5

    :cond_3
    move-object v3, v1

    .line 5363
    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    iget-boolean v3, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->isItalic:Z

    if-eqz v3, :cond_4

    .line 5364
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-lt p1, v3, :cond_5

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gt p1, v3, :cond_5

    .line 5365
    const/4 v3, 0x1

    goto :goto_0

    :cond_4
    move v3, v4

    .line 5368
    goto :goto_0

    .line 5359
    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method private isL2R(Ljava/lang/String;I)Z
    .locals 5
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    const/4 v3, 0x1

    .line 5379
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 5413
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 5383
    :cond_1
    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineFromIndex(I)I

    move-result v2

    .line 5384
    .local v2, "line":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v4, :cond_0

    if-ltz v2, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    array-length v4, v4

    if-ge v2, v4, :cond_0

    .line 5389
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v4, v4, v2

    if-le p2, v4, :cond_3

    .line 5413
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineL2R:[Z

    aget-boolean v3, v3, v2

    goto :goto_0

    .line 5390
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge p2, v4, :cond_2

    .line 5393
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 5394
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v1

    .line 5395
    .local v1, "dir":I
    packed-switch v1, :pswitch_data_0

    .line 5408
    :pswitch_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    .line 5406
    :pswitch_2
    const/4 v3, 0x0

    goto :goto_0

    .line 5395
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private makeDeletePopup()Landroid/widget/TextView;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 6888
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 6889
    .local v3, "res":Landroid/content/res/Resources;
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 6890
    .local v2, "dm":Landroid/util/DisplayMetrics;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6891
    const/high16 v5, 0x42aa0000    # 85.0f

    invoke-static {v7, v5, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v5

    float-to-int v5, v5

    .line 6892
    const/high16 v6, 0x420c0000    # 35.0f

    .line 6891
    invoke-static {v7, v6, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v6

    float-to-int v6, v6

    .line 6890
    invoke-direct {v0, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 6893
    .local v0, "deleteLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v1, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 6894
    .local v1, "deletePopup":Landroid/widget/TextView;
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6895
    const/16 v5, 0x11

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 6896
    const v5, -0x1a1a1b

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 6897
    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v1, v7, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 6898
    const/4 v5, 0x0

    invoke-virtual {v1, v5, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 6899
    const-string v5, "string_delete_preset"

    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 6900
    .local v4, "stringDeleteText":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 6901
    const-string v4, "Delete"

    .line 6903
    :cond_0
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6904
    const/high16 v5, -0x1000000

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 6906
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v5, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 6907
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 6909
    new-instance v5, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$10;

    invoke-direct {v5, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$10;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 6921
    return-object v1
.end method

.method private static native native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_construct(ILandroid/content/Context;)Z
.end method

.method private static native native_construct(JLandroid/content/Context;)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_finalize(J)V
.end method

.method private static native native_getHeight(I)I
.end method

.method private static native native_getHeight(J)I
.end method

.method private static native native_getHintTextWidth(I)F
.end method

.method private static native native_getHintTextWidth(J)F
.end method

.method private static native native_getLineCount(I)I
.end method

.method private static native native_getLineCount(J)I
.end method

.method private static native native_getLineEndIndex(II)I
.end method

.method private static native native_getLineEndIndex(JI)I
.end method

.method private static native native_getLinePosition(IILandroid/graphics/PointF;)Z
.end method

.method private static native native_getLinePosition(JILandroid/graphics/PointF;)Z
.end method

.method private static native native_getLineStartIndex(II)I
.end method

.method private static native native_getLineStartIndex(JI)I
.end method

.method private static native native_getPan(I)F
.end method

.method private static native native_getPan(J)F
.end method

.method private static native native_getTextRect(IILandroid/graphics/RectF;)Z
.end method

.method private static native native_getTextRect(JILandroid/graphics/RectF;)Z
.end method

.method private static native native_init()I
.end method

.method private static native native_init_64()J
.end method

.method private static native native_measure(II)Z
.end method

.method private static native native_measure(JI)Z
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setBitmap(JLandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setObjectText(ILcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z
.end method

.method private static native native_setObjectText(JLcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z
.end method

.method private static native native_setPan(IF)V
.end method

.method private static native native_setPan(JF)V
.end method

.method private static native native_update(I)Z
.end method

.method private static native native_update(J)Z
.end method

.method private onCueBottomButtonDown(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 6012
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v0

    .line 6013
    .local v0, "deltaY":F
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v5

    int-to-float v2, v5

    .line 6014
    .local v2, "minHeight":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v5, :cond_0

    .line 6015
    const/4 v5, 0x0

    .line 6038
    :goto_0
    return v5

    .line 6018
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 6020
    .local v3, "objectRect":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    cmpl-float v5, v2, v5

    if-lez v5, :cond_1

    .line 6021
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 6023
    .local v1, "endTextRect":Landroid/graphics/RectF;
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    .line 6024
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineCount(J)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-direct {p0, v8, v9, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineEndIndex(JI)I

    move-result v5

    .line 6023
    invoke-direct {p0, v6, v7, v5, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 6026
    const/4 v5, 0x2

    new-array v4, v5, [F

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    aput v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    aput v6, v4, v5

    .line 6027
    .local v4, "point":[F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 6028
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 6030
    iget v5, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    add-float/2addr v6, v0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    .line 6031
    const/4 v5, 0x0

    aget v5, v4, v5

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;->baseRate:I

    mul-int/lit8 v7, v7, 0x32

    int-to-float v7, v7

    const/high16 v8, 0x44a00000    # 1280.0f

    div-float/2addr v7, v8

    sub-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    .line 6032
    const/4 v5, 0x1

    aget v5, v4, v5

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;->baseRate:I

    mul-int/lit8 v7, v7, 0x32

    int-to-float v7, v7

    const/high16 v8, 0x44a00000    # 1280.0f

    div-float/2addr v7, v8

    sub-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    .line 6033
    const/4 v5, 0x1

    goto :goto_0

    .line 6038
    .end local v1    # "endTextRect":Landroid/graphics/RectF;
    .end local v4    # "point":[F
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private onCueBottonDown(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/16 v10, 0x8

    const/4 v0, 0x0

    const/4 v9, 0x1

    .line 6043
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_1

    .line 6086
    :cond_0
    :goto_0
    return v0

    .line 6046
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v8

    .line 6047
    .local v8, "objectRect":Landroid/graphics/RectF;
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 6049
    .local v6, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v7

    .line 6050
    .local v7, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v7, :cond_0

    .line 6055
    invoke-direct {p0, v6, v8, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6057
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueTopButtonDown(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 6058
    iget v1, v6, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v10, v10, v0

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    sub-int v2, v1, v10

    .line 6059
    .local v2, "x":I
    iget v1, v6, Landroid/graphics/RectF;->top:F

    float-to-int v3, v1

    .line 6061
    .local v3, "y":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 6062
    .local v4, "width":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 6064
    .local v5, "height":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v1, v1, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    move v0, v9

    .line 6066
    goto :goto_0

    .line 6069
    .end local v2    # "x":I
    .end local v3    # "y":I
    .end local v4    # "width":I
    .end local v5    # "height":I
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueBottomButtonDown(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 6070
    iget v0, v6, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v9

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v2, v0, 0x2

    .line 6071
    .restart local v2    # "x":I
    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v9

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v3, v0, 0x4

    .line 6073
    .restart local v3    # "y":I
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 6074
    .restart local v4    # "width":I
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 6076
    .restart local v5    # "height":I
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v1, v0, v9

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    move v0, v9

    .line 6078
    goto :goto_0

    .line 6081
    .end local v2    # "x":I
    .end local v3    # "y":I
    .end local v4    # "width":I
    .end local v5    # "height":I
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v9, :cond_4

    .line 6082
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v0, v1, v0

    invoke-virtual {v0, v10}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 6083
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v0, v0, v9

    invoke-virtual {v0, v10}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_4
    move v0, v9

    .line 6086
    goto/16 :goto_0
.end method

.method private onCueTopButtonDown(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v10, 0x44a00000    # 1280.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 5983
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v0

    .line 5984
    .local v0, "deltaY":F
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v7

    int-to-float v2, v7

    .line 5985
    .local v2, "minHeight":F
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v7, :cond_1

    .line 6008
    :cond_0
    :goto_0
    return v5

    .line 5989
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 5991
    .local v3, "objectRect":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v7

    cmpl-float v7, v2, v7

    if-lez v7, :cond_0

    .line 5992
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 5994
    .local v1, "firstTextRect":Landroid/graphics/RectF;
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9, v5, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 5996
    const/4 v7, 0x2

    new-array v4, v7, [F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    aput v7, v4, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    aput v7, v4, v6

    .line 5997
    .local v4, "point":[F
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v7, v8}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 5998
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v7, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 6000
    iget v7, v1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v7, v7, v0

    if-gez v7, :cond_0

    .line 6001
    aget v7, v4, v5

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;->baseRate:I

    mul-int/lit8 v9, v9, 0x32

    int-to-float v9, v9

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    cmpl-float v7, v7, v8

    if-lez v7, :cond_0

    .line 6002
    aget v7, v4, v6

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;->baseRate:I

    mul-int/lit8 v8, v8, 0x32

    int-to-float v8, v8

    div-float/2addr v8, v10

    cmpg-float v7, v7, v8

    if-gez v7, :cond_0

    move v5, v6

    .line 6003
    goto :goto_0
.end method

.method private onDrawCursor(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3701
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    if-eqz v7, :cond_1

    .line 3761
    :cond_0
    :goto_0
    return-void

    .line 3704
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v7, :cond_0

    .line 3708
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z

    if-eqz v7, :cond_0

    .line 3709
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v7}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v5

    .line 3710
    .local v5, "start":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v7}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    .line 3712
    .local v3, "end":I
    if-ne v5, v3, :cond_0

    .line 3716
    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v1

    .line 3717
    .local v1, "cursorRect":Landroid/graphics/Rect;
    if-eqz v1, :cond_0

    .line 3722
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    .line 3723
    .local v4, "objectRect":Landroid/graphics/RectF;
    if-eqz v4, :cond_0

    .line 3728
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v2

    .line 3729
    .local v2, "deltaY":F
    iget v7, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    cmpg-float v7, v7, v2

    if-ltz v7, :cond_0

    iget v7, v1, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v8

    add-float/2addr v8, v2

    cmpl-float v7, v7, v8

    if-gtz v7, :cond_0

    .line 3733
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7, v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 3734
    invoke-direct {p0, v1, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->adjustCursorSize(Landroid/graphics/Rect;II)V

    .line 3736
    iget v7, v1, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    sub-float/2addr v7, v2

    float-to-int v6, v7

    .line 3737
    .local v6, "top":I
    iget v7, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    sub-float/2addr v7, v2

    float-to-int v0, v7

    .line 3739
    .local v0, "bottom":I
    if-gez v6, :cond_2

    const/4 v6, 0x0

    .end local v6    # "top":I
    :cond_2
    iput v6, v1, Landroid/graphics/Rect;->top:I

    .line 3740
    int-to-float v7, v0

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v8

    cmpl-float v7, v7, v8

    if-lez v7, :cond_4

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    :goto_1
    float-to-int v7, v7

    iput v7, v1, Landroid/graphics/Rect;->bottom:I

    .line 3742
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getEllipsisType()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_3

    .line 3743
    iget v7, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v8

    cmpl-float v7, v7, v8

    if-eqz v7, :cond_0

    .line 3747
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v7, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 3748
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v7, v8}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 3749
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v7, v1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 3751
    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isItalicAttribute(I)Z

    move-result v7

    if-nez v7, :cond_5

    .line 3752
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 3740
    :cond_4
    int-to-float v7, v0

    goto :goto_1

    .line 3754
    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 3755
    sget v7, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->SIN_15_DEGREE:F

    float-to-double v8, v7

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-double v10, v7

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double/2addr v10, v12

    mul-double/2addr v8, v10

    double-to-float v7, v8

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3756
    const/high16 v7, 0x41700000    # 15.0f

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {p1, v7, v8, v9}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 3757
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 3758
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method

.method private onDrawHandle()V
    .locals 24

    .prologue
    .line 3844
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    if-eqz v2, :cond_1

    .line 4113
    :cond_0
    :goto_0
    return-void

    .line 3848
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v2, :cond_0

    .line 3852
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v16

    .line 3853
    .local v16, "start":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v12

    .line 3854
    .local v12, "end":I
    move/from16 v0, v16

    if-ne v0, v12, :cond_3

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    if-nez v2, :cond_3

    .line 3855
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/widget/ImageButton;->isShown()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3856
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3858
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/widget/ImageButton;->isShown()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3859
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 3864
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v14

    .line 3865
    .local v14, "objectRect":Landroid/graphics/RectF;
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    .line 3867
    .local v8, "relativeRect":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v9

    .line 3868
    .local v9, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v9, :cond_0

    .line 3873
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v14, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3879
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v10

    .line 3880
    .local v10, "cursorRect":Landroid/graphics/Rect;
    if-eqz v10, :cond_0

    .line 3885
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v11

    .line 3887
    .local v11, "deltaY":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getEllipsisType()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_4

    .line 3888
    iget v2, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    sub-float/2addr v2, v11

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_4

    .line 3889
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3890
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3891
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 3898
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x1

    aget-object v3, v2, v4

    .line 3900
    .local v3, "handle":Landroid/widget/ImageButton;
    move/from16 v0, v16

    if-ne v0, v12, :cond_d

    .line 3901
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    if-nez v2, :cond_5

    iget v2, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    cmpg-float v2, v2, v11

    if-ltz v2, :cond_5

    iget v2, v10, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v4

    add-float/2addr v4, v11

    cmpl-float v2, v2, v4

    if-lez v2, :cond_6

    .line 3902
    :cond_5
    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 3907
    :cond_6
    const/4 v2, 0x0

    neg-float v4, v11

    float-to-int v4, v4

    invoke-virtual {v10, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 3909
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v10}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 3910
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 3911
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v10}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 3913
    iget v2, v8, Landroid/graphics/RectF;->left:F

    invoke-virtual {v10}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    add-float v18, v2, v4

    .line 3914
    .local v18, "x":F
    iget v2, v8, Landroid/graphics/RectF;->top:F

    iget v4, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    add-float v19, v2, v4

    .line 3916
    .local v19, "y":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x5

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 3917
    .local v6, "width":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x5

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 3919
    .local v7, "height":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    if-eqz v2, :cond_a

    .line 3920
    int-to-float v2, v7

    add-float v2, v2, v19

    iget-object v4, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_b

    .line 3921
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getHeight()I

    move-result v4

    add-int/2addr v2, v4

    int-to-float v2, v2

    sub-float v19, v19, v2

    .line 3922
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x4

    aget-object v2, v2, v4

    if-eqz v2, :cond_7

    .line 3923
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x4

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3924
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x1

    const/4 v5, 0x1

    aput-boolean v5, v2, v4

    .line 3932
    :cond_7
    :goto_1
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    cmpg-float v2, v19, v2

    if-gez v2, :cond_c

    .line 3933
    :goto_2
    move/from16 v0, v18

    float-to-double v4, v0

    int-to-double v0, v6

    move-wide/from16 v20, v0

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v20, v20, v22

    sub-double v4, v4, v20

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v4, v4

    move/from16 v0, v19

    float-to-int v5, v0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    .line 3935
    move/from16 v0, v18

    float-to-double v4, v0

    int-to-double v0, v6

    move-wide/from16 v20, v0

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v20, v20, v22

    sub-double v4, v4, v20

    double-to-float v13, v4

    .line 3936
    .local v13, "left":F
    move/from16 v17, v19

    .line 3938
    .local v17, "top":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 3939
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v13

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x33d6bf95    # 1.0E-7f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_8

    .line 3940
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float v2, v2, v17

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x33d6bf95    # 1.0E-7f

    cmpl-float v2, v2, v4

    if-lez v2, :cond_9

    .line 3941
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    int-to-float v4, v6

    add-float/2addr v4, v13

    int-to-float v5, v7

    add-float v5, v5, v17

    move/from16 v0, v17

    invoke-virtual {v2, v13, v0, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 3942
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    .line 3943
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 3944
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    .line 3948
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    int-to-float v4, v6

    add-float/2addr v4, v13

    int-to-float v5, v7

    add-float v5, v5, v17

    move/from16 v0, v17

    invoke-virtual {v2, v13, v0, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 3950
    .end local v13    # "left":F
    .end local v17    # "top":F
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3951
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 3927
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x5

    aget-object v2, v2, v4

    if-eqz v2, :cond_7

    .line 3928
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x5

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3929
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput-boolean v5, v2, v4

    goto/16 :goto_1

    .line 3932
    :cond_c
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x40800000    # 4.0f

    sub-float v19, v2, v4

    goto/16 :goto_2

    .line 3956
    .end local v6    # "width":I
    .end local v7    # "height":I
    .end local v18    # "x":F
    .end local v19    # "y":F
    :cond_d
    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3961
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v10

    .line 3962
    if-eqz v10, :cond_0

    .line 3967
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x0

    aget-object v3, v2, v4

    .line 3969
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    if-nez v2, :cond_e

    iget v2, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    cmpg-float v2, v2, v11

    if-ltz v2, :cond_e

    iget v2, v10, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v4

    add-float/2addr v4, v11

    cmpl-float v2, v2, v4

    if-lez v2, :cond_10

    .line 3970
    :cond_e
    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 4040
    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v10

    .line 4041
    if-eqz v10, :cond_0

    .line 4046
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x2

    aget-object v3, v2, v4

    .line 4048
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    if-nez v2, :cond_f

    iget v2, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    cmpg-float v2, v2, v11

    if-ltz v2, :cond_f

    iget v2, v10, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v4

    add-float/2addr v4, v11

    cmpl-float v2, v2, v4

    if-lez v2, :cond_18

    .line 4049
    :cond_f
    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 3973
    :cond_10
    const/4 v2, 0x0

    neg-float v4, v11

    float-to-int v4, v4

    invoke-virtual {v10, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 3975
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v10}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 3976
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 3977
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v10}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 3979
    iget v2, v8, Landroid/graphics/RectF;->left:F

    iget v4, v10, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    add-float v18, v2, v4

    .line 3980
    .restart local v18    # "x":F
    iget v2, v8, Landroid/graphics/RectF;->top:F

    iget v4, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    add-float v19, v2, v4

    .line 3982
    .restart local v19    # "y":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 3983
    .restart local v6    # "width":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 3985
    .restart local v7    # "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup;

    .line 3986
    .local v15, "parentLayout":Landroid/view/ViewGroup;
    if-eqz v15, :cond_0

    .line 3990
    invoke-virtual {v15}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    if-eqz v2, :cond_0

    .line 3994
    int-to-float v2, v6

    sub-float v2, v18, v2

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-gez v2, :cond_14

    int-to-float v2, v7

    add-float v2, v2, v19

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_14

    .line 3995
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v7

    int-to-float v2, v2

    sub-float v19, v19, v2

    .line 3996
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    if-eqz v2, :cond_11

    .line 3997
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3998
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x0

    const/4 v5, 0x1

    aput-boolean v5, v2, v4

    .line 4021
    :cond_11
    :goto_4
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    cmpg-float v2, v19, v2

    if-gez v2, :cond_17

    .line 4022
    :goto_5
    move/from16 v0, v18

    float-to-int v4, v0

    move/from16 v0, v19

    float-to-int v5, v0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    .line 4024
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 4025
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float v2, v2, v18

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x33d6bf95    # 1.0E-7f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_12

    .line 4026
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float v2, v2, v19

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x33d6bf95    # 1.0E-7f

    cmpl-float v2, v2, v4

    if-lez v2, :cond_13

    .line 4027
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    int-to-float v4, v6

    add-float v4, v4, v18

    int-to-float v5, v7

    add-float v5, v5, v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 4028
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    .line 4029
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 4030
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    .line 4034
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    int-to-float v4, v6

    add-float v4, v4, v18

    int-to-float v5, v7

    add-float v5, v5, v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_3

    .line 4000
    :cond_14
    int-to-float v2, v7

    add-float v2, v2, v19

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_15

    .line 4001
    int-to-float v2, v6

    sub-float v18, v18, v2

    .line 4002
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v7

    int-to-float v2, v2

    sub-float v19, v19, v2

    .line 4003
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    if-eqz v2, :cond_11

    .line 4004
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4005
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x0

    const/4 v5, 0x1

    aput-boolean v5, v2, v4

    goto/16 :goto_4

    .line 4007
    :cond_15
    int-to-float v2, v6

    sub-float v2, v18, v2

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-gez v2, :cond_16

    .line 4009
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    if-eqz v2, :cond_11

    .line 4010
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4011
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput-boolean v5, v2, v4

    goto/16 :goto_4

    .line 4014
    :cond_16
    int-to-float v2, v6

    sub-float v18, v18, v2

    .line 4016
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    if-eqz v2, :cond_11

    .line 4017
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4018
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput-boolean v5, v2, v4

    goto/16 :goto_4

    .line 4021
    :cond_17
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x40800000    # 4.0f

    sub-float v19, v2, v4

    goto/16 :goto_5

    .line 4052
    .end local v6    # "width":I
    .end local v7    # "height":I
    .end local v15    # "parentLayout":Landroid/view/ViewGroup;
    .end local v18    # "x":F
    .end local v19    # "y":F
    :cond_18
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v4

    neg-float v4, v4

    float-to-int v4, v4

    invoke-virtual {v10, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 4054
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v10}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 4055
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 4056
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v10}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 4058
    iget v2, v8, Landroid/graphics/RectF;->left:F

    iget v4, v10, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    add-float v18, v2, v4

    .line 4059
    .restart local v18    # "x":F
    iget v2, v8, Landroid/graphics/RectF;->top:F

    iget v4, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    add-float v19, v2, v4

    .line 4061
    .restart local v19    # "y":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 4062
    .restart local v6    # "width":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 4064
    .restart local v7    # "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup;

    .line 4065
    .restart local v15    # "parentLayout":Landroid/view/ViewGroup;
    if-eqz v15, :cond_0

    .line 4069
    invoke-virtual {v15}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    if-eqz v2, :cond_0

    .line 4073
    int-to-float v2, v6

    add-float v2, v2, v18

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_1c

    int-to-float v2, v7

    add-float v2, v2, v19

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_1c

    .line 4074
    int-to-float v2, v6

    sub-float v18, v18, v2

    .line 4075
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v7

    int-to-float v2, v2

    sub-float v19, v19, v2

    .line 4076
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    if-eqz v2, :cond_19

    .line 4077
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4078
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x2

    const/4 v5, 0x1

    aput-boolean v5, v2, v4

    .line 4098
    :cond_19
    :goto_6
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    cmpg-float v2, v19, v2

    if-gez v2, :cond_1f

    .line 4099
    :goto_7
    move/from16 v0, v18

    float-to-int v4, v0

    move/from16 v0, v19

    float-to-int v5, v0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    .line 4101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 4102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float v2, v2, v18

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x33d6bf95    # 1.0E-7f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_1a

    .line 4103
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float v2, v2, v19

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x33d6bf95    # 1.0E-7f

    cmpl-float v2, v2, v4

    if-lez v2, :cond_1b

    .line 4104
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    int-to-float v4, v6

    add-float v4, v4, v18

    int-to-float v5, v7

    add-float v5, v5, v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 4105
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    .line 4106
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 4107
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    .line 4111
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    int-to-float v4, v6

    add-float v4, v4, v18

    int-to-float v5, v7

    add-float v5, v5, v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_0

    .line 4080
    :cond_1c
    int-to-float v2, v7

    add-float v2, v2, v19

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_1d

    .line 4081
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v7

    int-to-float v2, v2

    sub-float v19, v19, v2

    .line 4082
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    if-eqz v2, :cond_19

    .line 4083
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4084
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x2

    const/4 v5, 0x1

    aput-boolean v5, v2, v4

    goto/16 :goto_6

    .line 4086
    :cond_1d
    int-to-float v2, v6

    add-float v2, v2, v18

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_1e

    .line 4087
    int-to-float v2, v6

    sub-float v18, v18, v2

    .line 4088
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    if-eqz v2, :cond_19

    .line 4089
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4090
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x2

    const/4 v5, 0x0

    aput-boolean v5, v2, v4

    goto/16 :goto_6

    .line 4093
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    if-eqz v2, :cond_19

    .line 4094
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4095
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x2

    const/4 v5, 0x0

    aput-boolean v5, v2, v4

    goto/16 :goto_6

    .line 4098
    :cond_1f
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x40800000    # 4.0f

    sub-float v19, v2, v4

    goto/16 :goto_7
.end method

.method private onDrawScrollBar(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3765
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarVisible:Z

    if-nez v1, :cond_1

    .line 3779
    :cond_0
    :goto_0
    return-void

    .line 3769
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getScrollBarRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 3770
    .local v0, "scrollbarRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_0

    .line 3774
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 3775
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 3776
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 3778
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private onDrawSelect(Landroid/graphics/Canvas;)V
    .locals 21
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 4184
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    if-eqz v2, :cond_1

    .line 4266
    :cond_0
    :goto_0
    return-void

    .line 4188
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v4

    .line 4189
    .local v4, "startIndex":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v5

    .line 4190
    .local v5, "endIndex":I
    if-eq v5, v4, :cond_0

    .line 4194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v15

    .line 4195
    .local v15, "cursorRect":Landroid/graphics/Rect;
    if-eqz v15, :cond_0

    .line 4199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v18

    .line 4200
    .local v18, "objectRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v16

    .line 4201
    .local v16, "deltaY":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getEllipsisType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 4202
    iget v2, v15, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    sub-float v2, v2, v16

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->height()F

    move-result v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 4206
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v2, :cond_3

    .line 4207
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 4210
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v2, :cond_0

    .line 4216
    if-le v4, v5, :cond_4

    .line 4217
    move/from16 v20, v4

    .line 4218
    .local v20, "temp":I
    move v4, v5

    .line 4219
    move/from16 v5, v20

    .line 4224
    .end local v20    # "temp":I
    :cond_4
    const/16 v17, -0x1

    .local v17, "endLine":I
    move/from16 v19, v17

    .line 4227
    .local v19, "startLine":I
    const/4 v14, 0x0

    .local v14, "cnt":I
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lt v14, v2, :cond_5

    .line 4247
    :goto_2
    const/4 v2, -0x1

    move/from16 v0, v19

    if-eq v0, v2, :cond_0

    const/4 v2, -0x1

    move/from16 v0, v17

    if-eq v0, v2, :cond_0

    .line 4251
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 4252
    .local v6, "selectRect":Landroid/graphics/RectF;
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 4254
    .local v7, "drawRect":Landroid/graphics/Rect;
    move/from16 v0, v19

    move/from16 v1, v17

    if-ne v0, v1, :cond_8

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 4255
    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->drawSelectRect(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 4230
    .end local v6    # "selectRect":Landroid/graphics/RectF;
    .end local v7    # "drawRect":Landroid/graphics/Rect;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v2, v2, v14

    if-gt v2, v4, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v2, v2, v14

    if-lt v2, v4, :cond_6

    .line 4231
    move/from16 v19, v14

    .line 4233
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v2, v2, v14

    add-int/lit8 v3, v5, -0x1

    if-gt v2, v3, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v2, v2, v14

    add-int/lit8 v3, v5, -0x1

    if-lt v2, v3, :cond_7

    .line 4234
    move/from16 v17, v14

    .line 4235
    goto :goto_2

    .line 4227
    :cond_7
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 4259
    .restart local v6    # "selectRect":Landroid/graphics/RectF;
    .restart local v7    # "drawRect":Landroid/graphics/Rect;
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v2, v2, v19

    add-int/lit8 v11, v2, 0x1

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move v10, v4

    move-object v12, v6

    move-object v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->drawSelectRect(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V

    .line 4261
    add-int/lit8 v14, v19, 0x1

    :goto_3
    move/from16 v0, v17

    if-lt v14, v0, :cond_9

    .line 4265
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v10, v2, v17

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move v11, v5

    move-object v12, v6

    move-object v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->drawSelectRect(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 4262
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v10, v2, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v11, v2, v14

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object v12, v6

    move-object v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->drawSelectedLine(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V

    .line 4261
    add-int/lit8 v14, v14, 0x1

    goto :goto_3
.end method

.method private onMoveByKey(ILandroid/view/KeyEvent;)V
    .locals 27
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v22

    .line 2387
    .local v22, "tempStart":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v21

    .line 2389
    .local v21, "tempEnd":I
    move/from16 v0, v22

    move/from16 v1, v21

    if-ge v0, v1, :cond_1

    move/from16 v17, v22

    .line 2390
    .local v17, "start":I
    :goto_0
    move/from16 v0, v22

    move/from16 v1, v21

    if-ge v0, v1, :cond_2

    move/from16 v7, v21

    .line 2392
    .local v7, "end":I
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorPos()I

    move-result v15

    .local v15, "pos":I
    const/4 v12, 0x0

    .line 2393
    .local v12, "line":I
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v5

    .line 2394
    .local v5, "cursorRect":Landroid/graphics/Rect;
    if-nez v5, :cond_3

    .line 2743
    :cond_0
    :goto_2
    return-void

    .end local v5    # "cursorRect":Landroid/graphics/Rect;
    .end local v7    # "end":I
    .end local v12    # "line":I
    .end local v15    # "pos":I
    .end local v17    # "start":I
    :cond_1
    move/from16 v17, v21

    .line 2389
    goto :goto_0

    .restart local v17    # "start":I
    :cond_2
    move/from16 v7, v22

    .line 2390
    goto :goto_1

    .line 2399
    .restart local v5    # "cursorRect":Landroid/graphics/Rect;
    .restart local v7    # "end":I
    .restart local v12    # "line":I
    .restart local v15    # "pos":I
    :cond_3
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerY()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v12

    .line 2401
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    move-object/from16 v24, v0

    if-eqz v24, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v24, v0

    if-eqz v24, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v24, v0

    if-eqz v24, :cond_0

    .line 2406
    sparse-switch p1, :sswitch_data_0

    .line 2722
    :cond_4
    :goto_3
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSwipeOnKeyboard:Z

    .line 2723
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v24

    .line 2724
    const-string v25, "default_input_method"

    .line 2723
    invoke-static/range {v24 .. v25}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2725
    .local v10, "inputMethodId":Ljava/lang/String;
    const/4 v11, 0x0

    .line 2726
    .local v11, "isSamsungKeypad":Z
    if-eqz v10, :cond_5

    const-string v24, "com.sec.android.inputmethod/.SamsungKeypad"

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_5

    .line 2727
    const/4 v11, 0x1

    .line 2729
    :cond_5
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v24

    if-nez v24, :cond_6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_0

    .line 2730
    :cond_6
    if-eqz v11, :cond_50

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHardKeyboardConnected:Z

    move/from16 v24, v0

    if-nez v24, :cond_50

    .line 2731
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v24

    if-eqz v24, :cond_0

    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v24

    if-lez v24, :cond_0

    .line 2732
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSwipeOnKeyboard:Z

    .line 2733
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V

    .line 2734
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->reset()V

    .line 2735
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    goto/16 :goto_2

    .line 2409
    .end local v10    # "inputMethodId":Ljava/lang/String;
    .end local v11    # "isSamsungKeypad":Z
    :sswitch_0
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2410
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v24

    if-nez v24, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_8

    .line 2411
    :cond_7
    const/16 v24, 0x0

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2412
    :cond_8
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v24

    if-nez v24, :cond_9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_d

    .line 2413
    :cond_9
    if-lez v12, :cond_4

    .line 2414
    add-int/lit8 v24, v12, -0x1

    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v13

    .line 2415
    .local v13, "newCursorPos":I
    move/from16 v0, v17

    if-ne v0, v15, :cond_a

    .line 2416
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v13, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2418
    :cond_a
    move/from16 v0, v17

    if-ge v13, v0, :cond_b

    .line 2419
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v24

    invoke-virtual {v0, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2420
    :cond_b
    move/from16 v0, v17

    if-ne v13, v0, :cond_c

    .line 2421
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2423
    :cond_c
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v24

    invoke-virtual {v0, v1, v13, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2429
    .end local v13    # "newCursorPos":I
    :cond_d
    if-lez v12, :cond_4

    .line 2430
    add-int/lit8 v24, v12, -0x1

    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v24

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2437
    :sswitch_1
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2438
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v24

    if-nez v24, :cond_e

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_f

    .line 2439
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v24

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2440
    :cond_f
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v24

    if-nez v24, :cond_10

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_14

    .line 2441
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x1

    move/from16 v0, v24

    if-ge v12, v0, :cond_4

    .line 2442
    add-int/lit8 v24, v12, 0x1

    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v13

    .line 2443
    .restart local v13    # "newCursorPos":I
    if-ne v7, v15, :cond_11

    .line 2444
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v24

    invoke-virtual {v0, v1, v13, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2447
    :cond_11
    if-ge v13, v7, :cond_12

    .line 2448
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v13, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2449
    :cond_12
    if-ne v13, v7, :cond_13

    .line 2450
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2452
    :cond_13
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v7, v13, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2458
    .end local v13    # "newCursorPos":I
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x1

    move/from16 v0, v24

    if-ge v12, v0, :cond_4

    .line 2459
    add-int/lit8 v24, v12, 0x1

    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v24

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2466
    :sswitch_2
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2467
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v24

    if-nez v24, :cond_15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_16

    .line 2468
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v25, v0

    aget v25, v25, v12

    aget-object v24, v24, v25

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v12, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v24

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2469
    :cond_16
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v24

    if-nez v24, :cond_17

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_23

    .line 2470
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v18

    .line 2471
    .local v18, "str":Ljava/lang/String;
    if-eqz v18, :cond_4

    .line 2472
    move/from16 v0, v17

    if-ne v0, v15, :cond_1c

    .line 2473
    const/16 v16, 0x0

    .line 2474
    .local v16, "processed":Z
    new-instance v23, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    const/16 v24, 0x0

    invoke-direct/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)V

    .line 2475
    .local v23, "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    move/from16 v9, v17

    .line 2476
    .local v9, "index":I
    :cond_18
    if-gtz v9, :cond_1a

    .line 2485
    :goto_4
    if-eqz v16, :cond_4

    .line 2486
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v24

    if-nez v24, :cond_19

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_1b

    .line 2487
    :cond_19
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v24, v0

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v7, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2477
    :cond_1a
    add-int/lit8 v9, v9, -0x1

    .line 2478
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v14

    .line 2479
    .local v14, "oneChar":C
    const/16 v24, 0xa

    move/from16 v0, v24

    if-eq v14, v0, :cond_18

    const/16 v24, 0xd

    move/from16 v0, v24

    if-eq v14, v0, :cond_18

    const/16 v24, 0x20

    move/from16 v0, v24

    if-eq v14, v0, :cond_18

    const/16 v24, 0x9

    move/from16 v0, v24

    if-eq v14, v0, :cond_18

    .line 2480
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v9, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    .line 2481
    const/16 v16, 0x1

    .line 2482
    goto :goto_4

    .line 2489
    .end local v14    # "oneChar":C
    :cond_1b
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2493
    .end local v9    # "index":I
    .end local v16    # "processed":Z
    .end local v23    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    :cond_1c
    const/16 v16, 0x0

    .line 2494
    .restart local v16    # "processed":Z
    new-instance v23, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    const/16 v24, 0x0

    invoke-direct/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)V

    .line 2495
    .restart local v23    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    move v9, v7

    .line 2496
    .restart local v9    # "index":I
    :cond_1d
    if-gtz v9, :cond_1f

    .line 2505
    :goto_5
    if-eqz v16, :cond_4

    .line 2506
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v24

    if-nez v24, :cond_1e

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_22

    .line 2507
    :cond_1e
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v24, v0

    move/from16 v0, v17

    move/from16 v1, v24

    if-ge v0, v1, :cond_20

    .line 2508
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v24, v0

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2509
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2497
    :cond_1f
    add-int/lit8 v9, v9, -0x1

    .line 2498
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v14

    .line 2499
    .restart local v14    # "oneChar":C
    const/16 v24, 0xa

    move/from16 v0, v24

    if-eq v14, v0, :cond_1d

    const/16 v24, 0xd

    move/from16 v0, v24

    if-eq v14, v0, :cond_1d

    const/16 v24, 0x20

    move/from16 v0, v24

    if-eq v14, v0, :cond_1d

    const/16 v24, 0x9

    move/from16 v0, v24

    if-eq v14, v0, :cond_1d

    .line 2500
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v9, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    .line 2501
    const/16 v16, 0x1

    .line 2502
    goto :goto_5

    .line 2510
    .end local v14    # "oneChar":C
    :cond_20
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v24, v0

    move/from16 v0, v17

    move/from16 v1, v24

    if-ne v0, v1, :cond_21

    .line 2511
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2513
    :cond_21
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v24, v0

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v17

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2516
    :cond_22
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2521
    .end local v9    # "index":I
    .end local v16    # "processed":Z
    .end local v18    # "str":Ljava/lang/String;
    .end local v23    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    :cond_23
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v24

    if-nez v24, :cond_24

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_28

    .line 2522
    :cond_24
    const/16 v24, 0x1

    move/from16 v0, v24

    if-le v15, v0, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v24

    add-int/lit8 v25, v15, -0x2

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->charAt(I)C

    move-result v24

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHighSurrogateByte(I)Z

    move-result v24

    if-eqz v24, :cond_25

    const/4 v6, 0x2

    .line 2523
    .local v6, "d":I
    :goto_6
    move/from16 v0, v17

    if-ne v0, v15, :cond_26

    .line 2524
    if-lez v17, :cond_4

    .line 2525
    sub-int v24, v17, v6

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v7, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2522
    .end local v6    # "d":I
    :cond_25
    const/4 v6, 0x1

    goto :goto_6

    .line 2528
    .restart local v6    # "d":I
    :cond_26
    sub-int v24, v7, v6

    move/from16 v0, v17

    move/from16 v1, v24

    if-ge v0, v1, :cond_27

    .line 2529
    sub-int v24, v7, v6

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2530
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    sub-int v25, v7, v6

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2531
    :cond_27
    sub-int v24, v7, v6

    move/from16 v0, v17

    move/from16 v1, v24

    if-ne v0, v1, :cond_4

    .line 2532
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2536
    .end local v6    # "d":I
    :cond_28
    move/from16 v0, v17

    if-eq v0, v7, :cond_29

    .line 2537
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2539
    :cond_29
    if-lez v15, :cond_4

    .line 2540
    const/16 v24, 0x1

    move/from16 v0, v24

    if-le v15, v0, :cond_2a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v24

    add-int/lit8 v25, v15, -0x2

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->charAt(I)C

    move-result v24

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHighSurrogateByte(I)Z

    move-result v24

    if-eqz v24, :cond_2a

    const/4 v6, 0x2

    .line 2541
    .restart local v6    # "d":I
    :goto_7
    sub-int v24, v15, v6

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2540
    .end local v6    # "d":I
    :cond_2a
    const/4 v6, 0x1

    goto :goto_7

    .line 2549
    :sswitch_3
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2550
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v24

    if-nez v24, :cond_2b

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_2c

    .line 2551
    :cond_2b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v25, v0

    aget v25, v25, v12

    aget-object v24, v24, v25

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v12, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v24

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2552
    :cond_2c
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v24

    if-nez v24, :cond_2d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_39

    .line 2553
    :cond_2d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v18

    .line 2554
    .restart local v18    # "str":Ljava/lang/String;
    if-eqz v18, :cond_4

    .line 2555
    if-ne v7, v15, :cond_32

    .line 2556
    const/16 v16, 0x0

    .line 2557
    .restart local v16    # "processed":Z
    new-instance v23, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    const/16 v24, 0x0

    invoke-direct/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)V

    .line 2558
    .restart local v23    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    move v9, v7

    .line 2559
    .restart local v9    # "index":I
    :goto_8
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v24

    move/from16 v0, v24

    if-lt v9, v0, :cond_2f

    .line 2568
    :goto_9
    if-eqz v16, :cond_4

    .line 2569
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v24

    if-nez v24, :cond_2e

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_31

    .line 2570
    :cond_2e
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v24, v0

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2571
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2560
    :cond_2f
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v14

    .line 2561
    .restart local v14    # "oneChar":C
    const/16 v24, 0xa

    move/from16 v0, v24

    if-eq v14, v0, :cond_30

    const/16 v24, 0xd

    move/from16 v0, v24

    if-eq v14, v0, :cond_30

    const/16 v24, 0x20

    move/from16 v0, v24

    if-eq v14, v0, :cond_30

    const/16 v24, 0x9

    move/from16 v0, v24

    if-eq v14, v0, :cond_30

    .line 2562
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v9, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    .line 2563
    const/16 v16, 0x1

    .line 2564
    goto :goto_9

    .line 2566
    :cond_30
    add-int/lit8 v9, v9, 0x1

    goto :goto_8

    .line 2573
    .end local v14    # "oneChar":C
    :cond_31
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2577
    .end local v9    # "index":I
    .end local v16    # "processed":Z
    .end local v23    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    :cond_32
    const/16 v16, 0x0

    .line 2578
    .restart local v16    # "processed":Z
    new-instance v23, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    const/16 v24, 0x0

    invoke-direct/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)V

    .line 2579
    .restart local v23    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    move/from16 v9, v17

    .line 2580
    .restart local v9    # "index":I
    :goto_a
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v24

    move/from16 v0, v24

    if-lt v9, v0, :cond_34

    .line 2589
    :goto_b
    if-eqz v16, :cond_4

    .line 2590
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v24

    if-nez v24, :cond_33

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_38

    .line 2591
    :cond_33
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v0, v7, :cond_36

    .line 2592
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v24, v0

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v7, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2581
    :cond_34
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v14

    .line 2582
    .restart local v14    # "oneChar":C
    const/16 v24, 0xa

    move/from16 v0, v24

    if-eq v14, v0, :cond_35

    const/16 v24, 0xd

    move/from16 v0, v24

    if-eq v14, v0, :cond_35

    const/16 v24, 0x20

    move/from16 v0, v24

    if-eq v14, v0, :cond_35

    const/16 v24, 0x9

    move/from16 v0, v24

    if-eq v14, v0, :cond_35

    .line 2583
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v9, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    .line 2584
    const/16 v16, 0x1

    .line 2585
    goto :goto_b

    .line 2587
    :cond_35
    add-int/lit8 v9, v9, 0x1

    goto :goto_a

    .line 2593
    .end local v14    # "oneChar":C
    :cond_36
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ne v0, v7, :cond_37

    .line 2594
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2596
    :cond_37
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v24, v0

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v7, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2597
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2600
    :cond_38
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2605
    .end local v9    # "index":I
    .end local v16    # "processed":Z
    .end local v18    # "str":Ljava/lang/String;
    .end local v23    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    :cond_39
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v24

    if-nez v24, :cond_3a

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_3e

    .line 2606
    :cond_3a
    add-int/lit8 v24, v15, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_3b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v24

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHighSurrogateByte(I)Z

    move-result v24

    if-eqz v24, :cond_3b

    const/4 v6, 0x2

    .line 2607
    .restart local v6    # "d":I
    :goto_c
    if-ne v7, v15, :cond_3c

    .line 2608
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Landroid/text/Editable;->length()I

    move-result v24

    move/from16 v0, v24

    if-ge v7, v0, :cond_4

    .line 2609
    add-int v24, v7, v6

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2610
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    add-int v25, v7, v6

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2606
    .end local v6    # "d":I
    :cond_3b
    const/4 v6, 0x1

    goto :goto_c

    .line 2613
    .restart local v6    # "d":I
    :cond_3c
    add-int v24, v17, v6

    move/from16 v0, v24

    if-ge v0, v7, :cond_3d

    .line 2614
    add-int v24, v17, v6

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v7, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2615
    :cond_3d
    add-int v24, v17, v6

    move/from16 v0, v24

    if-ne v0, v7, :cond_4

    .line 2616
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2620
    .end local v6    # "d":I
    :cond_3e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v24, v0

    if-lez v24, :cond_4

    .line 2621
    add-int/lit8 v24, v15, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_3f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v24

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHighSurrogateByte(I)Z

    move-result v24

    if-eqz v24, :cond_3f

    const/4 v6, 0x2

    .line 2622
    .restart local v6    # "d":I
    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, -0x1

    aget v24, v24, v25

    add-int v24, v24, v6

    move/from16 v0, v24

    if-ge v15, v0, :cond_4

    .line 2623
    move/from16 v0, v17

    if-eq v0, v7, :cond_40

    .line 2624
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2621
    .end local v6    # "d":I
    :cond_3f
    const/4 v6, 0x1

    goto :goto_d

    .line 2626
    .restart local v6    # "d":I
    :cond_40
    add-int v24, v15, v6

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2635
    .end local v6    # "d":I
    :sswitch_4
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2637
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v24, v0

    aget v24, v24, v12

    if-ltz v24, :cond_0

    .line 2641
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v25, v0

    aget v25, v25, v12

    aget-object v24, v24, v25

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v12, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v19

    .line 2642
    .local v19, "temp":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v24

    if-nez v24, :cond_41

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_42

    :cond_41
    move/from16 v0, v19

    move/from16 v1, v17

    if-eq v0, v1, :cond_42

    .line 2643
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v17

    move/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2645
    :cond_42
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2651
    .end local v19    # "temp":I
    :sswitch_5
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2653
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v24, v0

    aget v24, v24, v12

    if-ltz v24, :cond_0

    .line 2657
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v25, v0

    aget v25, v25, v12

    aget-object v24, v24, v25

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v12, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v4

    .line 2660
    .local v4, "cursorPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v24, v0

    if-eqz v24, :cond_44

    .line 2661
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    if-lez v24, :cond_44

    .line 2662
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    if-lt v8, v0, :cond_46

    .line 2676
    :goto_f
    if-lez v4, :cond_44

    .line 2677
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v25, v0

    aget v25, v25, v12

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->charAt(I)C

    move-result v24

    const/16 v25, 0x20

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_43

    .line 2678
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v25, v0

    aget v25, v25, v12

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->charAt(I)C

    move-result v24

    const/16 v25, 0x9

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_4a

    .line 2680
    :cond_43
    add-int/lit8 v4, v4, -0x1

    .line 2690
    .end local v8    # "i":I
    :cond_44
    :goto_10
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v24

    if-nez v24, :cond_45

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_4b

    :cond_45
    if-eq v4, v7, :cond_4b

    .line 2691
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v7, v4, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2663
    .restart local v8    # "i":I
    :cond_46
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v24, v0

    aget v24, v24, v8

    move/from16 v0, v24

    if-ne v4, v0, :cond_49

    .line 2664
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x1

    move/from16 v0, v24

    if-ne v8, v0, :cond_47

    .line 2665
    add-int/lit8 v4, v4, 0x1

    .line 2666
    goto :goto_f

    .line 2668
    :cond_47
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v24

    add-int/lit8 v25, v4, 0x1

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->charAt(I)C

    move-result v14

    .line 2669
    .restart local v14    # "oneChar":C
    const/16 v24, 0xa

    move/from16 v0, v24

    if-eq v14, v0, :cond_48

    const/16 v24, 0xd

    move/from16 v0, v24

    if-ne v14, v0, :cond_49

    .line 2670
    :cond_48
    add-int/lit8 v4, v4, 0x1

    .line 2671
    goto/16 :goto_f

    .line 2662
    .end local v14    # "oneChar":C
    :cond_49
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_e

    .line 2681
    :cond_4a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x1

    move/from16 v0, v24

    if-ge v12, v0, :cond_44

    .line 2682
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v25, v0

    add-int/lit8 v26, v12, 0x1

    aget v25, v25, v26

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->charAt(I)C

    move-result v24

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_44

    .line 2683
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v25, v0

    add-int/lit8 v26, v12, 0x1

    aget v25, v25, v26

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->charAt(I)C

    move-result v24

    const/16 v25, 0xd

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_44

    .line 2684
    add-int/lit8 v4, v4, -0x1

    goto/16 :goto_10

    .line 2693
    .end local v8    # "i":I
    :cond_4b
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2699
    .end local v4    # "cursorPos":I
    :sswitch_6
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2700
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v24

    if-nez v24, :cond_4c

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_4d

    :cond_4c
    if-eqz v17, :cond_4d

    .line 2701
    const/16 v24, 0x0

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v17

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2703
    :cond_4d
    const/16 v24, 0x0

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2709
    :sswitch_7
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2710
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v24, v0

    if-lez v24, :cond_4

    .line 2711
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, -0x1

    aget v24, v24, v25

    add-int/lit8 v20, v24, 0x1

    .line 2712
    .local v20, "temp1":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v24

    if-nez v24, :cond_4e

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v24, v0

    if-eqz v24, :cond_4f

    :cond_4e
    move/from16 v0, v20

    if-eq v7, v0, :cond_4f

    .line 2713
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v24

    invoke-virtual {v0, v7, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2715
    :cond_4f
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2737
    .end local v20    # "temp1":I
    .restart local v10    # "inputMethodId":Ljava/lang/String;
    .restart local v11    # "isSamsungKeypad":Z
    :cond_50
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHardKeyboardConnected:Z

    move/from16 v24, v0

    if-eqz v24, :cond_0

    .line 2738
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V

    .line 2739
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->reset()V

    .line 2740
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    goto/16 :goto_2

    .line 2406
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x5c -> :sswitch_6
        0x5d -> :sswitch_7
        0x7a -> :sswitch_4
        0x7b -> :sswitch_5
        0x91 -> :sswitch_5
        0x92 -> :sswitch_1
        0x93 -> :sswitch_7
        0x94 -> :sswitch_2
        0x96 -> :sswitch_3
        0x97 -> :sswitch_4
        0x98 -> :sswitch_0
        0x99 -> :sswitch_6
    .end sparse-switch
.end method

.method private paste(Landroid/content/ClipboardManager;II)Z
    .locals 9
    .param p1, "clipboard"    # Landroid/content/ClipboardManager;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v8, 0x0

    .line 7118
    const/4 v4, 0x0

    .line 7119
    .local v4, "str":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    .line 7120
    .local v0, "clip":Landroid/content/ClipData;
    if-eqz v0, :cond_0

    .line 7121
    invoke-virtual {v0, v8}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v2

    .line 7122
    .local v2, "item":Landroid/content/ClipData$Item;
    if-eqz v2, :cond_0

    .line 7123
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    .line 7124
    .local v3, "sequence":Ljava/lang/CharSequence;
    if-eqz v3, :cond_0

    .line 7125
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 7126
    if-eqz v4, :cond_0

    .line 7127
    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 7128
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 7129
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->appendText(Ljava/lang/String;)V

    .line 7130
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v5, :cond_1

    .line 7131
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 7132
    const-string v6, "string_pasted_to_clipboard"

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 7131
    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    .line 7137
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const/16 v6, 0x50

    const/16 v7, 0x96

    invoke-virtual {v5, v6, v8, v7}, Landroid/widget/Toast;->setGravity(III)V

    .line 7138
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 7141
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    sub-int v6, p3, p2

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    sub-int v1, v5, v6

    .line 7142
    .local v1, "diff":I
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v6

    add-int/2addr v6, v1

    if-le v5, v6, :cond_2

    .line 7143
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    add-int p3, p2, v5

    .line 7147
    :goto_1
    if-le p2, p3, :cond_3

    .end local p2    # "start":I
    :goto_2
    invoke-virtual {p0, p2, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 7153
    .end local v1    # "diff":I
    .end local v2    # "item":Landroid/content/ClipData$Item;
    .end local v3    # "sequence":Ljava/lang/CharSequence;
    :cond_0
    const/4 v4, 0x0

    .line 7154
    const/4 v0, 0x0

    .line 7155
    const/4 v5, 0x1

    return v5

    .line 7134
    .restart local v2    # "item":Landroid/content/ClipData$Item;
    .restart local v3    # "sequence":Ljava/lang/CharSequence;
    .restart local p2    # "start":I
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string v6, "string_pasted_to_clipboard"

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 7135
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v5, v8}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_0

    .line 7145
    .restart local v1    # "diff":I
    :cond_2
    sub-int p3, p2, v1

    goto :goto_1

    :cond_3
    move p2, p3

    .line 7147
    goto :goto_2
.end method

.method private redo()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 7207
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->nextText:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 7208
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 7209
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    .line 7210
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->nextText:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setText(Ljava/lang/String;)V

    .line 7211
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    .line 7212
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    if-gez v0, :cond_0

    .line 7213
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 7215
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    invoke-virtual {p0, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 7216
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 7218
    :cond_1
    return v1
.end method

.method private relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/RectF;
    .param p3, "coordinateInfo"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .prologue
    .line 5344
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 5345
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    .line 5346
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    .line 5345
    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 5347
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 5348
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    .line 5349
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    .line 5348
    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 5350
    return-void
.end method

.method private removeHoveringIcon(I)Z
    .locals 6
    .param p1, "nHoverIconID"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7222
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-nez v4, :cond_1

    .line 7258
    :cond_0
    :goto_0
    return v2

    .line 7225
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 7226
    .local v1, "pm":Landroid/content/pm/PackageManager;
    if-eqz v1, :cond_0

    .line 7227
    const-string v4, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 7235
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(I)V

    .line 7236
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-virtual {v4, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->removeHoveringSpenCustomIcon(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    move v2, v3

    .line 7258
    goto :goto_0

    .line 7237
    :catch_0
    move-exception v0

    .line 7238
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "SpenTextBox"

    const-string v4, "removeCustomHoveringIcon() IllegalArgumentException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 7239
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 7241
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 7242
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v3, "SpenTextBox"

    const-string v4, "removeCustomHoveringIcon() ClassNotFoundException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 7243
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 7245
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_2
    move-exception v0

    .line 7246
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v3, "SpenTextBox"

    const-string v4, "removeCustomHoveringIcon() NoSuchMethodException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 7247
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 7249
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_3
    move-exception v0

    .line 7250
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "SpenTextBox"

    const-string v4, "removeCustomHoveringIcon() IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 7251
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 7253
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 7254
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "SpenTextBox"

    const-string v4, "removeCustomHoveringIcon() IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 7255
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private resize(FF)V
    .locals 10
    .param p1, "relativeWidth"    # F
    .param p2, "relativeHeight"    # F

    .prologue
    const/4 v9, 0x0

    .line 3260
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 3261
    .local v3, "prevRelativeRect":Landroid/graphics/RectF;
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 3263
    .local v4, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    .line 3264
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v0, :cond_1

    .line 3325
    :cond_0
    :goto_0
    return-void

    .line 3269
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v7, :cond_0

    .line 3273
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 3274
    .local v2, "objectRelativeRect":Landroid/graphics/RectF;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 3275
    .local v1, "objectRect":Landroid/graphics/RectF;
    if-eqz v1, :cond_0

    .line 3279
    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3281
    invoke-virtual {v3, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 3283
    invoke-virtual {v4, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 3284
    iget v7, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v7, p2

    iput v7, v4, Landroid/graphics/RectF;->bottom:F

    .line 3285
    iget v7, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v7, p1

    iput v7, v4, Landroid/graphics/RectF;->right:F

    .line 3287
    invoke-virtual {v2, v4}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 3289
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v5

    .line 3290
    .local v5, "rotation":F
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setRotation(F)V

    .line 3292
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 3294
    .local v6, "tempAbsoluteRect":Landroid/graphics/RectF;
    cmpl-float v7, v5, v9

    if-nez v7, :cond_3

    .line 3296
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotX(F)V

    .line 3297
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v7

    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotY(F)V

    .line 3298
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 3300
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotX(F)V

    .line 3301
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v7

    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotY(F)V

    .line 3302
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 3304
    iget v7, v4, Landroid/graphics/RectF;->left:F

    iget v8, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v7, v8

    iget v8, v3, Landroid/graphics/RectF;->top:F

    .line 3305
    iget v9, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v8, v9

    .line 3304
    invoke-virtual {v2, v7, v8}, Landroid/graphics/RectF;->offset(FF)V

    .line 3314
    :cond_2
    :goto_1
    invoke-direct {p0, v6, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3316
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    const/4 v8, 0x1

    invoke-virtual {v7, v6, v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setRect(Landroid/graphics/RectF;Z)V

    .line 3318
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onObjectChanged()V

    goto :goto_0

    .line 3307
    :cond_3
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v8

    cmpl-float v7, v7, v8

    if-nez v7, :cond_4

    .line 3308
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    sub-float/2addr v7, v8

    invoke-virtual {v2, v9, v7}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_1

    .line 3309
    :cond_4
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v8

    cmpl-float v7, v7, v8

    if-nez v7, :cond_2

    .line 3310
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    sub-float/2addr v7, v8

    invoke-virtual {v2, v7, v9}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_1
.end method

.method private selectAll()Z
    .locals 1

    .prologue
    .line 7000
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelectionAll()V

    .line 7001
    const/4 v0, 0x1

    return v0
.end method

.method private sendAccessibilityEventTypeViewTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "beforeText"    # Ljava/lang/CharSequence;
    .param p2, "fromIndex"    # I
    .param p3, "removedCount"    # I
    .param p4, "addedCount"    # I

    .prologue
    .line 6755
    const/16 v1, 0x10

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 6756
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0, p2}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 6757
    invoke-virtual {v0, p3}, Landroid/view/accessibility/AccessibilityEvent;->setRemovedCount(I)V

    .line 6758
    invoke-virtual {v0, p4}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    .line 6759
    invoke-virtual {v0, p1}, Landroid/view/accessibility/AccessibilityEvent;->setBeforeText(Ljava/lang/CharSequence;)V

    .line 6760
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 6761
    return-void
.end method

.method private setChangeWatcher()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 4579
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-nez v6, :cond_1

    .line 4606
    :cond_0
    :goto_0
    return-void

    .line 4583
    :cond_1
    const/16 v0, 0x64

    .line 4584
    .local v0, "PRIORITY":I
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v4

    .line 4586
    .local v4, "textLength":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    .line 4588
    .local v3, "sp":Landroid/text/Spannable;
    invoke-interface {v3}, Landroid/text/Spannable;->length()I

    move-result v6

    const-class v7, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    invoke-interface {v3, v8, v6, v7}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    .line 4589
    .local v5, "watchers":[Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;
    array-length v1, v5

    .line 4590
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-lt v2, v1, :cond_3

    .line 4594
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    if-nez v6, :cond_2

    .line 4595
    new-instance v6, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    .line 4600
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    const v7, 0x640012

    invoke-interface {v3, v6, v8, v4, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 4603
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    if-eqz v6, :cond_0

    .line 4604
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    const/16 v7, 0x12

    invoke-interface {v3, v6, v8, v4, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 4591
    :cond_3
    aget-object v6, v5, v2

    invoke-interface {v3, v6}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 4590
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private setCustomHoveringIcon()I
    .locals 8

    .prologue
    const/4 v3, -0x1

    .line 7262
    const/4 v1, -0x1

    .line 7263
    .local v1, "nHoverIconID":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_1

    :cond_0
    move v3, v1

    .line 7303
    :goto_0
    return v3

    .line 7267
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 7268
    .local v2, "pm":Landroid/content/pm/PackageManager;
    if-eqz v2, :cond_2

    .line 7269
    const-string v4, "com.sec.feature.hovering_ui"

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    move v3, v1

    .line 7270
    goto :goto_0

    :cond_2
    move v3, v1

    .line 7273
    goto :goto_0

    .line 7277
    :cond_3
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(ILandroid/graphics/drawable/Drawable;Landroid/graphics/Point;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    move-result v1

    move v3, v1

    .line 7303
    goto :goto_0

    .line 7278
    :catch_0
    move-exception v0

    .line 7279
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "SpenTextBox"

    const-string v5, "setCustomHoveringIcon() IllegalArgumentException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 7280
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 7282
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 7283
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v4, "SpenTextBox"

    const-string v5, "setCustomHoveringIcon() NotFoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 7284
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0

    .line 7286
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    :catch_2
    move-exception v0

    .line 7287
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v4, "SpenTextBox"

    const-string v5, "setCustomHoveringIcon() ClassNotFoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 7288
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 7290
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_3
    move-exception v0

    .line 7291
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v4, "SpenTextBox"

    const-string v5, "setCustomHoveringIcon() NoSuchMethodException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 7292
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 7294
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v0

    .line 7295
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "SpenTextBox"

    const-string v5, "setCustomHoveringIcon() IllegalAccessException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 7296
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 7298
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_5
    move-exception v0

    .line 7299
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v4, "SpenTextBox"

    const-string v5, "setCustomHoveringIcon() InvocationTargetException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 7300
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V
    .locals 8
    .param p1, "handle"    # Landroid/view/View;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "relativeObjectRect"    # Landroid/graphics/RectF;

    .prologue
    .line 3808
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v4, :cond_1

    .line 3841
    :cond_0
    :goto_0
    return-void

    .line 3812
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 3813
    .local v2, "parentLayout":Landroid/view/ViewGroup;
    if-eqz v2, :cond_0

    .line 3817
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3819
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 3820
    .local v0, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 3821
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v4

    invoke-virtual {p6}, Landroid/graphics/RectF;->centerX()F

    move-result v5

    invoke-virtual {p6}, Landroid/graphics/RectF;->centerY()F

    move-result v6

    invoke-virtual {v0, v4, v5, v6}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 3823
    const/4 v4, 0x2

    new-array v3, v4, [F

    const/4 v4, 0x0

    int-to-float v5, p2

    aput v5, v3, v4

    const/4 v4, 0x1

    int-to-float v5, p3

    aput v5, v3, v4

    .line 3824
    .local v3, "points":[F
    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 3826
    const/4 v4, 0x0

    aget v4, v3, v4

    float-to-int v4, v4

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 3827
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    iget v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    add-int/2addr v5, p4

    sub-int/2addr v4, v5

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3828
    const/4 v4, 0x1

    aget v4, v3, v4

    float-to-int v4, v4

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3829
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    iget v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    add-int/2addr v5, p5

    sub-int/2addr v4, v5

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 3830
    iput p4, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 3831
    iput p5, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 3833
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/View;->setPivotX(F)V

    .line 3834
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/View;->setPivotY(F)V

    .line 3835
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v4

    invoke-virtual {p1, v4}, Landroid/view/View;->setRotation(F)V

    .line 3836
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 3838
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3839
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3840
    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    goto :goto_0
.end method

.method private setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 7307
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 7312
    :goto_0
    return-void

    .line 7310
    :cond_0
    const-string v0, "SpenTextBox"

    const-string v1, "setHoverPointerDrawable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 7311
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private setParagraphAlign(I)V
    .locals 8
    .param p1, "align"    # I

    .prologue
    .line 4824
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v7, :cond_1

    .line 4855
    :cond_0
    :goto_0
    return-void

    .line 4828
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v5

    .line 4829
    .local v5, "paragraphs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    if-eqz v5, :cond_0

    .line 4834
    const/4 v4, 0x0

    .line 4835
    .local v4, "paragraphNumber":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v6

    .line 4836
    .local v6, "text":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 4837
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v2

    .line 4838
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_3

    .line 4845
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_2
    add-int/lit8 v4, v4, 0x1

    .line 4847
    new-instance v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;-><init>()V

    .line 4848
    .local v3, "para":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;
    const/4 v7, 0x0

    iput v7, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->startPos:I

    .line 4849
    iput v4, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->endPos:I

    .line 4850
    iput p1, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    .line 4852
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4854
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7, v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setParagraph(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 4839
    .end local v3    # "para":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_3
    invoke-virtual {v6, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 4840
    .local v0, "c":C
    const/16 v7, 0xa

    if-eq v0, v7, :cond_4

    const/16 v7, 0xd

    if-ne v0, v7, :cond_5

    .line 4841
    :cond_4
    add-int/lit8 v4, v4, 0x1

    .line 4838
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private setParagraphIndent(I)V
    .locals 0
    .param p1, "indent"    # I

    .prologue
    .line 4858
    return-void
.end method

.method private setParagraphSpacing(IF)V
    .locals 5
    .param p1, "type"    # I
    .param p2, "spacing"    # F

    .prologue
    const/4 v4, 0x0

    .line 4902
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_1

    .line 4925
    :cond_0
    :goto_0
    return-void

    .line 4906
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v1

    .line 4907
    .local v1, "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    if-eqz v1, :cond_0

    .line 4911
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;-><init>()V

    .line 4912
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->startPos:I

    .line 4913
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->endPos:I

    .line 4914
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    .line 4915
    .local v2, "str":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 4916
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    iput v3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->endPos:I

    .line 4919
    :cond_2
    iput p1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    .line 4920
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    .line 4922
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4924
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setParagraph(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private setTextBackgroundColor(III)V
    .locals 5
    .param p1, "color"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 4732
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    .line 4752
    :goto_0
    return-void

    .line 4736
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;-><init>()V

    .line 4737
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v3

    if-nez v3, :cond_2

    .line 4738
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 4739
    .local v1, "str":Ljava/lang/String;
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4740
    if-nez v1, :cond_1

    :goto_1
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4741
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v1    # "str":Ljava/lang/String;
    :goto_2
    move-object v2, v0

    .line 4749
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    iput p1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    .line 4751
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4740
    .restart local v1    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_1

    .line 4744
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4745
    iput p3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4746
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextBold(ZII)V
    .locals 5
    .param p1, "bold"    # Z
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 4755
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    .line 4775
    :goto_0
    return-void

    .line 4759
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;-><init>()V

    .line 4760
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v3

    if-nez v3, :cond_2

    .line 4761
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 4762
    .local v1, "str":Ljava/lang/String;
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4763
    if-nez v1, :cond_1

    :goto_1
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4764
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v1    # "str":Ljava/lang/String;
    :goto_2
    move-object v2, v0

    .line 4772
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    iput-boolean p1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->isBold:Z

    .line 4774
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4763
    .restart local v1    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_1

    .line 4767
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4768
    iput p3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4769
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextFontColor(III)V
    .locals 8
    .param p1, "color"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v7, 0x3

    const/4 v5, 0x0

    .line 4674
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v4, :cond_0

    .line 4706
    :goto_0
    return-void

    .line 4678
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getSpan()Ljava/util/ArrayList;

    move-result-object v2

    .line 4679
    .local v2, "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v2, :cond_2

    .line 4680
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 4691
    :cond_2
    :goto_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;-><init>()V

    .line 4692
    .local v1, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v4

    if-nez v4, :cond_5

    .line 4693
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    .line 4694
    .local v3, "str":Ljava/lang/String;
    iput v5, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4695
    if-nez v3, :cond_4

    move v4, v5

    :goto_2
    iput v4, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4696
    iput v7, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v3    # "str":Ljava/lang/String;
    :goto_3
    move-object v4, v1

    .line 4704
    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    iput p1, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    .line 4705
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4680
    .end local v1    # "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 4681
    .local v0, "inf":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    instance-of v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    if-eqz v4, :cond_1

    .line 4682
    iget v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ne v4, p2, :cond_1

    iget v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-ne v4, p3, :cond_1

    move-object v4, v0

    .line 4683
    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    if-eq v4, p1, :cond_1

    .line 4684
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->removeSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_1

    .line 4695
    .end local v0    # "inf":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    .restart local v1    # "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    .restart local v3    # "str":Ljava/lang/String;
    :cond_4
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    goto :goto_2

    .line 4699
    .end local v3    # "str":Ljava/lang/String;
    :cond_5
    iput p2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4700
    iput p3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4701
    iput v7, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_3
.end method

.method private setTextFontName(Ljava/lang/String;II)V
    .locals 5
    .param p1, "font"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 4709
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    .line 4729
    :goto_0
    return-void

    .line 4713
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;-><init>()V

    .line 4714
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v3

    if-nez v3, :cond_2

    .line 4715
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 4716
    .local v1, "str":Ljava/lang/String;
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4717
    if-nez v1, :cond_1

    :goto_1
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4718
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v1    # "str":Ljava/lang/String;
    :goto_2
    move-object v2, v0

    .line 4726
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    iput-object p1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    .line 4728
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4717
    .restart local v1    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_1

    .line 4721
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4722
    iput p3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4723
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextFontSize(FII)V
    .locals 5
    .param p1, "size"    # F
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 4651
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    .line 4671
    :goto_0
    return-void

    .line 4655
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;-><init>()V

    .line 4656
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v3

    if-nez v3, :cond_2

    .line 4657
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 4658
    .local v1, "str":Ljava/lang/String;
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4659
    if-nez v1, :cond_1

    :goto_1
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4660
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v1    # "str":Ljava/lang/String;
    :goto_2
    move-object v2, v0

    .line 4668
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iput p1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    .line 4670
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4659
    .restart local v1    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_1

    .line 4663
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4664
    iput p3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4665
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextItalic(ZII)V
    .locals 5
    .param p1, "italic"    # Z
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 4778
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    .line 4798
    :goto_0
    return-void

    .line 4782
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;-><init>()V

    .line 4783
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v3

    if-nez v3, :cond_2

    .line 4784
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 4785
    .local v1, "str":Ljava/lang/String;
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4786
    if-nez v1, :cond_1

    :goto_1
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4787
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v1    # "str":Ljava/lang/String;
    :goto_2
    move-object v2, v0

    .line 4795
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    iput-boolean p1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->isItalic:Z

    .line 4797
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4786
    .restart local v1    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_1

    .line 4790
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4791
    iput p3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4792
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextUnderline(ZII)V
    .locals 5
    .param p1, "underline"    # Z
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 4801
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    .line 4821
    :goto_0
    return-void

    .line 4805
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;-><init>()V

    .line 4806
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v3

    if-nez v3, :cond_2

    .line 4807
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 4808
    .local v1, "str":Ljava/lang/String;
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4809
    if-nez v1, :cond_1

    :goto_1
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4810
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v1    # "str":Ljava/lang/String;
    :goto_2
    move-object v2, v0

    .line 4818
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    iput-boolean p1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->isUnderline:Z

    .line 4820
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4809
    .restart local v1    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_1

    .line 4813
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4814
    iput p3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4815
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private shouldBlink()Z
    .locals 1

    .prologue
    .line 5667
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCursorVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5668
    :cond_0
    const/4 v0, 0x0

    .line 5670
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private showCursorHandle()V
    .locals 6

    .prologue
    .line 5812
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    if-nez v0, :cond_0

    .line 5813
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    .line 5816
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5817
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1388

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;->postAtTime(Ljava/lang/Runnable;J)Z

    .line 5819
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    .line 5820
    return-void
.end method

.method private showDeletePopupWindow()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    .line 6925
    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10}, Landroid/graphics/RectF;-><init>()V

    .line 6926
    .local v10, "objectRelativeRect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v2

    invoke-direct {p0, v10, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6927
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v13

    .line 6928
    .local v13, "pos":I
    invoke-direct {p0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 6929
    .local v0, "cRect":Landroid/graphics/Rect;
    if-nez v0, :cond_0

    .line 6961
    :goto_0
    return-void

    .line 6932
    :cond_0
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 6933
    .local v8, "cursorRect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->top:F

    const/high16 v3, 0x40a00000    # 5.0f

    add-float/2addr v2, v3

    invoke-virtual {v8, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 6934
    new-instance v9, Landroid/graphics/RectF;

    invoke-direct {v9}, Landroid/graphics/RectF;-><init>()V

    .line 6935
    .local v9, "cursorRelativeRect":Landroid/graphics/RectF;
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v1

    invoke-direct {p0, v9, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6936
    invoke-virtual {v9}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    float-to-int v2, v1

    iget v1, v9, Landroid/graphics/RectF;->bottom:F

    float-to-int v3, v1

    .line 6937
    invoke-virtual {v10}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v10}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v1

    float-to-double v6, v1

    move-object v1, p0

    .line 6936
    invoke-direct/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v12

    .line 6939
    .local v12, "point":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6940
    .local v11, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, v12, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    .line 6941
    const/4 v2, 0x1

    const/high16 v3, 0x42200000    # 40.0f

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 6942
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 6941
    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    .line 6940
    iput v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 6943
    iget v1, v12, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    iput v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 6944
    iget v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v1, :cond_3

    .line 6945
    iput v14, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 6949
    :cond_1
    :goto_1
    iget v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v1, :cond_4

    .line 6950
    iput v14, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 6954
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6956
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v1, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 6957
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->bringToFront()V

    .line 6959
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeletePopupHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeletePopupRunable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 6960
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeletePopupHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeletePopupRunable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xfa0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 6946
    :cond_3
    iget v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getWidth()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    if-le v2, v1, :cond_1

    .line 6947
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    goto :goto_1

    .line 6951
    :cond_4
    iget v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getHeight()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    if-le v2, v1, :cond_2

    .line 6952
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto :goto_2
.end method

.method private showDragText(ZII)V
    .locals 15
    .param p1, "visible"    # Z
    .param p2, "posX"    # I
    .param p3, "posY"    # I

    .prologue
    .line 2894
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 2946
    :cond_0
    :goto_0
    return-void

    .line 2897
    :cond_1
    if-eqz p1, :cond_8

    .line 2898
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v6, 0x14

    if-le v3, v6, :cond_7

    .line 2899
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    const/16 v6, 0x13

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHighSurrogateByte(I)Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v10, 0x13

    .line 2900
    .local v10, "length":I
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2904
    .end local v10    # "length":I
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/widget/TextView;->measure(II)V

    .line 2905
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    .line 2906
    .local v2, "height":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v14

    .line 2908
    .local v14, "width":I
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    .line 2909
    .local v13, "relativeRect":Landroid/graphics/RectF;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v6

    invoke-direct {p0, v13, v3, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2911
    move/from16 v0, p2

    int-to-float v3, v0

    iget v6, v13, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v6

    int-to-float v6, v14

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    sub-float/2addr v3, v6

    float-to-int v4, v3

    .line 2912
    .local v4, "pivotX":I
    move/from16 v0, p3

    int-to-float v3, v0

    iget v6, v13, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v6

    int-to-float v6, v2

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    sub-float/2addr v3, v6

    float-to-int v5, v3

    .line 2914
    .local v5, "pivotY":I
    iget v3, v13, Landroid/graphics/RectF;->left:F

    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v6, v3

    .line 2915
    iget v3, v13, Landroid/graphics/RectF;->top:F

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    add-float/2addr v7, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v3

    float-to-double v8, v3

    move-object v3, p0

    .line 2914
    invoke-direct/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v12

    .line 2917
    .local v12, "point":Landroid/graphics/PointF;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2918
    .local v11, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget v3, v12, Landroid/graphics/PointF;->x:F

    float-to-int v3, v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2919
    iget v3, v12, Landroid/graphics/PointF;->y:F

    float-to-int v3, v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2921
    iget v6, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLeft()I

    move-result v3

    if-ge v6, v3, :cond_2

    .line 2922
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLeft()I

    move-result v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2924
    :cond_2
    iget v6, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getTop()I

    move-result v3

    if-ge v6, v3, :cond_3

    .line 2925
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getTop()I

    move-result v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2927
    :cond_3
    iget v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    add-int v6, v3, v14

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getRight()I

    move-result v3

    if-le v6, v3, :cond_4

    .line 2928
    iget v6, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    add-int v7, v3, v14

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getRight()I

    move-result v3

    sub-int v3, v7, v3

    sub-int v3, v6, v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2930
    :cond_4
    iget v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    add-int v6, v3, v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getBottom()I

    move-result v3

    if-le v6, v3, :cond_5

    .line 2931
    iget v6, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    add-int v7, v3, v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getBottom()I

    move-result v3

    sub-int v3, v7, v3

    sub-int v3, v6, v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2934
    :cond_5
    iput v14, v11, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2935
    iput v2, v11, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2937
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2939
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setRotation(F)V

    .line 2941
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2942
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->bringToFront()V

    goto/16 :goto_0

    .line 2899
    .end local v2    # "height":I
    .end local v4    # "pivotX":I
    .end local v5    # "pivotY":I
    .end local v11    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v12    # "point":Landroid/graphics/PointF;
    .end local v13    # "relativeRect":Landroid/graphics/RectF;
    .end local v14    # "width":I
    :cond_6
    const/16 v10, 0x14

    goto/16 :goto_1

    .line 2902
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2944
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private showScrollBar()V
    .locals 6

    .prologue
    .line 5823
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    if-nez v0, :cond_0

    .line 5824
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    .line 5827
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5828
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1f4

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->postAtTime(Ljava/lang/Runnable;J)Z

    .line 5830
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarVisible:Z

    .line 5831
    return-void
.end method

.method private undo()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 7173
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 7174
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 7175
    const/4 v0, 0x0

    .line 7176
    .local v0, "same":Z
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 7177
    .local v1, "text":Ljava/lang/String;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->nextText:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 7178
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7179
    const/4 v0, 0x1

    .line 7183
    :cond_0
    if-nez v0, :cond_1

    .line 7184
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->nextText:Ljava/lang/String;

    .line 7187
    :cond_1
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    .line 7189
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setText(Ljava/lang/String;)V

    .line 7191
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    .line 7193
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    if-gez v2, :cond_2

    .line 7194
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 7196
    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_4

    .line 7197
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    invoke-virtual {p0, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 7201
    :goto_0
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 7203
    .end local v0    # "same":Z
    .end local v1    # "text":Ljava/lang/String;
    :cond_3
    return v5

    .line 7199
    .restart local v0    # "same":Z
    .restart local v1    # "text":Ljava/lang/String;
    :cond_4
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    invoke-virtual {p0, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto :goto_0
.end method

.method private updateContextmenu()V
    .locals 22

    .prologue
    .line 1776
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    if-eqz v19, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mDirtyFlag:Z

    move/from16 v19, v0

    if-eqz v19, :cond_a

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    move/from16 v19, v0

    if-eqz v19, :cond_a

    .line 1777
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->reset()V

    .line 1780
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup;

    .line 1781
    .local v12, "parentLayout":Landroid/view/ViewGroup;
    new-instance v13, Landroid/graphics/Rect;

    invoke-direct {v13}, Landroid/graphics/Rect;-><init>()V

    .line 1782
    .local v13, "parentRect":Landroid/graphics/Rect;
    invoke-virtual {v12, v13}, Landroid/view/ViewGroup;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1785
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1787
    .local v10, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v18, Landroid/graphics/Rect;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Rect;-><init>()V

    .line 1788
    .local v18, "textRect":Landroid/graphics/Rect;
    iget v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1789
    iget v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1790
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    iget v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1791
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    iget v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1793
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v16

    .line 1794
    .local v16, "selStart":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v15

    .line 1795
    .local v15, "selEnd":I
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v17

    .line 1796
    .local v17, "startCursorRect":Landroid/graphics/Rect;
    if-nez v17, :cond_1

    .line 1869
    .end local v10    # "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v12    # "parentLayout":Landroid/view/ViewGroup;
    .end local v13    # "parentRect":Landroid/graphics/Rect;
    .end local v15    # "selEnd":I
    .end local v16    # "selStart":I
    .end local v17    # "startCursorRect":Landroid/graphics/Rect;
    .end local v18    # "textRect":Landroid/graphics/Rect;
    :cond_0
    :goto_0
    return-void

    .line 1800
    .restart local v10    # "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v12    # "parentLayout":Landroid/view/ViewGroup;
    .restart local v13    # "parentRect":Landroid/graphics/Rect;
    .restart local v15    # "selEnd":I
    .restart local v16    # "selStart":I
    .restart local v17    # "startCursorRect":Landroid/graphics/Rect;
    .restart local v18    # "textRect":Landroid/graphics/Rect;
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v6

    .line 1801
    .local v6, "endCursorRect":Landroid/graphics/Rect;
    if-eqz v6, :cond_0

    .line 1806
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v5

    .line 1808
    .local v5, "deltaY":F
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v20, v20, v5

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v17

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 1809
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v20, v20, v5

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 1812
    new-instance v14, Landroid/graphics/Rect;

    invoke-direct {v14}, Landroid/graphics/Rect;-><init>()V

    .line 1813
    .local v14, "rect":Landroid/graphics/Rect;
    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v9, v0, [Landroid/graphics/Rect;

    .line 1815
    .local v9, "listHandleRect":[Landroid/graphics/Rect;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .line 1816
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v4, v0

    .line 1817
    .local v4, "cursorHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->getMenuHeight()F

    move-result v19

    move/from16 v0, v19

    float-to-int v11, v0

    .line 1818
    .local v11, "menuHeight":I
    const/4 v8, 0x0

    .line 1820
    .local v8, "isHandleButtonShown":Z
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    const/16 v19, 0x3

    move/from16 v0, v19

    if-lt v7, v0, :cond_3

    .line 1840
    if-eqz v8, :cond_7

    .line 1841
    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-gt v0, v1, :cond_2

    .line 1843
    iget v0, v14, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_6

    .line 1844
    iget v0, v14, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v14, Landroid/graphics/Rect;->top:I

    .line 1845
    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    add-int v19, v19, v11

    move/from16 v0, v19

    iput v0, v14, Landroid/graphics/Rect;->bottom:I

    .line 1864
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setRect(Landroid/graphics/Rect;)V

    .line 1865
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->show()V

    goto/16 :goto_0

    .line 1821
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    move-object/from16 v19, v0

    aget-object v19, v19, v7

    if-eqz v19, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    move-object/from16 v19, v0

    aget-object v19, v19, v7

    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v19

    if-nez v19, :cond_4

    .line 1822
    new-instance v19, Landroid/graphics/Rect;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Rect;-><init>()V

    aput-object v19, v9, v7

    .line 1823
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    move-object/from16 v19, v0

    aget-object v19, v19, v7

    aget-object v20, v9, v7

    invoke-virtual/range {v19 .. v20}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 1825
    aget-object v19, v9, v7

    iget v0, v13, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/Rect;->offset(II)V

    .line 1827
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    move-object/from16 v19, v0

    aget-boolean v19, v19, v7

    if-eqz v19, :cond_5

    .line 1828
    aget-object v19, v9, v7

    aget-object v20, v9, v7

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    sub-int v20, v20, v11

    add-int/lit8 v20, v20, -0xf

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1829
    aget-object v19, v9, v7

    aget-object v20, v9, v7

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    add-int v20, v20, v4

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1834
    :goto_3
    const/4 v8, 0x1

    .line 1836
    aget-object v19, v9, v7

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 1820
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 1831
    :cond_5
    aget-object v19, v9, v7

    aget-object v20, v9, v7

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    sub-int v20, v20, v11

    sub-int v20, v20, v4

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->top:I

    goto :goto_3

    .line 1847
    :cond_6
    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v14, Landroid/graphics/Rect;->top:I

    goto/16 :goto_2

    .line 1852
    :cond_7
    move-object/from16 v14, v17

    .line 1853
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v19

    add-int/lit16 v0, v0, -0x190

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_8

    .line 1854
    const/16 v19, 0x0

    const/16 v20, -0x190

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v14, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    goto/16 :goto_2

    .line 1855
    :cond_8
    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    move/from16 v0, v19

    add-int/lit16 v0, v0, 0x140

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_9

    .line 1856
    move-object v14, v6

    .line 1857
    const/16 v19, 0x0

    const/16 v20, 0xc8

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v14, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    goto/16 :goto_2

    .line 1859
    :cond_9
    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v14, Landroid/graphics/Rect;->top:I

    goto/16 :goto_2

    .line 1866
    .end local v4    # "cursorHeight":I
    .end local v5    # "deltaY":F
    .end local v6    # "endCursorRect":Landroid/graphics/Rect;
    .end local v7    # "i":I
    .end local v8    # "isHandleButtonShown":Z
    .end local v9    # "listHandleRect":[Landroid/graphics/Rect;
    .end local v10    # "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v11    # "menuHeight":I
    .end local v12    # "parentLayout":Landroid/view/ViewGroup;
    .end local v13    # "parentRect":Landroid/graphics/Rect;
    .end local v14    # "rect":Landroid/graphics/Rect;
    .end local v15    # "selEnd":I
    .end local v16    # "selStart":I
    .end local v17    # "startCursorRect":Landroid/graphics/Rect;
    .end local v18    # "textRect":Landroid/graphics/Rect;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    move/from16 v19, v0

    if-nez v19, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isContextMenuShowing()Z

    move-result v19

    if-eqz v19, :cond_0

    .line 1867
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    goto/16 :goto_0
.end method

.method private updateEditable()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2371
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    .line 2372
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2373
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 2374
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v5, v3, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 2376
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v0

    .line 2377
    .local v0, "cursorPos":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 2378
    .local v1, "len":I
    if-ltz v0, :cond_0

    if-gt v0, v1, :cond_0

    .line 2379
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 2382
    .end local v0    # "cursorPos":I
    .end local v1    # "len":I
    :cond_0
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    .line 2383
    return-void
.end method

.method private updateSelection()V
    .locals 7

    .prologue
    .line 4632
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v6, "input_method"

    invoke-virtual {v1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 4633
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4634
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    .line 4635
    .local v2, "selStart":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    .line 4636
    .local v3, "selEnd":I
    const/4 v4, -0x1

    .line 4637
    .local v4, "candStart":I
    const/4 v5, -0x1

    .line 4638
    .local v5, "candEnd":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 4639
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v4

    .line 4640
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v5

    :cond_0
    move-object v1, p0

    .line 4643
    invoke-virtual/range {v0 .. v5}, Landroid/view/inputmethod/InputMethodManager;->updateSelection(Landroid/view/View;IIII)V

    .line 4648
    .end local v2    # "selStart":I
    .end local v3    # "selEnd":I
    .end local v4    # "candStart":I
    .end local v5    # "candEnd":I
    :cond_1
    return-void
.end method

.method private updateSettingInfo()V
    .locals 3

    .prologue
    .line 4609
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v2, :cond_1

    .line 4629
    :cond_0
    :goto_0
    return-void

    .line 4612
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v2, :cond_0

    .line 4613
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v0

    .line 4614
    .local v0, "pos":I
    invoke-direct {p0, v0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v1

    .line 4616
    .local v1, "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    goto :goto_0
.end method


# virtual methods
.method public appendText(Ljava/lang/String;)V
    .locals 6
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 872
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 873
    .local v1, "start":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 875
    .local v0, "end":I
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    const/16 v4, 0x1388

    if-eq v2, v4, :cond_1

    .line 876
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    if-le v2, v4, :cond_0

    .line 877
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2, v1, v0, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 885
    :goto_0
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-eqz v2, :cond_2

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    .line 887
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 888
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 889
    invoke-virtual {p0, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 890
    return-void

    .line 879
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    sub-int/2addr v4, v1

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v1, v0, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0

    .line 882
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2, v1, v0, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0

    .line 885
    :cond_2
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public close()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 549
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 550
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideSoftInput()V

    .line 552
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 554
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 555
    .local v2, "layout":Landroid/view/ViewGroup;
    if-eqz v2, :cond_1

    .line 556
    invoke-virtual {v2, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 557
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v3, 0x3

    if-lt v1, v3, :cond_6

    .line 560
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 561
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 562
    invoke-virtual {v2}, Landroid/view/ViewGroup;->invalidate()V

    .line 565
    .end local v1    # "i":I
    :cond_1
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    cmp-long v3, v4, v8

    if-eqz v3, :cond_2

    .line 566
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_finalize(J)V

    .line 567
    iput-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    .line 569
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_2

    .line 570
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 571
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    .line 575
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    const/4 v3, 0x6

    if-lt v1, v3, :cond_7

    .line 582
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v3, :cond_3

    .line 583
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 584
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->close()V

    .line 587
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v3, :cond_4

    .line 588
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v3}, Landroid/text/Editable;->getFilters()[Landroid/text/InputFilter;

    move-result-object v0

    .line 589
    .local v0, "arrayOfInputFilter":[Landroid/text/InputFilter;
    if-eqz v0, :cond_4

    .line 590
    const/4 v3, 0x0

    aget-object v3, v0, v3

    check-cast v3, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->close()V

    .line 594
    .end local v0    # "arrayOfInputFilter":[Landroid/text/InputFilter;
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v3, :cond_5

    .line 595
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->close()V

    .line 596
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 598
    :cond_5
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 599
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 601
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    .line 602
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 603
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 604
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    .line 605
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    .line 606
    return-void

    .line 558
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 557
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 576
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v1

    if-eqz v3, :cond_8

    .line 577
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 578
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aput-object v6, v3, v1

    .line 575
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2022
    invoke-super {p0, p1}, Landroid/view/View;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public enableTextInput(Z)V
    .locals 3
    .param p1, "option"    # Z

    .prologue
    const/4 v2, 0x1

    .line 1127
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditable:Z

    .line 1129
    if-eqz p1, :cond_0

    .line 1130
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateEditable()V

    .line 1131
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusableInTouchMode(Z)V

    .line 1132
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    .line 1133
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusable(Z)V

    .line 1134
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 1136
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->requestFocus()Z

    .line 1138
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsShowSoftInputEnable:Z

    if-eqz v0, :cond_1

    .line 1139
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showSoftInput()V

    .line 1146
    :cond_0
    :goto_0
    return-void

    .line 1140
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsShowSoftInputEnable:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHardKeyboardHidden:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1141
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showSoftInput()V

    goto :goto_0

    .line 1143
    :cond_2
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsShowSoftInputEnable:Z

    goto :goto_0
.end method

.method public enableTouch(Z)V
    .locals 0
    .param p1, "touchEnable"    # Z

    .prologue
    .line 5852
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTouchEnable:Z

    .line 5853
    return-void
.end method

.method public fit(Z)V
    .locals 20
    .param p1, "checkCursorPos"    # Z

    .prologue
    .line 627
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const-wide/16 v18, 0x0

    cmp-long v3, v4, v18

    if-nez v3, :cond_1

    .line 732
    :cond_0
    :goto_0
    return-void

    .line 632
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v3, :cond_0

    .line 636
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->adjustTextBoxRect()V

    .line 640
    const/4 v2, 0x0

    .line 641
    .local v2, "content":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v3, :cond_2

    .line 642
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 644
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-nez v3, :cond_3

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 645
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 646
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateEditable()V

    .line 647
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 651
    :cond_3
    if-eqz p1, :cond_4

    .line 652
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->adjustTextBox()V

    .line 655
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v9

    .line 656
    .local v9, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v9, :cond_0

    .line 661
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v13

    .line 662
    .local v13, "objectRect":Landroid/graphics/RectF;
    if-eqz v13, :cond_0

    .line 667
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 669
    .local v15, "relativeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 671
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup;

    .line 672
    .local v14, "parentLayout":Landroid/view/ViewGroup;
    if-eqz v14, :cond_7

    .line 673
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 675
    .local v12, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v3, v15, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iput v3, v12, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 676
    iget v3, v15, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iput v3, v12, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 677
    invoke-virtual {v14}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, v15, Landroid/graphics/RectF;->left:F

    invoke-virtual {v15}, Landroid/graphics/RectF;->width()F

    move-result v5

    add-float/2addr v4, v5

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v12, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 678
    invoke-virtual {v15}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    iput v3, v12, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 679
    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    iput v3, v12, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 681
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_a

    .line 682
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    move/from16 v16, v0

    .line 683
    .local v16, "width":I
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v11, v4

    .line 684
    .local v11, "height":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    move/from16 v0, v16

    if-ne v3, v0, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-eq v3, v11, :cond_9

    .line 686
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 688
    if-lez v16, :cond_6

    if-lez v11, :cond_6

    .line 690
    :try_start_0
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v16

    invoke-static {v0, v11, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    .line 691
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setBitmap(JLandroid/graphics/Bitmap;)Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 714
    :cond_6
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 717
    .end local v11    # "height":I
    .end local v12    # "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v16    # "width":I
    :cond_7
    invoke-virtual {v15}, Landroid/graphics/RectF;->width()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotX(F)V

    .line 718
    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotY(F)V

    .line 719
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setRotation(F)V

    .line 721
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget v4, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    iget v5, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    const/4 v8, 0x0

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v3, v4, v5, v8, v0}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 722
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    if-eqz v3, :cond_8

    .line 723
    const/4 v6, 0x1

    .line 724
    .local v6, "textOnCanvasCommand":I
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 725
    .local v7, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 726
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 729
    .end local v6    # "textOnCanvasCommand":I
    .end local v7    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_8
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_update(J)Z

    .line 730
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->restartBlink()V

    .line 731
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    goto/16 :goto_0

    .line 692
    .restart local v11    # "height":I
    .restart local v12    # "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v16    # "width":I
    :catch_0
    move-exception v10

    .line 693
    .local v10, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v10}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_1

    .line 699
    .end local v10    # "e":Ljava/lang/OutOfMemoryError;
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v8

    invoke-virtual {v3, v4, v5, v8}, Landroid/graphics/Bitmap;->setPixel(III)V

    goto/16 :goto_1

    .line 702
    .end local v11    # "height":I
    .end local v16    # "width":I
    :cond_a
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    move/from16 v16, v0

    .line 703
    .restart local v16    # "width":I
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v11, v4

    .line 704
    .restart local v11    # "height":I
    if-lez v16, :cond_6

    if-lez v11, :cond_6

    .line 706
    :try_start_1
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v16

    invoke-static {v0, v11, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    .line 707
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setBitmap(JLandroid/graphics/Bitmap;)Z
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 708
    :catch_1
    move-exception v10

    .line 709
    .restart local v10    # "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v10}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public getCursorPos()I
    .locals 1

    .prologue
    .line 1179
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_0

    .line 1180
    const/4 v0, 0x0

    .line 1183
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v0

    goto :goto_0
.end method

.method public getDefaultHeight()I
    .locals 14

    .prologue
    const/4 v7, 0x0

    .line 1446
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v6, :cond_0

    move v6, v7

    .line 1490
    :goto_0
    return v6

    .line 1450
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v6

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getBottomMargin()F

    move-result v8

    add-float/2addr v6, v8

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1452
    .local v0, "absoluteHeight":I
    const/4 v2, 0x0

    .line 1453
    .local v2, "fontSize":F
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v8

    .line 1454
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v9

    .line 1453
    invoke-virtual {v6, v8, v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v5

    .line 1455
    .local v5, "sInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v5, :cond_2

    .line 1456
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_7

    .line 1463
    :cond_2
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v4

    .line 1464
    .local v4, "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    if-eqz v4, :cond_4

    .line 1465
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_8

    .line 1476
    :cond_4
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-gtz v6, :cond_6

    .line 1477
    :cond_5
    const/4 v6, 0x0

    cmpl-float v6, v2, v6

    if-lez v6, :cond_a

    .line 1478
    int-to-double v8, v0

    float-to-double v10, v2

    const-wide v12, 0x3ff4cccccccccccdL    # 1.3

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    double-to-int v0, v8

    .line 1484
    :cond_6
    :goto_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v1

    .line 1485
    .local v1, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v1, :cond_b

    move v6, v7

    .line 1487
    goto :goto_0

    .line 1456
    .end local v1    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .end local v4    # "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    :cond_7
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 1457
    .local v3, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    instance-of v8, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v8, :cond_1

    .line 1458
    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v3    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v2, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    .line 1459
    goto :goto_1

    .line 1465
    .restart local v4    # "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    :cond_8
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;

    .line 1466
    .local v3, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    instance-of v6, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    if-eqz v6, :cond_3

    move-object v6, v3

    .line 1467
    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    const/4 v9, 0x1

    if-ne v6, v9, :cond_9

    .line 1468
    int-to-float v6, v0

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    .end local v3    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    iget v9, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    mul-float/2addr v9, v2

    add-float/2addr v6, v9

    float-to-int v0, v6

    .line 1469
    goto :goto_2

    .line 1470
    .restart local v3    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    :cond_9
    int-to-float v6, v0

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    .end local v3    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    iget v9, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    add-float/2addr v6, v9

    float-to-int v0, v6

    goto :goto_2

    .line 1480
    :cond_a
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    float-to-int v0, v6

    goto :goto_3

    .line 1490
    .restart local v1    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    :cond_b
    int-to-float v6, v0

    iget v7, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v6, v6

    goto/16 :goto_0
.end method

.method public getDefaultWidth()I
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 1418
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v7, :cond_1

    .line 1442
    :cond_0
    :goto_0
    return v6

    .line 1422
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v8

    add-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1423
    .local v0, "absoluteWidth":I
    const/4 v2, 0x0

    .line 1425
    .local v2, "fontWidth":F
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I

    move-result v5

    .line 1426
    .local v5, "stringLength":I
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 1427
    .local v4, "rect":Landroid/graphics/RectF;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v5, :cond_2

    .line 1434
    const/high16 v7, 0x40800000    # 4.0f

    add-float/2addr v2, v7

    .line 1436
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v1

    .line 1437
    .local v1, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v1, :cond_0

    .line 1442
    int-to-float v6, v0

    add-float/2addr v6, v2

    iget v7, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v6, v6

    goto :goto_0

    .line 1428
    .end local v1    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    :cond_2
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 1429
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    cmpg-float v7, v2, v7

    if-gez v7, :cond_3

    .line 1430
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 1427
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public getMaximumHeight()F
    .locals 2

    .prologue
    .line 1414
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public getMaximumWidth()F
    .locals 24

    .prologue
    .line 1326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v13

    .line 1327
    .local v13, "objectRect":Landroid/graphics/RectF;
    if-nez v13, :cond_0

    .line 1328
    const/4 v12, 0x0

    .line 1410
    :goto_0
    return v12

    .line 1331
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v7

    .line 1332
    .local v7, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v7, :cond_1

    .line 1334
    const/4 v12, 0x0

    goto :goto_0

    .line 1337
    :cond_1
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 1338
    .local v15, "relativeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 1342
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 1343
    .local v6, "absoluteTextRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v5

    .line 1345
    .local v5, "absoluteRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v22

    add-float v4, v21, v22

    .line 1346
    .local v4, "absoluteMargin":F
    const/4 v14, 0x0

    .line 1347
    .local v14, "relativeMargin":F
    const/16 v16, 0x0

    .line 1349
    .local v16, "relativeTextWidth":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v18

    .line 1350
    .local v18, "str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->isHintTextEnabled()Z

    move-result v21

    if-eqz v21, :cond_5

    if-eqz v18, :cond_2

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v21

    if-nez v21, :cond_5

    .line 1351
    :cond_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHintTextWidth(J)F

    move-result v21

    add-float v4, v4, v21

    .line 1369
    :cond_3
    :goto_1
    iget v0, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v21, v0

    mul-float v14, v21, v4

    .line 1371
    move v11, v4

    .line 1372
    .local v11, "lineAbsoluteWidth":F
    move v12, v14

    .line 1373
    .local v12, "lineRelativeWidth":F
    move/from16 v19, v4

    .line 1374
    .local v19, "tempLineAbsoluteWidth":F
    move/from16 v20, v14

    .line 1376
    .local v20, "tempLineRelativeWidth":F
    if-eqz v18, :cond_4

    .line 1377
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v10

    .line 1379
    .local v10, "length":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    if-lt v8, v10, :cond_8

    .line 1408
    .end local v8    # "i":I
    .end local v10    # "length":I
    :cond_4
    :goto_3
    float-to-double v0, v12

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v12, v0

    .line 1410
    goto/16 :goto_0

    .line 1353
    .end local v11    # "lineAbsoluteWidth":F
    .end local v12    # "lineRelativeWidth":F
    .end local v19    # "tempLineAbsoluteWidth":F
    .end local v20    # "tempLineRelativeWidth":F
    :cond_5
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineCount(J)I

    move-result v21

    if-nez v21, :cond_7

    .line 1354
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v22

    .line 1355
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v23

    .line 1354
    invoke-virtual/range {v21 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v17

    .line 1356
    .local v17, "sInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v17, :cond_3

    .line 1357
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_6
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_3

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 1358
    .local v9, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    instance-of v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    move/from16 v22, v0

    if-eqz v22, :cond_6

    .line 1359
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    move/from16 v21, v0

    add-float v4, v4, v21

    .line 1360
    goto :goto_1

    .line 1365
    .end local v17    # "sInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    :cond_7
    const/high16 v21, 0x40a00000    # 5.0f

    add-float v4, v4, v21

    goto :goto_1

    .line 1380
    .restart local v8    # "i":I
    .restart local v10    # "length":I
    .restart local v11    # "lineAbsoluteWidth":F
    .restart local v12    # "lineRelativeWidth":F
    .restart local v19    # "tempLineAbsoluteWidth":F
    .restart local v20    # "tempLineRelativeWidth":F
    :cond_8
    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v21

    const/16 v22, 0xa

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_b

    .line 1381
    move/from16 v19, v4

    .line 1382
    move/from16 v20, v14

    .line 1399
    :goto_4
    cmpl-float v21, v19, v11

    if-lez v21, :cond_9

    .line 1400
    move/from16 v11, v19

    .line 1402
    :cond_9
    cmpl-float v21, v20, v12

    if-lez v21, :cond_a

    .line 1403
    move/from16 v12, v20

    .line 1379
    :cond_a
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    .line 1384
    :cond_b
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2, v8, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 1385
    iget v0, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v21, v0

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v22

    mul-float v16, v21, v22

    .line 1386
    iget v0, v5, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    add-float v21, v21, v19

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v22

    add-float v21, v21, v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    move/from16 v22, v0

    .line 1387
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    sub-float v22, v22, v23

    const/high16 v23, 0x40800000    # 4.0f

    sub-float v22, v22, v23

    cmpg-float v21, v21, v22

    if-gez v21, :cond_c

    .line 1388
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v21

    add-float v19, v19, v21

    .line 1389
    add-float v20, v20, v16

    .line 1390
    goto :goto_4

    .line 1391
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    sub-float v21, v21, v22

    const/high16 v22, 0x40800000    # 4.0f

    sub-float v21, v21, v22

    .line 1392
    iget v0, v5, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    .line 1391
    sub-float v11, v21, v22

    .line 1393
    iget v0, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v21, v0

    mul-float v12, v21, v11

    .line 1395
    goto/16 :goto_3
.end method

.method public getObjectText()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    .locals 1

    .prologue
    .line 1055
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    return-object v0
.end method

.method public getPixel(II)I
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v5, 0x0

    .line 1306
    const/4 v4, 0x0

    .line 1307
    .local v4, "res":I
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_1

    .line 1308
    int-to-float v6, p1

    int-to-float v7, p2

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getAbsolutePoint(FF)[F

    move-result-object v3

    .line 1309
    .local v3, "pts":[F
    if-nez v3, :cond_0

    .line 1322
    .end local v3    # "pts":[F
    :goto_0
    return v5

    .line 1314
    .restart local v3    # "pts":[F
    :cond_0
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v0

    .line 1315
    .local v0, "deltaY":F
    aget v5, v3, v5

    float-to-int v1, v5

    .line 1316
    .local v1, "nX":I
    const/4 v5, 0x1

    aget v5, v3, v5

    sub-float/2addr v5, v0

    float-to-int v2, v5

    .line 1318
    .local v2, "nY":I
    if-ltz v1, :cond_1

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-ge v1, v5, :cond_1

    if-ltz v2, :cond_1

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 1319
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v4

    .end local v0    # "deltaY":F
    .end local v1    # "nX":I
    .end local v2    # "nY":I
    .end local v3    # "pts":[F
    :cond_1
    move v5, v4

    .line 1322
    goto :goto_0
.end method

.method public getText(Z)Ljava/lang/String;
    .locals 5
    .param p1, "option"    # Z

    .prologue
    const/4 v3, 0x0

    .line 937
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v4, :cond_2

    .line 938
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v4}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 939
    .local v1, "start":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v4}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 941
    .local v0, "end":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 943
    .local v2, "str":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 944
    if-eqz v2, :cond_1

    if-gt v1, v0, :cond_1

    .line 945
    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 953
    .end local v0    # "end":I
    .end local v1    # "start":I
    .end local v2    # "str":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .restart local v0    # "end":I
    .restart local v1    # "start":I
    .restart local v2    # "str":Ljava/lang/String;
    :cond_1
    move-object v2, v3

    .line 947
    goto :goto_0

    .end local v0    # "end":I
    .end local v1    # "start":I
    .end local v2    # "str":Ljava/lang/String;
    :cond_2
    move-object v2, v3

    .line 953
    goto :goto_0
.end method

.method public getTextLimit()I
    .locals 1

    .prologue
    .line 1302
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    return v0
.end method

.method public hideSoftInput()V
    .locals 3

    .prologue
    .line 1118
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1119
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1120
    return-void
.end method

.method public hideTextBox()V
    .locals 1

    .prologue
    .line 867
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mFirstDraw:Z

    .line 868
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    .line 869
    return-void
.end method

.method public isAccessoryKeyboardState()I
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1059
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v6, "input_method"

    invoke-virtual {v4, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 1060
    .local v2, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 1062
    .local v0, "cls":Ljava/lang/Class;
    :try_start_0
    const-string v4, "isAccessoryKeyboardState"

    const/4 v6, 0x0

    invoke-virtual {v0, v4, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v3

    .line 1064
    .local v3, "mtd":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    :try_start_1
    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v4

    .line 1076
    .end local v3    # "mtd":Ljava/lang/reflect/Method;
    :goto_0
    return v4

    .line 1065
    .restart local v3    # "mtd":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 1066
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    .end local v3    # "mtd":Ljava/lang/reflect/Method;
    :goto_1
    move v4, v5

    .line 1076
    goto :goto_0

    .line 1067
    .restart local v3    # "mtd":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v1

    .line 1068
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 1072
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    .end local v3    # "mtd":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v1

    .line 1073
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 1069
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v3    # "mtd":Ljava/lang/reflect/Method;
    :catch_3
    move-exception v1

    .line 1070
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1
.end method

.method public isContextMenuShowing()Z
    .locals 1

    .prologue
    .line 1167
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v0, :cond_0

    .line 1168
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v0

    .line 1171
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isContextMenuVisible()Z
    .locals 1

    .prologue
    .line 1007
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    return v0
.end method

.method protected isSelectByKey()Z
    .locals 1

    .prologue
    .line 1772
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    return v0
.end method

.method public isTextInputable()Z
    .locals 1

    .prologue
    .line 1768
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v0

    return v0
.end method

.method public isTouchEnabled()Z
    .locals 1

    .prologue
    .line 5863
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTouchEnable:Z

    return v0
.end method

.method public isViewModeEnabled()Z
    .locals 1

    .prologue
    .line 1163
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    return v0
.end method

.method public measureText()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 736
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_1

    .line 860
    :cond_0
    :goto_0
    return-void

    .line 741
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v8, :cond_0

    .line 745
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    move-result v10

    float-to-int v10, v10

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_measure(JI)Z

    .line 747
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getGravity()I

    move-result v8

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDeltaY(I)F

    move-result v2

    .line 749
    .local v2, "diffY":F
    const/4 v7, 0x0

    .line 751
    .local v7, "stringLength":I
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v6

    .line 752
    .local v6, "str":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 753
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    .line 756
    :cond_2
    if-lez v7, :cond_8

    .line 757
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineCount(J)I

    move-result v8

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    .line 758
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    new-array v8, v8, [Landroid/graphics/PointF;

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    .line 759
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    .line 760
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    .line 761
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineL2R:[Z

    .line 762
    new-array v8, v7, [Landroid/graphics/RectF;

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    .line 763
    new-array v8, v7, [F

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextSize:[F

    .line 764
    new-array v8, v7, [Z

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextItalic:[Z

    .line 766
    const/4 v1, 0x0

    .line 767
    .local v1, "cnt":I
    const/4 v1, 0x0

    :goto_1
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lt v1, v8, :cond_3

    .line 808
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineStartIndex(JI)I

    move-result v8

    const/4 v9, -0x1

    if-ne v8, v9, :cond_7

    .line 809
    iput-boolean v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFirstCharLF:Z

    .line 814
    :goto_2
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v8

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeltaY:F

    .line 816
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v7, :cond_0

    .line 817
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    new-instance v9, Landroid/graphics/RectF;

    invoke-direct {v9}, Landroid/graphics/RectF;-><init>()V

    aput-object v9, v8, v1

    .line 818
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v10, v10, v1

    invoke-direct {p0, v8, v9, v1, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 819
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v8, v8, v1

    invoke-virtual {v8, v14, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 820
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextItalic:[Z

    aput-boolean v12, v8, v1

    .line 816
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 768
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    new-instance v9, Landroid/graphics/PointF;

    invoke-direct {v9}, Landroid/graphics/PointF;-><init>()V

    aput-object v9, v8, v1

    .line 769
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    aget-object v10, v10, v1

    invoke-direct {p0, v8, v9, v1, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLinePosition(JILandroid/graphics/PointF;)Z

    .line 770
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    aget-object v8, v8, v1

    invoke-virtual {v8, v14, v2}, Landroid/graphics/PointF;->offset(FF)V

    .line 771
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v10, v11, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineStartIndex(JI)I

    move-result v9

    aput v9, v8, v1

    .line 772
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v10, v11, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineEndIndex(JI)I

    move-result v9

    aput v9, v8, v1

    .line 775
    const/4 v5, 0x0

    .line 776
    .local v5, "stop":Z
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineL2R:[Z

    aput-boolean v13, v8, v1

    .line 777
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v8, v8, v1

    if-ltz v8, :cond_5

    .line 778
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v4, v8, v1

    .line 779
    .local v4, "index":I
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v8, v8, v1

    if-le v4, v8, :cond_6

    .line 767
    .end local v4    # "index":I
    :cond_5
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 780
    .restart local v4    # "index":I
    :cond_6
    invoke-virtual {v6, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 781
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v3

    .line 782
    .local v3, "direction":I
    sparse-switch v3, :sswitch_data_0

    .line 797
    add-int/lit8 v4, v4, 0x1

    .line 800
    :goto_5
    if-eqz v5, :cond_4

    goto :goto_4

    .line 786
    :sswitch_0
    const/4 v5, 0x1

    .line 787
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineL2R:[Z

    aput-boolean v13, v8, v1

    goto :goto_5

    .line 793
    :sswitch_1
    const/4 v5, 0x1

    .line 794
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineL2R:[Z

    aput-boolean v12, v8, v1

    goto :goto_5

    .line 811
    .end local v0    # "c":C
    .end local v3    # "direction":I
    .end local v4    # "index":I
    .end local v5    # "stop":Z
    :cond_7
    iput-boolean v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFirstCharLF:Z

    goto/16 :goto_2

    .line 856
    .end local v1    # "cnt":I
    :cond_8
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initMeasureInfo()V

    goto/16 :goto_0

    .line 782
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0xe -> :sswitch_0
        0xf -> :sswitch_0
        0x10 -> :sswitch_1
        0x11 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCheckIsTextEditor()Z
    .locals 1

    .prologue
    .line 2262
    const/4 v0, 0x1

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1988
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCurrentOrientation:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_1

    .line 1989
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 1991
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    .line 1993
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1994
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->reset()V

    .line 1997
    :cond_0
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCurrentOrientation:I

    .line 1999
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeleteTextPopup()V

    .line 2002
    :cond_1
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHardKeyboardConnected:Z

    .line 2003
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHardKeyboardHidden:I

    iget v1, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-eq v0, v1, :cond_2

    .line 2004
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHardKeyboardHidden:I

    .line 2005
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v0, v2, :cond_3

    .line 2007
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHardKeyboardConnected:Z

    .line 2008
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideSoftInput()V

    .line 2016
    :cond_2
    :goto_0
    invoke-super {p0, p1}, Landroid/view/View;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2017
    return-void

    .line 2009
    :cond_3
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 2011
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHardKeyboardConnected:Z

    .line 2012
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showSoftInput()V

    goto :goto_0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 4
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    .line 2267
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getIMEActionType()I

    move-result v0

    .line 2268
    .local v0, "imeOption":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTextInputType()I

    move-result v1

    .line 2270
    .local v1, "inputType":I
    packed-switch v0, :pswitch_data_0

    .line 2293
    const/4 v0, 0x1

    .line 2297
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2314
    const/4 v1, 0x1

    .line 2318
    :goto_1
    const/4 v2, 0x0

    iput-object v2, p1, Landroid/view/inputmethod/EditorInfo;->actionLabel:Ljava/lang/CharSequence;

    .line 2319
    const-string v2, "SPenSDK"

    iput-object v2, p1, Landroid/view/inputmethod/EditorInfo;->label:Ljava/lang/CharSequence;

    .line 2320
    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v3, 0x10000000

    or-int/2addr v3, v0

    or-int/2addr v2, v3

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 2323
    or-int/lit16 v2, v1, 0x4000

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 2325
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputConnection:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

    if-nez v2, :cond_0

    .line 2326
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

    invoke-direct {v2, p0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputConnection:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

    .line 2329
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    .line 2330
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    .line 2332
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputConnection:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

    return-object v2

    .line 2272
    :pswitch_0
    const/4 v0, 0x6

    .line 2273
    goto :goto_0

    .line 2275
    :pswitch_1
    const/4 v0, 0x2

    .line 2276
    goto :goto_0

    .line 2278
    :pswitch_2
    const/4 v0, 0x5

    .line 2279
    goto :goto_0

    .line 2281
    :pswitch_3
    const/4 v0, 0x1

    .line 2282
    goto :goto_0

    .line 2284
    :pswitch_4
    const/4 v0, 0x3

    .line 2285
    goto :goto_0

    .line 2287
    :pswitch_5
    const/4 v0, 0x4

    .line 2288
    goto :goto_0

    .line 2290
    :pswitch_6
    const/4 v0, 0x0

    .line 2291
    goto :goto_0

    .line 2299
    :pswitch_7
    const/4 v1, 0x4

    .line 2300
    goto :goto_1

    .line 2302
    :pswitch_8
    const/4 v1, 0x0

    .line 2303
    goto :goto_1

    .line 2305
    :pswitch_9
    const/4 v1, 0x2

    .line 2306
    goto :goto_1

    .line 2308
    :pswitch_a
    const/4 v1, 0x3

    .line 2309
    goto :goto_1

    .line 2311
    :pswitch_b
    const/4 v1, 0x1

    .line 2312
    goto :goto_1

    .line 2270
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_2
    .end packed-switch

    .line 2297
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_b
        :pswitch_9
        :pswitch_a
        :pswitch_7
    .end packed-switch
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 1907
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->stopBlink()V

    .line 1909
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->stopInput()V

    .line 1911
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1912
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 1881
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mFirstDraw:Z

    if-eqz v0, :cond_0

    .line 1882
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mFirstDraw:Z

    .line 1883
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onVisibleUpdated(Z)V

    .line 1886
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 1903
    :cond_1
    :goto_0
    return-void

    .line 1890
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawSelect(Landroid/graphics/Canvas;)V

    .line 1892
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1894
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawHandle()V

    .line 1896
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawCursor(Landroid/graphics/Canvas;)V

    .line 1898
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawScrollBar(Landroid/graphics/Canvas;)V

    .line 1900
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    .line 1902
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 7
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    const/16 v2, 0x8

    const/4 v6, 0x0

    .line 1916
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    .line 1917
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onFocusChanged(Z)V

    .line 1920
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mShowCursor:J

    .line 1921
    if-eqz p1, :cond_2

    .line 1922
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->startBlink()V

    .line 1924
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v0, :cond_1

    .line 1925
    const/4 v4, 0x0

    .line 1926
    .local v4, "textOnCanvasCommand":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1927
    .local v5, "tempObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1928
    const-wide/16 v2, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 1942
    .end local v4    # "textOnCanvasCommand":I
    .end local v5    # "tempObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_1
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 1943
    return-void

    .line 1932
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->stopBlink()V

    .line 1933
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->stopInput()V

    .line 1935
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V

    .line 1936
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v0, v0, v6

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1937
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1939
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeleteTextPopup()V

    goto :goto_0
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 6091
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_0

    .line 6092
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 6102
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/view/View;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    return v2

    .line 6094
    :pswitch_0
    const/16 v2, 0x9

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    .line 6095
    .local v0, "vscroll":F
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_0

    .line 6096
    const/high16 v2, 0x41200000    # 10.0f

    mul-float/2addr v2, v0

    neg-float v1, v2

    .line 6097
    .local v1, "y":F
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->scrollTextbox(F)V

    goto :goto_0

    .line 6092
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 11
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v10, 0x0

    const/4 v9, -0x1

    const/4 v8, 0x1

    .line 2029
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v7}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v5

    .line 2030
    .local v5, "start":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v7}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 2032
    .local v1, "end":I
    sparse-switch p1, :sswitch_data_0

    .line 2151
    :goto_0
    const/16 v7, 0x43

    if-ne p1, v7, :cond_15

    .line 2152
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 2153
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 2154
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    if-nez v7, :cond_0

    .line 2155
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v7}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v7

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 2156
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    .line 2157
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    .line 2163
    :cond_0
    :goto_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    if-eqz v7, :cond_16

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v7, :cond_16

    .line 2164
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v7, p0, v9, p1, p2}, Landroid/text/method/KeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v7

    if-eqz v7, :cond_16

    move v7, v8

    .line 2169
    :goto_2
    return v7

    .line 2038
    :sswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v7

    if-nez v7, :cond_1

    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    if-eqz v7, :cond_2

    :cond_1
    const/16 v7, 0x7a

    if-ne p1, v7, :cond_2

    .line 2039
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->restartBlink()V

    .line 2040
    const/16 v7, 0x5c

    invoke-direct {p0, v7, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onMoveByKey(ILandroid/view/KeyEvent;)V

    move v7, v8

    .line 2041
    goto :goto_2

    .line 2044
    :cond_2
    :sswitch_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v7

    if-nez v7, :cond_3

    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    if-eqz v7, :cond_4

    :cond_3
    const/16 v7, 0x7b

    if-ne p1, v7, :cond_4

    .line 2045
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->restartBlink()V

    .line 2046
    const/16 v7, 0x5d

    invoke-direct {p0, v7, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onMoveByKey(ILandroid/view/KeyEvent;)V

    move v7, v8

    .line 2047
    goto :goto_2

    .line 2051
    :cond_4
    :sswitch_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->restartBlink()V

    .line 2052
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onMoveByKey(ILandroid/view/KeyEvent;)V

    move v7, v8

    .line 2053
    goto :goto_2

    .line 2058
    :sswitch_3
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    goto :goto_0

    .line 2063
    :sswitch_4
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 2064
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 2065
    if-ne v5, v1, :cond_10

    .line 2066
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v7

    if-nez v7, :cond_5

    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    if-eqz v7, :cond_f

    .line 2067
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorPos()I

    move-result v4

    .local v4, "pos":I
    const/4 v2, 0x0

    .line 2068
    .local v2, "line":I
    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 2069
    .local v0, "cursorRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_a

    .line 2070
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v7

    int-to-float v7, v7

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v2

    .line 2071
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v7, :cond_a

    .line 2072
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v7, v7, v2

    if-ltz v7, :cond_6

    .line 2073
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, v2

    aget-object v7, v7, v9

    iget v7, v7, Landroid/graphics/RectF;->left:F

    invoke-direct {p0, v2, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v5

    .line 2075
    :cond_6
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v7, :cond_a

    .line 2076
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v7, v7, v2

    if-ltz v7, :cond_7

    .line 2077
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v9, v9, v2

    aget-object v7, v7, v9

    iget v7, v7, Landroid/graphics/RectF;->right:F

    invoke-direct {p0, v2, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v1

    .line 2080
    :cond_7
    add-int/lit8 v7, v2, 0x1

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-ge v7, v9, :cond_9

    .line 2081
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v6

    .line 2082
    .local v6, "str":Ljava/lang/String;
    if-eqz v6, :cond_9

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v1, v7, :cond_9

    .line 2083
    invoke-virtual {v6, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 2084
    .local v3, "oneChar":C
    const/16 v7, 0xa

    if-eq v3, v7, :cond_8

    const/16 v7, 0xd

    if-ne v3, v7, :cond_9

    .line 2085
    :cond_8
    add-int/lit8 v1, v1, 0x1

    .line 2090
    .end local v3    # "oneChar":C
    .end local v6    # "str":Ljava/lang/String;
    :cond_9
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 2094
    :cond_a
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    if-nez v7, :cond_b

    .line 2095
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    .line 2096
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    .line 2098
    :cond_b
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v7, :cond_c

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v7}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-le v1, v7, :cond_c

    .line 2099
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v7}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    .line 2101
    :cond_c
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v7, :cond_d

    .line 2102
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v7, v5, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 2104
    :cond_d
    invoke-virtual {p0, v5, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .end local v0    # "cursorRect":Landroid/graphics/Rect;
    .end local v2    # "line":I
    .end local v4    # "pos":I
    :cond_e
    :goto_3
    move v7, v8

    .line 2118
    goto/16 :goto_2

    .line 2105
    :cond_f
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I

    move-result v7

    if-ge v5, v7, :cond_e

    .line 2106
    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 2107
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v7, :cond_e

    .line 2108
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    add-int/lit8 v9, v5, 0x1

    invoke-interface {v7, v5, v9}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_3

    .line 2112
    :cond_10
    if-ge v5, v1, :cond_11

    move v7, v5

    :goto_4
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 2113
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v7, :cond_e

    .line 2114
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-ge v5, v1, :cond_12

    move v9, v5

    :goto_5
    if-ge v5, v1, :cond_13

    move v7, v1

    :goto_6
    invoke-interface {v10, v9, v7}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_3

    :cond_11
    move v7, v1

    .line 2112
    goto :goto_4

    :cond_12
    move v9, v1

    .line 2114
    goto :goto_5

    :cond_13
    move v7, v5

    goto :goto_6

    .line 2130
    :sswitch_5
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isNumLockOn()Z

    move-result v7

    if-eqz v7, :cond_14

    .line 2131
    add-int/lit16 v7, p1, -0x90

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->appendText(Ljava/lang/String;)V

    :goto_7
    move v7, v8

    .line 2135
    goto/16 :goto_2

    .line 2133
    :cond_14
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onMoveByKey(ILandroid/view/KeyEvent;)V

    goto :goto_7

    .line 2139
    :sswitch_6
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    goto/16 :goto_0

    .line 2143
    :sswitch_7
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    goto/16 :goto_0

    .line 2147
    :sswitch_8
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    goto/16 :goto_0

    .line 2160
    :cond_15
    iput-boolean v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    goto/16 :goto_1

    .line 2169
    :cond_16
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    goto/16 :goto_2

    .line 2032
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x39 -> :sswitch_6
        0x3a -> :sswitch_6
        0x3b -> :sswitch_8
        0x3c -> :sswitch_8
        0x3d -> :sswitch_3
        0x3e -> :sswitch_3
        0x42 -> :sswitch_3
        0x5c -> :sswitch_2
        0x5d -> :sswitch_2
        0x70 -> :sswitch_4
        0x71 -> :sswitch_7
        0x72 -> :sswitch_7
        0x7a -> :sswitch_0
        0x7b -> :sswitch_1
        0x90 -> :sswitch_5
        0x91 -> :sswitch_5
        0x92 -> :sswitch_5
        0x93 -> :sswitch_5
        0x94 -> :sswitch_5
        0x95 -> :sswitch_5
        0x96 -> :sswitch_5
        0x97 -> :sswitch_5
        0x98 -> :sswitch_5
        0x99 -> :sswitch_5
    .end sparse-switch
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x0

    .line 2220
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v3

    .line 2222
    .local v3, "state":I
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v6

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    if-nez v6, :cond_0

    or-int/lit16 v6, v3, 0x1000

    if-eqz v6, :cond_1

    .line 2223
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "clipboard"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 2224
    .local v0, "clipboard":Landroid/content/ClipboardManager;
    if-nez v0, :cond_2

    .line 2257
    .end local v0    # "clipboard":Landroid/content/ClipboardManager;
    :cond_1
    :goto_0
    return v5

    .line 2228
    .restart local v0    # "clipboard":Landroid/content/ClipboardManager;
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v6}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    .line 2229
    .local v2, "start":I
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v6}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 2230
    .local v1, "end":I
    if-le v2, v1, :cond_3

    .line 2231
    move v4, v2

    .line 2232
    .local v4, "temp":I
    move v2, v1

    .line 2233
    move v1, v4

    .line 2236
    .end local v4    # "temp":I
    :cond_3
    const/16 v6, 0x36

    if-eq p1, v6, :cond_4

    .line 2237
    const/4 v6, -0x1

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 2239
    :cond_4
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 2241
    :sswitch_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->selectAll()Z

    .line 2242
    const/4 v5, 0x1

    goto :goto_0

    .line 2244
    :sswitch_1
    invoke-direct {p0, v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->copy(Landroid/content/ClipboardManager;II)Z

    move-result v5

    goto :goto_0

    .line 2246
    :sswitch_2
    invoke-direct {p0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->delete(II)Z

    move-result v5

    goto :goto_0

    .line 2248
    :sswitch_3
    invoke-direct {p0, v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->cut(Landroid/content/ClipboardManager;II)Z

    move-result v5

    goto :goto_0

    .line 2250
    :sswitch_4
    invoke-direct {p0, v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->paste(Landroid/content/ClipboardManager;II)Z

    move-result v5

    goto :goto_0

    .line 2252
    :sswitch_5
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->undo()Z

    move-result v5

    goto :goto_0

    .line 2254
    :sswitch_6
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->redo()Z

    move-result v5

    goto :goto_0

    .line 2239
    nop

    :sswitch_data_0
    .sparse-switch
        0x1d -> :sswitch_0
        0x1f -> :sswitch_1
        0x20 -> :sswitch_2
        0x32 -> :sswitch_4
        0x34 -> :sswitch_3
        0x35 -> :sswitch_6
        0x36 -> :sswitch_5
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 2175
    sparse-switch p1, :sswitch_data_0

    .line 2208
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    if-eqz v2, :cond_1

    .line 2209
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2, p0, v3, p1, p2}, Landroid/text/method/KeyListener;->onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2210
    const/4 v2, 0x1

    .line 2214
    :goto_1
    :sswitch_0
    return v2

    .line 2186
    :sswitch_1
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    goto :goto_0

    .line 2190
    :sswitch_2
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    goto :goto_0

    .line 2194
    :sswitch_3
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    .line 2195
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSwipeOnKeyboard:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHardKeyboardConnected:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v3, :cond_0

    .line 2196
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2197
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V

    .line 2198
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    .line 2199
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 2200
    .local v1, "start":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 2201
    .local v0, "end":I
    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    .line 2202
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    .line 2203
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSwipeOnKeyboard:Z

    goto :goto_0

    .line 2214
    .end local v0    # "end":I
    .end local v1    # "start":I
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_1

    .line 2175
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x39 -> :sswitch_1
        0x3a -> :sswitch_1
        0x3b -> :sswitch_3
        0x3c -> :sswitch_3
        0x3d -> :sswitch_0
        0x42 -> :sswitch_0
        0x43 -> :sswitch_0
        0x71 -> :sswitch_2
        0x72 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onObjectChanged()V
    .locals 2

    .prologue
    .line 1946
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    .line 1947
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onObjectChanged(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 1949
    :cond_0
    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 2349
    invoke-super {p0, p1}, Landroid/view/View;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 2351
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2352
    return-void
.end method

.method protected onRequestScroll(FF)V
    .locals 2
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 1970
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    .line 1971
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onRequestScroll(FF)V

    .line 1972
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onObjectChanged(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 1984
    :cond_0
    return-void
.end method

.method protected onSelectionChanged(II)V
    .locals 1
    .param p1, "selStart"    # I
    .param p2, "selEnd"    # I

    .prologue
    .line 3033
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-nez v0, :cond_1

    .line 3042
    :cond_0
    :goto_0
    return-void

    .line 3037
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSelectionChanged(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3041
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 22
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 6196
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTouchEnable:Z

    move/from16 v18, v0

    if-nez v18, :cond_0

    .line 6197
    const/16 v18, 0x0

    .line 6516
    :goto_0
    return v18

    .line 6200
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeleteTextPopup()V

    .line 6204
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    if-nez v18, :cond_2

    .line 6205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    .line 6206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 6209
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    move/from16 v18, v0

    if-eqz v18, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v18

    if-nez v18, :cond_2

    .line 6210
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->bringToFront()V

    .line 6211
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->requestFocus()Z

    .line 6214
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 6215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    if-nez v18, :cond_3

    .line 6216
    const/16 v18, 0x0

    goto :goto_0

    .line 6218
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    .line 6219
    .local v10, "objectRect":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v4

    .line 6220
    .local v4, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v4, :cond_4

    .line 6222
    const/16 v18, 0x0

    goto :goto_0

    .line 6224
    :cond_4
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    .line 6225
    .local v13, "relativeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v10, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6228
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v11, v0, [F

    const/16 v18, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v19

    aput v19, v11, v18

    const/16 v18, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    aput v19, v11, v18

    .line 6229
    .local v11, "pts":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 6230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 6231
    const/16 v18, 0x1

    aget v19, v11, v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v20

    add-float v19, v19, v20

    aput v19, v11, v18

    .line 6233
    const/4 v8, 0x0

    .local v8, "line":I
    const/4 v7, 0x0

    .local v7, "index":I
    const/4 v15, 0x0

    .line 6234
    .local v15, "start":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    packed-switch v18, :pswitch_data_0

    .line 6271
    :cond_5
    :goto_1
    if-ne v7, v15, :cond_6

    .line 6272
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 6273
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V

    .line 6274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    .line 6275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->reset()V

    .line 6276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 6279
    :cond_6
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 6236
    :pswitch_0
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6237
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6238
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_1

    .line 6241
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v15

    .line 6242
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6243
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6244
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v0, v7, :cond_5

    .line 6245
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v15, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 6246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 6247
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkForVerticalScroll(I)F

    .line 6248
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    .line 6250
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHapticEnabled()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 6251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-object/from16 v18, v0

    const/16 v19, 0x16

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->performHapticFeedback(I)Z

    goto/16 :goto_1

    .line 6256
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v15

    .line 6257
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6258
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6259
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v0, v7, :cond_7

    .line 6260
    invoke-static {v15, v7}, Ljava/lang/Math;->min(II)I

    move-result v18

    invoke-static {v15, v7}, Ljava/lang/Math;->max(II)I

    move-result v19

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 6261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 6262
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkForVerticalScroll(I)F

    .line 6263
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    goto/16 :goto_1

    .line 6264
    :cond_7
    if-ge v7, v15, :cond_5

    .line 6265
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v15, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 6266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 6267
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkForVerticalScroll(I)F

    goto/16 :goto_1

    .line 6280
    .end local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .end local v7    # "index":I
    .end local v8    # "line":I
    .end local v10    # "objectRect":Landroid/graphics/RectF;
    .end local v11    # "pts":[F
    .end local v13    # "relativeRect":Landroid/graphics/RectF;
    .end local v15    # "start":I
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    move/from16 v18, v0

    if-eqz v18, :cond_b

    .line 6281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    if-nez v18, :cond_9

    .line 6282
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 6284
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    .line 6286
    .restart local v10    # "objectRect":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v4

    .line 6287
    .restart local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v4, :cond_a

    .line 6289
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 6292
    :cond_a
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    .line 6293
    .restart local v13    # "relativeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v10, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6295
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v11, v0, [F

    const/16 v18, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v19

    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    aput v19, v11, v18

    const/16 v18, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    aput v19, v11, v18

    .line 6296
    .restart local v11    # "pts":[F
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getInvMatrix()Landroid/graphics/Matrix;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 6298
    const/4 v8, 0x0

    .restart local v8    # "line":I
    const/4 v7, 0x0

    .restart local v7    # "index":I
    const/4 v15, 0x0

    .line 6300
    .restart local v15    # "start":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    packed-switch v18, :pswitch_data_1

    .line 6326
    :goto_2
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 6302
    :pswitch_3
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6303
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6304
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_2

    .line 6308
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v15

    .line 6309
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6310
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6312
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v15, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_2

    .line 6316
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v15

    .line 6317
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6318
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6320
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v15, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 6322
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    goto :goto_2

    .line 6327
    .end local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .end local v7    # "index":I
    .end local v8    # "line":I
    .end local v10    # "objectRect":Landroid/graphics/RectF;
    .end local v11    # "pts":[F
    .end local v13    # "relativeRect":Landroid/graphics/RectF;
    .end local v15    # "start":I
    :cond_b
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v18

    const/16 v19, 0x3

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_10

    .line 6328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    if-nez v18, :cond_c

    .line 6329
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 6331
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    .line 6333
    .restart local v10    # "objectRect":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v4

    .line 6334
    .restart local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v4, :cond_d

    .line 6336
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 6339
    :cond_d
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    .line 6340
    .restart local v13    # "relativeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v10, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6343
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v11, v0, [F

    const/16 v18, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v19

    aput v19, v11, v18

    const/16 v18, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    aput v19, v11, v18

    .line 6344
    .restart local v11    # "pts":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 6345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 6346
    const/16 v18, 0x1

    aget v19, v11, v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v20

    add-float v19, v19, v20

    aput v19, v11, v18

    .line 6348
    const/4 v8, 0x0

    .restart local v8    # "line":I
    const/4 v7, 0x0

    .restart local v7    # "index":I
    const/4 v15, 0x0

    .line 6350
    .restart local v15    # "start":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    packed-switch v18, :pswitch_data_2

    .line 6516
    .end local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .end local v7    # "index":I
    .end local v8    # "line":I
    .end local v10    # "objectRect":Landroid/graphics/RectF;
    .end local v11    # "pts":[F
    .end local v13    # "relativeRect":Landroid/graphics/RectF;
    .end local v15    # "start":I
    :cond_e
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v18

    goto/16 :goto_0

    .line 6352
    .restart local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .restart local v7    # "index":I
    .restart local v8    # "line":I
    .restart local v10    # "objectRect":Landroid/graphics/RectF;
    .restart local v11    # "pts":[F
    .restart local v13    # "relativeRect":Landroid/graphics/RectF;
    .restart local v15    # "start":I
    :pswitch_6
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6353
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6354
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_3

    .line 6358
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v15

    .line 6359
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6360
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6361
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v0, v7, :cond_e

    .line 6362
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v15, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 6363
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    .line 6365
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHapticEnabled()Z

    move-result v18

    if-eqz v18, :cond_e

    .line 6366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-object/from16 v18, v0

    const/16 v19, 0x16

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->performHapticFeedback(I)Z

    goto :goto_3

    .line 6372
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v15

    .line 6373
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6374
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6375
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v0, v7, :cond_f

    .line 6376
    invoke-static {v15, v7}, Ljava/lang/Math;->min(II)I

    move-result v18

    invoke-static {v15, v7}, Ljava/lang/Math;->max(II)I

    move-result v19

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 6377
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    goto/16 :goto_3

    .line 6378
    :cond_f
    if-ge v7, v15, :cond_e

    .line 6379
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v15, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 6383
    .end local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .end local v7    # "index":I
    .end local v8    # "line":I
    .end local v10    # "objectRect":Landroid/graphics/RectF;
    .end local v11    # "pts":[F
    .end local v13    # "relativeRect":Landroid/graphics/RectF;
    .end local v15    # "start":I
    :cond_10
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    move/from16 v18, v0

    if-nez v18, :cond_e

    .line 6384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    if-nez v18, :cond_11

    .line 6385
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 6388
    :cond_11
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    packed-switch v18, :pswitch_data_3

    goto/16 :goto_3

    .line 6390
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    .line 6391
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    .line 6393
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_e

    .line 6396
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mActionDownTime:J

    .line 6398
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->charAt(I)C

    move-result v18

    const/16 v19, 0x20

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_12

    .line 6399
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    .line 6402
    :cond_12
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSelectionRect(II)Landroid/graphics/RectF;

    move-result-object v12

    .line 6403
    .local v12, "rect":Landroid/graphics/RectF;
    if-eqz v12, :cond_e

    .line 6407
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    .line 6408
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    .line 6409
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v12, v0, v1}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v18

    if-eqz v18, :cond_e

    .line 6410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v20, v0

    invoke-interface/range {v18 .. v20}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    .line 6411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v20, v0

    invoke-virtual/range {v18 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    .line 6417
    .end local v12    # "rect":Landroid/graphics/RectF;
    :pswitch_a
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mActionDownTime:J

    move-wide/from16 v20, v0

    sub-long v16, v18, v20

    .line 6418
    .local v16, "time":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_13

    const-wide/16 v18, 0x258

    cmp-long v18, v16, v18

    if-ltz v18, :cond_13

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mActionDownTime:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-lez v18, :cond_13

    .line 6419
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    .line 6421
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    move/from16 v18, v0

    if-eqz v18, :cond_e

    .line 6424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    if-eqz v18, :cond_e

    .line 6431
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v5

    .line 6432
    .local v5, "deltaY":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v18

    add-float v18, v18, v5

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6433
    .restart local v8    # "line":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6435
    .restart local v7    # "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Landroid/text/Editable;->length()I

    move-result v18

    move/from16 v0, v18

    if-lt v0, v7, :cond_e

    .line 6438
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    .line 6439
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 6440
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v7}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 6442
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 6443
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V

    .line 6445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    if-eqz v18, :cond_14

    .line 6446
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 6449
    :cond_14
    const/16 v18, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showDragText(ZII)V

    .line 6451
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    .line 6453
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 6456
    .end local v5    # "deltaY":F
    .end local v7    # "index":I
    .end local v8    # "line":I
    .end local v16    # "time":J
    :pswitch_b
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mActionDownTime:J

    .line 6458
    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showDragText(ZII)V

    .line 6460
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    move/from16 v18, v0

    if-eqz v18, :cond_e

    .line 6464
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-gt v0, v1, :cond_15

    .line 6465
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    goto/16 :goto_3

    .line 6469
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->appendText(Ljava/lang/String;)V

    .line 6471
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    .line 6472
    .local v9, "newPos":I
    const/4 v14, 0x0

    .line 6473
    .local v14, "spanOffset":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_18

    .line 6474
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    add-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static/range {v18 .. v20}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 6475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    add-int v9, v9, v18

    .line 6476
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    sub-int v14, v18, v19

    .line 6482
    :cond_16
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    .line 6484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    if-eqz v18, :cond_17

    .line 6485
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-lt v6, v0, :cond_19

    .line 6498
    .end local v6    # "i":I
    :cond_17
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v9, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 6500
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    .line 6501
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    .line 6503
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 6478
    :cond_18
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_16

    .line 6479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v20, v0

    invoke-static/range {v18 .. v20}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 6480
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v19, v0

    sub-int v14, v18, v19

    goto :goto_4

    .line 6486
    .restart local v6    # "i":I
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v19, v0

    add-int v19, v19, v14

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 6487
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    sub-int v19, v9, v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_1a

    .line 6488
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    sub-int v19, v9, v19

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 6490
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v19, v0

    add-int v19, v19, v14

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 6491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v0, v9, :cond_1b

    .line 6492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, v18

    iput v9, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 6494
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    .line 6485
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_5

    .line 6506
    .end local v6    # "i":I
    .end local v9    # "newPos":I
    .end local v14    # "spanOffset":I
    :pswitch_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    move/from16 v18, v0

    if-eqz v18, :cond_e

    .line 6507
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mActionDownTime:J

    .line 6508
    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showDragText(ZII)V

    .line 6509
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    .line 6510
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    .line 6511
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 6234
    nop

    :pswitch_data_0
    .packed-switch 0xd3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 6300
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch

    .line 6350
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_8
        :pswitch_7
    .end packed-switch

    .line 6388
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_b
        :pswitch_a
        :pswitch_c
    .end packed-switch
.end method

.method protected onVisibleUpdated(Z)V
    .locals 2
    .param p1, "bVisible"    # Z

    .prologue
    .line 1952
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    .line 1953
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V

    .line 1955
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 2337
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHasWindowFocus:Z

    .line 2339
    if-eqz p1, :cond_0

    .line 2340
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->startBlink()V

    .line 2344
    :goto_0
    invoke-super {p0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    .line 2345
    return-void

    .line 2342
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->stopBlink()V

    goto :goto_0
.end method

.method public removeText()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 893
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 894
    .local v1, "start":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 896
    .local v0, "end":I
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 898
    if-eq v1, v0, :cond_2

    .line 899
    if-ge v1, v0, :cond_1

    .line 900
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2, v1, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 901
    invoke-virtual {p0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 913
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 914
    return-void

    .line 903
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 904
    invoke-virtual {p0, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto :goto_0

    .line 908
    :cond_2
    if-lez v1, :cond_0

    .line 909
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    add-int/lit8 v3, v1, -0x1

    invoke-interface {v2, v3, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0
.end method

.method public scrollTextbox(F)V
    .locals 18
    .param p1, "y"    # F

    .prologue
    .line 6106
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_1

    .line 6192
    :cond_0
    :goto_0
    return-void

    .line 6109
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v9

    .line 6110
    .local v9, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    iget v3, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float p1, p1, v3

    .line 6112
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v3

    int-to-float v12, v3

    .line 6113
    .local v12, "minimumHeight":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v13

    .line 6114
    .local v13, "objectRect":Landroid/graphics/RectF;
    if-eqz v13, :cond_0

    .line 6118
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup;

    .line 6119
    .local v14, "parentLayout":Landroid/view/ViewGroup;
    if-eqz v14, :cond_0

    .line 6122
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 6123
    .local v15, "relativeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6125
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v10

    .line 6126
    .local v10, "deltaY":F
    const/4 v3, 0x0

    cmpl-float v3, p1, v3

    if-lez v3, :cond_9

    .line 6127
    add-float v10, v10, p1

    .line 6129
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float v3, v12, v3

    cmpg-float v3, v10, v3

    if-gez v3, :cond_5

    .line 6130
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setPan(JF)V

    .line 6172
    :cond_2
    :goto_1
    float-to-double v4, v12

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v16

    cmpl-double v3, v4, v16

    if-lez v3, :cond_0

    .line 6174
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    if-eqz v3, :cond_3

    .line 6175
    const/4 v6, 0x1

    .line 6176
    .local v6, "textOnCanvasCommand":I
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 6177
    .local v7, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6178
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 6180
    .end local v6    # "textOnCanvasCommand":I
    .end local v7    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_3
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_update(J)Z

    .line 6182
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_4

    .line 6183
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v8

    invoke-virtual {v3, v4, v5, v8}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 6186
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showScrollBar()V

    .line 6188
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 6190
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    goto/16 :goto_0

    .line 6132
    :cond_5
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    cmpg-float v3, v12, v3

    if-gez v3, :cond_7

    .line 6133
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setPan(JF)V

    .line 6135
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditable:Z

    if-eqz v3, :cond_2

    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v14}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_2

    .line 6136
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 6137
    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v14}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float v11, v3, v4

    .line 6138
    .local v11, "maxScroll":F
    cmpg-float v3, p1, v11

    if-gez v3, :cond_6

    move/from16 v11, p1

    .end local v11    # "maxScroll":F
    :cond_6
    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    .line 6139
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCanvasScroll:Z

    .line 6140
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onRequestScroll(FF)V

    goto/16 :goto_1

    .line 6143
    :cond_7
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float v3, v12, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setPan(JF)V

    .line 6145
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6146
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditable:Z

    if-eqz v3, :cond_2

    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v14}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_2

    .line 6147
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 6148
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float v3, v12, v3

    sub-float v2, v10, v3

    .line 6149
    .local v2, "canvasScroll":F
    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v14}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float v11, v3, v4

    .line 6150
    .restart local v11    # "maxScroll":F
    cmpg-float v3, v2, v11

    if-gez v3, :cond_8

    .end local v2    # "canvasScroll":F
    :goto_2
    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    .line 6151
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCanvasScroll:Z

    .line 6152
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onRequestScroll(FF)V

    goto/16 :goto_1

    .restart local v2    # "canvasScroll":F
    :cond_8
    move v2, v11

    .line 6150
    goto :goto_2

    .line 6156
    .end local v2    # "canvasScroll":F
    .end local v11    # "maxScroll":F
    :cond_9
    add-float v10, v10, p1

    .line 6157
    const/4 v3, 0x0

    cmpg-float v3, v10, v3

    if-gez v3, :cond_b

    .line 6158
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditable:Z

    if-eqz v3, :cond_a

    iget-object v3, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_a

    .line 6159
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 6160
    iget-object v3, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    neg-float v11, v3

    .line 6161
    .restart local v11    # "maxScroll":F
    cmpl-float v3, v10, v11

    if-lez v3, :cond_c

    .end local v10    # "deltaY":F
    :goto_3
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    .line 6162
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCanvasScroll:Z

    .line 6163
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onRequestScroll(FF)V

    .line 6165
    .end local v11    # "maxScroll":F
    :cond_a
    const/4 v10, 0x0

    .line 6168
    .restart local v10    # "deltaY":F
    :cond_b
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setPan(JF)V

    goto/16 :goto_1

    .restart local v11    # "maxScroll":F
    :cond_c
    move v10, v11

    .line 6161
    goto :goto_3
.end method

.method protected seFitOnSizeChanged()V
    .locals 1

    .prologue
    .line 3199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFitOnSizeChanged:Z

    .line 3200
    return-void
.end method

.method public setCheckCursorOnScroll(Z)V
    .locals 0
    .param p1, "isCheck"    # Z

    .prologue
    .line 3479
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCheckCursorOnScroll:Z

    .line 3480
    return-void
.end method

.method public setContextMenu(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V
    .locals 1
    .param p1, "contextMenu"    # Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .prologue
    .line 997
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 999
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setInstance(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    .line 1000
    return-void
.end method

.method public setContextMenuVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 1003
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    .line 1004
    return-void
.end method

.method public setCursorPos(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1175
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 1176
    return-void
.end method

.method public setEraserMode(Z)V
    .locals 0
    .param p1, "option"    # Z

    .prologue
    .line 1034
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    .line 1035
    return-void
.end method

.method public setMargin(IIII)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 1494
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    .line 1495
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    .line 1496
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    .line 1497
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    .line 1498
    return-void
.end method

.method public setObjectText(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 4
    .param p1, "textObj"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 1039
    if-eqz p1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1052
    :cond_0
    :goto_0
    return-void

    .line 1044
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 1045
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setObjectText(JLcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z

    .line 1047
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getVerticalPan()F

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setPan(JF)V

    .line 1049
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initEditable()V

    .line 1051
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto :goto_0
.end method

.method public setParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V
    .locals 2
    .param p1, "paragraphInfo"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;

    .prologue
    .line 1011
    if-nez p1, :cond_0

    .line 1031
    .end local p1    # "paragraphInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    :goto_0
    return-void

    .line 1015
    .restart local p1    # "paragraphInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    :cond_0
    instance-of v0, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    if-eqz v0, :cond_2

    .line 1016
    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    .end local p1    # "paragraphInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    iget v0, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setParagraphAlign(I)V

    .line 1028
    :cond_1
    :goto_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 1030
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto :goto_0

    .line 1023
    .restart local p1    # "paragraphInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    :cond_2
    instance-of v0, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1024
    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    .line 1025
    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    .end local p1    # "paragraphInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    iget v1, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    .line 1024
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setParagraphSpacing(IF)V

    goto :goto_1
.end method

.method public setSelection(IIZ)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "isForce"    # Z

    .prologue
    .line 1228
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_1

    .line 1257
    :cond_0
    :goto_0
    return-void

    .line 1232
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lt v1, p2, :cond_0

    .line 1236
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1, p1, p2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 1238
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 1240
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V

    .line 1242
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v1, :cond_2

    .line 1243
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    .line 1245
    .local v0, "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 1247
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    .line 1250
    .end local v0    # "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    .line 1252
    if-eqz p3, :cond_3

    .line 1253
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 1256
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto :goto_0
.end method

.method public setSelection(IZ)V
    .locals 7
    .param p1, "pos"    # I
    .param p2, "isForce"    # Z

    .prologue
    const/16 v6, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1191
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    if-ge v4, p1, :cond_1

    .line 1224
    :cond_0
    :goto_0
    return-void

    .line 1195
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v4, p1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 1197
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v4, :cond_0

    .line 1201
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 1203
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V

    .line 1205
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v4, v4, v2

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1206
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1208
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v4, :cond_2

    .line 1209
    invoke-direct {p0, p1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v1

    .line 1211
    .local v1, "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v4, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 1214
    .end local v1    # "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_2
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-eqz v4, :cond_4

    :goto_1
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    .line 1216
    if-eqz p2, :cond_3

    .line 1217
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1218
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-lez v2, :cond_3

    iget v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    if-lez v2, :cond_3

    .line 1219
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 1223
    .end local v0    # "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto :goto_0

    :cond_4
    move v2, v3

    .line 1214
    goto :goto_1
.end method

.method public setSelectionAll()V
    .locals 4

    .prologue
    .line 1260
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_1

    .line 1274
    :cond_0
    :goto_0
    return-void

    .line 1264
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    .line 1265
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1268
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1272
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_0
.end method

.method protected setShowSoftInputEnable(Z)V
    .locals 0
    .param p1, "isShowSoftInputEnable"    # Z

    .prologue
    .line 1123
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsShowSoftInputEnable:Z

    .line 1124
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 917
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v1, :cond_0

    .line 918
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSetEmptyText:Z

    .line 921
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    const/16 v1, 0x1388

    if-ne v0, v1, :cond_2

    if-eqz p1, :cond_2

    .line 922
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-interface {v0, v3, v1, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 931
    :cond_1
    :goto_0
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSetEmptyText:Z

    .line 933
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 934
    return-void

    .line 923
    :cond_2
    if-eqz p1, :cond_1

    .line 924
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_3

    .line 925
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-interface {v0, v3, v1, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0

    .line 927
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v3, v1, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0
.end method

.method public setTextBoxListener(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;)V
    .locals 4
    .param p1, "actionListener"    # Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    .prologue
    .line 613
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    .line 615
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v3, :cond_0

    .line 616
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 617
    .local v1, "start":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 619
    .local v0, "end":I
    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v2

    .line 620
    .local v2, "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 622
    .end local v0    # "end":I
    .end local v1    # "start":I
    .end local v2    # "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_0
    return-void
.end method

.method public setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V
    .locals 3
    .param p1, "textSpan"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .prologue
    .line 958
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 959
    .local v1, "start":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 961
    .local v0, "end":I
    instance-of v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    if-eqz v2, :cond_1

    .line 962
    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    .end local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFontColor(III)V

    .line 971
    :cond_0
    :goto_0
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 973
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 974
    return-void

    .line 963
    .restart local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_1
    instance-of v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v2, :cond_2

    .line 964
    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFontSize(FII)V

    goto :goto_0

    .line 965
    .restart local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_2
    instance-of v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    if-eqz v2, :cond_3

    .line 966
    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    .end local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextBackgroundColor(III)V

    goto :goto_0

    .line 967
    .restart local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_3
    instance-of v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    if-eqz v2, :cond_0

    .line 968
    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    .end local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget-object v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFontName(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public setTextLimit(I)V
    .locals 5
    .param p1, "count"    # I

    .prologue
    const/4 v4, 0x1

    .line 1277
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-nez v1, :cond_0

    .line 1299
    :goto_0
    return-void

    .line 1281
    :cond_0
    const/16 v1, 0x1388

    if-ge v1, p1, :cond_1

    .line 1282
    const/16 p1, 0x1388

    .line 1285
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    .line 1287
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    if-le v1, v2, :cond_2

    .line 1288
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 1290
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 1291
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 1295
    :cond_2
    new-array v0, v4, [Landroid/text/InputFilter;

    .line 1296
    .local v0, "arrayOfInputFilter":[Landroid/text/InputFilter;
    const/4 v1, 0x0

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;-><init>(Landroid/content/Context;I)V

    aput-object v2, v0, v1

    .line 1298
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1, v0}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_0
.end method

.method public setTextStyle(IZ)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "option"    # Z

    .prologue
    const/4 v3, 0x1

    .line 980
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 981
    .local v1, "start":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 983
    .local v0, "end":I
    if-ne p1, v3, :cond_1

    .line 984
    invoke-direct {p0, p2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextBold(ZII)V

    .line 991
    :cond_0
    :goto_0
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 993
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 994
    return-void

    .line 985
    :cond_1
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    .line 986
    invoke-direct {p0, p2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextItalic(ZII)V

    goto :goto_0

    .line 987
    :cond_2
    const/4 v2, 0x4

    if-ne p1, v2, :cond_0

    .line 988
    invoke-direct {p0, p2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextUnderline(ZII)V

    goto :goto_0
.end method

.method public setViewModeEnabled(Z)V
    .locals 2
    .param p1, "option"    # Z

    .prologue
    const/4 v1, 0x1

    .line 1149
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    .line 1151
    if-eqz p1, :cond_0

    .line 1152
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusableInTouchMode(Z)V

    .line 1153
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    .line 1154
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusable(Z)V

    .line 1156
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->requestFocus()Z

    .line 1158
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 1160
    :cond_0
    return-void
.end method

.method public showSoftInput()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x64

    const/4 v4, 0x1

    .line 1080
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 1081
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPreventShowSoftInput:Z

    if-eqz v2, :cond_1

    .line 1115
    :cond_0
    :goto_0
    return-void

    .line 1084
    :cond_1
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPreventShowSoftInput:Z

    .line 1085
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 1087
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHasWindowFocus:Z

    if-eqz v2, :cond_2

    .line 1088
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1090
    :try_start_0
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1091
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$8;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$8;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 1099
    const-wide/16 v4, 0x64

    .line 1091
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1101
    :catch_0
    move-exception v0

    .line 1102
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0

    .line 1106
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :cond_2
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$9;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$9;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public showTextBox()V
    .locals 1

    .prologue
    .line 863
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    .line 864
    return-void
.end method

.method protected updateContextMenuLocation()V
    .locals 1

    .prologue
    .line 1872
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 1873
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    .line 1874
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    .line 1876
    :cond_0
    return-void
.end method
