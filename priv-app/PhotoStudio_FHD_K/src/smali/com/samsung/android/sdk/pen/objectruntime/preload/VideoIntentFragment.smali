.class public Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;
.super Landroid/app/Fragment;
.source "VideoIntentFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;
    }
.end annotation


# static fields
.field private static mBackupListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;


# instance fields
.field private mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 60
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    if-eqz v1, :cond_0

    .line 61
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 62
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.PICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    const-string v1, "video/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 66
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 75
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 76
    if-eqz p3, :cond_2

    .line 77
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    if-nez v0, :cond_1

    .line 78
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mBackupListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    if-eqz v0, :cond_0

    .line 79
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mBackupListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    invoke-interface {v0, p3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;->onResult(Landroid/content/Intent;)V

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    invoke-interface {v0, p3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;->onResult(Landroid/content/Intent;)V

    goto :goto_0

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    invoke-interface {v0, p3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;->onResult(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 21
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 22
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;->onDestroy()V

    .line 25
    :cond_0
    return-void
.end method

.method public setVideoIntentFragmentListener(Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    .prologue
    .line 96
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mBackupListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    .line 99
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    .line 100
    return-void
.end method
