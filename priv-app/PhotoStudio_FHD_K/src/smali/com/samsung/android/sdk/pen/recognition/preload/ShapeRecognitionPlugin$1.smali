.class Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$1;
.super Landroid/os/Handler;
.source "ShapeRecognitionPlugin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;

    .line 183
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 186
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$HandleInfo;

    .line 187
    .local v0, "info":Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$HandleInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->access$0(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->access$0(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$HandleInfo;->mInput:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$HandleInfo;->access$0(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$HandleInfo;)Ljava/util/List;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$HandleInfo;->mOutput:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$HandleInfo;->access$1(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$HandleInfo;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;->onResult(Ljava/util/List;Ljava/util/List;)V

    .line 190
    :cond_0
    return-void
.end method
