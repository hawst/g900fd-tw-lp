.class public interface abstract Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;
.super Ljava/lang/Object;
.source "SpenNotePad.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ActionListener"
.end annotation


# virtual methods
.method public abstract onChangePan(FF)V
.end method

.method public abstract onCropBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;FF)V
.end method

.method public abstract onPostTouch(Landroid/view/MotionEvent;)Z
.end method

.method public abstract onPreTouch(Landroid/view/MotionEvent;)Z
.end method

.method public abstract onSizeChanged()V
.end method

.method public abstract onStop()V
.end method

.method public abstract onUpdate(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
.end method

.method public abstract onViewTouchEvent(Landroid/view/MotionEvent;FFFF)V
.end method
