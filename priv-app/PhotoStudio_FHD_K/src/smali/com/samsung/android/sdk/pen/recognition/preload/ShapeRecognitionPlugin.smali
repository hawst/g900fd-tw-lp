.class public Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;
.super Ljava/lang/Object;
.source "ShapeRecognitionPlugin.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$HandleInfo;,
        Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ShapeRecognitionPlugin"

.field private static mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;


# instance fields
.field private final mHandler:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .line 183
    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mHandler:Landroid/os/Handler;

    .line 54
    const-string v0, "ShapeRecognitionPlugin"

    const-string v1, "creating shape recognition plugin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->addStroke(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$2()Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addStroke(Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "input":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/high16 v12, 0x3f000000    # 0.5f

    .line 81
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 106
    return-void

    .line 81
    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 82
    .local v6, "obj":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .end local v6    # "obj":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v7

    .line 83
    .local v7, "pointArray":[Landroid/graphics/PointF;
    if-eqz v7, :cond_0

    .line 84
    array-length v5, v7

    .line 85
    .local v5, "length":I
    const/16 v10, 0x400

    if-le v5, v10, :cond_2

    .line 86
    const/16 v5, 0x400

    .line 88
    :cond_2
    if-lez v5, :cond_0

    .line 89
    new-array v8, v5, [F

    .line 90
    .local v8, "pointX":[F
    new-array v9, v5, [F

    .line 92
    .local v9, "pointY":[F
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-lt v4, v5, :cond_3

    .line 102
    sget-object v10, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    invoke-virtual {v10, v8, v9}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->addStroke([F[F)V

    goto :goto_0

    .line 94
    :cond_3
    aget-object v10, v7, v4

    iget v0, v10, Landroid/graphics/PointF;->x:F

    .line 95
    .local v0, "fx":F
    aget-object v10, v7, v4

    iget v1, v10, Landroid/graphics/PointF;->y:F

    .line 96
    .local v1, "fy":F
    aget-object v10, v7, v4

    iget v10, v10, Landroid/graphics/PointF;->x:F

    float-to-int v2, v10

    .line 97
    .local v2, "gx":I
    aget-object v10, v7, v4

    iget v10, v10, Landroid/graphics/PointF;->y:F

    float-to-int v3, v10

    .line 99
    .local v3, "gy":I
    int-to-float v10, v2

    sub-float v10, v0, v10

    cmpl-float v10, v10, v12

    if-lez v10, :cond_4

    int-to-float v10, v2

    add-float/2addr v10, v12

    :goto_2
    aput v10, v8, v4

    .line 100
    int-to-float v10, v3

    sub-float v10, v1, v10

    cmpl-float v10, v10, v12

    if-lez v10, :cond_5

    int-to-float v10, v3

    add-float/2addr v10, v12

    :goto_3
    aput v10, v9, v4

    .line 92
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 99
    :cond_4
    int-to-float v10, v2

    goto :goto_2

    .line 100
    :cond_5
    int-to-float v10, v3

    goto :goto_3
.end method


# virtual methods
.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 293
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 277
    return-void
.end method

.method public initRecognizer(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 62
    const-class v1, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;

    monitor-enter v1

    .line 63
    :try_start_0
    sget-object v2, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    if-nez v2, :cond_1

    .line 64
    new-instance v2, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;-><init>()V

    sput-object v2, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    .line 66
    sget-object v2, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    if-nez v2, :cond_0

    .line 67
    const-string v2, "ShapeRecognitionPlugin"

    const-string v3, "Fail to create Shape recognition instance."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    monitor-exit v1

    .line 77
    :goto_0
    return v0

    .line 71
    :cond_0
    sget-object v2, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->init(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 72
    const-string v2, "ShapeRecognitionPlugin"

    const-string v3, "Fail to initialize."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    monitor-exit v1

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 229
    const-string v1, "Shape Recognition"

    const-string v2, "Load libSPenVIRecognition.so."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    :try_start_0
    const-string v1, "SPenVIRecognition"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->initRecognizer(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 253
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Fail to load Shape recognition engine"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 234
    :catch_0
    move-exception v0

    .line 235
    .local v0, "error":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 236
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "libSPenVIRecognition.so is not loaded."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 255
    .end local v0    # "error":Ljava/lang/Exception;
    :cond_0
    return-void
.end method

.method public onUnload()V
    .locals 2

    .prologue
    .line 263
    const-class v1, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;

    monitor-enter v1

    .line 264
    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    if-eqz v0, :cond_0

    .line 265
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->dispose()V

    .line 266
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    .line 263
    :cond_0
    monitor-exit v1

    .line 269
    return-void

    .line 263
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public request(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 199
    .local p1, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    if-nez v1, :cond_0

    .line 200
    const-string v1, "ShapeRecognitionPlugin"

    const-string v2, "The result listener isn\'t set yet!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 205
    :cond_0
    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    if-eqz v1, :cond_1

    .line 206
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 207
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 212
    .end local v0    # "thread":Ljava/lang/Thread;
    :goto_0
    return-void

    .line 209
    :cond_1
    const-string v1, "ShapeRecognitionPlugin"

    const-string v2, "The recognition engine is null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 285
    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .line 221
    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 302
    const/4 v0, 0x1

    return v0
.end method
