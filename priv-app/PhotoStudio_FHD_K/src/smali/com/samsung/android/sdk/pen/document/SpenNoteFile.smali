.class public Lcom/samsung/android/sdk/pen/document/SpenNoteFile;
.super Ljava/lang/Object;
.source "SpenNoteFile.java"


# static fields
.field public static final ORIENTATION_LANDSCAPE:I = 0x1

.field public static final ORIENTATION_PORTRAIT:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method private static native NoteFile_copy(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private static native NoteFile_getAppName(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private static native NoteFile_getAppVersion(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/StringBuffer;)Z
.end method

.method private static native NoteFile_getCoverImage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method private static native NoteFile_getFormatVersion(Ljava/lang/String;)I
.end method

.method private static native NoteFile_getOrientation(Ljava/lang/String;)I
.end method

.method private static native NoteFile_getSize(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Z
.end method

.method private static native NoteFile_hasUnsavedData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Z
.end method

.method private static native NoteFile_isFavorite(Ljava/lang/String;)Z
.end method

.method private static native NoteFile_isLocked(Ljava/lang/String;)Z
.end method

.method private static native NoteFile_isValid(Ljava/lang/String;)Z
.end method

.method private static native NoteFile_lock(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private static native NoteFile_removeNote(Ljava/lang/String;)Z
.end method

.method private static native NoteFile_setCoverImage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private static native NoteFile_setFavorite(Ljava/lang/String;Z)Z
.end method

.method private static native NoteFile_unlock(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public static copy(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "destFilePath"    # Ljava/lang/String;
    .param p1, "srcFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 162
    invoke-static {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_copy(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 163
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 164
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 171
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 175
    :cond_0
    return-void

    .line 166
    :pswitch_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 168
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "E_UNSUPPORTED_TYPE : ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 169
    const-string v3, "] does not correspond to the SPD file format"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 168
    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 164
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getAppName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 412
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_getAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAppVersion(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/StringBuffer;)V
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "major"    # Ljava/lang/Integer;
    .param p2, "minor"    # Ljava/lang/Integer;
    .param p3, "patchName"    # Ljava/lang/StringBuffer;

    .prologue
    .line 435
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_getAppVersion(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/StringBuffer;)Z

    move-result v0

    .line 436
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 437
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 439
    :cond_0
    return-void
.end method

.method public static getCoverImagePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 305
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_getCoverImage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getFormatVersion(Ljava/lang/String;)I
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 453
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_getFormatVersion(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getOrientation(Ljava/lang/String;)I
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 322
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_getOrientation(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getSize(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "width"    # Ljava/lang/Integer;
    .param p2, "height"    # Ljava/lang/Integer;

    .prologue
    .line 391
    invoke-static {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_getSize(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Z

    move-result v0

    .line 392
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 393
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 395
    :cond_0
    return-void
.end method

.method public static hasUnsavedData(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "lastEditedTime"    # Ljava/lang/Long;

    .prologue
    .line 341
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_hasUnsavedData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Z

    move-result v0

    return v0
.end method

.method private static isBuildTypeEngMode()Z
    .locals 2

    .prologue
    .line 457
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isFavorite(Ljava/lang/String;)Z
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 224
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_isFavorite(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isLocked(Ljava/lang/String;)Z
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 140
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_isLocked(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isValid(Ljava/lang/String;)Z
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 239
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_isValid(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static lock(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_lock(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 65
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 66
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 76
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 79
    :cond_0
    return-void

    .line 68
    :pswitch_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 73
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "E_UNSUPPORTED_TYPE : ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 74
    const-string v3, "] does not correspond to the SPD file format"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 73
    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 66
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static removeNote(Ljava/lang/String;)V
    .locals 4
    .param p0, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 360
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_removeNote(Ljava/lang/String;)Z

    move-result v0

    .line 361
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 362
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 372
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 375
    :cond_0
    return-void

    .line 364
    :pswitch_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 369
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "E_UNSUPPORTED_TYPE : ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 370
    const-string v3, "] does not correspond to the SPD file format"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 369
    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 362
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static setCoverImage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "imageFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 271
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_setCoverImage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 272
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 273
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 283
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 286
    :cond_0
    return-void

    .line 275
    :pswitch_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 280
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "E_UNSUPPORTED_TYPE : ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 281
    const-string v3, "] does not correspond to the SPD file format"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 280
    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 273
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static setFavorite(Ljava/lang/String;Z)V
    .locals 4
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "flag"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 197
    invoke-static {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_setFavorite(Ljava/lang/String;Z)Z

    move-result v0

    .line 198
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 199
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 206
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 209
    :cond_0
    return-void

    .line 201
    :pswitch_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 203
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "E_UNSUPPORTED_TYPE : ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 204
    const-string v3, "] does not correspond to the SPD file format"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 203
    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 199
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static unlock(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_unlock(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 108
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 109
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 121
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 124
    :cond_0
    return-void

    .line 111
    :sswitch_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 113
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v2, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 118
    :sswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "E_UNSUPPORTED_TYPE : ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 119
    const-string v3, "] does not correspond to the SPD file format"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 118
    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 109
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0xd -> :sswitch_2
        0x11 -> :sswitch_1
    .end sparse-switch
.end method
