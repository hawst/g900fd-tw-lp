.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    .line 1889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1895
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$86(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v5

    const/4 v6, 0x5

    if-eq v5, v6, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$86(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v5

    const/4 v6, 0x6

    if-eq v5, v6, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$86(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v5

    const/4 v6, 0x7

    if-ne v5, v6, :cond_1

    .line 2034
    :cond_0
    :goto_0
    return-void

    .line 1898
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$45(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 1900
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$83(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-result-object v6

    const-string v7, "string_pen"

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$83(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-result-object v6

    const-string v7, "string_tab_selected_tts"

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 1901
    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    .line 1899
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1902
    .local v4, "strPenTabChar":Ljava/lang/CharSequence;
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$83(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-result-object v6

    const-string v7, "string_preset"

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1903
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$83(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-result-object v6

    const-string v7, "string_tab_selected_tts"

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1902
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 1903
    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    .line 1902
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1905
    .local v3, "strPenPresetChar":Ljava/lang/CharSequence;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$84(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/TextView;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1906
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1907
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$84(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1909
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$102(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 1911
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1912
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$84(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1913
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-boolean v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    if-eqz v5, :cond_4

    .line 1914
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    const v7, 0x44bb8000    # 1500.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 1918
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    .line 1919
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$89(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1920
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$90(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/ViewGroup;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1921
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$91(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1922
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$22(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1923
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setVisibility(I)V

    .line 1924
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$94(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1925
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setColorSelectorViewForBeautifyPen()V
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$103(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    .line 1927
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99()I

    move-result v5

    const/16 v6, 0x10

    if-ge v5, v6, :cond_5

    .line 1928
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$96(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$104()Ljava/lang/String;

    move-result-object v7

    .line 1929
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$105()Ljava/lang/String;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$105()Ljava/lang/String;

    move-result-object v9

    .line 1928
    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1935
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$96(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ImageView;->invalidate()V

    .line 1938
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isBeautifyPen(Ljava/lang/String;)Z
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1939
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v6, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$92(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 1940
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v6, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyStyleBtnsLayout(Z)V
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$93(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 1948
    :goto_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMontblancMode:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$108(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v6, "com.samsung.android.sdk.pen.pen.preload.MontblancFountainPen"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1949
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMontblancStyleBtnsLayout:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1951
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$100(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/LinearLayout;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1953
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$42(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1954
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$94(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getY()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$109(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 1955
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->currenMagicPenHeight:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$110(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v6

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setMagicPenMode(I)V
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$111(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 2030
    :cond_3
    :goto_4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setButtonFocus(Landroid/view/View;)V
    invoke-static {v5, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$85(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2031
    .end local v3    # "strPenPresetChar":Ljava/lang/CharSequence;
    .end local v4    # "strPenTabChar":Ljava/lang/CharSequence;
    :catch_0
    move-exception v0

    .line 2032
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 1916
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local v3    # "strPenPresetChar":Ljava/lang/CharSequence;
    .restart local v4    # "strPenTabChar":Ljava/lang/CharSequence;
    :cond_4
    :try_start_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$88(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v6

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    goto/16 :goto_1

    .line 1931
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$96(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$104()Ljava/lang/String;

    move-result-object v7

    .line 1932
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$105()Ljava/lang/String;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$105()Ljava/lang/String;

    move-result-object v9

    .line 1931
    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 1941
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isChinesePen(Ljava/lang/String;)Z
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$106(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1942
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v6, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$92(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    goto/16 :goto_3

    .line 1944
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v6, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$92(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 1945
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v6, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifySettingViews(Z)V
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$107(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    goto/16 :goto_3

    .line 1957
    :cond_8
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$84(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1958
    const/4 v5, 0x1

    sput-boolean v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsSwichTab:Z

    .line 1959
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v5

    if-eqz v5, :cond_9

    .line 1960
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v5

    sget-boolean v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsSwichTab:Z

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->setSwitchTabFlag(Z)V

    .line 1962
    :cond_9
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1963
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$84(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1965
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$102(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 1967
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1968
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$84(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1969
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    .line 1970
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-boolean v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    if-eqz v5, :cond_d

    .line 1971
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    const v7, 0x44bb8000    # 1500.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 1975
    :goto_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$89(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1976
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$90(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/ViewGroup;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1977
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$91(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1978
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$22(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1979
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setVisibility(I)V

    .line 1980
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v6, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$92(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 1981
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v6, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyStyleBtnsLayout(Z)V
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$93(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 1982
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$94(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1983
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMontblancStyleBtnsLayout:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1984
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$95(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$96(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$97()Ljava/lang/String;

    move-result-object v7

    .line 1985
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$98()Ljava/lang/String;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$98()Ljava/lang/String;

    move-result-object v9

    .line 1984
    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1986
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99()I

    move-result v5

    const/16 v6, 0x10

    if-ge v5, v6, :cond_e

    .line 1987
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$96(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$97()Ljava/lang/String;

    move-result-object v7

    .line 1988
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$98()Ljava/lang/String;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$98()Ljava/lang/String;

    move-result-object v9

    .line 1987
    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1994
    :goto_6
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$96(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ImageView;->invalidate()V

    .line 1995
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v5

    const/4 v6, -0x1

    iput v6, v5, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCurrentSeleted:I

    .line 1996
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/List;

    move-result-object v5

    if-nez v5, :cond_a

    .line 1997
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$81(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/util/List;)V

    .line 1999
    :cond_a
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isRemovePreset:Z
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$2()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 2000
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_7
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lt v1, v5, :cond_f

    .line 2024
    .end local v1    # "i":I
    :cond_b
    :goto_8
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$42(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 2025
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$94(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getY()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$109(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 2027
    :cond_c
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetDisplay()V
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    .line 2028
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$100(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/LinearLayout;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_4

    .line 1973
    :cond_d
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$88(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v6

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    goto/16 :goto_5

    .line 1990
    :cond_e
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$96(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$97()Ljava/lang/String;

    move-result-object v7

    .line 1991
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$98()Ljava/lang/String;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$98()Ljava/lang/String;

    move-result-object v9

    .line 1990
    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_6

    .line 2001
    .restart local v1    # "i":I
    :cond_f
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    .line 2002
    .local v2, "info":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v6

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_10

    .line 2003
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getColor()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v6

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    if-ne v5, v6, :cond_10

    .line 2004
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 2005
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 2006
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviousSelectedPresetIndex:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v6

    iput v6, v5, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCurrentSeleted:I

    .line 2007
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$SpenPresetGridView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$17(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$SpenPresetGridView;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviousSelectedPresetIndex:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$SpenPresetGridView;->smoothScrollToPosition(I)V

    goto/16 :goto_8

    .line 2000
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_7

    .line 2012
    .end local v1    # "i":I
    .end local v2    # "info":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    :cond_11
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_12
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    .line 2013
    .restart local v2    # "info":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v7

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    cmpl-float v6, v6, v7

    if-nez v6, :cond_12

    .line 2014
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getColor()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v7

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    if-ne v6, v7, :cond_12

    .line 2015
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 2016
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 2017
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviousSelectedPresetIndex:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v6

    iput v6, v5, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCurrentSeleted:I

    .line 2018
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$SpenPresetGridView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$17(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$SpenPresetGridView;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviousSelectedPresetIndex:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$SpenPresetGridView;->smoothScrollToPosition(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_8
.end method
