.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;
.super Ljava/lang/Object;
.source "SpenSettingDockingLayout.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    .line 586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPopup(Z)V
    .locals 2
    .param p1, "open"    # Z

    .prologue
    const/4 v1, 0x0

    .line 590
    if-eqz p1, :cond_1

    .line 591
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    .line 592
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    .line 593
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    .line 594
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    .line 596
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    .line 602
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setTopBottomToZeroMarginItem()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    .line 603
    return-void

    .line 600
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    goto :goto_0
.end method
