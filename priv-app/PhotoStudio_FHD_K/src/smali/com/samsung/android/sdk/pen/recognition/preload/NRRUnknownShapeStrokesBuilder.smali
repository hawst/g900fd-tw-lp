.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRUnknownShapeStrokesBuilder;
.super Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;
.source "NRRUnknownShapeStrokesBuilder.java"


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;Lcom/samsung/recognitionengine/ShapeInfo;)V
    .locals 0
    .param p1, "penSettings"    # Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;
    .param p2, "shapeInfo"    # Lcom/samsung/recognitionengine/ShapeInfo;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;Lcom/samsung/recognitionengine/ShapeInfo;)V

    .line 26
    return-void
.end method


# virtual methods
.method public buildLayoutObject()Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 30

    .prologue
    .line 29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUnknownShapeStrokesBuilder;->mShapeInfo:Lcom/samsung/recognitionengine/ShapeInfo;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/recognitionengine/ShapeInfo;->getRecognizedPoints()Lcom/samsung/recognitionengine/PointFVector;

    move-result-object v17

    .line 30
    .local v17, "recognizedPoints":Lcom/samsung/recognitionengine/PointFVector;
    new-instance v22, Lcom/samsung/recognitionengine/PolylineSmoother;

    invoke-direct/range {v22 .. v22}, Lcom/samsung/recognitionengine/PolylineSmoother;-><init>()V

    .line 33
    .local v22, "smoother":Lcom/samsung/recognitionengine/PolylineSmoother;
    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/PolylineSmoother;->smoothPolyline(Lcom/samsung/recognitionengine/PointFVector;)Lcom/samsung/recognitionengine/ShapeInfoVector;

    move-result-object v21

    .line 34
    .local v21, "smoothedSegments":Lcom/samsung/recognitionengine/ShapeInfoVector;
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/recognitionengine/ShapeInfoVector;->size()J

    move-result-wide v26

    move-wide/from16 v0, v26

    long-to-int v0, v0

    move/from16 v19, v0

    .line 35
    .local v19, "segmentsCount":I
    if-nez v19, :cond_0

    .line 36
    const/4 v4, 0x0

    .line 81
    :goto_0
    return-object v4

    .line 38
    :cond_0
    new-instance v4, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 39
    .local v4, "container":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    new-instance v23, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>()V

    .line 41
    .local v23, "stroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    const/4 v14, 0x0

    .line 42
    .local v14, "pointsCount":I
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v19}, Ljava/util/ArrayList;-><init>(I)V

    .line 43
    .local v18, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/recognitionengine/VectorPointFVectors;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    move/from16 v0, v19

    if-lt v6, v0, :cond_2

    .line 54
    new-array v15, v14, [Landroid/graphics/PointF;

    .line 55
    .local v15, "pointsToAdd":[Landroid/graphics/PointF;
    new-array v0, v14, [F

    move-object/from16 v16, v0

    .line 56
    .local v16, "pressures":[F
    new-array v0, v14, [I

    move-object/from16 v24, v0

    .line 57
    .local v24, "timestamps":[I
    const/high16 v25, 0x3f800000    # 1.0f

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 59
    const/4 v10, 0x0

    .line 60
    .local v10, "pointIndex":I
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :cond_1
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-nez v26, :cond_4

    .line 74
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    move-object/from16 v2, v24

    invoke-virtual {v0, v15, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPoints([Landroid/graphics/PointF;[F[I)V

    .line 76
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUnknownShapeStrokesBuilder;->setStrokeStyle(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    .line 77
    const/16 v25, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setToolType(I)V

    .line 78
    const/16 v25, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setCurveEnabled(Z)V

    .line 79
    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0

    .line 44
    .end local v10    # "pointIndex":I
    .end local v15    # "pointsToAdd":[Landroid/graphics/PointF;
    .end local v16    # "pressures":[F
    .end local v24    # "timestamps":[I
    :cond_2
    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Lcom/samsung/recognitionengine/ShapeInfoVector;->get(I)Lcom/samsung/recognitionengine/ShapeInfo;

    move-result-object v20

    .line 46
    .local v20, "shapeSegment":Lcom/samsung/recognitionengine/ShapeInfo;
    const/16 v5, 0x19

    .line 47
    .local v5, "generateCount":I
    int-to-long v0, v5

    move-wide/from16 v26, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeInfo;->generatePoints(J)Lcom/samsung/recognitionengine/VectorPointFVectors;

    move-result-object v11

    .line 48
    .local v11, "pointVector":Lcom/samsung/recognitionengine/VectorPointFVectors;
    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    invoke-virtual {v11}, Lcom/samsung/recognitionengine/VectorPointFVectors;->size()J

    move-result-wide v26

    move-wide/from16 v0, v26

    long-to-int v12, v0

    .line 50
    .local v12, "pointVectorSize":I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_2
    if-lt v7, v12, :cond_3

    .line 43
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 51
    :cond_3
    int-to-long v0, v14

    move-wide/from16 v26, v0

    invoke-virtual {v11, v7}, Lcom/samsung/recognitionengine/VectorPointFVectors;->get(I)Lcom/samsung/recognitionengine/PointFVector;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/recognitionengine/PointFVector;->size()J

    move-result-wide v28

    add-long v26, v26, v28

    move-wide/from16 v0, v26

    long-to-int v14, v0

    .line 50
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 60
    .end local v5    # "generateCount":I
    .end local v7    # "j":I
    .end local v11    # "pointVector":Lcom/samsung/recognitionengine/VectorPointFVectors;
    .end local v12    # "pointVectorSize":I
    .end local v20    # "shapeSegment":Lcom/samsung/recognitionengine/ShapeInfo;
    .restart local v10    # "pointIndex":I
    .restart local v15    # "pointsToAdd":[Landroid/graphics/PointF;
    .restart local v16    # "pressures":[F
    .restart local v24    # "timestamps":[I
    :cond_4
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/recognitionengine/VectorPointFVectors;

    .line 61
    .restart local v11    # "pointVector":Lcom/samsung/recognitionengine/VectorPointFVectors;
    invoke-virtual {v11}, Lcom/samsung/recognitionengine/VectorPointFVectors;->size()J

    move-result-wide v26

    move-wide/from16 v0, v26

    long-to-int v12, v0

    .line 63
    .restart local v12    # "pointVectorSize":I
    const/4 v7, 0x0

    .restart local v7    # "j":I
    :goto_3
    if-ge v7, v12, :cond_1

    .line 64
    invoke-virtual {v11, v7}, Lcom/samsung/recognitionengine/VectorPointFVectors;->get(I)Lcom/samsung/recognitionengine/PointFVector;

    move-result-object v13

    .line 65
    .local v13, "points":Lcom/samsung/recognitionengine/PointFVector;
    invoke-virtual {v13}, Lcom/samsung/recognitionengine/PointFVector;->size()J

    move-result-wide v26

    move-wide/from16 v0, v26

    long-to-int v14, v0

    .line 66
    const/4 v8, 0x0

    .local v8, "l":I
    :goto_4
    if-lt v8, v14, :cond_5

    .line 63
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 67
    :cond_5
    invoke-virtual {v13, v8}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v9

    .line 68
    .local v9, "p":Lcom/samsung/recognitionengine/PointF;
    new-instance v26, Landroid/graphics/PointF;

    invoke-virtual {v9}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v27

    invoke-virtual {v9}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v28

    invoke-direct/range {v26 .. v28}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v26, v15, v10

    .line 69
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v26

    move-wide/from16 v0, v26

    long-to-int v0, v0

    move/from16 v26, v0

    aput v26, v24, v10

    .line 70
    add-int/lit8 v10, v10, 0x1

    .line 66
    add-int/lit8 v8, v8, 0x1

    goto :goto_4
.end method
