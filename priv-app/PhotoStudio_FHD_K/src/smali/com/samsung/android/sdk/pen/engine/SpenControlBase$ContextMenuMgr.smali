.class public Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;
.super Ljava/lang/Object;
.source "SpenControlBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ContextMenuMgr"
.end annotation


# instance fields
.field public mDirtyFlag:Z

.field public mFirstDraw:Z

.field public mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

.field public mItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mVisible:Z

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;


# direct methods
.method protected constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 465
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 466
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    .line 467
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mDirtyFlag:Z

    .line 468
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .line 469
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mFirstDraw:Z

    .line 470
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    return-void
.end method


# virtual methods
.method public getPopupMenu()Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getPopupMenu()Landroid/widget/PopupWindow;

    move-result-object v0

    .line 501
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->hide()V

    .line 494
    :cond_0
    return-void
.end method

.method public resetDirty()V
    .locals 1

    .prologue
    .line 477
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mDirtyFlag:Z

    .line 478
    return-void
.end method

.method public setDirty()V
    .locals 1

    .prologue
    .line 473
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mDirtyFlag:Z

    .line 474
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 481
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mDirtyFlag:Z

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->updateContextMenu()V

    .line 483
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->resetDirty()V

    .line 485
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    if-eqz v0, :cond_1

    .line 486
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->show()V

    .line 488
    :cond_1
    return-void
.end method
