.class public Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;
.super Ljava/lang/Object;
.source "EquationRecognition.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EquationRecognition"


# instance fields
.field private mEqRecLib:Lcom/samsung/vip/engine/VIEquationRecognitionLib;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method


# virtual methods
.method public addStroke([F[F)V
    .locals 1
    .param p1, "x"    # [F
    .param p2, "y"    # [F

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/VIEquationRecognitionLib;

    if-nez v0, :cond_0

    .line 57
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/VIEquationRecognitionLib;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->addStroke([F[F)V

    goto :goto_0
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/VIEquationRecognitionLib;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/VIEquationRecognitionLib;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->close()V

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/VIEquationRecognitionLib;

    .line 79
    :cond_0
    return-void
.end method

.method public init(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 29
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/recognition/preload/VIRecogUtils;->copyDatabase(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 30
    const-string v3, "EquationRecognition"

    const-string v4, "Fail to copy database."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    :goto_0
    return v2

    .line 34
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/vidata/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "dataPath":Ljava/lang/String;
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/VIEquationRecognitionLib;

    .line 37
    new-instance v3, Lcom/samsung/vip/engine/VIEquationRecognitionLib;

    invoke-direct {v3}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/VIEquationRecognitionLib;

    .line 39
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/VIEquationRecognitionLib;

    invoke-virtual {v3, v0}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->init(Ljava/lang/String;)I

    move-result v1

    .line 40
    .local v1, "ret":I
    if-eqz v1, :cond_1

    .line 41
    const-string v3, "EquationRecognition"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "equation engine init : ret = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 45
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public process()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/VIEquationRecognitionLib;

    if-nez v0, :cond_0

    .line 65
    const/4 v0, 0x0

    .line 67
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/VIEquationRecognitionLib;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->recog()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
