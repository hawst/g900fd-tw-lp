.class Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;
.super Ljava/lang/Object;
.source "SpenInView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenInView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BaseControlListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 5231
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;)V
    .locals 0

    .prologue
    .line 5231
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    return-void
.end method


# virtual methods
.method public onClosed(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v3, 0x0

    .line 5234
    const-string v1, "SpenInView"

    const-string v2, "BaseControlListener onClosed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5235
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/view/ViewGroup;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5236
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/view/ViewGroup;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 5239
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5241
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->selectObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5249
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-static {v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    .line 5251
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 5252
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlListener;->onClosed(Ljava/util/ArrayList;)Z

    .line 5255
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObjectCount(Z)I

    move-result v1

    if-lez v1, :cond_3

    .line 5256
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    .line 5258
    :cond_3
    return-void

    .line 5242
    :catch_0
    move-exception v0

    .line 5243
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onMenuSelected(Ljava/util/ArrayList;I)V
    .locals 1
    .param p2, "itemId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 5312
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5313
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlListener;->onMenuSelected(Ljava/util/ArrayList;I)Z

    .line 5315
    :cond_0
    return-void
.end method

.method public onObjectChanged(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 5262
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const-string v0, "SpenInView"

    const-string v1, "onObjectChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5263
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 5275
    :cond_0
    :goto_0
    return-void

    .line 5266
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5270
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 5271
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlListener;->onObjectChanged(Ljava/util/ArrayList;)V

    .line 5274
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    goto :goto_0
.end method

.method public onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 5298
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5299
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlListener;->onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 5301
    :cond_0
    return-void
.end method

.method public onRequestBackground()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 5348
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/view/SurfaceView;

    if-eqz v0, :cond_0

    .line 5349
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->captureCurrentView(Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 5351
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 6
    .param p1, "coordinateInfo"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .prologue
    .line 5319
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    .line 5320
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    .line 5319
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 5321
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getPan()Landroid/graphics/PointF;

    move-result-object v0

    iput-object v0, p1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    .line 5322
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomRatio()F

    move-result v0

    iput v0, p1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    .line 5323
    return-void
.end method

.method public onRequestScroll(FF)V
    .locals 9
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    const/4 v6, 0x1

    .line 5327
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 5344
    :goto_0
    return-void

    .line 5330
    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 5332
    .local v0, "point":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getPan(JLandroid/graphics/PointF;)V
    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/graphics/PointF;)V

    .line 5333
    iget v1, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, p1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 5334
    iget v1, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, p2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 5335
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEditableTextBox()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5336
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget v5, v0, Landroid/graphics/PointF;->y:F

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPan(JFFZ)V
    invoke-static/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenInView;JFFZ)V

    .line 5337
    const/4 v0, 0x0

    .line 5338
    goto :goto_0

    .line 5339
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget v5, v0, Landroid/graphics/PointF;->y:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v7

    const/high16 v8, 0x3f800000    # 1.0f

    cmpl-float v7, v7, v8

    if-nez v7, :cond_2

    :goto_1
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPan(JFFZ)V
    invoke-static/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenInView;JFFZ)V

    .line 5340
    const/4 v0, 0x0

    .line 5341
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    goto :goto_0

    .line 5339
    :cond_2
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1
    .param p1, "angle"    # F
    .param p2, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 5305
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5306
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlListener;->onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 5308
    :cond_0
    return-void
.end method

.method public onVisibleUpdated(Ljava/util/ArrayList;Z)V
    .locals 8
    .param p2, "visible"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 5279
    if-nez p1, :cond_1

    .line 5280
    const-string v0, "SpenInView"

    const-string v1, "onVisibleUpdated : objectList is null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5294
    :cond_0
    :goto_0
    return-void

    .line 5283
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    .line 5284
    :cond_2
    const-string v0, "SpenInView"

    const-string v1, "onVisibleUpdated : the size of list is zero."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5288
    :cond_3
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRuntimeHandle()I

    move-result v4

    .line 5289
    .local v4, "runtimeHandle":I
    if-nez p2, :cond_4

    .line 5290
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    move v6, v5

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_inVisibleUpdate(JIZZ)V
    invoke-static/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenInView;JIZZ)V

    goto :goto_0

    .line 5291
    :cond_4
    if-eqz p2, :cond_0

    .line 5292
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    move v5, v7

    move v6, v7

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_inVisibleUpdate(JIZZ)V
    invoke-static/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenInView;JIZZ)V

    goto :goto_0
.end method
