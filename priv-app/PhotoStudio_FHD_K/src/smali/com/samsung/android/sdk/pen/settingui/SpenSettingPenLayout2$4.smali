.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout2.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    .line 718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onColorChanged(III)V
    .locals 4
    .param p1, "color"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 722
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v1

    .line 723
    .local v1, "penPluginIndex":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 761
    :cond_0
    :goto_0
    return-void

    .line 726
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 731
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 732
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    const/16 v3, 0xff

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setAlpha(I)V

    .line 734
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 735
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->invalidate()V

    .line 736
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->invalidate()V

    .line 737
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->invalidate()V

    .line 738
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->invalidate()V

    .line 740
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_0

    .line 741
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v0

    .line 742
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    if-eqz v0, :cond_0

    .line 743
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v2

    .line 744
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 745
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlpha:I

    shl-int/lit8 v2, v2, 0x18

    const/high16 v3, -0x1000000

    and-int/2addr v2, v3

    const v3, 0xffffff

    and-int/2addr v3, p1

    or-int/2addr v2, v3

    iput v2, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 750
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v3, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 751
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 752
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 753
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeColor(I)V

    .line 754
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->invalidate()V

    .line 755
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    iget v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setColor(I)V

    .line 756
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-interface {v2, v3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setColor(I)V

    .line 757
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->setColorPickerColor(I)V

    goto/16 :goto_0

    .line 748
    :cond_2
    iput p1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    goto :goto_1
.end method
