.class public Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;
.super Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.source "SpenControlContainer.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 40
    return-void
.end method


# virtual methods
.method public getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .locals 3

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    .line 77
    .local v0, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 78
    :cond_0
    const/4 v1, 0x0

    .line 80
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    goto :goto_0
.end method

.method public setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
    .locals 2
    .param p1, "objectContainer"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .prologue
    .line 55
    if-nez p1, :cond_0

    .line 64
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getRotation()F

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;->mRotateAngle:F

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v0, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;->setObjectList(Ljava/util/ArrayList;)V

    goto :goto_0
.end method
