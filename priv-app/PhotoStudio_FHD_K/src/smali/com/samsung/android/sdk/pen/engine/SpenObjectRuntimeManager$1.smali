.class Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager$1;
.super Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;
.source "SpenObjectRuntimeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager$InstallListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager;

    .line 75
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onInstalled(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "pluginType"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 78
    const-string v0, "ObjectRuntime"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager$InstallListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager;)Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager$InstallListener;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager$InstallListener;->onInstalled(Ljava/lang/String;)V

    .line 81
    :cond_0
    return-void
.end method

.method public onUninstalled(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "pluginType"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 85
    const-string v0, "ObjectRuntime"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager$InstallListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager;)Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager$InstallListener;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntimeManager$InstallListener;->onUninstalled(Ljava/lang/String;)V

    .line 88
    :cond_0
    return-void
.end method
