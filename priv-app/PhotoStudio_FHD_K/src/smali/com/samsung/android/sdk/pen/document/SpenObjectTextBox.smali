.class public Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
.super Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.source "SpenObjectTextBox.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BulletParagraphInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$IndentLevelParagraphInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;
    }
.end annotation


# static fields
.field public static final ALIGN_BOTH:I = 0x3

.field public static final ALIGN_CENTER:I = 0x2

.field public static final ALIGN_LEFT:I = 0x0

.field public static final ALIGN_RIGHT:I = 0x1

.field public static final AUTO_FIT_BOTH:I = 0x3

.field public static final AUTO_FIT_HORIZONTAL:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AUTO_FIT_NONE:I = 0x0

.field public static final AUTO_FIT_VERTICAL:I = 0x2

.field public static final BORDER_TYPE_DOT:I = 0x3

.field public static final BORDER_TYPE_NONE:I = 0x0

.field public static final BORDER_TYPE_SHADOW:I = 0x2

.field public static final BORDER_TYPE_SQUARE:I = 0x1

.field public static final BULLET_TYPE_ALPHABET:I = 0x6

.field public static final BULLET_TYPE_ARROW:I = 0x1

.field public static final BULLET_TYPE_CHECKER:I = 0x2

.field public static final BULLET_TYPE_CIRCLED_DIGIT:I = 0x5

.field public static final BULLET_TYPE_DIAMOND:I = 0x3

.field public static final BULLET_TYPE_DIGIT:I = 0x4

.field public static final BULLET_TYPE_NONE:I = 0x0

.field public static final BULLET_TYPE_ROMAN_NUMERAL:I = 0x7

.field public static final CURSOR_POS_END:I = 0x1

.field public static final CURSOR_POS_START:I = 0x0

.field public static final DIRECTION_LEFT_TO_RIGHT:I = 0x0

.field public static final DIRECTION_RIGHT_TO_LEFT:I = 0x1

.field public static final ELLIPSIS_DOTS:I = 0x1

.field public static final ELLIPSIS_NONE:I = 0x0

.field public static final ELLIPSIS_TRIANGLE:I = 0x2

.field public static final GRAVITY_BOTTOM:I = 0x2

.field public static final GRAVITY_CENTER:I = 0x1

.field public static final GRAVITY_TOP:I = 0x0

.field public static final HYPER_TEXT_ADDRESS:I = 0x5

.field public static final HYPER_TEXT_DATE:I = 0x4

.field public static final HYPER_TEXT_EMAIL:I = 0x1

.field public static final HYPER_TEXT_TEL:I = 0x2

.field public static final HYPER_TEXT_UNKNOWN:I = 0x0

.field public static final HYPER_TEXT_URL:I = 0x3

.field public static final IMEACTION_PREVIOUS:I = 0x7

.field public static final IME_ACTION_DONE:I = 0x4

.field public static final IME_ACTION_GO:I = 0x2

.field public static final IME_ACTION_NEXT:I = 0x6

.field public static final IME_ACTION_NONE:I = 0x1

.field public static final IME_ACTION_SEARCH:I = 0x3

.field public static final IME_ACTION_SEND:I = 0x5

.field public static final IME_ACTION_UNSPECIFIED:I = 0x0

.field public static final INPUT_TYPE_DATETIME:I = 0x4

.field public static final INPUT_TYPE_NONE:I = 0x0

.field public static final INPUT_TYPE_NUMBER:I = 0x2

.field public static final INPUT_TYPE_PHONE:I = 0x3

.field public static final INPUT_TYPE_TEXT:I = 0x1

.field public static final LINE_SPACING_PERCENT:I = 0x1

.field public static final LINE_SPACING_PIXEL:I = 0x0

.field public static final STYLE_BOLD:I = 0x1

.field public static final STYLE_ITALIC:I = 0x2

.field public static final STYLE_MASK:I = 0x7

.field public static final STYLE_NONE:I = 0x0

.field public static final STYLE_UNDERLINE:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 832
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 833
    return-void
.end method

.method protected constructor <init>(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 823
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 824
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 859
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 860
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init2(Ljava/lang/String;)Z

    move-result v0

    .line 861
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 862
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 864
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 897
    .local p2, "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 899
    if-eqz p2, :cond_1

    .line 900
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 909
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init3(Ljava/lang/String;Ljava/util/ArrayList;)Z

    move-result v0

    .line 910
    .local v0, "rnt":Z
    if-nez v0, :cond_2

    .line 911
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 913
    .end local v0    # "rnt":Z
    :cond_2
    :goto_0
    return-void

    .line 900
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 901
    .local v1, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v3, :cond_4

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v3, :cond_0

    .line 902
    :cond_4
    const/4 v2, 0x7

    .line 903
    const-string v3, "startPos and endPos of TextSpanInfo should have positive value"

    .line 902
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 965
    .local p2, "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    .local p3, "paras":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 967
    if-eqz p2, :cond_1

    .line 968
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 977
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init4(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v0

    .line 978
    .local v0, "rnt":Z
    if-nez v0, :cond_2

    .line 979
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 982
    .end local v0    # "rnt":Z
    :cond_2
    :goto_0
    return-void

    .line 968
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 969
    .local v1, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v3, :cond_4

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v3, :cond_0

    .line 970
    :cond_4
    const/4 v2, 0x7

    .line 971
    const-string v3, "startPos and endPos of TextSpanInfo should have positive value"

    .line 970
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p4, "isTemplateObject"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1005
    .local p2, "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    .local p3, "paras":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 1007
    if-eqz p2, :cond_1

    .line 1008
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1017
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v0

    .line 1018
    .local v0, "rnt":Z
    if-nez v0, :cond_2

    .line 1019
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1022
    .end local v0    # "rnt":Z
    :cond_2
    :goto_0
    return-void

    .line 1008
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 1009
    .local v1, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v3, :cond_4

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v3, :cond_0

    .line 1010
    :cond_4
    const/4 v2, 0x7

    .line 1011
    const-string v3, "startPos and endPos of TextSpanInfo should have positive value"

    .line 1010
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;Z)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p3, "isTemplateObject"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 930
    .local p2, "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 932
    if-eqz p2, :cond_1

    .line 933
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 942
    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, v2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v0

    .line 943
    .local v0, "rnt":Z
    if-nez v0, :cond_2

    .line 944
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 946
    .end local v0    # "rnt":Z
    :cond_2
    :goto_0
    return-void

    .line 933
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 934
    .local v1, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v3, :cond_4

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v3, :cond_0

    .line 935
    :cond_4
    const/4 v2, 0x7

    .line 936
    const-string v3, "startPos and endPos of TextSpanInfo should have positive value"

    .line 935
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "isTemplateObject"    # Z

    .prologue
    const/4 v2, 0x0

    .line 877
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 878
    invoke-direct {p0, p1, v2, v2, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v0

    .line 879
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 880
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 882
    :cond_0
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3
    .param p1, "isTemplateObject"    # Z

    .prologue
    const/4 v2, 0x0

    .line 843
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 844
    invoke-direct {p0, v2, v2, v2, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v0

    .line 845
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 846
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 848
    :cond_0
    return-void
.end method

.method private native ObjectTextBox_appendParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)Z
.end method

.method private native ObjectTextBox_appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)Z
.end method

.method private native ObjectTextBox_clearChangedFlag()V
.end method

.method private native ObjectTextBox_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native ObjectTextBox_enableReadOnly(Z)Z
.end method

.method private native ObjectTextBox_findParagraphs(II)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;"
        }
    .end annotation
.end method

.method private native ObjectTextBox_findSpans(II)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;"
        }
    .end annotation
.end method

.method private native ObjectTextBox_getAutoFitOption()I
.end method

.method private native ObjectTextBox_getBackgroundColor()I
.end method

.method private native ObjectTextBox_getBorderType()I
.end method

.method private native ObjectTextBox_getBottomMargin()F
.end method

.method private native ObjectTextBox_getBulletType()I
.end method

.method private native ObjectTextBox_getCursorPos()I
.end method

.method private native ObjectTextBox_getDrawnRect()Landroid/graphics/RectF;
.end method

.method private native ObjectTextBox_getEllipsisType()I
.end method

.method private native ObjectTextBox_getFont()Ljava/lang/String;
.end method

.method private native ObjectTextBox_getFontSize()F
.end method

.method private native ObjectTextBox_getGravity()I
.end method

.method private native ObjectTextBox_getHintText()Ljava/lang/String;
.end method

.method private native ObjectTextBox_getHintTextColor()I
.end method

.method private native ObjectTextBox_getHintTextFontSize()F
.end method

.method private native ObjectTextBox_getIMEActionType()I
.end method

.method private native ObjectTextBox_getLeftMargin()F
.end method

.method private native ObjectTextBox_getLineBorderColor()I
.end method

.method private native ObjectTextBox_getLineBorderWidth()F
.end method

.method private native ObjectTextBox_getParagraph()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;"
        }
    .end annotation
.end method

.method private native ObjectTextBox_getRightMargin()F
.end method

.method private native ObjectTextBox_getSpan()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;"
        }
    .end annotation
.end method

.method private native ObjectTextBox_getText()Ljava/lang/String;
.end method

.method private native ObjectTextBox_getTextAlignment()I
.end method

.method private native ObjectTextBox_getTextColor()I
.end method

.method private native ObjectTextBox_getTextDirection()I
.end method

.method private native ObjectTextBox_getTextIndentLevel()I
.end method

.method private native ObjectTextBox_getTextInputType()I
.end method

.method private native ObjectTextBox_getTextLineSpacing()F
.end method

.method private native ObjectTextBox_getTextLineSpacingType()I
.end method

.method private native ObjectTextBox_getTextStyle()I
.end method

.method private native ObjectTextBox_getTopMargin()F
.end method

.method private native ObjectTextBox_getVerticalPan()F
.end method

.method private native ObjectTextBox_init1()Z
.end method

.method private native ObjectTextBox_init2(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_init3(Ljava/lang/String;Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native ObjectTextBox_init4(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;Z)Z"
        }
    .end annotation
.end method

.method private native ObjectTextBox_insertChar(CI)Z
.end method

.method private native ObjectTextBox_insertCharAtCursor(C)Z
.end method

.method private native ObjectTextBox_insertText(Ljava/lang/String;I)Z
.end method

.method private native ObjectTextBox_insertTextAtCursor(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_isChanged()Z
.end method

.method private native ObjectTextBox_isHintTextVisiable()Z
.end method

.method private native ObjectTextBox_isReadOnly()Z
.end method

.method private native ObjectTextBox_parseHyperText()Z
.end method

.method private native ObjectTextBox_removeAllText()Z
.end method

.method private native ObjectTextBox_removeParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)Z
.end method

.method private native ObjectTextBox_removeSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)Z
.end method

.method private native ObjectTextBox_removeText(II)Z
.end method

.method private native ObjectTextBox_replaceText(Ljava/lang/String;II)Z
.end method

.method private native ObjectTextBox_setAutoFitOption(I)Z
.end method

.method private native ObjectTextBox_setBackgroundColor(I)Z
.end method

.method private native ObjectTextBox_setBorderType(I)Z
.end method

.method private native ObjectTextBox_setBulletType(I)Z
.end method

.method private native ObjectTextBox_setCursorPos(I)Z
.end method

.method private native ObjectTextBox_setEllipsisType(I)Z
.end method

.method private native ObjectTextBox_setFont(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_setFontSize(F)Z
.end method

.method private native ObjectTextBox_setGravity(I)Z
.end method

.method private native ObjectTextBox_setHintText(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_setHintTextColor(I)Z
.end method

.method private native ObjectTextBox_setHintTextFontSize(F)Z
.end method

.method private native ObjectTextBox_setHintTextVisibility(Z)Z
.end method

.method private native ObjectTextBox_setIMEActionType(I)Z
.end method

.method private static native ObjectTextBox_setInitialCursorPos(I)Z
.end method

.method private native ObjectTextBox_setLineBorderColor(I)Z
.end method

.method private native ObjectTextBox_setLineBorderWidth(F)Z
.end method

.method private native ObjectTextBox_setMargin(FFFF)Z
.end method

.method private native ObjectTextBox_setParagraph(Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native ObjectTextBox_setSpan(Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native ObjectTextBox_setText(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_setTextAlignment(I)Z
.end method

.method private native ObjectTextBox_setTextColor(I)Z
.end method

.method private native ObjectTextBox_setTextDirection(I)Z
.end method

.method private native ObjectTextBox_setTextIndentLevel(I)Z
.end method

.method private native ObjectTextBox_setTextInputType(I)Z
.end method

.method private native ObjectTextBox_setTextLineSpacingInfo(IF)Z
.end method

.method private native ObjectTextBox_setTextStyle(I)Z
.end method

.method private native ObjectTextBox_setVerticalPan(F)Z
.end method

.method private static parseHyperlink(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 38
    .param p0, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<[I>;"
        }
    .end annotation

    .prologue
    .line 2174
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 2176
    .local v30, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[I>;"
    const-string v7, "\\b(?:(?:[\\w]?[\\d]{1,4}(?:[\\-\\s]\\d[0-9a-zA-Z]{0,3})?)|(?:\\d[0-9a-zA-Z]{0,3}))\\b"

    .line 2177
    .local v7, "REGEX_ADDRESS_STREETNUM":Ljava/lang/String;
    const-string v6, "(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}"

    .line 2178
    .local v6, "REGEX_ADDRESS_SEPARATECITYSTATES":Ljava/lang/String;
    const-string v5, "\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))"

    .line 2179
    .local v5, "REGEX_ADDRESS_POSTALCODE":Ljava/lang/String;
    const-string v3, "(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})"

    .line 2181
    .local v3, "REGEX_ADDRESS_CITYSTATES":Ljava/lang/String;
    const-string v4, "(?:[\\t \\,][\\t ]?(?:[Uu]nited[\\t ])?[a-zA-Z\u00a0-\uaf00\\\'\\.]{2,20}\\b)?"

    .line 2182
    .local v4, "REGEX_ADDRESS_COUNTRY":Ljava/lang/String;
    const-string v8, "(?:(?:Ankara)|(?:Athens)|(?:Atlanta)|(?:Baghdad)|(?:Bandung)|(?:Bangalore)|(?:Bangkok)|(?:Barcelona)|(?:Beijing)|(?:Berlin)|(?:Bombay)|(?:Boston)|(?:Brasillia)|(?:Buenos\\sAires)|(?:Busan)|(?:Cairo)|(?:Calcutta)|(?:Casablandca)|(?:Chicago)|(?:Chongqing)|(?:Dallas)|(?:Delhi)|(?:Detroit)|(?:Dhaka)|(?:Guangzhou)|(?:Hanoi)|(?:Hong\\sKong)|(?:Houston)|(?:Istanbul)|(?:Karachi)|(?:Jakarta)|(?:Kobe)|(?:Lagos)|(?:Lahore)|(?:Lima)|(?:London)|(?:Los\\sAngeles)|(?:Madrid)|(?:Melbourne)|(?:Metro\\sManila)|(?:Mexico\\sCity)|(?:Miami)|(?:Milan)|(?:Montreal)|(?:Moscow)|(?:Mumbai)|(?:New\\sYork)|(?:Osaka)|(?:Paris)|(?:Philadelphia)|(?:Phoenix)|(?:Pusan)|(?:Rio\\sde\\sJaneiro)|(?:Santiago)|(?:Sao\\sPaulo)|(?:Seoul)|(?:Shanghai)|(?:Shenyang)|(?:Singapore)|(?:Sydney)|(?:Tehran)|(?:Tianjin)|(?:Tokyo)|(?:Toronto)|(?:Washington(?:[\\,\\s]{1,2}[Dd][\\.]?[Cc][\\.]?))|(?:Wuhan)|(?:Xi[\\\']?an))"

    .line 2190
    .local v8, "REGEX_ADDRESS_WORLDCITY":Ljava/lang/String;
    const-string v13, "(?<!((\\s[0-9]{1,2}:)|([0-9]{1,2}/)))\\b(?:(?:[\\w]?[\\d]{1,4}(?:[\\-\\s]\\d[0-9a-zA-Z]{0,3})?)|(?:\\d[0-9a-zA-Z]{0,3}))\\b(?:\\s|\\,|(?:\\&nbsp;)){1,3}[a-zA-Z\u00a0-\uaf00](?:[\u00a0-\uaf00\\\'\\w\\s#@\\-\\,\\.]{4,18})(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}(?i:<[^>]+>(?:[\\s\\x0d\\x0a]|(?:\\&nbsp;)){0,2}){0,5}(?:(?:(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})(?:\\s|\\,|(?:\\&nbsp;)){1,3}\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))\\b)|(?:\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))(?:\\s|\\,|(?:\\&nbsp;)){1,3}(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})\\b)|(?:(?:(?:Ankara)|(?:Athens)|(?:Atlanta)|(?:Baghdad)|(?:Bandung)|(?:Bangalore)|(?:Bangkok)|(?:Barcelona)|(?:Beijing)|(?:Berlin)|(?:Bombay)|(?:Boston)|(?:Brasillia)|(?:Buenos\\sAires)|(?:Busan)|(?:Cairo)|(?:Calcutta)|(?:Casablandca)|(?:Chicago)|(?:Chongqing)|(?:Dallas)|(?:Delhi)|(?:Detroit)|(?:Dhaka)|(?:Guangzhou)|(?:Hanoi)|(?:Hong\\sKong)|(?:Houston)|(?:Istanbul)|(?:Karachi)|(?:Jakarta)|(?:Kobe)|(?:Lagos)|(?:Lahore)|(?:Lima)|(?:London)|(?:Los\\sAngeles)|(?:Madrid)|(?:Melbourne)|(?:Metro\\sManila)|(?:Mexico\\sCity)|(?:Miami)|(?:Milan)|(?:Montreal)|(?:Moscow)|(?:Mumbai)|(?:New\\sYork)|(?:Osaka)|(?:Paris)|(?:Philadelphia)|(?:Phoenix)|(?:Pusan)|(?:Rio\\sde\\sJaneiro)|(?:Santiago)|(?:Sao\\sPaulo)|(?:Seoul)|(?:Shanghai)|(?:Shenyang)|(?:Singapore)|(?:Sydney)|(?:Tehran)|(?:Tianjin)|(?:Tokyo)|(?:Toronto)|(?:Washington(?:[\\,\\s]{1,2}[Dd][\\.]?[Cc][\\.]?))|(?:Wuhan)|(?:Xi[\\\']?an))))(?:[\\t \\,][\\t ]?(?:[Uu]nited[\\t ])?[a-zA-Z\u00a0-\uaf00\\\'\\.]{2,20}\\b)?"

    .line 2197
    .local v13, "REGEX_US":Ljava/lang/String;
    const/4 v14, 0x5

    .line 2199
    .local v14, "TYPE_ADDRESS":I
    const-string v10, "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}(?:\\@|(?:\\&\\#[0]*64\\;))[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(?:\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"

    .line 2201
    .local v10, "REGEX_EMAIL":Ljava/lang/String;
    const/16 v16, 0x1

    .line 2211
    .local v16, "TYPE_EMAIL":I
    const-string v9, "(([0-9]{3,4}[/.-][0-9]{1,2}[/.-][0-9]{1,2})|([0-9]{1,2}[.-][0-9]{1,2}[.-][0-9]{2,4}))((\\s[0-9]{1,2}(\\s(AM|Am|am|PM|Pm|pm)))|(\\s[0-9]{1,2}(:[0-9]{1,2})(\\s(AM|Am|am|PM|Pm|pm))?))?"

    .line 2213
    .local v9, "REGEX_DATA_TIME":Ljava/lang/String;
    const/4 v15, 0x4

    .line 2215
    .local v15, "TYPE_DATA_TIME":I
    const-string v11, "(?<!((\\s[0-9]{1,2}:)|([0-9]{1,2}[/.-])))(?:\\+[0-9]{1,3}[\\- \\.]*)?(?:([0-9]{2,5}[\\- \\.]?[0-9]{3,5}[\\- \\.]?[0-9]{3,5})|[0-9]{5,7})"

    .line 2217
    .local v11, "REGEX_TEL":Ljava/lang/String;
    const/16 v17, 0x2

    .line 2232
    .local v17, "TYPE_TEL":I
    const-string v12, "(?i)((https?|ftp)://)?([a-z0-9+!*(),;?&=$_.-]+(:[a-z0-9+!*(),;?&=$_.-]+)?@)?([A-Za-z0-9-.]*)\\.([a-z]{2,3})(:[0-9]{2,5})?(/([A-Za-z0-9?#+$_-].?[A-Za-z0-9(),\'!;:@&%=+/$_.-?]*)+)*/?(#[A-Za-z_.-][A-Za-z0-9+$_.-]*)?(?=([\\u0080-\\uffff]|\\W)|$)"

    .line 2236
    .local v12, "REGEX_URL":Ljava/lang/String;
    const/16 v18, 0x3

    .line 2238
    .local v18, "TYPE_URL":I
    new-instance v28, Ljava/util/LinkedHashMap;

    invoke-direct/range {v28 .. v28}, Ljava/util/LinkedHashMap;-><init>()V

    .line 2240
    .local v28, "regexList":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    const/16 v34, 0x4

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    const-string v35, "(([0-9]{3,4}[/.-][0-9]{1,2}[/.-][0-9]{1,2})|([0-9]{1,2}[.-][0-9]{1,2}[.-][0-9]{2,4}))((\\s[0-9]{1,2}(\\s(AM|Am|am|PM|Pm|pm)))|(\\s[0-9]{1,2}(:[0-9]{1,2})(\\s(AM|Am|am|PM|Pm|pm))?))?"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2241
    const/16 v34, 0x2

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    const-string v35, "(?<!((\\s[0-9]{1,2}:)|([0-9]{1,2}[/.-])))(?:\\+[0-9]{1,3}[\\- \\.]*)?(?:([0-9]{2,5}[\\- \\.]?[0-9]{3,5}[\\- \\.]?[0-9]{3,5})|[0-9]{5,7})"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2242
    const/16 v34, 0x1

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    const-string v35, "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}(?:\\@|(?:\\&\\#[0]*64\\;))[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(?:\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2243
    const/16 v34, 0x3

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    const-string v35, "(?i)((https?|ftp)://)?([a-z0-9+!*(),;?&=$_.-]+(:[a-z0-9+!*(),;?&=$_.-]+)?@)?([A-Za-z0-9-.]*)\\.([a-z]{2,3})(:[0-9]{2,5})?(/([A-Za-z0-9?#+$_-].?[A-Za-z0-9(),\'!;:@&%=+/$_.-?]*)+)*/?(#[A-Za-z_.-][A-Za-z0-9+$_.-]*)?(?=([\\u0080-\\uffff]|\\W)|$)"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2244
    const/16 v34, 0x5

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    const-string v35, "(?<!((\\s[0-9]{1,2}:)|([0-9]{1,2}/)))\\b(?:(?:[\\w]?[\\d]{1,4}(?:[\\-\\s]\\d[0-9a-zA-Z]{0,3})?)|(?:\\d[0-9a-zA-Z]{0,3}))\\b(?:\\s|\\,|(?:\\&nbsp;)){1,3}[a-zA-Z\u00a0-\uaf00](?:[\u00a0-\uaf00\\\'\\w\\s#@\\-\\,\\.]{4,18})(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}(?i:<[^>]+>(?:[\\s\\x0d\\x0a]|(?:\\&nbsp;)){0,2}){0,5}(?:(?:(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})(?:\\s|\\,|(?:\\&nbsp;)){1,3}\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))\\b)|(?:\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))(?:\\s|\\,|(?:\\&nbsp;)){1,3}(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})\\b)|(?:(?:(?:Ankara)|(?:Athens)|(?:Atlanta)|(?:Baghdad)|(?:Bandung)|(?:Bangalore)|(?:Bangkok)|(?:Barcelona)|(?:Beijing)|(?:Berlin)|(?:Bombay)|(?:Boston)|(?:Brasillia)|(?:Buenos\\sAires)|(?:Busan)|(?:Cairo)|(?:Calcutta)|(?:Casablandca)|(?:Chicago)|(?:Chongqing)|(?:Dallas)|(?:Delhi)|(?:Detroit)|(?:Dhaka)|(?:Guangzhou)|(?:Hanoi)|(?:Hong\\sKong)|(?:Houston)|(?:Istanbul)|(?:Karachi)|(?:Jakarta)|(?:Kobe)|(?:Lagos)|(?:Lahore)|(?:Lima)|(?:London)|(?:Los\\sAngeles)|(?:Madrid)|(?:Melbourne)|(?:Metro\\sManila)|(?:Mexico\\sCity)|(?:Miami)|(?:Milan)|(?:Montreal)|(?:Moscow)|(?:Mumbai)|(?:New\\sYork)|(?:Osaka)|(?:Paris)|(?:Philadelphia)|(?:Phoenix)|(?:Pusan)|(?:Rio\\sde\\sJaneiro)|(?:Santiago)|(?:Sao\\sPaulo)|(?:Seoul)|(?:Shanghai)|(?:Shenyang)|(?:Singapore)|(?:Sydney)|(?:Tehran)|(?:Tianjin)|(?:Tokyo)|(?:Toronto)|(?:Washington(?:[\\,\\s]{1,2}[Dd][\\.]?[Cc][\\.]?))|(?:Wuhan)|(?:Xi[\\\']?an))))(?:[\\t \\,][\\t ]?(?:[Uu]nited[\\t ])?[a-zA-Z\u00a0-\uaf00\\\'\\.]{2,20}\\b)?"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2246
    invoke-virtual/range {v28 .. v28}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v34

    invoke-interface/range {v34 .. v34}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v35

    :cond_0
    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->hasNext()Z

    move-result v34

    if-nez v34, :cond_1

    .line 2322
    return-object v30

    .line 2246
    :cond_1
    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/util/Map$Entry;

    .line 2247
    .local v29, "regexSet":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    invoke-interface/range {v29 .. v29}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    .line 2248
    .local v23, "key":Ljava/lang/Integer;
    invoke-interface/range {v29 .. v29}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    invoke-static/range {v34 .. v34}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v26

    .line 2249
    .local v26, "p":Ljava/util/regex/Pattern;
    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v24

    .line 2250
    .local v24, "m":Ljava/util/regex/Matcher;
    :cond_2
    :goto_0
    invoke-virtual/range {v24 .. v24}, Ljava/util/regex/Matcher;->find()Z

    move-result v34

    if-eqz v34, :cond_0

    .line 2251
    const/16 v34, 0x3

    move/from16 v0, v34

    new-array v0, v0, [I

    move-object/from16 v20, v0

    .line 2252
    .local v20, "hyperlink":[I
    const/16 v34, 0x0

    invoke-virtual/range {v24 .. v24}, Ljava/util/regex/Matcher;->start()I

    move-result v36

    aput v36, v20, v34

    .line 2253
    const/16 v34, 0x1

    invoke-virtual/range {v24 .. v24}, Ljava/util/regex/Matcher;->end()I

    move-result v36

    aput v36, v20, v34

    .line 2254
    const/16 v34, 0x2

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v36

    aput v36, v20, v34

    .line 2259
    const/16 v19, 0x0

    .line 2260
    .local v19, "contain":Z
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_1
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v21

    move/from16 v1, v34

    if-lt v0, v1, :cond_5

    .line 2311
    :cond_3
    :goto_2
    if-nez v19, :cond_2

    .line 2312
    const/16 v34, 0x1

    aget v34, v20, v34

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-le v0, v1, :cond_4

    .line 2313
    const/16 v34, 0x1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v36

    aput v36, v20, v34

    .line 2315
    :cond_4
    const-string v34, "Model_ObjectText"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "Add hyperlink - type["

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v37, 0x2

    aget v37, v20, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "] startPos["

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const/16 v37, 0x0

    aget v37, v20, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    .line 2316
    const-string v37, "] endPos["

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const/16 v37, 0x1

    aget v37, v20, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "]"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    .line 2315
    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2317
    move-object/from16 v0, v30

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2261
    :cond_5
    move-object/from16 v0, v30

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, [I

    .line 2263
    .local v32, "v":[I
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x3

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_6

    const/16 v34, 0x2

    aget v34, v32, v34

    const/16 v36, 0x1

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_6

    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_6

    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_6

    .line 2264
    const/16 v19, 0x1

    .line 2265
    goto/16 :goto_2

    .line 2266
    :cond_6
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x5

    move/from16 v0, v34

    move/from16 v1, v36

    if-eq v0, v1, :cond_7

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x3

    move/from16 v0, v34

    move/from16 v1, v36

    if-eq v0, v1, :cond_7

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x1

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_9

    :cond_7
    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-gt v0, v1, :cond_9

    .line 2267
    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-lt v0, v1, :cond_9

    .line 2269
    const-string v34, "Model_ObjectText"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "Remove contain hyperlink - type["

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v37, 0x2

    aget v37, v32, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "] startPos["

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const/16 v37, 0x0

    aget v37, v32, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    .line 2270
    const-string v37, "] endPos["

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const/16 v37, 0x1

    aget v37, v32, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "]"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    .line 2269
    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2271
    move-object/from16 v0, v30

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2272
    add-int/lit8 v21, v21, -0x1

    .line 2260
    :cond_8
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_1

    .line 2273
    :cond_9
    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-lt v0, v1, :cond_a

    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-le v0, v1, :cond_b

    .line 2274
    :cond_a
    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-lt v0, v1, :cond_8

    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-gt v0, v1, :cond_8

    .line 2276
    :cond_b
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x5

    move/from16 v0, v34

    move/from16 v1, v36

    if-eq v0, v1, :cond_c

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x2

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_f

    :cond_c
    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-ge v0, v1, :cond_f

    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-le v0, v1, :cond_f

    .line 2277
    const/16 v34, 0x1

    aget v34, v32, v34

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v36

    add-int/lit8 v36, v36, -0x1

    move/from16 v0, v34

    move/from16 v1, v36

    if-ge v0, v1, :cond_f

    .line 2278
    const/16 v34, 0x1

    aget v34, v32, v34

    add-int/lit8 v34, v34, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v31

    .line 2279
    .local v31, "subStr":Ljava/lang/String;
    invoke-interface/range {v29 .. v29}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    invoke-static/range {v34 .. v34}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v27

    .line 2280
    .local v27, "p1":Ljava/util/regex/Pattern;
    move-object/from16 v0, v27

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v25

    .line 2281
    .local v25, "m1":Ljava/util/regex/Matcher;
    invoke-virtual/range {v25 .. v25}, Ljava/util/regex/Matcher;->find()Z

    move-result v34

    if-eqz v34, :cond_e

    .line 2282
    const/16 v34, 0x0

    invoke-virtual/range {v25 .. v25}, Ljava/util/regex/Matcher;->start()I

    move-result v36

    const/16 v37, 0x1

    aget v37, v32, v37

    add-int v36, v36, v37

    add-int/lit8 v36, v36, 0x1

    aput v36, v20, v34

    .line 2283
    const/16 v34, 0x1

    invoke-virtual/range {v25 .. v25}, Ljava/util/regex/Matcher;->end()I

    move-result v36

    const/16 v37, 0x1

    aget v37, v32, v37

    add-int v36, v36, v37

    add-int/lit8 v36, v36, 0x1

    aput v36, v20, v34

    .line 2285
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x5

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_3

    .line 2286
    const/16 v22, 0x0

    .local v22, "j":I
    :goto_3
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v22

    move/from16 v1, v34

    if-ge v0, v1, :cond_3

    .line 2287
    move-object/from16 v0, v30

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v33

    check-cast v33, [I

    .line 2288
    .local v33, "v1":[I
    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v33, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-gt v0, v1, :cond_d

    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v33, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-lt v0, v1, :cond_d

    .line 2289
    const-string v34, "Model_ObjectText"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "Remove overlap hyperlink - type["

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v37, 0x2

    aget v37, v33, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    .line 2290
    const-string v37, "] startPos["

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const/16 v37, 0x0

    aget v37, v33, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "] endPos["

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const/16 v37, 0x1

    aget v37, v33, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "]"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    .line 2289
    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2291
    move-object/from16 v0, v30

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2292
    add-int/lit8 v22, v22, -0x1

    .line 2286
    :cond_d
    add-int/lit8 v22, v22, 0x1

    goto :goto_3

    .line 2298
    .end local v22    # "j":I
    .end local v33    # "v1":[I
    :cond_e
    const/16 v19, 0x1

    .line 2299
    goto/16 :goto_2

    .line 2303
    .end local v25    # "m1":Ljava/util/regex/Matcher;
    .end local v27    # "p1":Ljava/util/regex/Pattern;
    .end local v31    # "subStr":Ljava/lang/String;
    :cond_f
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x5

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_10

    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-le v0, v1, :cond_10

    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-ge v0, v1, :cond_10

    .line 2304
    const/16 v34, 0x1

    const/16 v36, 0x0

    aget v36, v32, v36

    add-int/lit8 v36, v36, -0x1

    aput v36, v20, v34

    goto/16 :goto_2

    .line 2307
    :cond_10
    const/16 v19, 0x1

    .line 2308
    goto/16 :goto_2
.end method

.method public static setInitialCursorPos(I)V
    .locals 4
    .param p0, "initialCursorPos"    # I

    .prologue
    .line 2376
    if-eqz p0, :cond_0

    const/4 v2, 0x1

    if-eq p0, v2, :cond_0

    .line 2377
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "E_INVALID_ARG"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2380
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setInitialCursorPos(I)Z

    move-result v1

    .line 2381
    .local v1, "rnt":Z
    if-nez v1, :cond_1

    .line 2382
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "E_INVALID_ARG"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2384
    .end local v1    # "rnt":Z
    :catch_0
    move-exception v0

    .line 2385
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2386
    const-string v2, "SpenObjectTextBox"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2388
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :cond_1
    return-void
.end method

.method private throwUncheckedException(I)V
    .locals 3
    .param p1, "errno"    # I

    .prologue
    .line 2391
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    .line 2392
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenObjectTextBox("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2394
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2396
    return-void
.end method


# virtual methods
.method public appendParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V
    .locals 2
    .param p1, "paragraph"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;

    .prologue
    .line 1338
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_appendParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)Z

    move-result v0

    .line 1339
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1340
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1342
    :cond_0
    return-void
.end method

.method public appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V
    .locals 2
    .param p1, "span"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .prologue
    .line 1264
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)Z

    move-result v0

    .line 1265
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1266
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1268
    :cond_0
    return-void
.end method

.method public clearChangedFlag()V
    .locals 0

    .prologue
    .line 2330
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_clearChangedFlag()V

    .line 2331
    return-void
.end method

.method public copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2
    .param p1, "source"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 2354
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    .line 2355
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2356
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2358
    :cond_0
    return-void
.end method

.method public findParagraphs(II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "startPos"    # I
    .param p2, "endPos"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1310
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_findParagraphs(II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public findSpans(II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "startPos"    # I
    .param p2, "endPos"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1235
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_findSpans(II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getAutoFitOption()I
    .locals 1

    .prologue
    .line 1588
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getAutoFitOption()I

    move-result v0

    return v0
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 1692
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getBackgroundColor()I

    move-result v0

    return v0
.end method

.method public getBorderType()I
    .locals 1

    .prologue
    .line 1481
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getBorderType()I

    move-result v0

    return v0
.end method

.method public getBottomMargin()F
    .locals 1

    .prologue
    .line 1401
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getBottomMargin()F

    move-result v0

    return v0
.end method

.method public getBulletType()I
    .locals 1

    .prologue
    .line 1995
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getBulletType()I

    move-result v0

    return v0
.end method

.method public getCursorPos()I
    .locals 1

    .prologue
    .line 1180
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getCursorPos()I

    move-result v0

    return v0
.end method

.method public getDrawnRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 2346
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getDrawnRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getEllipsisType()I
    .locals 1

    .prologue
    .line 2170
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getEllipsisType()I

    move-result v0

    return v0
.end method

.method public getFont()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1759
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getFont()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFontSize()F
    .locals 1

    .prologue
    .line 1725
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getFontSize()F

    move-result v0

    return v0
.end method

.method public getGravity()I
    .locals 1

    .prologue
    .line 1627
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getGravity()I

    move-result v0

    return v0
.end method

.method public getHintText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1532
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getHintText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHintTextColor()I
    .locals 1

    .prologue
    .line 2019
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getHintTextColor()I

    move-result v0

    return v0
.end method

.method public getHintTextFontSize()F
    .locals 1

    .prologue
    .line 2046
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getHintTextFontSize()F

    move-result v0

    return v0
.end method

.method public getIMEActionType()I
    .locals 1

    .prologue
    .line 2075
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getIMEActionType()I

    move-result v0

    return v0
.end method

.method public getLeftMargin()F
    .locals 1

    .prologue
    .line 1371
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getLeftMargin()F

    move-result v0

    return v0
.end method

.method public getLineBorderColor()I
    .locals 1

    .prologue
    .line 1425
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getLineBorderColor()I

    move-result v0

    return v0
.end method

.method public getLineBorderWidth()F
    .locals 1

    .prologue
    .line 1452
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getLineBorderWidth()F

    move-result v0

    return v0
.end method

.method public getParagraph()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1296
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getParagraph()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getRightMargin()F
    .locals 1

    .prologue
    .line 1391
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getRightMargin()F

    move-result v0

    return v0
.end method

.method public getSpan()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1220
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getSpan()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1056
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextAlignment()I
    .locals 1

    .prologue
    .line 1868
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextAlignment()I

    move-result v0

    return v0
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 1660
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextColor()I

    move-result v0

    return v0
.end method

.method public getTextDirection()I
    .locals 1

    .prologue
    .line 1795
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextDirection()I

    move-result v0

    return v0
.end method

.method public getTextIndentLevel()I
    .locals 1

    .prologue
    .line 1957
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextIndentLevel()I

    move-result v0

    return v0
.end method

.method public getTextInputType()I
    .locals 1

    .prologue
    .line 2103
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextInputType()I

    move-result v0

    return v0
.end method

.method public getTextLineSpacing()F
    .locals 1

    .prologue
    .line 1919
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextLineSpacing()F

    move-result v0

    return v0
.end method

.method public getTextLineSpacingType()I
    .locals 1

    .prologue
    .line 1905
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextLineSpacingType()I

    move-result v0

    return v0
.end method

.method public getTextStyle()I
    .locals 1

    .prologue
    .line 1835
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextStyle()I

    move-result v0

    return v0
.end method

.method public getTopMargin()F
    .locals 1

    .prologue
    .line 1381
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTopMargin()F

    move-result v0

    return v0
.end method

.method public getVerticalPan()F
    .locals 1

    .prologue
    .line 2143
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getVerticalPan()F

    move-result v0

    return v0
.end method

.method public insertText(Ljava/lang/String;I)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "position"    # I

    .prologue
    .line 1075
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_insertText(Ljava/lang/String;I)Z

    move-result v0

    .line 1076
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1077
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1079
    :cond_0
    return-void
.end method

.method public insertTextAtCursor(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1093
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_insertTextAtCursor(Ljava/lang/String;)Z

    move-result v0

    .line 1094
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1095
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1097
    :cond_0
    return-void
.end method

.method public isChanged()Z
    .locals 1

    .prologue
    .line 2338
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_isChanged()Z

    move-result v0

    return v0
.end method

.method public isHintTextEnabled()Z
    .locals 1

    .prologue
    .line 1556
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_isHintTextVisiable()Z

    move-result v0

    return v0
.end method

.method public isReadOnlyEnabled()Z
    .locals 1

    .prologue
    .line 1508
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_isReadOnly()Z

    move-result v0

    return v0
.end method

.method public parseHyperText()V
    .locals 2

    .prologue
    .line 1597
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_parseHyperText()Z

    move-result v0

    .line 1598
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1599
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1602
    :cond_0
    return-void
.end method

.method public removeAllText()V
    .locals 2

    .prologue
    .line 1125
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_removeAllText()Z

    move-result v0

    .line 1126
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1127
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1129
    :cond_0
    return-void
.end method

.method public removeParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V
    .locals 2
    .param p1, "paragraph"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;

    .prologue
    .line 1323
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_removeParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)Z

    move-result v0

    .line 1324
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1325
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1327
    :cond_0
    return-void
.end method

.method public removeSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V
    .locals 2
    .param p1, "span"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .prologue
    .line 1249
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_removeSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)Z

    move-result v0

    .line 1250
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1251
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1253
    :cond_0
    return-void
.end method

.method public removeText(II)V
    .locals 2
    .param p1, "startPosition"    # I
    .param p2, "length"    # I

    .prologue
    .line 1113
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_removeText(II)Z

    move-result v0

    .line 1114
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1115
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1117
    :cond_0
    return-void
.end method

.method public replaceText(Ljava/lang/String;II)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "startPosition"    # I
    .param p3, "length"    # I

    .prologue
    .line 1146
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_replaceText(Ljava/lang/String;II)Z

    move-result v0

    .line 1147
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1148
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1150
    :cond_0
    return-void
.end method

.method public setAutoFitOption(I)V
    .locals 2
    .param p1, "textAutoFit"    # I

    .prologue
    .line 1573
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setAutoFitOption(I)Z

    move-result v0

    .line 1574
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1575
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1577
    :cond_0
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 1675
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setBackgroundColor(I)Z

    move-result v0

    .line 1676
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1677
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1679
    :cond_0
    return-void
.end method

.method public setBorderType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 1467
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setBorderType(I)Z

    move-result v0

    .line 1468
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1469
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1472
    :cond_0
    return-void
.end method

.method public setBulletType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 1976
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setBulletType(I)Z

    move-result v0

    .line 1977
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1978
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1980
    :cond_0
    return-void
.end method

.method public setCursorPos(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1166
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setCursorPos(I)Z

    move-result v0

    .line 1167
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1168
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1170
    :cond_0
    return-void
.end method

.method public setEllipsisType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 2157
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setEllipsisType(I)Z

    move-result v0

    .line 2158
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2159
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2161
    :cond_0
    return-void
.end method

.method public setFont(Ljava/lang/String;)V
    .locals 2
    .param p1, "font"    # Ljava/lang/String;

    .prologue
    .line 1741
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setFont(Ljava/lang/String;)Z

    move-result v0

    .line 1742
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1743
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1745
    :cond_0
    return-void
.end method

.method public setFontSize(F)V
    .locals 2
    .param p1, "size"    # F

    .prologue
    .line 1708
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setFontSize(F)Z

    move-result v0

    .line 1709
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1710
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1712
    :cond_0
    return-void
.end method

.method public setGravity(I)V
    .locals 2
    .param p1, "textGravity"    # I

    .prologue
    .line 1614
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setGravity(I)Z

    move-result v0

    .line 1615
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1616
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1618
    :cond_0
    return-void
.end method

.method public setHintText(Ljava/lang/String;)V
    .locals 2
    .param p1, "hintText"    # Ljava/lang/String;

    .prologue
    .line 1519
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setHintText(Ljava/lang/String;)Z

    move-result v0

    .line 1520
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1521
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1523
    :cond_0
    return-void
.end method

.method public setHintTextColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 2006
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setHintTextColor(I)Z

    move-result v0

    .line 2007
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2008
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2010
    :cond_0
    return-void
.end method

.method public setHintTextEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 1543
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setHintTextVisibility(Z)Z

    move-result v0

    .line 1544
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1545
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1547
    :cond_0
    return-void
.end method

.method public setHintTextFontSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 2033
    int-to-float v1, p1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setHintTextFontSize(F)Z

    move-result v0

    .line 2034
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2035
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2037
    :cond_0
    return-void
.end method

.method public setIMEActionType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 2062
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setIMEActionType(I)Z

    move-result v0

    .line 2063
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2064
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2066
    :cond_0
    return-void
.end method

.method public setLineBorderColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 1412
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setLineBorderColor(I)Z

    move-result v0

    .line 1413
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1414
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1416
    :cond_0
    return-void
.end method

.method public setLineBorderWidth(F)V
    .locals 2
    .param p1, "width"    # F

    .prologue
    .line 1439
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setLineBorderWidth(F)Z

    move-result v0

    .line 1440
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1441
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1443
    :cond_0
    return-void
.end method

.method public setMargin(FFFF)V
    .locals 2
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "right"    # F
    .param p4, "bottom"    # F

    .prologue
    .line 1358
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setMargin(FFFF)Z

    move-result v0

    .line 1359
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1360
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1362
    :cond_0
    return-void
.end method

.method public setParagraph(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1283
    .local p1, "paragraph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setParagraph(Ljava/util/ArrayList;)Z

    move-result v0

    .line 1284
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1285
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1287
    :cond_0
    return-void
.end method

.method public setReadOnlyEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 1492
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_enableReadOnly(Z)Z

    move-result v0

    .line 1493
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1494
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1496
    :cond_0
    return-void
.end method

.method public setRect(Landroid/graphics/RectF;Z)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "regionOnly"    # Z

    .prologue
    .line 1032
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 1033
    return-void
.end method

.method public setSpan(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1197
    .local p1, "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz p1, :cond_1

    .line 1198
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1207
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setSpan(Ljava/util/ArrayList;)Z

    move-result v0

    .line 1208
    .local v0, "rnt":Z
    if-nez v0, :cond_2

    .line 1209
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1211
    .end local v0    # "rnt":Z
    :cond_2
    :goto_0
    return-void

    .line 1198
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 1199
    .local v1, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v3, :cond_4

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v3, :cond_0

    .line 1200
    :cond_4
    const/4 v2, 0x7

    .line 1201
    const-string v3, "startPos and endPos of TextSpanInfo should have positive value"

    .line 1200
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1043
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setText(Ljava/lang/String;)Z

    move-result v0

    .line 1044
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1045
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1047
    :cond_0
    return-void
.end method

.method public setTextAlignment(I)V
    .locals 2
    .param p1, "align"    # I

    .prologue
    .line 1851
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextAlignment(I)Z

    move-result v0

    .line 1852
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1853
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1855
    :cond_0
    return-void
.end method

.method public setTextColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 1642
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextColor(I)Z

    move-result v0

    .line 1643
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1644
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1646
    :cond_0
    return-void
.end method

.method public setTextDirection(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 1776
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextDirection(I)Z

    move-result v0

    .line 1777
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1778
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1780
    :cond_0
    return-void
.end method

.method public setTextIndentLevel(I)V
    .locals 2
    .param p1, "indentLevel"    # I

    .prologue
    .line 1938
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextIndentLevel(I)Z

    move-result v0

    .line 1939
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1940
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1942
    :cond_0
    return-void
.end method

.method public setTextInputType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 2090
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextInputType(I)Z

    move-result v0

    .line 2091
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2092
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2094
    :cond_0
    return-void
.end method

.method public setTextLineSpacingInfo(IF)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "lineSpacing"    # F

    .prologue
    .line 1885
    const/4 v1, 0x0

    cmpg-float v1, p2, v1

    if-gez v1, :cond_0

    .line 1886
    const/4 v1, 0x7

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1888
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextLineSpacingInfo(IF)Z

    move-result v0

    .line 1889
    .local v0, "rnt":Z
    if-nez v0, :cond_1

    .line 1890
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1892
    :cond_1
    return-void
.end method

.method public setTextStyle(I)V
    .locals 3
    .param p1, "style"    # I

    .prologue
    .line 1815
    and-int/lit8 v1, p1, 0x7

    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    .line 1816
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "style is invalid"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1818
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextStyle(I)Z

    move-result v0

    .line 1819
    .local v0, "rnt":Z
    if-nez v0, :cond_1

    .line 1820
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1822
    :cond_1
    return-void
.end method

.method public setVereticalPan(F)V
    .locals 2
    .param p1, "position"    # F
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2116
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setVerticalPan(F)Z

    move-result v0

    .line 2117
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2118
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2120
    :cond_0
    return-void
.end method

.method public setVerticalPan(F)V
    .locals 2
    .param p1, "position"    # F

    .prologue
    .line 2130
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setVerticalPan(F)Z

    move-result v0

    .line 2131
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2132
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2134
    :cond_0
    return-void
.end method
