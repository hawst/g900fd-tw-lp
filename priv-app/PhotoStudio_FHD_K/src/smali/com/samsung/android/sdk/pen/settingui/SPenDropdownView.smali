.class Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;
.super Ljava/lang/Object;
.source "SPenDropdownView.java"


# instance fields
.field protected final anchor:Landroid/view/View;

.field private background:Landroid/graphics/drawable/Drawable;

.field private root:Landroid/view/View;

.field protected final window:Landroid/widget/PopupWindow;

.field protected final windowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "anchor"    # Landroid/view/View;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->anchor:Landroid/view/View;

    .line 23
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    .line 24
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->windowManager:Landroid/view/WindowManager;

    .line 26
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->onCreate()V

    .line 27
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 73
    return-void
.end method

.method protected onCreate()V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 41
    return-void
.end method

.method protected setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->background:Landroid/graphics/drawable/Drawable;

    .line 65
    return-void
.end method

.method protected setContentView(Landroid/view/View;)V
    .locals 1
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->root:Landroid/view/View;

    .line 60
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 61
    return-void
.end method

.method public setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/PopupWindow$OnDismissListener;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 69
    return-void
.end method

.method protected show()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->root:Landroid/view/View;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "illegalStateException preShow"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->background:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->background:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 53
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 55
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->root:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 56
    return-void
.end method
