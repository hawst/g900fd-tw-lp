.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$7;
.super Ljava/lang/Object;
.source "SpenTextBox.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenTextBox;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 514
    sget-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverCursorEnable:Z

    if-eqz v0, :cond_0

    .line 515
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    .line 516
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCustomHoveringIcon()I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$70(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$71(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V

    .line 521
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 517
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 518
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverIconID:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$72(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeHoveringIcon(I)Z
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$73(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)Z

    goto :goto_0
.end method
