.class public Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
.super Landroid/widget/LinearLayout;
.source "SpenSettingTextLayout.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;
    }
.end annotation


# static fields
.field private static final BODY_LAYOUT_HEIGHT:I = 0xfe

.field private static final BOTTOM_LAYOUT_HEIGHT:I = 0x1a

.field private static final EXIT_BUTTON_HEIGHT:I = 0x24

.field private static EXIT_BUTTON_WIDTH:I = 0x0

.field private static final GRADIENT_LAYOUT_HEIGHT:I = 0x34

.field private static final GRAY_PICKER_HEIGHT:I = 0x44

.field private static final GRAY_SCALE_HEIGHT:I = 0xd

.field private static final LL_VERSION_CODE:I = 0x15

.field private static final PICKER_LAYOUT_HEIGHT:I = 0x8c

.field private static final TAG:Ljava/lang/String; = "settingui-settingText"

.field private static final TEXT_SETTING_BOLD:I = 0x2

.field private static final TEXT_SETTING_ITALIC:I = 0x3

.field private static final TEXT_SETTING_UNDERLINE:I = 0x4

.field private static final TITLE_LAYOUT_HEIGHT:I = 0x29

.field private static final TOTAL_LAYOUT_WIDTH:I = 0xf7

.field public static final VIEW_MODE_COLOR:I = 0x4

.field public static final VIEW_MODE_MINIMUM:I = 0x1

.field public static final VIEW_MODE_MINIMUM_WITHOUT_PREVIEW:I = 0x2

.field public static final VIEW_MODE_NORMAL:I = 0x0

.field public static final VIEW_MODE_PARAGRAPH:I = 0x5

.field public static final VIEW_MODE_STYLE:I = 0x3

.field public static final VIEW_MODE_TITLE:I = 0x6

.field private static final bottomExpandPath:Ljava/lang/String; = "snote_popup_bg_expand"

.field private static final bottomExpandPressPath:Ljava/lang/String; = "snote_popup_bg_expand_press"

.field private static final bottomHandlePath:Ljava/lang/String; = "snote_popup_handler"

.field private static final exitPath:Ljava/lang/String; = "snote_popup_close"

.field private static final exitPressPath:Ljava/lang/String; = "snote_popup_close_press"

.field private static final exitfocusPath:Ljava/lang/String; = "snote_popup_close_focus"

.field private static final grayBodyLeftPath:Ljava/lang/String; = "snote_popup_bg02_left"

.field private static final grayBodyRightPath:Ljava/lang/String; = "snote_popup_bg02_right"

.field private static final lefBgDimPath:Ljava/lang/String; = "snote_popup_arrow_left_dim"

.field private static final lefBgFocusPath:Ljava/lang/String; = "snote_popup_arrow_left_focus"

.field private static final lefBgPressPath:Ljava/lang/String; = "snote_popup_arrow_left_press"

.field private static final leftBgPath:Ljava/lang/String; = "snote_popup_arrow_left_normal"

.field private static final lightBodyLeftPath:Ljava/lang/String; = "snote_popup_bg_left"

.field private static final lightBodyRightPath:Ljava/lang/String; = "snote_popup_bg_right"

.field protected static final mBoldIconPath:Ljava/lang/String; = "snote_popup_textoption_bold"

.field private static final mBoldLeftFocusPath:Ljava/lang/String; = "snote_popup_option_btn_left_focus"

.field private static final mBoldLeftNomarPath:Ljava/lang/String; = "snote_popup_option_btn_left_normal"

.field private static final mBoldLeftPressPath:Ljava/lang/String; = "snote_popup_option_btn_left_press_1"

.field private static final mButtonBgPath:Ljava/lang/String; = "snote_popup_btn_normal"

.field private static final mButtonBgPressPath:Ljava/lang/String; = "snote_popup_btn_press"

.field private static final mCenterIconPressPath:Ljava/lang/String;

.field private static final mCenterIocnPath:Ljava/lang/String;

.field private static final mDefaultPath:Ljava/lang/String; = ""

.field private static final mDropdownFocusPath:Ljava/lang/String; = "snote_dropdown_focused"

.field private static final mDropdownNormalPath:Ljava/lang/String; = "snote_dropdown_normal"

.field private static final mDropdownPressPath:Ljava/lang/String; = "snote_dropdown_pressed"

.field private static final mItalicIconPath:Ljava/lang/String; = "snote_popup_textoption_italic"

.field private static final mItalicLeftFocusPath:Ljava/lang/String; = "snote_popup_option_btn_center_focus"

.field private static final mItalicLeftNomarPath:Ljava/lang/String; = "snote_popup_option_btn_center_normal"

.field private static final mItalicLeftPressPath:Ljava/lang/String; = "snote_popup_option_btn_center_press"

.field private static final mLeftIconPressPath:Ljava/lang/String;

.field private static final mLeftIndentIconPath:Ljava/lang/String;

.field private static final mOptionBgPath:Ljava/lang/String;

.field protected static final mPreviewBgPath:Ljava/lang/String; = "snote_popup_preview_bg"

.field private static final mRightIconPath:Ljava/lang/String;

.field private static final mRightIconPressPath:Ljava/lang/String;

.field private static final mRightIndentIconPath:Ljava/lang/String;

.field private static final mScrollHandelNormal:Ljava/lang/String;

.field private static mSdkVersion:I = 0x0

.field private static final mUnderLeftFocusPath:Ljava/lang/String; = "snote_popup_option_btn_right_focus"

.field private static final mUnderLeftNomarPath:Ljava/lang/String; = "snote_popup_option_btn_right_normal"

.field private static final mUnderLeftPressPath:Ljava/lang/String; = "snote_popup_option_btn_right_press_1"

.field private static final mUnderLineIconPath:Ljava/lang/String; = "snote_popup_textoption_underline"

.field private static minHeight:I = 0x0

.field private static minHeightNormal:I = 0x0

.field private static final rightBgDimPath:Ljava/lang/String; = "snote_popup_arrow_right_dim"

.field private static final rightBgFocusPath:Ljava/lang/String; = "snote_popup_arrow_right_focus"

.field private static final rightBgPath:Ljava/lang/String; = "snote_popup_arrow_right_normal"

.field private static final rightBgPressPath:Ljava/lang/String; = "snote_popup_arrow_right_press"

.field private static final titleCenterPath:Ljava/lang/String; = "snote_popup_title_center"

.field private static final titleLeftPath:Ljava/lang/String; = "snote_popup_title_left"

.field private static final titleRightIndicatorPath:Ljava/lang/String; = "snote_popup_title_bended"

.field private static final titleRightPath:Ljava/lang/String; = "snote_popup_title_right"


# instance fields
.field private EXIT_BUTTON_RIGHT_MARGIN:I

.field private EXIT_BUTTON_TOP_MARGIN:I

.field private final arrImageView:[Landroid/view/View;

.field private deltaOfFirstTime:I

.field private deltaPreview:I

.field private final handlerRotate:Landroid/os/Handler;

.field private isChangePosition:Z

.field private isFirstTime:Z

.field private final localDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

.field private mAlignCenterBtn:Landroid/widget/ImageButton;

.field private mAlignLeftBtn:Landroid/widget/ImageButton;

.field private mAlignRightBtn:Landroid/widget/ImageButton;

.field private mBodyBg:Landroid/view/View;

.field private mBodyLayout:Landroid/view/View;

.field private mBodyLayoutHeight:I

.field private mBoldBtn:Landroid/widget/ImageButton;

.field private mBottomExtendBg:Landroid/widget/ImageView;

.field private mBottomLayout:Landroid/view/View;

.field private mCanvasLayout:Landroid/widget/RelativeLayout;

.field private mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

.field private mCanvasWidth:I

.field private mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

.field private mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

.field private mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

.field private mColorPickerColor:Landroid/view/View;

.field private final mColorPickerColorChangeListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

.field private final mColorPickerColorListener:Landroid/view/View$OnClickListener;

.field private mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

.field private mColorPickerSettingExitButton:Landroid/view/View;

.field private mColorSelectedAndPicker:Landroid/view/View;

.field private mCurrentFontName:Ljava/lang/String;

.field private mCurrentFontSize:Ljava/lang/String;

.field private final mCurrentLocation:[I

.field private mCurrentTopMargin:I

.field private final mDismissPopupRunnable:Ljava/lang/Runnable;

.field private mDismissPopupWhenScroll:Z

.field private mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mExitButton:Landroid/view/View;

.field private final mExitButtonListener:Landroid/view/View$OnClickListener;

.field private mExpandFlag:Z

.field mExpendBarHoverListener:Landroid/view/View$OnHoverListener;

.field mExpendBarListener:Landroid/view/View$OnTouchListener;

.field private mFirstLongPress:Z

.field private mFontLineSpaceSpinner:Landroid/widget/Spinner;

.field private mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

.field private mFontSizeButton:Landroid/widget/Button;

.field private mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

.field private final mFontSizeOnClickListener:Landroid/view/View$OnClickListener;

.field private mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

.field private mFontSizeSpinnerView:Landroid/widget/Button;

.field private mFontTypeButton:Landroid/widget/Button;

.field private final mFontTypeOnClickListener:Landroid/view/View$OnClickListener;

.field private mFontTypeSpinnerView:Landroid/view/ViewGroup;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private final mGestureDetectorListener:Landroid/view/GestureDetector$OnGestureListener;

.field private final mGrayColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

.field private mGrayScaleView:Landroid/view/View;

.field private final mHoverListener:Landroid/view/View$OnHoverListener;

.field private mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field private mIndentLeftBtn:Landroid/widget/ImageButton;

.field private mIndentRightBtn:Landroid/widget/ImageButton;

.field private mIndicator:Landroid/widget/ImageView;

.field private mIsColorPickerEnabled:Z

.field private mIsFrirstShown:Z

.field private mIsRotated:Z

.field private mIsRotated2:Z

.field private mItalicBtn:Landroid/widget/ImageButton;

.field mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private mLeftMargin:I

.field private final mMovableRect:Landroid/graphics/Rect;

.field private mMoveSettingLayout:Z

.field private mNeedCalculateMargin:Z

.field private mNeedRecalculateRotate:Z

.field private mNeedRotateWhenSetPosition:Z

.field private final mOldLocation:[I

.field private final mOldMovableRect:Landroid/graphics/Rect;

.field private final mOnColorChangedListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

.field private final mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

.field private final mOnTouchListener:Landroid/view/View$OnTouchListener;

.field private final mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

.field private mPaletteBg:Landroid/view/View;

.field private mPaletteLeftButton:Landroid/widget/ImageView;

.field private final mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

.field private mPaletteRightButton:Landroid/widget/ImageView;

.field private mPaletteView:Landroid/view/View;

.field private mParaLineSpinner1stSelect:Z

.field private mParagraphSetting:Landroid/view/View;

.field private mPickerView:Landroid/view/View;

.field private mPopupHeight:I

.field private mPosY:I

.field private mPreCanvasFingerAction:I

.field private mPreCanvasPenAction:I

.field private mRightIndicator:Landroid/widget/ImageView;

.field private mScale:F

.field private mScrollHandle:Landroid/widget/ImageView;

.field private mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

.field private final mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

.field private mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

.field private final mTempMovableRect:Landroid/graphics/Rect;

.field private final mTextAlignSettingListener:Landroid/view/View$OnClickListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation
.end field

.field private mTextContext:Landroid/content/Context;

.field private mTextFontSizeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTextIndentSettingListener:Landroid/view/View$OnClickListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation
.end field

.field private final mTextLineSpacingSettingListener:Landroid/widget/AdapterView$OnItemSelectedListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation
.end field

.field private final mTextOptButtonListener:Landroid/view/View$OnClickListener;

.field private mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

.field private mTextSettingPreview:Landroid/view/View;

.field private mTextSizeButtonView:[Landroid/view/View;

.field private final mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field private mTitleLayout:Landroid/view/View;

.field private mTopMargin:I

.field private mUnderlineBtn:Landroid/widget/ImageButton;

.field private mViewMode:I

.field private mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

.field private mXDelta:I

.field private mYDelta:I

.field private palateLayoutTranparent:Landroid/widget/RelativeLayout;

.field private paragraphSettingLayout:Landroid/widget/LinearLayout;

.field private final runnableRotate:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 115
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    .line 211
    const/16 v0, 0x26

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_WIDTH:I

    .line 219
    sput v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    .line 220
    sput v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeightNormal:I

    .line 3771
    const-string v0, "snote_text_center"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCenterIocnPath:Ljava/lang/String;

    .line 3772
    const-string v0, "snote_text_right"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIconPath:Ljava/lang/String;

    .line 3773
    const-string v0, "snote_text_all_left"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIndentIconPath:Ljava/lang/String;

    .line 3774
    const-string v0, "snote_text_all_right"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndentIconPath:Ljava/lang/String;

    .line 3775
    const-string v0, "snote_text_left_press"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIconPressPath:Ljava/lang/String;

    .line 3776
    const-string v0, "snote_text_center_press"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCenterIconPressPath:Ljava/lang/String;

    .line 3777
    const-string v0, "snote_text_right_press"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIconPressPath:Ljava/lang/String;

    .line 3778
    const-string v0, "snote_option_in_bg"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOptionBgPath:Ljava/lang/String;

    .line 3779
    const-string v0, "snote_popup_scroll_handle_n"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandelNormal:Ljava/lang/String;

    .line 3780
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Landroid/widget/RelativeLayout;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p4, "relativeLayout"    # Landroid/widget/RelativeLayout;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/widget/RelativeLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "fontName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v6, 0x0

    const/16 v5, 0x15

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1126
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 53
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z

    .line 54
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsFrirstShown:Z

    .line 125
    const-string v1, "10"

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    .line 137
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpandFlag:Z

    .line 138
    const/4 v1, -0x2

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    .line 171
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    .line 172
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    .line 182
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParaLineSpinner1stSelect:Z

    .line 183
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    .line 184
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 185
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    .line 189
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedCalculateMargin:Z

    .line 190
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFirstLongPress:Z

    .line 196
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    .line 197
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    .line 200
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isChangePosition:Z

    .line 201
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    .line 203
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 204
    const/16 v1, 0x5a0

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 205
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    .line 221
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 222
    const/4 v1, 0x5

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    .line 226
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    .line 227
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    .line 228
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaOfFirstTime:I

    .line 390
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 474
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    .line 481
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetectorListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 546
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGrayColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    .line 594
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColorChangeListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    .line 636
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnColorChangedListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    .line 683
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeOnClickListener:Landroid/view/View$OnClickListener;

    .line 733
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeOnClickListener:Landroid/view/View$OnClickListener;

    .line 791
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$9;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextOptButtonListener:Landroid/view/View$OnClickListener;

    .line 798
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 822
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$11;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColorListener:Landroid/view/View$OnClickListener;

    .line 844
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$12;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    .line 879
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$13;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    .line 898
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    .line 928
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$15;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    .line 960
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextLineSpacingSettingListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 986
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$17;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$17;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 995
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$18;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$18;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpendBarHoverListener:Landroid/view/View$OnHoverListener;

    .line 1004
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpendBarListener:Landroid/view/View$OnTouchListener;

    .line 1049
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupWhenScroll:Z

    .line 1050
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupRunnable:Ljava/lang/Runnable;

    .line 1058
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$21;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$21;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    .line 1816
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    .line 1817
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->handlerRotate:Landroid/os/Handler;

    .line 1818
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$22;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$22;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->runnableRotate:Ljava/lang/Runnable;

    .line 2471
    const/16 v1, 0xe

    new-array v1, v1, [Landroid/view/View;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    .line 3820
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 3900
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    .line 3901
    const/16 v1, 0x1f4

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    .line 3930
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsColorPickerEnabled:Z

    .line 1127
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    .line 1128
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    invoke-direct {v1, p1, p2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1129
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 1130
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 1131
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 1132
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1134
    new-instance v1, Lcom/samsung/android/sdk/pen/util/SpenFont;

    invoke-direct {v1, p1, p3}, Lcom/samsung/android/sdk/pen/util/SpenFont;-><init>(Landroid/content/Context;Ljava/util/HashMap;)V

    .line 1135
    const/16 v0, 0x8

    .local v0, "mTextSize":I
    :goto_0
    if-lt v0, v5, :cond_2

    .line 1138
    const/16 v0, 0x16

    :goto_1
    const/16 v1, 0x21

    if-lt v0, v1, :cond_3

    .line 1141
    const/16 v0, 0x24

    :goto_2
    const/16 v1, 0x41

    if-lt v0, v1, :cond_4

    .line 1144
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 1145
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x3fc00000    # 1.5f

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    .line 1146
    const/16 v1, 0xa

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 1147
    const/4 v1, 0x6

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    .line 1149
    :cond_0
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    if-lt v1, v5, :cond_1

    .line 1150
    const/16 v1, 0x24

    sput v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_WIDTH:I

    .line 1152
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->initView()V

    .line 1153
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setListener()V

    .line 1154
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    .line 1155
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    .line 1156
    new-array v1, v4, [I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    .line 1157
    new-array v1, v4, [I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentLocation:[I

    .line 1158
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTempMovableRect:Landroid/graphics/Rect;

    .line 1159
    return-void

    .line 1136
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1135
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1139
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1138
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 1142
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1141
    add-int/lit8 v0, v0, 0x4

    goto :goto_2
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Landroid/widget/RelativeLayout;F)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p4, "relativeLayout"    # Landroid/widget/RelativeLayout;
    .param p5, "ratio"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/widget/RelativeLayout;",
            "F)V"
        }
    .end annotation

    .prologue
    .local p3, "fontName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v6, 0x0

    const/16 v5, 0x15

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1226
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 53
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z

    .line 54
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsFrirstShown:Z

    .line 125
    const-string v1, "10"

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    .line 137
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpandFlag:Z

    .line 138
    const/4 v1, -0x2

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    .line 171
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    .line 172
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    .line 182
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParaLineSpinner1stSelect:Z

    .line 183
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    .line 184
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 185
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    .line 189
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedCalculateMargin:Z

    .line 190
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFirstLongPress:Z

    .line 196
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    .line 197
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    .line 200
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isChangePosition:Z

    .line 201
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    .line 203
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 204
    const/16 v1, 0x5a0

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 205
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    .line 221
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 222
    const/4 v1, 0x5

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    .line 226
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    .line 227
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    .line 228
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaOfFirstTime:I

    .line 390
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 474
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    .line 481
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetectorListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 546
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGrayColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    .line 594
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColorChangeListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    .line 636
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnColorChangedListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    .line 683
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeOnClickListener:Landroid/view/View$OnClickListener;

    .line 733
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeOnClickListener:Landroid/view/View$OnClickListener;

    .line 791
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$9;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextOptButtonListener:Landroid/view/View$OnClickListener;

    .line 798
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 822
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$11;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColorListener:Landroid/view/View$OnClickListener;

    .line 844
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$12;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    .line 879
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$13;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    .line 898
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    .line 928
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$15;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    .line 960
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextLineSpacingSettingListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 986
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$17;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$17;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 995
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$18;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$18;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpendBarHoverListener:Landroid/view/View$OnHoverListener;

    .line 1004
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpendBarListener:Landroid/view/View$OnTouchListener;

    .line 1049
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupWhenScroll:Z

    .line 1050
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupRunnable:Ljava/lang/Runnable;

    .line 1058
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$21;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$21;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    .line 1816
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    .line 1817
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->handlerRotate:Landroid/os/Handler;

    .line 1818
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$22;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$22;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->runnableRotate:Ljava/lang/Runnable;

    .line 2471
    const/16 v1, 0xe

    new-array v1, v1, [Landroid/view/View;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    .line 3820
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 3900
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    .line 3901
    const/16 v1, 0x1f4

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    .line 3930
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsColorPickerEnabled:Z

    .line 1227
    iput p5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 1228
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    const/high16 v2, 0x40000000    # 2.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    :goto_0
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 1229
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    const v2, 0x3f59999a    # 0.85f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    :goto_1
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 1230
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    .line 1231
    const-string v1, "Hoa"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mSdkVersion1: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1232
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    invoke-direct {v1, p1, p2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1233
    const-string v1, "Hoa"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mSdkVersion2: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1234
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 1235
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 1236
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 1237
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1239
    new-instance v1, Lcom/samsung/android/sdk/pen/util/SpenFont;

    invoke-direct {v1, p1, p3}, Lcom/samsung/android/sdk/pen/util/SpenFont;-><init>(Landroid/content/Context;Ljava/util/HashMap;)V

    .line 1240
    const/16 v0, 0x8

    .local v0, "mTextSize":I
    :goto_2
    if-lt v0, v5, :cond_4

    .line 1243
    const/16 v0, 0x16

    :goto_3
    const/16 v1, 0x21

    if-lt v0, v1, :cond_5

    .line 1246
    const/16 v0, 0x24

    :goto_4
    const/16 v1, 0x41

    if-lt v0, v1, :cond_6

    .line 1249
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 1250
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x3fc00000    # 1.5f

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    .line 1251
    const/16 v1, 0xa

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 1252
    const/4 v1, 0x6

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    .line 1254
    :cond_0
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    if-lt v1, v5, :cond_1

    .line 1255
    const/16 v1, 0x24

    sput v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_WIDTH:I

    .line 1258
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->initView()V

    .line 1260
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setListener()V

    .line 1261
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    .line 1262
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    .line 1263
    new-array v1, v4, [I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    .line 1264
    new-array v1, v4, [I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentLocation:[I

    .line 1265
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTempMovableRect:Landroid/graphics/Rect;

    .line 1266
    return-void

    .line 1228
    .end local v0    # "mTextSize":I
    :cond_2
    const/high16 v1, 0x40000000    # 2.0f

    goto/16 :goto_0

    .line 1229
    :cond_3
    const v1, 0x3f59999a    # 0.85f

    goto/16 :goto_1

    .line 1241
    .restart local v0    # "mTextSize":I
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1240
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1244
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1243
    add-int/lit8 v0, v0, 0x2

    goto :goto_3

    .line 1247
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1246
    add-int/lit8 v0, v0, 0x4

    goto/16 :goto_4
.end method

.method private ColorPickerSettinginit()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1748
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;FII)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    .line 1749
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSettingExitButton:Landroid/view/View;

    .line 1750
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSettingExitButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColor:Landroid/view/View;

    .line 1752
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColor:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColorListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1753
    return-void
.end method

.method private GrayScaleView()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 2456
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2457
    .local v1, "localLinearLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 2458
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41500000    # 13.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2457
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2459
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41d00000    # 26.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2460
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41d80000    # 27.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2461
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42f00000    # 120.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2462
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2464
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->colorGrayScaleView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    .line 2465
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2466
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    .line 2468
    return-object v1
.end method

.method private PaletteView()Landroid/view/ViewGroup;
    .locals 14
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 2476
    new-instance v11, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v11, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2477
    .local v11, "sizeDisplayLayout":Landroid/widget/RelativeLayout;
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 2478
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x425c0000    # 55.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 2477
    invoke-direct {v12, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2479
    .local v12, "sizeDisplayLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 2480
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41100000    # 9.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, 0x0

    .line 2479
    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2481
    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2482
    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 2483
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    .line 2484
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2485
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x425c0000    # 55.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 2484
    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2486
    .local v10, "rightImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2487
    const/16 v0, 0xb

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2488
    const/16 v0, 0xf

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2490
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2491
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_next"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2493
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 2494
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_arrow_right_normal"

    const-string v2, "snote_popup_arrow_right_press"

    .line 2495
    const-string v3, "snote_popup_arrow_right_focus"

    const-string v4, "snote_popup_arrow_right_dim"

    const/16 v5, 0x12

    const/16 v6, 0x37

    .line 2494
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2507
    :goto_0
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    .line 2508
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2509
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x425c0000    # 55.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 2508
    invoke-direct {v9, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2510
    .local v9, "leftImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2511
    const/16 v0, 0x9

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2512
    const/16 v0, 0xf

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2514
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2515
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_back"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2517
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_2

    .line 2518
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_arrow_left_normal"

    const-string v2, "snote_popup_arrow_left_press"

    .line 2519
    const-string v3, "snote_popup_arrow_left_focus"

    const-string v4, "snote_popup_arrow_left_dim"

    const/16 v5, 0x12

    const/16 v6, 0x37

    .line 2518
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2531
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->colorPaletteView()Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    .line 2533
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paletteTransparent()V

    .line 2534
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2535
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2536
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2537
    return-object v11

    .line 2496
    .end local v9    # "leftImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 2497
    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    const/4 v0, -0x1

    invoke-direct {v8, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2498
    .local v8, "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/RippleDrawable;

    .line 2499
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v3, 0x2d

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v8}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2498
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2500
    iget-object v13, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_arrow_right_normal"

    const/4 v2, 0x0

    const-string v3, "snote_popup_arrow_right_focus"

    .line 2501
    const/4 v4, 0x0

    const-string v5, "snote_popup_arrow_right_dim"

    const/16 v6, 0x12

    const/16 v7, 0x37

    .line 2500
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 2503
    .end local v8    # "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_arrow_right_normal"

    const-string v2, "snote_popup_arrow_right_press"

    .line 2504
    const-string v3, "snote_popup_arrow_right_focus"

    const-string v4, "snote_popup_arrow_right_dim"

    const/16 v5, 0x12

    const/16 v6, 0x37

    .line 2503
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 2520
    .restart local v9    # "leftImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 2521
    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 2522
    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    const/4 v0, -0x1

    invoke-direct {v8, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2523
    .restart local v8    # "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/RippleDrawable;

    .line 2524
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v3, 0x2d

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v8}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2523
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2525
    iget-object v13, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_arrow_left_normal"

    const/4 v2, 0x0

    const-string v3, "snote_popup_arrow_left_focus"

    .line 2526
    const/4 v4, 0x0

    const-string v5, "snote_popup_arrow_left_dim"

    const/16 v6, 0x12

    const/16 v7, 0x37

    .line 2525
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 2528
    .end local v8    # "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, "snote_popup_arrow_left_normal"

    const-string v2, "snote_popup_arrow_left_press"

    const-string v3, "snote_popup_arrow_left_focus"

    .line 2529
    const-string v4, "snote_popup_arrow_left_dim"

    const/16 v5, 0x12

    const/16 v6, 0x37

    .line 2528
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1
.end method

.method private PickerView()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 2441
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2442
    .local v1, "localLinearLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 2443
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42400000    # 48.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2442
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2444
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41d00000    # 26.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2445
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41d80000    # 27.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2446
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x428a0000    # 69.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2447
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2449
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->colorPickerView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    .line 2450
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2451
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 2452
    return-object v1
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/view/GestureDetector;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 201
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    return-object v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 3887
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)[I
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedCalculateMargin:Z

    return v0
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 191
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftMargin:I

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 192
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTopMargin:I

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 189
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedCalculateMargin:Z

    return-void
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFirstLongPress:Z

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 190
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFirstLongPress:Z

    return-void
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    return-void
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 186
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mXDelta:I

    return-void
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 187
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mYDelta:I

    return-void
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    return-object v0
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    return-object v0
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    return v0
.end method

.method static synthetic access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    return-object v0
.end method

.method static synthetic access$31(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    return-object v0
.end method

.method static synthetic access$32(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    return-object v0
.end method

.method static synthetic access$33(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    return-object v0
.end method

.method static synthetic access$34(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 3930
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsColorPickerEnabled:Z

    return v0
.end method

.method static synthetic access$35(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 179
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasPenAction:I

    return-void
.end method

.method static synthetic access$36(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 180
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasFingerAction:I

    return-void
.end method

.method static synthetic access$37(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    return v0
.end method

.method static synthetic access$38(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 227
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    return-void
.end method

.method static synthetic access$39(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mXDelta:I

    return v0
.end method

.method static synthetic access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 226
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    return v0
.end method

.method static synthetic access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    .prologue
    .line 3903
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->calculatePopupPosition()V

    return-void
.end method

.method static synthetic access$42(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$43(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 3901
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    return v0
.end method

.method static synthetic access$44(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)F
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    return v0
.end method

.method static synthetic access$45(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    return-void
.end method

.method static synthetic access$46(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/util/DisplayMetrics;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    return-object v0
.end method

.method static synthetic access$47(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 204
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    return-void
.end method

.method static synthetic access$48(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 204
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    return v0
.end method

.method static synthetic access$49(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mYDelta:I

    return v0
.end method

.method static synthetic access$50(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$51(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$52(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 3900
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    return v0
.end method

.method static synthetic access$53(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    return-void
.end method

.method static synthetic access$54(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$55(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$56()I
    .locals 1

    .prologue
    .line 115
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    return v0
.end method

.method static synthetic access$57(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$58(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 3226
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setTextStyle(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$59(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasPenAction:I

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$60(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasFingerAction:I

    return v0
.end method

.method static synthetic access$61(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$62(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$63(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$64(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$65(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$66(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$67(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$68(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)[Landroid/view/View;
    .locals 1

    .prologue
    .line 2471
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$69(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParaLineSpinner1stSelect:Z

    return v0
.end method

.method static synthetic access$7()I
    .locals 1

    .prologue
    .line 219
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    return v0
.end method

.method static synthetic access$70(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 182
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParaLineSpinner1stSelect:Z

    return-void
.end method

.method static synthetic access$71(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$72(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$73(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    return-object v0
.end method

.method static synthetic access$74(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 137
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpandFlag:Z

    return-void
.end method

.method static synthetic access$75(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpandFlag:Z

    return v0
.end method

.method static synthetic access$76(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 1049
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupWhenScroll:Z

    return-void
.end method

.method static synthetic access$77(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 1049
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupWhenScroll:Z

    return v0
.end method

.method static synthetic access$78(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 1816
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    return-void
.end method

.method static synthetic access$79(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$80(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 196
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    return-void
.end method

.method static synthetic access$81(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 197
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    return-void
.end method

.method static synthetic access$82(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    return v0
.end method

.method static synthetic access$83(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)[I
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentLocation:[I

    return-object v0
.end method

.method static synthetic access$84(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 200
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isChangePosition:Z

    return-void
.end method

.method static synthetic access$85(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 200
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isChangePosition:Z

    return v0
.end method

.method static synthetic access$86(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 1816
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    return v0
.end method

.method static synthetic access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    .prologue
    .line 2702
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->rotatePosition()V

    return-void
.end method

.method static synthetic access$88(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z

    return v0
.end method

.method static synthetic access$89(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    .prologue
    .line 2764
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->checkPosition()V

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    return-object v0
.end method

.method private addParagraphSettingLayout()V
    .locals 27

    .prologue
    .line 2117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-nez v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-nez v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-nez v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v22, v0

    if-nez v22, :cond_0

    .line 2121
    new-instance v13, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v13, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2122
    .local v13, "paragraphSettingAlignLayout":Landroid/widget/LinearLayout;
    new-instance v14, Landroid/widget/LinearLayout$LayoutParams;

    .line 2123
    const/16 v22, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x42580000    # 54.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2122
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v14, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2124
    .local v14, "paragraphSettingAlignLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x40c00000    # 6.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    iput v0, v14, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x40e00000    # 7.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    iput v0, v14, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2126
    const/16 v22, 0x10

    move/from16 v0, v22

    iput v0, v14, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2127
    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2128
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2129
    new-instance v10, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v10, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2130
    .local v10, "mAlignTextView":Landroid/widget/TextView;
    new-instance v11, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v22, -0x1

    .line 2131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x42580000    # 54.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2130
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v11, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2132
    .local v11, "mAlignTextViewParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v22

    iput v0, v11, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2133
    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-object/from16 v22, v0

    const-string v23, "string_align"

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2135
    const/high16 v22, -0x1000000

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2136
    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41700000    # 15.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v10, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2137
    const/16 v22, 0x11

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 2138
    invoke-virtual {v13, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2139
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    .line 2140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x42300000    # 44.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41d80000    # 27.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2139
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v9, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2141
    .local v9, "mAlignLeftBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x41300000    # 11.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    iput v0, v9, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2142
    const/16 v22, 0x10

    move/from16 v0, v22

    iput v0, v9, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2143
    new-instance v22, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    .line 2144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const-string v24, "snote_text_left"

    sget-object v25, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIconPressPath:Ljava/lang/String;

    .line 2147
    sget-object v26, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIconPressPath:Ljava/lang/String;

    .line 2146
    invoke-virtual/range {v23 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2149
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    .line 2150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x42300000    # 44.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41d80000    # 27.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2149
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v8, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2151
    .local v8, "mAlignCenterBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v22, 0x10

    move/from16 v0, v22

    iput v0, v8, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2152
    new-instance v22, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    .line 2153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCenterIocnPath:Ljava/lang/String;

    sget-object v25, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCenterIconPressPath:Ljava/lang/String;

    .line 2156
    sget-object v26, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCenterIconPressPath:Ljava/lang/String;

    .line 2155
    invoke-virtual/range {v23 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2158
    new-instance v22, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    .line 2159
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2162
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v23, v0

    const-string v24, "snote_popup_btn_normal"

    const-string v25, "snote_popup_btn_press"

    .line 2163
    const-string v26, "snote_popup_btn_press"

    .line 2162
    invoke-virtual/range {v22 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2164
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v23, v0

    const-string v24, "snote_popup_btn_normal"

    const-string v25, "snote_popup_btn_press"

    .line 2165
    const-string v26, "snote_popup_btn_press"

    .line 2164
    invoke-virtual/range {v22 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v23, v0

    const-string v24, "snote_popup_btn_normal"

    const-string v25, "snote_popup_btn_press"

    .line 2167
    const-string v26, "snote_popup_btn_press"

    .line 2166
    invoke-virtual/range {v22 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIconPath:Ljava/lang/String;

    sget-object v25, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIconPressPath:Ljava/lang/String;

    .line 2170
    sget-object v26, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIconPressPath:Ljava/lang/String;

    .line 2169
    invoke-virtual/range {v23 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2175
    new-instance v15, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v15, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2176
    .local v15, "paragraphSettingIndentLayout":Landroid/widget/LinearLayout;
    new-instance v16, Landroid/widget/LinearLayout$LayoutParams;

    .line 2177
    const/16 v22, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x42580000    # 54.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2176
    move-object/from16 v0, v16

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2178
    .local v16, "paragraphSettingIndentLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x40c00000    # 6.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v16

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x40e00000    # 7.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v16

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2180
    const/16 v22, 0x10

    move/from16 v0, v22

    move-object/from16 v1, v16

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2181
    invoke-virtual/range {v15 .. v16}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2182
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2184
    new-instance v7, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v7, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2185
    .local v7, "indentTextView":Landroid/widget/TextView;
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v22, -0x1

    .line 2186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x42580000    # 54.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2185
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v6, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2187
    .local v6, "indentTextParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v22

    iput v0, v6, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2188
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-object/from16 v22, v0

    const-string v23, "string_indent"

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2190
    const/high16 v22, -0x1000000

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2191
    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41700000    # 15.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2192
    const/16 v22, 0x11

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 2193
    invoke-virtual {v15, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2195
    new-instance v22, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    .line 2196
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 2197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x42300000    # 44.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41d80000    # 27.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2196
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v4, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2198
    .local v4, "indentLeftBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x41300000    # 11.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    iput v0, v4, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2199
    const/16 v22, 0x10

    move/from16 v0, v22

    iput v0, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIndentIconPath:Ljava/lang/String;

    sget-object v25, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIndentIconPath:Ljava/lang/String;

    .line 2202
    sget-object v26, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIndentIconPath:Ljava/lang/String;

    .line 2201
    invoke-virtual/range {v23 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2205
    new-instance v22, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    .line 2206
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 2207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x42300000    # 44.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41d80000    # 27.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2206
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v5, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2208
    .local v5, "indentRightBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v22, 0x10

    move/from16 v0, v22

    iput v0, v5, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v23, v0

    const-string v24, "snote_popup_btn_normal"

    const-string v25, "snote_popup_btn_press"

    .line 2213
    const-string v26, "snote_popup_btn_press"

    .line 2212
    invoke-virtual/range {v22 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v23, v0

    const-string v24, "snote_popup_btn_normal"

    const-string v25, "snote_popup_btn_press"

    .line 2215
    const-string v26, "snote_popup_btn_press"

    .line 2214
    invoke-virtual/range {v22 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndentIconPath:Ljava/lang/String;

    .line 2218
    sget-object v25, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndentIconPath:Ljava/lang/String;

    sget-object v26, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndentIconPath:Ljava/lang/String;

    .line 2217
    invoke-virtual/range {v23 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2223
    new-instance v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2224
    .local v17, "paragraphSettingSpacingLayout":Landroid/widget/LinearLayout;
    new-instance v18, Landroid/widget/LinearLayout$LayoutParams;

    .line 2225
    const/16 v22, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x42580000    # 54.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2224
    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2226
    .local v18, "paragraphSettingSpacingLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x40c00000    # 6.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x40e00000    # 7.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2228
    const/16 v22, 0x10

    move/from16 v0, v22

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2229
    invoke-virtual/range {v17 .. v18}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2230
    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2232
    new-instance v20, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2233
    .local v20, "spacingTextView":Landroid/widget/TextView;
    new-instance v19, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v22, -0x1

    .line 2234
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x42580000    # 54.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2233
    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2235
    .local v19, "spacingTextParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v22

    move-object/from16 v1, v19

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2236
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-object/from16 v22, v0

    const-string v23, "string_line_spacing"

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2238
    const/high16 v22, -0x1000000

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2239
    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41700000    # 15.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v20

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2240
    const/16 v22, 0x11

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 2241
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2243
    new-instance v22, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    .line 2244
    new-instance v21, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v22, -0x2

    .line 2245
    const/16 v23, -0x2

    .line 2244
    invoke-direct/range {v21 .. v23}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2246
    .local v21, "spinnerBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x41300000    # 11.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2247
    const/16 v22, 0x10

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2249
    const/16 v22, 0x1e

    move/from16 v0, v22

    new-array v12, v0, [Ljava/lang/String;

    const/16 v22, 0x0

    const-string v23, "10"

    aput-object v23, v12, v22

    const/16 v22, 0x1

    const-string v23, "11"

    aput-object v23, v12, v22

    const/16 v22, 0x2

    const-string v23, "12"

    aput-object v23, v12, v22

    const/16 v22, 0x3

    const-string v23, "13"

    aput-object v23, v12, v22

    const/16 v22, 0x4

    const-string v23, "14"

    aput-object v23, v12, v22

    const/16 v22, 0x5

    const-string v23, "15"

    aput-object v23, v12, v22

    const/16 v22, 0x6

    const-string v23, "16"

    aput-object v23, v12, v22

    const/16 v22, 0x7

    const-string v23, "17"

    aput-object v23, v12, v22

    const/16 v22, 0x8

    const-string v23, "18"

    aput-object v23, v12, v22

    const/16 v22, 0x9

    const-string v23, "19"

    aput-object v23, v12, v22

    const/16 v22, 0xa

    const-string v23, "20"

    aput-object v23, v12, v22

    const/16 v22, 0xb

    const-string v23, "22"

    aput-object v23, v12, v22

    const/16 v22, 0xc

    const-string v23, "24"

    aput-object v23, v12, v22

    const/16 v22, 0xd

    const-string v23, "26"

    aput-object v23, v12, v22

    const/16 v22, 0xe

    .line 2250
    const-string v23, "28"

    aput-object v23, v12, v22

    const/16 v22, 0xf

    const-string v23, "30"

    aput-object v23, v12, v22

    const/16 v22, 0x10

    const-string v23, "32"

    aput-object v23, v12, v22

    const/16 v22, 0x11

    const-string v23, "36"

    aput-object v23, v12, v22

    const/16 v22, 0x12

    const-string v23, "40"

    aput-object v23, v12, v22

    const/16 v22, 0x13

    const-string v23, "43"

    aput-object v23, v12, v22

    const/16 v22, 0x14

    const-string v23, "44"

    aput-object v23, v12, v22

    const/16 v22, 0x15

    const-string v23, "48"

    aput-object v23, v12, v22

    const/16 v22, 0x16

    const-string v23, "52"

    aput-object v23, v12, v22

    const/16 v22, 0x17

    const-string v23, "56"

    aput-object v23, v12, v22

    const/16 v22, 0x18

    const-string v23, "60"

    aput-object v23, v12, v22

    const/16 v22, 0x19

    const-string v23, "64"

    aput-object v23, v12, v22

    const/16 v22, 0x1a

    const-string v23, "68"

    aput-object v23, v12, v22

    const/16 v22, 0x1b

    const-string v23, "72"

    aput-object v23, v12, v22

    const/16 v22, 0x1c

    const-string v23, "80"

    aput-object v23, v12, v22

    const/16 v22, 0x1d

    const-string v23, "88"

    aput-object v23, v12, v22

    .line 2251
    .local v12, "numbers":[Ljava/lang/String;
    new-instance v3, Landroid/widget/ArrayAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const v23, 0x1090008

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v3, v0, v1, v12}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 2253
    .local v3, "adpater":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 2254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v22, v0

    const/16 v23, 0x13

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Spinner;->setSelection(I)V

    .line 2255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextLineSpacingSettingListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v22, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2260
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2264
    .end local v3    # "adpater":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    .end local v4    # "indentLeftBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v5    # "indentRightBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v6    # "indentTextParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v7    # "indentTextView":Landroid/widget/TextView;
    .end local v8    # "mAlignCenterBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v9    # "mAlignLeftBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v10    # "mAlignTextView":Landroid/widget/TextView;
    .end local v11    # "mAlignTextViewParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v12    # "numbers":[Ljava/lang/String;
    .end local v13    # "paragraphSettingAlignLayout":Landroid/widget/LinearLayout;
    .end local v14    # "paragraphSettingAlignLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v15    # "paragraphSettingIndentLayout":Landroid/widget/LinearLayout;
    .end local v16    # "paragraphSettingIndentLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v17    # "paragraphSettingSpacingLayout":Landroid/widget/LinearLayout;
    .end local v18    # "paragraphSettingSpacingLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v19    # "spacingTextParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v20    # "spacingTextView":Landroid/widget/TextView;
    .end local v21    # "spinnerBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-eqz v22, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-eqz v22, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-eqz v22, :cond_1

    .line 2265
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2267
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2270
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2272
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-eqz v22, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-eqz v22, :cond_2

    .line 2273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2276
    :cond_2
    return-void
.end method

.method private bodyBg()Landroid/view/ViewGroup;
    .locals 12

    .prologue
    const/16 v11, 0xa

    const/4 v10, 0x1

    const/high16 v7, 0x43770000    # 247.0f

    const/high16 v9, 0x42f80000    # 124.0f

    const/4 v8, -0x1

    .line 1491
    new-instance v5, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1492
    .local v5, "layout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1493
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1492
    invoke-direct {v0, v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1495
    .local v0, "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1497
    new-instance v1, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1498
    .local v1, "bodyLeft":Landroid/widget/ImageView;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1499
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1498
    invoke-direct {v2, v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1500
    .local v2, "bodyLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v10, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1501
    const/16 v6, 0x9

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1502
    invoke-virtual {v2, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1503
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1504
    new-instance v3, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v3, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1505
    .local v3, "bodyRight":Landroid/widget/ImageView;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1506
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    sub-int/2addr v6, v7

    .line 1505
    invoke-direct {v4, v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1508
    .local v4, "bodyRightParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v10, v4, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1509
    const/16 v6, 0xb

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1510
    invoke-virtual {v4, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1511
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1513
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v7, "snote_popup_bg_left"

    invoke-virtual {v6, v1, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1514
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v7, "snote_popup_bg_right"

    invoke-virtual {v6, v3, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1515
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1516
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1517
    invoke-virtual {v5, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1518
    invoke-virtual {v5, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1519
    return-object v5
.end method

.method private bodyLayout()Landroid/view/ViewGroup;
    .locals 15

    .prologue
    const/high16 v14, 0x43770000    # 247.0f

    const/4 v10, 0x1

    const/4 v13, -0x1

    const/4 v12, -0x2

    const/4 v11, 0x0

    .line 1429
    new-instance v8, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    .line 1430
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1431
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1430
    invoke-direct {v3, v8, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1432
    .local v3, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x41d00000    # 26.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    invoke-virtual {v3, v11, v11, v11, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1433
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v8, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1434
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v8, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setVerticalFadingEdgeEnabled(Z)V

    .line 1435
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v8, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setFadingEdgeLength(I)V

    .line 1436
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v8, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 1437
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v8, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setOverScrollMode(I)V

    .line 1439
    new-instance v4, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;-><init>(Landroid/content/Context;)V

    .line 1440
    .local v4, "palletViewLayout":Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v13, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1442
    .local v5, "palletViewLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1443
    invoke-virtual {v4, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->setOrientation(I)V

    .line 1444
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->textPreview()Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    .line 1445
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->fontSizeSpinnerOptButton()Landroid/view/ViewGroup;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    .line 1446
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->colorSelectedAndPicker()Landroid/view/ViewGroup;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    .line 1447
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 1448
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 1449
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 1451
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSetting()Landroid/view/ViewGroup;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    .line 1453
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->bottomLayout()Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    .line 1454
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v10, "string_drag_to_resize"

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1456
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 1457
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1458
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    neg-int v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    neg-int v9, v9

    invoke-virtual {v4, v8, v11, v9, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->setPadding(IIII)V

    .line 1459
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v8, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->addView(Landroid/view/View;)V

    .line 1461
    new-instance v6, Landroid/widget/RelativeLayout;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v6, v8}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1462
    .local v6, "scrollParent":Landroid/widget/RelativeLayout;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v7, v13, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1464
    .local v7, "scrollParentParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x40c00000    # 6.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    neg-int v8, v8

    invoke-virtual {v7, v11, v11, v11, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1465
    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1466
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    invoke-virtual {v6, v8, v11, v9, v11}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 1468
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1470
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v8}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1471
    .local v1, "layout":Landroid/widget/RelativeLayout;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1472
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1471
    invoke-direct {v2, v8, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1473
    .local v2, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1474
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->bodyBg()Landroid/view/ViewGroup;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyBg:Landroid/view/View;

    .line 1475
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1477
    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1478
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1479
    iget-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    if-nez v8, :cond_0

    .line 1480
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1481
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x42900000    # 72.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1480
    invoke-direct {v0, v13, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1482
    .local v0, "colorPickerParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1487
    .end local v0    # "colorPickerParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    return-object v1

    .line 1484
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v8, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 1485
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v8, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    goto :goto_0
.end method

.method private bottomLayout()Landroid/view/View;
    .locals 15
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/high16 v14, 0x41b00000    # 22.0f

    const/high16 v13, 0x41a00000    # 20.0f

    const/4 v8, -0x1

    const/16 v12, 0x10

    const/4 v11, 0x0

    .line 2652
    new-instance v2, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v2, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2653
    .local v2, "bottomLayout":Landroid/widget/RelativeLayout;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2654
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41d00000    # 26.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2653
    invoke-direct {v3, v8, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2656
    .local v3, "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v7, v7, 0xfe

    add-int/lit8 v7, v7, -0x1a

    int-to-float v7, v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2655
    invoke-virtual {v3, v11, v6, v11, v11}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2657
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2658
    new-instance v6, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    .line 2659
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2660
    invoke-direct {v7, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2659
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2662
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    const-string v8, "snote_popup_bg_expand"

    const-string v9, "snote_popup_bg_expand_press"

    .line 2663
    const-string v10, "snote_popup_bg_expand_press"

    .line 2662
    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2664
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    if-ge v6, v12, :cond_0

    .line 2665
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v8, "snote_popup_bg_expand"

    .line 2666
    const-string v9, "snote_popup_bg_expand_press"

    const-string v10, "snote_popup_bg_expand_press"

    .line 2665
    invoke-virtual {v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2672
    :goto_0
    new-instance v0, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2673
    .local v0, "bottomHandle":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2674
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/lit8 v7, v7, -0x13

    .line 2673
    invoke-direct {v1, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2675
    .local v1, "bottomHandleParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v6, 0x1

    iput-boolean v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2676
    const/16 v6, 0xe

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2677
    const/16 v6, 0x8

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2679
    iput v12, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 2680
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2681
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v7, "snote_popup_handler"

    const/16 v8, 0x16

    invoke-virtual {v6, v7, v8, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2682
    new-instance v5, Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 2683
    .local v5, "mButtonHandle":Landroid/widget/ImageButton;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2684
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/lit8 v7, v7, -0x13

    .line 2683
    invoke-direct {v4, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2685
    .local v4, "buttonHandleParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v6, 0x1

    iput-boolean v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2686
    const/16 v6, 0xe

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2687
    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2688
    iput v12, v4, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 2689
    invoke-virtual {v5, v4}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2690
    invoke-virtual {v5, v11}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 2691
    invoke-virtual {v5, v11}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 2692
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v7, "string_drag_to_resize"

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2694
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2695
    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2696
    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2697
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 2699
    return-object v2

    .line 2668
    .end local v0    # "bottomHandle":Landroid/widget/ImageView;
    .end local v1    # "bottomHandleParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v4    # "buttonHandleParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v5    # "mButtonHandle":Landroid/widget/ImageButton;
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v8, "snote_popup_bg_expand"

    const-string v9, "snote_popup_bg_expand_press"

    .line 2669
    const-string v10, "snote_popup_bg_expand_press"

    .line 2668
    invoke-virtual {v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method private calculatePopupPosition()V
    .locals 10

    .prologue
    const/16 v9, 0x14a

    const/high16 v8, 0x433b0000    # 187.0f

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 3904
    new-array v0, v7, [I

    .line 3905
    .local v0, "location":[I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->getLocationOnScreen([I)V

    .line 3906
    new-array v1, v7, [I

    .line 3907
    .local v1, "location2":[I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 3908
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43200000    # 160.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    .line 3909
    aget v2, v1, v6

    aget v3, v0, v6

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x420c0000    # 35.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    if-ge v2, v3, :cond_1

    sget v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    add-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    if-ge v2, v3, :cond_1

    .line 3910
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_1

    aget v2, v0, v6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    if-le v2, v3, :cond_1

    .line 3911
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    neg-int v2, v2

    add-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    .line 3916
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    if-ge v2, v3, :cond_0

    .line 3917
    aget v2, v0, v6

    if-le v2, v9, :cond_2

    .line 3918
    const/16 v2, 0xf0

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    .line 3919
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    neg-int v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41d80000    # 27.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    .line 3928
    :cond_0
    :goto_1
    return-void

    .line 3913
    :cond_1
    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    goto :goto_0

    .line 3920
    :cond_2
    aget v2, v0, v6

    if-gt v2, v9, :cond_3

    aget v2, v0, v6

    const/16 v3, 0x136

    if-lt v2, v3, :cond_3

    .line 3921
    const/16 v2, 0xc8

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    .line 3922
    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    goto :goto_1

    .line 3924
    :cond_3
    const/16 v2, 0xe6

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    .line 3925
    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    goto :goto_1
.end method

.method private checkPosition()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2765
    const/4 v3, 0x2

    new-array v0, v3, [I

    .line 2766
    .local v0, "location":[I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43770000    # 247.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    add-int/lit8 v1, v3, 0x13

    .line 2767
    .local v1, "minWidth":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    aget v3, v3, v6

    aput v3, v0, v6

    .line 2768
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    aget v3, v3, v7

    aput v3, v0, v7

    .line 2769
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2771
    .local v2, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    aget v3, v0, v6

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-ge v3, v4, :cond_0

    .line 2772
    iput v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2774
    :cond_0
    aget v3, v0, v7

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-ge v3, v4, :cond_1

    .line 2775
    iput v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2778
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    aget v4, v0, v6

    sub-int/2addr v3, v4

    if-ge v3, v1, :cond_2

    .line 2779
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v3, v1

    add-int/lit8 v3, v3, -0x2

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2781
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-gez v3, :cond_2

    .line 2782
    iput v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2785
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    aget v4, v0, v7

    sub-int/2addr v3, v4

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    if-ge v3, v4, :cond_3

    .line 2786
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2788
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-gez v3, :cond_3

    .line 2789
    iput v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2792
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v5

    aput v4, v3, v6

    .line 2793
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v5

    aput v4, v3, v7

    .line 2794
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2795
    return-void
.end method

.method private colorGrayScaleView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2629
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    .line 2630
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 2629
    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    .line 2631
    .local v1, "localf":Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 2632
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41500000    # 13.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2631
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2634
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2635
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setClickable(Z)V

    .line 2636
    invoke-virtual {v1, v5, v5, v5, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setPadding(IIII)V

    .line 2637
    const v2, -0xff0100

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setBackgroundColor(I)V

    .line 2638
    return-object v1
.end method

.method private colorPaletteView()Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 2581
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    .line 2582
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 2581
    invoke-direct {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    .line 2583
    .local v0, "colorPaletteView":Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsColorPickerEnabled:Z

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setColorPickerEnable(Z)V

    .line 2585
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2586
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43410000    # 193.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, -0x2

    .line 2585
    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2588
    .local v1, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2589
    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2590
    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2592
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2593
    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setClickable(Z)V

    .line 2594
    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setFocusable(Z)V

    .line 2595
    sget v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    .line 2596
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setImportantForAccessibility(I)V

    .line 2598
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_palette"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2600
    return-object v0
.end method

.method private colorPickerView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2612
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    .line 2613
    .local v1, "localf":Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 2614
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x423c0000    # 47.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2613
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2616
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2617
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setClickable(Z)V

    .line 2618
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v3, "string_gradation"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2619
    invoke-virtual {v1, v5, v5, v5, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setPadding(IIII)V

    .line 2620
    return-object v1
.end method

.method private colorSelectedAndPicker()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 2393
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2394
    .local v1, "localLinearLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 2395
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x430c0000    # 140.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2394
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2397
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2398
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->PickerView()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPickerView:Landroid/view/View;

    .line 2399
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->PaletteView()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteView:Landroid/view/View;

    .line 2400
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paletteBg()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    .line 2401
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->GrayScaleView()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGrayScaleView:Landroid/view/View;

    .line 2402
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2403
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2404
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPickerView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2405
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGrayScaleView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2406
    return-object v1
.end method

.method private exitButton()Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0x29

    const/16 v6, 0x24

    const/4 v4, -0x1

    const/high16 v3, 0x42240000    # 41.0f

    const/16 v7, 0xff

    .line 1924
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1926
    .local v1, "exitButton":Landroid/widget/ImageButton;
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v2, 0x15

    if-ge v0, v2, :cond_0

    .line 1927
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1928
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    sub-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x26

    div-int/lit8 v0, v0, 0x24

    .line 1927
    invoke-direct {v8, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1930
    .local v8, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v2, "snote_popup_close"

    const-string v3, "snote_popup_close_press"

    const-string v4, "snote_popup_close_focus"

    .line 1931
    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_WIDTH:I

    .line 1930
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1941
    :goto_0
    const/16 v0, 0xb

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1942
    invoke-virtual {v1, v8}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1943
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1944
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1946
    return-object v1

    .line 1933
    .end local v8    # "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1934
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    sub-int/2addr v0, v2

    .line 1933
    invoke-direct {v8, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1936
    .restart local v8    # "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v0, Landroid/graphics/drawable/RippleDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 1937
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1936
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1938
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v2, "snote_popup_close"

    const-string v3, "snote_popup_close_press"

    const-string v4, "snote_popup_close_focus"

    .line 1939
    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_WIDTH:I

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    .line 1938
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    goto :goto_0
.end method

.method private fontSizeSpinner()Landroid/widget/Button;
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2289
    new-instance v1, Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    .line 2290
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2291
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x420c0000    # 35.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41d80000    # 27.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 2290
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2292
    .local v0, "fontSizeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40e00000    # 7.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2293
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2294
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 2295
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v3, "snote_dropdown_normal"

    .line 2296
    const-string v4, "snote_dropdown_pressed"

    const-string v5, "snote_dropdown_focused"

    .line 2295
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2306
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setGravity(I)V

    .line 2307
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v8, v8, v8}, Landroid/widget/Button;->setPadding(IIII)V

    .line 2309
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setSingleLine(Z)V

    .line 2310
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    invoke-static {v8, v8, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 2311
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v8, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 2312
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2313
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2315
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2316
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    return-object v1

    .line 2297
    :cond_0
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 2298
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v4, 0x3d

    invoke-static {v4, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    .line 2299
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v5, "snote_dropdown_normal"

    const-string v6, "snote_dropdown_normal"

    const-string v7, "snote_dropdown_focused"

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    .line 2300
    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2298
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2302
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v3, "snote_dropdown_normal"

    const-string v4, "snote_dropdown_pressed"

    .line 2303
    const-string v5, "snote_dropdown_focused"

    .line 2302
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method private fontSizeSpinnerOptButton()Landroid/view/ViewGroup;
    .locals 13

    .prologue
    .line 2005
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2006
    .local v0, "fontSizeSpinnerOptButtonLayout":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2007
    const/4 v5, -0x1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x421c0000    # 39.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2006
    invoke-direct {v1, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2008
    .local v1, "fontSizeSpinnerOptButtonLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2009
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 2010
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2011
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41500000    # 13.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 2012
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x40a00000    # 5.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 2011
    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2013
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->fontTypeSpinnerView()Landroid/view/ViewGroup;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    .line 2014
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->fontSizeSpinner()Landroid/widget/Button;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/Button;

    .line 2015
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2016
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2018
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v5

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    cmpg-double v5, v6, v8

    if-gtz v5, :cond_0

    .line 2019
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41e00000    # 28.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 2020
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41f80000    # 31.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2019
    invoke-direct {v2, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2026
    .local v2, "mBoldBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    new-instance v5, Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    .line 2027
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v2}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2028
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2029
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v7, "string_bold"

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2030
    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v6, 0x15

    if-ge v5, v6, :cond_1

    .line 2031
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    const-string v7, "snote_popup_option_btn_left_normal"

    const-string v8, "snote_popup_option_btn_left_press_1"

    .line 2032
    const-string v9, "snote_popup_option_btn_left_focus"

    .line 2031
    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2038
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v7, "snote_popup_textoption_bold"

    const/16 v8, 0x14

    const/16 v9, 0x14

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2039
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2040
    new-instance v5, Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    .line 2042
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v5

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    cmpg-double v5, v6, v8

    if-gtz v5, :cond_2

    .line 2043
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41e00000    # 28.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 2044
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41f80000    # 31.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2043
    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2049
    .local v3, "mItalicBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v3}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2050
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2051
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v7, "string_italic"

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2053
    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v6, 0x15

    if-ge v5, v6, :cond_3

    .line 2054
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    const-string v7, "snote_popup_option_btn_center_normal"

    const-string v8, "snote_popup_option_btn_center_press"

    .line 2055
    const-string v9, "snote_popup_option_btn_center_focus"

    .line 2054
    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2061
    :goto_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v7, "snote_popup_textoption_italic"

    const/16 v8, 0x14

    const/16 v9, 0x14

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2062
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2064
    new-instance v5, Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    .line 2066
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v5

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    cmpg-double v5, v6, v8

    if-gtz v5, :cond_4

    .line 2067
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41e00000    # 28.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 2068
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41f80000    # 31.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2067
    invoke-direct {v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2073
    .local v4, "mUnderlineBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v5, v4}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2074
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2075
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v7, "string_underline"

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2079
    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v6, 0x15

    if-ge v5, v6, :cond_5

    .line 2080
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    const-string v7, "snote_popup_option_btn_right_normal"

    const-string v8, "snote_popup_option_btn_right_press_1"

    .line 2081
    const-string v9, "snote_popup_option_btn_right_focus"

    .line 2080
    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2087
    :goto_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v7, "snote_popup_textoption_underline"

    const/16 v8, 0x14

    const/16 v9, 0x14

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2088
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2090
    return-object v0

    .line 2022
    .end local v2    # "mBoldBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "mItalicBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v4    # "mUnderlineBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41e00000    # 28.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 2023
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41e80000    # 29.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2022
    invoke-direct {v2, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .restart local v2    # "mBoldBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    goto/16 :goto_0

    .line 2034
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    new-instance v6, Landroid/graphics/drawable/RippleDrawable;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v8, 0x3d

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v8, v9, v10, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v7

    .line 2035
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v9, "snote_popup_option_btn_left_normal"

    const-string v10, "snote_popup_option_btn_left_normal"

    const-string v11, "snote_popup_option_btn_left_focus"

    .line 2036
    const-string v12, "snote_popup_option_btn_left_press_1"

    .line 2035
    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v8

    .line 2036
    const/4 v9, 0x0

    invoke-direct {v6, v7, v8, v9}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2034
    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 2046
    :cond_2
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41e00000    # 28.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 2047
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41e80000    # 29.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2046
    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .restart local v3    # "mItalicBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    goto/16 :goto_2

    .line 2057
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    new-instance v6, Landroid/graphics/drawable/RippleDrawable;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v8, 0x3d

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v8, v9, v10, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v7

    .line 2058
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v9, "snote_popup_option_btn_center_normal"

    const-string v10, "snote_popup_option_btn_center_normal"

    const-string v11, "snote_popup_option_btn_center_focus"

    .line 2059
    const-string v12, "snote_popup_option_btn_center_press"

    .line 2058
    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v8

    .line 2059
    const/4 v9, 0x0

    invoke-direct {v6, v7, v8, v9}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2057
    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 2070
    :cond_4
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41e00000    # 28.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 2071
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41e80000    # 29.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2070
    invoke-direct {v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .restart local v4    # "mUnderlineBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    goto/16 :goto_4

    .line 2083
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    new-instance v6, Landroid/graphics/drawable/RippleDrawable;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v8, 0x3d

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v8, v9, v10, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v7

    .line 2084
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v9, "snote_popup_option_btn_right_normal"

    const-string v10, "snote_popup_option_btn_right_normal"

    const-string v11, "snote_popup_option_btn_right_focus"

    .line 2085
    const-string v12, "snote_popup_option_btn_right_press_1"

    .line 2084
    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v8

    .line 2085
    const/4 v9, 0x0

    invoke-direct {v6, v7, v8, v9}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2083
    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5
.end method

.method private fontTypeSpinner()Landroid/widget/Button;
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0x10

    const/4 v8, 0x0

    .line 2350
    new-instance v1, Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    .line 2351
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2352
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42b00000    # 88.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41d80000    # 27.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 2351
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2353
    .local v0, "fontTypeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2355
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    if-ge v1, v9, :cond_0

    .line 2356
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v3, "snote_dropdown_normal"

    .line 2357
    const-string v4, "snote_dropdown_pressed"

    const-string v5, "snote_dropdown_focused"

    .line 2356
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2367
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setGravity(I)V

    .line 2368
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 2369
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41200000    # 10.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2368
    invoke-virtual {v1, v2, v8, v3, v8}, Landroid/widget/Button;->setPadding(IIII)V

    .line 2371
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setSingleLine(Z)V

    .line 2372
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    invoke-static {v8, v8, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 2373
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v8, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 2374
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2375
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    if-lt v1, v9, :cond_2

    .line 2376
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2380
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2381
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    return-object v1

    .line 2358
    :cond_0
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 2359
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v4, 0x3d

    invoke-static {v4, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    .line 2360
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v5, "snote_dropdown_normal"

    const-string v6, "snote_dropdown_normal"

    const-string v7, "snote_dropdown_focused"

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    .line 2361
    invoke-direct {v2, v3, v4, v10}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2359
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2363
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v3, "snote_dropdown_normal"

    const-string v4, "snote_dropdown_pressed"

    .line 2364
    const-string v5, "snote_dropdown_focused"

    .line 2363
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 2378
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    invoke-virtual {v1, v10}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_1
.end method

.method private fontTypeSpinnerView()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 2329
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2330
    .local v0, "fontTypeSpinnerLayout":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2331
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42b00000    # 88.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41d80000    # 27.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2330
    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2332
    .local v1, "fontTypeSpinnerLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2333
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40e00000    # 7.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2334
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->fontTypeSpinner()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2335
    return-object v0
.end method

.method private getMovableRect()Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3888
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 3889
    .local v0, "location":[I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 3891
    .local v1, "r":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    .line 3893
    aget v2, v0, v4

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 3894
    aget v2, v0, v5

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTopMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 3895
    aget v2, v0, v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 3896
    aget v2, v0, v5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 3897
    return-object v1
.end method

.method private initColorSelecteView()V
    .locals 3

    .prologue
    .line 1741
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    if-eqz v0, :cond_0

    .line 1742
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnColorChangedListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getPreviewTextColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setInitialValue(Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;I)V

    .line 1745
    :cond_0
    return-void
.end method

.method private initView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1655
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43190000    # 153.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    sput v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeightNormal:I

    .line 1656
    sget v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeightNormal:I

    sput v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    .line 1657
    const/4 v0, 0x0

    .line 1658
    .local v0, "checkCanvasWidth":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v2, v3, :cond_2

    .line 1659
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1663
    :goto_0
    const/16 v2, 0x640

    if-ne v0, v2, :cond_3

    .line 1664
    const/16 v2, 0x21

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    .line 1669
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->totalLayout()V

    .line 1670
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    new-array v2, v2, [Landroid/view/View;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    .line 1671
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-lt v1, v2, :cond_4

    .line 1675
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_0

    .line 1676
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1677
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1678
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1679
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1680
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1681
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1683
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_1

    .line 1684
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1685
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1688
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->initColorSelecteView()V

    .line 1689
    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 1690
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->ColorPickerSettinginit()V

    .line 1691
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    .line 1692
    return-void

    .line 1661
    .end local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0

    .line 1666
    :cond_3
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    goto :goto_1

    .line 1672
    .restart local v1    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v1

    .line 1671
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private paletteBg()Landroid/view/ViewGroup;
    .locals 11

    .prologue
    const/16 v10, 0xa

    const/4 v9, 0x1

    const v8, 0x42fa999a    # 125.3f

    const/4 v7, -0x1

    .line 2411
    new-instance v3, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v3, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2412
    .local v3, "paletteBgLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2415
    .local v0, "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2417
    new-instance v4, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v4, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2418
    .local v4, "paletteLeft":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2419
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2418
    invoke-direct {v1, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2420
    .local v1, "bodyLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v9, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2421
    const/16 v6, 0x9

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2422
    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2423
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2424
    new-instance v5, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2425
    .local v5, "paletteRight":Landroid/widget/ImageView;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2426
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2425
    invoke-direct {v2, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2427
    .local v2, "bodyRightParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v9, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2428
    const/16 v6, 0xb

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2429
    invoke-virtual {v2, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2430
    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2432
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v7, "snote_popup_bg02_left"

    invoke-virtual {v6, v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 2433
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v7, "snote_popup_bg02_right"

    invoke-virtual {v6, v5, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 2435
    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2436
    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2437
    return-object v3
.end method

.method private paletteTransparent()V
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v6, -0x2

    const/high16 v8, 0x41c80000    # 25.0f

    .line 2543
    new-instance v4, Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    .line 2544
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2546
    .local v2, "palateParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v9, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2547
    const/16 v4, 0xe

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2548
    const/16 v4, 0xf

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2549
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2550
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 2551
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2552
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v10, :cond_1

    .line 2566
    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_0

    .line 2567
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v10}, Landroid/widget/RelativeLayout;->setImportantForAccessibility(I)V

    .line 2569
    :cond_0
    return-void

    .line 2553
    :cond_1
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    const/4 v4, 0x7

    if-lt v1, v4, :cond_2

    .line 2552
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2554
    :cond_2
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2555
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 2554
    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2556
    .local v3, "transparentLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    mul-int/lit8 v5, v0, 0x7

    add-int/2addr v5, v1

    new-instance v6, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    aput-object v6, v4, v5

    .line 2557
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    int-to-float v5, v1

    const/high16 v6, 0x41e00000    # 28.0f

    mul-float/2addr v5, v6

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2558
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    int-to-float v5, v0

    const/high16 v6, 0x41f00000    # 30.0f

    mul-float/2addr v5, v6

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2559
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    mul-int/lit8 v5, v0, 0x7

    add-int/2addr v5, v1

    aget-object v4, v4, v5

    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2560
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    mul-int/lit8 v5, v0, 0x7

    add-int/2addr v5, v1

    aget-object v4, v4, v5

    .line 2561
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    mul-int/lit8 v6, v0, 0x7

    add-int/2addr v6, v1

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2562
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    mul-int/lit8 v5, v0, 0x7

    add-int/2addr v5, v1

    aget-object v4, v4, v5

    invoke-virtual {v4, v9}, Landroid/view/View;->setFocusable(Z)V

    .line 2563
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    mul-int/lit8 v6, v0, 0x7

    add-int/2addr v6, v1

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2553
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private paragraphSetting()Landroid/view/ViewGroup;
    .locals 4

    .prologue
    .line 2107
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    .line 2108
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2109
    const/4 v1, -0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43300000    # 176.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 2108
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2110
    .local v0, "paragraphSettingLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2111
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2113
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    return-object v1
.end method

.method private rotatePosition()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    const v13, 0x3f7d70a4    # 0.99f

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 2704
    const-string v8, "settingui-settingText"

    const-string v9, "==== SettingText ===="

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2705
    const-string v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "old  = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 2706
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2705
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2707
    const-string v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "new  = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 2708
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2707
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2710
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 2712
    .local v4, "r":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    aget v8, v8, v11

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 2713
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    aget v8, v8, v14

    iput v8, v4, Landroid/graphics/Rect;->top:I

    .line 2714
    iget v8, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->right:I

    .line 2715
    iget v8, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 2717
    const-string v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "view = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2719
    iget v8, v4, Landroid/graphics/Rect;->left:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v9

    int-to-float v2, v8

    .line 2720
    .local v2, "left":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    iget v9, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v9

    int-to-float v5, v8

    .line 2721
    .local v5, "right":F
    iget v8, v4, Landroid/graphics/Rect;->top:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v9

    int-to-float v6, v8

    .line 2722
    .local v6, "top":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget v9, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v8, v9

    int-to-float v0, v8

    .line 2724
    .local v0, "bottom":F
    add-float v8, v2, v5

    div-float v1, v2, v8

    .line 2725
    .local v1, "hRatio":F
    add-float v8, v6, v0

    div-float v7, v6, v8

    .line 2727
    .local v7, "vRatio":F
    const-string v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "left :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", right :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2728
    const-string v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "top :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", bottom :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2729
    const-string v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "hRatio = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", vRatio = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2731
    cmpl-float v8, v1, v13

    if-lez v8, :cond_2

    .line 2732
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2737
    :cond_0
    :goto_0
    cmpl-float v8, v7, v13

    if-lez v8, :cond_3

    .line 2738
    const/high16 v7, 0x3f800000    # 1.0f

    .line 2743
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2745
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    if-ge v8, v9, :cond_4

    .line 2746
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, v1

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2751
    :goto_2
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    if-ge v8, v9, :cond_5

    .line 2752
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, v7

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2756
    :goto_3
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    iget v9, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    add-int/2addr v9, v10

    aput v9, v8, v11

    .line 2757
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    iget v9, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v10

    aput v9, v8, v14

    .line 2759
    const-string v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "lMargin = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", tMargin = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2760
    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    .line 2761
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2762
    return-void

    .line 2733
    .end local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_2
    cmpg-float v8, v1, v12

    if-gez v8, :cond_0

    .line 2734
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2739
    :cond_3
    cmpg-float v8, v7, v12

    if-gez v8, :cond_1

    .line 2740
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 2748
    .restart local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_4
    iput v11, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_2

    .line 2754
    :cond_5
    iput v11, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_3
.end method

.method private setListener()V
    .locals 4

    .prologue
    .line 1695
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1696
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1699
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    if-eqz v1, :cond_1

    .line 1700
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColorChangeListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setColorPickerColorChangeListener(Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;)V

    .line 1702
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    if-eqz v1, :cond_2

    .line 1703
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGrayColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setColorGrayScaleColorChangeListener(Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;)V

    .line 1705
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 1706
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1708
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    if-eqz v1, :cond_4

    .line 1710
    const/4 v0, 0x0

    .local v0, "mTextOptButton":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_9

    .line 1719
    .end local v0    # "mTextOptButton":I
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    if-eqz v1, :cond_5

    .line 1720
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpendBarListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1721
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpendBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1725
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    if-eqz v1, :cond_6

    .line 1726
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1729
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    if-eqz v1, :cond_7

    .line 1730
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1733
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    if-eqz v1, :cond_8

    .line 1734
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setOnScrollChangedListener(Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;)V

    .line 1737
    :cond_8
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetectorListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 1738
    return-void

    .line 1711
    .restart local v0    # "mTextOptButton":I
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v1, v1, v0

    if-eqz v1, :cond_a

    .line 1712
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v1, v1, v0

    instance-of v1, v1, Landroid/widget/ImageButton;

    if-eqz v1, :cond_a

    .line 1713
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextOptButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1710
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setTextStyle(Landroid/view/View;)V
    .locals 9
    .param p1, "paramView"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3227
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    if-nez v3, :cond_0

    .line 3263
    :goto_0
    return-void

    .line 3230
    :cond_0
    const/4 v1, 0x2

    .local v1, "mStyleButton":I
    :goto_1
    if-le v1, v6, :cond_3

    .line 3256
    :cond_1
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getTextStyle()C

    move-result v2

    .line 3257
    .local v2, "textType":C
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v3, :cond_2

    .line 3258
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    .line 3259
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iput v2, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 3260
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 3262
    .end local v0    # "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->invalidate()V

    goto :goto_0

    .line 3231
    .end local v2    # "textType":C
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 3232
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 3233
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3, v4}, Landroid/view/View;->setSelected(Z)V

    .line 3234
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 3235
    if-ne v1, v7, :cond_4

    .line 3236
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewBold(Z)V

    goto :goto_2

    .line 3237
    :cond_4
    if-ne v1, v8, :cond_5

    .line 3238
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSkewX(Z)V

    goto :goto_2

    .line 3239
    :cond_5
    if-ne v1, v6, :cond_1

    .line 3240
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewUnderLine(Z)V

    goto :goto_2

    .line 3243
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3, v4}, Landroid/view/View;->setSelected(Z)V

    .line 3244
    invoke-virtual {p1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 3245
    if-ne v1, v7, :cond_7

    .line 3246
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewBold(Z)V

    goto :goto_2

    .line 3247
    :cond_7
    if-ne v1, v8, :cond_8

    .line 3248
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSkewX(Z)V

    goto :goto_2

    .line 3249
    :cond_8
    if-ne v1, v6, :cond_1

    .line 3250
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewUnderLine(Z)V

    goto :goto_2

    .line 3230
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private textPreview()Landroid/view/View;
    .locals 9

    .prologue
    const/high16 v8, 0x41500000    # 13.0f

    const/4 v7, -0x1

    .line 1958
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1959
    .local v1, "mPreviewLayout":Landroid/widget/LinearLayout;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 1960
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x428e0000    # 71.0f

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1959
    invoke-direct {v3, v7, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1961
    .local v3, "previewLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41200000    # 10.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1962
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1963
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1964
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1966
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v5, "snote_popup_preview_bg"

    invoke-virtual {v4, v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1967
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    if-eqz v4, :cond_3

    .line 1968
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v4, v5, :cond_2

    .line 1969
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 1974
    :goto_0
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    const/16 v5, 0x5f0

    if-eq v4, v5, :cond_0

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    const/16 v5, 0x5fc

    if-ne v4, v5, :cond_1

    .line 1975
    :cond_0
    const/16 v4, 0x5a0

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 1986
    :cond_1
    :goto_1
    new-instance v4, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    invoke-direct {v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;-><init>(Landroid/content/Context;I)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    .line 1988
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 1989
    const/16 v4, 0x11

    .line 1988
    invoke-direct {v2, v7, v7, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 1990
    .local v2, "mPreviewParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, -0x3f600000    # -5.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1991
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v4, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1992
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1993
    return-object v1

    .line 1971
    .end local v2    # "mPreviewParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto :goto_0

    .line 1978
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1980
    .local v0, "dm":Landroid/util/DisplayMetrics;
    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v4, v5, :cond_4

    .line 1981
    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto :goto_1

    .line 1983
    :cond_4
    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto :goto_1
.end method

.method private titleBg()Landroid/view/View;
    .locals 14

    .prologue
    const/16 v13, 0x8

    const/4 v12, 0x1

    const/4 v11, -0x2

    const v10, 0x42fa999a    # 125.3f

    const/4 v9, -0x1

    .line 1358
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v7}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1359
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1361
    new-instance v3, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v3, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1362
    .local v3, "titleLeft":Landroid/widget/ImageView;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1363
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1362
    invoke-direct {v4, v7, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1364
    .local v4, "titleLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v12, v4, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1365
    const/16 v7, 0x9

    invoke-virtual {v4, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1366
    const/16 v7, 0xa

    invoke-virtual {v4, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1368
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1370
    new-instance v7, Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    .line 1371
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1373
    .local v2, "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1375
    new-instance v5, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1376
    .local v5, "titleRight":Landroid/widget/ImageView;
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1377
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1376
    invoke-direct {v6, v7, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1378
    .local v6, "titleRightParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v12, v6, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1379
    const/16 v7, 0xb

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1380
    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1381
    const/16 v7, 0x13

    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1382
    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1384
    new-instance v7, Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    .line 1385
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1387
    .local v1, "rightIndicatorParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v7, 0xb

    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1388
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1390
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v8, "snote_popup_title_left"

    invoke-virtual {v7, v3, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1391
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    const-string v9, "snote_popup_title_center"

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1392
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string v8, "snote_popup_title_right"

    invoke-virtual {v7, v5, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1393
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    const-string v9, "snote_popup_title_bended"

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1395
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1396
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1397
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1398
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1399
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1400
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1401
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1402
    return-object v0
.end method

.method private titleLayout()Landroid/view/ViewGroup;
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/high16 v6, 0x42240000    # 41.0f

    const/4 v5, 0x0

    .line 1327
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1328
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1329
    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v3, v7, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1328
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1330
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->exitButton()Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    .line 1331
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->titleBg()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1332
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->titleText()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1334
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1335
    .local v1, "mButtonLayout":Landroid/widget/RelativeLayout;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1336
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1335
    invoke-direct {v2, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1337
    .local v2, "mButtonLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1338
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 1339
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1340
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    add-int/lit8 v3, v3, 0x13

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1341
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_0

    .line 1342
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v1, v5, v5, v3, v5}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 1344
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1345
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1346
    return-object v0
.end method

.method private titleText()Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1406
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1407
    .local v0, "titleView":Landroid/widget/TextView;
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1408
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1409
    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 1410
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 1411
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1412
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1413
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_text_settings"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1414
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1415
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string v2, "string_text_settings"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1416
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/lit8 v1, v1, 0x9

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1417
    return-object v0
.end method

.method private totalLayout()V
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v11, -0x2

    const/high16 v10, 0x41d00000    # 26.0f

    const/high16 v9, 0x43770000    # 247.0f

    const/4 v8, 0x0

    .line 1274
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 1275
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    add-int/lit8 v5, v5, 0x13

    .line 1274
    invoke-direct {v2, v5, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1276
    .local v2, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1278
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setOrientation(I)V

    .line 1279
    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v6, 0x11

    if-lt v5, v6, :cond_0

    .line 1280
    invoke-virtual {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setLayoutDirection(I)V

    .line 1282
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->titleLayout()Landroid/view/ViewGroup;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    .line 1283
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->bodyLayout()Landroid/view/ViewGroup;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    .line 1284
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->addView(Landroid/view/View;)V

    .line 1285
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->addView(Landroid/view/View;)V

    .line 1287
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1288
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v7, v7, 0xfe

    int-to-float v7, v7

    .line 1287
    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    if-ge v5, v6, :cond_1

    .line 1289
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    if-eq v5, v11, :cond_1

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    if-nez v5, :cond_2

    .line 1290
    :cond_1
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 1291
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1292
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v7, v7, 0xfe

    add-int/lit8 v7, v7, 0x1a

    .line 1293
    add-int/lit8 v7, v7, -0x34

    int-to-float v7, v7

    .line 1292
    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1290
    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1294
    .local v3, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1296
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1297
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1298
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v7, v7, 0xfe

    add-int/lit8 v7, v7, 0x1a

    .line 1299
    add-int/lit8 v7, v7, -0x34

    int-to-float v7, v7

    .line 1298
    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1296
    invoke-direct {v0, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1300
    .local v0, "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1302
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1303
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1304
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1302
    invoke-direct {v1, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1306
    .local v1, "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v6, v6, 0xfe

    add-int/lit8 v6, v6, -0x34

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1305
    invoke-virtual {v1, v8, v5, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1308
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v5, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1309
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v6, v6, 0xfe

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    .line 1310
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1311
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1312
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v7, v7, 0xfe

    add-int/lit8 v7, v7, 0x1a

    int-to-float v7, v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1310
    invoke-direct {v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1313
    .local v4, "scrollLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v4, v8, v8, v8, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1314
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v5, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1316
    .end local v0    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v4    # "scrollLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    return-void
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1531
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    if-nez v1, :cond_0

    .line 1652
    :goto_0
    return-void

    .line 1535
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    if-eqz v1, :cond_1

    .line 1536
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->dismiss()V

    .line 1537
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    .line 1539
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    if-eqz v1, :cond_2

    .line 1540
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->dismiss()V

    .line 1541
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    .line 1544
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1545
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1546
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    .line 1547
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1548
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    .line 1549
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1550
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    .line 1551
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    .line 1552
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_7

    .line 1557
    .end local v0    # "i":I
    :cond_3
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    .line 1558
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1559
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    .line 1560
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1561
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    .line 1562
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1563
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/Button;

    .line 1565
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1566
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    .line 1567
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1568
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    .line 1569
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    if-eqz v1, :cond_4

    .line 1570
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->close()V

    .line 1571
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1572
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    .line 1574
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    if-eqz v1, :cond_5

    .line 1575
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->close()V

    .line 1576
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1577
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    .line 1579
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1580
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    .line 1581
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1582
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    .line 1583
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1584
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    .line 1585
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1586
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    .line 1587
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1588
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    .line 1589
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1590
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    .line 1591
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1592
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    .line 1594
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1595
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    .line 1596
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1597
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    .line 1599
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1600
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    .line 1601
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1602
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    .line 1603
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteView:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1604
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteView:Landroid/view/View;

    .line 1605
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPickerView:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1606
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPickerView:Landroid/view/View;

    .line 1607
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1608
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    .line 1610
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1611
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    .line 1612
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1613
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    .line 1614
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1615
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    .line 1616
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1617
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    .line 1618
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColor:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1619
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColor:Landroid/view/View;

    .line 1620
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSettingExitButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1621
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSettingExitButton:Landroid/view/View;

    .line 1623
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1624
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    .line 1625
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1626
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyBg:Landroid/view/View;

    .line 1627
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1628
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    .line 1629
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1630
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    .line 1632
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->close()V

    .line 1633
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1634
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    .line 1635
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 1636
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    .line 1637
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 1638
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    if-eqz v1, :cond_6

    .line 1639
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1640
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    .line 1643
    :cond_6
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    .line 1644
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    .line 1645
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 1646
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 1647
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1648
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1650
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->close()V

    .line 1651
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    goto/16 :goto_0

    .line 1553
    .restart local v0    # "i":I
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1554
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aput-object v3, v1, v0

    .line 1552
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method

.method public getInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    .prologue
    .line 3291
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    return-object v0
.end method

.method protected getSizeOption()I
    .locals 1

    .prologue
    .line 3552
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    return v0
.end method

.method public getViewMode()I
    .locals 1

    .prologue
    .line 2929
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 8
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 58
    const-string v3, "settingui-settingText"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onConfig text "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getVisibility()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    :try_start_0
    const-string v3, "settingui-settingText"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "old  = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 62
    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 61
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTempMovableRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 65
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupWhenScroll:Z

    .line 66
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->handlerRotate:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 67
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->handlerRotate:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupRunnable:Ljava/lang/Runnable;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 69
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getVisibility()I

    move-result v3

    if-nez v3, :cond_5

    .line 70
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 72
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 73
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTempMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 74
    const-string v1, "settingui-settingText"

    const-string v2, "Resote old moveable rect "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :cond_0
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    .line 94
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    if-eqz v1, :cond_1

    .line 95
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->setRotation()V

    .line 98
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    if-eqz v1, :cond_2

    .line 99
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->updatePosition()V

    .line 101
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    if-eqz v1, :cond_3

    .line 102
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->updatePosition()V

    .line 105
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 110
    :goto_1
    return-void

    .line 76
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLocationOnScreen([I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 80
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_5
    :try_start_1
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsFrirstShown:Z

    if-nez v3, :cond_0

    .line 81
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z

    if-eqz v3, :cond_6

    :goto_2
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z

    .line 82
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 83
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 84
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTempMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 85
    const-string v1, "settingui-settingText"

    const-string v2, "Resote old moveable rect "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_6
    move v1, v2

    .line 81
    goto :goto_2

    .line 87
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLocationOnScreen([I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected onScroll(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 3620
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 5
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 3785
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    .line 3818
    :goto_0
    return-void

    .line 3789
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    if-eqz v2, :cond_1

    .line 3790
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    .line 3791
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {p0, p1, v2, p3, p4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->onSizeChanged(IIII)V

    .line 3792
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->requestLayout()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3814
    :catch_0
    move-exception v0

    .line 3815
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 3797
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpandFlag:Z

    if-nez v2, :cond_2

    .line 3798
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41d00000    # 26.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    add-int/2addr v2, v3

    .line 3799
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42240000    # 41.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 3798
    add-int/2addr v2, v3

    .line 3799
    if-le v2, p2, :cond_2

    .line 3800
    const-string v2, "settingui-settingText"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onSizeChanged height "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3802
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42860000    # 67.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    sub-int v2, p2, v2

    .line 3801
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3804
    :cond_2
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 3805
    .local v1, "handler":Landroid/os/Handler;
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$24;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$24;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3812
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 11
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 2800
    if-ne p1, p0, :cond_0

    .line 2801
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    if-eqz v4, :cond_0

    .line 2803
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    invoke-interface {v4, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;->onVisibilityChanged(I)V

    .line 2806
    :cond_0
    if-ne p1, p0, :cond_6

    if-nez p2, :cond_6

    .line 2808
    const/4 v4, 0x2

    new-array v1, v4, [I

    .line 2809
    .local v1, "location":[I
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLocationInWindow([I)V

    .line 2811
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsFrirstShown:Z

    if-eqz v4, :cond_1

    .line 2812
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsFrirstShown:Z

    .line 2814
    :cond_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z

    if-eqz v4, :cond_4

    .line 2815
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2816
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    if-eqz v4, :cond_2

    .line 2817
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->rotatePosition()V

    .line 2818
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2820
    :cond_2
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2857
    .end local v1    # "location":[I
    :cond_3
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 2858
    return-void

    .line 2824
    .restart local v1    # "location":[I
    :cond_4
    :try_start_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedCalculateMargin:Z

    if-eqz v4, :cond_5

    .line 2825
    const/4 v4, 0x2

    new-array v2, v4, [I

    .line 2826
    .local v2, "parentLocation":[I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->getLocationInWindow([I)V

    .line 2828
    const/4 v4, 0x0

    aget v4, v1, v4

    const/4 v5, 0x0

    aget v5, v2, v5

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftMargin:I

    .line 2829
    const/4 v4, 0x1

    aget v4, v1, v4

    const/4 v5, 0x1

    aget v5, v2, v5

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTopMargin:I

    .line 2831
    const/4 v4, 0x2

    new-array v3, v4, [I

    .line 2832
    .local v3, "rootLocation":[I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2833
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedCalculateMargin:Z

    .line 2836
    .end local v2    # "parentLocation":[I
    .end local v3    # "rootLocation":[I
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2838
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    aget v6, v1, v6

    const/4 v7, 0x1

    aget v7, v1, v7

    const/4 v8, 0x0

    aget v8, v1, v8

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    const/4 v9, 0x1

    aget v9, v1, v9

    .line 2839
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getHeight()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2838
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v4

    .line 2839
    if-nez v4, :cond_3

    .line 2840
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->checkPosition()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2854
    .end local v1    # "location":[I
    :catch_0
    move-exception v0

    .line 2855
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 2844
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_6
    const/4 v4, 0x1

    :try_start_2
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    .line 2845
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z

    .line 2847
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    if-eqz v4, :cond_7

    .line 2848
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->dismiss()V

    .line 2850
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    if-eqz v4, :cond_3

    .line 2851
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->dismiss()V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 3694
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    if-nez v1, :cond_1

    .line 3711
    :cond_0
    :goto_0
    return-void

    .line 3697
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 3700
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3701
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->hide()V

    .line 3702
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v1, :cond_2

    .line 3703
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v2, 0x2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasPenAction:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 3704
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasFingerAction:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3710
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowVisibilityChanged(I)V

    goto :goto_0

    .line 3707
    :catch_0
    move-exception v0

    .line 3708
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method protected scroll(F)V
    .locals 1
    .param p1, "scrollYPosition"    # F

    .prologue
    .line 3598
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-gez v0, :cond_0

    .line 3600
    const/4 p1, 0x0

    .line 3602
    :cond_0
    return-void
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    .prologue
    .line 3514
    if-eqz p1, :cond_0

    .line 3515
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    .line 3517
    :cond_0
    return-void
.end method

.method public setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 0
    .param p1, "canvasView"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .prologue
    .line 3581
    if-eqz p1, :cond_0

    .line 3582
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 3584
    :cond_0
    return-void
.end method

.method public setColorPickerEnable(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 3939
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsColorPickerEnabled:Z

    .line 3940
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    if-eqz v0, :cond_0

    .line 3941
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsColorPickerEnabled:Z

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setColorPickerEnable(Z)V

    .line 3943
    :cond_0
    return-void
.end method

.method public setColorPickerPosition(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1908
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    if-eqz v0, :cond_0

    .line 1909
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->movePosition(II)V

    .line 1912
    :cond_0
    return-void
.end method

.method setExpandBarPosition(I)V
    .locals 13
    .param p1, "position"    # I

    .prologue
    const/high16 v12, 0x42aa0000    # 85.0f

    const/high16 v8, 0x42860000    # 67.0f

    const/4 v11, -0x1

    const/high16 v10, 0x41d00000    # 26.0f

    const/4 v9, 0x0

    .line 3138
    iget-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    if-nez v5, :cond_a

    .line 3139
    const/16 v5, 0x44

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaOfFirstTime:I

    .line 3143
    :goto_0
    const-string v5, "settingui-settingText"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "setExpandBarPosition position "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3144
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    if-lez v5, :cond_0

    .line 3145
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 3146
    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    sub-int/2addr v5, v6

    if-lt p1, v5, :cond_0

    .line 3147
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    sub-int/2addr v5, v6

    .line 3148
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 3147
    sub-int p1, v5, v6

    .line 3150
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3152
    .local v4, "expandParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput p1, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3154
    iget v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v7, v7, 0xfe

    .line 3155
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaOfFirstTime:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    .line 3154
    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    if-le v5, v6, :cond_1

    .line 3156
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v6, v6, 0xfe

    .line 3157
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaOfFirstTime:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    .line 3156
    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3158
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3159
    const/4 v5, -0x2

    .line 3158
    invoke-direct {v3, v11, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3161
    .local v3, "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v6, v6, 0xfe

    add-int/lit8 v6, v6, -0x1a

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 3160
    invoke-virtual {v3, v9, v5, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 3163
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3164
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3165
    const/4 v5, -0x2

    .line 3164
    invoke-direct {v1, v11, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3166
    .local v1, "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v5, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3168
    .end local v1    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    if-lez v5, :cond_2

    .line 3169
    iget v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 3170
    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v6

    if-le v5, v6, :cond_2

    .line 3171
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    .line 3172
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    sub-int/2addr v5, v6

    .line 3173
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x42240000    # 41.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    sub-int/2addr v5, v6

    .line 3171
    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3175
    iget v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v5, :cond_2

    .line 3176
    iput v9, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3180
    :cond_2
    iget v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    if-ge v5, v6, :cond_3

    .line 3181
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3183
    :cond_3
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    .line 3184
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x42f00000    # 120.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3186
    :cond_4
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v6, 0x2

    if-eq v5, v6, :cond_5

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_6

    .line 3187
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x421c0000    # 39.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3189
    :cond_6
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_7

    .line 3190
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x43060000    # 134.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3192
    :cond_7
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v6, 0x5

    if-ne v5, v6, :cond_8

    .line 3193
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x43300000    # 176.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3196
    :cond_8
    iget v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    .line 3198
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 3199
    .local v2, "bodyParam":Landroid/widget/LinearLayout$LayoutParams;
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 3200
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v5, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3202
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v6, 0x4

    if-eq v5, v6, :cond_9

    .line 3203
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3204
    .local v0, "PaletteBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput v11, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 3206
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    if-ltz v5, :cond_9

    .line 3207
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3210
    .end local v0    # "PaletteBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_9
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3212
    .restart local v3    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    invoke-virtual {v3, v9, v5, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 3213
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3214
    return-void

    .line 3141
    .end local v2    # "bodyParam":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v4    # "expandParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_a
    iput v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaOfFirstTime:I

    goto/16 :goto_0
.end method

.method public setIndicatorPosition(I)V
    .locals 11
    .param p1, "position"    # I

    .prologue
    const/4 v10, -0x1

    const/high16 v9, 0x43770000    # 247.0f

    const/high16 v8, 0x41f00000    # 30.0f

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 1774
    if-gez p1, :cond_1

    .line 1775
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1776
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1777
    const/16 v1, -0x63

    if-ne p1, v1, :cond_0

    .line 1778
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    .line 1779
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFirstLongPress:Z

    .line 1814
    :goto_0
    return-void

    .line 1781
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    goto :goto_0

    .line 1784
    :cond_1
    const/16 v1, 0x9

    if-ge p1, v1, :cond_2

    .line 1785
    const/16 p1, 0x9

    .line 1787
    :cond_2
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    .line 1788
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1789
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_4

    .line 1790
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1791
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x9

    if-le p1, v1, :cond_3

    .line 1792
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1793
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1795
    :cond_3
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1796
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1795
    invoke-direct {v0, v1, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1797
    .local v0, "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, p1, v1, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1798
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1799
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1802
    .end local v0    # "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/lit8 v1, v1, -0x2d

    add-int/lit8 v1, v1, -0x9

    if-le p1, v1, :cond_5

    .line 1803
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1804
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1806
    :cond_5
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1807
    const/4 v1, -0x2

    .line 1806
    invoke-direct {v0, v1, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1808
    .restart local v0    # "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, p1, v6, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1809
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1810
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public setInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 12
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    .line 3326
    if-nez p1, :cond_0

    .line 3492
    :goto_0
    return-void

    .line 3330
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 3331
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 3332
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 3333
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 3334
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 3335
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 3336
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    .line 3337
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    .line 3338
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v7, :cond_1

    .line 3339
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v7}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v1

    .line 3340
    .local v1, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 3341
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 3342
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 3343
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 3344
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 3345
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 3346
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    .line 3347
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    .line 3348
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v7, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 3351
    .end local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextColor(I)V

    .line 3352
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSize(F)V

    .line 3353
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v8}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTypeface(Landroid/graphics/Typeface;)V

    .line 3354
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->setColorPickerColor(I)V

    .line 3356
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v7, :cond_9

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    if-eqz v7, :cond_9

    .line 3357
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v8, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v7, v8, :cond_8

    .line 3358
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 3363
    :goto_1
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    if-eqz v7, :cond_2

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    const/16 v8, 0x5f0

    if-eq v7, v8, :cond_2

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    const/16 v8, 0x5fc

    if-ne v7, v8, :cond_3

    .line 3364
    :cond_2
    const/16 v7, 0x5a0

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 3370
    :cond_3
    :goto_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v7}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    .line 3371
    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v8, 0x10

    if-ge v7, v8, :cond_d

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    invoke-virtual {v7}, Landroid/widget/Button;->getWidth()I

    move-result v7

    if-lez v7, :cond_d

    .line 3372
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    .line 3373
    .local v5, "mTempFontName":Ljava/lang/String;
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 3374
    .local v3, "mPaint":Landroid/graphics/Paint;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    if-eqz v7, :cond_c

    .line 3375
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 3376
    .local v6, "mTextWidth":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    invoke-virtual {v7}, Landroid/widget/Button;->getWidth()I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x42040000    # 33.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    sub-int/2addr v7, v8

    if-lt v6, v7, :cond_b

    .line 3377
    :goto_3
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    invoke-virtual {v7}, Landroid/widget/Button;->getWidth()I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x42040000    # 33.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    sub-int/2addr v7, v8

    if-gt v6, v7, :cond_a

    .line 3382
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3393
    .end local v3    # "mPaint":Landroid/graphics/Paint;
    .end local v5    # "mTempFontName":Ljava/lang/String;
    .end local v6    # "mTextWidth":I
    :goto_4
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v8}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 3395
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    int-to-double v8, v8

    const-wide v10, 0x4076800000000000L    # 360.0

    div-double/2addr v8, v10

    double-to-float v8, v8

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    if-gtz v7, :cond_e

    .line 3396
    const-string v7, ""

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    .line 3401
    :goto_5
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3403
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    if-eqz v7, :cond_4

    .line 3404
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    if-nez v7, :cond_f

    .line 3405
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3406
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3407
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3419
    :cond_4
    :goto_6
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setColor(I)V

    .line 3421
    sget-boolean v7, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->IS_COLOR_GRADATION_SELECT:Z

    if-eqz v7, :cond_6

    .line 3422
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    if-nez v7, :cond_5

    .line 3423
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    .line 3424
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3425
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGrayScaleView:Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3426
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x1

    .line 3427
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x430c0000    # 140.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 3426
    invoke-direct {v2, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3428
    .local v2, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v7, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3429
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v8, v8, 0xfe

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3431
    .end local v2    # "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_5
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 3432
    const/high16 v9, 0x41a00000    # 20.0f

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    float-to-double v10, v9

    .line 3431
    invoke-virtual {v7, v8, v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->selectColorForGrayScale(ID)V

    .line 3433
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->getGrayCursoVisibility()Z

    move-result v7

    if-nez v7, :cond_11

    .line 3434
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 3435
    const/high16 v9, 0x41a00000    # 20.0f

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    float-to-double v10, v9

    .line 3434
    invoke-virtual {v7, v8, v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->selectColorForGradiation(ID)V

    .line 3441
    :cond_6
    :goto_7
    const/4 v4, 0x2

    .local v4, "mStyleButton":I
    :goto_8
    const/4 v7, 0x4

    if-le v4, v7, :cond_12

    .line 3482
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    if-eqz v7, :cond_7

    .line 3483
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_9
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v7}, Landroid/widget/Spinner;->getCount()I

    move-result v7

    if-lt v0, v7, :cond_16

    .line 3491
    .end local v0    # "i":I
    :cond_7
    :goto_a
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->invalidate()V

    goto/16 :goto_0

    .line 3360
    .end local v4    # "mStyleButton":I
    :cond_8
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto/16 :goto_1

    .line 3367
    :cond_9
    const/16 v7, 0x5a0

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto/16 :goto_2

    .line 3378
    .restart local v3    # "mPaint":Landroid/graphics/Paint;
    .restart local v5    # "mTempFontName":Ljava/lang/String;
    .restart local v6    # "mTextWidth":I
    :cond_a
    const/4 v7, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 3379
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    .line 3380
    const-string v9, "..."

    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v9

    add-float/2addr v8, v9

    .line 3379
    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    goto/16 :goto_3

    .line 3384
    :cond_b
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 3387
    .end local v6    # "mTextWidth":I
    :cond_c
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    const-string v8, ""

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 3391
    .end local v3    # "mPaint":Landroid/graphics/Paint;
    .end local v5    # "mTempFontName":Ljava/lang/String;
    :cond_d
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 3398
    :cond_e
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    int-to-double v8, v8

    const-wide v10, 0x4076800000000000L    # 360.0

    div-double/2addr v8, v10

    double-to-float v8, v8

    div-float/2addr v7, v8

    const/high16 v8, 0x3f000000    # 0.5f

    sub-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    goto/16 :goto_5

    .line 3408
    :cond_f
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_10

    .line 3409
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3410
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3411
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto/16 :goto_6

    .line 3412
    :cond_10
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_4

    .line 3413
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3414
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3415
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto/16 :goto_6

    .line 3437
    :cond_11
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setGradientCursorRectVisibility(Z)V

    goto/16 :goto_7

    .line 3442
    .restart local v4    # "mStyleButton":I
    :cond_12
    packed-switch v4, :pswitch_data_0

    .line 3441
    :goto_b
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_8

    .line 3444
    :pswitch_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    and-int/lit8 v7, v7, 0x1

    const/4 v8, 0x1

    if-ne v7, v8, :cond_13

    .line 3445
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v7, v7, v4

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    .line 3446
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3447
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewBold(Z)V

    goto :goto_b

    .line 3449
    :cond_13
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v7, v7, v4

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    .line 3450
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3451
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewBold(Z)V

    goto :goto_b

    .line 3456
    :pswitch_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    and-int/lit8 v7, v7, 0x2

    const/4 v8, 0x2

    if-ne v7, v8, :cond_14

    .line 3457
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v7, v7, v4

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    .line 3458
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3459
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSkewX(Z)V

    goto :goto_b

    .line 3461
    :cond_14
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v7, v7, v4

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    .line 3462
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3463
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSkewX(Z)V

    goto :goto_b

    .line 3468
    :pswitch_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    and-int/lit8 v7, v7, 0x4

    const/4 v8, 0x4

    if-ne v7, v8, :cond_15

    .line 3469
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v7, v7, v4

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    .line 3470
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3471
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewUnderLine(Z)V

    goto/16 :goto_b

    .line 3473
    :cond_15
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v7, v7, v4

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    .line 3474
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3475
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewUnderLine(Z)V

    goto/16 :goto_b

    .line 3484
    .restart local v0    # "i":I
    :cond_16
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v7, v0}, Landroid/widget/Spinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    int-to-float v7, v7

    cmpl-float v7, v8, v7

    if-nez v7, :cond_17

    .line 3485
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v7, v0}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_a

    .line 3483
    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_9

    .line 3442
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setPosition(II)V
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v6, 0x0

    .line 1849
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    .line 1850
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->handlerRotate:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->runnableRotate:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1851
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->handlerRotate:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->runnableRotate:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1853
    const-string v2, "settingui-settingText"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "set Position x,y : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1855
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1857
    .local v1, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43770000    # 247.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/lit8 v0, v2, 0x13

    .line 1859
    .local v0, "minWidth":I
    if-gez p1, :cond_4

    .line 1860
    const/4 p1, 0x0

    .line 1865
    :cond_0
    :goto_0
    if-gez p2, :cond_5

    .line 1866
    const/4 p2, 0x0

    .line 1871
    :cond_1
    :goto_1
    iput p1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1872
    iput p2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1873
    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    .line 1874
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1875
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1876
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, p1

    aput v3, v2, v6

    .line 1877
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, p2

    aput v4, v2, v3

    .line 1879
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    if-eqz v2, :cond_2

    .line 1880
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->updatePosition()V

    .line 1882
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    if-eqz v2, :cond_3

    .line 1883
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->updatePosition()V

    .line 1885
    :cond_3
    return-void

    .line 1861
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v2, v0

    if-le p1, v2, :cond_0

    .line 1862
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v2, v0

    add-int/lit8 p1, v2, -0x2

    goto :goto_0

    .line 1867
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    sub-int/2addr v2, v3

    if-le p2, v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    if-le v2, v3, :cond_1

    .line 1868
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    sub-int p2, v2, v3

    goto :goto_1
.end method

.method public setViewMode(I)V
    .locals 12
    .param p1, "viewMode"    # I

    .prologue
    const/high16 v11, 0x421c0000    # 39.0f

    const/high16 v10, 0x41d00000    # 26.0f

    const/high16 v9, 0x42240000    # 41.0f

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 2959
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    .line 2960
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->isShown()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2961
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->hide()V

    .line 2962
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v4, :cond_0

    .line 2963
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v5, 0x2

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasPenAction:I

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 2964
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasFingerAction:I

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 2967
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setScrollingEnabled(Z)V

    .line 2968
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setColor(I)V

    .line 2969
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    packed-switch v4, :pswitch_data_0

    .line 3117
    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    .line 3118
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3119
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3120
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3121
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3122
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 3123
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->invalidate()V

    .line 3124
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3125
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3126
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3128
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3129
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3130
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3131
    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeightNormal:I

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    .line 3134
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->checkPosition()V

    .line 3135
    return-void

    .line 2971
    :pswitch_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2972
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2973
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2974
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2975
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 2976
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->invalidate()V

    .line 2977
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2978
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2979
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2981
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2983
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2984
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2985
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v5, 0x43938000    # 295.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 2987
    .local v2, "mModeNormalHeigh":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    if-ne v4, v5, :cond_2

    .line 2988
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v5, v5, 0xfe

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 2990
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    if-lez v4, :cond_3

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    if-le v4, v5, :cond_3

    .line 2991
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    .line 2992
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 2991
    sub-int/2addr v4, v5

    .line 2993
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 2991
    sub-int v2, v4, v5

    .line 2995
    :cond_3
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 2996
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    if-nez v4, :cond_4

    .line 2997
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    .line 2998
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x42900000    # 72.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 2997
    invoke-direct {v0, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2999
    .local v0, "colorPickerParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3004
    .end local v0    # "colorPickerParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_1
    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeightNormal:I

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    goto/16 :goto_0

    .line 3001
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3002
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    goto :goto_1

    .line 3008
    .end local v2    # "mModeNormalHeigh":I
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3009
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3010
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3011
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3012
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 3013
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3014
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3015
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3017
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3018
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3019
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    .line 3020
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3021
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3022
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42f00000    # 120.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3023
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x433b0000    # 187.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    goto/16 :goto_0

    .line 3026
    :pswitch_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3027
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3028
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3029
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3030
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 3031
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3032
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3033
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3035
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3036
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3037
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    .line 3038
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3039
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3040
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3041
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42d40000    # 106.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    goto/16 :goto_0

    .line 3044
    :pswitch_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3045
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3046
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3047
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3048
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 3049
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3050
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3051
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3053
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3054
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3055
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    .line 3056
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3057
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3058
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3059
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42d40000    # 106.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    goto/16 :goto_0

    .line 3062
    :pswitch_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3063
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3064
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3065
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 3066
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3067
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3068
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3070
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3071
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3072
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3073
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    .line 3074
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3075
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3076
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43020000    # 130.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3077
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    .line 3078
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x430c0000    # 140.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 3077
    invoke-direct {v1, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3079
    .local v1, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3080
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3081
    .local v3, "palletBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x430e0000    # 142.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 3082
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3083
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43490000    # 201.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    goto/16 :goto_0

    .line 3086
    .end local v1    # "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "palletBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_5
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->addParagraphSettingLayout()V

    .line 3087
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3088
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3089
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3090
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3091
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 3092
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3093
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3094
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3096
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3097
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3098
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    .line 3099
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3100
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3101
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setScrollingEnabled(Z)V

    .line 3102
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43300000    # 176.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3103
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43730000    # 243.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    goto/16 :goto_0

    .line 3107
    :pswitch_6
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3108
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3109
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    if-lez v4, :cond_1

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    if-le v4, v5, :cond_1

    .line 3110
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    .line 3111
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    sub-int/2addr v4, v5

    .line 3112
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    sub-int/2addr v4, v5

    .line 3110
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 2969
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setVisibility(I)V
    .locals 8
    .param p1, "visibility"    # I

    .prologue
    const-wide v6, 0x4076800000000000L    # 360.0

    .line 3637
    const/4 v1, 0x1

    .line 3638
    .local v1, "mIsInit":Z
    if-nez p1, :cond_2

    .line 3639
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-boolean v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-nez v2, :cond_2

    .line 3640
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->loadImage()V

    .line 3642
    if-eqz v1, :cond_2

    .line 3643
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    if-eqz v2, :cond_4

    .line 3644
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v2, v3, :cond_3

    .line 3645
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 3649
    :goto_0
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    const/16 v3, 0x5f0

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    const/16 v3, 0x5fc

    if-ne v2, v3, :cond_1

    .line 3650
    :cond_0
    const-string v2, "settingui-settingText"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "canvas size: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3651
    const/16 v2, 0x5a0

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 3656
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    .line 3657
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3658
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 3660
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    int-to-double v4, v3

    div-double/2addr v4, v6

    double-to-float v3, v4

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    if-gtz v2, :cond_5

    .line 3661
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    .line 3667
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3669
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSize(F)V

    .line 3670
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTypeface(Landroid/graphics/Typeface;)V

    .line 3671
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->invalidate()V

    .line 3675
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    if-nez v2, :cond_6

    .line 3689
    :goto_3
    return-void

    .line 3647
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 3686
    :catch_0
    move-exception v0

    .line 3687
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_3

    .line 3654
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_4
    const/16 v2, 0x5a0

    :try_start_1
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto :goto_1

    .line 3663
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 3664
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    int-to-double v4, v3

    div-double/2addr v4, v6

    double-to-float v3, v4

    div-float/2addr v2, v3

    const/high16 v3, 0x3f000000    # 0.5f

    sub-float/2addr v2, v3

    .line 3663
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    goto :goto_2

    .line 3678
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isShown()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 3679
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->hide()V

    .line 3680
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_7

    .line 3681
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v3, 0x2

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasPenAction:I

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 3682
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v3, 0x1

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasFingerAction:I

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 3685
    :cond_7
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method public setVisibilityChangedListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    .prologue
    .line 3538
    if-eqz p1, :cond_0

    .line 3539
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    .line 3541
    :cond_0
    return-void
.end method

.method protected textSettingScroll()Landroid/view/View;
    .locals 12

    .prologue
    const/high16 v11, 0x40f00000    # 7.5f

    const/4 v10, 0x0

    const/4 v9, -0x1

    const/high16 v8, 0x40400000    # 3.0f

    .line 2869
    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;-><init>(Landroid/content/Context;)V

    .line 2870
    .local v3, "localThumbControlBackGround":Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40800000    # 4.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->setTrackWidth(I)V

    .line 2871
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->setTopPadding(I)V

    .line 2872
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2873
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41c80000    # 25.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 2872
    invoke-direct {v0, v4, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2874
    .local v0, "localLayoutParams1":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2875
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41000000    # 8.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2876
    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2877
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 2878
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2879
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 2877
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->setPadding(IIII)V

    .line 2881
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOptionBgPath:Ljava/lang/String;

    invoke-virtual {v4, v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 2882
    new-instance v2, Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2883
    .local v2, "localRelativeLayout":Landroid/widget/RelativeLayout;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2884
    invoke-direct {v4, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2883
    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2885
    new-instance v4, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    .line 2886
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2887
    const/4 v4, -0x2

    .line 2886
    invoke-direct {v1, v9, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2888
    .local v1, "localLayoutParams2":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2889
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2890
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v4, v10, v5, v10, v10}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 2891
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandelNormal:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2892
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2893
    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->addView(Landroid/view/View;)V

    .line 2894
    return-object v3
.end method
