.class Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$3;
.super Ljava/lang/Object;
.source "Video.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->startVideoPlay()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$3;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    .line 375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 379
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/Object;

    .line 380
    .local v0, "args":[Ljava/lang/Object;
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v2

    .line 381
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$3;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$3;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "setVisibility"

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETVISIBILITY_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    invoke-static {}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$19()[Ljava/lang/Class;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    invoke-static {v1, v2, v3, v4, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$11(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$3;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$17(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$3;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$7(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Landroid/graphics/RectF;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$3;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;->onObjectUpdated(Landroid/graphics/RectF;Ljava/lang/Object;)V

    .line 383
    return-void
.end method
