.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;
.super Ljava/lang/Object;
.source "NRRPenSettings.java"


# instance fields
.field private mColor:I

.field private mPenName:Ljava/lang/String;

.field private mPenSize:F


# direct methods
.method constructor <init>(Ljava/lang/String;FI)V
    .locals 0
    .param p1, "penName"    # Ljava/lang/String;
    .param p2, "penSize"    # F
    .param p3, "color"    # I

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;->mPenName:Ljava/lang/String;

    .line 11
    iput p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;->mPenSize:F

    .line 12
    iput p3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;->mColor:I

    .line 13
    return-void
.end method


# virtual methods
.method public getColor()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;->mColor:I

    return v0
.end method

.method public getPenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;->mPenName:Ljava/lang/String;

    return-object v0
.end method

.method public getPenSize()F
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;->mPenSize:F

    return v0
.end method
