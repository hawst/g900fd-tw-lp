.class Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;
.super Ljava/lang/Object;
.source "SpenMultiView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenMultiView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnGestureDoubleTapListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V
    .locals 0

    .prologue
    .line 2979
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;)V
    .locals 0

    .prologue
    .line 2979
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    .line 2983
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsDoubleTap:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2998
    :cond_0
    :goto_0
    return v5

    .line 2986
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 2987
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 2988
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 2989
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 2990
    const-string v0, "SpenMultiView"

    const-string v1, "one finger double tab"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2991
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 2992
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setZoom(FFF)V

    .line 2996
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->invalidate()V

    goto :goto_0

    .line 2994
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setZoom(FFF)V

    goto :goto_1
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 3003
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 3008
    const/4 v0, 0x0

    return v0
.end method
