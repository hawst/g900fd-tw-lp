.class Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;
.super Ljava/lang/Object;
.source "SpenImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StateImage"
.end annotation


# instance fields
.field private mFocusPathList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHeightList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNormalPathList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPressPathList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRippleColorList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mViewList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mWidthList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mRippleColorList:Ljava/util/ArrayList;

    .line 42
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mRippleColorList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;

    return-void
.end method
