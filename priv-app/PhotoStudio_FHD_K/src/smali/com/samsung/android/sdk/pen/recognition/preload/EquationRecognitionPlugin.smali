.class public Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;
.super Ljava/lang/Object;
.source "EquationRecognitionPlugin.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;,
        Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "recognition-EquationRecognitionPlugin"

.field private static mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;


# instance fields
.field private final mHandler:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .line 142
    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mHandler:Landroid/os/Handler;

    .line 53
    const-string v0, "recognition-EquationRecognitionPlugin"

    const-string v1, "creating equation recognition plugin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    return-object v0
.end method

.method static synthetic access$1()Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 221
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 205
    return-void
.end method

.method public initRecognizer(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 61
    const-class v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;

    monitor-enter v1

    .line 62
    :try_start_0
    sget-object v2, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    if-nez v2, :cond_1

    .line 63
    new-instance v2, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    invoke-direct {v2, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    .line 65
    sget-object v2, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    if-nez v2, :cond_0

    .line 66
    const-string v2, "recognition-EquationRecognitionPlugin"

    const-string v3, "Fail to create Equation recognition instance."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    monitor-exit v1

    .line 76
    :goto_0
    return v0

    .line 70
    :cond_0
    sget-object v2, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->init(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 71
    const-string v2, "recognition-EquationRecognitionPlugin"

    const-string v3, "Fail to initialize."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    monitor-exit v1

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 180
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->initRecognizer(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Fail to load Equation recognition engine"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :cond_0
    return-void
.end method

.method public onUnload()V
    .locals 2

    .prologue
    .line 191
    const-class v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;

    monitor-enter v1

    .line 192
    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    if-eqz v0, :cond_0

    .line 193
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->dispose()V

    .line 194
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    .line 191
    :cond_0
    monitor-exit v1

    .line 197
    return-void

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public request(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 158
    .local p1, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    if-nez v1, :cond_0

    .line 159
    const-string v1, "recognition-EquationRecognitionPlugin"

    const-string v2, "The result listener isn\'t set yet!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 164
    :cond_0
    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    if-eqz v1, :cond_1

    .line 165
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 166
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 172
    .end local v0    # "thread":Ljava/lang/Thread;
    :goto_0
    return-void

    .line 169
    :cond_1
    const-string v1, "recognition-EquationRecognitionPlugin"

    const-string v2, "The recognition engine is null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 213
    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .prologue
    .line 230
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .line 231
    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 239
    const/4 v0, 0x1

    return v0
.end method
