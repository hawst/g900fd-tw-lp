.class Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
.super Landroid/view/View;
.source "SpenNotePad.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    }
.end annotation


# static fields
.field private static final PAD_BG_COLOR:I = 0x33000000

.field private static final SIGMA:F = 1.0E-4f

.field private static final STATE_BOX_MOVE:I = 0x1

.field private static final STATE_BOX_RESIZE:I = 0x2

.field private static final STATE_BTN_DOWN:I = 0xb

.field private static final STATE_BTN_ENTER:I = 0x8

.field private static final STATE_BTN_LEFT:I = 0x6

.field private static final STATE_BTN_RIGHT:I = 0x7

.field private static final STATE_BTN_UP:I = 0xa

.field private static final STATE_DRAW:I = 0x9

.field private static final STATE_NONE:I = 0x0

.field private static final STATE_PAD_MOVE:I = 0x3

.field private static final STATE_PAD_MOVING_RECT:I = 0x5

.field private static final STATE_PAD_NONE:I = 0x4

.field private static final STROKE_BOX_LINE_COLOR:I = -0xf4742a

.field private static final STROKE_BOX_WIDTH:I = 0x2

.field public static final TAG:Ljava/lang/String; = "ZoomPad"

.field private static final penNameBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Brush"

.field private static final penNameChineseBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

.field private static final penNameFountainPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.FountainPen"

.field private static final penNameInkPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.InkPen"

.field private static final penNameMagicPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.MagicPen"

.field private static final penNameMarker:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Marker"

.field private static final penNameObliquePen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

.field private static final penNamePencil:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Pencil"


# instance fields
.field private BUTTON_HEIGHT:I

.field private BUTTON_LINE_HEIGHT:I

.field private BUTTON_LINE_WIDTH:I

.field private BUTTON_WIDTH:I

.field private PAD_HANDLE_HEIGHT:I

.field private PAD_HANDLE_WIDTH:I

.field private STROKE_BOX_HEIGHT_LIMIT:I

.field private STROKE_BOX_HEIGHT_LIMIT_FLOAT:F

.field private STROKE_BOX_RESIZE:I

.field private STROKE_BOX_RESIZE_GAP:I

.field private activePen:I

.field private bIsSupport:Z

.field dstRect:Landroid/graphics/RectF;

.field private isSoundEnabled:Z

.field private mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

.field private mAntiAliasPaint:Landroid/graphics/Paint;

.field private mAutoMoveArea:F

.field private mAutoMovingRect:Landroid/graphics/RectF;

.field private mBitmap:[Landroid/graphics/Bitmap;

.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private mBoxHeight:F

.field private mBoxHeightChange:Z

.field private mBoxPaint:Landroid/graphics/Paint;

.field private mBoxRate:F

.field private mBoxSaveStartX:F

.field private mBoxSaveStartY:F

.field private mBoxStartX:F

.field private mBoxStartY:F

.field private mContext:Landroid/content/Context;

.field private mDeltaSaveX:F

.field private mDeltaX:F

.field private mDeltaY:F

.field private mDownButton:Z

.field private mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

.field private mEnable:Z

.field private mEnterButton:Z

.field private mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

.field private mIndexBrush:I

.field private mIndexEraser:I

.field private mIndexMarker:I

.field private mIndexPencil:I

.field private mIsMultiTouch:Z

.field private mIsStrokeDrawing:Z

.field private mIsTablet:Z

.field private mLeftButton:Z

.field private mMaxDeltaX:F

.field private mMaxDeltaY:F

.field private mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

.field private mOldX:F

.field private mOldY:F

.field private final mOnePT:F

.field private mPadBarRect:Landroid/graphics/RectF;

.field private mPadMargin:F

.field private mPadPaint:Landroid/graphics/Paint;

.field private mPadRect:Landroid/graphics/RectF;

.field private mParent:Landroid/view/ViewGroup;

.field private mPrePoint:Landroid/graphics/PointF;

.field private mRatio:F

.field private mReferenceBackground:Landroid/graphics/Bitmap;

.field private mRelative:Landroid/widget/RelativeLayout;

.field private mRightButton:Z

.field private mSaveMaxDeltaX:F

.field private mSaveMaxDeltaY:F

.field private mSaveRatio:F

.field private mScreenHeight:I

.field private mScreenStartX:I

.field private mScreenStartY:I

.field private mScreenWidth:I

.field private mSdkResources:Landroid/content/res/Resources;

.field private mSmps:Lcom/samsung/audio/SmpsManager;

.field private mState:I

.field private mToolAndActionMap:Landroid/util/SparseIntArray;

.field private mUpButton:Z

.field private preEvent:Landroid/view/MotionEvent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 175
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 59
    const/16 v3, 0x11

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    .line 60
    const/4 v3, 0x7

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    .line 61
    const/16 v3, 0x1e

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    .line 62
    const/high16 v3, 0x41f00000    # 30.0f

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT_FLOAT:F

    .line 63
    const/16 v3, 0x1f

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    .line 64
    const/16 v3, 0x22

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    .line 65
    const/16 v3, 0x71

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 66
    const/16 v3, 0x31

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 67
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    .line 68
    const/16 v3, 0x1a

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    .line 82
    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    .line 86
    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    .line 89
    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    .line 96
    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmapWidth:I

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmapHeight:I

    .line 101
    new-instance v3, Landroid/util/SparseIntArray;

    const/16 v4, 0xa

    invoke-direct {v3, v4}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    .line 104
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    .line 105
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 106
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsMultiTouch:Z

    .line 107
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeightChange:Z

    .line 108
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mLeftButton:Z

    .line 109
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRightButton:Z

    .line 110
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnterButton:Z

    .line 111
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpButton:Z

    .line 112
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDownButton:Z

    .line 114
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 115
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadMargin:F

    .line 116
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 117
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    .line 118
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartY:F

    .line 119
    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    .line 120
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    .line 121
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    .line 122
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSaveMaxDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSaveMaxDeltaY:F

    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSaveRatio:F

    .line 129
    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    .line 133
    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 135
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldX:F

    .line 136
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldY:F

    .line 139
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    .line 140
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    .line 141
    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 142
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexPencil:I

    .line 143
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexMarker:I

    .line 144
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexBrush:I

    .line 145
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexEraser:I

    .line 146
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    .line 866
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    .line 1206
    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    .line 176
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    .line 178
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 179
    .local v1, "mDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v3, v1, Landroid/util/DisplayMetrics;->density:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    .line 181
    const/4 v3, 0x4

    new-array v3, v3, [Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    .line 182
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    .line 183
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x33000000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 185
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 186
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 188
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxPaint:Landroid/graphics/Paint;

    .line 189
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 190
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40000000    # 2.0f

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 191
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxPaint:Landroid/graphics/Paint;

    const v4, -0xf4742a

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 193
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v6, v6}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    .line 194
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    .line 195
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    .line 196
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    .line 198
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    .line 200
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 201
    .local v2, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    .end local v2    # "manager":Landroid/content/pm/PackageManager;
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v7, v7}, Landroid/util/SparseIntArray;->put(II)V

    .line 207
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    const/4 v4, 0x2

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 208
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    const/4 v4, 0x4

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 209
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    const/4 v4, 0x3

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 211
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;-><init>(Landroid/content/Context;F)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    .line 212
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;)V

    .line 277
    return-void

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dstRectF"    # Landroid/graphics/RectF;
    .param p2, "srcRectF"    # Landroid/graphics/RectF;

    .prologue
    .line 280
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 281
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 282
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 283
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 284
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    return v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 279
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    return v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    return v0
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    return v0
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    return-object v0
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    return v0
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V
    .locals 0

    .prologue
    .line 114
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    return-void
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    return v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    return-object v0
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V
    .locals 0

    .prologue
    .line 122
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSaveMaxDeltaX:F

    return-void
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    return v0
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V
    .locals 0

    .prologue
    .line 122
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSaveMaxDeltaY:F

    return-void
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V
    .locals 0

    .prologue
    .line 122
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSaveRatio:F

    return-void
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mLeftButton:Z

    return v0
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRightButton:Z

    return v0
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpButton:Z

    return v0
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDownButton:Z

    return v0
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnterButton:Z

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V
    .locals 0

    .prologue
    .line 114
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    return-void
.end method

.method private convertPenNameToMaxThicknessValue(Ljava/lang/String;)I
    .locals 2
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 531
    const/4 v0, 0x0

    .line 533
    .local v0, "maxValue":I
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 534
    :cond_0
    const/16 v0, 0x40

    .line 545
    :cond_1
    :goto_0
    return v0

    .line 535
    :cond_2
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 536
    :cond_3
    const/16 v0, 0x20

    .line 537
    goto :goto_0

    :cond_4
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 538
    :cond_5
    const/16 v0, 0x50

    .line 539
    goto :goto_0

    :cond_6
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 540
    const/16 v0, 0x6c

    .line 541
    goto :goto_0

    :cond_7
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "Eraser"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 542
    :cond_8
    const/16 v0, 0x64

    goto :goto_0
.end method

.method private initHapticFeedback()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 466
    const-string v2, "ZoomPad"

    const-string v3, "initHapticFeedback() - Start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-nez v2, :cond_0

    .line 469
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 470
    .local v0, "dm":Landroid/util/DisplayMetrics;
    new-instance v2, Lcom/samsung/hapticfeedback/HapticEffect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/hapticfeedback/HapticEffect;-><init>(Landroid/content/Context;II)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 479
    .end local v0    # "dm":Landroid/util/DisplayMetrics;
    :cond_0
    :goto_0
    const-string v2, "ZoomPad"

    const-string v3, "initHapticFeedback() - End"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    return-void

    .line 471
    :catch_0
    move-exception v1

    .line 472
    .local v1, "error":Ljava/lang/UnsatisfiedLinkError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 473
    const-string v2, "TAG"

    const-string v3, "Haptic Effect UnsatisfiedLinkError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 474
    .end local v1    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v1

    .line 475
    .local v1, "error":Ljava/lang/NoClassDefFoundError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 476
    const-string v2, "TAG"

    const-string v3, "Haptic Effect NoClassDefFoundError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private initialize()V
    .locals 21

    .prologue
    .line 323
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 324
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 327
    .local v2, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v14, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v15, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v14, v15, :cond_5

    .line 328
    iget v9, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 329
    .local v9, "widthPixels":I
    iget v4, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 335
    .local v4, "heightPixels":I
    :goto_0
    iget v14, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v14, v14

    iget v15, v2, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v14, v15

    float-to-double v14, v14

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    .line 336
    .local v10, "x":D
    iget v14, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v14, v14

    iget v15, v2, Landroid/util/DisplayMetrics;->ydpi:F

    div-float/2addr v14, v15

    float-to-double v14, v14

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    .line 337
    .local v12, "y":D
    add-double v14, v10, v12

    invoke-static {v14, v15}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    .line 339
    .local v6, "screenInches":D
    int-to-float v14, v9

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    div-float/2addr v14, v15

    float-to-int v8, v14

    .line 340
    .local v8, "w":I
    const/4 v3, 0x0

    .line 341
    .local v3, "h":I
    const-wide/high16 v14, 0x4020000000000000L    # 8.0

    cmpl-double v14, v6, v14

    if-lez v14, :cond_6

    .line 343
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    .line 344
    const/high16 v14, 0x44480000    # 800.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    float-to-int v8, v14

    .line 345
    const/high16 v14, 0x436a0000    # 234.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    float-to-int v3, v14

    .line 346
    const/16 v14, 0x31

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 347
    const/16 v14, 0x71

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 355
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    sub-int v15, v4, v9

    int-to-float v15, v15

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    sub-int v16, v4, v3

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    sub-int v17, v4, v9

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    int-to-float v0, v8

    move/from16 v18, v0

    add-float v17, v17, v18

    .line 356
    int-to-float v0, v4

    move/from16 v18, v0

    .line 355
    invoke-virtual/range {v14 .. v18}, Landroid/graphics/RectF;->set(FFFF)V

    .line 358
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    mul-int/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    div-int/2addr v14, v15

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    .line 359
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    .line 361
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    sub-int v14, v3, v14

    int-to-float v14, v14

    const/high16 v15, 0x40a00000    # 5.0f

    div-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    div-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    int-to-float v15, v15

    mul-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    int-to-float v15, v15

    div-float/2addr v14, v15

    float-to-int v5, v14

    .line 362
    .local v5, "tmp":I
    int-to-float v14, v5

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    int-to-float v15, v15

    mul-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    int-to-float v15, v15

    div-float/2addr v14, v15

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    .line 363
    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    .line 364
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v14, :cond_1

    .line 365
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    move/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setHandleSize(II)V

    .line 368
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    invoke-virtual/range {v14 .. v18}, Landroid/graphics/RectF;->set(FFFF)V

    .line 369
    const-string v14, "ZoomPad"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "initialize["

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 370
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 369
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    const-string v14, "ZoomPad"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "initialize["

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 372
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 371
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/RectF;->width()F

    move-result v14

    const/high16 v15, 0x40800000    # 4.0f

    mul-float/2addr v14, v15

    const/high16 v15, 0x40a00000    # 5.0f

    div-float/2addr v14, v15

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    .line 375
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    move/from16 v16, v0

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v17, v0

    .line 376
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    .line 375
    invoke-virtual/range {v14 .. v18}, Landroid/graphics/RectF;->set(FFFF)V

    .line 378
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v15

    sub-float/2addr v14, v15

    float-to-double v14, v14

    const-wide/high16 v16, 0x400c000000000000L    # 3.5

    div-double v14, v14, v16

    double-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    .line 379
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v15

    sub-float/2addr v14, v15

    float-to-double v14, v14

    const-wide/high16 v16, 0x400c000000000000L    # 3.5

    div-double v14, v14, v16

    double-to-float v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT_FLOAT:F

    .line 380
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    int-to-float v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 381
    const/high16 v14, 0x42200000    # 40.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadMargin:F

    .line 382
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v14, :cond_7

    .line 383
    const/16 v14, 0x19

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    .line 384
    const/high16 v14, 0x40e00000    # 7.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    .line 389
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/RectF;->width()F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->height()F

    move-result v16

    sub-float v15, v15, v16

    div-float/2addr v14, v15

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    .line 390
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v14, :cond_2

    .line 391
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    move/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setHeightAndRate(FF)V

    .line 394
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    if-eqz v14, :cond_3

    .line 395
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->unbindDrawables(Landroid/view/View;)V

    .line 397
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->makeLayout()Landroid/widget/RelativeLayout;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    .line 399
    .end local v2    # "displayMetrics":Landroid/util/DisplayMetrics;
    .end local v3    # "h":I
    .end local v4    # "heightPixels":I
    .end local v5    # "tmp":I
    .end local v6    # "screenInches":D
    .end local v8    # "w":I
    .end local v9    # "widthPixels":I
    .end local v10    # "x":D
    .end local v12    # "y":D
    :cond_4
    return-void

    .line 331
    .restart local v2    # "displayMetrics":Landroid/util/DisplayMetrics;
    :cond_5
    iget v9, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 332
    .restart local v9    # "widthPixels":I
    iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .restart local v4    # "heightPixels":I
    goto/16 :goto_0

    .line 349
    .restart local v3    # "h":I
    .restart local v6    # "screenInches":D
    .restart local v8    # "w":I
    .restart local v10    # "x":D
    .restart local v12    # "y":D
    :cond_6
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    .line 350
    const/high16 v14, 0x43b40000    # 360.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    float-to-int v8, v14

    .line 351
    const/high16 v14, 0x43160000    # 150.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    float-to-int v3, v14

    .line 352
    const/16 v14, 0x22

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 353
    const/16 v14, 0x32

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    goto/16 :goto_1

    .line 386
    .restart local v5    # "tmp":I
    :cond_7
    const/16 v14, 0x11

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    .line 387
    const/high16 v14, 0x40800000    # 4.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    goto/16 :goto_2
.end method

.method private makeLayout()Landroid/widget/RelativeLayout;
    .locals 38
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1488
    new-instance v30, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v30

    invoke-direct {v0, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1489
    .local v30, "relative":Landroid/widget/RelativeLayout;
    new-instance v11, Landroid/content/res/ColorStateList;

    .line 1490
    const/4 v2, 0x1

    new-array v2, v2, [[I

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [I

    const/4 v5, 0x0

    const v6, 0x10100a7

    aput v6, v4, v5

    aput-object v4, v2, v3

    .line 1491
    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v5, 0x1e

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v37, 0x0

    move/from16 v0, v37

    invoke-static {v5, v6, v7, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    aput v5, v3, v4

    .line 1489
    invoke-direct {v11, v2, v3}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 1492
    .local v11, "colorStateList":Landroid/content/res/ColorStateList;
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_0

    .line 1493
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1495
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v2, :cond_1

    .line 1496
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1499
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v0, v2

    move/from16 v36, v0

    .line 1500
    .local v36, "width":I
    new-instance v19, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1501
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1500
    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1502
    .local v19, "lButton":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v21, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1503
    .local v21, "leftButton":Landroid/widget/ImageButton;
    const/16 v2, 0x9

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1504
    const/4 v2, 0x0

    move-object/from16 v0, v19

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1505
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1506
    const/4 v2, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1507
    const-string v2, "string_move_left"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1508
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_5

    .line 1509
    const/4 v3, 0x0

    const-string v4, "zoompad_menu_press"

    const-string v5, "zoompad_menu_focus"

    .line 1510
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1509
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1525
    :goto_0
    new-instance v28, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1526
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1525
    move-object/from16 v0, v28

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1527
    .local v28, "rButton":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v31, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v31

    invoke-direct {v0, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1528
    .local v31, "rightButton":Landroid/widget/ImageButton;
    const/16 v2, 0x9

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1529
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v28

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1530
    move-object/from16 v0, v31

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1531
    const/4 v2, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1532
    const-string v2, "string_move_right"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1533
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_8

    .line 1534
    const/4 v3, 0x0

    const-string v4, "zoompad_menu_press"

    const-string v5, "zoompad_menu_focus"

    .line 1535
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1534
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1550
    :goto_1
    new-instance v15, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1551
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1550
    invoke-direct {v15, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1552
    .local v15, "eButton":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v16, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1553
    .local v16, "enterButton":Landroid/widget/ImageButton;
    const/16 v2, 0x9

    invoke-virtual {v15, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1554
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v15, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1555
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1556
    const/4 v2, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1557
    const-string v2, "string_enter"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1558
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_b

    .line 1559
    const/4 v3, 0x0

    const-string v4, "zoompad_menu_press"

    const-string v5, "zoompad_menu_focus"

    .line 1560
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1559
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1576
    :goto_2
    new-instance v33, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1577
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1576
    move-object/from16 v0, v33

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1578
    .local v33, "uButton":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v34, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v34

    invoke-direct {v0, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1579
    .local v34, "upButton":Landroid/widget/ImageButton;
    const/16 v2, 0x9

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1580
    move/from16 v0, v36

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v3, v3, 0x3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v33

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1581
    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1582
    const/4 v2, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1583
    const-string v2, "string_move_up"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1584
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_e

    .line 1585
    const/4 v3, 0x0

    const-string v4, "zoompad_menu_press"

    const-string v5, "zoompad_menu_focus"

    .line 1586
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1585
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1601
    :goto_3
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1602
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1601
    invoke-direct {v12, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1603
    .local v12, "dButton":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v13, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-direct {v13, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1604
    .local v13, "downButton":Landroid/widget/ImageButton;
    const/16 v2, 0x9

    invoke-virtual {v12, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1605
    move/from16 v0, v36

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v12, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1606
    invoke-virtual {v13, v12}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1607
    const/4 v2, 0x1

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1608
    const-string v2, "string_move_down"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1609
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_11

    .line 1610
    const/4 v3, 0x0

    const-string v4, "zoompad_menu_press"

    const-string v5, "zoompad_menu_focus"

    .line 1611
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1610
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1626
    :goto_4
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1627
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1626
    invoke-direct {v8, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1628
    .local v8, "cButton":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v9, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-direct {v9, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1629
    .local v9, "closeButton":Landroid/widget/ImageButton;
    const/16 v2, 0x9

    invoke-virtual {v8, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1630
    move/from16 v0, v36

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1631
    invoke-virtual {v9, v8}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1632
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1633
    const-string v2, "string_close"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1634
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_14

    .line 1635
    const/4 v3, 0x0

    const-string v4, "zoompad_menu_press"

    const-string v5, "zoompad_menu_focus"

    .line 1636
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1635
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1646
    :goto_5
    new-instance v20, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1647
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1646
    move-object/from16 v0, v20

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1648
    .local v20, "lLine":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v20

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1649
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    sub-int/2addr v2, v3

    move-object/from16 v0, v20

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1650
    new-instance v29, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1651
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1650
    move-object/from16 v0, v29

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1652
    .local v29, "rLine":Landroid/widget/RelativeLayout$LayoutParams;
    move/from16 v0, v36

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v29

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1653
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    sub-int/2addr v2, v3

    move-object/from16 v0, v29

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1655
    new-instance v18, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1656
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1655
    move-object/from16 v0, v18

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1657
    .local v18, "l2Line":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v18

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1658
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    sub-int/2addr v2, v3

    move-object/from16 v0, v18

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1659
    new-instance v27, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1660
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1659
    move-object/from16 v0, v27

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1661
    .local v27, "r2Line":Landroid/widget/RelativeLayout$LayoutParams;
    move/from16 v0, v36

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v27

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1662
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    sub-int/2addr v2, v3

    move-object/from16 v0, v27

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1664
    move-object/from16 v0, v30

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1665
    invoke-virtual/range {v30 .. v31}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1666
    move-object/from16 v0, v30

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1667
    move-object/from16 v0, v30

    invoke-virtual {v0, v13}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1668
    move-object/from16 v0, v30

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1669
    move-object/from16 v0, v30

    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1671
    new-instance v22, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1672
    .local v22, "leftImage":Landroid/widget/ImageView;
    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1673
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_17

    .line 1674
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_16

    const-string v2, "zoompad_menu_left"

    .line 1675
    :goto_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1674
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1686
    :goto_7
    new-instance v32, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v32

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1687
    .local v32, "rightImage":Landroid/widget/ImageView;
    move-object/from16 v0, v32

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1688
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_1c

    .line 1689
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_1b

    const-string v2, "zoompad_menu_right"

    .line 1690
    :goto_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1689
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1701
    :goto_9
    new-instance v35, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v35

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1702
    .local v35, "upImage":Landroid/widget/ImageView;
    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1703
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_21

    .line 1704
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_20

    const-string v2, "zoompad_menu_up"

    .line 1705
    :goto_a
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1704
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1716
    :goto_b
    new-instance v14, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-direct {v14, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1717
    .local v14, "downImage":Landroid/widget/ImageView;
    invoke-virtual {v14, v12}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1718
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_26

    .line 1719
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_25

    const-string v2, "zoompad_menu_down"

    .line 1720
    :goto_c
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1719
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v14, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1731
    :goto_d
    new-instance v17, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1732
    .local v17, "enterImage":Landroid/widget/ImageView;
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1733
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_2b

    .line 1734
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_2a

    const-string v2, "zoompad_menu_enter"

    .line 1735
    :goto_e
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1734
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1746
    :goto_f
    new-instance v10, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-direct {v10, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1747
    .local v10, "closeImage":Landroid/widget/ImageView;
    invoke-virtual {v10, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1748
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_30

    .line 1749
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_2f

    const-string v2, "zoompad_menu_close"

    .line 1750
    :goto_10
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1749
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v10, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1756
    :goto_11
    new-instance v23, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1757
    .local v23, "lineView4":Landroid/widget/ImageView;
    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1758
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_32

    .line 1759
    const-string v2, "zoompad_menu_line"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    add-int/lit8 v3, v3, -0x14

    .line 1760
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    .line 1759
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1764
    :goto_12
    new-instance v24, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v24

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1765
    .local v24, "lineView5":Landroid/widget/ImageView;
    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1766
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_33

    .line 1767
    const-string v2, "zoompad_menu_line"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1771
    :goto_13
    new-instance v25, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v25

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1772
    .local v25, "lineView6":Landroid/widget/ImageView;
    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1773
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_34

    .line 1774
    const-string v2, "zoompad_menu_line"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1778
    :goto_14
    new-instance v26, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1779
    .local v26, "lineView7":Landroid/widget/ImageView;
    move-object/from16 v0, v26

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1780
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_35

    .line 1781
    const-string v2, "zoompad_menu_line"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1785
    :goto_15
    move-object/from16 v0, v30

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1786
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_2

    .line 1787
    move-object/from16 v0, v30

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1789
    :cond_2
    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1791
    move-object/from16 v0, v30

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1793
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_3

    .line 1794
    move-object/from16 v0, v30

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1795
    move-object/from16 v0, v30

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1798
    :cond_3
    move-object/from16 v0, v30

    invoke-virtual {v0, v14}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1800
    move-object/from16 v0, v30

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1801
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_4

    .line 1802
    move-object/from16 v0, v30

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1804
    :cond_4
    move-object/from16 v0, v30

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1806
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$3;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1815
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1824
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$5;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$5;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1833
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$6;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$6;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1842
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$7;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$7;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1851
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$8;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$8;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1858
    return-object v30

    .line 1512
    .end local v8    # "cButton":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v9    # "closeButton":Landroid/widget/ImageButton;
    .end local v10    # "closeImage":Landroid/widget/ImageView;
    .end local v12    # "dButton":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v13    # "downButton":Landroid/widget/ImageButton;
    .end local v14    # "downImage":Landroid/widget/ImageView;
    .end local v15    # "eButton":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v16    # "enterButton":Landroid/widget/ImageButton;
    .end local v17    # "enterImage":Landroid/widget/ImageView;
    .end local v18    # "l2Line":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v20    # "lLine":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v22    # "leftImage":Landroid/widget/ImageView;
    .end local v23    # "lineView4":Landroid/widget/ImageView;
    .end local v24    # "lineView5":Landroid/widget/ImageView;
    .end local v25    # "lineView6":Landroid/widget/ImageView;
    .end local v26    # "lineView7":Landroid/widget/ImageView;
    .end local v27    # "r2Line":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v28    # "rButton":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v29    # "rLine":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v31    # "rightButton":Landroid/widget/ImageButton;
    .end local v32    # "rightImage":Landroid/widget/ImageView;
    .end local v33    # "uButton":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v34    # "upButton":Landroid/widget/ImageButton;
    .end local v35    # "upImage":Landroid/widget/ImageView;
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mLeftButton:Z

    if-eqz v2, :cond_7

    .line 1513
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_6

    .line 1514
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1516
    :cond_6
    const/4 v3, 0x0

    const-string v4, "zoompad_menu_press"

    const-string v5, "zoompad_menu_focus"

    .line 1517
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1516
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1520
    :cond_7
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "zoompad_menu_focus"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 1521
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1520
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1537
    .restart local v28    # "rButton":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v31    # "rightButton":Landroid/widget/ImageButton;
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRightButton:Z

    if-eqz v2, :cond_a

    .line 1538
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_9

    .line 1539
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 1541
    :cond_9
    const/4 v3, 0x0

    const-string v4, "zoompad_menu_press"

    const-string v5, "zoompad_menu_focus"

    .line 1542
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1541
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 1545
    :cond_a
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "zoompad_menu_focus"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 1546
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1545
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 1562
    .restart local v15    # "eButton":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v16    # "enterButton":Landroid/widget/ImageButton;
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnterButton:Z

    if-eqz v2, :cond_d

    .line 1563
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_c

    .line 1564
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 1566
    :cond_c
    const/4 v3, 0x0

    const-string v4, "zoompad_menu_press"

    const-string v5, "zoompad_menu_focus"

    .line 1567
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1566
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 1570
    :cond_d
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "zoompad_menu_focus"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 1571
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1570
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 1588
    .restart local v33    # "uButton":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v34    # "upButton":Landroid/widget/ImageButton;
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpButton:Z

    if-eqz v2, :cond_10

    .line 1589
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_f

    .line 1590
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 1592
    :cond_f
    const/4 v3, 0x0

    const-string v4, "zoompad_menu_press"

    const-string v5, "zoompad_menu_focus"

    .line 1593
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1592
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 1596
    :cond_10
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "zoompad_menu_focus"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 1597
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1596
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 1613
    .restart local v12    # "dButton":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v13    # "downButton":Landroid/widget/ImageButton;
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDownButton:Z

    if-eqz v2, :cond_13

    .line 1614
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_12

    .line 1615
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 1617
    :cond_12
    const/4 v3, 0x0

    const-string v4, "zoompad_menu_press"

    const-string v5, "zoompad_menu_focus"

    .line 1618
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1617
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 1621
    :cond_13
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "zoompad_menu_focus"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 1622
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1621
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 1638
    .restart local v8    # "cButton":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v9    # "closeButton":Landroid/widget/ImageButton;
    :cond_14
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_15

    .line 1639
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    .line 1641
    :cond_15
    const/4 v3, 0x0

    const-string v4, "zoompad_menu_press"

    const-string v5, "zoompad_menu_focus"

    .line 1642
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1641
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    .line 1674
    .restart local v18    # "l2Line":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v20    # "lLine":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v22    # "leftImage":Landroid/widget/ImageView;
    .restart local v27    # "r2Line":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v29    # "rLine":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_16
    const-string v2, "zoompad_menu_left"

    goto/16 :goto_6

    .line 1677
    :cond_17
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mLeftButton:Z

    if-eqz v2, :cond_19

    .line 1678
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_18

    const-string v2, "zoompad_menu_left"

    .line 1679
    :goto_16
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1678
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_7

    :cond_18
    const-string v2, "zoompad_menu_left"

    goto :goto_16

    .line 1681
    :cond_19
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_1a

    const-string v2, "zoompad_menu_left_dim"

    .line 1682
    :goto_17
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1681
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_7

    :cond_1a
    const-string v2, "zoompad_menu_left_dim"

    goto :goto_17

    .line 1689
    .restart local v32    # "rightImage":Landroid/widget/ImageView;
    :cond_1b
    const-string v2, "zoompad_menu_right"

    goto/16 :goto_8

    .line 1692
    :cond_1c
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRightButton:Z

    if-eqz v2, :cond_1e

    .line 1693
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_1d

    const-string v2, "zoompad_menu_right"

    .line 1694
    :goto_18
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1693
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_9

    :cond_1d
    const-string v2, "zoompad_menu_right"

    goto :goto_18

    .line 1697
    :cond_1e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_1f

    const-string v2, "zoompad_menu_right_dim"

    :goto_19
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1696
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_9

    .line 1697
    :cond_1f
    const-string v2, "zoompad_menu_right_dim"

    goto :goto_19

    .line 1704
    .restart local v35    # "upImage":Landroid/widget/ImageView;
    :cond_20
    const-string v2, "zoompad_menu_up"

    goto/16 :goto_a

    .line 1707
    :cond_21
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpButton:Z

    if-eqz v2, :cond_23

    .line 1708
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_22

    const-string v2, "zoompad_menu_up"

    :goto_1a
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 1709
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1708
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_b

    :cond_22
    const-string v2, "zoompad_menu_up"

    goto :goto_1a

    .line 1711
    :cond_23
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_24

    const-string v2, "zoompad_menu_up_dim"

    .line 1712
    :goto_1b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1711
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_b

    :cond_24
    const-string v2, "zoompad_menu_up_dim"

    goto :goto_1b

    .line 1719
    .restart local v14    # "downImage":Landroid/widget/ImageView;
    :cond_25
    const-string v2, "zoompad_menu_down"

    goto/16 :goto_c

    .line 1722
    :cond_26
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDownButton:Z

    if-eqz v2, :cond_28

    .line 1723
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_27

    const-string v2, "zoompad_menu_down"

    .line 1724
    :goto_1c
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1723
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v14, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_d

    :cond_27
    const-string v2, "zoompad_menu_down"

    goto :goto_1c

    .line 1726
    :cond_28
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_29

    const-string v2, "zoompad_menu_down_dim"

    .line 1727
    :goto_1d
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1726
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v14, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_d

    :cond_29
    const-string v2, "zoompad_menu_down_dim"

    goto :goto_1d

    .line 1734
    .restart local v17    # "enterImage":Landroid/widget/ImageView;
    :cond_2a
    const-string v2, "zoompad_menu_enter"

    goto/16 :goto_e

    .line 1737
    :cond_2b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnterButton:Z

    if-eqz v2, :cond_2d

    .line 1738
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_2c

    const-string v2, "zoompad_menu_enter"

    .line 1739
    :goto_1e
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1738
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_f

    :cond_2c
    const-string v2, "zoompad_menu_enter"

    goto :goto_1e

    .line 1742
    :cond_2d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_2e

    const-string v2, "zoompad_menu_enter_dim"

    :goto_1f
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1741
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_f

    .line 1742
    :cond_2e
    const-string v2, "zoompad_menu_enter_dim"

    goto :goto_1f

    .line 1749
    .restart local v10    # "closeImage":Landroid/widget/ImageView;
    :cond_2f
    const-string v2, "zoompad_menu_close"

    goto/16 :goto_10

    .line 1752
    :cond_30
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_31

    const-string v2, "zoompad_menu_close"

    .line 1753
    :goto_20
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1752
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v10, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_11

    :cond_31
    const-string v2, "zoompad_menu_close"

    goto :goto_20

    .line 1762
    .restart local v23    # "lineView4":Landroid/widget/ImageView;
    :cond_32
    const-string v2, "zoompad_menu_line"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_12

    .line 1769
    .restart local v24    # "lineView5":Landroid/widget/ImageView;
    :cond_33
    const-string v2, "zoompad_menu_line"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_13

    .line 1776
    .restart local v25    # "lineView6":Landroid/widget/ImageView;
    :cond_34
    const-string v2, "zoompad_menu_line"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_14

    .line 1783
    .restart local v26    # "lineView7":Landroid/widget/ImageView;
    :cond_35
    const-string v2, "zoompad_menu_line"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_15
.end method

.method private onTouchSelection(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1362
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-nez v6, :cond_0

    .line 1363
    const/4 v6, 0x0

    .line 1483
    :goto_0
    return v6

    .line 1365
    :cond_0
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 1366
    .local v3, "tmpRect":Landroid/graphics/RectF;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    and-int/lit16 v0, v6, 0xff

    .line 1367
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 1368
    .local v4, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 1369
    .local v5, "y":F
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float v1, v6, v4

    .line 1370
    .local v1, "dx":F
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    sub-float v2, v6, v5

    .line 1372
    .local v2, "dy":F
    packed-switch v0, :pswitch_data_0

    .line 1479
    :cond_1
    :goto_1
    :pswitch_0
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_2

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_2

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x3

    if-eq v6, v7, :cond_2

    .line 1480
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x5

    if-eq v6, v7, :cond_2

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    if-nez v6, :cond_14

    .line 1481
    :cond_2
    const/4 v6, 0x1

    goto :goto_0

    .line 1378
    :pswitch_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    iput v4, v6, Landroid/graphics/PointF;->x:F

    .line 1379
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    iput v5, v6, Landroid/graphics/PointF;->y:F

    .line 1381
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v6, v4, v5}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1382
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v6, v4, v5}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1383
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    const/high16 v7, 0x428c0000    # 70.0f

    sub-float/2addr v6, v7

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    .line 1384
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    const/high16 v9, 0x428c0000    # 70.0f

    add-float/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    .line 1383
    invoke-virtual {v3, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1385
    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1386
    const/4 v6, 0x3

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    .line 1418
    :cond_3
    :goto_2
    const-string v6, "ZoomPad"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "State="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1388
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v7, v7, 0x4

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    .line 1389
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    .line 1388
    invoke-virtual {v3, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1390
    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1391
    const/4 v6, 0x4

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    goto :goto_2

    .line 1393
    :cond_5
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    goto :goto_2

    .line 1397
    :cond_6
    const/16 v6, 0x9

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    .line 1398
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    sub-float/2addr v6, v7

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    .line 1399
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    int-to-float v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    int-to-float v8, v8

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    .line 1400
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    int-to-float v9, v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v10

    float-to-int v9, v9

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    .line 1398
    invoke-virtual {v3, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1401
    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1402
    const/4 v6, 0x5

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    goto/16 :goto_2

    .line 1405
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRect()Landroid/graphics/Rect;

    move-result-object v6

    float-to-int v7, v4

    float-to-int v8, v5

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1406
    const/4 v6, 0x1

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    .line 1407
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    float-to-int v6, v6

    int-to-float v6, v6

    .line 1408
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    .line 1409
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    float-to-int v8, v8

    int-to-float v8, v8

    .line 1410
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v10, v10

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    float-to-int v9, v9

    int-to-float v9, v9

    .line 1407
    invoke-virtual {v3, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1411
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    neg-int v6, v6

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    neg-int v7, v7

    int-to-float v7, v7

    invoke-virtual {v3, v6, v7}, Landroid/graphics/RectF;->offset(FF)V

    .line 1412
    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1413
    const/4 v6, 0x2

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    goto/16 :goto_2

    .line 1416
    :cond_8
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    goto/16 :goto_2

    .line 1422
    :pswitch_2
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_c

    .line 1423
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    sub-float/2addr v6, v1

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1424
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    sub-float/2addr v6, v2

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1425
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 1450
    :cond_9
    :goto_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    iput v4, v6, Landroid/graphics/PointF;->x:F

    .line 1451
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    iput v5, v6, Landroid/graphics/PointF;->y:F

    .line 1452
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_a

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_a

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x3

    if-eq v6, v7, :cond_a

    .line 1453
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x5

    if-ne v6, v7, :cond_1

    .line 1454
    :cond_a
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    .line 1455
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_b

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    .line 1456
    :cond_b
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateFrameBuffer()V

    goto/16 :goto_1

    .line 1426
    :cond_c
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_d

    .line 1427
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeightChange:Z

    if-eqz v6, :cond_9

    .line 1428
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 1429
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-float v6, v6

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 1430
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    goto :goto_3

    .line 1432
    :cond_d
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_e

    .line 1433
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    neg-float v7, v1

    neg-float v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/RectF;->offset(FF)V

    .line 1435
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    goto :goto_3

    .line 1436
    :cond_e
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x5

    if-ne v6, v7, :cond_9

    .line 1437
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    sub-float/2addr v6, v1

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    .line 1438
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_f

    .line 1439
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    .line 1441
    :cond_f
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    const/high16 v8, 0x40800000    # 4.0f

    mul-float/2addr v7, v8

    const/high16 v8, 0x40a00000    # 5.0f

    div-float/2addr v7, v8

    cmpl-float v6, v6, v7

    if-lez v6, :cond_10

    .line 1442
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    const/high16 v7, 0x40800000    # 4.0f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40a00000    # 5.0f

    div-float/2addr v6, v7

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    .line 1445
    :cond_10
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    add-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->left:F

    .line 1446
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    invoke-virtual {v3, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1447
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    neg-float v6, v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    neg-float v7, v7

    invoke-virtual {v3, v6, v7}, Landroid/graphics/RectF;->offset(FF)V

    .line 1448
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v6, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->updateRect(Landroid/graphics/RectF;)V

    goto/16 :goto_3

    .line 1464
    :pswitch_3
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_11

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x3

    if-eq v6, v7, :cond_11

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x5

    if-eq v6, v7, :cond_11

    .line 1465
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    .line 1466
    :cond_11
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_12

    .line 1467
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setHeightAndRate(FF)V

    .line 1469
    :cond_12
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-float v6, v6

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1470
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-float v6, v6

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1471
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_13

    const/4 v6, 0x1

    :goto_4
    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateBox(ZF)V

    .line 1472
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 1473
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    .line 1474
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    goto/16 :goto_1

    .line 1471
    :cond_13
    const/4 v6, 0x0

    goto :goto_4

    .line 1483
    :cond_14
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 1372
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private registerPensoundSolution()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 492
    const-string v1, "ZoomPad"

    const-string v2, "registerPensoundSolution() - Start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    :try_start_0
    sget-boolean v1, Lcom/samsung/audio/SmpsManager;->isSupport:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 504
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-nez v1, :cond_0

    .line 505
    new-instance v1, Lcom/samsung/audio/SmpsManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/audio/SmpsManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 506
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v1, :cond_0

    .line 507
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexPencil:I

    .line 508
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexMarker:I

    .line 509
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexBrush:I

    .line 510
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexEraser:I

    .line 511
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    if-eq v1, v3, :cond_1

    .line 512
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 518
    :cond_0
    :goto_0
    const-string v1, "ZoomPad"

    const-string v2, "registerPensoundSolution() - End"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    :goto_1
    return-void

    .line 495
    :catch_0
    move-exception v0

    .line 496
    .local v0, "error":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "ZoomPad"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    goto :goto_1

    .line 499
    .end local v0    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 500
    .local v0, "error":Ljava/lang/NoClassDefFoundError;
    const-string v1, "ZoomPad"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    goto :goto_1

    .line 513
    .end local v0    # "error":Ljava/lang/NoClassDefFoundError;
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexPencil:I

    if-eq v1, v3, :cond_0

    .line 514
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexPencil:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    goto :goto_0
.end method

.method private relativeCoordinate()Landroid/graphics/RectF;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 903
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmapWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmapHeight:I

    int-to-float v3, v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 904
    .local v1, "srcRect":Landroid/graphics/RectF;
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 905
    .local v0, "dstRect":Landroid/graphics/RectF;
    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 906
    iget v2, v1, Landroid/graphics/RectF;->right:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 907
    iget v2, v1, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 908
    iget v2, v1, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 910
    return-object v0
.end method

.method private releaseHapticFeedback()V
    .locals 2

    .prologue
    .line 483
    const-string v0, "ZoomPad"

    const-string v1, "releaseHapticFeedback() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    invoke-virtual {v0}, Lcom/samsung/hapticfeedback/HapticEffect;->closeDevice()V

    .line 486
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 488
    :cond_0
    const-string v0, "ZoomPad"

    const-string v1, "releaseHapticFeedback() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    return-void
.end method

.method private unregisterPensoundSolution()V
    .locals 2

    .prologue
    .line 522
    const-string v0, "ZoomPad"

    const-string v1, "unregisterPensoundSolution() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v0, :cond_0

    .line 524
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    invoke-virtual {v0}, Lcom/samsung/audio/SmpsManager;->onDestroy()V

    .line 525
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 527
    :cond_0
    const-string v0, "ZoomPad"

    const-string v1, "unregisterPensoundSolution() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    return-void
.end method


# virtual methods
.method checkBox()V
    .locals 6

    .prologue
    const/high16 v5, -0x40800000    # -1.0f

    const/4 v4, 0x0

    .line 980
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    if-nez v0, :cond_2

    .line 981
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 982
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1029
    :cond_1
    :goto_0
    return-void

    .line 985
    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 986
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 988
    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 989
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 991
    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 992
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 995
    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    mul-float/2addr v0, v5

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_6

    .line 996
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float/2addr v0, v5

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 999
    :cond_6
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    mul-float/2addr v0, v5

    float-to-double v0, v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_7

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_7

    .line 1000
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float/2addr v0, v5

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1003
    :cond_7
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_8

    .line 1004
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_8

    .line 1005
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1008
    :cond_8
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_9

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_9

    .line 1009
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1012
    :cond_9
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_a

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_a

    .line 1013
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1016
    :cond_a
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_b

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_b

    .line 1017
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1020
    :cond_b
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_c

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_c

    .line 1021
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1024
    :cond_c
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 1025
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    goto/16 :goto_0
.end method

.method public close()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 287
    const-string v1, "ZoomPad"

    const-string v2, "close"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    if-eqz v1, :cond_0

    .line 290
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->clear()V

    .line 291
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    .line 293
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v1, :cond_1

    .line 294
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->close()V

    .line 295
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    .line 297
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 298
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_3

    .line 303
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    .line 305
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->unbindDrawables(Landroid/view/View;)V

    .line 306
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    .line 307
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    .line 308
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    .line 309
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    .line 310
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    .line 311
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    .line 312
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    .line 313
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mParent:Landroid/view/ViewGroup;

    .line 314
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    .line 315
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxPaint:Landroid/graphics/Paint;

    .line 316
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 317
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    .line 318
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    .line 319
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    .line 320
    return-void

    .line 298
    :cond_3
    aget-object v0, v2, v1

    .line 299
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_4

    .line 300
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 298
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getBoxHeight()F
    .locals 1

    .prologue
    .line 585
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    return v0
.end method

.method public getDrawingRectF()Landroid/graphics/RectF;
    .locals 5

    .prologue
    .line 1195
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method getIntValueAppliedDensity(F)I
    .locals 1
    .param p1, "paramFloat"    # F

    .prologue
    .line 1966
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public getPadRectF()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1191
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method getResourceString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "strName"    # Ljava/lang/String;

    .prologue
    .line 1950
    const/4 v2, 0x0

    .line 1951
    .local v2, "string":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    if-eqz v3, :cond_1

    .line 1953
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    const-string v4, "string"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 1954
    .local v1, "resId":I
    if-nez v1, :cond_0

    .line 1955
    const/4 v3, 0x0

    .line 1962
    .end local v1    # "resId":I
    :goto_0
    return-object v3

    .line 1957
    .restart local v1    # "resId":I
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .end local v1    # "resId":I
    :cond_1
    :goto_1
    move-object v3, v2

    .line 1962
    goto :goto_0

    .line 1958
    :catch_0
    move-exception v0

    .line 1959
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method getStrokeBoxRect()Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 1171
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1172
    .local v0, "rect":Landroid/graphics/Rect;
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1173
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1174
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1175
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1177
    return-object v0
.end method

.method public getStrokeBoxRectF()Landroid/graphics/RectF;
    .locals 4

    .prologue
    .line 1181
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 1182
    .local v0, "rect":Landroid/graphics/RectF;
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1183
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1184
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1185
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1187
    return-object v0
.end method

.method public isEnabled()Z
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    .prologue
    .line 564
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    return v0
.end method

.method public isStroking()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1199
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    if-eqz v1, :cond_0

    .line 1200
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1202
    :cond_0
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v1, 0x0

    .line 870
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 900
    :cond_0
    :goto_0
    return-void

    .line 874
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float v4, v0, v2

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 875
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v2, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v3, v3

    add-float/2addr v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v0, v4

    .line 876
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    move-object v0, p1

    .line 875
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 877
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    add-float v3, v0, v2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float v4, v0, v2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v5, v0

    .line 878
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float v6, v0, v2

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    move-object v2, p1

    .line 877
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 879
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 882
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    .line 883
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 884
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    int-to-float v4, v4

    .line 882
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 885
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    neg-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 886
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeightChange:Z

    if-eqz v0, :cond_2

    .line 887
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    if-eq v0, v8, :cond_3

    .line 888
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v8

    if-eqz v0, :cond_2

    .line 889
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v8

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v10, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 897
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRect()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 899
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 892
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v9

    if-eqz v0, :cond_2

    .line 893
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v10, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 810
    sub-int v3, p4, p2

    .line 811
    .local v3, "w":I
    sub-int v0, p5, p3

    .line 812
    .local v0, "h":I
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    if-ne v5, v3, :cond_0

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    if-eq v5, v0, :cond_2

    .line 813
    :cond_0
    const-string v5, "ZoomPad"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "onLayout w="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " h="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    .line 815
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    .line 817
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v4

    .line 818
    .local v4, "width":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v2

    .line 819
    .local v2, "height":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v6, v6

    sub-float/2addr v6, v4

    div-float/2addr v6, v9

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v7, v7

    sub-float/2addr v7, v2

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v8, v8

    sub-float/2addr v8, v4

    div-float/2addr v8, v9

    add-float/2addr v8, v4

    .line 820
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v9, v9

    .line 819
    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 822
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 823
    .local v1, "handler":Landroid/os/Handler;
    new-instance v5, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;

    invoke-direct {v5, p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    .line 857
    const-wide/16 v6, 0x0

    .line 823
    invoke-virtual {v1, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 864
    .end local v1    # "handler":Landroid/os/Handler;
    .end local v2    # "height":F
    .end local v4    # "width":F
    :cond_1
    :goto_0
    return-void

    .line 859
    :cond_2
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSaveMaxDeltaX:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_3

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSaveMaxDeltaY:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_1

    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    if-eqz v5, :cond_1

    .line 860
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    invoke-interface {v5, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 861
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 19
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1210
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    if-nez v2, :cond_1

    .line 1211
    :cond_0
    const/4 v2, 0x0

    .line 1358
    :goto_0
    return v2

    .line 1213
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    if-eqz v2, :cond_21

    .line 1215
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onPreTouch(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1216
    const/4 v2, 0x1

    goto :goto_0

    .line 1219
    :cond_2
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->onTouchSelection(Landroid/view/MotionEvent;)Z

    .line 1221
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v8, v2, 0xff

    .line 1222
    .local v8, "action":I
    new-instance v16, Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    sub-float/2addr v5, v6

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1224
    .local v16, "tmpRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/16 v3, 0x9

    if-ne v2, v3, :cond_b

    .line 1225
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    neg-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    add-float/2addr v3, v4

    neg-float v3, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1227
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v17

    .line 1229
    .local v17, "toolTypeAction":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    .line 1230
    .local v9, "newX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    .line 1232
    .local v10, "newY":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_a

    .line 1233
    const/4 v2, 0x2

    move/from16 v0, v17

    if-eq v0, v2, :cond_3

    const/4 v2, 0x3

    move/from16 v0, v17

    if-eq v0, v2, :cond_3

    const/4 v2, 0x4

    move/from16 v0, v17

    if-ne v0, v2, :cond_a

    .line 1234
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v2, :cond_5

    .line 1235
    const/4 v2, 0x3

    move/from16 v0, v17

    if-eq v0, v2, :cond_4

    const/4 v2, 0x4

    move/from16 v0, v17

    if-ne v0, v2, :cond_5

    .line 1236
    :cond_4
    if-nez v8, :cond_c

    .line 1237
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexEraser:I

    invoke-virtual {v2, v3}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 1243
    :cond_5
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v14

    .line 1244
    .local v14, "tempX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v15

    .line 1245
    .local v15, "tempY":F
    move v12, v14

    .line 1246
    .local v12, "originX":F
    move v13, v15

    .line 1248
    .local v13, "originY":F
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v2, v14, v2

    if-gez v2, :cond_d

    .line 1249
    move-object/from16 v0, v16

    iget v14, v0, Landroid/graphics/RectF;->left:F

    .line 1250
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    .line 1256
    :cond_6
    :goto_2
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v2, v15, v2

    if-gez v2, :cond_e

    .line 1257
    move-object/from16 v0, v16

    iget v15, v0, Landroid/graphics/RectF;->top:F

    .line 1258
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    .line 1264
    :cond_7
    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1266
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    if-eqz v2, :cond_12

    .line 1267
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v2, :cond_8

    .line 1268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/audio/SmpsManager;->generateSound(Landroid/view/MotionEvent;)V

    .line 1271
    :cond_8
    if-nez v8, :cond_f

    .line 1272
    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldX:F

    .line 1273
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldY:F

    .line 1294
    :cond_9
    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1295
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    .line 1297
    .end local v12    # "originX":F
    .end local v13    # "originY":F
    .end local v14    # "tempX":F
    .end local v15    # "tempY":F
    :cond_a
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_13

    .line 1298
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsMultiTouch:Z

    .line 1302
    :goto_5
    const/4 v2, 0x2

    move/from16 v0, v17

    if-ne v0, v2, :cond_14

    .line 1304
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 1305
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->onTouchInput(Landroid/view/MotionEvent;)Z

    .line 1354
    .end local v9    # "newX":F
    .end local v10    # "newY":F
    .end local v17    # "toolTypeAction":I
    :cond_b
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    if-eqz v2, :cond_21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onPostTouch(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 1355
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1238
    .restart local v9    # "newX":F
    .restart local v10    # "newY":F
    .restart local v17    # "toolTypeAction":I
    :cond_c
    const/4 v2, 0x1

    if-ne v8, v2, :cond_5

    .line 1239
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    invoke-virtual {v2, v3}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    goto/16 :goto_1

    .line 1251
    .restart local v12    # "originX":F
    .restart local v13    # "originY":F
    .restart local v14    # "tempX":F
    .restart local v15    # "tempY":F
    :cond_d
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->width()F

    move-result v3

    add-float/2addr v2, v3

    cmpl-float v2, v14, v2

    if-lez v2, :cond_6

    .line 1252
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->width()F

    move-result v3

    add-float v14, v2, v3

    .line 1253
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    goto/16 :goto_2

    .line 1259
    :cond_e
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->height()F

    move-result v3

    add-float/2addr v2, v3

    cmpl-float v2, v15, v2

    if-lez v2, :cond_7

    .line 1260
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->height()F

    move-result v3

    add-float v15, v2, v3

    .line 1261
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    goto/16 :goto_3

    .line 1274
    :cond_f
    const/4 v2, 0x2

    if-ne v8, v2, :cond_11

    .line 1276
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v2, :cond_10

    .line 1277
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldX:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldY:F

    invoke-virtual {v2, v3, v4, v9, v10}, Lcom/samsung/hapticfeedback/HapticEffect;->playEffectByDistance(FFFF)V

    .line 1279
    :cond_10
    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldX:F

    .line 1280
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldY:F

    goto/16 :goto_4

    .line 1281
    :cond_11
    const/4 v2, 0x1

    if-ne v8, v2, :cond_9

    .line 1283
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v2, :cond_9

    .line 1284
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    invoke-virtual {v2}, Lcom/samsung/hapticfeedback/HapticEffect;->stopAllEffect()V

    goto/16 :goto_4

    .line 1287
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v2, :cond_9

    .line 1288
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v11

    .line 1290
    .local v11, "originAction":I
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1291
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/audio/SmpsManager;->generateSound(Landroid/view/MotionEvent;)V

    .line 1292
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->setAction(I)V

    goto/16 :goto_4

    .line 1300
    .end local v11    # "originAction":I
    .end local v12    # "originX":F
    .end local v13    # "originY":F
    .end local v14    # "tempX":F
    .end local v15    # "tempY":F
    :cond_13
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsMultiTouch:Z

    goto/16 :goto_5

    .line 1307
    :cond_14
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 1308
    const/4 v2, 0x3

    move/from16 v0, v17

    if-eq v0, v2, :cond_15

    .line 1309
    const/4 v2, 0x4

    move/from16 v0, v17

    if-eq v0, v2, :cond_15

    .line 1310
    const/4 v2, 0x5

    move/from16 v0, v17

    if-ne v0, v2, :cond_b

    .line 1311
    :cond_15
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 1312
    const/4 v2, 0x2

    if-ne v8, v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    if-nez v2, :cond_16

    .line 1313
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1315
    :cond_16
    const/4 v2, 0x3

    move/from16 v0, v17

    if-ne v0, v2, :cond_1a

    .line 1316
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->onTouchEraser(Landroid/view/MotionEvent;)Z

    .line 1320
    :cond_17
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsMultiTouch:Z

    if-nez v2, :cond_18

    .line 1321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v4, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1322
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v5, v5

    add-float/2addr v5, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    sub-float/2addr v6, v7

    div-float v6, v3, v6

    .line 1323
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->height()F

    move-result v18

    sub-float v7, v7, v18

    div-float v7, v3, v7

    move-object/from16 v3, p1

    .line 1321
    invoke-interface/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onViewTouchEvent(Landroid/view/MotionEvent;FFFF)V

    .line 1325
    :cond_18
    const/4 v2, 0x1

    if-eq v8, v2, :cond_19

    const/4 v2, 0x3

    if-ne v8, v2, :cond_1b

    .line 1326
    :cond_19
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 1330
    :goto_8
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    goto/16 :goto_6

    .line 1317
    :cond_1a
    const/4 v2, 0x4

    move/from16 v0, v17

    if-ne v0, v2, :cond_17

    .line 1318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->onTouchRemover(Landroid/view/MotionEvent;)Z

    goto :goto_7

    .line 1328
    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateFrameBuffer()V

    goto :goto_8

    .line 1332
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    if-eqz v2, :cond_b

    .line 1333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1d

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_b

    .line 1334
    :cond_1d
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1335
    const/4 v2, 0x3

    move/from16 v0, v17

    if-ne v0, v2, :cond_20

    .line 1336
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->onTouchEraser(Landroid/view/MotionEvent;)Z

    .line 1340
    :cond_1e
    :goto_9
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsMultiTouch:Z

    if-nez v2, :cond_1f

    .line 1341
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v4, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1342
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v5, v5

    add-float/2addr v5, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    sub-float/2addr v6, v7

    div-float v6, v3, v6

    .line 1343
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->height()F

    move-result v18

    sub-float v7, v7, v18

    div-float v7, v3, v7

    move-object/from16 v3, p1

    .line 1341
    invoke-interface/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onViewTouchEvent(Landroid/view/MotionEvent;FFFF)V

    .line 1345
    :cond_1f
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    .line 1347
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    goto/16 :goto_6

    .line 1337
    :cond_20
    const/4 v2, 0x4

    move/from16 v0, v17

    if-ne v0, v2, :cond_1e

    .line 1338
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->onTouchRemover(Landroid/view/MotionEvent;)Z

    goto :goto_9

    .line 1358
    .end local v8    # "action":I
    .end local v9    # "newX":F
    .end local v10    # "newY":F
    .end local v16    # "tmpRect":Landroid/graphics/RectF;
    .end local v17    # "toolTypeAction":I
    :cond_21
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 550
    const-string v0, "ZoomPad"

    const-string v1, "onWindowFocusChanged() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    if-eqz p1, :cond_0

    .line 552
    const-string v0, "ZoomPad"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->registerPensoundSolution()V

    .line 558
    :goto_0
    const-string v0, "ZoomPad"

    const-string v1, "onWindowFocusChanged() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    return-void

    .line 555
    :cond_0
    const-string v0, "ZoomPad"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->unregisterPensoundSolution()V

    goto :goto_0
.end method

.method resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;
    .locals 14
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I
    .param p3, "iconWidth"    # I
    .param p4, "iconHeight"    # I

    .prologue
    .line 1972
    invoke-virtual/range {p1 .. p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v13

    .line 1973
    .local v13, "stream":Ljava/io/InputStream;
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1976
    .local v1, "BitmapOrg":Landroid/graphics/Bitmap;
    if-nez v1, :cond_0

    .line 1977
    const/4 v2, 0x0

    .line 2001
    :goto_0
    return-object v2

    .line 1979
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 1980
    .local v4, "width":I
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 1981
    .local v5, "height":I
    move/from16 v0, p3

    int-to-float v2, v0

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getIntValueAppliedDensity(F)I

    move-result v9

    .line 1982
    .local v9, "newWidth":I
    move/from16 v0, p4

    int-to-float v2, v0

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1985
    .local v8, "newHeight":I
    int-to-float v2, v9

    int-to-float v3, v4

    div-float v12, v2, v3

    .line 1986
    .local v12, "scaleWidth":F
    int-to-float v2, v8

    int-to-float v3, v5

    div-float v11, v2, v3

    .line 1989
    .local v11, "scaleHeight":F
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 1991
    .local v6, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v6, v12, v11}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1997
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x1

    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 2001
    .local v10, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, p1, v10}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    .prologue
    .line 805
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    .line 806
    return-void
.end method

.method public setBoxHeight(F)V
    .locals 7
    .param p1, "height"    # F

    .prologue
    const/4 v6, 0x0

    .line 568
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeightChange:Z

    if-nez v0, :cond_1

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 571
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->initialize()V

    .line 572
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v0, v1

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    float-to-double v0, p1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT_FLOAT:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x400c000000000000L    # 3.5

    mul-double/2addr v2, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    cmpl-double v0, v0, v2

    if-gtz v0, :cond_0

    .line 575
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 576
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 577
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 578
    const/4 v0, 0x0

    invoke-virtual {p0, v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateBox(ZF)V

    .line 579
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 580
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setHeightAndRate(FF)V

    .line 581
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    goto :goto_0
.end method

.method public setBoxHeightEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 713
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeightChange:Z

    .line 714
    return-void
.end method

.method public setBoxPosition(FF)V
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 589
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 591
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 593
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 594
    .local v2, "widthPixels":I
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 595
    .local v1, "heightPixels":I
    cmpg-float v3, p1, v6

    if-ltz v3, :cond_0

    int-to-float v3, v2

    cmpl-float v3, p1, v3

    if-gtz v3, :cond_0

    cmpg-float v3, p2, v6

    if-ltz v3, :cond_0

    int-to-float v3, v1

    cmpl-float v3, p2, v3

    if-lez v3, :cond_1

    .line 612
    :cond_0
    :goto_0
    return-void

    .line 598
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 599
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 601
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    cmpg-float v3, v3, v6

    if-gez v3, :cond_2

    .line 602
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 604
    :cond_2
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    if-eqz v3, :cond_3

    .line 605
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 608
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 609
    invoke-virtual {p0, v7, v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateBox(ZF)V

    .line 610
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 611
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    goto :goto_0
.end method

.method public setButtonEnabled(ZZZZZ)V
    .locals 1
    .param p1, "left"    # Z
    .param p2, "right"    # Z
    .param p3, "enter"    # Z
    .param p4, "up"    # Z
    .param p5, "down"    # Z

    .prologue
    .line 717
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mLeftButton:Z

    .line 718
    iput-boolean p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRightButton:Z

    .line 719
    iput-boolean p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnterButton:Z

    .line 720
    iput-boolean p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpButton:Z

    .line 721
    iput-boolean p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDownButton:Z

    .line 722
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    if-nez v0, :cond_1

    .line 723
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 724
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->unbindDrawables(Landroid/view/View;)V

    .line 726
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->makeLayout()Landroid/widget/RelativeLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    .line 728
    :cond_1
    return-void
.end method

.method setButtonState(I)V
    .locals 7
    .param p1, "direction"    # I

    .prologue
    const/4 v3, 0x0

    const v4, 0x38d1b717    # 1.0E-4f

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 914
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    if-nez v2, :cond_1

    .line 977
    :cond_0
    :goto_0
    return-void

    .line 917
    :cond_1
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 918
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    invoke-virtual {v2, v3, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->setMovingEnabled(ZF)V

    .line 919
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->removeMessages(I)V

    .line 920
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    div-float/2addr v2, v6

    float-to-int v0, v2

    .line 921
    .local v0, "diff":I
    const/4 v1, 0x0

    .line 922
    .local v1, "save":Z
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 924
    :pswitch_1
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    cmpg-float v2, v2, v5

    if-gtz v2, :cond_3

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    sub-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_3

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v4

    if-lez v2, :cond_3

    .line 925
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 926
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v6

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 927
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 971
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 972
    invoke-virtual {p0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateBox(ZF)V

    .line 973
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 975
    const-string v2, "ZoomPad"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "direction = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    goto/16 :goto_0

    .line 929
    :cond_3
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    int-to-float v3, v0

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    goto :goto_1

    .line 934
    :pswitch_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_5

    .line 935
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_5

    .line 938
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v3, v4

    if-ne v2, v3, :cond_4

    .line 939
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_2

    .line 940
    :cond_4
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v6

    add-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 941
    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 942
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    invoke-interface {v2, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    goto/16 :goto_1

    .line 945
    :cond_5
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    int-to-float v3, v0

    add-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    goto/16 :goto_1

    .line 950
    :pswitch_3
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v6

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    goto/16 :goto_1

    .line 954
    :pswitch_4
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v6

    add-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    goto/16 :goto_1

    .line 958
    :pswitch_5
    const-string v2, "ZoomPad"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "STATE_BTN_ENTER=["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartX:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartY:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 959
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v3, v4

    if-ne v2, v3, :cond_6

    .line 960
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_7

    .line 961
    :cond_6
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v6

    add-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 962
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartX:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 963
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaSaveX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 965
    :cond_7
    const/4 v1, 0x1

    .line 966
    goto/16 :goto_1

    .line 922
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "drawableName"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 1887
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    if-nez v4, :cond_1

    .line 1925
    :cond_0
    :goto_0
    return-object v0

    .line 1890
    :cond_1
    const/4 v2, 0x0

    .line 1891
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "drawable"

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, p1, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 1893
    .local v3, "mDrawableResID":I
    if-nez v3, :cond_3

    .line 1894
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    const-string v5, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, p1, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 1895
    if-eqz v3, :cond_0

    .line 1898
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {p0, v4, v3, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1900
    const/4 v0, 0x0

    .line 1901
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    .line 1902
    instance-of v4, v2, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v4, :cond_2

    .line 1903
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1905
    .restart local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 1906
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1905
    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1907
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1908
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    invoke-virtual {v2, v7, v7, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1909
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1913
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {p0, v4, v3, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1915
    const/4 v0, 0x0

    .line 1916
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    .line 1917
    instance-of v4, v2, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v4, :cond_4

    .line 1918
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1920
    .restart local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_4
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1921
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1922
    .restart local v1    # "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    invoke-virtual {v2, v7, v7, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1923
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "drawableName"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 1929
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    if-nez v3, :cond_1

    :cond_0
    move-object v0, v2

    .line 1946
    :goto_0
    return-object v0

    .line 1932
    :cond_1
    const/4 v0, 0x0

    .line 1933
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "drawable"

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 1935
    .local v1, "mDrawableResID":I
    if-nez v1, :cond_3

    .line 1936
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    const-string v4, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 1937
    if-nez v1, :cond_2

    move-object v0, v2

    .line 1938
    goto :goto_0

    .line 1940
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {p0, v2, v1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1942
    goto :goto_0

    .line 1944
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0, v2, v1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1946
    goto :goto_0
.end method

.method setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1864
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 1865
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 1866
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 1867
    invoke-virtual {p0, p1, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1866
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1870
    :cond_0
    if-eqz p2, :cond_1

    .line 1871
    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    .line 1872
    invoke-virtual {p0, p2, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1871
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1874
    :cond_1
    if-eqz p3, :cond_2

    .line 1875
    new-array v1, v4, [I

    const v2, 0x101009c

    aput v2, v1, v3

    .line 1876
    invoke-virtual {p0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1875
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1878
    :cond_2
    if-eqz p2, :cond_3

    .line 1879
    new-array v1, v4, [I

    const v2, 0x10100a1

    aput v2, v1, v3

    .line 1880
    invoke-virtual {p0, p2, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1879
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1883
    :cond_3
    return-object v0

    .line 1866
    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    .line 693
    const-string v0, "ZoomPad"

    const-string v1, "setEraserSettingInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    .line 697
    :cond_0
    return-void
.end method

.method public setLayer(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 659
    .local p1, "layer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setLayer(Ljava/util/ArrayList;)V

    .line 662
    :cond_0
    return-void
.end method

.method public setPadPosition(FF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 615
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->initialize()V

    .line 616
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 618
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float v0, p1, v2

    .line 619
    .local v0, "dx":F
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float v1, p2, v2

    .line 620
    .local v1, "dy":F
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 621
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 622
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    .line 623
    return-void
.end method

.method public setPanAndZoom(FFFFF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "maxX"    # F
    .param p4, "maxY"    # F
    .param p5, "ratio"    # F

    .prologue
    .line 626
    const-string v0, "ZoomPad"

    const-string v1, "setPanAndZoom"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    .line 628
    float-to-double v0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    .line 629
    float-to-double v0, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    .line 630
    float-to-double v0, p4

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    .line 631
    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    .line 632
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0, p1, p2, p5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setPanAndZoom(FFF)V

    .line 635
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSaveRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 636
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSaveRatio:F

    .line 637
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSaveMaxDeltaX:F

    .line 638
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSaveMaxDeltaY:F

    .line 640
    :cond_1
    return-void
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 4
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    .line 670
    const-string v0, "ZoomPad"

    const-string v1, "setPenSettingInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 676
    :cond_0
    if-eqz p1, :cond_3

    .line 677
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v0, :cond_3

    .line 678
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 679
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 680
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexPencil:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    .line 686
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    invoke-virtual {v0, v1}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 687
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    .line 690
    :cond_3
    return-void

    .line 681
    :cond_4
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 682
    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexBrush:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    goto :goto_0

    .line 683
    :cond_6
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 684
    :cond_7
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexMarker:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    goto :goto_0
.end method

.method public setReference(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "background"    # Landroid/graphics/Bitmap;

    .prologue
    .line 707
    if-eqz p1, :cond_0

    .line 708
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    .line 710
    :cond_0
    return-void
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 700
    const-string v0, "ZoomPad"

    const-string v1, "setRemoverSettingInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 702
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 704
    :cond_0
    return-void
.end method

.method public setScreenSize(II)V
    .locals 2
    .param p1, "widht"    # I
    .param p2, "height"    # I

    .prologue
    .line 643
    const-string v0, "ZoomPad"

    const-string v1, "setScreenSize"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    .line 646
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    .line 647
    return-void
.end method

.method public setScreenStart(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 650
    const-string v0, "ZoomPad"

    const-string v1, "setScreenStart"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    .line 652
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    .line 653
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setScreenStart(II)V

    .line 656
    :cond_0
    return-void
.end method

.method public setToolTypeAction(II)V
    .locals 2
    .param p1, "toolType"    # I
    .param p2, "action"    # I

    .prologue
    .line 665
    const-string v0, "ZoomPad"

    const-string v1, "setToolTypeAction"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 667
    return-void
.end method

.method public start(Landroid/view/ViewGroup;II)V
    .locals 10
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "bitmapW"    # I
    .param p3, "bitmapH"    # I

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 402
    const-string v1, "ZoomPad"

    const-string v2, "start"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    if-nez v1, :cond_0

    .line 404
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mParent:Landroid/view/ViewGroup;

    .line 406
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->initialize()V

    .line 407
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mParent:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 409
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    const-string v3, "zoompad_bg"

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v1, v2

    .line 410
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    const-string v2, "zoompad_handle_bottom"

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    invoke-virtual {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v7

    .line 411
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v1, :cond_1

    .line 412
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    const-string v2, "zoompad_selected_handle_normal"

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    invoke-virtual {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v8

    .line 413
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    const-string v2, "zoompad_selected_handle_press"

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    invoke-virtual {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v9

    .line 419
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setBitmap([Landroid/graphics/Bitmap;)V

    .line 421
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmapWidth:I

    .line 422
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmapHeight:I

    .line 424
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 425
    invoke-virtual {p0, v7, v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateBox(ZF)V

    .line 426
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 428
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-direct {v2, v6, v6, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setRect(Landroid/graphics/RectF;)V

    .line 429
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 430
    .local v0, "tmpRect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    neg-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 431
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->updateRect(Landroid/graphics/RectF;)V

    .line 432
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->createBitmap(II)V

    .line 434
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 435
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    .line 436
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->initHapticFeedback()V

    .line 437
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->registerPensoundSolution()V

    .line 439
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    .line 441
    .end local v0    # "tmpRect":Landroid/graphics/RectF;
    :cond_0
    return-void

    .line 415
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    const-string v2, "zoompad_selected_handle_normal_hd"

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    .line 416
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    .line 415
    invoke-virtual {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v8

    .line 417
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    const-string v2, "zoompad_selected_handle_press_hd"

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    invoke-virtual {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v9

    goto/16 :goto_0
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 444
    const-string v0, "ZoomPad"

    const-string v1, "stop"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    if-eqz v0, :cond_3

    .line 446
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    .line 447
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 448
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 450
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    .line 452
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 453
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mParent:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 454
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mParent:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 456
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    if-eqz v0, :cond_2

    .line 457
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onStop()V

    .line 460
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->releaseHapticFeedback()V

    .line 461
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->unregisterPensoundSolution()V

    .line 463
    :cond_3
    return-void
.end method

.method unbindDrawables(Landroid/view/View;)V
    .locals 4
    .param p1, "root"    # Landroid/view/View;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2007
    if-nez p1, :cond_1

    .line 2047
    :cond_0
    :goto_0
    return-void

    .line 2011
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_7

    .line 2012
    invoke-virtual {p1, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2017
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2018
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2020
    :cond_2
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    .line 2021
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_8

    .line 2024
    instance-of v1, p1, Landroid/widget/AdapterView;

    if-nez v1, :cond_9

    move-object v1, p1

    .line 2025
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2031
    .end local v0    # "i":I
    :cond_3
    :goto_3
    instance-of v1, p1, Landroid/widget/SeekBar;

    if-eqz v1, :cond_4

    move-object v1, p1

    .line 2032
    check-cast v1, Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2035
    :cond_4
    instance-of v1, p1, Landroid/widget/ImageView;

    if-eqz v1, :cond_5

    move-object v1, p1

    .line 2036
    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v1, p1

    .line 2037
    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2039
    :cond_5
    instance-of v1, p1, Landroid/widget/ImageButton;

    if-eqz v1, :cond_6

    move-object v1, p1

    .line 2040
    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v1, p1

    .line 2041
    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2044
    :cond_6
    if-eqz p1, :cond_0

    .line 2045
    const/4 p1, 0x0

    goto :goto_0

    .line 2014
    :cond_7
    invoke-virtual {p1, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .restart local v0    # "i":I
    :cond_8
    move-object v1, p1

    .line 2022
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->unbindDrawables(Landroid/view/View;)V

    .line 2021
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2027
    :cond_9
    check-cast p1, Landroid/widget/AdapterView;

    .end local p1    # "root":Landroid/view/View;
    invoke-virtual {p1, v3}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 2028
    const/4 p1, 0x0

    .restart local p1    # "root":Landroid/view/View;
    goto :goto_3
.end method

.method updateBox(ZF)V
    .locals 16
    .param p1, "save"    # Z
    .param p2, "distance"    # F

    .prologue
    .line 1032
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    if-eqz v9, :cond_0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    if-nez v9, :cond_2

    .line 1033
    :cond_0
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    float-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    double-to-float v9, v10

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1034
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    float-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    double-to-float v9, v10

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1128
    :cond_1
    :goto_0
    return-void

    .line 1038
    :cond_2
    if-eqz p1, :cond_6

    .line 1039
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v9

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    sub-float/2addr v10, v11

    .line 1040
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v12, v12

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    .line 1041
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v13, v13

    add-float/2addr v12, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v13, v14

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    add-float/2addr v12, v13

    .line 1042
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v14, v14

    add-float/2addr v13, v14

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    add-float/2addr v13, v14

    .line 1039
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v9

    .line 1042
    if-eqz v9, :cond_6

    .line 1044
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v3

    .line 1045
    .local v3, "height":F
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    add-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->top:F

    .line 1046
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    add-float/2addr v10, v3

    iput v10, v9, Landroid/graphics/RectF;->bottom:F

    .line 1047
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_3

    .line 1048
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    sub-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->bottom:F

    .line 1049
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v10, v3

    iput v10, v9, Landroid/graphics/RectF;->top:F

    .line 1051
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 1053
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v9

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    sub-float/2addr v10, v11

    .line 1054
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v12, v12

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    .line 1055
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v13, v13

    add-float/2addr v12, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v13, v14

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    add-float/2addr v12, v13

    .line 1056
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v14, v14

    add-float/2addr v13, v14

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    add-float/2addr v13, v14

    .line 1053
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v9

    .line 1056
    if-eqz v9, :cond_6

    .line 1058
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v8

    .line 1059
    .local v8, "width":F
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    sub-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->right:F

    .line 1060
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    sub-float/2addr v10, v8

    iput v10, v9, Landroid/graphics/RectF;->left:F

    .line 1061
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->left:F

    const/4 v10, 0x0

    cmpg-float v9, v9, v10

    if-gez v9, :cond_4

    .line 1062
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    add-float/2addr v10, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    add-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->left:F

    .line 1063
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    add-float/2addr v10, v8

    iput v10, v9, Landroid/graphics/RectF;->right:F

    .line 1065
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 1066
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v9

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    sub-float/2addr v10, v11

    .line 1067
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v12, v12

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    .line 1068
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v13, v13

    add-float/2addr v12, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v13, v14

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    add-float/2addr v12, v13

    .line 1069
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v14, v14

    add-float/2addr v13, v14

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    add-float/2addr v13, v14

    .line 1066
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v9

    .line 1069
    if-eqz v9, :cond_5

    .line 1071
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    add-float v6, v9, v10

    .line 1072
    .local v6, "spaceLeft":F
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v9, v9

    sub-float/2addr v9, v6

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v10, v11

    sub-float v7, v9, v10

    .line 1073
    .local v7, "spaceRight":F
    cmpl-float v9, v6, v7

    if-lez v9, :cond_d

    .line 1074
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    const/4 v10, 0x0

    iput v10, v9, Landroid/graphics/RectF;->left:F

    .line 1075
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    add-float/2addr v10, v8

    iput v10, v9, Landroid/graphics/RectF;->right:F

    .line 1082
    .end local v6    # "spaceLeft":F
    .end local v7    # "spaceRight":F
    :cond_5
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 1086
    .end local v3    # "height":F
    .end local v8    # "width":F
    :cond_6
    new-instance v5, Landroid/graphics/RectF;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v11, v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v12, v12

    invoke-direct {v5, v9, v10, v11, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1087
    .local v5, "screenRect":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->relativeCoordinate()Landroid/graphics/RectF;

    move-result-object v2

    .line 1088
    .local v2, "bitmapRect":Landroid/graphics/RectF;
    invoke-virtual {v5, v2}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v9

    if-nez v9, :cond_b

    .line 1089
    const/4 v9, 0x0

    cmpl-float v9, p2, v9

    if-nez v9, :cond_7

    .line 1090
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    float-to-int v9, v9

    int-to-float v0, v9

    move/from16 p2, v0

    .line 1092
    :cond_7
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_8

    .line 1093
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    sub-float v10, v10, p2

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    invoke-interface {v9, v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 1094
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1096
    :cond_8
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_9

    .line 1097
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    add-float v10, v10, p2

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    invoke-interface {v9, v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 1098
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v9, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    float-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-float v9, v10

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1100
    :cond_9
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_a

    .line 1101
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v11, v12

    invoke-interface {v9, v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 1102
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1104
    :cond_a
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v9, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_b

    .line 1105
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    add-float/2addr v9, v10

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1106
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    invoke-interface {v9, v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 1107
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    sub-float/2addr v9, v10

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1108
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v9, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_b

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    cmpl-float v9, v9, v10

    if-nez v9, :cond_b

    .line 1109
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v9, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v9, v10

    float-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-float v9, v10

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1114
    :cond_b
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    float-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    double-to-float v9, v10

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1115
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    float-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    double-to-float v9, v10

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1116
    if-eqz p1, :cond_c

    .line 1117
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaSaveX:F

    .line 1118
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartX:F

    .line 1119
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartY:F

    .line 1121
    :cond_c
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_1

    .line 1122
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    const/high16 v10, 0x3f800000    # 1.0f

    cmpg-float v9, v9, v10

    if-gtz v9, :cond_1

    .line 1123
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    float-to-int v10, v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    float-to-int v11, v11

    .line 1124
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v12, v13

    float-to-int v12, v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    float-to-int v13, v13

    .line 1123
    invoke-static {v9, v10, v11, v12, v13}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1125
    .local v4, "mCropBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v9, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setReferenceBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 1077
    .end local v2    # "bitmapRect":Landroid/graphics/RectF;
    .end local v4    # "mCropBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "screenRect":Landroid/graphics/RectF;
    .restart local v3    # "height":F
    .restart local v6    # "spaceLeft":F
    .restart local v7    # "spaceRight":F
    .restart local v8    # "width":F
    :cond_d
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v10, v10

    iput v10, v9, Landroid/graphics/RectF;->right:F

    .line 1078
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    sub-float/2addr v10, v8

    iput v10, v9, Landroid/graphics/RectF;->left:F

    goto/16 :goto_1
.end method

.method public updateFrameBuffer()V
    .locals 3

    .prologue
    .line 799
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 800
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRectF()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->updateFrameBuffer(ZLandroid/graphics/RectF;)V

    .line 802
    :cond_0
    return-void
.end method

.method updateLayout()V
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 1131
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v3

    .line 1132
    .local v3, "width":F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v0

    .line 1133
    .local v0, "height":F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    cmpg-float v4, v4, v6

    if-gez v4, :cond_0

    .line 1134
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iput v6, v4, Landroid/graphics/RectF;->left:F

    .line 1135
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iput v3, v4, Landroid/graphics/RectF;->right:F

    .line 1137
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    .line 1138
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v5, v5

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 1139
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v5, v5

    sub-float/2addr v5, v3

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 1141
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    cmpg-float v4, v4, v6

    if-gez v4, :cond_2

    .line 1142
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iput v6, v4, Landroid/graphics/RectF;->top:F

    .line 1143
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iput v0, v4, Landroid/graphics/RectF;->bottom:F

    .line 1145
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadMargin:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    .line 1146
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadMargin:F

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 1147
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadMargin:F

    add-float/2addr v5, v0

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    .line 1149
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_4

    .line 1150
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v5, v5

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    .line 1151
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v5, v5

    sub-float/2addr v5, v0

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 1153
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1154
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1155
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v4, :cond_5

    .line 1156
    new-instance v1, Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    invoke-direct {v1, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1157
    .local v1, "tmpRect":Landroid/graphics/RectF;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    neg-float v4, v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    neg-float v5, v5

    invoke-virtual {v1, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    .line 1158
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v4, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->updateRect(Landroid/graphics/RectF;)V

    .line 1161
    .end local v1    # "tmpRect":Landroid/graphics/RectF;
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    if-eqz v4, :cond_6

    .line 1162
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    .line 1163
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-double v4, v4

    .line 1162
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 1163
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    .line 1162
    invoke-direct {v2, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1164
    .local v2, "totalLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1165
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1166
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1168
    .end local v2    # "totalLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_6
    return-void
.end method

.method public updatePad()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/high16 v12, 0x40000000    # 2.0f

    .line 731
    const-string v6, "ZoomPad"

    const-string v7, "updatePad"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    sub-float/2addr v7, v8

    .line 733
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v12

    div-float/2addr v9, v12

    sub-float/2addr v8, v9

    .line 734
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v10, v12

    div-float/2addr v10, v12

    add-float/2addr v9, v10

    .line 735
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    div-float/2addr v11, v12

    add-float/2addr v10, v11

    .line 732
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v6

    .line 735
    if-eqz v6, :cond_3

    .line 737
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 738
    .local v1, "height":F
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    add-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->top:F

    .line 739
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    add-float/2addr v7, v1

    iput v7, v6, Landroid/graphics/RectF;->bottom:F

    .line 740
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-lez v6, :cond_0

    .line 741
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    sub-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->bottom:F

    .line 742
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v7, v1

    iput v7, v6, Landroid/graphics/RectF;->top:F

    .line 744
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 746
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    sub-float/2addr v7, v8

    .line 747
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v12

    div-float/2addr v9, v12

    sub-float/2addr v8, v9

    .line 748
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v10, v12

    div-float/2addr v10, v12

    add-float/2addr v9, v10

    .line 749
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    div-float/2addr v11, v12

    add-float/2addr v10, v11

    .line 746
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v6

    .line 749
    if-eqz v6, :cond_3

    .line 751
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 752
    .local v5, "width":F
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    sub-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->right:F

    .line 753
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    sub-float/2addr v7, v5

    iput v7, v6, Landroid/graphics/RectF;->left:F

    .line 754
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    cmpg-float v6, v6, v13

    if-gez v6, :cond_1

    .line 755
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    add-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->left:F

    .line 757
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    add-float/2addr v7, v5

    iput v7, v6, Landroid/graphics/RectF;->right:F

    .line 759
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 760
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    sub-float/2addr v7, v8

    .line 761
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v12

    div-float/2addr v9, v12

    sub-float/2addr v8, v9

    .line 762
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v10, v12

    div-float/2addr v10, v12

    add-float/2addr v9, v10

    .line 763
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    div-float/2addr v11, v12

    add-float/2addr v10, v11

    .line 760
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v6

    .line 763
    if-eqz v6, :cond_2

    .line 765
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v7, v7

    add-float v3, v6, v7

    .line 766
    .local v3, "spaceLeft":F
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v6, v6

    sub-float/2addr v6, v3

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v7, v8

    sub-float v4, v6, v7

    .line 767
    .local v4, "spaceRight":F
    cmpl-float v6, v3, v4

    if-lez v6, :cond_5

    .line 768
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iput v13, v6, Landroid/graphics/RectF;->left:F

    .line 769
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    add-float/2addr v7, v5

    iput v7, v6, Landroid/graphics/RectF;->right:F

    .line 776
    .end local v3    # "spaceLeft":F
    .end local v4    # "spaceRight":F
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 782
    .end local v1    # "height":F
    .end local v5    # "width":F
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v6, :cond_4

    .line 783
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 784
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_6

    .line 785
    const-string v6, "ZoomPad"

    const-string v7, "updatePad bitmap is null"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    :goto_1
    return-void

    .line 771
    .restart local v1    # "height":F
    .restart local v3    # "spaceLeft":F
    .restart local v4    # "spaceRight":F
    .restart local v5    # "width":F
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/RectF;->right:F

    .line 772
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    sub-float/2addr v7, v5

    iput v7, v6, Landroid/graphics/RectF;->left:F

    goto :goto_0

    .line 788
    .end local v1    # "height":F
    .end local v3    # "spaceLeft":F
    .end local v4    # "spaceRight":F
    .end local v5    # "width":F
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_6
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 789
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRectF()Landroid/graphics/RectF;

    move-result-object v2

    .line 790
    .local v2, "rect":Landroid/graphics/RectF;
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    neg-int v6, v6

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    neg-int v7, v7

    int-to-float v7, v7

    invoke-virtual {v2, v6, v7}, Landroid/graphics/RectF;->offset(FF)V

    .line 791
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v9

    sub-float/2addr v8, v9

    div-float/2addr v7, v8

    .line 792
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v10

    sub-float/2addr v9, v10

    div-float/2addr v8, v9

    .line 791
    invoke-interface {v6, v0, v2, v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onCropBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;FF)V

    .line 793
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->update()V

    goto :goto_1
.end method
