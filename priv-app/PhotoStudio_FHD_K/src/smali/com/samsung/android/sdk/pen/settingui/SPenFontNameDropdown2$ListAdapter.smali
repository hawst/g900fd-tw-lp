.class Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SPenFontNameDropdown2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    .line 43
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->items:Ljava/util/List;

    .line 44
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->items:Ljava/util/List;

    .line 45
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->context:Landroid/content/Context;

    .line 46
    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 50
    if-nez p2, :cond_0

    .line 51
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 52
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x1090008

    invoke-virtual {v0, v3, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 54
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v3, 0x1020014

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 56
    .local v2, "tv":Landroid/widget/TextView;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 57
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 58
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 60
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x41900000    # 18.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v6, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 61
    const/16 v3, 0x13

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 62
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x41000000    # 8.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    const/high16 v5, 0x41200000    # 10.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v2, v3, v6, v4, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 63
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x42240000    # 41.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 65
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v1

    .line 66
    .local v1, "tf":Landroid/graphics/Typeface;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 68
    return-object p2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 74
    if-nez p2, :cond_0

    .line 75
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 77
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x1090008

    invoke-virtual {v0, v3, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 79
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v3, 0x1020014

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 81
    .local v2, "tv":Landroid/widget/TextView;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 82
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 83
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 85
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x41900000    # 18.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v6, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 86
    const/16 v3, 0x13

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 87
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x41000000    # 8.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    const/high16 v5, 0x41200000    # 10.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v2, v3, v6, v4, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 88
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x42240000    # 41.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 95
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v1

    .line 96
    .local v1, "tf":Landroid/graphics/Typeface;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 98
    return-object p2
.end method
