.class Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
.super Landroid/view/View;
.source "SpenNotePadDrawing.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;
    }
.end annotation


# static fields
.field private static final PAD_BAR_COLOR:I = -0x699d9b95

.field private static final PAD_MOVING_RECT_COLOR:I = 0x4779caf2

.field private static final STROKE_PAD_WIDTH:I = 0x4

.field private static final TRANSPARENCY_DEGREE:I = 0x96


# instance fields
.field private PAD_HANDLE_HEIGHT:I

.field private PAD_HANDLE_WIDTH:I

.field dstRect:Landroid/graphics/RectF;

.field private first:I

.field private isEraserCursor:Z

.field private mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

.field private mAntiAliasPaint:Landroid/graphics/Paint;

.field private mAutoMovingRect:Landroid/graphics/RectF;

.field private mBitmap:[Landroid/graphics/Bitmap;

.field private mBoxHeight:F

.field private mCanvasLayer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mCirclePaint:Landroid/graphics/Paint;

.field private mCirclePoint:Landroid/graphics/PointF;

.field private mCircleRadius:F

.field private mContext:Landroid/content/Context;

.field private mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

.field private mDeltaX:F

.field private mDeltaY:F

.field private mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

.field private mFloatingLayer:Landroid/graphics/Bitmap;

.field private mHeight:I

.field private mIsMultiTouch:Z

.field private mIsOOV:Z

.field private mIsPenUp:Z

.field private final mOnePT:F

.field private mPadBarRect:Landroid/graphics/RectF;

.field private mPadLayer:Landroid/graphics/Bitmap;

.field private mPadLinePaint:Landroid/graphics/Paint;

.field private mPadPaint:Landroid/graphics/Paint;

.field private mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

.field private mPenName:Ljava/lang/String;

.field private mRatio:F

.field private mReferenceBitmap:Landroid/graphics/Bitmap;

.field private mRemoverRadius:F

.field private mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field private mScreenStartX:I

.field private mScreenStartY:I

.field private mSrcPaint:Landroid/graphics/Paint;

.field private mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

.field private mWidth:I

.field private preEvent:Landroid/view/MotionEvent;

.field srcRect:Landroid/graphics/Rect;

.field private x:F

.field private y:F


# direct methods
.method public constructor <init>(Landroid/content/Context;F)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "PT"    # F

    .prologue
    const/high16 v5, -0x3d380000    # -100.0f

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 88
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 39
    const/16 v0, 0x1f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_WIDTH:I

    .line 40
    const/16 v0, 0x22

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_HEIGHT:I

    .line 51
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    .line 52
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    .line 63
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 64
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 65
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    .line 66
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v5, v5}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    .line 67
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    .line 68
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    .line 69
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    .line 70
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsPenUp:Z

    .line 71
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMultiTouch:Z

    .line 72
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsOOV:Z

    .line 74
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartX:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartY:I

    .line 75
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaX:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaY:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    .line 77
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->x:F

    .line 78
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->y:F

    .line 79
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->first:I

    .line 304
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    .line 594
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    .line 595
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    .line 89
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mContext:Landroid/content/Context;

    .line 91
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadPaint:Landroid/graphics/Paint;

    .line 92
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLinePaint:Landroid/graphics/Paint;

    .line 93
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLinePaint:Landroid/graphics/Paint;

    const v1, -0x699d9b95

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 94
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 95
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v1, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 97
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mSrcPaint:Landroid/graphics/Paint;

    .line 98
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mSrcPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 99
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mSrcPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 100
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 101
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 102
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    .line 103
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 104
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 105
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 107
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    .line 108
    return-void
.end method

.method private ExtendRectFromRectF(Landroid/graphics/Rect;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dst"    # Landroid/graphics/Rect;
    .param p2, "src"    # Landroid/graphics/RectF;

    .prologue
    .line 671
    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 672
    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 673
    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 674
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 675
    return-void
.end method

.method private MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;
    .locals 6
    .param p1, "src"    # Landroid/graphics/RectF;

    .prologue
    .line 678
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    iget v2, p1, Landroid/graphics/RectF;->top:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, p1, Landroid/graphics/RectF;->right:F

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 679
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 678
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dstRectF"    # Landroid/graphics/RectF;
    .param p2, "srcRectF"    # Landroid/graphics/RectF;

    .prologue
    .line 664
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartX:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 665
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartX:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 666
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartY:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 667
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartY:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 668
    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 112
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCanvasLayer:Ljava/util/ArrayList;

    .line 113
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    .line 114
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 115
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 117
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-eqz v0, :cond_2

    .line 120
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V

    .line 122
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->close()V

    .line 125
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    .line 127
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 128
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 129
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    .line 131
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 132
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 133
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    .line 135
    :cond_4
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    .line 136
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    .line 137
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadPaint:Landroid/graphics/Paint;

    .line 138
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLinePaint:Landroid/graphics/Paint;

    .line 139
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mSrcPaint:Landroid/graphics/Paint;

    .line 140
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 141
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    .line 142
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    .line 143
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 144
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 146
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    .line 147
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    .line 148
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mContext:Landroid/content/Context;

    .line 149
    return-void
.end method

.method public createBitmap(II)V
    .locals 3
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 152
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 155
    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    .line 157
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 160
    :cond_1
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    .line 161
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v0, :cond_2

    .line 162
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 164
    :cond_2
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mWidth:I

    .line 165
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    .line 167
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    .line 168
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    .line 169
    return-void
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, 0x40800000    # 4.0f

    const/4 v7, 0x0

    .line 601
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 602
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v7, v7}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    invoke-virtual {v1, v7, v7, v2}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 603
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v2, v8

    float-to-int v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v4, v8

    sub-float/2addr v3, v4

    float-to-int v3, v3

    .line 604
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v8

    sub-float/2addr v4, v5

    float-to-int v4, v4

    .line 603
    invoke-virtual {v1, v2, v7, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 605
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v2, v8

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    .line 606
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v8

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v6, v8

    sub-float/2addr v5, v6

    .line 605
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 607
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 610
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 611
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v7, v7}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    invoke-virtual {v1, v7, v7, v2}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 612
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v1, v7, v7, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 613
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    mul-float v0, v1, v2

    .line 614
    .local v0, "rate":F
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    .line 615
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    .line 614
    invoke-virtual {v1, v11, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 616
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 618
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    cmpl-float v1, v1, v11

    if-lez v1, :cond_1

    .line 619
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 621
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    cmpl-float v1, v1, v11

    if-lez v1, :cond_2

    .line 622
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 626
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadPaint:Landroid/graphics/Paint;

    const v2, -0x699d9b95

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 627
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mWidth:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v3, v3

    invoke-virtual {v1, v7, v7, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 631
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    const/16 v2, 0x96

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 632
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v3, v8

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v4, v8

    add-float/2addr v3, v4

    .line 633
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v8

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    .line 632
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 634
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v7

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 635
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 638
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadPaint:Landroid/graphics/Paint;

    const v2, 0x4779caf2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 639
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v8

    sub-float/2addr v4, v5

    .line 640
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v6, v8

    sub-float/2addr v5, v6

    .line 639
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 641
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 645
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v3, v9

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mWidth:I

    int-to-float v4, v4

    .line 646
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v9

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    add-float/2addr v5, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v6, v9

    sub-float/2addr v5, v6

    .line 645
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 647
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 650
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v10

    if-eqz v1, :cond_3

    .line 651
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v10

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v1, v7, v7, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 652
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_WIDTH:I

    int-to-float v3, v3

    div-float/2addr v3, v9

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    .line 653
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_HEIGHT:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v4, v9

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    .line 654
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_WIDTH:I

    int-to-float v5, v5

    div-float/2addr v5, v9

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    .line 655
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v6, v9

    sub-float/2addr v5, v6

    .line 652
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 656
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v10

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 660
    .end local v0    # "rate":F
    :cond_3
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 661
    return-void
.end method

.method onTouchEraser(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, -0x3d380000    # -100.0f

    .line 508
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v0, v4, 0xff

    .line 509
    .local v0, "action":I
    const/4 v4, 0x5

    if-eq v0, v4, :cond_0

    const/4 v4, 0x6

    if-ne v0, v4, :cond_1

    .line 510
    :cond_0
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMultiTouch:Z

    .line 512
    :cond_1
    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    if-ne v4, v8, :cond_2

    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMultiTouch:Z

    if-eqz v4, :cond_2

    .line 513
    const/4 v0, 0x0

    .line 514
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMultiTouch:Z

    .line 516
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {p1, v5, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 517
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 518
    .local v2, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 519
    .local v3, "y":F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    neg-float v4, v4

    invoke-virtual {p1, v5, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 521
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    div-float v1, v4, v5

    .line 522
    .local v1, "rate":F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-nez v4, :cond_3

    .line 523
    new-instance v4, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 525
    :cond_3
    if-nez v0, :cond_5

    .line 526
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    .line 527
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    mul-float/2addr v4, v1

    div-float/2addr v4, v7

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    .line 528
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 529
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v3, v4, Landroid/graphics/PointF;->y:F

    .line 530
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    mul-float v5, v7, v1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 546
    :cond_4
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->invalidate()V

    .line 547
    return v8

    .line 531
    :cond_5
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    if-eqz v4, :cond_4

    .line 532
    const/4 v4, 0x2

    if-ne v0, v4, :cond_6

    .line 533
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    mul-float/2addr v4, v1

    div-float/2addr v4, v7

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    .line 534
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 535
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v3, v4, Landroid/graphics/PointF;->y:F

    .line 536
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    mul-float v5, v7, v1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_0

    .line 538
    :cond_6
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    .line 539
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    .line 540
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    .line 541
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 542
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v6, v4, Landroid/graphics/PointF;->y:F

    goto :goto_0
.end method

.method onTouchInput(Landroid/view/MotionEvent;)Z
    .locals 43
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 307
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-nez v9, :cond_0

    .line 308
    const/4 v9, 0x0

    .line 504
    :goto_0
    return v9

    .line 310
    :cond_0
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsPenUp:Z

    .line 311
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    and-int/lit16 v8, v9, 0xff

    .line 312
    .local v8, "action":I
    new-instance v42, Landroid/graphics/RectF;

    invoke-direct/range {v42 .. v42}, Landroid/graphics/RectF;-><init>()V

    .line 313
    .local v42, "tmpRect":Landroid/graphics/RectF;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    .line 314
    .local v4, "downTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    .line 315
    .local v6, "eventTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v12

    .line 316
    .local v12, "metaState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v13

    .line 317
    .local v13, "buttonState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getXPrecision()F

    move-result v14

    .line 318
    .local v14, "xPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getYPrecision()F

    move-result v15

    .line 319
    .local v15, "yPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v16

    .line 320
    .local v16, "deviceId":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v17

    .line 321
    .local v17, "edgeFlags":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v18

    .line 322
    .local v18, "source":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getFlags()I

    move-result v19

    .line 324
    .local v19, "flags":I
    const/4 v9, 0x1

    new-array v10, v9, [Landroid/view/MotionEvent$PointerProperties;

    .line 325
    .local v10, "pp":[Landroid/view/MotionEvent$PointerProperties;
    const/4 v9, 0x0

    new-instance v20, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct/range {v20 .. v20}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    aput-object v20, v10, v9

    .line 326
    const/4 v9, 0x0

    aget-object v9, v10, v9

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    .line 328
    const/4 v9, 0x1

    new-array v11, v9, [Landroid/view/MotionEvent$PointerCoords;

    .line 329
    .local v11, "pc":[Landroid/view/MotionEvent$PointerCoords;
    const/4 v9, 0x0

    new-instance v20, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v20 .. v20}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v20, v11, v9

    .line 330
    const/4 v9, 0x0

    const/16 v20, 0x0

    aget-object v20, v11, v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v9, v1}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    .line 332
    const/16 v38, 0x0

    .line 333
    .local v38, "ev":Landroid/view/MotionEvent;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v9

    if-lez v9, :cond_7

    .line 334
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v21, v0

    div-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 335
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v21, v0

    div-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 336
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 338
    const/4 v9, 0x1

    invoke-static/range {v4 .. v19}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v38

    .line 341
    const/16 v36, 0x1

    .local v36, "a":I
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v9

    move/from16 v0, v36

    if-lt v0, v9, :cond_6

    .line 348
    const/4 v9, 0x0

    aget-object v9, v11, v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v21, v0

    div-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 349
    const/4 v9, 0x0

    aget-object v9, v11, v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v21, v0

    div-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 350
    const/4 v9, 0x0

    aget-object v9, v11, v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 352
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v20

    move-object/from16 v0, v38

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2, v11, v12}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 362
    .end local v36    # "a":I
    :goto_2
    new-instance v37, Landroid/graphics/RectF;

    const/4 v9, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mWidth:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v0, v37

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-direct {v0, v9, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 363
    .local v37, "bitmapRect":Landroid/graphics/RectF;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v20

    move-object/from16 v0, v37

    move/from16 v1, v20

    invoke-virtual {v0, v9, v1}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v9

    if-eqz v9, :cond_14

    .line 365
    packed-switch v8, :pswitch_data_0

    .line 477
    :cond_1
    :goto_3
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    .line 491
    :cond_2
    :goto_4
    const/4 v9, 0x3

    if-ne v8, v9, :cond_4

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsPenUp:Z

    if-nez v9, :cond_4

    .line 492
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    if-eqz v9, :cond_3

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v9, :cond_3

    .line 493
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v38

    invoke-interface {v9, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Landroid/view/MotionEvent;)V

    .line 495
    :cond_3
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 496
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    .line 497
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 498
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsPenUp:Z

    .line 500
    :cond_4
    if-eqz v38, :cond_5

    .line 501
    invoke-virtual/range {v38 .. v38}, Landroid/view/MotionEvent;->recycle()V

    .line 502
    const/16 v38, 0x0

    .line 504
    :cond_5
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 342
    .end local v37    # "bitmapRect":Landroid/graphics/RectF;
    .restart local v36    # "a":I
    :cond_6
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 343
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v21, v0

    div-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 344
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v21, v0

    div-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 346
    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v20

    move-object/from16 v0, v38

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2, v11, v12}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 341
    add-int/lit8 v36, v36, 0x1

    goto/16 :goto_1

    .line 354
    .end local v36    # "a":I
    :cond_7
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v21, v0

    div-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 355
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v21, v0

    div-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 356
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 358
    const/4 v9, 0x1

    invoke-static/range {v4 .. v19}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v38

    goto/16 :goto_2

    .line 367
    .restart local v37    # "bitmapRect":Landroid/graphics/RectF;
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    if-eqz v9, :cond_8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    const-string v20, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_8

    .line 368
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setReferenceBitmap(Landroid/graphics/Bitmap;)V

    .line 370
    :cond_8
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsOOV:Z

    .line 371
    new-instance v9, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-direct {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>()V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 372
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPenName(Ljava/lang/String;)V

    .line 373
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getColor()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setColor(I)V

    .line 374
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getSize()F

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPenSize(F)V

    .line 376
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->isCurveEnabled()Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setCurveEnabled(Z)V

    .line 377
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setAdvancedPenSetting(Ljava/lang/String;)V

    .line 378
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setToolType(I)V

    .line 380
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    if-eqz v9, :cond_9

    .line 381
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v38

    invoke-interface {v9, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Landroid/view/MotionEvent;)V

    .line 384
    :cond_9
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v0, v38

    move-object/from16 v1, v42

    invoke-virtual {v9, v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    goto/16 :goto_3

    .line 388
    :pswitch_1
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsOOV:Z

    if-eqz v9, :cond_a

    .line 389
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v9

    const/16 v20, 0x1

    move/from16 v0, v20

    if-gt v9, v0, :cond_1

    .line 392
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsOOV:Z

    .line 393
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    .line 396
    :cond_a
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    if-nez v9, :cond_c

    .line 397
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->setAction(I)V

    .line 398
    const/4 v9, 0x0

    move-object/from16 v0, v38

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->setAction(I)V

    .line 400
    new-instance v9, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-direct {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>()V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 401
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPenName(Ljava/lang/String;)V

    .line 402
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getColor()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setColor(I)V

    .line 403
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getSize()F

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPenSize(F)V

    .line 404
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->isCurveEnabled()Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setCurveEnabled(Z)V

    .line 405
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setAdvancedPenSetting(Ljava/lang/String;)V

    .line 406
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setToolType(I)V

    .line 408
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->first:I

    .line 409
    invoke-virtual/range {v38 .. v38}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v9

    add-int/lit8 v36, v9, -0x1

    .restart local v36    # "a":I
    :goto_5
    if-gez v36, :cond_e

    .line 417
    :goto_6
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->first:I

    if-eqz v9, :cond_b

    .line 418
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->first:I

    move/from16 v20, v0

    move-object/from16 v0, v38

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 419
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->first:I

    move/from16 v20, v0

    move-object/from16 v0, v38

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 420
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->first:I

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 421
    invoke-virtual/range {v38 .. v38}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v22

    invoke-virtual/range {v38 .. v38}, Landroid/view/MotionEvent;->getAction()I

    move-result v24

    const/16 v25, 0x1

    move-wide/from16 v20, v4

    move-object/from16 v26, v10

    move-object/from16 v27, v11

    move/from16 v28, v12

    move/from16 v29, v13

    move/from16 v30, v14

    move/from16 v31, v15

    move/from16 v32, v16

    move/from16 v33, v17

    move/from16 v34, v18

    move/from16 v35, v19

    invoke-static/range {v20 .. v35}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v41

    .line 423
    .local v41, "tempEv":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->first:I

    move/from16 v40, v0

    .local v40, "i":I
    :goto_7
    invoke-virtual/range {v38 .. v38}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v9

    move/from16 v0, v40

    if-lt v0, v9, :cond_10

    .line 429
    invoke-virtual/range {v38 .. v38}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v20

    move-object/from16 v0, v41

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2, v11, v12}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 430
    invoke-virtual/range {v38 .. v38}, Landroid/view/MotionEvent;->recycle()V

    .line 431
    const/16 v38, 0x0

    .line 432
    move-object/from16 v38, v41

    .line 435
    .end local v40    # "i":I
    .end local v41    # "tempEv":Landroid/view/MotionEvent;
    :cond_b
    invoke-virtual/range {v38 .. v38}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v9

    if-nez v9, :cond_11

    .line 436
    const/4 v9, 0x0

    aget-object v9, v11, v9

    invoke-virtual/range {v38 .. v38}, Landroid/view/MotionEvent;->getX()F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 437
    const/4 v9, 0x0

    aget-object v9, v11, v9

    invoke-virtual/range {v38 .. v38}, Landroid/view/MotionEvent;->getY()F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 438
    const/4 v9, 0x0

    aget-object v9, v11, v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 444
    :goto_8
    invoke-virtual/range {v38 .. v38}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v22

    const/16 v24, 0x0

    const/16 v25, 0x1

    move-wide/from16 v20, v4

    move-object/from16 v26, v10

    move-object/from16 v27, v11

    move/from16 v28, v12

    move/from16 v29, v13

    move/from16 v30, v14

    move/from16 v31, v15

    move/from16 v32, v16

    move/from16 v33, v17

    move/from16 v34, v18

    move/from16 v35, v19

    invoke-static/range {v20 .. v35}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v39

    .line 447
    .local v39, "firstEv":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v0, v39

    move-object/from16 v1, v42

    invoke-virtual {v9, v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 448
    invoke-virtual/range {v42 .. v42}, Landroid/graphics/RectF;->setEmpty()V

    .line 451
    .end local v36    # "a":I
    .end local v39    # "firstEv":Landroid/view/MotionEvent;
    :cond_c
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    if-eqz v9, :cond_d

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v9, :cond_d

    .line 452
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v38

    invoke-interface {v9, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Landroid/view/MotionEvent;)V

    .line 455
    :cond_d
    const/4 v9, 0x2

    move-object/from16 v0, v38

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->setAction(I)V

    .line 456
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v0, v38

    move-object/from16 v1, v42

    invoke-virtual {v9, v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 457
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->invalidate()V

    goto/16 :goto_3

    .line 410
    .restart local v36    # "a":I
    :cond_e
    move-object/from16 v0, v38

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v20, v0

    div-float v9, v9, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v9, v9, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v20, v0

    mul-float v9, v9, v20

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->x:F

    .line 411
    move-object/from16 v0, v38

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v20, v0

    div-float v9, v9, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v9, v9, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v20, v0

    mul-float v9, v9, v20

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->y:F

    .line 412
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v37

    move/from16 v1, v20

    invoke-virtual {v0, v9, v1}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v9

    if-nez v9, :cond_f

    add-int/lit8 v9, v36, 0x1

    invoke-virtual/range {v38 .. v38}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v20

    move/from16 v0, v20

    if-ge v9, v0, :cond_f

    .line 413
    add-int/lit8 v9, v36, 0x1

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->first:I

    goto/16 :goto_6

    .line 409
    :cond_f
    add-int/lit8 v36, v36, -0x1

    goto/16 :goto_5

    .line 424
    .restart local v40    # "i":I
    .restart local v41    # "tempEv":Landroid/view/MotionEvent;
    :cond_10
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move-object/from16 v0, v38

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 425
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move-object/from16 v0, v38

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 426
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 427
    move-object/from16 v0, v38

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v20

    move-object/from16 v0, v41

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2, v11, v12}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 423
    add-int/lit8 v40, v40, 0x1

    goto/16 :goto_7

    .line 440
    .end local v40    # "i":I
    .end local v41    # "tempEv":Landroid/view/MotionEvent;
    :cond_11
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v20, 0x0

    move-object/from16 v0, v38

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 441
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v20, 0x0

    move-object/from16 v0, v38

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 442
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v20

    move/from16 v0, v20

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    goto/16 :goto_8

    .line 462
    .end local v36    # "a":I
    :pswitch_2
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsPenUp:Z

    .line 463
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    if-nez v9, :cond_12

    .line 464
    invoke-virtual/range {v38 .. v38}, Landroid/view/MotionEvent;->recycle()V

    .line 465
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 467
    :cond_12
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    if-eqz v9, :cond_13

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v9, :cond_13

    .line 468
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v38

    invoke-interface {v9, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Landroid/view/MotionEvent;)V

    .line 470
    :cond_13
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 472
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v0, v38

    move-object/from16 v1, v42

    invoke-virtual {v9, v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 473
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 474
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->invalidate()V

    goto/16 :goto_3

    .line 479
    :cond_14
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    if-eqz v9, :cond_2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    invoke-virtual {v9}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v9, v0, :cond_2

    .line 480
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsOOV:Z

    .line 481
    const/4 v9, 0x1

    move-object/from16 v0, v38

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->setAction(I)V

    .line 482
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsPenUp:Z

    .line 483
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    if-eqz v9, :cond_15

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v9, :cond_15

    .line 484
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v38

    invoke-interface {v9, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Landroid/view/MotionEvent;)V

    .line 486
    :cond_15
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 487
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    .line 488
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/graphics/Bitmap;->eraseColor(I)V

    goto/16 :goto_4

    .line 365
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method onTouchRemover(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v9, 0x41a00000    # 20.0f

    const/4 v5, 0x0

    const/high16 v8, -0x3d380000    # -100.0f

    const/4 v7, 0x1

    const/high16 v6, 0x40000000    # 2.0f

    .line 551
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v0, v4, 0xff

    .line 552
    .local v0, "action":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {p1, v5, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 553
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 554
    .local v2, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 555
    .local v3, "y":F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    neg-float v4, v4

    invoke-virtual {p1, v5, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 557
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    div-float v1, v4, v5

    .line 558
    .local v1, "rate":F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-nez v4, :cond_0

    .line 559
    new-instance v4, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 561
    :cond_0
    if-nez v0, :cond_4

    .line 562
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    .line 563
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-nez v4, :cond_3

    .line 564
    mul-float v4, v9, v1

    div-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    .line 568
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 569
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v3, v4, Landroid/graphics/PointF;->y:F

    .line 570
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    mul-float v5, v6, v1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 590
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->invalidate()V

    .line 591
    return v7

    .line 565
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-ne v4, v7, :cond_1

    .line 566
    const/high16 v4, 0x42200000    # 40.0f

    mul-float/2addr v4, v1

    div-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    goto :goto_0

    .line 571
    :cond_4
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    if-eqz v4, :cond_2

    .line 572
    const/4 v4, 0x2

    if-eq v0, v4, :cond_5

    const/4 v4, 0x5

    if-eq v0, v4, :cond_5

    .line 573
    const/4 v4, 0x6

    if-ne v0, v4, :cond_8

    .line 574
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-nez v4, :cond_7

    .line 575
    mul-float v4, v9, v1

    div-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    .line 579
    :cond_6
    :goto_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 580
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v3, v4, Landroid/graphics/PointF;->y:F

    .line 581
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    mul-float v5, v6, v1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_1

    .line 576
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-ne v4, v7, :cond_6

    .line 577
    const/high16 v4, 0x42200000    # 40.0f

    mul-float/2addr v4, v1

    div-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    goto :goto_2

    .line 583
    :cond_8
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    .line 584
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    .line 585
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v8, v4, Landroid/graphics/PointF;->x:F

    .line 586
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v8, v4, Landroid/graphics/PointF;->y:F

    goto :goto_1
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    .prologue
    .line 301
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    .line 302
    return-void
.end method

.method public setBitmap([Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # [Landroid/graphics/Bitmap;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    .line 173
    return-void
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    .line 244
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 245
    return-void
.end method

.method setHandleSize(II)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_WIDTH:I

    .line 192
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_HEIGHT:I

    .line 193
    return-void
.end method

.method public setHeightAndRate(FF)V
    .locals 0
    .param p1, "height"    # F
    .param p2, "rate"    # F

    .prologue
    .line 205
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    .line 206
    return-void
.end method

.method public setLayer(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 176
    .local p1, "layer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCanvasLayer:Ljava/util/ArrayList;

    .line 177
    return-void
.end method

.method public setPanAndZoom(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 180
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaX:F

    .line 181
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaY:F

    .line 182
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    .line 183
    return-void
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    .line 209
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-nez v1, :cond_0

    .line 210
    new-instance v1, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    .line 214
    :cond_0
    :try_start_0
    iget-object v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    .line 215
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v1, :cond_1

    .line 216
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V

    .line 218
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->createPen(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 228
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 229
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setColor(I)V

    .line 230
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V

    .line 231
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 233
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-boolean v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setCurveEnabled(Z)V

    .line 235
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 236
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setAdvancedSetting(Ljava/lang/String;)V

    .line 238
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_4

    .line 239
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setReferenceBitmap(Landroid/graphics/Bitmap;)V

    .line 241
    :cond_4
    return-void

    .line 219
    :catch_0
    move-exception v0

    .line 220
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 221
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 222
    .local v0, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 223
    .end local v0    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v0

    .line 224
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 225
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 226
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setRect(Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 196
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    .line 197
    return-void
.end method

.method public setReferenceBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "mRef"    # Landroid/graphics/Bitmap;

    .prologue
    .line 253
    if-eqz p1, :cond_1

    .line 254
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 257
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    .line 259
    :cond_1
    return-void
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 248
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 249
    return-void
.end method

.method public setScreenStart(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 186
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartX:I

    .line 187
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartY:I

    .line 188
    return-void
.end method

.method public update()V
    .locals 0

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->postInvalidate()V

    .line 267
    return-void
.end method

.method public updateFrameBuffer(ZLandroid/graphics/RectF;)V
    .locals 10
    .param p1, "isUpdate"    # Z
    .param p2, "boxRect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 270
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCanvasLayer:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    .line 271
    new-instance v3, Landroid/graphics/Canvas;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-direct {v3, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 272
    .local v3, "canvas":Landroid/graphics/Canvas;
    sget-object v6, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v8, v6}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 274
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 275
    .local v1, "abRectF":Landroid/graphics/RectF;
    invoke-direct {p0, v1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 276
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 277
    .local v0, "abRect":Landroid/graphics/Rect;
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->ExtendRectFromRectF(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    .line 278
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 279
    .local v4, "dstRect":Landroid/graphics/RectF;
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    int-to-float v7, v7

    invoke-virtual {v4, v9, v9, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 282
    const/4 v5, 0x0

    .local v5, "layer":I
    :goto_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v5, v6, :cond_1

    .line 294
    if-eqz p1, :cond_0

    .line 295
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->postInvalidate()V

    .line 298
    .end local v0    # "abRect":Landroid/graphics/Rect;
    .end local v1    # "abRectF":Landroid/graphics/RectF;
    .end local v3    # "canvas":Landroid/graphics/Canvas;
    .end local v4    # "dstRect":Landroid/graphics/RectF;
    .end local v5    # "layer":I
    :cond_0
    return-void

    .line 283
    .restart local v0    # "abRect":Landroid/graphics/Rect;
    .restart local v1    # "abRectF":Landroid/graphics/RectF;
    .restart local v3    # "canvas":Landroid/graphics/Canvas;
    .restart local v4    # "dstRect":Landroid/graphics/RectF;
    .restart local v5    # "layer":I
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 284
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_2

    .line 285
    invoke-virtual {v2, v8, v8}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    invoke-virtual {v2, v8, v8, v6}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 286
    if-nez v5, :cond_3

    .line 287
    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mSrcPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2, v0, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 282
    :cond_2
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 289
    :cond_3
    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2, v0, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public updateRect(Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    .line 201
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->invalidate()V

    .line 202
    return-void
.end method
