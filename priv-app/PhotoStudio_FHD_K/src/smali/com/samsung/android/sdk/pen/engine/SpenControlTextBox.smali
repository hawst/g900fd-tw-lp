.class public Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;
.super Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.source "SpenControlTextBox.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_BORDER_LEFT_MARGIN:I = 0x3

.field private static final TEXT_INPUT_UNLIMITED:I = -0x1


# instance fields
.field private BOTTOM_MARGIN:I

.field private LEFT_MARGIN:I

.field private RIGHT_MARGIN:I

.field private TOP_MARGIN:I

.field private mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

.field private mEditable:Z

.field private mFirstDraw:Z

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mIsShowSoftInputEnable:Z

.field private mReceiveActionDown:Z

.field private final mRequestObjectChange:Ljava/lang/Runnable;

.field private mSizeChanged:Z

.field private mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

.field private final mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

.field private mTextLimitCount:I

.field private mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

.field private mTransactionFitTextBox:Z

.field private final mUpdateHandler:Landroid/os/Handler;

.field private mUseTextEraser:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 113
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 32
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 34
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    .line 35
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUseTextEraser:Z

    .line 36
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mReceiveActionDown:Z

    .line 37
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionFitTextBox:Z

    .line 39
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsShowSoftInputEnable:Z

    .line 44
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->LEFT_MARGIN:I

    .line 45
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->RIGHT_MARGIN:I

    .line 46
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->TOP_MARGIN:I

    .line 47
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->BOTTOM_MARGIN:I

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextLimitCount:I

    .line 52
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    .line 57
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mFirstDraw:Z

    .line 1154
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    .line 115
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 116
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    .line 118
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUpdateHandler:Landroid/os/Handler;

    .line 120
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mRequestObjectChange:Ljava/lang/Runnable;

    .line 126
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;Z)V
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mReceiveActionDown:Z

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    return-object v0
.end method

.method private checkInvalidStateEditable()V
    .locals 1

    .prologue
    .line 1292
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-nez v0, :cond_0

    .line 1293
    const/16 v0, 0x8

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 1295
    :cond_0
    return-void
.end method

.method private checkSettingTextInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 4
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x7

    .line 595
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    if-ltz v0, :cond_0

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_1

    .line 596
    :cond_0
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 599
    :cond_1
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    if-ltz v0, :cond_2

    .line 600
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_3

    .line 601
    :cond_2
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 604
    :cond_3
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    cmpg-float v0, v0, v3

    if-ltz v0, :cond_4

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_5

    .line 605
    :cond_4
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 608
    :cond_5
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-ltz v0, :cond_6

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-le v0, v2, :cond_7

    .line 609
    :cond_6
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 611
    :cond_7
    return-void
.end method

.method private checkTextBoxValidation()Z
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-nez v0, :cond_0

    .line 589
    const/4 v0, 0x0

    .line 591
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private verifyRect(Landroid/graphics/RectF;)Z
    .locals 3
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 1091
    if-nez p1, :cond_1

    .line 1098
    :cond_0
    :goto_0
    return v0

    .line 1095
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 1096
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public appendText(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 622
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 632
    :goto_0
    return-void

    .line 626
    :cond_0
    if-nez p1, :cond_1

    .line 627
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 631
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->appendText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 999
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsClosed:Z

    .line 1000
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1001
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v2, :cond_0

    .line 1002
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->close()V

    .line 1003
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 1034
    :cond_0
    :goto_0
    return-void

    .line 1009
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v2, :cond_2

    .line 1010
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getObjectText()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v1

    .line 1011
    .local v1, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->close()V

    .line 1012
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    :try_end_0
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1014
    if-eqz v1, :cond_2

    .line 1015
    :try_start_1
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->parseHyperText()V

    .line 1017
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v2, :cond_3

    .line 1018
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    .line 1019
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onVisibleUpdated(Z)V
    :try_end_1
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1033
    .end local v1    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    :cond_2
    :goto_1
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    goto :goto_0

    .line 1021
    .restart local v1    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    :cond_3
    const/4 v2, 0x0

    :try_start_2
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    .line 1022
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V
    :try_end_2
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 1025
    :catch_0
    move-exception v0

    .line 1026
    .local v0, "e":Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;
    :try_start_3
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;->printStackTrace()V
    :try_end_3
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 1029
    .end local v0    # "e":Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;
    .end local v1    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    :catch_1
    move-exception v0

    .line 1030
    .restart local v0    # "e":Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;->printStackTrace()V

    goto :goto_1
.end method

.method public fit(Z)V
    .locals 1
    .param p1, "checkCursorPosition"    # Z

    .prologue
    .line 392
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    .line 394
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionFitTextBox:Z

    if-eqz v0, :cond_0

    .line 405
    :goto_0
    return-void

    .line 398
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionFitTextBox:Z

    .line 400
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 404
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionFitTextBox:Z

    goto :goto_0
.end method

.method public getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    .locals 2

    .prologue
    .line 433
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    .line 434
    .local v0, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 435
    :cond_0
    const/4 v1, 0x0

    .line 438
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    goto :goto_0
.end method

.method protected getPanKey(II)Ljava/lang/String;
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v0, :cond_1

    .line 131
    if-le p1, p2, :cond_0

    .line 132
    new-instance v0, Ljava/lang/String;

    const-string v1, "WidthHeight"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 137
    :goto_0
    return-object v0

    .line 134
    :cond_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "HeightWidth"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getPanKey(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPixel(II)I
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1081
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    .line 1082
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getX()F

    move-result v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    sub-int/2addr p1, v0

    .line 1083
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getY()F

    move-result v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    sub-int/2addr p2, v0

    .line 1085
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getPixel(II)I

    move-result v0

    .line 1087
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getText(Z)Ljava/lang/String;
    .locals 1
    .param p1, "onlySelection"    # Z

    .prologue
    .line 710
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 711
    const/4 v0, 0x0

    .line 714
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTextCursorPosition()I
    .locals 1

    .prologue
    .line 759
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 760
    const/4 v0, 0x0

    .line 765
    :goto_0
    return v0

    .line 763
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkInvalidStateEditable()V

    .line 765
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorPos()I

    move-result v0

    goto :goto_0
.end method

.method public getTextLimit()I
    .locals 1

    .prologue
    .line 803
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 804
    const/4 v0, -0x1

    .line 807
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLimit()I

    move-result v0

    goto :goto_0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 2

    .prologue
    .line 978
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    .line 979
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 980
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 981
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 982
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 983
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 984
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 985
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    .line 986
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    .line 988
    return-object v0
.end method

.method protected handleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 443
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_0

    .line 444
    const/4 v8, 0x0

    .line 519
    :goto_0
    return v8

    .line 447
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    and-int/lit16 v8, v8, 0xff

    packed-switch v8, :pswitch_data_0

    .line 519
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    goto :goto_0

    .line 449
    :pswitch_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v8

    if-nez v8, :cond_1

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v8

    if-nez v8, :cond_1

    .line 450
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 452
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 453
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setAutoFitOption(I)V

    .line 456
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isHorizontalResizeZonePressed()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 457
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v6

    .line 458
    .local v6, "option":I
    and-int/lit8 v6, v6, -0x2

    .line 459
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setAutoFitOption(I)V

    .line 462
    .end local v6    # "option":I
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isVerticalResizeZonePressed()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 463
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v6

    .line 464
    .restart local v6    # "option":I
    and-int/lit8 v6, v6, -0x3

    .line 465
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setAutoFitOption(I)V

    .line 468
    .end local v6    # "option":I
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultWidth()I

    move-result v5

    .line 469
    .local v5, "minTextWidth":I
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultHeight()I

    move-result v4

    .line 471
    .local v4, "minTextHeight":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v3, v8, :cond_6

    .line 507
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V

    .line 510
    .end local v3    # "i":I
    .end local v4    # "minTextHeight":I
    .end local v5    # "minTextWidth":I
    :cond_5
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    .line 512
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 514
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->invalidate()V

    .line 516
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 472
    .restart local v3    # "i":I
    .restart local v4    # "minTextHeight":I
    .restart local v5    # "minTextWidth":I
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getRectList()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/RectF;

    .line 473
    .local v7, "rectf":Landroid/graphics/RectF;
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v0

    .line 474
    .local v0, "boundBox":Landroid/graphics/RectF;
    if-nez v0, :cond_7

    .line 475
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 478
    :cond_7
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v8

    int-to-float v9, v5

    cmpg-float v8, v8, v9

    if-gez v8, :cond_9

    .line 479
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v9, 0x3

    if-eq v8, v9, :cond_8

    .line 480
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v9, 0x5

    if-ne v8, v9, :cond_c

    .line 481
    :cond_8
    iget v8, v7, Landroid/graphics/RectF;->left:F

    int-to-float v9, v5

    add-float/2addr v8, v9

    iput v8, v7, Landroid/graphics/RectF;->right:F

    .line 482
    iget v8, v0, Landroid/graphics/RectF;->left:F

    int-to-float v9, v5

    add-float/2addr v8, v9

    iput v8, v0, Landroid/graphics/RectF;->right:F

    .line 490
    :cond_9
    :goto_2
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v8

    int-to-float v9, v4

    cmpg-float v8, v8, v9

    if-gez v8, :cond_b

    .line 491
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_a

    .line 492
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v9, 0x6

    if-eq v8, v9, :cond_a

    .line 493
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v9, 0x7

    if-ne v8, v9, :cond_e

    .line 494
    :cond_a
    iget v8, v7, Landroid/graphics/RectF;->top:F

    int-to-float v9, v4

    add-float/2addr v8, v9

    iput v8, v7, Landroid/graphics/RectF;->bottom:F

    .line 495
    iget v8, v0, Landroid/graphics/RectF;->top:F

    int-to-float v9, v4

    add-float/2addr v8, v9

    iput v8, v0, Landroid/graphics/RectF;->bottom:F

    .line 503
    :cond_b
    :goto_3
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    invoke-virtual {v7}, Landroid/graphics/RectF;->centerX()F

    move-result v9

    sub-float/2addr v8, v9

    float-to-int v1, v8

    .line 504
    .local v1, "deltaX":I
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    invoke-virtual {v7}, Landroid/graphics/RectF;->centerY()F

    move-result v9

    sub-float/2addr v8, v9

    float-to-int v2, v8

    .line 505
    .local v2, "deltaY":I
    int-to-float v8, v1

    int-to-float v9, v2

    invoke-virtual {v7, v8, v9}, Landroid/graphics/RectF;->offset(FF)V

    .line 471
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 483
    .end local v1    # "deltaX":I
    .end local v2    # "deltaY":I
    :cond_c
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v9, 0x6

    if-eq v8, v9, :cond_d

    .line 484
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v9, 0x1

    if-eq v8, v9, :cond_d

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v9, 0x4

    if-ne v8, v9, :cond_9

    .line 485
    :cond_d
    iget v8, v7, Landroid/graphics/RectF;->right:F

    int-to-float v9, v5

    sub-float/2addr v8, v9

    iput v8, v7, Landroid/graphics/RectF;->left:F

    .line 486
    iget v8, v0, Landroid/graphics/RectF;->right:F

    int-to-float v9, v5

    sub-float/2addr v8, v9

    iput v8, v0, Landroid/graphics/RectF;->left:F

    goto :goto_2

    .line 496
    :cond_e
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v9, 0x1

    if-eq v8, v9, :cond_f

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v9, 0x2

    if-eq v8, v9, :cond_f

    .line 497
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v9, 0x3

    if-ne v8, v9, :cond_b

    .line 498
    :cond_f
    iget v8, v7, Landroid/graphics/RectF;->bottom:F

    int-to-float v9, v4

    sub-float/2addr v8, v9

    iput v8, v7, Landroid/graphics/RectF;->top:F

    .line 499
    iget v8, v0, Landroid/graphics/RectF;->bottom:F

    int-to-float v9, v4

    sub-float/2addr v8, v9

    iput v8, v0, Landroid/graphics/RectF;->top:F

    goto :goto_3

    .line 447
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public hideSoftInput()V
    .locals 1

    .prologue
    .line 317
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 322
    :goto_0
    return-void

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideSoftInput()V

    goto :goto_0
.end method

.method public isContextMenuVisible()Z
    .locals 1

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isEditable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1136
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isContextMenuVisible()Z

    move-result v0

    .line 1139
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isContextMenuVisible()Z

    move-result v0

    goto :goto_0
.end method

.method public isEditable()Z
    .locals 1

    .prologue
    .line 295
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    return v0
.end method

.method public isSelectByKey()Z
    .locals 1

    .prologue
    .line 1151
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isSelectByKey()Z

    move-result v0

    return v0
.end method

.method public isTextEraserEnabled()Z
    .locals 1

    .prologue
    .line 1062
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUseTextEraser:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 185
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    .line 187
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 188
    .local v2, "rect":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 189
    .local v1, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->verifyRect(Landroid/graphics/RectF;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 190
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v3

    if-nez v3, :cond_0

    .line 223
    :goto_0
    return-void

    .line 194
    :cond_0
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v7

    invoke-direct {v4, v5, v3, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;II)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 195
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setObjectText(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 196
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUseTextEraser:Z

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setEraserMode(Z)V

    .line 197
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextBoxListener(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;)V

    .line 198
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v4

    invoke-virtual {v3, v4, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 199
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->LEFT_MARGIN:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->TOP_MARGIN:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->RIGHT_MARGIN:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->BOTTOM_MARGIN:I

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setMargin(IIII)V

    .line 200
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsShowSoftInputEnable:Z

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setShowSoftInputEnable(Z)V

    .line 201
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsShowSoftInputEnable:Z

    .line 202
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->enableTextInput(Z)V

    .line 204
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextLimitCount:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 205
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextLimitCount:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextLimit(I)V

    .line 208
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "desctription":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 210
    const-string v0, ""

    .line 212
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TextBox "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 213
    const/16 v3, 0x80

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->sendAccessibilityEvent(I)V

    .line 215
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v3, :cond_4

    .line 216
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 222
    .end local v0    # "desctription":Ljava/lang/String;
    :cond_3
    :goto_1
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onAttachedToWindow()V

    goto/16 :goto_0

    .line 218
    .restart local v0    # "desctription":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->bringToFront()V

    goto :goto_1
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 350
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mFirstDraw:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mFirstDraw:Z

    .line 352
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUpdateHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mRequestObjectChange:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 355
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_1

    .line 366
    :goto_0
    return-void

    .line 359
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    if-eqz v0, :cond_2

    .line 360
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    .line 361
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->seFitOnSizeChanged()V

    .line 362
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 365
    :cond_2
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onDrawBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "rect"    # Landroid/graphics/RectF;
    .param p3, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 326
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getStyle()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isEditable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 327
    iget v1, p2, Landroid/graphics/RectF;->left:F

    const/high16 v2, 0x40400000    # 3.0f

    sub-float/2addr v1, v2

    iput v1, p2, Landroid/graphics/RectF;->left:F

    .line 329
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v0

    .line 331
    .local v0, "tmpPaint":Landroid/graphics/Paint;
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SM-N750"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 332
    const/high16 v1, 0x40800000    # 4.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 335
    :cond_0
    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 339
    .end local v0    # "tmpPaint":Landroid/graphics/Paint;
    :goto_0
    return-void

    .line 337
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDrawBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 370
    invoke-super {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onSizeChanged(IIII)V

    .line 371
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;-><init>()V

    .line 372
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 374
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    .line 376
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isContextMenuShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 377
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextMenuLocation()V

    .line 379
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 532
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-nez v2, :cond_0

    move v0, v1

    .line 578
    :goto_0
    return v0

    .line 534
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isTouchEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 535
    const/4 v0, 0x0

    goto :goto_0

    .line 538
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    .line 539
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mReceiveActionDown:Z

    .line 542
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionTouchEvent:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->check(Landroid/view/MotionEvent;)V

    .line 544
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-boolean v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-eqz v2, :cond_4

    .line 545
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 546
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_3

    .line 547
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->close()V

    :cond_3
    move v0, v1

    .line 549
    goto :goto_0

    .line 552
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    .line 563
    :cond_5
    :goto_1
    const/4 v0, 0x0

    .line 564
    .local v0, "ret":Z
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->handleTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 565
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v2, :cond_6

    .line 566
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 568
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    if-ne v2, v1, :cond_7

    .line 569
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    .line 573
    :cond_7
    const/4 v0, 0x1

    .line 576
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 554
    .end local v0    # "ret":Z
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v2, :cond_9

    .line 555
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->hide()V

    .line 557
    :cond_9
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-nez v2, :cond_5

    .line 558
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideTextBox()V

    goto :goto_1

    .line 552
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected onVisibleUpdated(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onVisibleUpdated(Ljava/util/ArrayList;Z)V

    .line 146
    :cond_0
    return-void
.end method

.method protected onZoom()V
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    .line 162
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onZoom()V

    .line 163
    return-void
.end method

.method public removeText()V
    .locals 1

    .prologue
    .line 641
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 646
    :goto_0
    return-void

    .line 645
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    goto :goto_0
.end method

.method public selectAllText()V
    .locals 1

    .prologue
    .line 859
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 866
    :goto_0
    return-void

    .line 863
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkInvalidStateEditable()V

    .line 865
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelectionAll()V

    goto :goto_0
.end method

.method protected setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;)V
    .locals 0
    .param p1, "actionListener"    # Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    .line 157
    return-void
.end method

.method protected setCheckCursorOnScroll(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 582
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 585
    :cond_0
    return-void
.end method

.method public setContextMenuVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isEditable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1115
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setContextMenuVisible(Z)V

    .line 1116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextMenuLocation()V

    .line 1120
    :goto_0
    return-void

    .line 1118
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setContextMenuVisible(Z)V

    goto :goto_0
.end method

.method public setEditable(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 239
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsClosed:Z

    if-eqz v0, :cond_1

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eq v0, p1, :cond_0

    .line 247
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    .line 248
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v0, :cond_3

    .line 249
    invoke-super {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setContextMenuVisible(Z)V

    .line 251
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_2

    .line 252
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->bringToFront()V

    .line 253
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsShowSoftInputEnable:Z

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setShowSoftInputEnable(Z)V

    .line 254
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsShowSoftInputEnable:Z

    .line 255
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->enableTextInput(Z)V

    .line 257
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setStyle(I)V

    .line 275
    :goto_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-nez v0, :cond_0

    .line 276
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->bringToFront()V

    goto :goto_0

    .line 259
    :cond_3
    invoke-super {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setContextMenuVisible(Z)V

    .line 261
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onVisibleUpdated(Z)V

    .line 263
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_4

    .line 264
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsClosed:Z

    .line 265
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V

    .line 266
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsClosed:Z

    .line 268
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusable(Z)V

    .line 269
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideTextBox()V

    .line 270
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideSoftInput()V

    .line 272
    :cond_4
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setStyle(I)V

    goto :goto_1
.end method

.method public setMargin(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 663
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->LEFT_MARGIN:I

    .line 664
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->RIGHT_MARGIN:I

    .line 665
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->TOP_MARGIN:I

    .line 666
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->BOTTOM_MARGIN:I

    .line 668
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 673
    :goto_0
    return-void

    .line 672
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setMargin(IIII)V

    goto :goto_0
.end method

.method public setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 1
    .param p1, "ObjectTextBox"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 416
    if-nez p1, :cond_0

    .line 423
    :goto_0
    return-void

    .line 419
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 420
    .local v0, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 422
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setObjectList(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public setSelection(II)V
    .locals 5
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x1

    .line 826
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v1

    if-nez v1, :cond_0

    .line 848
    :goto_0
    return-void

    .line 830
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkInvalidStateEditable()V

    .line 832
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v0

    .line 833
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_1

    if-le p1, p2, :cond_2

    .line 834
    :cond_1
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 838
    :cond_2
    if-ltz p1, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le p2, v1, :cond_4

    .line 839
    :cond_3
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 843
    :cond_4
    if-ne p1, p2, :cond_5

    .line 844
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, p1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto :goto_0

    .line 846
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, p1, p2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_0
.end method

.method protected setShowSoftInputEnable(Z)V
    .locals 0
    .param p1, "isShowSoftInputEnable"    # Z

    .prologue
    .line 281
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsShowSoftInputEnable:Z

    .line 282
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 687
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 697
    :goto_0
    return-void

    .line 691
    :cond_0
    if-nez p1, :cond_1

    .line 692
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 696
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTextCursorPosition(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x7

    .line 729
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v1

    if-nez v1, :cond_0

    .line 747
    :goto_0
    return-void

    .line 733
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkInvalidStateEditable()V

    .line 735
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v0

    .line 736
    .local v0, "str":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 737
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 741
    :cond_1
    if-ltz p1, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le p1, v1, :cond_3

    .line 742
    :cond_2
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 746
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto :goto_0
.end method

.method public setTextEraserEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1046
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUseTextEraser:Z

    .line 1047
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    .line 1048
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setEraserMode(Z)V

    .line 1050
    :cond_0
    return-void
.end method

.method public setTextLimit(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 781
    const/4 v0, -0x1

    if-ge p1, v0, :cond_0

    .line 782
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 787
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_1

    .line 792
    :goto_1
    return-void

    .line 784
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextLimitCount:I

    goto :goto_0

    .line 791
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextLimit(I)V

    goto :goto_1
.end method

.method public setTextSelectionContextMenu(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V
    .locals 1
    .param p1, "contextMenu"    # Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .prologue
    .line 877
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    .line 878
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setContextMenu(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    .line 880
    :cond_0
    return-void
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 11
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 891
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v6

    if-nez v6, :cond_1

    .line 968
    :cond_0
    :goto_0
    return-void

    .line 895
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkSettingTextInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 897
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-eq v6, v7, :cond_4

    .line 898
    const/4 v5, 0x0

    .line 899
    .local v5, "type":I
    const/4 v2, 0x0

    .line 901
    .local v2, "option":Z
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-le v6, v7, :cond_c

    .line 902
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    sub-int v5, v6, v7

    .line 903
    const/4 v2, 0x0

    .line 909
    :goto_1
    and-int/lit8 v6, v5, 0x1

    if-ne v6, v8, :cond_2

    .line 910
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v8, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextStyle(IZ)V

    .line 912
    :cond_2
    and-int/lit8 v6, v5, 0x2

    if-ne v6, v9, :cond_3

    .line 913
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v9, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextStyle(IZ)V

    .line 915
    :cond_3
    and-int/lit8 v6, v5, 0x4

    if-ne v6, v10, :cond_4

    .line 916
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v10, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextStyle(IZ)V

    .line 919
    .end local v2    # "option":Z
    .end local v5    # "type":I
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    if-eq v6, v7, :cond_5

    .line 920
    new-instance v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;-><init>()V

    .line 921
    .local v4, "txtSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v6, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    .line 923
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    .line 925
    .end local v4    # "txtSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_6

    .line 926
    new-instance v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;-><init>()V

    .line 927
    .local v4, "txtSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v6, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    .line 929
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    .line 931
    .end local v4    # "txtSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iget-object v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_7

    .line 932
    new-instance v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;-><init>()V

    .line 933
    .local v4, "txtSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;
    iget-object v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v6, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    .line 935
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    .line 937
    .end local v4    # "txtSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;
    :cond_7
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    if-eq v6, v7, :cond_8

    .line 938
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;-><init>()V

    .line 939
    .local v1, "directionInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    int-to-char v6, v6

    iput v6, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->textDirection:I

    .line 941
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    .line 943
    .end local v1    # "directionInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;
    :cond_8
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    if-eq v6, v7, :cond_9

    .line 944
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;-><init>()V

    .line 945
    .local v0, "alignInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    int-to-char v6, v6

    iput v6, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    .line 946
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V

    .line 948
    .end local v0    # "alignInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;
    :cond_9
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    cmpl-float v6, v6, v7

    if-nez v6, :cond_a

    .line 949
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    if-eq v6, v7, :cond_b

    .line 950
    :cond_a
    new-instance v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;-><init>()V

    .line 951
    .local v3, "spacingInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    iput v6, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    .line 952
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v6, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    .line 953
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V

    .line 956
    .end local v3    # "spacingInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;
    :cond_b
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 957
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 958
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 959
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 960
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 961
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    .line 962
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    .line 964
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-nez v6, :cond_0

    .line 965
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    .line 966
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V

    goto/16 :goto_0

    .line 905
    .restart local v2    # "option":Z
    .restart local v5    # "type":I
    :cond_c
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    sub-int v5, v6, v7

    .line 906
    const/4 v2, 0x1

    goto/16 :goto_1
.end method

.method public setTouchEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 177
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setTouchEnabled(Z)V

    .line 178
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->enableTouch(Z)V

    .line 181
    :cond_0
    return-void
.end method

.method public showSoftInput()V
    .locals 1

    .prologue
    .line 304
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 309
    :goto_0
    return-void

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showSoftInput()V

    goto :goto_0
.end method
