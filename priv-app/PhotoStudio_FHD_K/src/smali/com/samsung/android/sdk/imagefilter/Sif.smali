.class public Lcom/samsung/android/sdk/imagefilter/Sif;
.super Ljava/lang/Object;
.source "Sif.java"

# interfaces
.implements Lcom/samsung/android/sdk/interfaces/ISamsungSdk;


# static fields
.field private static final CODENAME:Ljava/lang/String; = "Trieste"

.field private static final VERSION:Ljava/lang/String; = "1.0"

.field private static final VERSION_LEVEL:I = 0x1

.field private static mIsInitialized:Z


# instance fields
.field private mSPenSdk:Lcom/samsung/android/sdk/SamsungSdkManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/imagefilter/Sif;->mIsInitialized:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string v0, "1.0"

    return-object v0
.end method

.method public getVersionCodename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const-string v0, "Trieste"

    return-object v0
.end method

.method public getVersionLevel()I
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    return v0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "context is null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 21
    :cond_0
    sget-boolean v1, Lcom/samsung/android/sdk/imagefilter/Sif;->mIsInitialized:Z

    if-eqz v1, :cond_1

    .line 42
    :goto_0
    return-void

    .line 35
    :cond_1
    :try_start_0
    const-string v1, "SPenImageFilterLibs"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 36
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/android/sdk/imagefilter/Sif;->mIsInitialized:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public requestUpdate()V
    .locals 1

    .prologue
    .line 61
    sget-boolean v0, Lcom/samsung/android/sdk/imagefilter/Sif;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 65
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/imagefilter/Sif;->mSPenSdk:Lcom/samsung/android/sdk/SamsungSdkManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/SamsungSdkManager;->requestUpdate()V

    goto :goto_0
.end method
