.class public Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;
.super Ljava/lang/Object;
.source "SpenContextMenuItemInfo.java"


# instance fields
.field public drawableBackgroundNormal:Landroid/graphics/drawable/Drawable;

.field public drawableBackgroundPressed:Landroid/graphics/drawable/Drawable;

.field public drawableDisableItem:Landroid/graphics/drawable/Drawable;

.field public drawableNormalItem:Landroid/graphics/drawable/Drawable;

.field public enable:Z

.field public id:I

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    .line 25
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableNormalItem:Landroid/graphics/drawable/Drawable;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableDisableItem:Landroid/graphics/drawable/Drawable;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundNormal:Landroid/graphics/drawable/Drawable;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundPressed:Landroid/graphics/drawable/Drawable;

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    .line 69
    return-void
.end method

.method public constructor <init>(ILandroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "drawableNormalItem"    # Landroid/graphics/drawable/Drawable;
    .param p3, "drawableDisableItem"    # Landroid/graphics/drawable/Drawable;
    .param p4, "drawableBackgroundNormal"    # Landroid/graphics/drawable/Drawable;
    .param p5, "drawableBackgroundPressed"    # Landroid/graphics/drawable/Drawable;
    .param p6, "name"    # Ljava/lang/String;
    .param p7, "enable"    # Z

    .prologue
    const/4 v1, 0x0

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    .line 25
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableNormalItem:Landroid/graphics/drawable/Drawable;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableDisableItem:Landroid/graphics/drawable/Drawable;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundNormal:Landroid/graphics/drawable/Drawable;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundPressed:Landroid/graphics/drawable/Drawable;

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    .line 168
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    .line 169
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableNormalItem:Landroid/graphics/drawable/Drawable;

    .line 170
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableDisableItem:Landroid/graphics/drawable/Drawable;

    .line 171
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundNormal:Landroid/graphics/drawable/Drawable;

    .line 172
    iput-object p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundPressed:Landroid/graphics/drawable/Drawable;

    .line 173
    iput-object p6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    .line 174
    iput-boolean p7, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    .line 175
    return-void
.end method

.method public constructor <init>(ILandroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "drawableItem"    # Landroid/graphics/drawable/Drawable;
    .param p3, "drawableBackgroundNormal"    # Landroid/graphics/drawable/Drawable;
    .param p4, "drawableBackgroundPressed"    # Landroid/graphics/drawable/Drawable;
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "enable"    # Z

    .prologue
    const/4 v1, 0x0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    .line 25
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableNormalItem:Landroid/graphics/drawable/Drawable;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableDisableItem:Landroid/graphics/drawable/Drawable;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundNormal:Landroid/graphics/drawable/Drawable;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundPressed:Landroid/graphics/drawable/Drawable;

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    .line 135
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    .line 136
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableNormalItem:Landroid/graphics/drawable/Drawable;

    .line 137
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundNormal:Landroid/graphics/drawable/Drawable;

    .line 138
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundPressed:Landroid/graphics/drawable/Drawable;

    .line 139
    iput-object p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    .line 140
    iput-boolean p6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    .line 141
    return-void
.end method

.method public constructor <init>(ILandroid/graphics/drawable/Drawable;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "drawableItem"    # Landroid/graphics/drawable/Drawable;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "enable"    # Z

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    .line 25
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableNormalItem:Landroid/graphics/drawable/Drawable;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableDisableItem:Landroid/graphics/drawable/Drawable;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundNormal:Landroid/graphics/drawable/Drawable;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundPressed:Landroid/graphics/drawable/Drawable;

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    .line 107
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    .line 108
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableNormalItem:Landroid/graphics/drawable/Drawable;

    .line 109
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    .line 110
    iput-boolean p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    .line 111
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Z)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "enable"    # Z

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    .line 25
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableNormalItem:Landroid/graphics/drawable/Drawable;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableDisableItem:Landroid/graphics/drawable/Drawable;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundNormal:Landroid/graphics/drawable/Drawable;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundPressed:Landroid/graphics/drawable/Drawable;

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    .line 85
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    .line 86
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    .line 87
    iput-boolean p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    .line 88
    return-void
.end method
