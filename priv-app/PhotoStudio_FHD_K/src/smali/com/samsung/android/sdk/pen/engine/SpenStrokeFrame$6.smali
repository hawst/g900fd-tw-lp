.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;
.super Ljava/lang/Object;
.source "SpenStrokeFrame.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startCamera()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    .line 806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete([B)V
    .locals 14
    .param p1, "arg0"    # [B

    .prologue
    const/high16 v13, 0x41200000    # 10.0f

    const/4 v9, 0x0

    const/4 v12, 0x0

    const/16 v11, 0x20

    .line 810
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$44(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 811
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$44(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 812
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$45(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V

    .line 814
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$46(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 815
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$46(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 816
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$47(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V

    .line 818
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 819
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 820
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$48(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V

    .line 824
    :cond_2
    if-nez p1, :cond_3

    .line 825
    const-string v8, "SpenStrokeFrame"

    const-string v9, "Camera onComplete arg0 is null"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v8, v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 906
    :goto_0
    return-void

    .line 830
    :cond_3
    array-length v8, p1

    invoke-static {p1, v12, v8, v9}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 831
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-nez v1, :cond_4

    .line 832
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    .line 833
    const-string v8, "SpenStrokeFrame"

    const-string v9, "Camera onComplete Bitmap is null"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v8, v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto :goto_0

    .line 837
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V

    .line 839
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createRotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    invoke-static {v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$50(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 842
    .local v7, "rotateBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 843
    .local v0, "absoluteRect":Landroid/graphics/RectF;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v10

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    .line 844
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v8

    invoke-virtual {v8, v0, v12}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 845
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 846
    .local v4, "relativePath":Landroid/graphics/Path;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v8, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v5

    .line 847
    .local v5, "relativeRect":Landroid/graphics/RectF;
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 848
    .local v3, "m":Landroid/graphics/Matrix;
    sget-object v8, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v3, v0, v5, v8}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 849
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 851
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createSizeFitableBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    invoke-static {v8, v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 852
    .local v2, "fitBitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_5

    .line 853
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    .line 854
    const-string v8, "SpenStrokeFrame"

    const-string v9, "Camera onComplete Bitmap is null"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v8, v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 858
    :cond_5
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createStrokeFrameBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;
    invoke-static {v8, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 859
    .local v6, "resultBitmap":Landroid/graphics/Bitmap;
    if-nez v6, :cond_6

    .line 860
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    .line 861
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v8, v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 864
    :cond_6
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 865
    const/4 v2, 0x0

    .line 866
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$53(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    move-result-object v8

    invoke-virtual {v8, v0, v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setRect(Landroid/graphics/RectF;Z)V

    .line 867
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$53(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setImage(Landroid/graphics/Bitmap;)V

    .line 868
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 869
    const/4 v6, 0x0

    .line 872
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v10

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    .line 873
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v8

    invoke-virtual {v8, v0, v12}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 874
    new-instance v4, Landroid/graphics/Path;

    .end local v4    # "relativePath":Landroid/graphics/Path;
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 875
    .restart local v4    # "relativePath":Landroid/graphics/Path;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v8, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v5

    .line 876
    new-instance v3, Landroid/graphics/Matrix;

    .end local v3    # "m":Landroid/graphics/Matrix;
    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 877
    .restart local v3    # "m":Landroid/graphics/Matrix;
    sget-object v8, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v3, v0, v5, v8}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 878
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 879
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createSizeFitableBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    invoke-static {v8, v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 880
    if-nez v2, :cond_7

    .line 881
    const-string v8, "SpenStrokeFrame"

    const-string v9, "Camera onComplete Bitmap is null"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v8, v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 885
    :cond_7
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 886
    const/4 v1, 0x0

    .line 887
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 888
    const/4 v7, 0x0

    .line 889
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createStrokeFrameBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;
    invoke-static {v8, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 890
    if-nez v6, :cond_8

    .line 891
    const-string v8, "SpenStrokeFrame"

    const-string v9, "Beautify Stroke Frame ResultBitmap is null"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 892
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v8, v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 895
    :cond_8
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 896
    const/4 v2, 0x0

    .line 897
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$54(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    move-result-object v8

    invoke-virtual {v8, v0, v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setRect(Landroid/graphics/RectF;Z)V

    .line 898
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$54(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setImage(Landroid/graphics/Bitmap;)V

    .line 899
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 900
    const/4 v6, 0x0

    .line 902
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v8

    invoke-virtual {v8, v13, v13}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setMinSize(FF)V

    .line 903
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$55(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v10

    invoke-interface {v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCompleted(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    .line 904
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$24(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/app/Activity;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOrientation:I
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$56(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 905
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    goto/16 :goto_0
.end method
