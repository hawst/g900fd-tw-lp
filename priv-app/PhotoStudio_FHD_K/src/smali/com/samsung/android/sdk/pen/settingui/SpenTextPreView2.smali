.class Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;
.super Landroid/view/View;
.source "SpenTextPreView2.java"


# static fields
.field protected static final TEXT_PREVIEW_WIDTH:I = 0x140


# instance fields
.field private isCheckUnderLine:Z

.field private mBold:I

.field private mColor:I

.field private mOnePoint:F

.field private final mPaint:Landroid/graphics/Paint;

.field private final mTextRect:Landroid/graphics/Rect;

.field private mTextSize:F

.field private mTextSkewValue:F

.field private mTypeFace:Landroid/graphics/Typeface;

.field private maxFontSize:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 22
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mColor:I

    .line 23
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSize:F

    .line 24
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mBold:I

    .line 25
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSkewValue:F

    .line 26
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->isCheckUnderLine:Z

    .line 27
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTypeFace:Landroid/graphics/Typeface;

    .line 28
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    .line 29
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    .line 33
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mOnePoint:F

    .line 34
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->maxFontSize:F

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 12
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "mCanvasWidth"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 22
    const/high16 v8, -0x1000000

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mColor:I

    .line 23
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSize:F

    .line 24
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mBold:I

    .line 25
    const/high16 v8, -0x80000000

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSkewValue:F

    .line 26
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->isCheckUnderLine:Z

    .line 27
    sget-object v8, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTypeFace:Landroid/graphics/Typeface;

    .line 28
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    .line 29
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    .line 33
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mOnePoint:F

    .line 34
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->maxFontSize:F

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 54
    .local v3, "localDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v8, v3, Landroid/util/DisplayMetrics;->density:F

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mOnePoint:F

    .line 55
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    .line 56
    .local v4, "mFontList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v7, "ABC abc"

    .line 57
    .local v7, "str":Ljava/lang/String;
    const-string v5, ""

    .line 58
    .local v5, "maxFontName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 59
    .local v6, "maxLength":F
    int-to-double v8, p2

    const-wide v10, 0x4076800000000000L    # 360.0

    div-double/2addr v8, v10

    double-to-float v0, v8

    .line 61
    .local v0, "fontPointPixel":F
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->getFontSizeList()Ljava/util/ArrayList;

    move-result-object v1

    .line 63
    .local v1, "fontSizeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v2, v8, :cond_0

    .line 75
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v2, v8, :cond_2

    .line 88
    :goto_2
    return-void

    .line 65
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    const/high16 v9, 0x41f00000    # 30.0f

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 66
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 67
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 68
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    const/4 v9, 0x0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v8, v7, v9, v10, v11}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 69
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    cmpl-float v8, v8, v6

    if-lez v8, :cond_1

    .line 70
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v6, v8

    .line 71
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "maxFontName":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 63
    .restart local v5    # "maxFontName":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 77
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v0

    invoke-virtual {v9, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 78
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 79
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-static {v5}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 80
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    const/4 v9, 0x0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v8, v7, v9, v10, v11}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 81
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    const/high16 v9, 0x43960000    # 300.0f

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mOnePoint:F

    mul-float/2addr v9, v10

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_3

    .line 82
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v0

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->maxFontSize:F

    goto/16 :goto_2

    .line 75
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1
.end method


# virtual methods
.method protected getFontSizeList()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v0, "fontList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v1, 0x8

    .local v1, "txtSize":I
    :goto_0
    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 97
    const/16 v1, 0x16

    :goto_1
    const/16 v2, 0x21

    if-lt v1, v2, :cond_1

    .line 100
    const/16 v1, 0x24

    :goto_2
    const/16 v2, 0x41

    if-lt v1, v2, :cond_2

    .line 104
    return-object v0

    .line 95
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 98
    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 101
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    add-int/lit8 v1, v1, 0x4

    goto :goto_2
.end method

.method public getPreviewTextColor()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mColor:I

    return v0
.end method

.method public getTextStyle()C
    .locals 3

    .prologue
    .line 233
    const/4 v0, 0x0

    .line 234
    .local v0, "nTextStyle":C
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mBold:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_0

    .line 235
    const/4 v1, 0x1

    int-to-char v0, v1

    .line 237
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSkewValue:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    .line 238
    or-int/lit8 v1, v0, 0x2

    int-to-char v0, v1

    .line 240
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->isCheckUnderLine:Z

    if-eqz v1, :cond_2

    .line 241
    or-int/lit8 v1, v0, 0x4

    int-to-char v0, v1

    .line 243
    :cond_2
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 118
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 120
    const-string v0, "ABC abc"

    .line 122
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 123
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 124
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSize:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 125
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 126
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTypeFace:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 127
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mBold:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFlags(I)V

    .line 128
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSkewValue:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 129
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->isCheckUnderLine:Z

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setUnderlineText(Z)V

    .line 130
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 133
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSize:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->maxFontSize:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 134
    const-string v0, "AB ab"

    .line 135
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 139
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 140
    return-void
.end method

.method public setPreviewBold(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 208
    if-eqz p1, :cond_0

    .line 209
    const/16 v0, 0x20

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mBold:I

    .line 213
    :goto_0
    return-void

    .line 211
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mBold:I

    goto :goto_0
.end method

.method public setPreviewTextColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 160
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mColor:I

    .line 161
    return-void
.end method

.method public setPreviewTextSize(F)V
    .locals 0
    .param p1, "textSize"    # F

    .prologue
    .line 182
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSize:F

    .line 183
    return-void
.end method

.method public setPreviewTextSkewX(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 193
    if-eqz p1, :cond_0

    .line 194
    const v0, -0x41666666    # -0.3f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSkewValue:F

    .line 198
    :goto_0
    return-void

    .line 196
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSkewValue:F

    goto :goto_0
.end method

.method public setPreviewTypeface(Landroid/graphics/Typeface;)V
    .locals 0
    .param p1, "typeface"    # Landroid/graphics/Typeface;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTypeFace:Landroid/graphics/Typeface;

    .line 224
    return-void
.end method

.method public setPreviewUnderLine(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 171
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->isCheckUnderLine:Z

    .line 172
    return-void
.end method
