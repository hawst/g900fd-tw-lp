.class public Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;
.super Ljava/lang/Object;
.source "SpenControlBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CTouchZone"
.end annotation


# instance fields
.field private final mTempTouchZone:Landroid/graphics/RectF;

.field private mZoneSize:F

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 574
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 575
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    .line 576
    const/high16 v1, 0x40e00000    # 7.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    .line 578
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 579
    const/high16 v1, 0x41400000    # 12.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    .line 582
    :cond_0
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mControlBaseContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 583
    .local v0, "dm":Landroid/util/DisplayMetrics;
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    invoke-static {v2, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    .line 584
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;)F
    .locals 1

    .prologue
    .line 572
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    return v0
.end method


# virtual methods
.method protected checkTouchPosition(Landroid/graphics/RectF;IILcom/samsung/android/sdk/pen/document/SpenObjectBase;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;)V
    .locals 6
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .param p5, "retState"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    .prologue
    const/4 v5, 0x1

    const v2, 0x38d1b717    # 1.0E-4f

    const/4 v4, 0x2

    .line 677
    invoke-virtual {p5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 679
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 702
    :cond_0
    :goto_0
    return-void

    .line 683
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 684
    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v1

    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 685
    if-nez v0, :cond_2

    invoke-virtual {p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 686
    :cond_2
    if-lt v0, v5, :cond_3

    const/16 v1, 0x8

    if-gt v0, v1, :cond_3

    .line 687
    invoke-virtual {p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v1

    if-eq v1, v4, :cond_4

    .line 688
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    if-eqz v1, :cond_5

    if-eq v0, v4, :cond_4

    .line 689
    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-ne v0, v1, :cond_5

    .line 683
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 693
    :cond_5
    iput v0, p5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    .line 694
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)I

    move-result v1

    if-eq v1, v4, :cond_6

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)I

    move-result v1

    if-ne v1, v5, :cond_0

    .line 695
    :cond_6
    iget v1, p5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v2, 0x9

    if-eq v1, v2, :cond_0

    .line 696
    const/4 v1, -0x1

    iput v1, p5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    goto :goto_0
.end method

.method getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 13
    .param p1, "touchZone"    # I
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 587
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    float-to-int v5, v6

    .line 588
    .local v5, "rect_w":I
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    float-to-int v4, v6

    .line 589
    .local v4, "rect_h":I
    const/4 v1, 0x0

    .line 590
    .local v1, "disableTopBottom":Z
    const/4 v0, 0x0

    .line 592
    .local v0, "disableLeftRight":Z
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    .line 593
    int-to-float v6, v5

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v8, 0x40800000    # 4.0f

    mul-float/2addr v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_0

    .line 594
    const/4 v1, 0x1

    .line 597
    :cond_0
    int-to-float v6, v4

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v8, 0x40800000    # 4.0f

    mul-float/2addr v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_1

    .line 598
    const/4 v0, 0x1

    .line 612
    :cond_1
    :goto_0
    const/high16 v6, 0x40000000    # 2.0f

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    mul-float v2, v6, v7

    .line 614
    .local v2, "outsideGap":F
    packed-switch p1, :pswitch_data_0

    .line 673
    :cond_2
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    return-object v6

    .line 601
    .end local v2    # "outsideGap":F
    :cond_3
    int-to-float v6, v5

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v8, 0x40c00000    # 6.0f

    mul-float/2addr v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_4

    .line 602
    const/4 v1, 0x1

    .line 605
    :cond_4
    int-to-float v6, v4

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v8, 0x40c00000    # 6.0f

    mul-float/2addr v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_1

    .line 606
    const/4 v0, 0x1

    goto :goto_0

    .line 616
    .restart local v2    # "outsideGap":F
    :pswitch_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v7, p2, Landroid/graphics/RectF;->left:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    sub-float/2addr v7, v8

    sub-float/2addr v7, v2

    iget v8, p2, Landroid/graphics/RectF;->top:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    sub-float/2addr v8, v9

    sub-float/2addr v8, v2

    iget v9, p2, Landroid/graphics/RectF;->left:F

    .line 617
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v11, 0x3fc00000    # 1.5f

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p2, Landroid/graphics/RectF;->top:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v12, 0x3fc00000    # 1.5f

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    .line 616
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_1

    .line 620
    :pswitch_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v9, 0x3fc00000    # 1.5f

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget v8, p2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v8, v2

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v9

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    .line 621
    const/high16 v11, 0x3fc00000    # 1.5f

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p2, Landroid/graphics/RectF;->top:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v12, 0x3fc00000    # 1.5f

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    .line 620
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 622
    if-eqz v1, :cond_2

    .line 623
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_1

    .line 628
    :pswitch_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v7, p2, Landroid/graphics/RectF;->right:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v9, 0x3fc00000    # 1.5f

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget v8, p2, Landroid/graphics/RectF;->top:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    sub-float/2addr v8, v9

    sub-float/2addr v8, v2

    iget v9, p2, Landroid/graphics/RectF;->right:F

    .line 629
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v9, v10

    add-float/2addr v9, v2

    iget v10, p2, Landroid/graphics/RectF;->top:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v12, 0x3fc00000    # 1.5f

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    .line 628
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    .line 632
    :pswitch_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v7, p2, Landroid/graphics/RectF;->left:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    sub-float/2addr v7, v8

    sub-float/2addr v7, v2

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v10, 0x3fc00000    # 1.5f

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    iget v9, p2, Landroid/graphics/RectF;->left:F

    .line 633
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v11, 0x3fc00000    # 1.5f

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v10

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v12, 0x3fc00000    # 1.5f

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    .line 632
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 634
    if-eqz v0, :cond_2

    .line 635
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    .line 639
    :pswitch_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v7, p2, Landroid/graphics/RectF;->left:F

    iget v8, p2, Landroid/graphics/RectF;->top:F

    iget v9, p2, Landroid/graphics/RectF;->right:F

    iget v10, p2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    .line 642
    :pswitch_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v7, p2, Landroid/graphics/RectF;->right:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v9, 0x3fc00000    # 1.5f

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v10, 0x3fc00000    # 1.5f

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    iget v9, p2, Landroid/graphics/RectF;->right:F

    .line 643
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v9, v10

    add-float/2addr v9, v2

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v10

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v12, 0x3fc00000    # 1.5f

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    .line 642
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 644
    if-eqz v0, :cond_2

    .line 645
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    .line 649
    :pswitch_6
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v7, p2, Landroid/graphics/RectF;->left:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    sub-float/2addr v7, v8

    sub-float/2addr v7, v2

    iget v8, p2, Landroid/graphics/RectF;->bottom:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v10, 0x3fc00000    # 1.5f

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    iget v9, p2, Landroid/graphics/RectF;->left:F

    .line 650
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v11, 0x3fc00000    # 1.5f

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p2, Landroid/graphics/RectF;->bottom:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v10, v11

    add-float/2addr v10, v2

    .line 649
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    .line 653
    :pswitch_7
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v9, 0x3fc00000    # 1.5f

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget v8, p2, Landroid/graphics/RectF;->bottom:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v10, 0x3fc00000    # 1.5f

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v9

    .line 654
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v11, 0x3fc00000    # 1.5f

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p2, Landroid/graphics/RectF;->bottom:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v10, v11

    add-float/2addr v10, v2

    .line 653
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 655
    if-eqz v1, :cond_2

    .line 656
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    .line 661
    :pswitch_8
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v7, p2, Landroid/graphics/RectF;->right:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v9, 0x3fc00000    # 1.5f

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget v8, p2, Landroid/graphics/RectF;->bottom:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v10, 0x3fc00000    # 1.5f

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    iget v9, p2, Landroid/graphics/RectF;->right:F

    .line 662
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v9, v10

    add-float/2addr v9, v2

    iget v10, p2, Landroid/graphics/RectF;->bottom:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v10, v11

    add-float/2addr v10, v2

    .line 661
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    .line 665
    :pswitch_9
    const/high16 v3, 0x3fc00000    # 1.5f

    .line 666
    .local v3, "ratio":F
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v7, p2, Landroid/graphics/RectF;->left:F

    iget v8, p2, Landroid/graphics/RectF;->right:F

    iget v9, p2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    mul-float/2addr v8, v3

    sub-float/2addr v7, v8

    iget v8, p2, Landroid/graphics/RectF;->top:F

    .line 667
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v10, 0x40800000    # 4.0f

    mul-float/2addr v9, v10

    mul-float/2addr v9, v3

    sub-float/2addr v8, v9

    const/high16 v9, 0x41f00000    # 30.0f

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v8, v9

    iget v9, p2, Landroid/graphics/RectF;->left:F

    iget v10, p2, Landroid/graphics/RectF;->right:F

    iget v11, p2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    add-float/2addr v9, v10

    .line 668
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    mul-float/2addr v10, v3

    add-float/2addr v9, v10

    iget v10, p2, Landroid/graphics/RectF;->top:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    mul-float/2addr v11, v3

    sub-float/2addr v10, v11

    const/high16 v11, 0x41f00000    # 30.0f

    sub-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v10, v11

    .line 666
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    .line 614
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_4
    .end packed-switch
.end method
