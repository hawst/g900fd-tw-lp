.class Lcom/samsung/android/sdk/pen/engine/SpenToolTip;
.super Ljava/lang/Object;
.source "SpenToolTip.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEraserBitmap:Landroid/graphics/Bitmap;

.field private mEraserPaint:Landroid/graphics/Paint;

.field private mPenBitmap:Landroid/graphics/Bitmap;

.field private mPenImageMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private mPenList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

.field private mPenPaint:Landroid/graphics/Paint;

.field private mPoints:[Landroid/graphics/PointF;

.field private mPressures:[F

.field private mSdkResources:Landroid/content/res/Resources;

.field private mSpoidBitmap:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/high16 v6, 0x43160000    # 150.0f

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    .line 32
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    .line 33
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    .line 34
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    .line 35
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    .line 43
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mContext:Landroid/content/Context;

    .line 45
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 46
    .local v1, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    .end local v1    # "manager":Landroid/content/pm/PackageManager;
    :goto_0
    new-instance v2, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-direct {v2, p1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    .line 53
    const/4 v2, 0x3

    new-array v2, v2, [Landroid/graphics/PointF;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    .line 54
    const/4 v2, 0x3

    new-array v2, v2, [F

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    .line 55
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    .line 56
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 57
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    .line 58
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 59
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40800000    # 4.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 60
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 61
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    const/16 v3, 0xff

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 62
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 64
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    new-instance v3, Landroid/graphics/PointF;

    const/4 v4, 0x0

    invoke-direct {v3, v4, v6}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v3, v2, v7

    .line 65
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    new-instance v3, Landroid/graphics/PointF;

    const/high16 v4, 0x42480000    # 50.0f

    invoke-direct {v3, v4, v6}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v3, v2, v5

    .line 66
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    new-instance v3, Landroid/graphics/PointF;

    const/high16 v4, 0x42c80000    # 100.0f

    invoke-direct {v3, v4, v6}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v3, v2, v8

    .line 67
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v2, v7

    .line 68
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const v3, 0x3f4ccccd    # 0.8f

    aput v3, v2, v5

    .line 69
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const v3, 0x3f19999a    # 0.6f

    aput v3, v2, v8

    .line 70
    return-void

    .line 47
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 73
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 75
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 80
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 83
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 84
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    .line 86
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 87
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 88
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    .line 90
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-eqz v0, :cond_4

    .line 91
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->close()V

    .line 92
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    .line 94
    :cond_4
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mContext:Landroid/content/Context;

    .line 96
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    if-eqz v0, :cond_5

    .line 97
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->flushLayoutCache()V

    .line 98
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    .line 100
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    if-eqz v0, :cond_6

    .line 101
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    .line 102
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v1, 0x1

    aput-object v2, v0, v1

    .line 103
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v1, 0x2

    aput-object v2, v0, v1

    .line 104
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    .line 106
    :cond_6
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    .line 107
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    .line 108
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    .line 109
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    if-eqz v0, :cond_7

    .line 110
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 111
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    .line 113
    :cond_7
    return-void
.end method

.method public getDrawableEraserImage(F)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "size"    # F

    .prologue
    const/16 v2, 0xcc

    const/high16 v3, 0x42cc0000    # 102.0f

    .line 266
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 267
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    .line 270
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 271
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 272
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v3, p1, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 273
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v1
.end method

.method public getDrawableEraserImage(FFF)Landroid/graphics/drawable/Drawable;
    .locals 8
    .param p1, "size"    # F
    .param p2, "rx"    # F
    .param p3, "ry"    # F

    .prologue
    const/16 v5, 0xcc

    const/high16 v7, 0x42cc0000    # 102.0f

    .line 277
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    if-nez v4, :cond_0

    .line 278
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v5, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    .line 282
    :cond_0
    mul-float v2, p1, p2

    .line 283
    .local v2, "xr":F
    mul-float v3, p1, p3

    .line 284
    .local v3, "yr":F
    new-instance v1, Landroid/graphics/RectF;

    sub-float v4, v7, v2

    sub-float v5, v7, v3

    add-float v6, v7, v2

    add-float/2addr v7, v3

    invoke-direct {v1, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 286
    .local v1, "ovalRect":Landroid/graphics/RectF;
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 287
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v4, 0x0

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 288
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 290
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v4
.end method

.method public getDrawableHoverImage()Landroid/graphics/drawable/Drawable;
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/16 v11, 0x64

    const/16 v10, 0x39

    const/4 v5, 0x0

    .line 322
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    const-string v7, "snote_toolbar_icon_spoid_hover"

    const-string v8, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 323
    .local v4, "mDrawableResID":I
    if-nez v4, :cond_1

    .line 346
    :cond_0
    :goto_0
    return-object v5

    .line 326
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {p0, v6, v4, v10, v10}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 327
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    .line 331
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    if-nez v6, :cond_2

    .line 332
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v11, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    .line 334
    :cond_2
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 335
    .local v3, "iconBitmap":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_0

    .line 339
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 340
    .local v0, "canvas":Landroid/graphics/Canvas;
    sget-object v6, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v12, v6}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 341
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 342
    .local v2, "dstRect":Landroid/graphics/Rect;
    const/16 v6, 0x2b

    invoke-virtual {v2, v6, v12, v11, v10}, Landroid/graphics/Rect;->set(IIII)V

    .line 343
    invoke-virtual {v0, v3, v5, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 346
    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v5, v6, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public getDrawableImage(Ljava/lang/String;IF)Landroid/graphics/drawable/Drawable;
    .locals 35
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "color"    # I
    .param p3, "size"    # F

    .prologue
    .line 116
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    if-nez v8, :cond_0

    .line 117
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->getPenInfoList()Ljava/util/List;

    move-result-object v8

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    .line 118
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    if-nez v8, :cond_0

    .line 119
    const/4 v8, 0x0

    .line 262
    :goto_0
    return-object v8

    .line 122
    :cond_0
    const/16 v33, 0x0

    .local v33, "pos":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    move/from16 v0, v33

    if-lt v0, v8, :cond_1

    .line 262
    const/4 v8, 0x0

    goto :goto_0

    .line 123
    :cond_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    move/from16 v0, v33

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    .line 124
    .local v28, "info":Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_16

    .line 125
    const/16 v24, 0x0

    .line 126
    .local v24, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    if-eqz v8, :cond_2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 127
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    .end local v24    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    check-cast v24, Landroid/graphics/drawable/BitmapDrawable;

    .line 129
    .restart local v24    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_2
    if-nez v24, :cond_3

    .line 130
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->iconImageUri:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v24

    .end local v24    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    check-cast v24, Landroid/graphics/drawable/BitmapDrawable;

    .line 132
    .restart local v24    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_3
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    if-nez v8, :cond_4

    .line 133
    const/16 v8, 0x64

    const/16 v9, 0x12c

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    .line 136
    :cond_4
    new-instance v25, Landroid/graphics/Rect;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Rect;-><init>()V

    .line 137
    .local v25, "dst":Landroid/graphics/Rect;
    new-instance v22, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, v22

    invoke-direct {v0, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 138
    .local v22, "canvas":Landroid/graphics/Canvas;
    const/4 v8, 0x0

    sget-object v9, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 140
    :try_start_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->createPen(Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;)Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v31

    .line 141
    .local v31, "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_9

    .line 143
    const-string v8, "snote_popup_pensetting_preview_alpha"

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v30

    check-cast v30, Landroid/graphics/drawable/BitmapDrawable;

    .line 144
    .local v30, "magic":Landroid/graphics/drawable/BitmapDrawable;
    const/4 v8, 0x0

    const/16 v9, 0x96

    const/16 v10, 0x64

    const/high16 v11, 0x43160000    # 150.0f

    add-float v11, v11, p3

    float-to-int v11, v11

    move-object/from16 v0, v25

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 145
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    shr-int/lit8 v9, p2, 0x18

    and-int/lit16 v9, v9, 0xff

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 147
    if-eqz v30, :cond_5

    .line 148
    invoke-virtual/range {v30 .. v30}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v8, v9, v1, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 150
    :cond_5
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    const/16 v9, 0xff

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 151
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 220
    .end local v30    # "magic":Landroid/graphics/drawable/BitmapDrawable;
    :goto_2
    const/4 v8, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 221
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    move-object/from16 v0, v31

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 232
    .end local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :goto_3
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x64

    const/16 v11, 0xfa

    move-object/from16 v0, v25

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 233
    if-eqz v24, :cond_8

    .line 234
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_6

    .line 235
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.MontblancFountainPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_d

    .line 236
    :cond_6
    const/high16 v8, -0x3e500000    # -22.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 256
    :cond_7
    :goto_4
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v8, v9, v1, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 259
    :cond_8
    new-instance v8, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v8, v9, v10}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 152
    .restart local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :cond_9
    :try_start_1
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_a

    .line 153
    const/16 v8, 0x12c

    const/16 v9, 0x12c

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v29

    .line 154
    .local v29, "mOblicquePenBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, v31

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setColor(I)V

    .line 155
    const/high16 v8, 0x40400000    # 3.0f

    mul-float v8, v8, p3

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V

    .line 156
    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 157
    const/4 v8, 0x3

    new-array v0, v8, [Landroid/graphics/PointF;

    move-object/from16 v32, v0

    .line 158
    .local v32, "points":[Landroid/graphics/PointF;
    const/4 v8, 0x0

    new-instance v9, Landroid/graphics/PointF;

    const/high16 v10, 0x42700000    # 60.0f

    const/high16 v11, 0x43160000    # 150.0f

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v9, v32, v8

    .line 159
    const/4 v8, 0x1

    new-instance v9, Landroid/graphics/PointF;

    const/high16 v10, 0x43160000    # 150.0f

    const/high16 v11, 0x43160000    # 150.0f

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v9, v32, v8

    .line 160
    const/4 v8, 0x2

    new-instance v9, Landroid/graphics/PointF;

    const/high16 v10, 0x43730000    # 243.0f

    const/high16 v11, 0x43160000    # 150.0f

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v9, v32, v8

    .line 162
    new-instance v34, Landroid/graphics/RectF;

    invoke-direct/range {v34 .. v34}, Landroid/graphics/RectF;-><init>()V

    .line 163
    .local v34, "rect":Landroid/graphics/RectF;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 165
    .local v4, "time":J
    const/4 v8, 0x0

    const/4 v9, 0x0

    aget-object v9, v32, v9

    iget v9, v9, Landroid/graphics/PointF;->x:F

    .line 166
    const/4 v10, 0x0

    aget-object v10, v32, v10

    iget v10, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v12, 0x0

    aget v11, v11, v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-wide v6, v4

    move/from16 v12, p3

    .line 165
    invoke-static/range {v4 .. v17}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v27

    .line 167
    .local v27, "event":Landroid/view/MotionEvent;
    move-object/from16 v0, v31

    move-object/from16 v1, v27

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 168
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V

    .line 171
    const-wide/16 v8, 0x5

    add-long v6, v4, v8

    .line 172
    .local v6, "currentTime":J
    const/4 v8, 0x2

    .line 173
    const/4 v9, 0x1

    aget-object v9, v32, v9

    iget v9, v9, Landroid/graphics/PointF;->x:F

    const/4 v10, 0x1

    aget-object v10, v32, v10

    iget v10, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v12, 0x1

    aget v11, v11, v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v12, p3

    .line 172
    invoke-static/range {v4 .. v17}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v27

    .line 174
    move-object/from16 v0, v31

    move-object/from16 v1, v27

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 175
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V

    .line 178
    const-wide/16 v8, 0xa

    add-long v6, v4, v8

    .line 179
    const/4 v8, 0x1

    .line 180
    const/4 v9, 0x2

    aget-object v9, v32, v9

    iget v9, v9, Landroid/graphics/PointF;->x:F

    const/4 v10, 0x2

    aget-object v10, v32, v10

    iget v10, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v12, 0x2

    aget v11, v11, v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v12, p3

    .line 179
    invoke-static/range {v4 .. v17}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v27

    .line 181
    move-object/from16 v0, v31

    move-object/from16 v1, v27

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 182
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V

    .line 185
    new-instance v23, Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x64

    const/16 v11, 0x12c

    move-object/from16 v0, v23

    invoke-direct {v0, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 186
    .local v23, "dest":Landroid/graphics/Rect;
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v8, v2, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 187
    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->recycle()V

    .line 188
    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x2

    const/4 v11, 0x0

    aput-object v11, v32, v10

    aput-object v11, v32, v9

    aput-object v11, v32, v8
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_2

    .line 222
    .end local v4    # "time":J
    .end local v6    # "currentTime":J
    .end local v23    # "dest":Landroid/graphics/Rect;
    .end local v27    # "event":Landroid/view/MotionEvent;
    .end local v29    # "mOblicquePenBitmap":Landroid/graphics/Bitmap;
    .end local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    .end local v32    # "points":[Landroid/graphics/PointF;
    .end local v34    # "rect":Landroid/graphics/RectF;
    :catch_0
    move-exception v26

    .line 223
    .local v26, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto/16 :goto_3

    .line 191
    .end local v26    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :cond_a
    :try_start_2
    move-object/from16 v0, v31

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setColor(I)V

    .line 192
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_c

    .line 193
    move/from16 v0, p3

    float-to-double v8, v0

    const-wide/high16 v10, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v8, v10

    double-to-float v0, v8

    move/from16 p3, v0

    .line 197
    :cond_b
    :goto_5
    const/high16 v8, 0x40400000    # 3.0f

    mul-float v8, v8, p3

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V

    .line 198
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 200
    new-instance v34, Landroid/graphics/RectF;

    invoke-direct/range {v34 .. v34}, Landroid/graphics/RectF;-><init>()V

    .line 201
    .restart local v34    # "rect":Landroid/graphics/RectF;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 203
    .restart local v4    # "time":J
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    iget v13, v8, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    iget v14, v8, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v9, 0x0

    aget v15, v8, v9

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-wide v8, v4

    move-wide v10, v4

    move/from16 v16, p3

    invoke-static/range {v8 .. v21}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v27

    .line 204
    .restart local v27    # "event":Landroid/view/MotionEvent;
    move-object/from16 v0, v31

    move-object/from16 v1, v27

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 205
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V

    .line 208
    const-wide/16 v8, 0x5

    add-long v6, v4, v8

    .line 209
    .restart local v6    # "currentTime":J
    const/4 v8, 0x2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v10, 0x1

    aget-object v9, v9, v10

    iget v9, v9, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v11, 0x1

    aget-object v10, v10, v11

    iget v10, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v12, 0x1

    aget v11, v11, v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v12, p3

    invoke-static/range {v4 .. v17}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v27

    .line 210
    move-object/from16 v0, v31

    move-object/from16 v1, v27

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 211
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V

    .line 214
    const-wide/16 v8, 0xa

    add-long v6, v4, v8

    .line 215
    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v10, 0x2

    aget-object v9, v9, v10

    iget v9, v9, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v11, 0x2

    aget-object v10, v10, v11

    iget v10, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v12, 0x2

    aget v11, v11, v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v12, p3

    invoke-static/range {v4 .. v17}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v27

    .line 216
    move-object/from16 v0, v31

    move-object/from16 v1, v27

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 217
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_2

    .line 224
    .end local v4    # "time":J
    .end local v6    # "currentTime":J
    .end local v27    # "event":Landroid/view/MotionEvent;
    .end local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    .end local v34    # "rect":Landroid/graphics/RectF;
    :catch_1
    move-exception v26

    .line 225
    .local v26, "e":Ljava/lang/InstantiationException;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_3

    .line 194
    .end local v26    # "e":Ljava/lang/InstantiationException;
    .restart local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :cond_c
    :try_start_3
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result v8

    if-nez v8, :cond_b

    .line 195
    move/from16 v0, p3

    float-to-double v8, v0

    const-wide v10, 0x3ff3333333333333L    # 1.2

    mul-double/2addr v8, v10

    double-to-float v0, v8

    move/from16 p3, v0

    goto/16 :goto_5

    .line 226
    .end local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :catch_2
    move-exception v26

    .line 227
    .local v26, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_3

    .line 228
    .end local v26    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v26

    .line 229
    .local v26, "e":Ljava/lang/Exception;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 237
    .end local v26    # "e":Ljava/lang/Exception;
    :cond_d
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_e

    .line 238
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    .line 239
    const-string v9, "com.samsung.android.sdk.pen.pen.preload.MontblancCalligraphyPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    .line 238
    if-nez v8, :cond_f

    .line 240
    :cond_e
    const/high16 v8, -0x3eb00000    # -13.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_4

    .line 241
    :cond_f
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_10

    .line 242
    const/high16 v8, -0x3e600000    # -20.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_4

    .line 243
    :cond_10
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_11

    .line 244
    const/high16 v8, -0x3e700000    # -18.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_4

    .line 245
    :cond_11
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_12

    .line 246
    const/high16 v8, -0x3e900000    # -15.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_4

    .line 247
    :cond_12
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_13

    .line 248
    const/high16 v8, -0x3e500000    # -22.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_4

    .line 249
    :cond_13
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_14

    .line 250
    const/high16 v8, -0x3e900000    # -15.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_4

    .line 251
    :cond_14
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_15

    .line 252
    const/high16 v8, -0x3e500000    # -22.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_4

    .line 253
    :cond_15
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_7

    .line 254
    const/high16 v8, -0x3e600000    # -20.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_4

    .line 122
    .end local v22    # "canvas":Landroid/graphics/Canvas;
    .end local v24    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v25    # "dst":Landroid/graphics/Rect;
    :cond_16
    add-int/lit8 v33, v33, 0x1

    goto/16 :goto_1
.end method

.method public getDrawableRemoverImage(FFF)Landroid/graphics/drawable/Drawable;
    .locals 8
    .param p1, "size"    # F
    .param p2, "rx"    # F
    .param p3, "ry"    # F

    .prologue
    const/16 v5, 0xcc

    const/high16 v7, 0x42cc0000    # 102.0f

    .line 305
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    if-nez v4, :cond_0

    .line 306
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v5, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    .line 310
    :cond_0
    mul-float v2, p1, p2

    .line 311
    .local v2, "xr":F
    mul-float v3, p1, p3

    .line 312
    .local v3, "yr":F
    new-instance v1, Landroid/graphics/RectF;

    sub-float v4, v7, v2

    sub-float v5, v7, v3

    add-float v6, v7, v2

    add-float/2addr v7, v3

    invoke-direct {v1, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 314
    .local v1, "ovalRect":Landroid/graphics/RectF;
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 315
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v4, 0x0

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 316
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 318
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v4
.end method

.method public getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "size"    # I

    .prologue
    const/16 v2, 0xcc

    const/high16 v3, 0x42cc0000    # 102.0f

    .line 294
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 295
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    .line 298
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 299
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 300
    int-to-float v1, p1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 301
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v1
.end method

.method public resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;
    .locals 13
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I
    .param p3, "iconWidth"    # I
    .param p4, "iconHeight"    # I

    .prologue
    .line 360
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v12

    .line 361
    .local v12, "stream":Ljava/io/InputStream;
    invoke-static {v12}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 363
    .local v0, "BitmapOrg":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 364
    const/4 v1, 0x0

    .line 376
    :goto_0
    return-object v1

    .line 366
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 367
    .local v3, "width":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 368
    .local v4, "height":I
    move/from16 v8, p3

    .line 369
    .local v8, "newWidth":I
    move/from16 v7, p4

    .line 371
    .local v7, "newHeight":I
    int-to-float v1, v8

    int-to-float v2, v3

    div-float v11, v1, v2

    .line 372
    .local v11, "scaleWidth":F
    int-to-float v1, v7

    int-to-float v2, v4

    div-float v10, v1, v2

    .line 373
    .local v10, "scaleHeight":F
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 374
    .local v5, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v5, v11, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 375
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 376
    .local v9, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, p1, v9}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "drawableName"    # Ljava/lang/String;

    .prologue
    .line 350
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    const-string v2, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 351
    .local v0, "mDrawableResID":I
    if-nez v0, :cond_0

    .line 352
    const/4 v1, 0x0

    .line 354
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0
.end method

.method public setPenTooltipImage(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "penName"    # Ljava/lang/String;
    .param p2, "image"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 380
    if-nez p1, :cond_0

    .line 387
    :goto_0
    return-void

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 384
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    .line 386
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
