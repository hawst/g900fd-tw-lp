.class Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;
.super Ljava/lang/Object;
.source "SpenColorPickerLayout2.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    .line 601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 606
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Landroid/widget/RelativeLayout;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 608
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v10

    float-to-int v8, v10

    .line 609
    .local v8, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v10

    float-to-int v9, v10

    .line 611
    .local v9, "y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 677
    :goto_0
    :pswitch_0
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    check-cast v10, Landroid/view/ViewGroup;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 678
    const/4 v10, 0x1

    return v10

    .line 613
    :pswitch_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    .line 614
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 613
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 615
    .local v5, "spoidSettingParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    sub-int v11, v8, v11

    invoke-static {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;I)V

    .line 616
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    sub-int v11, v9, v11

    invoke-static {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;I)V

    goto :goto_0

    .line 621
    .end local v5    # "spoidSettingParams":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_2
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    .line 622
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 621
    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 623
    .local v6, "spoidSettinghandleParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    .line 624
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 623
    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 625
    .local v4, "spoidSettingBgParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    .line 626
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 625
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 627
    .local v2, "spoidColorImageParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    .line 628
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 627
    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 629
    .local v3, "spoidExitBtnParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    .line 630
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    .line 629
    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 632
    .local v7, "spuitCurrentColorParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mXDelta:I
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)I

    move-result v10

    sub-int v10, v8, v10

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 633
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mYDelta:I
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)I

    move-result v10

    sub-int v10, v9, v10

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 635
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v10, :cond_0

    .line 636
    const/4 v10, 0x0

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 638
    :cond_0
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v10, :cond_1

    .line 639
    const/4 v10, 0x0

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 642
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v10

    const/high16 v11, 0x42d40000    # 106.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 643
    .local v1, "minWidth":I
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v10

    const/high16 v11, 0x42180000    # 38.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 645
    .local v0, "minHeight":I
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v11

    sub-int/2addr v11, v1

    if-le v10, v11, :cond_2

    .line 646
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Landroid/widget/RelativeLayout;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v10

    sub-int/2addr v10, v1

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 648
    :cond_2
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v11

    sub-int/2addr v11, v0

    if-le v10, v11, :cond_3

    .line 649
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Landroid/widget/RelativeLayout;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v10

    sub-int/2addr v10, v0

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 652
    :cond_3
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 653
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x41b00000    # 22.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    add-int/2addr v10, v11

    .line 652
    iput v10, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 654
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    add-int/lit8 v10, v10, 0x0

    iput v10, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 656
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 657
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x41c80000    # 25.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    add-int/2addr v10, v11

    .line 656
    iput v10, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 658
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 659
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x40e00000    # 7.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    add-int/2addr v10, v11

    .line 658
    iput v10, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 661
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 662
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x42480000    # 50.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    add-int/2addr v10, v11

    .line 661
    iput v10, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 663
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 664
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x40e00000    # 7.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    add-int/2addr v10, v11

    .line 663
    iput v10, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 666
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 667
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x429c0000    # 78.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    add-int/2addr v10, v11

    .line 666
    iput v10, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 668
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v10, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 670
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v10, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 671
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v10, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 672
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v10, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 673
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v10, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 674
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v10, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 611
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
