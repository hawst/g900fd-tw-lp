.class Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;
.super Landroid/view/View;
.source "SpenControlBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MovingView"
.end annotation


# instance fields
.field private mAngle:F

.field private final mMovingPaint:Landroid/graphics/Paint;

.field private final mMovingRect:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 4607
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 4608
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mMovingRect:Landroid/graphics/RectF;

    .line 4609
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mMovingPaint:Landroid/graphics/Paint;

    .line 4610
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mAngle:F

    .line 4611
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 4615
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mMovingRect:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mMovingPaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 4616
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 4617
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mAngle:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mMovingRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mMovingRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 4618
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mMovingRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mMovingPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 4619
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 4621
    :cond_0
    return-void
.end method

.method protected setMovingPaint(Landroid/graphics/Paint;)V
    .locals 2
    .param p1, "movingPaint"    # Landroid/graphics/Paint;

    .prologue
    .line 4632
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mMovingPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 4633
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mMovingPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 4634
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mMovingPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 4635
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mMovingPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4636
    return-void
.end method

.method protected setRect(Landroid/graphics/RectF;)V
    .locals 5
    .param p1, "movingRect"    # Landroid/graphics/RectF;

    .prologue
    .line 4624
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mMovingRect:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget v2, p1, Landroid/graphics/RectF;->top:F

    iget v3, p1, Landroid/graphics/RectF;->right:F

    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 4625
    return-void
.end method

.method protected setRotatedAngle(F)V
    .locals 0
    .param p1, "angle"    # F

    .prologue
    .line 4628
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$MovingView;->mAngle:F

    .line 4629
    return-void
.end method
