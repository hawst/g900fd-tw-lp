.class public final Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;
.super Ljava/lang/Object;
.source "SpenSignatureVerificationManager.java"


# static fields
.field public static final SPEN_SIGNATURE:Ljava/lang/String; = "com.samsung.android.sdk.pen.recognition.preload.Signature"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPluginList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mContext:Landroid/content/Context;

    .line 24
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 25
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    .line 40
    if-nez p1, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_ARG : parameter \'context\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 45
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mContext:Landroid/content/Context;

    .line 46
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 353
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mContext:Landroid/content/Context;

    .line 354
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 355
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 356
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 359
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    .line 361
    :cond_1
    return-void
.end method

.method public createSignatureVerification(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;)Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;
    .locals 7
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 151
    if-nez p1, :cond_0

    .line 152
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "E_INVALID_ARG : parameter \'info\' is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 155
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    if-nez v2, :cond_1

    .line 156
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "E_INVALID_STATE : candidateList is not made yet"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 159
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 172
    new-instance v2, Ljava/lang/ClassNotFoundException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Can not find "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Signature Verification"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 159
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 160
    .local v1, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v3, p1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->className:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 161
    iget-object v3, p1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->name:Ljava/lang/String;

    iget-object v4, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->version:I

    iget v4, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    if-ne v3, v4, :cond_2

    .line 163
    :try_start_0
    new-instance v3, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mContext:Landroid/content/Context;

    .line 164
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 165
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mContext:Landroid/content/Context;

    const-string v6, ""

    invoke-virtual {v2, v5, v1, v6}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->loadPlugin(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 164
    check-cast v2, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    .line 163
    invoke-direct {v3, v4, v2}, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;)V
    :try_end_0
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Ljava/lang/reflect/UndeclaredThrowableException;
    new-instance v2, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string v3, "SignatureRecognizer is not loaded"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public createSignatureVerification(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;
    .locals 6
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 202
    if-nez p1, :cond_0

    .line 203
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "E_INVALID_ARG : parameter \'info\' is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 206
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    if-nez v2, :cond_1

    .line 207
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "E_INVALID_STATE : candidateList is not made yet"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 210
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 223
    new-instance v2, Ljava/lang/ClassNotFoundException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Can not find "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Signature Verification"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 210
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 211
    .local v1, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v3, p1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->className:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 212
    iget-object v3, p1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->name:Ljava/lang/String;

    iget-object v4, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->version:I

    iget v4, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    if-ne v3, v4, :cond_2

    .line 214
    :try_start_0
    new-instance v3, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mContext:Landroid/content/Context;

    .line 215
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5, v1, p2}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->loadPlugin(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    .line 214
    invoke-direct {v3, v4, v2}, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;)V
    :try_end_0
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 217
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/reflect/UndeclaredThrowableException;
    new-instance v2, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string v3, "SignatureRecognizer is not loaded"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public createSignatureVerification(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;
    .locals 8
    .param p1, "className"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 249
    if-nez p1, :cond_0

    .line 250
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "E_INVALID_ARG : parameter \'className\' is null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 253
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    const-string v4, "SignatureVerification"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getPluginList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 254
    .local v2, "tempList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    if-nez v2, :cond_1

    .line 255
    new-instance v3, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string v4, "There is no available SpenSignatureVerification engine"

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 258
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 259
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 262
    :cond_2
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    .line 264
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_4

    .line 274
    new-instance v3, Ljava/lang/ClassNotFoundException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Can not find "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Signature Verification"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/ClassNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 264
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 265
    .local v1, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 267
    :try_start_0
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mContext:Landroid/content/Context;

    .line 268
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mContext:Landroid/content/Context;

    const-string v7, ""

    invoke-virtual {v3, v6, v1, v7}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->loadPlugin(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    .line 267
    invoke-direct {v4, v5, v3}, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;)V
    :try_end_0
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v4

    .line 269
    :catch_0
    move-exception v0

    .line 270
    .local v0, "e":Ljava/lang/reflect/UndeclaredThrowableException;
    new-instance v3, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string v4, "SignatureRecognizer is not loaded"

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public createSignatureVerification(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;
    .locals 7
    .param p1, "className"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 302
    if-nez p1, :cond_0

    .line 303
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "E_INVALID_ARG : parameter \'packageName\' or \'className\' is null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 306
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    const-string v4, "SignatureVerification"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getPluginList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 307
    .local v2, "tempList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    if-nez v2, :cond_1

    .line 308
    new-instance v3, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string v4, "There is no available SpenSignatureVerification engine"

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 311
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 312
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 315
    :cond_2
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    .line 317
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_4

    .line 327
    new-instance v3, Ljava/lang/ClassNotFoundException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Can not find "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Signature Verification"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/ClassNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 317
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 318
    .local v1, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 320
    :try_start_0
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mContext:Landroid/content/Context;

    .line 321
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v6, v1, p2}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->loadPlugin(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    .line 320
    invoke-direct {v4, v5, v3}, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;)V
    :try_end_0
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v4

    .line 322
    :catch_0
    move-exception v0

    .line 323
    .local v0, "e":Ljava/lang/reflect/UndeclaredThrowableException;
    new-instance v3, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string v4, "SignatureRecognizer is not loaded"

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public destroySignatureVerification(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;)V
    .locals 2
    .param p1, "verification"    # Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;

    .prologue
    .line 341
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->getPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    move-result-object v0

    if-nez v0, :cond_1

    .line 342
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_STATE : parameter \'verification\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->getPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->unloadPlugin(Ljava/lang/Object;)V

    .line 346
    return-void
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 365
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 367
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mContext:Landroid/content/Context;

    .line 368
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 369
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 370
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 373
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    .line 375
    :cond_1
    return-void
.end method

.method public getInfoList()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-nez v4, :cond_0

    .line 57
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "E_INVALID_STATE: Recognition Manager is null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 59
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    const-string v5, "SignatureVerification"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getPluginList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 60
    .local v3, "tempList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    if-nez v3, :cond_1

    .line 61
    const/4 v1, 0x0

    .line 80
    :goto_0
    return-object v1

    .line 64
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v1, "outputList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 78
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    goto :goto_0

    .line 66
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 67
    .local v0, "info":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    new-instance v2, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;-><init>()V

    .line 69
    .local v2, "signatureInfo":Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;
    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    iput-object v5, v2, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->name:Ljava/lang/String;

    .line 70
    iget v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    iput v5, v2, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->version:I

    .line 71
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->className:Ljava/lang/String;

    .line 72
    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    iput-object v5, v2, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->iconImageUri:Ljava/lang/String;

    .line 73
    iget-boolean v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    iput-boolean v5, v2, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->hasPrivateKey:Z

    .line 75
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getPrivateKeyHint(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;)Ljava/lang/String;
    .locals 5
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .prologue
    .line 108
    if-nez p1, :cond_0

    .line 109
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "E_INVALID_ARG : parameter \'info\' is null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    if-nez v1, :cond_1

    .line 113
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "E_INVALID_STATE : candidateList is not made yet"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 116
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 123
    new-instance v1, Ljava/lang/ClassNotFoundException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can not find "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Signature Verification"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 116
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 117
    .local v0, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v2, p1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->className:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 118
    iget-object v2, p1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->name:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationInfo;->version:I

    iget v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    if-ne v2, v3, :cond_2

    .line 119
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerificationManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getPrivateKeyHint(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
