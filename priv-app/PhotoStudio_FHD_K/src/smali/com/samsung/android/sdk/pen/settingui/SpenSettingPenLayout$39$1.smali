.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

.field private final synthetic val$fromFinal:F

.field private final synthetic val$step:F

.field private final synthetic val$toFinal:F


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;FFF)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->val$fromFinal:F

    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->val$toFinal:F

    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->val$step:F

    .line 7697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 7700
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCount:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$156(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$157(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 7702
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->val$fromFinal:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->val$toFinal:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 7703
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->val$fromFinal:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCount:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$156(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->val$step:F

    mul-float/2addr v2, v3

    sub-float v0, v1, v2

    .line 7704
    .local v0, "pos":F
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->val$toFinal:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_2

    .line 7705
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$134(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/HorizontalScrollView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 7706
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$134(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/HorizontalScrollView;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v1, v2, v4}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    .line 7741
    :cond_0
    :goto_0
    return-void

    .line 7708
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$158(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/Timer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 7709
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$158(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 7710
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    invoke-static {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$159(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/util/Timer;)V

    goto :goto_0

    .line 7714
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$134(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/HorizontalScrollView;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->val$toFinal:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v1, v2, v4}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    .line 7716
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$158(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/Timer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 7717
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$158(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 7718
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    invoke-static {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$159(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/util/Timer;)V

    goto :goto_0

    .line 7722
    .end local v0    # "pos":F
    :cond_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->val$fromFinal:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCount:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$156(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->val$step:F

    mul-float/2addr v2, v3

    add-float v0, v1, v2

    .line 7723
    .restart local v0    # "pos":F
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->val$toFinal:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_5

    .line 7724
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$134(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/HorizontalScrollView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-lez v1, :cond_4

    .line 7725
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$134(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/HorizontalScrollView;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v1, v2, v4}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    goto/16 :goto_0

    .line 7727
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$158(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/Timer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 7728
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$158(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 7729
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    invoke-static {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$159(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/util/Timer;)V

    goto/16 :goto_0

    .line 7733
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$134(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/HorizontalScrollView;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->val$toFinal:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v1, v2, v4}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    .line 7735
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$158(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/Timer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 7736
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$158(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 7737
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    invoke-static {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$159(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/util/Timer;)V

    goto/16 :goto_0
.end method
