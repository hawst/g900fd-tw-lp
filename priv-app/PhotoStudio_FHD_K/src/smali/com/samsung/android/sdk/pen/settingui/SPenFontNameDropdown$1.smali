.class Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;
.super Ljava/lang/Object;
.source "SPenFontNameDropdown.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 173
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$0(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 174
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowHeight:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)I

    move-result v1

    .line 175
    .local v1, "mtempHeight":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->anchor:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v2

    if-le v1, v2, :cond_1

    .line 176
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->anchor:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v1

    .line 180
    :goto_0
    const/4 v2, 0x0

    sput-boolean v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->isAutoScroll:Z

    .line 181
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->anchor:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowWidth:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$2(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)I

    move-result v4

    invoke-virtual {v2, v3, v4, v1}, Landroid/widget/PopupWindow;->update(Landroid/view/View;II)V

    .line 182
    const/4 v2, 0x1

    sput-boolean v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->isAutoScroll:Z

    .line 190
    .end local v1    # "mtempHeight":I
    :cond_0
    :goto_1
    return-void

    .line 178
    .restart local v1    # "mtempHeight":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowHeight:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 187
    .end local v1    # "mtempHeight":I
    :catch_0
    move-exception v0

    .line 188
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method
