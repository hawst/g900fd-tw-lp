.class public Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;
.super Ljava/lang/Object;
.source "NRRShapeRecognitionPlugin.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin$RecognitionTask;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mExecutor:Ljava/util/concurrent/ExecutorService;

.field private volatile mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

.field private final mMutex:Ljava/lang/Object;

.field private mRecognizer:Lcom/samsung/recognitionengine/ShapeRecognizer;

.field private final mUiHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mUiHandler:Landroid/os/Handler;

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mMutex:Ljava/lang/Object;

    .line 31
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mMutex:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;)Lcom/samsung/recognitionengine/ShapeRecognizer;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mRecognizer:Lcom/samsung/recognitionengine/ShapeRecognizer;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    return-object v0
.end method

.method static synthetic access$4()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static getAllowedShapes()Lcom/samsung/recognitionengine/ShapeTypeVector;
    .locals 4

    .prologue
    .line 146
    new-instance v0, Lcom/samsung/recognitionengine/ShapeTypeVector;

    invoke-direct {v0}, Lcom/samsung/recognitionengine/ShapeTypeVector;-><init>()V

    .line 147
    .local v0, "allowedShapes":Lcom/samsung/recognitionengine/ShapeTypeVector;
    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Lcom/samsung/recognitionengine/ShapeTypeVector;->reserve(J)V

    .line 149
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Square:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 150
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Rect:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 151
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Circle:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 152
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Ellipse:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 153
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 154
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_Equilateral:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 155
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_Isosceles:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 156
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_RightAngled:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 157
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_RightAngled_Isosceles:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 158
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Parallelogram:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 159
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 160
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Isosceles_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 161
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_RightAngled_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 162
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pentagon:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 163
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Hexagon:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 164
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Elongated_Hexagon:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 165
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Cross:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 166
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Heart:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 167
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Diamond:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 168
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftArrow:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 169
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftRightArrow:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 170
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Moon:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 171
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Chevron:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 172
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pentagon_Equilateral:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 173
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LShape:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 174
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ConcaveArrow:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 175
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pie:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 176
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SandGlass:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 177
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ConcaveFlag:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 178
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Star4:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 179
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Star5:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 180
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Star6:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 181
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Hexagram:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 182
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pentagram:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 183
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_FlowchartTerminator:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 185
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Angle:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 186
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Arc:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 187
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Line:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->add(Lcom/samsung/recognitionengine/ShapeType;)V

    .line 189
    return-object v0
.end method


# virtual methods
.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 142
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 134
    return-void
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onLoad"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 77
    :try_start_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRNativeInit;->initialize()V

    .line 78
    new-instance v0, Lcom/samsung/recognitionengine/ShapeRecognizer;

    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->getAllowedShapes()Lcom/samsung/recognitionengine/ShapeTypeVector;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/samsung/recognitionengine/ShapeRecognizer;-><init>(Lcom/samsung/recognitionengine/ShapeTypeVector;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mRecognizer:Lcom/samsung/recognitionengine/ShapeRecognizer;

    .line 76
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 82
    return-void

    .line 76
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onUnload()V
    .locals 2

    .prologue
    .line 90
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onUnload"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 93
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 94
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mRecognizer:Lcom/samsung/recognitionengine/ShapeRecognizer;

    .line 93
    monitor-exit v1

    .line 96
    return-void

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public request(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mRecognizer:Lcom/samsung/recognitionengine/ShapeRecognizer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mExecutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_1

    .line 48
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Plugin should be loaded first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_1
    if-nez p1, :cond_2

    .line 51
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 53
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 54
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 57
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin$RecognitionTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin$RecognitionTask;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;Ljava/util/List;Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin$RecognitionTask;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 58
    return-void
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 125
    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .line 67
    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 115
    const/4 v0, 0x1

    return v0
.end method
