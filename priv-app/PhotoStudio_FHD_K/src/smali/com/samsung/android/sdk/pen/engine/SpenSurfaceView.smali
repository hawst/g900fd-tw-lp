.class public Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
.super Landroid/view/SurfaceView;
.source "SpenSurfaceView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;,
        Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;
    }
.end annotation


# static fields
.field public static final PAGE_TRANSITION_EFFECT_LEFT:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_RIGHT:I = 0x1

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SHADOW:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SLIDE:I = 0x1

.field public static final REPLAY_STATE_PAUSED:I = 0x2

.field public static final REPLAY_STATE_PLAYING:I = 0x1

.field public static final REPLAY_STATE_STOPPED:I = 0x0

.field public static final STROKE_FRAME_KEY:Ljava/lang/String; = "STROKE_FRAME"

.field public static final STROKE_FRAME_RETAKE:I = 0x2

.field public static final STROKE_FRAME_TAKE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SpenSurfaceView"


# instance fields
.field private mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

.field private mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

.field private mMetricsRect:Landroid/graphics/Rect;

.field private mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 196
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 58
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    .line 59
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    .line 60
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    .line 61
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    .line 197
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->construct(Landroid/content/Context;)V

    .line 198
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 224
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    .line 59
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    .line 60
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    .line 61
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    .line 225
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->construct(Landroid/content/Context;)V

    .line 226
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 262
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    .line 59
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    .line 60
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    .line 61
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    .line 263
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->construct(Landroid/content/Context;)V

    .line 264
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    return-object v0
.end method

.method private construct(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 288
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    .line 289
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    invoke-direct {v3, p1, v4, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;Z)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    .line 290
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v3, :cond_0

    .line 291
    const/16 v3, 0x9

    const-string v4, "failed to create SpenInView"

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 308
    :goto_0
    return-void

    .line 294
    :cond_0
    if-nez p1, :cond_1

    .line 295
    const/16 v3, 0x8

    const-string v4, " : context must not be null"

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 298
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setView(Landroid/view/View;)V

    .line 300
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 301
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v3, v4, :cond_2

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 303
    .local v2, "width":I
    :goto_1
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, v5, v5, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    .line 304
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    .line 305
    .local v1, "holder":Landroid/view/SurfaceHolder;
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    .line 306
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    invoke-interface {v1, v3}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 307
    invoke-interface {v1, v6}, Landroid/view/SurfaceHolder;->setFormat(I)V

    goto :goto_0

    .line 302
    .end local v1    # "holder":Landroid/view/SurfaceHolder;
    .end local v2    # "width":I
    :cond_2
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_1
.end method

.method private getScreenPointOfView(Landroid/view/View;)Landroid/graphics/Point;
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 3033
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 3034
    .local v1, "screenPointOfView":Landroid/graphics/Point;
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 3035
    .local v0, "screenOffsetOfView":[I
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 3036
    const/4 v2, 0x0

    aget v2, v0, v2

    iput v2, v1, Landroid/graphics/Point;->x:I

    .line 3037
    const/4 v2, 0x1

    aget v2, v0, v2

    iput v2, v1, Landroid/graphics/Point;->y:I

    .line 3038
    return-object v1
.end method


# virtual methods
.method public SetForceStretchView(ZII)Z
    .locals 1
    .param p1, "enable"    # Z
    .param p2, "width"    # I
    .param p3, "height"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2973
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2974
    const/4 v0, 0x0

    .line 2976
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setForceStretchView(ZII)Z

    move-result v0

    goto :goto_0
.end method

.method public cancelStroke()V
    .locals 1

    .prologue
    .line 2370
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2374
    :goto_0
    return-void

    .line 2373
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->cancelStroke()V

    goto :goto_0
.end method

.method public cancelStrokeFrame()V
    .locals 1

    .prologue
    .line 2958
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2962
    :goto_0
    return-void

    .line 2961
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->cancelStrokeFrame()V

    goto :goto_0
.end method

.method public captureCurrentView(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "includeBlank"    # Z

    .prologue
    .line 655
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 656
    const/4 v0, 0x0

    .line 658
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->captureCurrentView(Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public capturePage(F)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "ratio"    # F

    .prologue
    .line 689
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 690
    const/4 v0, 0x0

    .line 692
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->capturePage(F)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public capturePage(FZ)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "ratio"    # F
    .param p2, "isTransparent"    # Z

    .prologue
    .line 716
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 717
    const/4 v0, 0x0

    .line 719
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->capturePage(FZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public changeStrokeFrame(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
    .locals 1
    .param p1, "objectContainer"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .prologue
    .line 2946
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2950
    :goto_0
    return-void

    .line 2949
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->changeStrokeFrame(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    goto :goto_0
.end method

.method public clearHighlight()V
    .locals 1

    .prologue
    .line 1876
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1880
    :goto_0
    return-void

    .line 1879
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->clearHighlight()V

    goto :goto_0
.end method

.method public close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 320
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v1, :cond_0

    .line 331
    :goto_0
    return-void

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->close()V

    .line 324
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    .line 325
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    .line 327
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 328
    .local v0, "holder":Landroid/view/SurfaceHolder;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 329
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    .line 330
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    goto :goto_0
.end method

.method public closeControl()V
    .locals 1

    .prologue
    .line 1654
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1658
    :goto_0
    return-void

    .line 1657
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    goto :goto_0
.end method

.method public drawObjectList(Ljava/util/ArrayList;)Landroid/graphics/Bitmap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 2357
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2358
    const/4 v0, 0x0

    .line 2360
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->drawObjectList(Ljava/util/ArrayList;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public extractSmartClipData(Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;)I
    .locals 13
    .param p1, "resultElement"    # Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;
    .param p2, "croppedArea"    # Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;

    .prologue
    .line 3047
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v9, :cond_0

    .line 3048
    const/4 v9, 0x0

    .line 3083
    :goto_0
    return v9

    .line 3051
    :cond_0
    invoke-interface {p2}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 3054
    .local v1, "cropRect":Landroid/graphics/Rect;
    invoke-direct {p0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getScreenPointOfView(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v5

    .line 3055
    .local v5, "leftTopOfView":Landroid/graphics/Point;
    iget v9, v5, Landroid/graphics/Point;->x:I

    neg-int v9, v9

    iget v10, v5, Landroid/graphics/Point;->y:I

    neg-int v10, v10

    invoke-virtual {v1, v9, v10}, Landroid/graphics/Rect;->offset(II)V

    .line 3058
    :try_start_0
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v10, 0x1

    .line 3059
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    const/4 v12, 0x1

    .line 3058
    invoke-virtual {v9, v10, v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->findObjectInRect(ILandroid/graphics/RectF;Z)Ljava/util/ArrayList;

    move-result-object v7

    .line 3061
    .local v7, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    new-instance v4, Lcom/samsung/android/hermes/object/HermesStrokes;

    invoke-direct {v4}, Lcom/samsung/android/hermes/object/HermesStrokes;-><init>()V

    .line 3062
    .local v4, "hermesStrokes":Lcom/samsung/android/hermes/object/HermesStrokes;
    if-eqz v7, :cond_2

    .line 3063
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_4

    .line 3073
    :cond_2
    invoke-virtual {v4}, Lcom/samsung/android/hermes/object/HermesStrokes;->size()I

    move-result v9

    if-lez v9, :cond_3

    .line 3075
    new-instance v8, Lcom/samsung/android/smartclip/SmartClipMetaTagImpl;

    const-string v9, "stroke"

    const-string v10, ""

    invoke-direct {v8, v9, v10, v4}, Lcom/samsung/android/smartclip/SmartClipMetaTagImpl;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 3076
    .local v8, "tag":Lcom/samsung/android/smartclip/SmartClipMetaTagImpl;
    invoke-interface {p1, v8}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;->addTag(Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;)V

    .line 3083
    .end local v8    # "tag":Lcom/samsung/android/smartclip/SmartClipMetaTagImpl;
    :cond_3
    const/4 v9, 0x1

    goto :goto_0

    .line 3063
    :cond_4
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 3064
    .local v6, "obj":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object v0, v6

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object v9, v0

    const-string v11, "strokeType"

    invoke-virtual {v9, v11}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getExtraDataInt(Ljava/lang/String;)I

    move-result v9

    const/4 v11, 0x1

    if-eq v9, v11, :cond_1

    .line 3066
    new-instance v3, Lcom/samsung/android/hermes/object/HermesStroke;

    invoke-direct {v3}, Lcom/samsung/android/hermes/object/HermesStroke;-><init>()V

    .line 3067
    .local v3, "hermesStroke":Lcom/samsung/android/hermes/object/HermesStroke;
    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .end local v6    # "obj":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/samsung/android/hermes/object/HermesStroke;->setPoints([Landroid/graphics/PointF;)V

    .line 3068
    invoke-virtual {v4, v3}, Lcom/samsung/android/hermes/object/HermesStrokes;->addStroke(Lcom/samsung/android/hermes/object/HermesStroke;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 3079
    .end local v3    # "hermesStroke":Lcom/samsung/android/hermes/object/HermesStroke;
    .end local v4    # "hermesStrokes":Lcom/samsung/android/hermes/object/HermesStrokes;
    .end local v7    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :catch_0
    move-exception v2

    .line 3080
    .local v2, "e":Ljava/lang/NoClassDefFoundError;
    const/4 v9, 0x0

    goto :goto_0
.end method

.method public getBlankColor()I
    .locals 1

    .prologue
    .line 1059
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1060
    const/4 v0, 0x0

    .line 1062
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getBlankColor()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasHeight()I
    .locals 1

    .prologue
    .line 1092
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1093
    const/4 v0, 0x0

    .line 1095
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getCanvasHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasWidth()I
    .locals 1

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1079
    const/4 v0, 0x0

    .line 1081
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getCanvasWidth()I

    move-result v0

    goto :goto_0
.end method

.method public getControl()Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    .locals 1

    .prologue
    .line 1638
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1639
    const/4 v0, 0x0

    .line 1641
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getControl()Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v0

    goto :goto_0
.end method

.method public getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 1

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1238
    const/4 v0, 0x0

    .line 1240
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getFrameStartPosition()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 903
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 904
    const/4 v0, 0x0

    .line 906
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getFrameStartPosition()Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_0
.end method

.method public getMaxZoomRatio()F
    .locals 1

    .prologue
    .line 942
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 943
    const/4 v0, 0x0

    .line 945
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getMaxZoomRatio()F

    move-result v0

    goto :goto_0
.end method

.method public getMinZoomRatio()F
    .locals 1

    .prologue
    .line 981
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 982
    const/4 v0, 0x0

    .line 984
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getMinZoomRatio()F

    move-result v0

    goto :goto_0
.end method

.method public getPan()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1022
    const/4 v0, 0x0

    .line 1024
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getPan()Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_0
.end method

.method public getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 1

    .prologue
    .line 1193
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1194
    const/4 v0, 0x0

    .line 1196
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    .prologue
    .line 1281
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1282
    const/4 v0, 0x0

    .line 1284
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getReplayState()I
    .locals 1

    .prologue
    .line 1735
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1736
    const/4 v0, 0x0

    .line 1738
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v0

    goto :goto_0
.end method

.method public getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    .locals 1

    .prologue
    .line 1325
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1326
    const/4 v0, 0x0

    .line 1328
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getTemporaryStroke()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1925
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1926
    const/4 v0, 0x0

    .line 1928
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getTemporaryStroke()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    .prologue
    .line 1149
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1150
    const/4 v0, 0x0

    .line 1152
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getToolTypeAction(I)I
    .locals 1
    .param p1, "toolType"    # I

    .prologue
    .line 798
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 799
    const/4 v0, 0x0

    .line 801
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    goto :goto_0
.end method

.method public getZoomPadBoxHeight()F
    .locals 1

    .prologue
    .line 2193
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2194
    const/4 v0, 0x0

    .line 2196
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomPadBoxHeight()F

    move-result v0

    goto :goto_0
.end method

.method public getZoomPadBoxPosition()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 2222
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2223
    const/4 v0, 0x0

    .line 2225
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomPadBoxPosition()Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoomPadBoxRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 2120
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2121
    const/4 v0, 0x0

    .line 2123
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomPadBoxRect()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoomPadPosition()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 2249
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2250
    const/4 v0, 0x0

    .line 2252
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomPadPosition()Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoomPadRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 2133
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2134
    const/4 v0, 0x0

    .line 2136
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomPadRect()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoomRatio()F
    .locals 1

    .prologue
    .line 885
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 886
    const/4 v0, 0x0

    .line 888
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomRatio()F

    move-result v0

    goto :goto_0
.end method

.method public isDottedLineEnabled()Z
    .locals 1

    .prologue
    .line 1841
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1842
    const/4 v0, 0x0

    .line 1844
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isDottedLineEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isHorizontalScrollBarEnabled()Z
    .locals 1

    .prologue
    .line 1996
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->isScrollBarEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1997
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isHorizontalScrollBarEnabled()Z

    move-result v0

    .line 1999
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/view/SurfaceView;->isHorizontalScrollBarEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isHorizontalSmartScrollEnabled()Z
    .locals 1

    .prologue
    .line 1545
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1546
    const/4 v0, 0x0

    .line 1548
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isHorizontalSmartScrollEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isHyperTextViewEnabled()Z
    .locals 1

    .prologue
    .line 2079
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2080
    const/4 v0, 0x0

    .line 2082
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isHyperTextViewEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isLongPressEnabled()Z
    .locals 1

    .prologue
    .line 566
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 567
    const/4 v0, 0x0

    .line 569
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isLongPressEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isPenButtonSelectionEnabled()Z
    .locals 1

    .prologue
    .line 2401
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2402
    const/4 v0, 0x0

    .line 2404
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isPenButtonSelectionEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isScrollBarEnabled()Z
    .locals 1

    .prologue
    .line 1962
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1963
    const/4 v0, 0x0

    .line 1965
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isScrollBarEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isSmartScaleEnabled()Z
    .locals 1

    .prologue
    .line 1492
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1493
    const/4 v0, 0x0

    .line 1495
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSmartScaleEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isTextCursorEnable()Z
    .locals 1

    .prologue
    .line 595
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 596
    const/4 v0, 0x0

    .line 598
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isTextCursorEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isToolTipEnabled()Z
    .locals 1

    .prologue
    .line 2107
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2108
    const/4 v0, 0x0

    .line 2110
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isToolTipEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isVerticalScrollBarEnabled()Z
    .locals 1

    .prologue
    .line 2031
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->isScrollBarEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2032
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isVerticalScrollBarEnabled()Z

    move-result v0

    .line 2034
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/view/SurfaceView;->isVerticalScrollBarEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isVerticalSmartScrollEnabled()Z
    .locals 1

    .prologue
    .line 1597
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1598
    const/4 v0, 0x0

    .line 1600
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isVerticalSmartScrollEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isZoomPadDrawing()Z
    .locals 1

    .prologue
    .line 2164
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2165
    const/4 v0, 0x0

    .line 2167
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isZoomPadStroking()Z

    move-result v0

    goto :goto_0
.end method

.method public isZoomPadEnabled()Z
    .locals 1

    .prologue
    .line 2287
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2288
    const/4 v0, 0x0

    .line 2290
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isZoomPadEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isZoomPadStroking()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2149
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2150
    const/4 v0, 0x0

    .line 2152
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isZoomPadStroking()Z

    move-result v0

    goto :goto_0
.end method

.method public isZoomable()Z
    .locals 1

    .prologue
    .line 837
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 838
    const/4 v0, 0x0

    .line 840
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isZoomable()Z

    move-result v0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 384
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 389
    :goto_0
    return-void

    .line 387
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setParent(Landroid/view/ViewGroup;)V

    .line 388
    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 393
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 398
    :goto_0
    return-void

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setParent(Landroid/view/ViewGroup;)V

    .line 397
    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 552
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 553
    const/4 v0, 0x0

    .line 555
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation",
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 363
    invoke-super/range {p0 .. p5}, Landroid/view/SurfaceView;->onLayout(ZIIII)V

    .line 364
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 380
    :goto_0
    return-void

    .line 368
    :cond_0
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 369
    .local v6, "parentLayoutRect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 371
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 372
    .local v7, "windowVisibleRect":Landroid/graphics/Rect;
    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 373
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v7}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 374
    :cond_1
    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    iget v1, v6, Landroid/graphics/Rect;->bottom:I

    if-ge v0, v1, :cond_2

    .line 375
    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    iget v1, v6, Landroid/graphics/Rect;->top:I

    if-gt v0, v1, :cond_3

    .line 376
    :cond_2
    const/4 v7, 0x0

    .line 379
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onLayout(ZIIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 347
    :try_start_0
    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_deinit_c2dJNI()V

    .line 348
    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_init_c2dJNI()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_2

    .line 357
    :goto_0
    const-string v1, "SpenSurfaceView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onSizeChanged("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    return-void

    .line 349
    :catch_0
    move-exception v0

    .line 350
    .local v0, "error":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    goto :goto_0

    .line 351
    .end local v0    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 352
    .local v0, "error":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_0

    .line 353
    .end local v0    # "error":Ljava/lang/NoClassDefFoundError;
    :catch_2
    move-exception v0

    .line 354
    .local v0, "error":Ljava/lang/ExceptionInInitializerError;
    invoke-virtual {v0}, Ljava/lang/ExceptionInInitializerError;->printStackTrace()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 528
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 529
    const/4 v0, 0x0

    .line 531
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 337
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 342
    :goto_0
    return-void

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 341
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onVisibilityChanged(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 416
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 420
    :goto_0
    return-void

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onWindowFocusChanged(Z)V

    goto :goto_0
.end method

.method public pauseReplay()V
    .locals 1

    .prologue
    .line 1701
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1705
    :goto_0
    return-void

    .line 1704
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->pauseReplay()V

    goto :goto_0
.end method

.method public requestPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 2
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "isUpdate"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1439
    const/16 v0, 0xd

    const-string v1, " : requestPageDoc not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 1440
    const/4 v0, 0x0

    return v0
.end method

.method public resumeReplay()V
    .locals 1

    .prologue
    .line 1716
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1720
    :goto_0
    return-void

    .line 1719
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resumeReplay()V

    goto :goto_0
.end method

.method public retakeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "canvasLayout"    # Landroid/view/ViewGroup;
    .param p3, "strokeFrameContainer"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .param p4, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .prologue
    .line 2932
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2936
    :goto_0
    return-void

    .line 2935
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->retakeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V

    goto :goto_0
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2422
    const/16 v0, 0xd

    const-string v1, " : setBackground not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 2423
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 2
    .param p1, "color"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2458
    const/16 v0, 0xd

    const-string v1, " : setBackgroundColor not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 2459
    return-void
.end method

.method public setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "listener"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1109
    :goto_0
    return-void

    .line 1108
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V

    goto :goto_0
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2440
    const/16 v0, 0xd

    const-string v1, " : setBackgroundDrawable not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 2441
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 2
    .param p1, "resid"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2476
    const/16 v0, 0xd

    const-string v1, " : setBackgroundResource not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 2477
    return-void
.end method

.method public setBlankColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 1041
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1045
    :goto_0
    return-void

    .line 1044
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setBlankColor(I)V

    goto :goto_0
.end method

.method public setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .prologue
    .line 2593
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2597
    :goto_0
    return-void

    .line 2596
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V

    goto :goto_0
.end method

.method public setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 1
    .param p1, "control"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    .prologue
    .line 1619
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1623
    :goto_0
    return-void

    .line 1622
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    goto :goto_0
.end method

.method public setControlListener(Lcom/samsung/android/sdk/pen/engine/SpenControlListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    .prologue
    .line 2773
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2777
    :goto_0
    return-void

    .line 2776
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControlListener(Lcom/samsung/android/sdk/pen/engine/SpenControlListener;)V

    goto :goto_0
.end method

.method public setDottedLineEnabled(ZIII[FF)V
    .locals 7
    .param p1, "enable"    # Z
    .param p2, "intervalHeight"    # I
    .param p3, "color"    # I
    .param p4, "thickness"    # I
    .param p5, "pathIntervals"    # [F
    .param p6, "phase"    # F

    .prologue
    .line 1822
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1826
    :goto_0
    return-void

    .line 1825
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setDottedLineEnabled(ZIII[FF)V

    goto :goto_0
.end method

.method public setDoubleTapZoomable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 3009
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 3013
    :goto_0
    return-void

    .line 3012
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setDoubleTapZoomable(Z)V

    goto :goto_0
.end method

.method public setEraserChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .prologue
    .line 2693
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2697
    :goto_0
    return-void

    .line 2696
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setEraserChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;)V

    goto :goto_0
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    .line 1216
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1220
    :goto_0
    return-void

    .line 1219
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto :goto_0
.end method

.method public setFlickListener(Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .prologue
    .line 2793
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2797
    :goto_0
    return-void

    .line 2796
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setFlickListener(Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;)V

    goto :goto_0
.end method

.method public setForceStretchView(ZII)Z
    .locals 1
    .param p1, "enable"    # Z
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 2993
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-eqz v0, :cond_0

    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    .line 2994
    :cond_0
    const/4 v0, 0x0

    .line 2996
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setForceStretchView(ZII)Z

    move-result v0

    goto :goto_0
.end method

.method public setHighlight(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1861
    .local p1, "highlightInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1865
    :goto_0
    return-void

    .line 1864
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHighlight(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public setHorizontalScrollBarEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1980
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-eqz v0, :cond_0

    .line 1981
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHorizontalScrollBarEnabled(Z)V

    .line 1983
    :cond_0
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->setHorizontalScrollBarEnabled(Z)V

    .line 1984
    return-void
.end method

.method public setHorizontalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 6
    .param p1, "enable"    # Z
    .param p2, "leftScrollRegion"    # Landroid/graphics/Rect;
    .param p3, "rightScrollRegion"    # Landroid/graphics/Rect;
    .param p4, "responseTime"    # I
    .param p5, "velocity"    # I

    .prologue
    .line 1525
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1530
    :goto_0
    return-void

    .line 1528
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    .line 1529
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHorizontalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V

    goto :goto_0
.end method

.method public setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .prologue
    .line 2533
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2537
    :goto_0
    return-void

    .line 2536
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V

    goto :goto_0
.end method

.method public setHyperTextListener(Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    .prologue
    .line 2847
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2851
    :goto_0
    return-void

    .line 2850
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHyperTextListener(Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;)V

    goto :goto_0
.end method

.method public setHyperTextViewEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 2065
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2069
    :goto_0
    return-void

    .line 2068
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHyperTextViewEnabled(Z)V

    goto :goto_0
.end method

.method public setLongPressEnabled(Z)Z
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 582
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 583
    const/4 v0, 0x0

    .line 585
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setLongPressEnabled(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public setLongPressListener(Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    .prologue
    .line 2553
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2557
    :goto_0
    return-void

    .line 2556
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setLongPressListener(Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;)V

    goto :goto_0
.end method

.method public setMaxZoomRatio(F)Z
    .locals 1
    .param p1, "ratio"    # F

    .prologue
    .line 924
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 925
    const/4 v0, 0x0

    .line 927
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setMaxZoomRatio(F)Z

    move-result v0

    goto :goto_0
.end method

.method public setMinZoomRatio(F)Z
    .locals 1
    .param p1, "ratio"    # F

    .prologue
    .line 963
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 964
    const/4 v0, 0x0

    .line 966
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setMinZoomRatio(F)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;IIF)Z
    .locals 1
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "direction"    # I
    .param p3, "type"    # I
    .param p4, "centerY"    # F

    .prologue
    .line 1404
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1405
    const/4 v0, 0x0

    .line 1407
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;IIF)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 1
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "isUpdate"    # Z

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1358
    const/4 v0, 0x0

    .line 1360
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageEffectListener(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .prologue
    .line 2633
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2637
    :goto_0
    return-void

    .line 2636
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPageEffectListener(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;)V

    goto :goto_0
.end method

.method public setPan(Landroid/graphics/PointF;)V
    .locals 1
    .param p1, "position"    # Landroid/graphics/PointF;

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1007
    :goto_0
    return-void

    .line 1006
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPan(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public setPenButtonSelectionEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 2386
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2390
    :goto_0
    return-void

    .line 2389
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenButtonSelectionEnabled(Z)V

    goto :goto_0
.end method

.method public setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .prologue
    .line 2673
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2677
    :goto_0
    return-void

    .line 2676
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V

    goto :goto_0
.end method

.method public setPenDetachmentListener(Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    .prologue
    .line 2653
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2657
    :goto_0
    return-void

    .line 2656
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenDetachmentListener(Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;)V

    goto :goto_0
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1176
    :goto_0
    return-void

    .line 1175
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_0
.end method

.method public setPenTooltipImage(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "penName"    # Ljava/lang/String;
    .param p2, "image"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 3026
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 3030
    :goto_0
    return-void

    .line 3029
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenTooltipImage(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setPostDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .prologue
    .line 2833
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2837
    :goto_0
    return-void

    .line 2836
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPostDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V

    goto :goto_0
.end method

.method public setPreDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .prologue
    .line 2813
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2817
    :goto_0
    return-void

    .line 2816
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPreDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V

    goto :goto_0
.end method

.method public setPreTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .prologue
    .line 2493
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2497
    :goto_0
    return-void

    .line 2496
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPreTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V

    goto :goto_0
.end method

.method public setRemoverChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    .prologue
    .line 2713
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2717
    :goto_0
    return-void

    .line 2716
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setRemoverChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;)V

    goto :goto_0
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 1260
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1264
    :goto_0
    return-void

    .line 1263
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    goto :goto_0
.end method

.method public setReplayListener(Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    .prologue
    .line 2573
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2577
    :goto_0
    return-void

    .line 2576
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setReplayListener(Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;)V

    goto :goto_0
.end method

.method public setReplayPosition(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1781
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1785
    :goto_0
    return-void

    .line 1784
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setReplayPosition(I)V

    goto :goto_0
.end method

.method public setReplaySpeed(I)V
    .locals 1
    .param p1, "speed"    # I

    .prologue
    .line 1757
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1761
    :goto_0
    return-void

    .line 1760
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setReplaySpeed(I)V

    goto :goto_0
.end method

.method public setRequestPageDocListener(Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2888
    const/16 v0, 0xd

    const-string v1, " : setRequestPageDocListener not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 2894
    return-void
.end method

.method public setScrollBarEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1945
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1949
    :goto_0
    return-void

    .line 1948
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setScrollBarEnabled(Z)V

    goto :goto_0
.end method

.method public setSelectionChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    .prologue
    .line 2753
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2757
    :goto_0
    return-void

    .line 2756
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setSelectionChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;)V

    goto :goto_0
.end method

.method public setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .prologue
    .line 1304
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1308
    :goto_0
    return-void

    .line 1307
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V

    goto :goto_0
.end method

.method public setSmartScaleEnabled(ZLandroid/graphics/Rect;IIF)V
    .locals 6
    .param p1, "enable"    # Z
    .param p2, "region"    # Landroid/graphics/Rect;
    .param p3, "effectFrame"    # I
    .param p4, "zoomOutResponseTime"    # I
    .param p5, "zoomRatio"    # F

    .prologue
    .line 1473
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1477
    :goto_0
    return-void

    .line 1476
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setSmartScaleEnabled(ZLandroid/graphics/Rect;IIF)V

    goto :goto_0
.end method

.method public setTextChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    .prologue
    .line 2733
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2737
    :goto_0
    return-void

    .line 2736
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setTextChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;)V

    goto :goto_0
.end method

.method public setTextCursorEnable(Z)Z
    .locals 1
    .param p1, "enable"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 611
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 612
    const/4 v0, 0x0

    .line 614
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setTextCursorEnabled(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public setTextCursorEnabled(Z)Z
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 626
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 627
    const/4 v0, 0x0

    .line 629
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setTextCursorEnabled(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    .line 1128
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1132
    :goto_0
    return-void

    .line 1131
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    goto :goto_0
.end method

.method public setToolTipEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 2093
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2097
    :goto_0
    return-void

    .line 2096
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setToolTipEnabled(Z)V

    goto :goto_0
.end method

.method public setToolTypeAction(II)V
    .locals 1
    .param p1, "toolType"    # I
    .param p2, "action"    # I

    .prologue
    .line 758
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 762
    :goto_0
    return-void

    .line 761
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setToolTypeAction(II)V

    goto :goto_0
.end method

.method public setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .prologue
    .line 2513
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2517
    :goto_0
    return-void

    .line 2516
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V

    goto :goto_0
.end method

.method public setVerticalScrollBarEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 2015
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-eqz v0, :cond_0

    .line 2016
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setVerticalScrollBarEnabled(Z)V

    .line 2018
    :cond_0
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->setVerticalScrollBarEnabled(Z)V

    .line 2019
    return-void
.end method

.method public setVerticalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 6
    .param p1, "enable"    # Z
    .param p2, "topScrollRegion"    # Landroid/graphics/Rect;
    .param p3, "bottomScrollRegion"    # Landroid/graphics/Rect;
    .param p4, "responseTime"    # I
    .param p5, "velocity"    # I

    .prologue
    .line 1578
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1582
    :goto_0
    return-void

    .line 1581
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setVerticalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V

    goto :goto_0
.end method

.method public setZoom(FFF)V
    .locals 1
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 867
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 871
    :goto_0
    return-void

    .line 870
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoom(FFF)V

    goto :goto_0
.end method

.method public setZoomListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .prologue
    .line 2613
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2617
    :goto_0
    return-void

    .line 2616
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;)V

    goto :goto_0
.end method

.method public setZoomPadBoxHeight(F)V
    .locals 1
    .param p1, "height"    # F

    .prologue
    .line 2179
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2183
    :goto_0
    return-void

    .line 2182
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadBoxHeight(F)V

    goto :goto_0
.end method

.method public setZoomPadBoxHeightEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 2303
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2307
    :goto_0
    return-void

    .line 2306
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadBoxHeightEnabled(Z)V

    goto :goto_0
.end method

.method public setZoomPadBoxPosition(Landroid/graphics/PointF;)V
    .locals 1
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 2208
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2212
    :goto_0
    return-void

    .line 2211
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadBoxPosition(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public setZoomPadButtonEnabled(ZZZZZ)V
    .locals 6
    .param p1, "left"    # Z
    .param p2, "right"    # Z
    .param p3, "enter"    # Z
    .param p4, "up"    # Z
    .param p5, "down"    # Z

    .prologue
    .line 2331
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2335
    :goto_0
    return-void

    .line 2334
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadButtonEnabled(ZZZZZ)V

    goto :goto_0
.end method

.method public setZoomPadListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    .prologue
    .line 2861
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2865
    :goto_0
    return-void

    .line 2864
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;)V

    goto :goto_0
.end method

.method public setZoomPadPosition(Landroid/graphics/PointF;)V
    .locals 1
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 2236
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2240
    :goto_0
    return-void

    .line 2239
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadPosition(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public setZoomable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 818
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 822
    :goto_0
    return-void

    .line 821
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomable(Z)V

    goto :goto_0
.end method

.method public startReplay()V
    .locals 1

    .prologue
    .line 1670
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->closeControl()V

    .line 1671
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1675
    :goto_0
    return-void

    .line 1674
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->startReplay()V

    goto :goto_0
.end method

.method public startTemporaryStroke()V
    .locals 1

    .prologue
    .line 1891
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1895
    :goto_0
    return-void

    .line 1894
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->startTemporaryStroke()V

    goto :goto_0
.end method

.method public startZoomPad()V
    .locals 1

    .prologue
    .line 2261
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2265
    :goto_0
    return-void

    .line 2264
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->startZoomPad()V

    goto :goto_0
.end method

.method public stopReplay()V
    .locals 1

    .prologue
    .line 1686
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1690
    :goto_0
    return-void

    .line 1689
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->stopReplay()V

    goto :goto_0
.end method

.method public stopTemporaryStroke()V
    .locals 1

    .prologue
    .line 1906
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1910
    :goto_0
    return-void

    .line 1909
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->stopTemporaryStroke()V

    goto :goto_0
.end method

.method public stopZoomPad()V
    .locals 1

    .prologue
    .line 2273
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2277
    :goto_0
    return-void

    .line 2276
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->stopZoomPad()V

    goto :goto_0
.end method

.method public takeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Ljava/util/List;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "canvasLayout"    # Landroid/view/ViewGroup;
    .param p4, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;",
            "Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2911
    .local p3, "strokeList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2915
    :goto_0
    return-void

    .line 2914
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->takeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Ljava/util/List;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V

    goto :goto_0
.end method

.method public update()V
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 438
    :goto_0
    return-void

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    goto :goto_0
.end method

.method public updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 1
    .param p1, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    .prologue
    .line 504
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 508
    :goto_0
    return-void

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    goto :goto_0
.end method

.method public updateScreen()V
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 468
    :goto_0
    return-void

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreen()V

    goto :goto_0
.end method

.method public updateScreenFrameBuffer()V
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 453
    :goto_0
    return-void

    .line 452
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    goto :goto_0
.end method

.method public updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 1
    .param p1, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    .prologue
    .line 484
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 488
    :goto_0
    return-void

    .line 487
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    goto :goto_0
.end method
