.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
.super Landroid/os/Handler;
.source "SpenTextBox.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Blink"
.end annotation


# static fields
.field private static final CURSOR_DELAY:I = 0x258

.field public static final CURSOR_MESSAGE:I = 0x1


# instance fields
.field private mIsStartBlink:Z

.field private final mTextBox:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenTextBox;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 1
    .param p1, "textBox"    # Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .prologue
    .line 5681
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 5677
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    .line 5682
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    .line 5683
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    .line 5687
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    .line 5710
    :cond_0
    :goto_0
    return-void

    .line 5690
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 5691
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    if-eqz v0, :cond_0

    .line 5695
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 5709
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 5697
    :pswitch_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    .line 5698
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    if-eqz v1, :cond_2

    .line 5699
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->removeMessages(I)V

    .line 5700
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    :goto_2
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5701
    const-wide/16 v4, 0x258

    invoke-virtual {p0, v2, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    :cond_3
    move v1, v2

    .line 5700
    goto :goto_2

    .line 5695
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public restartBlink()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 5731
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    .line 5746
    :cond_0
    :goto_0
    return-void

    .line 5734
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 5735
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    if-eqz v0, :cond_0

    .line 5739
    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5741
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->removeMessages(I)V

    .line 5743
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    if-eqz v1, :cond_0

    .line 5744
    const-wide/16 v2, 0x258

    invoke-virtual {p0, v4, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public startBlink()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 5713
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    .line 5728
    :cond_0
    :goto_0
    return-void

    .line 5716
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 5717
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    if-eqz v0, :cond_0

    .line 5721
    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5723
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    .line 5725
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->removeMessages(I)V

    .line 5727
    const-wide/16 v2, 0x258

    invoke-virtual {p0, v4, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public stopBlink()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5749
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    .line 5762
    :cond_0
    :goto_0
    return-void

    .line 5752
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 5753
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    if-eqz v0, :cond_0

    .line 5757
    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5759
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    .line 5761
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->removeMessages(I)V

    goto :goto_0
.end method
