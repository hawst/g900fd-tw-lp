.class Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;
.super Ljava/lang/Object;
.source "SpenInView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenInView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 5367
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;)V
    .locals 0

    .prologue
    .line 5367
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 5371
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5448
    if-eqz p1, :cond_0

    .line 5449
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v2

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 5450
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsCancelFling:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$37(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5451
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    .line 5455
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 19
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 5394
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 5444
    :cond_0
    :goto_0
    return-void

    .line 5397
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5401
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsLongPressEnable:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5405
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    .line 5406
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMeasure:Landroid/graphics/PathMeasure;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PathMeasure;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/Path;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PathMeasure;->setPath(Landroid/graphics/Path;Z)V

    .line 5407
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMeasure:Landroid/graphics/PathMeasure;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PathMeasure;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->MIN_STROKE_LENGTH:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 5412
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$23(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 5413
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$23(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;->onLongPressed(Landroid/view/MotionEvent;)V

    .line 5416
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$24(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 5417
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenInView;Z)V

    .line 5418
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/high16 v3, -0x3d380000    # -100.0f

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V

    .line 5419
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/high16 v3, -0x3d380000    # -100.0f

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$27(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V

    .line 5420
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PointF;

    move-result-object v2

    const/high16 v3, -0x3d380000    # -100.0f

    iput v3, v2, Landroid/graphics/PointF;->x:F

    .line 5421
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PointF;

    move-result-object v2

    const/high16 v3, -0x3d380000    # -100.0f

    iput v3, v2, Landroid/graphics/PointF;->y:F

    .line 5424
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenInView;Z)V

    .line 5425
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartScale:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v2

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartHorizontal:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v2

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartVertical:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$33(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 5426
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v3

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v3

    add-float v17, v2, v3

    .line 5427
    .local v17, "x":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v3

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v3

    add-float v18, v2, v3

    .line 5428
    .local v18, "y":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 5429
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v2

    const/16 v3, 0xe

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v2, v3, v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->findTopObjectAtPosition(IFF)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 5431
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->getCenterX()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->getCenterY()F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoom(FFF)V

    .line 5432
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v2

    sub-float v2, v17, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v3

    mul-float v7, v2, v3

    .line 5433
    .local v7, "rx":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v2

    sub-float v2, v18, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v3

    mul-float v8, v2, v3

    .line 5434
    .local v8, "ry":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .line 5435
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSize()F

    move-result v10

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getXPrecision()F

    move-result v12

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getYPrecision()F

    move-result v13

    .line 5436
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v14

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v15

    .line 5434
    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v16

    .line 5438
    .local v16, "event":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v4

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    move-object/from16 v0, v16

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onLongPress(JLandroid/view/MotionEvent;I)Z
    invoke-static {v2, v4, v5, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/view/MotionEvent;I)Z

    .line 5439
    invoke-virtual/range {v16 .. v16}, Landroid/view/MotionEvent;->recycle()V

    goto/16 :goto_0

    .line 5442
    .end local v7    # "rx":F
    .end local v8    # "ry":F
    .end local v16    # "event":Landroid/view/MotionEvent;
    .end local v17    # "x":F
    .end local v18    # "y":F
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v4

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    move-object/from16 v0, p1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onLongPress(JLandroid/view/MotionEvent;I)Z
    invoke-static {v2, v4, v5, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/view/MotionEvent;I)Z

    goto/16 :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 5389
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 5376
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 5380
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5384
    :goto_0
    return v4

    .line 5383
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onSingleTapUp(JLandroid/view/MotionEvent;I)Z
    invoke-static {v0, v2, v3, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/view/MotionEvent;I)Z

    goto :goto_0
.end method
