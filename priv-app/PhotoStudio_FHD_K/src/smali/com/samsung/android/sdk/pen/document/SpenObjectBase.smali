.class public Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.super Ljava/lang/Object;
.source "SpenObjectBase.java"


# static fields
.field private static final NATIVE_COMMAND_DUMMY:I = 0x0

.field public static final OBJECT_MINIMUM_SIZE:F = 10.0f

.field public static final RESIZE_OPTION_DISABLE:I = 0x2

.field public static final RESIZE_OPTION_FREE:I = 0x0

.field public static final RESIZE_OPTION_KEEP_RATIO:I = 0x1

.field public static final SPEN_INFINITY_FLOAT:F = 3.4028235E38f

.field public static final SPEN_INFINITY_INT:I = 0x7fffffff

.field public static final TYPE_CONTAINER:I = 0x4

.field public static final TYPE_IMAGE:I = 0x3

.field public static final TYPE_NONE:I = 0x0

.field public static final TYPE_STROKE:I = 0x1

.field public static final TYPE_TEXT_BOX:I = 0x2


# instance fields
.field private mHandle:I

.field private final mType:I


# direct methods
.method protected constructor <init>(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput p1, p0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->mType:I

    .line 115
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->mHandle:I

    .line 116
    return-void
.end method

.method private native Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private native ObjectBase_attachFile(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_clearChangedFlag()V
.end method

.method private native ObjectBase_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native ObjectBase_copyExtraData(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native ObjectBase_detachFile()Z
.end method

.method private native ObjectBase_enableClip(Z)Z
.end method

.method private native ObjectBase_enableMovement(Z)Z
.end method

.method private native ObjectBase_enableRotation(Z)Z
.end method

.method private native ObjectBase_enableSelection(Z)Z
.end method

.method private native ObjectBase_finalize()V
.end method

.method private native ObjectBase_getAttachedFile()Ljava/lang/String;
.end method

.method private native ObjectBase_getCreateTimeStamp()I
.end method

.method private native ObjectBase_getDrawnRect()Landroid/graphics/RectF;
.end method

.method private native ObjectBase_getExtraDataByteArray(Ljava/lang/String;)[B
.end method

.method private native ObjectBase_getExtraDataInt(Ljava/lang/String;)I
.end method

.method private native ObjectBase_getExtraDataString(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native ObjectBase_getExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method private native ObjectBase_getHistoryManagerId()I
.end method

.method private native ObjectBase_getMaxHeight()F
.end method

.method private native ObjectBase_getMaxWidth()F
.end method

.method private native ObjectBase_getMinHeight()F
.end method

.method private native ObjectBase_getMinWidth()F
.end method

.method private native ObjectBase_getRect()Landroid/graphics/RectF;
.end method

.method private native ObjectBase_getResizeOption()I
.end method

.method private native ObjectBase_getRotation()F
.end method

.method private native ObjectBase_getRuntimeHandle()I
.end method

.method private native ObjectBase_getSorDataByteArray(Ljava/lang/String;)[B
.end method

.method private native ObjectBase_getSorDataInt(Ljava/lang/String;)I
.end method

.method private native ObjectBase_getSorDataString(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native ObjectBase_getSorDataStringArray(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method private native ObjectBase_getSorInfo()Ljava/lang/String;
.end method

.method private native ObjectBase_getSorPackageLink()Ljava/lang/String;
.end method

.method private native ObjectBase_getTemplateProperty()Z
.end method

.method private native ObjectBase_getType()I
.end method

.method private native ObjectBase_getUserId()I
.end method

.method private native ObjectBase_hasExtraDataByteArray(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_hasExtraDataInt(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_hasExtraDataString(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_hasExtraDataStringArray(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_hasSorDataByteArray(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_hasSorDataInt(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_hasSorDataString(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_hasSorDataStringArray(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_isChanged()Z
.end method

.method private native ObjectBase_isClippable()Z
.end method

.method private native ObjectBase_isFlipEnabled()Z
.end method

.method private native ObjectBase_isMovable()Z
.end method

.method private native ObjectBase_isRecorded()Z
.end method

.method private native ObjectBase_isRotatable()Z
.end method

.method private native ObjectBase_isSelectable()Z
.end method

.method private native ObjectBase_isVisible()Z
.end method

.method private native ObjectBase_move(FF)Z
.end method

.method private native ObjectBase_removeExtraDataByteArray(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_removeExtraDataInt(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_removeExtraDataString(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_removeExtraDataStringArray(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_removeSorDataByteArray(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_removeSorDataInt(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_removeSorDataString(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_removeSorDataStringArray(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_resize(FF)Z
.end method

.method private native ObjectBase_setCreateTimeStamp(I)Z
.end method

.method private native ObjectBase_setExtraDataByteArray(Ljava/lang/String;[BI)Z
.end method

.method private native ObjectBase_setExtraDataInt(Ljava/lang/String;I)Z
.end method

.method private native ObjectBase_setExtraDataString(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private native ObjectBase_setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z
.end method

.method private native ObjectBase_setFlipEnabled(Z)Z
.end method

.method private native ObjectBase_setMaxSize(FF)Z
.end method

.method private native ObjectBase_setMinSize(FF)Z
.end method

.method private native ObjectBase_setRecorded(Z)Z
.end method

.method private native ObjectBase_setRect(Landroid/graphics/RectF;Z)Z
.end method

.method private native ObjectBase_setResizeOption(I)Z
.end method

.method private native ObjectBase_setRotation(F)Z
.end method

.method private native ObjectBase_setSorDataByteArray(Ljava/lang/String;[BI)Z
.end method

.method private native ObjectBase_setSorDataInt(Ljava/lang/String;I)Z
.end method

.method private native ObjectBase_setSorDataString(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private native ObjectBase_setSorDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z
.end method

.method private native ObjectBase_setSorInfo(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_setSorPackageLink(Ljava/lang/String;)Z
.end method

.method private native ObjectBase_setUserId(I)Z
.end method

.method private native ObjectBase_setVisibility(Z)Z
.end method

.method private throwUncheckedException(I)V
    .locals 3
    .param p1, "errno"    # I

    .prologue
    .line 119
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    .line 120
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenObjectBase("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 124
    return-void
.end method


# virtual methods
.method public attachFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1004
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_attachFile(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1005
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 1007
    :cond_0
    return-void
.end method

.method public clearChangedFlag()V
    .locals 0

    .prologue
    .line 979
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_clearChangedFlag()V

    .line 980
    return-void
.end method

.method public copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2
    .param p1, "source"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 1068
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    .line 1069
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1070
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 1072
    :cond_0
    return-void
.end method

.method public detachFile()V
    .locals 1

    .prologue
    .line 1016
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_detachFile()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1017
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 1019
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1082
    instance-of v0, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    if-eqz v0, :cond_0

    .line 1083
    iget v0, p0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->mHandle:I

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .end local p1    # "o":Ljava/lang/Object;
    iget v1, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->mHandle:I

    if-ne v0, v1, :cond_0

    .line 1084
    const/4 v0, 0x1

    .line 1087
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 101
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;

    .line 103
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_finalize()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 107
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->mHandle:I

    .line 108
    return-void

    .line 104
    :catchall_0
    move-exception v0

    .line 105
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 106
    throw v0
.end method

.method public getAttachedFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1029
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getAttachedFile()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDrawnRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1052
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getDrawnRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataByteArray(Ljava/lang/String;)[B
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 941
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getExtraDataByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataInt(Ljava/lang/String;)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 809
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getExtraDataInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getExtraDataString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 747
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getExtraDataString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 875
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMaxHeight()F
    .locals 1

    .prologue
    .line 346
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getMaxHeight()F

    move-result v0

    return v0
.end method

.method public getMaxWidth()F
    .locals 1

    .prologue
    .line 332
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getMaxWidth()F

    move-result v0

    return v0
.end method

.method public getMinHeight()F
    .locals 1

    .prologue
    .line 292
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getMinHeight()F

    move-result v0

    return v0
.end method

.method public getMinWidth()F
    .locals 1

    .prologue
    .line 281
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getMinWidth()F

    move-result v0

    return v0
.end method

.method public getRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 164
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getResizeOption()I
    .locals 1

    .prologue
    .line 244
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getResizeOption()I

    move-result v0

    return v0
.end method

.method public getRotation()F
    .locals 1

    .prologue
    .line 370
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getRotation()F

    move-result v0

    return v0
.end method

.method public getRuntimeHandle()I
    .locals 1

    .prologue
    .line 1039
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getRuntimeHandle()I

    move-result v0

    return v0
.end method

.method public getSorDataInt(Ljava/lang/String;)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 659
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getSorDataInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getSorDataString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 626
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getSorDataString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSorInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 536
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getSorInfo()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSorPackageLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 561
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getSorPackageLink()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTemplateProperty()Z
    .locals 1

    .prologue
    .line 571
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getTemplateProperty()Z

    move-result v0

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getType()I

    move-result v0

    return v0
.end method

.method public getUserId()I
    .locals 1

    .prologue
    .line 593
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_getUserId()I

    move-result v0

    return v0
.end method

.method public hasExtraDataByteArray(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 953
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_hasExtraDataByteArray(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataInt(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 821
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_hasExtraDataInt(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataString(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 759
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_hasExtraDataString(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataStringArray(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 887
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_hasExtraDataStringArray(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasSorDataInt(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 684
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_hasSorDataInt(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasSorDataString(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 671
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_hasSorDataString(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1098
    iget v0, p0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->mHandle:I

    return v0
.end method

.method public isChanged()Z
    .locals 1

    .prologue
    .line 991
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_isChanged()Z

    move-result v0

    return v0
.end method

.method public isFlipEnabled()Z
    .locals 1

    .prologue
    .line 512
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_isFlipEnabled()Z

    move-result v0

    return v0
.end method

.method public isMovable()Z
    .locals 1

    .prologue
    .line 483
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_isMovable()Z

    move-result v0

    return v0
.end method

.method public isOutOfViewEnabled()Z
    .locals 1

    .prologue
    .line 427
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_isClippable()Z

    move-result v0

    return v0
.end method

.method public isRecorded()Z
    .locals 1

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_isRecorded()Z

    move-result v0

    return v0
.end method

.method public isRotatable()Z
    .locals 1

    .prologue
    .line 397
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_isRotatable()Z

    move-result v0

    return v0
.end method

.method public isSelectable()Z
    .locals 1

    .prologue
    .line 455
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_isSelectable()Z

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_isVisible()Z

    move-result v0

    return v0
.end method

.method public removeExtraDataByteArray(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 965
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_removeExtraDataByteArray(Ljava/lang/String;)Z

    move-result v0

    .line 966
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 967
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 969
    :cond_0
    return-void
.end method

.method public removeExtraDataInt(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 833
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_removeExtraDataInt(Ljava/lang/String;)Z

    move-result v0

    .line 834
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 835
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 837
    :cond_0
    return-void
.end method

.method public removeExtraDataString(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 772
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_removeExtraDataString(Ljava/lang/String;)Z

    move-result v0

    .line 773
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 774
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 776
    :cond_0
    return-void
.end method

.method public removeExtraDataStringArray(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 899
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_removeExtraDataStringArray(Ljava/lang/String;)Z

    move-result v0

    .line 900
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 901
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 903
    :cond_0
    return-void
.end method

.method public removeSorDataInt(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 711
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_removeSorDataInt(Ljava/lang/String;)Z

    move-result v0

    .line 712
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 713
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 715
    :cond_0
    return-void
.end method

.method public removeSorDataString(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 696
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_removeSorDataString(Ljava/lang/String;)Z

    move-result v0

    .line 697
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 698
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 700
    :cond_0
    return-void
.end method

.method public setExtraDataByteArray(Ljava/lang/String;[B)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 921
    if-nez p2, :cond_1

    .line 922
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setExtraDataByteArray(Ljava/lang/String;[BI)Z

    move-result v0

    .line 927
    .local v0, "rnt":Z
    :goto_0
    if-nez v0, :cond_0

    .line 928
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 930
    :cond_0
    return-void

    .line 924
    .end local v0    # "rnt":Z
    :cond_1
    array-length v1, p2

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setExtraDataByteArray(Ljava/lang/String;[BI)Z

    move-result v0

    .restart local v0    # "rnt":Z
    goto :goto_0
.end method

.method public setExtraDataInt(Ljava/lang/String;I)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 793
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setExtraDataInt(Ljava/lang/String;I)Z

    move-result v0

    .line 794
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 795
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 797
    :cond_0
    return-void
.end method

.method public setExtraDataString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 731
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setExtraDataString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 732
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 733
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 735
    :cond_0
    return-void
.end method

.method public setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [Ljava/lang/String;

    .prologue
    .line 854
    if-nez p2, :cond_1

    .line 855
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z

    move-result v0

    .line 860
    .local v0, "rnt":Z
    :goto_0
    if-nez v0, :cond_0

    .line 861
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 863
    :cond_0
    return-void

    .line 857
    .end local v0    # "rnt":Z
    :cond_1
    array-length v1, p2

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z

    move-result v0

    .restart local v0    # "rnt":Z
    goto :goto_0
.end method

.method public setFlipEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 498
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setFlipEnabled(Z)Z

    move-result v0

    .line 499
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 500
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 502
    :cond_0
    return-void
.end method

.method public setMaxSize(FF)V
    .locals 2
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    .line 315
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setMaxSize(FF)Z

    move-result v0

    .line 316
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 317
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 319
    :cond_0
    return-void
.end method

.method public setMinSize(FF)V
    .locals 2
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    .line 267
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setMinSize(FF)Z

    move-result v0

    .line 268
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 269
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 271
    :cond_0
    return-void
.end method

.method public setMovable(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 469
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_enableMovement(Z)Z

    move-result v0

    .line 470
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 471
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 473
    :cond_0
    return-void
.end method

.method public setOutOfViewEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 412
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_enableClip(Z)Z

    move-result v0

    .line 413
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 414
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 416
    :cond_0
    return-void
.end method

.method public setRecorded(Z)V
    .locals 2
    .param p1, "record"    # Z

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setRecorded(Z)Z

    move-result v0

    .line 178
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 179
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 181
    :cond_0
    return-void
.end method

.method public setRect(Landroid/graphics/RectF;Z)V
    .locals 2
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "regionOnly"    # Z

    .prologue
    .line 151
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setRect(Landroid/graphics/RectF;Z)Z

    move-result v0

    .line 152
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 153
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 155
    :cond_0
    return-void
.end method

.method public setResizeOption(I)V
    .locals 2
    .param p1, "option"    # I

    .prologue
    .line 231
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setResizeOption(I)Z

    move-result v0

    .line 232
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 233
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 235
    :cond_0
    return-void
.end method

.method public setRotatable(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 384
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_enableRotation(Z)Z

    move-result v0

    .line 385
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 386
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 388
    :cond_0
    return-void
.end method

.method public setRotation(F)V
    .locals 2
    .param p1, "degree"    # F

    .prologue
    .line 357
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setRotation(F)Z

    move-result v0

    .line 358
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 359
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 361
    :cond_0
    return-void
.end method

.method public setSelectable(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 441
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_enableSelection(Z)Z

    move-result v0

    .line 442
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 443
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 445
    :cond_0
    return-void
.end method

.method public setSorDataInt(Ljava/lang/String;I)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 643
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setSorDataInt(Ljava/lang/String;I)Z

    move-result v0

    .line 644
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 645
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 647
    :cond_0
    return-void
.end method

.method public setSorDataString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 610
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setSorDataString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 611
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 612
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 614
    :cond_0
    return-void
.end method

.method public setSorInfo(Ljava/lang/String;)V
    .locals 2
    .param p1, "info"    # Ljava/lang/String;

    .prologue
    .line 523
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setSorInfo(Ljava/lang/String;)Z

    move-result v0

    .line 524
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 525
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 527
    :cond_0
    return-void
.end method

.method public setSorPackageLink(Ljava/lang/String;)V
    .locals 2
    .param p1, "link"    # Ljava/lang/String;

    .prologue
    .line 547
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setSorPackageLink(Ljava/lang/String;)Z

    move-result v0

    .line 548
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 549
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 551
    :cond_0
    return-void
.end method

.method public setUserId(I)Z
    .locals 1
    .param p1, "userId"    # I

    .prologue
    .line 583
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setUserId(I)Z

    move-result v0

    return v0
.end method

.method public setVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 201
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->ObjectBase_setVisibility(Z)Z

    move-result v0

    .line 202
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 203
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->throwUncheckedException(I)V

    .line 205
    :cond_0
    return-void
.end method
