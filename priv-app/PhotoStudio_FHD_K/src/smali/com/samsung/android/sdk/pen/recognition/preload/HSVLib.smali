.class public Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;
.super Ljava/lang/Object;
.source "HSVLib.java"


# static fields
.field private static final SHOW_LOG:Z = true

.field public static final SIGNATURE_REGISTER_ERROR_DIFFERENT:I = -0x3

.field public static final SIGNATURE_REGISTER_ERROR_NORMAL:I = -0x1

.field public static final SIGNATURE_REGISTER_ERROR_SHORT:I = -0x2

.field public static final SIGNATURE_VERIFICATION_LEVEL_HIGH:I = 0x2

.field public static final SIGNATURE_VERIFICATION_LEVEL_LOW:I = 0x0

.field public static final SIGNATURE_VERIFICATION_LEVEL_MEDIUM:I = 0x1

.field public static final SIGNATURE_VERIFICATION_SCORE_DEFAULT:I = 0x1f4

.field public static final SIGNATURE_VERIFICATION_SCORE_HIGH:I = 0x258

.field public static final SIGNATURE_VERIFICATION_SCORE_LOW:I = 0x190

.field public static final SIGNATURE_VERIFICATION_SCORE_MEDIUM:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "Signature"

.field private static final USER_ID_DEFAULT:I = 0xa


# instance fields
.field private m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

.field private m_bRegistered:Z

.field private m_cTotalTag:[C

.field private m_nGENUINE_SCORE_THR:I

.field private m_nMaxNumRegistration:I

.field private m_nMaxPointCount:I

.field private m_nMinXYSize:I

.field private m_nNumRegistration:I

.field private m_nTotalPosition:[I

.field private m_nTotalPressure:[F

.field private m_nTotalTimeStamp:[I

.field private m_nUserID:I

.field private m_sCurrentPointCount:S

.field private m_sVerificationScore:S

.field private m_szSaveDirPath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    .line 32
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    .line 33
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    .line 34
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    .line 36
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    .line 37
    const/16 v0, 0x400

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    .line 38
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxNumRegistration:I

    .line 39
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nGENUINE_SCORE_THR:I

    .line 40
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    .line 41
    const/16 v0, 0xc8

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMinXYSize:I

    .line 43
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    .line 45
    iput-short v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    .line 46
    iput v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    .line 47
    iput-short v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sVerificationScore:S

    .line 48
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    .line 58
    return-void
.end method

.method private checkTrainingData(Ljava/lang/String;)Z
    .locals 5
    .param p1, "i_szPath"    # Ljava/lang/String;

    .prologue
    .line 201
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "SPenHSVreg00.dat"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 202
    .local v0, "tFile0":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "SPenHSVreg01.dat"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 203
    .local v1, "tFile1":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "SPenHSVreg02.dat"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 205
    .local v2, "tFile2":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 206
    const/4 v3, 0x1

    .line 208
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private deleteFolder(Ljava/lang/String;)Z
    .locals 6
    .param p1, "i_szPath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 173
    new-instance v0, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "SPenHSVreg00.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 174
    .local v0, "tFile0":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "SPenHSVreg01.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 175
    .local v1, "tFile1":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "SPenHSVreg02.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 177
    .local v2, "tFile2":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 178
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v4

    if-nez v4, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v3

    .line 183
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 184
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 189
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 190
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 195
    :cond_3
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private makeFolder(Ljava/lang/String;)Z
    .locals 3
    .param p1, "i_szPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 159
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 160
    .local v0, "tFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 161
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 167
    :cond_0
    :goto_0
    return v1

    .line 164
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cancel(I)Z
    .locals 4
    .param p1, "i_nUserID"    # I

    .prologue
    const/4 v0, 0x0

    .line 442
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->clearSignatureScreen()Z

    .line 443
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    if-eqz v1, :cond_1

    .line 444
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->checkRegistration(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 445
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->clearSignatureScreen()Z

    .line 446
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVDelUser(I)S

    .line 447
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVClose()S

    .line 448
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    .line 449
    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    .line 450
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->deleteFolder(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 466
    :goto_0
    return v0

    .line 455
    :cond_0
    const-string v0, "Signature"

    const-string v1, "Cencel"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    const-string v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "m_bRegistered = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    const-string v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "m_nNumRegistration = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    const/4 v0, 0x1

    goto :goto_0

    .line 462
    :cond_1
    const-string v1, "Signature"

    const-string v2, "Cencel false"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    const-string v1, "Signature"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "m_bRegistered = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    const-string v1, "Signature"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "m_nNumRegistration = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public checkRegistration(I)Z
    .locals 2
    .param p1, "i_nUserID"    # I

    .prologue
    const/4 v0, 0x0

    .line 217
    const/16 v1, 0xa

    if-ne p1, v1, :cond_0

    .line 218
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->checkTrainingData(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    const/4 v0, 0x1

    .line 224
    :cond_0
    return v0
.end method

.method public clearSignatureScreen()Z
    .locals 2

    .prologue
    .line 430
    const/4 v0, 0x0

    iput-short v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    .line 432
    const-string v0, "Signature"

    const-string v1, "Retry"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const/4 v0, 0x1

    return v0
.end method

.method public deleteRegistration(I)Z
    .locals 2
    .param p1, "i_nUserID"    # I

    .prologue
    const/4 v0, 0x0

    .line 263
    const/16 v1, 0xa

    if-ne p1, v1, :cond_0

    .line 264
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->deleteFolder(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 265
    const/4 v0, 0x1

    .line 270
    :cond_0
    return v0
.end method

.method public getRegisteredDataCount(I)I
    .locals 6
    .param p1, "i_nUserID"    # I

    .prologue
    .line 234
    const/4 v0, 0x0

    .line 236
    .local v0, "count":I
    const/16 v4, 0xa

    if-ne p1, v4, :cond_2

    .line 237
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "SPenHSVreg00.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 238
    .local v1, "tFile0":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "SPenHSVreg01.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 239
    .local v2, "tFile1":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "SPenHSVreg02.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 241
    .local v3, "tFile2":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 242
    add-int/lit8 v0, v0, 0x1

    .line 245
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 246
    add-int/lit8 v0, v0, 0x1

    .line 249
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 250
    add-int/lit8 v0, v0, 0x1

    .line 255
    .end local v1    # "tFile0":Ljava/io/File;
    .end local v2    # "tFile1":Ljava/io/File;
    .end local v3    # "tFile2":Ljava/io/File;
    :cond_2
    return v0
.end method

.method public initHSV(Ljava/lang/String;IIII)Z
    .locals 4
    .param p1, "i_szSaveDirPath"    # Ljava/lang/String;
    .param p2, "i_nMaxPointCount"    # I
    .param p3, "i_nMaxNumRegistration"    # I
    .param p4, "i_nUserID"    # I
    .param p5, "i_nMinXYSize"    # I

    .prologue
    const/4 v1, 0x0

    .line 67
    const-string v2, "Signature"

    const-string v3, "InitHSV"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    if-nez p1, :cond_0

    .line 72
    const-string v2, "Signature"

    const-string v3, "save directory path is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :goto_0
    return v1

    .line 77
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    .line 78
    iput p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    .line 79
    iput p3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxNumRegistration:I

    .line 80
    const/16 v2, 0x1f4

    iput v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nGENUINE_SCORE_THR:I

    .line 81
    iput p4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    .line 82
    iput p5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMinXYSize:I

    .line 84
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->makeFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 86
    const-string v2, "Signature"

    const-string v3, "make folder success"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :try_start_0
    new-instance v2, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    .line 98
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVInit(Ljava/lang/String;)S
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    mul-int/lit8 v2, v2, 0x2

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    .line 111
    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    .line 112
    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    new-array v2, v2, [C

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    .line 113
    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    new-array v2, v2, [F

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    .line 115
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVDelUser(I)S

    move-result v2

    if-gez v2, :cond_2

    .line 117
    const-string v2, "Signature"

    const-string v3, "delete user failure"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 90
    :cond_1
    const-string v2, "Signature"

    const-string v3, "make folder failure"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 102
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    .line 104
    const-string v2, "Signature"

    const-string v3, "create HSVJniLib failure"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 122
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setDrawData([Landroid/graphics/PointF;[I[F)V
    .locals 5
    .param p1, "i_Points"    # [Landroid/graphics/PointF;
    .param p2, "i_Time"    # [I
    .param p3, "i_Pressure"    # [F

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 281
    iget-short v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    array-length v2, p2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    if-lt v1, v2, :cond_0

    .line 282
    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    int-to-short v1, v1

    iput-short v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    .line 301
    :goto_0
    return-void

    .line 285
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v1, p2

    if-lt v0, v1, :cond_1

    .line 298
    iget-short v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    array-length v2, p2

    add-int/2addr v1, v2

    int-to-short v1, v1

    iput-short v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    goto :goto_0

    .line 286
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-short v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    mul-int/lit8 v2, v2, 0x2

    mul-int/lit8 v3, v0, 0x2

    add-int/2addr v2, v3

    aget-object v3, p1, v0

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    float-to-int v3, v3

    aput v3, v1, v2

    .line 287
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-short v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    mul-int/lit8 v2, v2, 0x2

    mul-int/lit8 v3, v0, 0x2

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    aget-object v3, p1, v0

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v4

    float-to-int v3, v3

    aput v3, v1, v2

    .line 289
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iget-short v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    add-int/2addr v2, v0

    aget v3, p2, v0

    aput v3, v1, v2

    .line 290
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iget-short v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    add-int/2addr v2, v0

    aget v3, p3, v0

    aput v3, v1, v2

    .line 292
    if-nez v0, :cond_2

    .line 293
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    iget-short v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    add-int/2addr v2, v0

    const/4 v3, 0x0

    aput-char v3, v1, v2

    .line 285
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 295
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    iget-short v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    add-int/2addr v2, v0

    const/4 v3, 0x1

    aput-char v3, v1, v2

    goto :goto_2
.end method

.method public setGenuineScoreThr(I)V
    .locals 0
    .param p1, "i_nGenuineScoreThr"    # I

    .prologue
    .line 138
    iput p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nGENUINE_SCORE_THR:I

    .line 139
    return-void
.end method

.method public setMaxNumRegistration(I)V
    .locals 0
    .param p1, "i_nMaxNumRegistration"    # I

    .prologue
    .line 130
    iput p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxNumRegistration:I

    .line 131
    return-void
.end method

.method public setMinXYSize(I)V
    .locals 0
    .param p1, "i_nMinXYSize"    # I

    .prologue
    .line 154
    iput p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMinXYSize:I

    .line 155
    return-void
.end method

.method public setUserID(I)V
    .locals 0
    .param p1, "i_nUserID"    # I

    .prologue
    .line 146
    iput p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    .line 147
    return-void
.end method

.method public signatureTraining(I)I
    .locals 12
    .param p1, "i_nUserID"    # I

    .prologue
    const/4 v10, -0x1

    const/4 v11, 0x0

    .line 309
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    if-nez v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVDelUser(I)S

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    .line 314
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iget-short v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    iget v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    iget v8, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMinXYSize:I

    .line 313
    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVCheckSignature([I[I[C[FSIII)S

    move-result v9

    .line 316
    .local v9, "nCheckSign":S
    if-eqz v9, :cond_1

    .line 317
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->clearSignatureScreen()Z

    move v0, v10

    .line 367
    :goto_0
    return v0

    .line 323
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxNumRegistration:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_2

    .line 324
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    .line 325
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iget-short v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    iget v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    .line 324
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVAddSignatureModel([I[I[C[FSII)S

    .line 326
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    .line 327
    iput v11, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    .line 328
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->clearSignatureScreen()Z

    .line 330
    const-string v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "m_bRegistered = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    const-string v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "m_nNumRegistration = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxNumRegistration:I

    goto :goto_0

    .line 334
    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    if-nez v0, :cond_5

    .line 336
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->deleteFolder(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v10

    .line 337
    goto :goto_0

    .line 340
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->makeFolder(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 341
    const-string v0, "Signature"

    const-string v1, "make folder success"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    .line 347
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iget-short v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    iget v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    .line 346
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVAddSignatureModel([I[I[C[FSII)S

    .line 348
    iput-boolean v11, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    .line 349
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    .line 350
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->clearSignatureScreen()Z

    .line 352
    const-string v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "m_bRegistered = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    const-string v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "m_nNumRegistration = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    goto/16 :goto_0

    .line 343
    :cond_4
    const-string v0, "Signature"

    const-string v1, "make folder failure or existed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 358
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    .line 359
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iget-short v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    iget v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    .line 358
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVAddSignatureModel([I[I[C[FSII)S

    .line 360
    iput-boolean v11, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    .line 361
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    .line 362
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->clearSignatureScreen()Z

    .line 364
    const-string v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "m_bRegistered = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    const-string v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "m_nNumRegistration = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    goto/16 :goto_0
.end method

.method public verification(II)Z
    .locals 10
    .param p1, "i_nUserID"    # I
    .param p2, "verificationLevel"    # I

    .prologue
    const/4 v9, 0x0

    .line 378
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->checkRegistration(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    const-string v0, "Signature"

    const-string v1, "StartVerification"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVDelUser(I)S

    .line 384
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVLoadSignatureModel()S

    .line 386
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    .line 387
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iget-short v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    iget v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nGENUINE_SCORE_THR:I

    .line 386
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVVerify([I[I[C[FSII)S

    move-result v0

    iput-short v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sVerificationScore:S

    .line 390
    packed-switch p2, :pswitch_data_0

    .line 401
    const/16 v8, 0x1f4

    .line 405
    .local v8, "thresholdScore":I
    :goto_0
    iget-short v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sVerificationScore:S

    if-lt v0, v8, :cond_0

    .line 407
    const-string v0, "Signature"

    const-string v1, "Verification : Success"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->cancel(I)Z

    .line 410
    const/4 v0, 0x1

    .line 420
    .end local v8    # "thresholdScore":I
    :goto_1
    return v0

    .line 392
    :pswitch_0
    const/16 v8, 0x190

    .line 393
    .restart local v8    # "thresholdScore":I
    goto :goto_0

    .line 395
    .end local v8    # "thresholdScore":I
    :pswitch_1
    const/16 v8, 0x1f4

    .line 396
    .restart local v8    # "thresholdScore":I
    goto :goto_0

    .line 398
    .end local v8    # "thresholdScore":I
    :pswitch_2
    const/16 v8, 0x258

    .line 399
    .restart local v8    # "thresholdScore":I
    goto :goto_0

    .line 413
    :cond_0
    const-string v0, "Signature"

    const-string v1, "Verification : Failure"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->cancel(I)Z

    move v0, v9

    .line 416
    goto :goto_1

    .line 419
    .end local v8    # "thresholdScore":I
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->cancel(I)Z

    move v0, v9

    .line 420
    goto :goto_1

    .line 390
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
