.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;
.super Ljava/lang/Object;
.source "SpenSettingRemoverLayout.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    .line 315
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v5, :cond_0

    .line 316
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v5, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 387
    :goto_0
    return v7

    .line 321
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    float-to-int v3, v5

    .line 322
    .local v3, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    float-to-int v4, v5

    .line 324
    .local v4, "y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 386
    :cond_1
    :goto_1
    :pswitch_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v5, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 331
    :pswitch_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-boolean v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    if-eqz v5, :cond_1

    .line 336
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mXDelta:I

    sub-int v1, v3, v5

    .line 337
    .local v1, "mMoveX":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mYDelta:I

    sub-int v2, v4, v5

    .line 339
    .local v2, "mMoveY":I
    if-gez v1, :cond_2

    .line 340
    const/4 v1, 0x0

    .line 342
    :cond_2
    if-gez v2, :cond_3

    .line 343
    const/4 v2, 0x0

    .line 346
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x2

    if-le v1, v5, :cond_4

    .line 347
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    add-int/lit8 v1, v5, -0x2

    .line 350
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    if-le v2, v5, :cond_5

    .line 351
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getHeight()I

    move-result v6

    sub-int v2, v5, v6

    .line 354
    :cond_5
    if-gez v1, :cond_6

    .line 355
    const/4 v1, 0x0

    .line 357
    :cond_6
    if-gez v2, :cond_7

    .line 358
    const/4 v2, 0x0

    .line 361
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 363
    .local v0, "Params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 364
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 366
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 373
    .end local v0    # "Params":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v1    # "mMoveX":I
    .end local v2    # "mMoveY":I
    :pswitch_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-boolean v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    if-eqz v5, :cond_1

    .line 374
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    if-eqz v5, :cond_8

    .line 376
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    invoke-interface {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;->onMoved()V

    .line 379
    :cond_8
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMovableRect()Landroid/graphics/Rect;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 380
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLocationOnScreen([I)V

    goto/16 :goto_1

    .line 324
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
