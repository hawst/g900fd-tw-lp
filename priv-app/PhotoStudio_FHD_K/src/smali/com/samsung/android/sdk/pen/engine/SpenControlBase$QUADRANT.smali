.class final enum Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;
.super Ljava/lang/Enum;
.source "SpenControlBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "QUADRANT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

.field public static final enum QUADRANT_1:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

.field public static final enum QUADRANT_2:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

.field public static final enum QUADRANT_3:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

.field public static final enum QUADRANT_4:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

.field public static final enum QUADRANT_MAX:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    const-string v1, "QUADRANT_1"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_1:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    .line 63
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    const-string v1, "QUADRANT_2"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_2:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    .line 64
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    const-string v1, "QUADRANT_3"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_3:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    .line 65
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    const-string v1, "QUADRANT_4"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_4:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    .line 66
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    const-string v1, "QUADRANT_MAX"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_MAX:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    .line 61
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_1:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_2:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_3:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_4:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_MAX:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ENUM$VALUES:[Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ENUM$VALUES:[Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
