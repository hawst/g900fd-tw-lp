.class public final Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
.super Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.source "SpenObjectImage.java"


# static fields
.field public static final BORDER_TYPE_DOT:I = 0x3

.field public static final BORDER_TYPE_IMAGE:I = 0x4

.field public static final BORDER_TYPE_NONE:I = 0x0

.field public static final BORDER_TYPE_SHADOW:I = 0x2

.field public static final BORDER_TYPE_SQUARE:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 70
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2
    .param p1, "isTemplateObject"    # Z

    .prologue
    .line 80
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 81
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_init(Z)Z

    move-result v0

    .line 82
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 83
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 85
    :cond_0
    return-void
.end method

.method private native Bitmap_saveImageTest()V
.end method

.method private native ObjectImage_clearChangedFlag()V
.end method

.method private native ObjectImage_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native ObjectImage_getBorderType()I
.end method

.method private native ObjectImage_getCropRect()Landroid/graphics/Rect;
.end method

.method private native ObjectImage_getDrawnRect()Landroid/graphics/RectF;
.end method

.method private native ObjectImage_getHintText()Ljava/lang/String;
.end method

.method private native ObjectImage_getHintTextColor()I
.end method

.method private native ObjectImage_getHintTextFontSize()F
.end method

.method private native ObjectImage_getHintTextVerticalOffset()F
.end method

.method private native ObjectImage_getImage()Landroid/graphics/Bitmap;
.end method

.method private native ObjectImage_getImageBorder()Landroid/graphics/Bitmap;
.end method

.method private native ObjectImage_getImageBorderBottomWidth()F
.end method

.method private native ObjectImage_getImageBorderLeftWidth()F
.end method

.method private native ObjectImage_getImageBorderNinePatchRect()Landroid/graphics/Rect;
.end method

.method private native ObjectImage_getImageBorderPath()Ljava/lang/String;
.end method

.method private native ObjectImage_getImageBorderRightWidth()F
.end method

.method private native ObjectImage_getImageBorderTopWidth()F
.end method

.method private native ObjectImage_getImagePath()Ljava/lang/String;
.end method

.method private native ObjectImage_getLineBorderColor()I
.end method

.method private native ObjectImage_getLineBorderWidth()F
.end method

.method private native ObjectImage_getNinePatchRect()Landroid/graphics/Rect;
.end method

.method private native ObjectImage_getTransparency()Z
.end method

.method private native ObjectImage_init(Z)Z
.end method

.method private native ObjectImage_isChanged()Z
.end method

.method private native ObjectImage_isHintTextVisiable()Z
.end method

.method private native ObjectImage_setBorderType(I)Z
.end method

.method private native ObjectImage_setCropRect(Landroid/graphics/Rect;)Z
.end method

.method private native ObjectImage_setHintText(Ljava/lang/String;)Z
.end method

.method private native ObjectImage_setHintTextColor(I)Z
.end method

.method private native ObjectImage_setHintTextFontSize(F)Z
.end method

.method private native ObjectImage_setHintTextVerticalOffset(F)Z
.end method

.method private native ObjectImage_setHintTextVisibility(Z)Z
.end method

.method private native ObjectImage_setImage(Landroid/graphics/Bitmap;)Z
.end method

.method private native ObjectImage_setImage2(Ljava/lang/String;)Z
.end method

.method private native ObjectImage_setImage3(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Z
.end method

.method private native ObjectImage_setImage4(Ljava/lang/String;Landroid/graphics/Rect;)Z
.end method

.method private native ObjectImage_setImageBorder(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Z
.end method

.method private native ObjectImage_setImageBorderWidth(FFFF)Z
.end method

.method private native ObjectImage_setLineBorderColor(I)Z
.end method

.method private native ObjectImage_setLineBorderWidth(F)Z
.end method

.method private native ObjectImage_setTransparency(Z)Z
.end method

.method private throwUncheckedException(I)V
    .locals 3
    .param p1, "errno"    # I

    .prologue
    .line 661
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    .line 662
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenObjectImage("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 664
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 666
    return-void
.end method


# virtual methods
.method public clearChangedFlag()V
    .locals 0

    .prologue
    .line 623
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_clearChangedFlag()V

    .line 624
    return-void
.end method

.method public copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2
    .param p1, "source"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 654
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    .line 655
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 656
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 658
    :cond_0
    return-void
.end method

.method public getBorderType()I
    .locals 1

    .prologue
    .line 458
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getBorderType()I

    move-result v0

    return v0
.end method

.method public getCropRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 312
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getCropRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getDrawnRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 639
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getDrawnRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getHintText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 482
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getHintText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHintTextColor()I
    .locals 1

    .prologue
    .line 563
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getHintTextColor()I

    move-result v0

    return v0
.end method

.method public getHintTextFontSize()F
    .locals 1

    .prologue
    .line 589
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getHintTextFontSize()F

    move-result v0

    return v0
.end method

.method public getHintTextVerticalOffset()F
    .locals 1

    .prologue
    .line 615
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getHintTextVerticalOffset()F

    move-result v0

    return v0
.end method

.method public getImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 198
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImage()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getImageBorder()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 256
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderPath()Ljava/lang/String;

    move-result-object v1

    .line 257
    .local v1, "path":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 258
    const/4 v0, 0x0

    .line 261
    :goto_0
    return-object v0

    .line 260
    :cond_0
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 261
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method public getImageBorderBottomWidth()F
    .locals 1

    .prologue
    .line 429
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderBottomWidth()F

    move-result v0

    return v0
.end method

.method public getImageBorderLeftWidth()F
    .locals 1

    .prologue
    .line 396
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderLeftWidth()F

    move-result v0

    return v0
.end method

.method public getImageBorderNinePatchRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 285
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderNinePatchRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getImageBorderPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImageBorderRightWidth()F
    .locals 1

    .prologue
    .line 418
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderRightWidth()F

    move-result v0

    return v0
.end method

.method public getImageBorderTopWidth()F
    .locals 1

    .prologue
    .line 407
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderTopWidth()F

    move-result v0

    return v0
.end method

.method public getImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImagePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLineBorderColor()I
    .locals 1

    .prologue
    .line 336
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getLineBorderColor()I

    move-result v0

    return v0
.end method

.method public getLineBorderWidth()F
    .locals 1

    .prologue
    .line 363
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getLineBorderWidth()F

    move-result v0

    return v0
.end method

.method public getNinePatchRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 188
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getNinePatchRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getTransparency()Z
    .locals 1

    .prologue
    .line 539
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getTransparency()Z

    move-result v0

    return v0
.end method

.method public isChanged()Z
    .locals 1

    .prologue
    .line 631
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_isChanged()Z

    move-result v0

    return v0
.end method

.method public isHintTextEnabled()Z
    .locals 1

    .prologue
    .line 506
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_isHintTextVisiable()Z

    move-result v0

    return v0
.end method

.method public setBorderType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 443
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setBorderType(I)Z

    move-result v0

    .line 444
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 445
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 448
    :cond_0
    return-void
.end method

.method public setCropRect(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "cropRect"    # Landroid/graphics/Rect;

    .prologue
    .line 298
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setCropRect(Landroid/graphics/Rect;)Z

    move-result v0

    .line 299
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 300
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 302
    :cond_0
    return-void
.end method

.method public setHintText(Ljava/lang/String;)V
    .locals 2
    .param p1, "hintText"    # Ljava/lang/String;

    .prologue
    .line 469
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setHintText(Ljava/lang/String;)Z

    move-result v0

    .line 470
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 471
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 473
    :cond_0
    return-void
.end method

.method public setHintTextColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 550
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setHintTextColor(I)Z

    move-result v0

    .line 551
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 552
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 554
    :cond_0
    return-void
.end method

.method public setHintTextEnabled(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 493
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setHintTextVisibility(Z)Z

    move-result v0

    .line 494
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 495
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 497
    :cond_0
    return-void
.end method

.method public setHintTextFontSize(F)V
    .locals 2
    .param p1, "size"    # F

    .prologue
    .line 576
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setHintTextFontSize(F)Z

    move-result v0

    .line 577
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 578
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 580
    :cond_0
    return-void
.end method

.method public setHintTextVerticalOffset(F)V
    .locals 2
    .param p1, "offset"    # F

    .prologue
    .line 602
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setHintTextVerticalOffset(F)Z

    move-result v0

    .line 603
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 604
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 606
    :cond_0
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 101
    if-eqz p1, :cond_0

    .line 102
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "image is recyled."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 106
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setImage(Landroid/graphics/Bitmap;)Z

    move-result v0

    .line 107
    .local v0, "rnt":Z
    if-nez v0, :cond_1

    .line 108
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 110
    :cond_1
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "image"    # Landroid/graphics/Bitmap;
    .param p2, "ninePatchRect"    # Landroid/graphics/Rect;

    .prologue
    .line 148
    if-eqz p1, :cond_0

    .line 149
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "image is recyled."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 153
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setImage3(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Z

    move-result v0

    .line 154
    .local v0, "rnt":Z
    if-nez v0, :cond_1

    .line 155
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 157
    :cond_1
    return-void
.end method

.method public setImage(Ljava/lang/String;)V
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setImage2(Ljava/lang/String;)Z

    move-result v0

    .line 125
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 126
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 128
    :cond_0
    return-void
.end method

.method public setImage(Ljava/lang/String;Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "ninePatchRect"    # Landroid/graphics/Rect;

    .prologue
    .line 175
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setImage4(Ljava/lang/String;Landroid/graphics/Rect;)Z

    move-result v0

    .line 176
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 177
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 179
    :cond_0
    return-void
.end method

.method public setImageBorder(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "image"    # Landroid/graphics/Bitmap;
    .param p2, "ninePatchRect"    # Landroid/graphics/Rect;

    .prologue
    .line 238
    if-eqz p1, :cond_0

    .line 239
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "image is recyled."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 243
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setImageBorder(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Z

    move-result v0

    .line 244
    .local v0, "rnt":Z
    if-nez v0, :cond_1

    .line 245
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 247
    :cond_1
    return-void
.end method

.method public setImageBorderWidth(FFFF)V
    .locals 2
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "right"    # F
    .param p4, "bottom"    # F

    .prologue
    .line 382
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setImageBorderWidth(FFFF)Z

    move-result v0

    .line 383
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 384
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 386
    :cond_0
    return-void
.end method

.method public setLineBorderColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 323
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setLineBorderColor(I)Z

    move-result v0

    .line 324
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 325
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 327
    :cond_0
    return-void
.end method

.method public setLineBorderWidth(F)V
    .locals 2
    .param p1, "width"    # F

    .prologue
    .line 350
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setLineBorderWidth(F)Z

    move-result v0

    .line 351
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 352
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 354
    :cond_0
    return-void
.end method

.method public setRect(Landroid/graphics/RectF;Z)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "regionOnly"    # Z

    .prologue
    .line 220
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 221
    return-void
.end method

.method public setTransparency(Z)V
    .locals 2
    .param p1, "transparency"    # Z

    .prologue
    .line 525
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setTransparency(Z)Z

    move-result v0

    .line 526
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 527
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    .line 529
    :cond_0
    return-void
.end method
