.class public Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;
.super Ljava/lang/Object;
.source "HSVJniLib.java"


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const-string v1, "/data/data/com.samsung.android.sdk.spenv10/lib/libSPenHSV.so"

    .line 19
    .local v1, "latestLib":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 20
    .local v2, "libFilePath":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 22
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    :cond_0
    :goto_0
    return-void

    .line 23
    :catch_0
    move-exception v0

    .line 24
    .local v0, "e":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public native jni_HSVAddSignatureModel([I[I[C[FSII)S
.end method

.method public native jni_HSVCheckSignature([I[I[C[FSIII)S
.end method

.method public native jni_HSVClose()S
.end method

.method public native jni_HSVDelUser(I)S
.end method

.method public native jni_HSVInit(Ljava/lang/String;)S
.end method

.method public native jni_HSVLoadSignatureModel()S
.end method

.method public native jni_HSVVerify([I[I[C[FSII)S
.end method
