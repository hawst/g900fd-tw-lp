.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeBeautifier;
.super Ljava/lang/Object;
.source "NRRShapeBeautifier.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$recognitionengine$ShapeType:[I


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$recognitionengine$ShapeType()[I
    .locals 3

    .prologue
    .line 7
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeBeautifier;->$SWITCH_TABLE$com$samsung$recognitionengine$ShapeType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/recognitionengine/ShapeType;->values()[Lcom/samsung/recognitionengine/ShapeType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Angle:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_48

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Arc:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_47

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Arrow:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_46

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_BarChart:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x40

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_45

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ChartAxis:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x2e

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_44

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Chevron:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_43

    :goto_6
    :try_start_6
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Chord:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x38

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_42

    :goto_7
    :try_start_7
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Circle:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_41

    :goto_8
    :try_start_8
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ColumnChart:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x3f

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_40

    :goto_9
    :try_start_9
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ConcaveArrow:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_3f

    :goto_a
    :try_start_a
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ConcaveFlag:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_3e

    :goto_b
    :try_start_b
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Cone:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x3b

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_3d

    :goto_c
    :try_start_c
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Cross:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_3c

    :goto_d
    :try_start_d
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Cube:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x45

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_3b

    :goto_e
    :try_start_e
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_CubicBezierCurve:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_3a

    :goto_f
    :try_start_f
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Cylinder:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x44

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_39

    :goto_10
    :try_start_10
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Diamond:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_38

    :goto_11
    :try_start_11
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_DoubleArrow:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_37

    :goto_12
    :try_start_12
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Ellipse:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_36

    :goto_13
    :try_start_13
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Ellipsoid:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x3d

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_35

    :goto_14
    :try_start_14
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Elongated_Hexagon:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_34

    :goto_15
    :try_start_15
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_FlowchartTerminator:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x48

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_33

    :goto_16
    :try_start_16
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Heart:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x36

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_32

    :goto_17
    :try_start_17
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Heptagon:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_31

    :goto_18
    :try_start_18
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Hexagon:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_30

    :goto_19
    :try_start_19
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Hexagram:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x34

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_2f

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Infinity:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x35

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_2e

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Integral:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x31

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_2d

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Isosceles_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_2c

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LShape:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_2b

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftArrow:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_2a

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftBrace:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x39

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_29

    :goto_20
    :try_start_20
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftRightArrow:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_28

    :goto_21
    :try_start_21
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftSquareBracket:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x46

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_27

    :goto_22
    :try_start_22
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Line:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_26

    :goto_23
    :try_start_23
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LineChart:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x43

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_25

    :goto_24
    :try_start_24
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Moon:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_24

    :goto_25
    :try_start_25
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Parallelogram:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_23

    :goto_26
    :try_start_26
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pentagon:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_22

    :goto_27
    :try_start_27
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pentagon_Equilateral:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_21

    :goto_28
    :try_start_28
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pentagram:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x33

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_20

    :goto_29
    :try_start_29
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pie:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x37

    aput v2, v0, v1
    :try_end_29
    .catch Ljava/lang/NoSuchFieldError; {:try_start_29 .. :try_end_29} :catch_1f

    :goto_2a
    :try_start_2a
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_PieChart:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x2d

    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_2a} :catch_1e

    :goto_2b
    :try_start_2b
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_PyramidChartHorizontal:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x42

    aput v2, v0, v1
    :try_end_2b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2b .. :try_end_2b} :catch_1d

    :goto_2c
    :try_start_2c
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_PyramidChartVertical:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x41

    aput v2, v0, v1
    :try_end_2c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2c .. :try_end_2c} :catch_1c

    :goto_2d
    :try_start_2d
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Radical:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x2f

    aput v2, v0, v1
    :try_end_2d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2d .. :try_end_2d} :catch_1b

    :goto_2e
    :try_start_2e
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Rect:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2e .. :try_end_2e} :catch_1a

    :goto_2f
    :try_start_2f
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_RightAngled_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_2f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2f .. :try_end_2f} :catch_19

    :goto_30
    :try_start_30
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_RightBrace:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x3a

    aput v2, v0, v1
    :try_end_30
    .catch Ljava/lang/NoSuchFieldError; {:try_start_30 .. :try_end_30} :catch_18

    :goto_31
    :try_start_31
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_RightSquareBracket:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x47

    aput v2, v0, v1
    :try_end_31
    .catch Ljava/lang/NoSuchFieldError; {:try_start_31 .. :try_end_31} :catch_17

    :goto_32
    :try_start_32
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SandGlass:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_32
    .catch Ljava/lang/NoSuchFieldError; {:try_start_32 .. :try_end_32} :catch_16

    :goto_33
    :try_start_33
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ShapesCount:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x49

    aput v2, v0, v1
    :try_end_33
    .catch Ljava/lang/NoSuchFieldError; {:try_start_33 .. :try_end_33} :catch_15

    :goto_34
    :try_start_34
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Sigma:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x32

    aput v2, v0, v1
    :try_end_34
    .catch Ljava/lang/NoSuchFieldError; {:try_start_34 .. :try_end_34} :catch_14

    :goto_35
    :try_start_35
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileConfused:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x2c

    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_35} :catch_13

    :goto_36
    :try_start_36
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileDisappointed:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x2a

    aput v2, v0, v1
    :try_end_36
    .catch Ljava/lang/NoSuchFieldError; {:try_start_36 .. :try_end_36} :catch_12

    :goto_37
    :try_start_37
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileHappy:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_37
    .catch Ljava/lang/NoSuchFieldError; {:try_start_37 .. :try_end_37} :catch_11

    :goto_38
    :try_start_38
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileSad:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_38
    .catch Ljava/lang/NoSuchFieldError; {:try_start_38 .. :try_end_38} :catch_10

    :goto_39
    :try_start_39
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileSurprised:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x2b

    aput v2, v0, v1
    :try_end_39
    .catch Ljava/lang/NoSuchFieldError; {:try_start_39 .. :try_end_39} :catch_f

    :goto_3a
    :try_start_3a
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Sphere:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x3c

    aput v2, v0, v1
    :try_end_3a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3a .. :try_end_3a} :catch_e

    :goto_3b
    :try_start_3b
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Square:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_3b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3b .. :try_end_3b} :catch_d

    :goto_3c
    :try_start_3c
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Star4:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3c .. :try_end_3c} :catch_c

    :goto_3d
    :try_start_3d
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Star5:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3d .. :try_end_3d} :catch_b

    :goto_3e
    :try_start_3e
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Star6:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_3e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3e .. :try_end_3e} :catch_a

    :goto_3f
    :try_start_3f
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Summation:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x30

    aput v2, v0, v1
    :try_end_3f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3f .. :try_end_3f} :catch_9

    :goto_40
    :try_start_40
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Table:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_40
    .catch Ljava/lang/NoSuchFieldError; {:try_start_40 .. :try_end_40} :catch_8

    :goto_41
    :try_start_41
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Tetrahedron:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x3e

    aput v2, v0, v1
    :try_end_41
    .catch Ljava/lang/NoSuchFieldError; {:try_start_41 .. :try_end_41} :catch_7

    :goto_42
    :try_start_42
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_42
    .catch Ljava/lang/NoSuchFieldError; {:try_start_42 .. :try_end_42} :catch_6

    :goto_43
    :try_start_43
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_43
    .catch Ljava/lang/NoSuchFieldError; {:try_start_43 .. :try_end_43} :catch_5

    :goto_44
    :try_start_44
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_Equilateral:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_44
    .catch Ljava/lang/NoSuchFieldError; {:try_start_44 .. :try_end_44} :catch_4

    :goto_45
    :try_start_45
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_Isosceles:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_45
    .catch Ljava/lang/NoSuchFieldError; {:try_start_45 .. :try_end_45} :catch_3

    :goto_46
    :try_start_46
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_RightAngled:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_46
    .catch Ljava/lang/NoSuchFieldError; {:try_start_46 .. :try_end_46} :catch_2

    :goto_47
    :try_start_47
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_RightAngled_Isosceles:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_47
    .catch Ljava/lang/NoSuchFieldError; {:try_start_47 .. :try_end_47} :catch_1

    :goto_48
    :try_start_48
    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Unknown:Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_48
    .catch Ljava/lang/NoSuchFieldError; {:try_start_48 .. :try_end_48} :catch_0

    :goto_49
    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeBeautifier;->$SWITCH_TABLE$com$samsung$recognitionengine$ShapeType:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_49

    :catch_1
    move-exception v1

    goto :goto_48

    :catch_2
    move-exception v1

    goto :goto_47

    :catch_3
    move-exception v1

    goto :goto_46

    :catch_4
    move-exception v1

    goto :goto_45

    :catch_5
    move-exception v1

    goto :goto_44

    :catch_6
    move-exception v1

    goto :goto_43

    :catch_7
    move-exception v1

    goto :goto_42

    :catch_8
    move-exception v1

    goto :goto_41

    :catch_9
    move-exception v1

    goto :goto_40

    :catch_a
    move-exception v1

    goto :goto_3f

    :catch_b
    move-exception v1

    goto/16 :goto_3e

    :catch_c
    move-exception v1

    goto/16 :goto_3d

    :catch_d
    move-exception v1

    goto/16 :goto_3c

    :catch_e
    move-exception v1

    goto/16 :goto_3b

    :catch_f
    move-exception v1

    goto/16 :goto_3a

    :catch_10
    move-exception v1

    goto/16 :goto_39

    :catch_11
    move-exception v1

    goto/16 :goto_38

    :catch_12
    move-exception v1

    goto/16 :goto_37

    :catch_13
    move-exception v1

    goto/16 :goto_36

    :catch_14
    move-exception v1

    goto/16 :goto_35

    :catch_15
    move-exception v1

    goto/16 :goto_34

    :catch_16
    move-exception v1

    goto/16 :goto_33

    :catch_17
    move-exception v1

    goto/16 :goto_32

    :catch_18
    move-exception v1

    goto/16 :goto_31

    :catch_19
    move-exception v1

    goto/16 :goto_30

    :catch_1a
    move-exception v1

    goto/16 :goto_2f

    :catch_1b
    move-exception v1

    goto/16 :goto_2e

    :catch_1c
    move-exception v1

    goto/16 :goto_2d

    :catch_1d
    move-exception v1

    goto/16 :goto_2c

    :catch_1e
    move-exception v1

    goto/16 :goto_2b

    :catch_1f
    move-exception v1

    goto/16 :goto_2a

    :catch_20
    move-exception v1

    goto/16 :goto_29

    :catch_21
    move-exception v1

    goto/16 :goto_28

    :catch_22
    move-exception v1

    goto/16 :goto_27

    :catch_23
    move-exception v1

    goto/16 :goto_26

    :catch_24
    move-exception v1

    goto/16 :goto_25

    :catch_25
    move-exception v1

    goto/16 :goto_24

    :catch_26
    move-exception v1

    goto/16 :goto_23

    :catch_27
    move-exception v1

    goto/16 :goto_22

    :catch_28
    move-exception v1

    goto/16 :goto_21

    :catch_29
    move-exception v1

    goto/16 :goto_20

    :catch_2a
    move-exception v1

    goto/16 :goto_1f

    :catch_2b
    move-exception v1

    goto/16 :goto_1e

    :catch_2c
    move-exception v1

    goto/16 :goto_1d

    :catch_2d
    move-exception v1

    goto/16 :goto_1c

    :catch_2e
    move-exception v1

    goto/16 :goto_1b

    :catch_2f
    move-exception v1

    goto/16 :goto_1a

    :catch_30
    move-exception v1

    goto/16 :goto_19

    :catch_31
    move-exception v1

    goto/16 :goto_18

    :catch_32
    move-exception v1

    goto/16 :goto_17

    :catch_33
    move-exception v1

    goto/16 :goto_16

    :catch_34
    move-exception v1

    goto/16 :goto_15

    :catch_35
    move-exception v1

    goto/16 :goto_14

    :catch_36
    move-exception v1

    goto/16 :goto_13

    :catch_37
    move-exception v1

    goto/16 :goto_12

    :catch_38
    move-exception v1

    goto/16 :goto_11

    :catch_39
    move-exception v1

    goto/16 :goto_10

    :catch_3a
    move-exception v1

    goto/16 :goto_f

    :catch_3b
    move-exception v1

    goto/16 :goto_e

    :catch_3c
    move-exception v1

    goto/16 :goto_d

    :catch_3d
    move-exception v1

    goto/16 :goto_c

    :catch_3e
    move-exception v1

    goto/16 :goto_b

    :catch_3f
    move-exception v1

    goto/16 :goto_a

    :catch_40
    move-exception v1

    goto/16 :goto_9

    :catch_41
    move-exception v1

    goto/16 :goto_8

    :catch_42
    move-exception v1

    goto/16 :goto_7

    :catch_43
    move-exception v1

    goto/16 :goto_6

    :catch_44
    move-exception v1

    goto/16 :goto_5

    :catch_45
    move-exception v1

    goto/16 :goto_4

    :catch_46
    move-exception v1

    goto/16 :goto_3

    :catch_47
    move-exception v1

    goto/16 :goto_2

    :catch_48
    move-exception v1

    goto/16 :goto_1
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static beautifyMoon(Lcom/samsung/recognitionengine/ShapeInfo;)Lcom/samsung/recognitionengine/ShapeInfo;
    .locals 16
    .param p0, "shapeInfo"    # Lcom/samsung/recognitionengine/ShapeInfo;

    .prologue
    .line 23
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/recognitionengine/ShapeInfo;->getRecognizedPoints()Lcom/samsung/recognitionengine/PointFVector;

    move-result-object v8

    .line 25
    .local v8, "recognizedPoints":Lcom/samsung/recognitionengine/PointFVector;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/recognitionengine/ShapeInfo;->getShapeType()Lcom/samsung/recognitionengine/ShapeType;

    move-result-object v11

    sget-object v12, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Moon:Lcom/samsung/recognitionengine/ShapeType;

    if-ne v11, v12, :cond_0

    invoke-virtual {v8}, Lcom/samsung/recognitionengine/PointFVector;->size()J

    move-result-wide v12

    const-wide/16 v14, 0x4

    cmp-long v11, v12, v14

    if-eqz v11, :cond_1

    .line 26
    :cond_0
    new-instance v11, Ljava/lang/IllegalArgumentException;

    invoke-direct {v11}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v11

    .line 28
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/recognitionengine/ShapeInfo;->getRelevance()I

    move-result v9

    .line 31
    .local v9, "relevance":I
    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v10

    .line 32
    .local v10, "start":Lcom/samsung/recognitionengine/PointF;
    const/4 v11, 0x3

    invoke-virtual {v8, v11}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v7

    .line 33
    .local v7, "middle":Lcom/samsung/recognitionengine/PointF;
    const/4 v11, 0x2

    invoke-virtual {v8, v11}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v6

    .line 36
    .local v6, "end":Lcom/samsung/recognitionengine/PointF;
    invoke-virtual {v10}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v11

    invoke-virtual {v6}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v12

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float v2, v11, v12

    .line 37
    .local v2, "centerFirstArcX":F
    invoke-virtual {v10}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v11

    invoke-virtual {v6}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v12

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float v3, v11, v12

    .line 38
    .local v3, "centerFirstArcY":F
    new-instance v1, Lcom/samsung/recognitionengine/PointF;

    invoke-direct {v1, v2, v3}, Lcom/samsung/recognitionengine/PointF;-><init>(FF)V

    .line 41
    .local v1, "centerFirstArc":Lcom/samsung/recognitionengine/PointF;
    invoke-virtual {v1}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v11

    invoke-virtual {v7}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v12

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float v4, v11, v12

    .line 42
    .local v4, "centerX":F
    invoke-virtual {v1}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v11

    invoke-virtual {v7}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v12

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float v5, v11, v12

    .line 43
    .local v5, "centerY":F
    new-instance v0, Lcom/samsung/recognitionengine/PointF;

    invoke-direct {v0, v4, v5}, Lcom/samsung/recognitionengine/PointF;-><init>(FF)V

    .line 46
    .local v0, "center":Lcom/samsung/recognitionengine/PointF;
    const/4 v11, 0x1

    invoke-virtual {v8, v11, v0}, Lcom/samsung/recognitionengine/PointFVector;->set(ILcom/samsung/recognitionengine/PointF;)V

    .line 48
    new-instance v11, Lcom/samsung/recognitionengine/ShapeInfo;

    sget-object v12, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Moon:Lcom/samsung/recognitionengine/ShapeType;

    invoke-direct {v11, v12, v8, v9}, Lcom/samsung/recognitionengine/ShapeInfo;-><init>(Lcom/samsung/recognitionengine/ShapeType;Lcom/samsung/recognitionengine/PointFVector;I)V

    return-object v11
.end method

.method public static beautifyShape(Lcom/samsung/recognitionengine/ShapeInfo;)Lcom/samsung/recognitionengine/ShapeInfo;
    .locals 2
    .param p0, "shapeInfo"    # Lcom/samsung/recognitionengine/ShapeInfo;

    .prologue
    .line 11
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeBeautifier;->$SWITCH_TABLE$com$samsung$recognitionengine$ShapeType()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/recognitionengine/ShapeInfo;->getShapeType()Lcom/samsung/recognitionengine/ShapeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/ShapeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 17
    .end local p0    # "shapeInfo":Lcom/samsung/recognitionengine/ShapeInfo;
    :goto_0
    return-object p0

    .line 14
    .restart local p0    # "shapeInfo":Lcom/samsung/recognitionengine/ShapeInfo;
    :pswitch_0
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeBeautifier;->beautifyMoon(Lcom/samsung/recognitionengine/ShapeInfo;)Lcom/samsung/recognitionengine/ShapeInfo;

    move-result-object p0

    goto :goto_0

    .line 11
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
    .end packed-switch
.end method
