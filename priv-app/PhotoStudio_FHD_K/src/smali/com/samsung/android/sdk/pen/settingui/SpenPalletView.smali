.class Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;
.super Landroid/widget/LinearLayout;
.source "SpenPalletView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "WrongCall"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;
    }
.end annotation


# instance fields
.field private mPalletViewOnLayoutListner:Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->mPalletViewOnLayoutListner:Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;

    .line 13
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 17
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 19
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->mPalletViewOnLayoutListner:Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->mPalletViewOnLayoutListner:Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;->onLayout(ZIIII)V

    .line 22
    :cond_0
    return-void
.end method

.method public setOnLayoutListener(Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;)V
    .locals 0
    .param p1, "mOnLayoutListner"    # Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->mPalletViewOnLayoutListner:Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;

    .line 26
    return-void
.end method
