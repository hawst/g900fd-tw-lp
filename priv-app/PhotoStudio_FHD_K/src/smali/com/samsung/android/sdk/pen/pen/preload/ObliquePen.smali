.class public Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;
.super Ljava/lang/Object;
.source "ObliquePen.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mIs64:Z

.field public final nativeObliquePen:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    .line 29
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    .line 31
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_init()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    .line 32
    return-void

    .line 29
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private Native_construct(J)Z
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 326
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 327
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_construct(J)Z

    move-result v0

    .line 329
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_construct(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_draw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "rect"    # Landroid/graphics/RectF;
    .param p5, "toolType"    # I

    .prologue
    .line 361
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 362
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_draw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    .line 364
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_draw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_getAdvancedSetting(J)Ljava/lang/String;
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 445
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 446
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getAdvancedSetting(J)Ljava/lang/String;

    move-result-object v0

    .line 448
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getAdvancedSetting(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_getColor(J)I
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 417
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 418
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getColor(J)I

    move-result v0

    .line 420
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getMaxSettingValue(J)F
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 403
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 404
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getMaxSettingValue(J)F

    move-result v0

    .line 406
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getMaxSettingValue(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getMinSettingValue(J)F
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 396
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 397
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getMinSettingValue(J)F

    move-result v0

    .line 399
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getMinSettingValue(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getPenAttribute(JI)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "attribute"    # I

    .prologue
    .line 462
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 463
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getPenAttribute(JI)Z

    move-result v0

    .line 465
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getPenAttribute(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_getProperty(JLandroid/os/Bundle;)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 354
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 355
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getProperty(JLandroid/os/Bundle;)Z

    move-result v0

    .line 357
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getProperty(ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_getSize(J)F
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 389
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 390
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getSize(J)F

    move-result v0

    .line 392
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getSize(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getStrokeRect(J[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
    .locals 7
    .param p1, "nativePen"    # J
    .param p3, "points"    # [Landroid/graphics/PointF;
    .param p4, "pressures"    # [F
    .param p5, "timestamps"    # [I
    .param p6, "size"    # F
    .param p7, "isCurvable"    # Z
    .param p8, "advanced"    # Ljava/lang/String;

    .prologue
    .line 453
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 454
    invoke-static/range {p1 .. p8}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getStrokeRect(J[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;

    move-result-object v0

    .line 457
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move v4, p6

    move v5, p7

    move-object v6, p8

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_getStrokeRect(I[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_init()J
    .locals 2

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 320
    invoke-static {}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_init_64()J

    move-result-wide v0

    .line 322
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_init()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private Native_isCurveEnabled(J)Z
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 431
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 432
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_isCurveEnabled(J)Z

    move-result v0

    .line 434
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_isCurveEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onLoad(J)V
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 333
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 334
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_onLoad(J)V

    .line 337
    :goto_0
    return-void

    .line 336
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_onLoad(I)V

    goto :goto_0
.end method

.method private Native_onUnload(J)V
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 340
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 341
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_onUnload(J)V

    .line 344
    :goto_0
    return-void

    .line 343
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_onUnload(I)V

    goto :goto_0
.end method

.method private Native_redraw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "rect"    # Landroid/graphics/RectF;
    .param p5, "toolType"    # I

    .prologue
    .line 368
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 369
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_redraw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    .line 371
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_redraw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setAdvancedSetting(JLjava/lang/String;)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "advancedSetting"    # Ljava/lang/String;

    .prologue
    .line 438
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 439
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_setAdvancedSetting(JLjava/lang/String;)Z

    move-result v0

    .line 441
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_setAdvancedSetting(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setBitmap(JLandroid/graphics/Bitmap;)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 375
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 376
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_setBitmap(JLandroid/graphics/Bitmap;)Z

    move-result v0

    .line 378
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_setBitmap(ILandroid/graphics/Bitmap;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setColor(JI)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "color"    # I

    .prologue
    .line 410
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 411
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_setColor(JI)Z

    move-result v0

    .line 413
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_setColor(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setCurveEnabled(JZ)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "curve"    # Z

    .prologue
    .line 424
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 425
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_setCurveEnabled(JZ)Z

    move-result v0

    .line 427
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_setCurveEnabled(IZ)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setProperty(JLandroid/os/Bundle;)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 347
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 348
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_setProperty(JLandroid/os/Bundle;)Z

    move-result v0

    .line 350
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_setProperty(ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setSize(JF)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "size"    # F

    .prologue
    .line 382
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 383
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_setSize(JF)Z

    move-result v0

    .line 385
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->native_setSize(IF)Z

    move-result v0

    goto :goto_0
.end method

.method private static native native_command(IILjava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_command(JILjava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_construct(I)Z
.end method

.method private static native native_construct(J)Z
.end method

.method private static native native_draw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_draw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_end(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_end(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_finalize(J)V
.end method

.method private static native native_getAdvancedSetting(I)Ljava/lang/String;
.end method

.method private static native native_getAdvancedSetting(J)Ljava/lang/String;
.end method

.method private static native native_getColor(I)I
.end method

.method private static native native_getColor(J)I
.end method

.method private static native native_getMaxSettingValue(I)F
.end method

.method private static native native_getMaxSettingValue(J)F
.end method

.method private static native native_getMinSettingValue(I)F
.end method

.method private static native native_getMinSettingValue(J)F
.end method

.method private static native native_getPenAttribute(II)Z
.end method

.method private static native native_getPenAttribute(JI)Z
.end method

.method private static native native_getProperty(ILandroid/os/Bundle;)Z
.end method

.method private static native native_getProperty(JLandroid/os/Bundle;)Z
.end method

.method private static native native_getSize(I)F
.end method

.method private static native native_getSize(J)F
.end method

.method private static native native_getStrokeRect(I[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
.end method

.method private static native native_getStrokeRect(J[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
.end method

.method private static native native_init()I
.end method

.method private static native native_init_64()J
.end method

.method private static native native_isCurveEnabled(I)Z
.end method

.method private static native native_isCurveEnabled(J)Z
.end method

.method private static native native_move(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_move(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_onLoad(I)V
.end method

.method private static native native_onLoad(J)V
.end method

.method private static native native_onUnload(I)V
.end method

.method private static native native_onUnload(J)V
.end method

.method private static native native_redraw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_redraw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_setAdvancedSetting(ILjava/lang/String;)Z
.end method

.method private static native native_setAdvancedSetting(JLjava/lang/String;)Z
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setBitmap(JLandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setColor(II)Z
.end method

.method private static native native_setColor(JI)Z
.end method

.method private static native native_setCurveEnabled(IZ)Z
.end method

.method private static native native_setCurveEnabled(JZ)Z
.end method

.method private static native native_setProperty(ILandroid/os/Bundle;)Z
.end method

.method private static native native_setProperty(JLandroid/os/Bundle;)Z
.end method

.method private static native native_setReferenceBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setReferenceBitmap(JLandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setSize(IF)Z
.end method

.method private static native native_setSize(JF)Z
.end method

.method private static native native_start(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_start(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method


# virtual methods
.method public construct()V
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_construct(J)Z

    .line 40
    return-void
.end method

.method public draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v1, 0x0

    .line 112
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mBitmap:Landroid/graphics/Bitmap;

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v1, v1}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 123
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_draw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public getAdvancedSetting()Ljava/lang/String;
    .locals 2

    .prologue
    .line 279
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_getAdvancedSetting(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getColor()I
    .locals 2

    .prologue
    .line 239
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_getColor(J)I

    move-result v0

    return v0
.end method

.method public getMaxSettingValue()F
    .locals 2

    .prologue
    .line 219
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_getMaxSettingValue(J)F

    move-result v0

    return v0
.end method

.method public getMinSettingValue()F
    .locals 2

    .prologue
    .line 210
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_getMinSettingValue(J)F

    move-result v0

    return v0
.end method

.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    return-wide v0
.end method

.method public getPenAttribute(I)Z
    .locals 2
    .param p1, "attribute"    # I

    .prologue
    .line 314
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_getPenAttribute(JI)Z

    move-result v0

    return v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_getProperty(JLandroid/os/Bundle;)Z

    .line 95
    return-void
.end method

.method public getSize()F
    .locals 2

    .prologue
    .line 201
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_getSize(J)F

    move-result v0

    return v0
.end method

.method public getStrokeRect([Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
    .locals 10
    .param p1, "points"    # [Landroid/graphics/PointF;
    .param p2, "pressures"    # [F
    .param p3, "timestamps"    # [I
    .param p4, "size"    # F
    .param p5, "isCurvable"    # Z
    .param p6, "advanced"    # Ljava/lang/String;

    .prologue
    .line 305
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    move v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_getStrokeRect(J[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public hideAdvancedSetting()V
    .locals 0

    .prologue
    .line 296
    return-void
.end method

.method public isCurveEnabled()Z
    .locals 2

    .prologue
    .line 259
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_isCurveEnabled(J)Z

    move-result v0

    return v0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_onLoad(J)V

    .line 49
    return-void
.end method

.method public onUnload()V
    .locals 2

    .prologue
    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mBitmap:Landroid/graphics/Bitmap;

    .line 76
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_onUnload(J)V

    .line 77
    return-void
.end method

.method public redrawPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 7
    .param p1, "allEvent"    # Landroid/view/MotionEvent;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v1, 0x0

    .line 134
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mBitmap:Landroid/graphics/Bitmap;

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v1, v1}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 145
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_redraw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setAdvancedSetting(Ljava/lang/String;)V
    .locals 2
    .param p1, "advancedSetting"    # Ljava/lang/String;

    .prologue
    .line 268
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_setAdvancedSetting(JLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 271
    :cond_0
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mBitmap:Landroid/graphics/Bitmap;

    .line 157
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_setBitmap(JLandroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 228
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_setColor(JI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 231
    :cond_0
    return-void
.end method

.method public setCurveEnabled(Z)V
    .locals 2
    .param p1, "curve"    # Z

    .prologue
    .line 248
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_setCurveEnabled(JZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 251
    :cond_0
    return-void
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_setProperty(JLandroid/os/Bundle;)Z

    .line 86
    return-void
.end method

.method public setReferenceBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 182
    return-void
.end method

.method public setSize(F)V
    .locals 2
    .param p1, "size"    # F

    .prologue
    .line 190
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->nativeObliquePen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/ObliquePen;->Native_setSize(JF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 193
    :cond_0
    return-void
.end method

.method public showAdvancedSetting(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 288
    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 66
    const/4 v0, 0x1

    return v0
.end method
