.class Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;
.super Ljava/lang/Object;
.source "SpenControlList.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    .line 933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onExceedLimit()V
    .locals 0

    .prologue
    .line 955
    return-void
.end method

.method public onFocusChanged(Z)V
    .locals 0
    .param p1, "gainFocus"    # Z

    .prologue
    .line 985
    return-void
.end method

.method public onMoreButtonDown(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 0
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 964
    return-void
.end method

.method public onObjectChanged(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 945
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v1, :cond_0

    .line 947
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 948
    .local v0, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 949
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onObjectChanged(Ljava/util/ArrayList;)V

    .line 951
    .end local v0    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :cond_0
    return-void
.end method

.method public onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 1
    .param p1, "coordinateInfo"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .prologue
    .line 968
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 969
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 971
    :cond_0
    return-void
.end method

.method public onRequestScroll(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 936
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->onRequestScroll(FF)V

    .line 937
    return-void
.end method

.method public onSelectionChanged(II)Z
    .locals 1
    .param p1, "selStart"    # I
    .param p2, "selEnd"    # I

    .prologue
    .line 959
    const/4 v0, 0x1

    return v0
.end method

.method public onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    .line 941
    return-void
.end method

.method public onUndo()V
    .locals 0

    .prologue
    .line 975
    return-void
.end method

.method public onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V
    .locals 1
    .param p1, "objectTextBox"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    .param p2, "visible"    # Z

    .prologue
    .line 979
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V

    .line 981
    return-void
.end method
