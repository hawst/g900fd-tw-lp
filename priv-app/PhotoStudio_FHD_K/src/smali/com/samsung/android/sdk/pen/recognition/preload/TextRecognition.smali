.class public Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;
.super Ljava/lang/Object;
.source "TextRecognition.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TextRecognition"


# instance fields
.field private mCurrLanguage:Ljava/lang/String;

.field private mDataPath:Ljava/lang/String;

.field private mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method private init(Ljava/lang/String;)Z
    .locals 4
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mCurrLanguage:Ljava/lang/String;

    .line 52
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    if-eqz v1, :cond_0

    .line 53
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    invoke-virtual {v1}, Lcom/samsung/vip/engine/VITextRecognitionLib;->close()V

    .line 54
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    .line 57
    :cond_0
    new-instance v1, Lcom/samsung/vip/engine/VITextAllRecognitionLib;

    invoke-direct {v1}, Lcom/samsung/vip/engine/VITextAllRecognitionLib;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    .line 59
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mDataPath:Ljava/lang/String;

    const-string v3, "text"

    invoke-virtual {v1, v2, p1, v3}, Lcom/samsung/vip/engine/VITextRecognitionLib;->init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 60
    .local v0, "ret":I
    if-eqz v0, :cond_1

    .line 61
    const-string v1, "TextRecognition"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "init error: ret = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const/4 v1, 0x0

    .line 64
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addStroke([F[F)V
    .locals 1
    .param p1, "x"    # [F
    .param p2, "y"    # [F

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    if-nez v0, :cond_0

    .line 76
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/vip/engine/VITextRecognitionLib;->addStroke([F[F)V

    goto :goto_0
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/VITextRecognitionLib;->close()V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    .line 100
    :cond_0
    return-void
.end method

.method public getCurrentLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    if-nez v0, :cond_0

    .line 124
    const/4 v0, 0x0

    .line 126
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mCurrLanguage:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSupportedLanguage()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    if-nez v1, :cond_0

    .line 135
    const/4 v1, 0x0

    .line 138
    :goto_0
    return-object v1

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    invoke-virtual {v1}, Lcom/samsung/vip/engine/VITextRecognitionLib;->getLangList()[Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "list":[Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public init(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lang"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/recognition/preload/VIRecogUtils;->copyDatabase(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 35
    const-string v1, "TextRecognition"

    const-string v2, "Fail to copy database."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    :goto_0
    return v0

    .line 39
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/vidata/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mDataPath:Ljava/lang/String;

    .line 40
    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->init(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 41
    const-string v1, "TextRecognition"

    const-string v2, "Fail to initialize."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mDataPath:Ljava/lang/String;

    goto :goto_0

    .line 46
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public process()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    if-nez v1, :cond_0

    .line 84
    const/4 v0, 0x0

    .line 88
    :goto_0
    return-object v0

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    invoke-virtual {v1}, Lcom/samsung/vip/engine/VITextRecognitionLib;->recog()[Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "result":[Ljava/lang/String;
    goto :goto_0
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 3
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 107
    const-string v0, "TextRecognition"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setLanguage() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    if-nez v0, :cond_0

    .line 116
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/VITextRecognitionLib;

    invoke-virtual {v0, p1}, Lcom/samsung/vip/engine/VITextRecognitionLib;->isSupportedLanguage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 112
    const-string v0, "TextRecognition"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported language: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 115
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->init(Ljava/lang/String;)Z

    goto :goto_0
.end method
