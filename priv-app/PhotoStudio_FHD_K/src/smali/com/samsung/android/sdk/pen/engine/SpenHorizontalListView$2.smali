.class Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SpenHorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    .line 345
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 349
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 354
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 398
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v6, 0x1

    .line 359
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    monitor-enter v3

    .line 360
    :try_start_0
    const-string v2, "SpenHorizontalListView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onScroll distanceX ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",mNextX = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    const/4 v4, 0x1

    invoke-static {v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;Z)V

    .line 362
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildCount()I

    move-result v0

    .line 363
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_3

    .line 367
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget v4, v2, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    float-to-int v5, p3

    add-int/2addr v4, v5

    iput v4, v2, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    .line 368
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    if-gtz v2, :cond_0

    .line 369
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    const/4 v4, 0x1

    invoke-static {v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;Z)V

    .line 370
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/EdgeEffect;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 371
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/EdgeEffect;

    move-result-object v2

    const v4, 0x3e99999a    # 0.3f

    invoke-virtual {v2, v4}, Landroid/widget/EdgeEffect;->onPull(F)V

    .line 374
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mMaxX:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 375
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;Z)V

    .line 376
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/EdgeEffect;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 377
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/EdgeEffect;

    move-result-object v2

    const v4, 0x3e99999a    # 0.3f

    invoke-virtual {v2, v4}, Landroid/widget/EdgeEffect;->onPull(F)V

    .line 359
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->requestLayout()V

    .line 383
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 384
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

    move-result-object v2

    invoke-interface {v2, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;->onScrolll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    .line 387
    :cond_2
    return v6

    .line 364
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setPressed(Z)V

    .line 363
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 359
    .end local v0    # "childCount":I
    .end local v1    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 392
    const/4 v0, 0x1

    return v0
.end method
