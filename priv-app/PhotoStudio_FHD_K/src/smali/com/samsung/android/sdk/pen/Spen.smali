.class public Lcom/samsung/android/sdk/pen/Spen;
.super Ljava/lang/Object;
.source "Spen.java"

# interfaces
.implements Lcom/samsung/android/sdk/SsdkInterface;


# static fields
.field private static final DEFAULT_MAX_CACHE_SIZE:I = 0x5

.field public static final DEVICE_PEN:I = 0x0

.field public static final IS_SPEN_PRELOAD_MODE:Z = true

.field public static final SPEN_DYNAMIC_LIB_MODE:I = 0x0

.field public static final SPEN_NATIVE_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.sdk.spenv10"

.field public static final SPEN_STATIC_LIB_MODE:I = 0x1

.field private static final VERSION:Ljava/lang/String; = "3.0.221"

.field private static final VERSION_LEVEL:I = 0x37

.field private static mIsInitialized:Z

.field private static mSpenPackageName:Ljava/lang/String;

.field private static os:I


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    sput-boolean v1, Lcom/samsung/android/sdk/pen/Spen;->mIsInitialized:Z

    .line 70
    const-string v0, "com.samsung.android.sdk.spenv10"

    sput-object v0, Lcom/samsung/android/sdk/pen/Spen;->mSpenPackageName:Ljava/lang/String;

    .line 74
    sput v1, Lcom/samsung/android/sdk/pen/Spen;->os:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native SPenSdk_OSType()I
.end method

.method private static native SPenSdk_init(Ljava/lang/String;II)Z
.end method

.method private static native SPenSdk_init2(Ljava/lang/String;III)Z
.end method

.method public static getSpenPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/samsung/android/sdk/pen/Spen;->mSpenPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public static osType()I
    .locals 3

    .prologue
    .line 365
    sget v1, Lcom/samsung/android/sdk/pen/Spen;->os:I

    if-eqz v1, :cond_0

    sget v1, Lcom/samsung/android/sdk/pen/Spen;->os:I

    .line 372
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return v1

    .line 367
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->SPenSdk_OSType()I

    move-result v1

    sput v1, Lcom/samsung/android/sdk/pen/Spen;->os:I

    .line 368
    sget v1, Lcom/samsung/android/sdk/pen/Spen;->os:I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 369
    :catch_0
    move-exception v0

    .line 370
    .restart local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "Spen"

    const-string v2, "osType - old so"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    const/16 v1, 0x20

    sput v1, Lcom/samsung/android/sdk/pen/Spen;->os:I

    .line 372
    sget v1, Lcom/samsung/android/sdk/pen/Spen;->os:I

    goto :goto_0
.end method


# virtual methods
.method V58433d09d024(I)I
    .locals 1
    .param p1, "input"    # I

    .prologue
    .line 88
    mul-int v0, p1, p1

    add-int/2addr v0, p1

    return v0
.end method

.method public getVersionCode()I
    .locals 1

    .prologue
    .line 123
    const/16 v0, 0x37

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    const-string v0, "3.0.221"

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/SsdkUnsupportedException;
        }
    .end annotation

    .prologue
    .line 171
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->initialize(Landroid/content/Context;II)V

    .line 172
    return-void
.end method

.method public initialize(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "maxCacheSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/SsdkUnsupportedException;
        }
    .end annotation

    .prologue
    .line 193
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/android/sdk/pen/Spen;->initialize(Landroid/content/Context;II)V

    .line 194
    return-void
.end method

.method public initialize(Landroid/content/Context;II)V
    .locals 26
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "maxCacheSize"    # I
    .param p3, "createMode"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/SsdkUnsupportedException;
        }
    .end annotation

    .prologue
    .line 216
    const-string v23, "Spen"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "SpenSdk version level = "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/Spen;->getVersionCode()I

    move-result v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const-string v23, "Spen"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "SpenSdk jar version = "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/Spen;->getVersionName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    if-nez p1, :cond_0

    .line 220
    new-instance v23, Ljava/lang/IllegalArgumentException;

    const-string v24, "context is invalid."

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 222
    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/Spen;->mContext:Landroid/content/Context;

    .line 224
    const/16 v23, 0x1

    move/from16 v0, p3

    move/from16 v1, v23

    if-ne v0, v1, :cond_1

    .line 225
    const-string v23, "Spen"

    const-string v24, "SpenSdk use static library"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/Spen;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v23

    sput-object v23, Lcom/samsung/android/sdk/pen/Spen;->mSpenPackageName:Ljava/lang/String;

    .line 229
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    .line 230
    .local v3, "appFilesDir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v23

    if-nez v23, :cond_2

    .line 231
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v23

    if-nez v23, :cond_2

    .line 232
    const-string v23, "Spen"

    const-string v24, "Cannot create application files directory"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    new-instance v23, Ljava/lang/IllegalStateException;

    const-string v24, "Cannot create application directory."

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 240
    :cond_2
    const-string v23, "window"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/view/WindowManager;

    .line 243
    .local v22, "windowManager":Landroid/view/WindowManager;
    :try_start_0
    new-instance v18, Landroid/graphics/Point;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Point;-><init>()V

    .line 244
    .local v18, "point":Landroid/graphics/Point;
    invoke-interface/range {v22 .. v22}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 245
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    .line 246
    .local v21, "width":I
    move-object/from16 v0, v18

    iget v10, v0, Landroid/graphics/Point;->y:I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    .end local v18    # "point":Landroid/graphics/Point;
    .local v10, "height":I
    :goto_0
    sget-boolean v23, Lcom/samsung/android/sdk/pen/Spen;->mIsInitialized:Z

    if-eqz v23, :cond_4

    .line 274
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v21

    move/from16 v2, p2

    invoke-static {v0, v1, v10, v2}, Lcom/samsung/android/sdk/pen/Spen;->SPenSdk_init2(Ljava/lang/String;III)Z

    move-result v23

    if-nez v23, :cond_3

    .line 275
    new-instance v23, Ljava/lang/IllegalStateException;

    const-string v24, "SDK Cache directory is not initialized."

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 247
    .end local v10    # "height":I
    .end local v21    # "width":I
    :catch_0
    move-exception v5

    .line 250
    .local v5, "e":Ljava/lang/NoSuchMethodError;
    :try_start_1
    invoke-interface/range {v22 .. v22}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    .line 251
    .local v4, "display":Landroid/view/Display;
    const-class v23, Landroid/view/Display;

    const-string v24, "getRawWidth"

    const/16 v25, 0x0

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v25, v0

    invoke-virtual/range {v23 .. v25}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v9

    .line 252
    .local v9, "getRawW":Ljava/lang/reflect/Method;
    const-class v23, Landroid/view/Display;

    const-string v24, "getRawHeight"

    const/16 v25, 0x0

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v25, v0

    invoke-virtual/range {v23 .. v25}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    .line 253
    .local v8, "getRawH":Ljava/lang/reflect/Method;
    const/16 v23, 0x0

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v9, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v21

    .line 254
    .restart local v21    # "width":I
    const/16 v23, 0x0

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v8, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v10

    .restart local v10    # "height":I
    goto :goto_0

    .line 255
    .end local v4    # "display":Landroid/view/Display;
    .end local v8    # "getRawH":Ljava/lang/reflect/Method;
    .end local v9    # "getRawW":Ljava/lang/reflect/Method;
    .end local v10    # "height":I
    .end local v21    # "width":I
    :catch_1
    move-exception v6

    .line 258
    .local v6, "e1":Ljava/lang/Exception;
    :try_start_2
    new-instance v18, Landroid/graphics/Point;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Point;-><init>()V

    .line 259
    .restart local v18    # "point":Landroid/graphics/Point;
    invoke-interface/range {v22 .. v22}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 261
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    .line 262
    .restart local v21    # "width":I
    move-object/from16 v0, v18

    iget v10, v0, Landroid/graphics/Point;->y:I
    :try_end_2
    .catch Ljava/lang/NoSuchMethodError; {:try_start_2 .. :try_end_2} :catch_2

    .restart local v10    # "height":I
    goto/16 :goto_0

    .line 263
    .end local v10    # "height":I
    .end local v18    # "point":Landroid/graphics/Point;
    .end local v21    # "width":I
    :catch_2
    move-exception v7

    .line 265
    .local v7, "e2":Ljava/lang/NoSuchMethodError;
    invoke-interface/range {v22 .. v22}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    .line 267
    .restart local v4    # "display":Landroid/view/Display;
    invoke-virtual {v4}, Landroid/view/Display;->getWidth()I

    move-result v21

    .line 268
    .restart local v21    # "width":I
    invoke-virtual {v4}, Landroid/view/Display;->getHeight()I

    move-result v10

    .restart local v10    # "height":I
    goto/16 :goto_0

    .line 277
    .end local v4    # "display":Landroid/view/Display;
    .end local v5    # "e":Ljava/lang/NoSuchMethodError;
    .end local v6    # "e1":Ljava/lang/Exception;
    .end local v7    # "e2":Ljava/lang/NoSuchMethodError;
    :cond_3
    const-string v23, "Spen"

    const-string v24, "initialize complete"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    :goto_1
    return-void

    .line 281
    :cond_4
    sget-object v19, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 282
    .local v19, "strBrand":Ljava/lang/String;
    sget-object v20, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 283
    .local v20, "strManufacturer":Ljava/lang/String;
    if-eqz v19, :cond_5

    if-nez v20, :cond_6

    .line 284
    :cond_5
    new-instance v23, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string v24, "Vendor is not supported"

    const/16 v25, 0x0

    invoke-direct/range {v23 .. v25}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v23

    .line 286
    :cond_6
    const-string v23, "Samsung"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-eqz v23, :cond_7

    const-string v23, "Samsung"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-eqz v23, :cond_7

    .line 287
    new-instance v23, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string v24, "Vendor is not supported"

    const/16 v25, 0x0

    invoke-direct/range {v23 .. v25}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v23

    .line 289
    :cond_7
    sget v23, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v24, 0xe

    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_8

    .line 290
    new-instance v23, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "Device SDK version codes is "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v25, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 291
    const/16 v25, 0x1

    .line 290
    invoke-direct/range {v23 .. v25}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v23

    .line 294
    :cond_8
    if-nez p3, :cond_11

    .line 296
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 297
    .local v17, "pm":Landroid/content/pm/PackageManager;
    const/4 v15, 0x0

    .line 300
    .local v15, "packageInfo":Landroid/content/pm/PackageInfo;
    :try_start_3
    const-string v23, "com.samsung.android.sdk.spenv10"

    const/16 v24, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v15

    .line 302
    const-string v23, "Spen"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "SpenSdk apk version = "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v15, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    iget-object v0, v15, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "\\."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 304
    .local v16, "packageVersion":[Ljava/lang/String;
    const-string v23, "3.0.221"

    const-string v24, "\\."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 306
    .local v13, "javaVersion":[Ljava/lang/String;
    const/16 v23, 0x4

    move/from16 v0, v23

    new-array v14, v0, [I

    .line 307
    .local v14, "packageData":[I
    const/16 v23, 0x4

    move/from16 v0, v23

    new-array v12, v0, [I

    .line 308
    .local v12, "javaData":[I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    const/16 v23, 0x4

    move/from16 v0, v23

    if-lt v11, v0, :cond_a

    .line 319
    const/4 v11, 0x0

    :goto_3
    const/16 v23, 0x4

    move/from16 v0, v23

    if-lt v11, v0, :cond_d

    .line 331
    const/16 v23, 0x0

    aget v23, v12, v23

    const/16 v24, 0x0

    aget v24, v14, v24

    move/from16 v0, v23

    move/from16 v1, v24

    if-gt v0, v1, :cond_9

    const/16 v23, 0x0

    aget v23, v12, v23

    const/16 v24, 0x0

    aget v24, v14, v24

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_10

    const/16 v23, 0x1

    aget v23, v12, v23

    const/16 v24, 0x1

    aget v24, v14, v24

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_10

    .line 332
    :cond_9
    const-string v23, "Spen"

    const-string v24, "You SHOULD UPDATE SpenSdk apk file !!!!"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    new-instance v23, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string v24, "SpenSdk apk version is low."

    .line 334
    const/16 v25, 0x2

    .line 333
    invoke-direct/range {v23 .. v25}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v23
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    .line 338
    .end local v11    # "i":I
    .end local v12    # "javaData":[I
    .end local v13    # "javaVersion":[Ljava/lang/String;
    .end local v14    # "packageData":[I
    .end local v16    # "packageVersion":[Ljava/lang/String;
    :catch_3
    move-exception v6

    .line 339
    .local v6, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v23, "Spen"

    const-string v24, "You SHOULD INSTALL SpenSdk apk file !!!!"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    new-instance v23, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string v24, "SpenSdk apk is not installed."

    .line 341
    const/16 v25, 0x2

    .line 340
    invoke-direct/range {v23 .. v25}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v23

    .line 309
    .end local v6    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v11    # "i":I
    .restart local v12    # "javaData":[I
    .restart local v13    # "javaVersion":[Ljava/lang/String;
    .restart local v14    # "packageData":[I
    .restart local v16    # "packageVersion":[Ljava/lang/String;
    :cond_a
    :try_start_4
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-gt v0, v11, :cond_b

    .line 310
    const/16 v23, 0x0

    aput v23, v14, v11

    .line 308
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 313
    :cond_b
    aget-object v23, v16, v11

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->isEmpty()Z

    move-result v23

    if-eqz v23, :cond_c

    .line 314
    const/16 v23, 0x0

    aput v23, v14, v11

    goto :goto_4

    .line 317
    :cond_c
    aget-object v23, v16, v11

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    aput v23, v14, v11

    goto :goto_4

    .line 320
    :cond_d
    array-length v0, v13

    move/from16 v23, v0

    move/from16 v0, v23

    if-gt v0, v11, :cond_e

    .line 321
    const/16 v23, 0x0

    aput v23, v12, v11

    .line 319
    :goto_5
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    .line 324
    :cond_e
    aget-object v23, v13, v11

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->isEmpty()Z

    move-result v23

    if-eqz v23, :cond_f

    .line 325
    const/16 v23, 0x0

    aput v23, v12, v11

    goto :goto_5

    .line 328
    :cond_f
    aget-object v23, v13, v11

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    aput v23, v12, v11

    goto :goto_5

    .line 336
    :cond_10
    const-string v23, "Spen"

    const-string v24, "Server UPDATE Check"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_3

    .line 345
    .end local v11    # "i":I
    .end local v12    # "javaData":[I
    .end local v13    # "javaVersion":[Ljava/lang/String;
    .end local v14    # "packageData":[I
    .end local v15    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v16    # "packageVersion":[Ljava/lang/String;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    :cond_11
    const-string v23, "SPenBase"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    const-string v23, "SPenPluginFW"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    const-string v23, "SPenInit"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    .line 346
    const-string v23, "SPenModel"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    const-string v23, "SPenSkia"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    const-string v23, "SPenEngine"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    .line 347
    const-string v23, "SPenBrush"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    const-string v23, "SPenChineseBrush"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    const-string v23, "SPenInkPen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    .line 348
    const-string v23, "SPenMarker"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    const-string v23, "SPenPencil"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    const-string v23, "SPenNativePen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    .line 349
    const-string v23, "SPenMontblancFountainPen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    const-string v23, "SPenMontblancCalligraphyPen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    .line 350
    const-string v23, "SPenMagicPen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    const-string v23, "SPenObliquePen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    const-string v23, "SPenFountainPen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-nez v23, :cond_13

    .line 351
    :cond_12
    new-instance v23, Ljava/lang/IllegalStateException;

    invoke-direct/range {v23 .. v23}, Ljava/lang/IllegalStateException;-><init>()V

    throw v23

    .line 354
    :cond_13
    const-string v23, "Spen"

    const-string v24, "SpenSdk Libraries are loaded."

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v21

    move/from16 v2, p2

    invoke-static {v0, v1, v10, v2}, Lcom/samsung/android/sdk/pen/Spen;->SPenSdk_init2(Ljava/lang/String;III)Z

    move-result v23

    if-nez v23, :cond_14

    .line 357
    new-instance v23, Ljava/lang/IllegalStateException;

    const-string v24, "SDK Cache directory is not initialized."

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 360
    :cond_14
    const/16 v23, 0x1

    sput-boolean v23, Lcom/samsung/android/sdk/pen/Spen;->mIsInitialized:Z

    .line 361
    const-string v23, "Spen"

    const-string v24, "initialize complete"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public isFeatureEnabled(I)Z
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 147
    packed-switch p1, :pswitch_data_0

    .line 155
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 149
    :pswitch_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/class/sec/sec_epen"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 150
    .local v0, "path":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    const/4 v1, 0x1

    .line 157
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 147
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method loadLibrary(Ljava/lang/String;)Z
    .locals 2
    .param p1, "libraryName"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation

    .prologue
    .line 95
    :try_start_0
    invoke-static {p1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "error":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method
