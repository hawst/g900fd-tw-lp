.class Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;
.super Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
.source "SpenPresetListItem.java"


# static fields
.field private static final DISPLAY_CHAGALL_WIDTH:I = 0xa00

.field public static final IB_PEN_PRESET_DELETE_ID:I = 0xb82e66

.field public static final IB_PEN_PRESET_PREVIEW_ID:I = 0xb82e65

.field public static final IV_PEN_PRESET_ALPHA_PREVIEW_ID:I = 0xb82f2d

.field public static final IV_PEN_PRESET_PREVIEW_ID:I = 0xb82ec9

.field private static final mImagePath_pen_preset_bg:Ljava/lang/String; = "pen_preset_bg"

.field private static final mImagePath_pen_preset_bg_focus:Ljava/lang/String; = "pen_preset_bg_focus"

.field private static final mImagePath_pen_preset_bg_press:Ljava/lang/String; = "pen_preset_bg_press"

.field private static final mImagePath_progress_bg_alpha:Ljava/lang/String; = "preview_alpha"

.field private static final mImagePath_snote_delete:Ljava/lang/String; = "pen_preset_delete_normal"

.field private static final mImagePath_snote_delete_chagall:Ljava/lang/String; = "pen_preset_delete_normal_chagall"

.field private static final mImagePath_snote_delete_focus:Ljava/lang/String; = "pen_preset_delete_focus"

.field private static final mImagePath_snote_delete_focus_chagall:Ljava/lang/String; = "pen_preset_delete_focus_chagall"

.field private static final mImagePath_snote_delete_press:Ljava/lang/String; = "pen_preset_delete_press"

.field private static final mImagePath_snote_delete_press_chagall:Ljava/lang/String; = "pen_preset_delete_press_chagall"


# instance fields
.field private localDisplayMetrics:Landroid/util/DisplayMetrics;

.field private final mPresetListItemView:Landroid/view/View;

.field private final mSdkVersion:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;F)V
    .locals 1
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "custom_imagepath"    # Ljava/lang/String;
    .param p3, "ratio"    # F

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    .line 20
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mSdkVersion:I

    .line 40
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mDrawableContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->PresetListItemView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mPresetListItemView:Landroid/view/View;

    .line 53
    return-void
.end method

.method private PresetListItemView(Landroid/content/Context;)Landroid/view/View;
    .locals 26
    .param p1, "paramContext"    # Landroid/content/Context;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 66
    const/high16 v20, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v16, v0

    .line 67
    .local v16, "preSetHeight":F
    const/high16 v20, 0x41b80000    # 23.0f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v20

    move/from16 v0, v20

    int-to-float v7, v0

    .line 69
    .local v7, "deleteSize":F
    new-instance v11, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 71
    .local v11, "localRelativeLayout":Landroid/widget/RelativeLayout;
    new-instance v12, Landroid/widget/AbsListView$LayoutParams;

    const/high16 v20, 0x42940000    # 74.0f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v20

    .line 72
    const/high16 v21, 0x42940000    # 74.0f

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v21

    .line 71
    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v12, v0, v1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 74
    .local v12, "localRelativeLayoutParam":Landroid/widget/AbsListView$LayoutParams;
    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 75
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 76
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 77
    const/16 v20, 0x10

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 81
    new-instance v14, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 82
    .local v14, "penAlphaPreview":Landroid/widget/RelativeLayout;
    new-instance v15, Landroid/widget/RelativeLayout$LayoutParams;

    .line 83
    const/high16 v20, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v20

    const/high16 v21, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v21

    .line 82
    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v15, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 84
    .local v15, "penPreviewLayoutParams02":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v20, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v20

    move/from16 v0, v20

    iput v0, v15, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 85
    const/high16 v20, 0x420c0000    # 35.0f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v20

    move/from16 v0, v20

    iput v0, v15, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 86
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 87
    invoke-virtual {v14, v15}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 88
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mSdkVersion:I

    move/from16 v20, v0

    const/16 v21, 0x10

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_0

    .line 89
    const-string v20, "preview_alpha"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 93
    :goto_0
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 94
    const v20, 0xb82f2d

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 97
    new-instance v18, Landroid/widget/ImageButton;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 98
    .local v18, "previewButton":Landroid/widget/ImageButton;
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 99
    const/high16 v20, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v20

    const/high16 v21, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v21

    .line 98
    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v9, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 100
    .local v9, "localLayoutParams1":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 101
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 102
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 103
    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setMaxHeight(I)V

    .line 104
    const/16 v20, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 105
    const/16 v20, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 106
    const v20, 0xb82e65

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setId(I)V

    .line 108
    new-instance v19, Landroid/widget/ImageView;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 110
    .local v19, "previewImage":Landroid/widget/ImageView;
    new-instance v20, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v21, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v21

    .line 111
    const/high16 v22, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v22

    invoke-direct/range {v20 .. v22}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 110
    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 112
    const v20, 0xb82ec9

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setId(I)V

    .line 114
    new-instance v17, Landroid/widget/ImageView;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 115
    .local v17, "presetBg":Landroid/widget/ImageView;
    new-instance v20, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v21, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v21

    .line 116
    const/high16 v22, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v22

    invoke-direct/range {v20 .. v22}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 115
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 118
    new-instance v5, Landroid/widget/ImageButton;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 119
    .local v5, "deleteButton":Landroid/widget/ImageButton;
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    float-to-int v0, v7

    move/from16 v20, v0

    .line 120
    float-to-int v0, v7

    move/from16 v21, v0

    .line 119
    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v6, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 121
    .local v6, "deleteButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v20, -0x40000000    # -2.0f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v20

    move/from16 v0, v20

    iput v0, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 122
    const/high16 v20, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v20

    move/from16 v0, v20

    iput v0, v6, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 123
    const/16 v20, 0xb

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 124
    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 126
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v8, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 130
    .local v8, "displayWidth":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    cmpl-float v20, v20, v21

    if-nez v20, :cond_3

    const/16 v20, 0xa00

    move/from16 v0, v20

    if-ne v8, v0, :cond_3

    .line 131
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mSdkVersion:I

    move/from16 v20, v0

    const/16 v21, 0x10

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_2

    .line 133
    const-string v20, "pen_preset_bg"

    .line 134
    const-string v21, "pen_preset_bg_press"

    const-string v22, "pen_preset_bg_focus"

    .line 135
    const-string v23, "pen_preset_bg_press"

    .line 133
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    move-object/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 136
    const-string v20, "pen_preset_bg"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 137
    const-string v20, "pen_preset_delete_normal_chagall"

    .line 138
    const-string v21, "pen_preset_delete_press_chagall"

    const-string v22, "pen_preset_delete_focus_chagall"

    .line 137
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 167
    :goto_2
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 168
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 169
    const v20, 0xb82e66

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setId(I)V

    .line 170
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 171
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mSdkVersion:I

    move/from16 v20, v0

    const/16 v21, 0x13

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_5

    .line 172
    new-instance v13, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 173
    .local v13, "mPreviewButtonLayout":Landroid/widget/RelativeLayout;
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 174
    const/high16 v20, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v20

    const/high16 v21, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v21

    .line 173
    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 175
    .local v10, "localLayoutParams2":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v13, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 177
    new-instance v20, Landroid/graphics/drawable/RippleDrawable;

    const/16 v21, 0x3d

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-static/range {v21 .. v24}, Landroid/graphics/Color;->argb(IIII)I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v21

    .line 178
    const-string v22, "pen_preset_bg"

    const/16 v23, 0x0

    const-string v24, "pen_preset_bg_focus"

    .line 179
    const-string v25, "pen_preset_bg_press"

    .line 178
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    move-object/from16 v3, v24

    move-object/from16 v4, v25

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v22

    .line 179
    const/16 v23, 0x0

    invoke-direct/range {v20 .. v23}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 177
    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 180
    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 181
    invoke-virtual {v11, v13}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 185
    .end local v10    # "localLayoutParams2":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v13    # "mPreviewButtonLayout":Landroid/widget/RelativeLayout;
    :goto_3
    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 186
    invoke-virtual {v11, v14}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 187
    invoke-virtual {v11, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 189
    return-object v11

    .line 91
    .end local v5    # "deleteButton":Landroid/widget/ImageButton;
    .end local v6    # "deleteButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "displayWidth":I
    .end local v9    # "localLayoutParams1":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v17    # "presetBg":Landroid/widget/ImageView;
    .end local v18    # "previewButton":Landroid/widget/ImageButton;
    .end local v19    # "previewImage":Landroid/widget/ImageView;
    :cond_0
    const-string v20, "preview_alpha"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 128
    .restart local v5    # "deleteButton":Landroid/widget/ImageButton;
    .restart local v6    # "deleteButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v9    # "localLayoutParams1":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v17    # "presetBg":Landroid/widget/ImageView;
    .restart local v18    # "previewButton":Landroid/widget/ImageButton;
    .restart local v19    # "previewImage":Landroid/widget/ImageView;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v8, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    goto/16 :goto_1

    .line 142
    .restart local v8    # "displayWidth":I
    :cond_2
    const-string v20, "pen_preset_bg"

    const-string v21, "pen_preset_bg_press"

    .line 143
    const-string v22, "pen_preset_bg_focus"

    const-string v23, "pen_preset_bg_press"

    .line 142
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    move-object/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 144
    const-string v20, "pen_preset_bg"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 145
    const-string v20, "pen_preset_delete_normal_chagall"

    .line 146
    const-string v21, "pen_preset_delete_press_chagall"

    const-string v22, "pen_preset_delete_focus_chagall"

    .line 145
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 149
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mSdkVersion:I

    move/from16 v20, v0

    const/16 v21, 0x10

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_4

    .line 151
    const-string v20, "pen_preset_bg"

    .line 152
    const-string v21, "pen_preset_bg_press"

    const-string v22, "pen_preset_bg_focus"

    .line 153
    const-string v23, "pen_preset_bg_press"

    .line 151
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    move-object/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 154
    const-string v20, "pen_preset_bg"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 155
    const-string v20, "pen_preset_delete_normal_chagall"

    .line 156
    const-string v21, "pen_preset_delete_press_chagall"

    const-string v22, "pen_preset_delete_focus_chagall"

    .line 155
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 160
    :cond_4
    const-string v20, "pen_preset_bg"

    const-string v21, "pen_preset_bg_press"

    .line 161
    const-string v22, "pen_preset_bg_focus"

    const-string v23, "pen_preset_bg_press"

    .line 160
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    move-object/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 162
    const-string v20, "pen_preset_bg"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 163
    const-string v20, "pen_preset_delete_normal"

    const-string v21, "pen_preset_delete_press"

    .line 164
    const-string v22, "pen_preset_delete_focus"

    .line 163
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 183
    :cond_5
    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_3
.end method


# virtual methods
.method public getPenPresetListRow()Landroid/view/View;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mPresetListItemView:Landroid/view/View;

    return-object v0
.end method
