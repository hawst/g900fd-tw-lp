.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RptUpdater"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V
    .locals 0

    .prologue
    .line 988
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v2, -0x1

    const/high16 v7, 0x43b40000    # 360.0f

    const/high16 v6, 0x41200000    # 10.0f

    const-wide/16 v4, 0x14

    const/16 v3, 0x438

    .line 991
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeAutoIncrement:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 992
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    .line 993
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMinSettingValue()F

    move-result v0

    .line 996
    .local v0, "min":F
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v1, :cond_3

    .line 997
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 998
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 1002
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    if-nez v1, :cond_0

    .line 1003
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iput v3, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 1009
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v6

    add-float/2addr v2, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v7

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1012
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    .line 1014
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$18(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1045
    .end local v0    # "min":F
    :cond_1
    :goto_2
    return-void

    .line 1000
    .restart local v0    # "min":F
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    goto :goto_0

    .line 1006
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iput v3, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    goto :goto_1

    .line 1015
    .end local v0    # "min":F
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeAutoDecrement:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1016
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    .line 1017
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMinSettingValue()F

    move-result v0

    .line 1020
    .restart local v0    # "min":F
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v1, :cond_7

    .line 1021
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 1022
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 1026
    :goto_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    if-nez v1, :cond_5

    .line 1027
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iput v3, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 1033
    :cond_5
    :goto_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v6

    add-float/2addr v2, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v7

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1036
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    .line 1037
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$18(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2

    .line 1024
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    goto :goto_3

    .line 1030
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iput v3, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    goto :goto_4

    .line 1038
    .end local v0    # "min":F
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaAutoIncrement:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1039
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    .line 1040
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$18(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2

    .line 1041
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaAutoDecrement:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1042
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    .line 1043
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$18(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2
.end method
