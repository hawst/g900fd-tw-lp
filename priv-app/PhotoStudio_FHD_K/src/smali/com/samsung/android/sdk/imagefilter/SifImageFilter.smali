.class public Lcom/samsung/android/sdk/imagefilter/SifImageFilter;
.super Ljava/lang/Object;
.source "SifImageFilter.java"


# static fields
.field public static final FILTER_BLUE_WASH:I = 0x67

.field public static final FILTER_BLUR:I = 0x2e

.field public static final FILTER_BRIGHT:I = 0xe

.field public static final FILTER_CARTOONIZE:I = 0x87

.field public static final FILTER_CLASSIC:I = 0x88

.field public static final FILTER_COLORIZE:I = 0x2d

.field public static final FILTER_COLOR_SKETCH:I = 0x4a

.field public static final FILTER_DARK:I = 0xf

.field public static final FILTER_DOWN_LIGHT:I = 0x66

.field public static final FILTER_FADED_COLOR:I = 0x2a

.field public static final FILTER_FUSAIN:I = 0x47

.field public static final FILTER_GRAY:I = 0xb

.field public static final FILTER_MAGIC_PEN:I = 0x84

.field public static final FILTER_MOSAIC:I = 0x82

.field public static final FILTER_NEGATIVE:I = 0xd

.field public static final FILTER_NOSTALGIA:I = 0x68

.field public static final FILTER_NUM:I = 0x21

.field public static final FILTER_OIL_PAINT:I = 0x85

.field public static final FILTER_OLD_PHOTO:I = 0x29

.field public static final FILTER_ORIGINAL:I = 0xa

.field public static final FILTER_PASTEL_SKETCH:I = 0x49

.field public static final FILTER_PENCIL_COLOR_SKETCH:I = 0x4c

.field public static final FILTER_PENCIL_PASTEL_SKETCH:I = 0x4b

.field public static final FILTER_PENCIL_SKETCH:I = 0x46

.field public static final FILTER_PEN_SKETCH:I = 0x48

.field public static final FILTER_POPART:I = 0x83

.field public static final FILTER_POSTERIZE:I = 0x86

.field public static final FILTER_RETRO:I = 0x64

.field public static final FILTER_SEPIA:I = 0xc

.field public static final FILTER_SOFT_GLOW:I = 0x6a

.field public static final FILTER_SUNSHINE:I = 0x65

.field public static final FILTER_VIGNETTE:I = 0x2b

.field public static final FILTER_VINTAGE:I = 0x28

.field public static final FILTER_VIVID:I = 0x2c

.field public static final FILTER_YELLOW_GLOW:I = 0x69

.field public static final LEVEL_LARGE:I = 0x3

.field public static final LEVEL_MEDIUM:I = 0x2

.field public static final LEVEL_SMALL:I = 0x1

.field public static final LEVEL_VERY_LARGE:I = 0x4

.field public static final LEVEL_VERY_SMALL:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ImageEffectEngine"

.field private static final strCheckSupportModel:Ljava/lang/String; = "SAMSUNG_SAMMFORMAT"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkLibrarySupportModel()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 673
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 674
    .local v3, "strModelName":Ljava/lang/String;
    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v6, "google_sdk"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_0

    .line 675
    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v6, "sdk"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_1

    .line 676
    :cond_0
    const-string v4, "SAMSUNG_SAMMFORMAT"

    .line 695
    :goto_0
    return-object v4

    .line 682
    :cond_1
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 683
    .local v0, "strBrand":Ljava/lang/String;
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 684
    .local v2, "strManufacturer":Ljava/lang/String;
    if-eqz v0, :cond_2

    if-nez v2, :cond_3

    .line 685
    :cond_2
    const-string v5, "ImageEffectEngine"

    const-string v6, "Unknown Brand/Manufacturer Device"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 688
    :cond_3
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 689
    .local v1, "strDeviceName":Ljava/lang/String;
    const-string v5, "Samsung"

    invoke-virtual {v0, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "Samsung"

    invoke-virtual {v2, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_4

    .line 690
    const-string v5, "ImageEffectEngine"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Device("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "), Model("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "), Brand("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 691
    const-string v7, "), Manufacturer("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") is not a Saumsung device."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 690
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 695
    :cond_4
    const-string v4, "SAMSUNG_SAMMFORMAT"

    goto :goto_0
.end method

.method private static checkValidImageFilterOption(I)Z
    .locals 1
    .param p0, "nImageFilterOption"    # I

    .prologue
    .line 883
    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    .line 884
    const/16 v0, 0xb

    if-eq p0, v0, :cond_0

    .line 885
    const/16 v0, 0xc

    if-eq p0, v0, :cond_0

    .line 886
    const/16 v0, 0xd

    if-eq p0, v0, :cond_0

    .line 887
    const/16 v0, 0xe

    if-eq p0, v0, :cond_0

    .line 888
    const/16 v0, 0xf

    if-eq p0, v0, :cond_0

    .line 889
    const/16 v0, 0x28

    if-eq p0, v0, :cond_0

    .line 890
    const/16 v0, 0x29

    if-eq p0, v0, :cond_0

    .line 891
    const/16 v0, 0x2a

    if-eq p0, v0, :cond_0

    .line 892
    const/16 v0, 0x2b

    if-eq p0, v0, :cond_0

    .line 893
    const/16 v0, 0x2c

    if-eq p0, v0, :cond_0

    .line 894
    const/16 v0, 0x2d

    if-eq p0, v0, :cond_0

    .line 895
    const/16 v0, 0x2e

    if-eq p0, v0, :cond_0

    .line 896
    const/16 v0, 0x46

    if-eq p0, v0, :cond_0

    .line 897
    const/16 v0, 0x47

    if-eq p0, v0, :cond_0

    .line 898
    const/16 v0, 0x48

    if-eq p0, v0, :cond_0

    .line 899
    const/16 v0, 0x49

    if-eq p0, v0, :cond_0

    .line 900
    const/16 v0, 0x4a

    if-eq p0, v0, :cond_0

    .line 901
    const/16 v0, 0x4b

    if-eq p0, v0, :cond_0

    .line 902
    const/16 v0, 0x4c

    if-eq p0, v0, :cond_0

    .line 903
    const/16 v0, 0x64

    if-eq p0, v0, :cond_0

    .line 904
    const/16 v0, 0x65

    if-eq p0, v0, :cond_0

    .line 905
    const/16 v0, 0x66

    if-eq p0, v0, :cond_0

    .line 906
    const/16 v0, 0x67

    if-eq p0, v0, :cond_0

    .line 907
    const/16 v0, 0x68

    if-eq p0, v0, :cond_0

    .line 908
    const/16 v0, 0x69

    if-eq p0, v0, :cond_0

    .line 909
    const/16 v0, 0x6a

    if-eq p0, v0, :cond_0

    .line 910
    const/16 v0, 0x82

    if-eq p0, v0, :cond_0

    .line 911
    const/16 v0, 0x83

    if-eq p0, v0, :cond_0

    .line 912
    const/16 v0, 0x84

    if-eq p0, v0, :cond_0

    .line 913
    const/16 v0, 0x85

    if-eq p0, v0, :cond_0

    .line 914
    const/16 v0, 0x86

    if-eq p0, v0, :cond_0

    .line 915
    const/16 v0, 0x87

    if-eq p0, v0, :cond_0

    .line 916
    const/16 v0, 0x88

    if-eq p0, v0, :cond_0

    .line 917
    const/4 v0, 0x0

    .line 920
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static filterImage(Landroid/graphics/Bitmap;II)V
    .locals 1
    .param p0, "inputBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "imageFilterOption"    # I
    .param p2, "filterLevel"    # I

    .prologue
    .line 426
    invoke-static {p0, p1, p2}, Lcom/samsung/android/sdk/imagefilter/SifImageFilter;->filterImageOperation(Landroid/graphics/Bitmap;II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 427
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 429
    :cond_0
    return-void
.end method

.method public static filterImageCopy(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "inputBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "imageFilterOption"    # I
    .param p2, "filterLevel"    # I

    .prologue
    .line 463
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 464
    .local v0, "resultBitmap":Landroid/graphics/Bitmap;
    invoke-static {v0, p1, p2}, Lcom/samsung/android/sdk/imagefilter/SifImageFilter;->filterImageOperation(Landroid/graphics/Bitmap;II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 465
    const/4 v0, 0x0

    .line 467
    .end local v0    # "resultBitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-object v0
.end method

.method private static filterImageOperation(Landroid/graphics/Bitmap;II)Z
    .locals 20
    .param p0, "InputBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "nImageFilterOption"    # I
    .param p2, "nFilterLevel"    # I

    .prologue
    .line 525
    invoke-static {}, Lcom/samsung/android/sdk/imagefilter/SifImageFilter;->checkLibrarySupportModel()Ljava/lang/String;

    move-result-object v19

    .line 526
    .local v19, "strModel":Ljava/lang/String;
    if-nez v19, :cond_0

    .line 527
    const/4 v1, 0x0

    .line 663
    :goto_0
    return v1

    .line 529
    :cond_0
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toCharArray()[C

    move-result-object v17

    .line 530
    .local v17, "SupportModel":[C
    if-nez v17, :cond_1

    .line 531
    const/4 v1, 0x0

    goto :goto_0

    .line 533
    :cond_1
    move-object/from16 v0, v17

    array-length v1, v0

    move-object/from16 v0, v17

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->setCheckSupportingModel([CI)V

    .line 535
    invoke-static/range {p1 .. p2}, Lcom/samsung/android/sdk/imagefilter/SifImageFilter;->getImageFilterConvertLevel(II)I

    move-result v18

    .line 536
    .local v18, "nConvertedLevel":I
    if-ltz v18, :cond_4

    .line 538
    const/16 v1, 0xa

    move/from16 v0, p1

    if-eq v0, v1, :cond_3

    .line 539
    if-nez p0, :cond_2

    .line 541
    const-string v1, "ImageEffectEngine"

    const-string v3, "Input Bitmap is null"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    const/4 v1, 0x0

    goto :goto_0

    .line 545
    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 546
    .local v4, "nWidth":I
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 547
    .local v8, "nHeight":I
    mul-int v1, v4, v8

    new-array v2, v1, [I

    .line 548
    .local v2, "in":[I
    mul-int v1, v4, v8

    new-array v10, v1, [I

    .line 550
    .local v10, "out":[I
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move v7, v4

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 552
    sparse-switch p1, :sswitch_data_0

    .line 653
    const/4 v1, 0x0

    goto :goto_0

    .line 554
    :sswitch_0
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyGray([I[IIII)V

    .line 656
    :goto_1
    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v9, p0

    move v12, v4

    move v15, v4

    move/from16 v16, v8

    invoke-virtual/range {v9 .. v16}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 657
    const/4 v2, 0x0

    .line 660
    .end local v2    # "in":[I
    .end local v4    # "nWidth":I
    .end local v8    # "nHeight":I
    .end local v10    # "out":[I
    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 557
    .restart local v2    # "in":[I
    .restart local v4    # "nWidth":I
    .restart local v8    # "nHeight":I
    .restart local v10    # "out":[I
    :sswitch_1
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applySepia([I[IIII)V

    goto :goto_1

    .line 560
    :sswitch_2
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyBright([I[IIII)V

    goto :goto_1

    .line 563
    :sswitch_3
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyDark([I[IIII)V

    goto :goto_1

    .line 566
    :sswitch_4
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyBlur([I[IIII)V

    goto :goto_1

    .line 569
    :sswitch_5
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyMosaic([I[IIII)V

    goto :goto_1

    .line 572
    :sswitch_6
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyPencilSketch([I[IIII)V

    goto :goto_1

    .line 575
    :sswitch_7
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyPenSketch([I[IIII)V

    goto :goto_1

    .line 578
    :sswitch_8
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyPastelSketch([I[IIII)V

    goto :goto_1

    .line 581
    :sswitch_9
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyColorSketch([I[IIII)V

    goto :goto_1

    .line 584
    :sswitch_a
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyPencilPastelSketch([I[IIII)V

    goto :goto_1

    .line 587
    :sswitch_b
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyPencilColorSketch([I[IIII)V

    goto :goto_1

    .line 590
    :sswitch_c
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyVivid([I[IIII)V

    goto :goto_1

    .line 593
    :sswitch_d
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyColorize([I[IIII)V

    goto :goto_1

    .line 596
    :sswitch_e
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyCartoonize([I[IIII)V

    goto :goto_1

    .line 599
    :sswitch_f
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyVignette([I[IIII)V

    goto :goto_1

    .line 602
    :sswitch_10
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyVintage([I[IIII)V

    goto :goto_1

    .line 605
    :sswitch_11
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyOldPhoto([I[IIII)V

    goto :goto_1

    .line 608
    :sswitch_12
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyPopArt([I[IIII)V

    goto :goto_1

    .line 611
    :sswitch_13
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyRetro([I[IIII)V

    goto :goto_1

    .line 614
    :sswitch_14
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applySunshine([I[IIII)V

    goto/16 :goto_1

    .line 617
    :sswitch_15
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyDownlight([I[IIII)V

    goto/16 :goto_1

    .line 620
    :sswitch_16
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyBluewash([I[IIII)V

    goto/16 :goto_1

    .line 623
    :sswitch_17
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyNostalgia([I[IIII)V

    goto/16 :goto_1

    .line 626
    :sswitch_18
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyYellowglow([I[IIII)V

    goto/16 :goto_1

    .line 629
    :sswitch_19
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyClassic([I[IIII)V

    goto/16 :goto_1

    .line 632
    :sswitch_1a
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applySoftglow([I[IIII)V

    goto/16 :goto_1

    .line 635
    :sswitch_1b
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyMagicPen([I[IIII)V

    goto/16 :goto_1

    .line 638
    :sswitch_1c
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyOilpaint([I[IIII)V

    goto/16 :goto_1

    .line 641
    :sswitch_1d
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyPosterize([I[IIII)V

    goto/16 :goto_1

    .line 644
    :sswitch_1e
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyFusain([I[IIII)V

    goto/16 :goto_1

    .line 647
    :sswitch_1f
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyNegative([I[IIII)V

    goto/16 :goto_1

    .line 650
    :sswitch_20
    move/from16 v0, v18

    invoke-static {v2, v10, v4, v8, v0}, Lcom/samsung/android/sdk/imagefilter/ImageEffectEngineJNI;->applyFadedColor([I[IIII)V

    goto/16 :goto_1

    .line 663
    .end local v2    # "in":[I
    .end local v4    # "nWidth":I
    .end local v8    # "nHeight":I
    .end local v10    # "out":[I
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 552
    nop

    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0xc -> :sswitch_1
        0xd -> :sswitch_1f
        0xe -> :sswitch_2
        0xf -> :sswitch_3
        0x28 -> :sswitch_10
        0x29 -> :sswitch_11
        0x2a -> :sswitch_20
        0x2b -> :sswitch_f
        0x2c -> :sswitch_c
        0x2d -> :sswitch_d
        0x2e -> :sswitch_4
        0x46 -> :sswitch_6
        0x47 -> :sswitch_1e
        0x48 -> :sswitch_7
        0x49 -> :sswitch_8
        0x4a -> :sswitch_9
        0x4b -> :sswitch_a
        0x4c -> :sswitch_b
        0x64 -> :sswitch_13
        0x65 -> :sswitch_14
        0x66 -> :sswitch_15
        0x67 -> :sswitch_16
        0x68 -> :sswitch_17
        0x69 -> :sswitch_18
        0x6a -> :sswitch_1a
        0x82 -> :sswitch_5
        0x83 -> :sswitch_12
        0x84 -> :sswitch_1b
        0x85 -> :sswitch_1c
        0x86 -> :sswitch_1d
        0x87 -> :sswitch_e
        0x88 -> :sswitch_19
    .end sparse-switch
.end method

.method private static getImageFilterConvertLevel(II)I
    .locals 4
    .param p0, "nImageFilterOption"    # I
    .param p1, "nFilterLevel"    # I

    .prologue
    const/4 v1, -0x1

    .line 699
    const/4 v0, -0x1

    .line 701
    .local v0, "nConvertedLevel":I
    invoke-static {p0}, Lcom/samsung/android/sdk/imagefilter/SifImageFilter;->checkValidImageFilterOption(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 858
    :goto_0
    return v1

    .line 704
    :cond_0
    const/4 v0, 0x0

    .line 707
    sparse-switch p0, :sswitch_data_0

    :goto_1
    move v1, v0

    .line 858
    goto :goto_0

    .line 709
    :sswitch_0
    packed-switch p1, :pswitch_data_0

    .line 726
    const-string v2, "ImageEffectEngine"

    const-string v3, "Invalid Blurring Option"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 711
    :pswitch_0
    const/4 v0, 0x0

    .line 712
    goto :goto_1

    .line 714
    :pswitch_1
    const/16 v0, 0x32

    .line 715
    goto :goto_1

    .line 717
    :pswitch_2
    const/16 v0, 0x4b

    .line 718
    goto :goto_1

    .line 720
    :pswitch_3
    const/16 v0, 0x5f

    .line 721
    goto :goto_1

    .line 723
    :pswitch_4
    const/16 v0, 0x62

    .line 724
    goto :goto_1

    .line 732
    :sswitch_1
    add-int/lit8 v0, p1, 0x1

    .line 734
    goto :goto_1

    .line 736
    :sswitch_2
    mul-int/lit8 v1, p1, 0xa

    add-int/lit8 v0, v1, 0x1e

    .line 738
    goto :goto_1

    .line 740
    :sswitch_3
    packed-switch p1, :pswitch_data_1

    .line 757
    const-string v2, "ImageEffectEngine"

    const-string v3, "Invalid Vivid Option"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 742
    :pswitch_5
    const/16 v0, 0x14

    .line 743
    goto :goto_1

    .line 745
    :pswitch_6
    const/16 v0, 0x46

    .line 746
    goto :goto_1

    .line 748
    :pswitch_7
    const/16 v0, 0x82

    .line 749
    goto :goto_1

    .line 751
    :pswitch_8
    const/16 v0, 0xa0

    .line 752
    goto :goto_1

    .line 754
    :pswitch_9
    const/16 v0, 0xbe

    .line 755
    goto :goto_1

    .line 763
    :sswitch_4
    packed-switch p1, :pswitch_data_2

    .line 780
    const-string v2, "ImageEffectEngine"

    const-string v3, "Invalid Colorize Option"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 765
    :pswitch_a
    const/16 v0, 0x24

    .line 766
    goto :goto_1

    .line 768
    :pswitch_b
    const/16 v0, 0x6c

    .line 769
    goto :goto_1

    .line 771
    :pswitch_c
    const/16 v0, 0xb4

    .line 772
    goto :goto_1

    .line 774
    :pswitch_d
    const/16 v0, 0xfa

    .line 775
    goto :goto_1

    .line 777
    :pswitch_e
    const/16 v0, 0x136

    .line 778
    goto :goto_1

    .line 786
    :sswitch_5
    add-int/lit8 v0, p1, 0x4

    .line 788
    goto :goto_1

    .line 791
    :sswitch_6
    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v0, v1, 0x14

    .line 793
    goto :goto_1

    .line 798
    :sswitch_7
    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v0, v1, 0xa

    .line 800
    goto :goto_1

    .line 804
    :sswitch_8
    packed-switch p1, :pswitch_data_3

    .line 821
    const-string v2, "ImageEffectEngine"

    const-string v3, "Invalid Sketch Option"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 806
    :pswitch_f
    const/16 v0, 0xe

    .line 807
    goto :goto_1

    .line 809
    :pswitch_10
    const/16 v0, 0xb

    .line 810
    goto :goto_1

    .line 812
    :pswitch_11
    const/16 v0, 0x8

    .line 813
    goto :goto_1

    .line 815
    :pswitch_12
    const/4 v0, 0x5

    .line 816
    goto :goto_1

    .line 818
    :pswitch_13
    const/4 v0, 0x2

    .line 819
    goto :goto_1

    .line 828
    :sswitch_9
    rsub-int/lit8 v0, p1, 0x5

    .line 830
    goto :goto_1

    .line 832
    :sswitch_a
    packed-switch p1, :pswitch_data_4

    .line 849
    const-string v2, "ImageEffectEngine"

    const-string v3, "Invalid FadedColor Option"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 834
    :pswitch_14
    const/16 v0, 0x14

    .line 835
    goto/16 :goto_1

    .line 837
    :pswitch_15
    const/16 v0, 0x28

    .line 838
    goto/16 :goto_1

    .line 840
    :pswitch_16
    const/16 v0, 0x3c

    .line 841
    goto/16 :goto_1

    .line 843
    :pswitch_17
    const/16 v0, 0x50

    .line 844
    goto/16 :goto_1

    .line 846
    :pswitch_18
    const/16 v0, 0x5f

    .line 847
    goto/16 :goto_1

    .line 707
    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_2
        0xe -> :sswitch_6
        0xf -> :sswitch_6
        0x2a -> :sswitch_a
        0x2c -> :sswitch_3
        0x2d -> :sswitch_4
        0x2e -> :sswitch_0
        0x46 -> :sswitch_7
        0x48 -> :sswitch_8
        0x49 -> :sswitch_8
        0x4a -> :sswitch_8
        0x4b -> :sswitch_7
        0x4c -> :sswitch_7
        0x6a -> :sswitch_7
        0x82 -> :sswitch_1
        0x86 -> :sswitch_9
        0x87 -> :sswitch_5
    .end sparse-switch

    .line 709
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 740
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 763
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 804
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch

    .line 832
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method public static setImageTransparency(Landroid/graphics/Bitmap;I)V
    .locals 4
    .param p0, "inputBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "alpha"    # I

    .prologue
    .line 488
    if-nez p0, :cond_0

    .line 490
    const-string v2, "ImageEffectEngine"

    const-string v3, "Input Bitmap for Alpha Converting Operation is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Input bitmap is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 494
    :cond_0
    if-ltz p1, :cond_1

    const/16 v2, 0xff

    if-le p1, v2, :cond_2

    .line 496
    :cond_1
    const-string v2, "ImageEffectEngine"

    const-string v3, "Input alpha Level is OutofRange"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Alpha value must be between 0~255"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 500
    :cond_2
    if-nez p1, :cond_3

    .line 508
    :goto_0
    return-void

    .line 504
    :cond_3
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 505
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 506
    .local v1, "paint":Landroid/graphics/Paint;
    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 507
    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    goto :goto_0
.end method
