.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;
.super Ljava/lang/Object;
.source "SpenSettingEraserLayout2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 230
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v0, v0, v1

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    const/high16 v1, 0x42c60000    # 99.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v0, v0, v1

    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 232
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 233
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 236
    :cond_0
    return-void
.end method
