.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;
.super Ljava/lang/Object;
.source "NRRSignaturesReader.java"


# static fields
.field private static final COMPONENT_ORIENTATION:I = 0x4

.field private static final COMPONENT_PRESSURE:I = 0x2

.field private static final COMPONENT_TILT:I = 0x5

.field private static final COMPONENT_TIMESTAMP:I = 0x3

.field private static final COMPONENT_X:I = 0x0

.field private static final COMPONENT_Y:I = 0x1

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final NUMBER_OF_COMPONENTS:I = 0x6


# instance fields
.field private mReader:Ljava/io/BufferedReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;->LOG_TAG:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljavax/crypto/Cipher;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "cipher"    # Ljavax/crypto/Cipher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    new-instance v2, Ljavax/crypto/CipherInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3, p2}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    invoke-direct {v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;->mReader:Ljava/io/BufferedReader;

    .line 37
    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 41
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;->mReader:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v0

    .line 43
    .local v0, "ex":Ljava/io/IOException;
    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;->LOG_TAG:Ljava/lang/String;

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public read()Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/recognitionengine/Signature;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/recognitionengine/Signature;>;"
    const/4 v2, 0x0

    .line 55
    .local v2, "line":Ljava/lang/String;
    :goto_0
    :try_start_0
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;->mReader:Ljava/io/BufferedReader;

    invoke-virtual {v11}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 115
    :cond_0
    :goto_1
    return-object v3

    .line 57
    :cond_1
    new-instance v6, Lcom/samsung/recognitionengine/Signature;

    invoke-direct {v6}, Lcom/samsung/recognitionengine/Signature;-><init>()V

    .line 58
    .local v6, "sig":Lcom/samsung/recognitionengine/Signature;
    const/4 v7, 0x0

    .line 60
    .local v7, "stroke":Lcom/samsung/recognitionengine/Stroke;
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .local v8, "stroke_num":I
    :goto_2
    if-gtz v8, :cond_2

    .line 104
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 106
    .end local v6    # "sig":Lcom/samsung/recognitionengine/Signature;
    .end local v7    # "stroke":Lcom/samsung/recognitionengine/Stroke;
    .end local v8    # "stroke_num":I
    :catch_0
    move-exception v0

    .line 107
    .local v0, "ex":Ljava/io/IOException;
    sget-object v11, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;->LOG_TAG:Ljava/lang/String;

    const-string v12, "Can not read from file!"

    invoke-static {v11, v12, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 108
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 62
    .end local v0    # "ex":Ljava/io/IOException;
    .restart local v6    # "sig":Lcom/samsung/recognitionengine/Signature;
    .restart local v7    # "stroke":Lcom/samsung/recognitionengine/Stroke;
    .restart local v8    # "stroke_num":I
    :cond_2
    :try_start_1
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;->mReader:Ljava/io/BufferedReader;

    invoke-virtual {v11}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 63
    new-instance v7, Lcom/samsung/recognitionengine/Stroke;

    .end local v7    # "stroke":Lcom/samsung/recognitionengine/Stroke;
    invoke-direct {v7}, Lcom/samsung/recognitionengine/Stroke;-><init>()V

    .line 64
    .restart local v7    # "stroke":Lcom/samsung/recognitionengine/Stroke;
    new-instance v10, Ljava/util/StringTokenizer;

    const-string v11, " "

    invoke-direct {v10, v2, v11}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .local v10, "tokenizer":Ljava/util/StringTokenizer;
    const/4 v4, 0x0

    .line 67
    .local v4, "point":Lcom/samsung/recognitionengine/TouchPoint;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    invoke-virtual {v10}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v11

    if-nez v11, :cond_3

    .line 98
    invoke-virtual {v6, v7}, Lcom/samsung/recognitionengine/Signature;->add(Lcom/samsung/recognitionengine/Stroke;)V

    .line 60
    add-int/lit8 v8, v8, -0x1

    goto :goto_2

    .line 69
    :cond_3
    invoke-virtual {v10}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    .line 71
    .local v9, "tok":Ljava/lang/String;
    rem-int/lit8 v11, v1, 0x6

    packed-switch v11, :pswitch_data_0

    .line 67
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 73
    :pswitch_0
    if-eqz v4, :cond_4

    .line 74
    invoke-virtual {v7, v4}, Lcom/samsung/recognitionengine/Stroke;->add(Lcom/samsung/recognitionengine/TouchPoint;)V

    .line 76
    :cond_4
    new-instance v4, Lcom/samsung/recognitionengine/TouchPoint;

    .end local v4    # "point":Lcom/samsung/recognitionengine/TouchPoint;
    invoke-direct {v4}, Lcom/samsung/recognitionengine/TouchPoint;-><init>()V

    .line 77
    .restart local v4    # "point":Lcom/samsung/recognitionengine/TouchPoint;
    new-instance v11, Lcom/samsung/recognitionengine/PointF;

    invoke-static {v9}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v12

    const/4 v13, 0x0

    invoke-direct {v11, v12, v13}, Lcom/samsung/recognitionengine/PointF;-><init>(FF)V

    invoke-virtual {v4, v11}, Lcom/samsung/recognitionengine/TouchPoint;->setPoint(Lcom/samsung/recognitionengine/PointF;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    .line 110
    .end local v1    # "i":I
    .end local v4    # "point":Lcom/samsung/recognitionengine/TouchPoint;
    .end local v6    # "sig":Lcom/samsung/recognitionengine/Signature;
    .end local v7    # "stroke":Lcom/samsung/recognitionengine/Stroke;
    .end local v8    # "stroke_num":I
    .end local v9    # "tok":Ljava/lang/String;
    .end local v10    # "tokenizer":Ljava/util/StringTokenizer;
    :catch_1
    move-exception v0

    .line 111
    .local v0, "ex":Ljava/lang/NullPointerException;
    sget-object v11, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesReader;->LOG_TAG:Ljava/lang/String;

    const-string v12, "tokenizer is null"

    invoke-static {v11, v12, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 112
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 80
    .end local v0    # "ex":Ljava/lang/NullPointerException;
    .restart local v1    # "i":I
    .restart local v4    # "point":Lcom/samsung/recognitionengine/TouchPoint;
    .restart local v6    # "sig":Lcom/samsung/recognitionengine/Signature;
    .restart local v7    # "stroke":Lcom/samsung/recognitionengine/Stroke;
    .restart local v8    # "stroke_num":I
    .restart local v9    # "tok":Ljava/lang/String;
    .restart local v10    # "tokenizer":Ljava/util/StringTokenizer;
    :pswitch_1
    :try_start_2
    invoke-virtual {v4}, Lcom/samsung/recognitionengine/TouchPoint;->getPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v5

    .line 81
    .local v5, "prev_x":F
    new-instance v11, Lcom/samsung/recognitionengine/PointF;

    invoke-static {v9}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v12

    invoke-direct {v11, v5, v12}, Lcom/samsung/recognitionengine/PointF;-><init>(FF)V

    invoke-virtual {v4, v11}, Lcom/samsung/recognitionengine/TouchPoint;->setPoint(Lcom/samsung/recognitionengine/PointF;)V

    goto :goto_4

    .line 84
    .end local v5    # "prev_x":F
    :pswitch_2
    invoke-static {v9}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    invoke-virtual {v4, v11}, Lcom/samsung/recognitionengine/TouchPoint;->setPressure(F)V

    goto :goto_4

    .line 87
    :pswitch_3
    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    invoke-virtual {v4, v12, v13}, Lcom/samsung/recognitionengine/TouchPoint;->setTimestamp(D)V

    goto :goto_4

    .line 90
    :pswitch_4
    invoke-static {v9}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    invoke-virtual {v4, v11}, Lcom/samsung/recognitionengine/TouchPoint;->setOrientation(F)V

    goto :goto_4

    .line 93
    :pswitch_5
    invoke-static {v9}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    invoke-virtual {v4, v11}, Lcom/samsung/recognitionengine/TouchPoint;->setTilt(F)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4

    .line 71
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
