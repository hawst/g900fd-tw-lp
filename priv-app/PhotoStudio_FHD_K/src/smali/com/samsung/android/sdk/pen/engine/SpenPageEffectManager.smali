.class Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
.super Ljava/lang/Object;
.source "SpenPageEffectHandler.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;


# instance fields
.field private mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

.field private mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    .prologue
    const/4 v3, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    .line 53
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V

    aput-object v1, v0, v3

    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    const/4 v1, 0x1

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;

    invoke-direct {v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V

    aput-object v2, v0, v1

    .line 56
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    .line 57
    return-void
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 61
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aget-object v0, v0, v2

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->close()V

    .line 63
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aput-object v1, v0, v2

    .line 64
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aget-object v0, v0, v3

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->close()V

    .line 65
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aput-object v1, v0, v3

    .line 66
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    .line 68
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    .line 69
    return-void
.end method

.method public drawAnimation(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->drawAnimation(Landroid/graphics/Canvas;)V

    .line 119
    return-void
.end method

.method public isWorking()Z
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->isWorking()Z

    move-result v0

    return v0
.end method

.method public saveScreenshot()Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->saveScreenshot()Z

    move-result v0

    return v0
.end method

.method public setCanvasInformation(IIII)V
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 96
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 99
    return-void

    .line 96
    :cond_0
    aget-object v0, v2, v1

    .line 97
    .local v0, "handler":Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->setCanvasInformation(IIII)V

    .line 96
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setPaint(Landroid/graphics/Paint;)V
    .locals 4
    .param p1, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 82
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 85
    return-void

    .line 82
    :cond_0
    aget-object v0, v2, v1

    .line 83
    .local v0, "handler":Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;
    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->setPaint(Landroid/graphics/Paint;)V

    .line 82
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setScreenResolution(II)V
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 89
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 92
    return-void

    .line 89
    :cond_0
    aget-object v0, v2, v1

    .line 90
    .local v0, "handler":Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;
    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->setScreenResolution(II)V

    .line 89
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setType(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 72
    if-gez p1, :cond_1

    .line 73
    const/4 p1, 0x0

    .line 77
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aget-object v0, v0, p1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    .line 78
    return-void

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v0, v0

    if-lt p1, v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v0, v0

    add-int/lit8 p1, v0, -0x1

    goto :goto_0
.end method

.method public startAnimation(I)Z
    .locals 1
    .param p1, "direction"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->startAnimation(I)Z

    move-result v0

    return v0
.end method
