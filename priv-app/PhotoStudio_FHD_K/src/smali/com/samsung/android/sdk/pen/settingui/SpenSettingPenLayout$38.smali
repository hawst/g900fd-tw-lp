.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$38;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$38;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    .line 6936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged(Z)V
    .locals 4
    .param p1, "transparent"    # Z

    .prologue
    .line 6942
    if-nez p1, :cond_0

    .line 6943
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$38;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const-string v3, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getPenNameIndex(Ljava/lang/String;)I
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/lang/String;)I

    move-result v1

    .line 6944
    .local v1, "removePenindex":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$38;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->removePen(I)V

    .line 6945
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$38;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$153(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 6952
    .end local v1    # "removePenindex":I
    :goto_0
    return-void

    .line 6947
    :cond_0
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$154(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 6949
    :catch_0
    move-exception v0

    .line 6950
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
