.class Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
.super Landroid/widget/ImageButton;
.source "SpenContextMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CustomImageView"
.end annotation


# static fields
.field private static final DEFAULT_TALKBACK_BORDER_LINE_COLOR:I = -0xff7a4a

.field private static final DEFAULT_TALKBACK_BORDER_WIDTH:I = 0x8

.field protected static final LEFT_EDGE_EFFECT_INDEX:I = 0x0

.field protected static final RIGHT_EDGE_EFFECT_INDEX:I = 0x2


# instance fields
.field private mIsLeftEdge:I

.field private mLeftEdgeEffect:Landroid/widget/EdgeEffect;

.field private mRightEdgeEffect:Landroid/widget/EdgeEffect;

.field private mTalkBackBoderPaint:Landroid/graphics/Paint;

.field private mTalkBackBorderRect:Landroid/graphics/RectF;

.field private mTalkBackIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 138
    invoke-direct {p0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 129
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    .line 130
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    .line 132
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mIsLeftEdge:I

    .line 139
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBoderPaint:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 140
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBoderPaint:Landroid/graphics/Paint;

    .line 141
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBoderPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41000000    # 8.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 142
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBoderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 143
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBoderPaint:Landroid/graphics/Paint;

    const v1, -0xff7a4a

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBorderRect:Landroid/graphics/RectF;

    if-nez v0, :cond_1

    .line 147
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBorderRect:Landroid/graphics/RectF;

    .line 150
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackIndex:I

    .line 151
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mIsLeftEdge:I

    .line 152
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isLeftEdge"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 155
    invoke-direct {p0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 129
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    .line 130
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    .line 132
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mIsLeftEdge:I

    .line 156
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mIsLeftEdge:I

    .line 158
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBoderPaint:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBoderPaint:Landroid/graphics/Paint;

    .line 160
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBoderPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41000000    # 8.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 161
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBoderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 162
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBoderPaint:Landroid/graphics/Paint;

    const v1, -0xff7a4a

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBorderRect:Landroid/graphics/RectF;

    if-nez v0, :cond_1

    .line 166
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBorderRect:Landroid/graphics/RectF;

    .line 169
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackIndex:I

    .line 170
    return-void
.end method

.method private drawEdgeEffectsUnclipped(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x0

    .line 186
    const/4 v1, 0x0

    .line 187
    .local v1, "needsInvalidate":Z
    const/4 v0, 0x0

    .line 189
    .local v0, "isReady":Z
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-nez v3, :cond_0

    .line 190
    const/4 v0, 0x1

    .line 192
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-nez v3, :cond_1

    .line 193
    const/4 v0, 0x1

    .line 196
    :cond_1
    if-eqz v0, :cond_3

    .line 197
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    .line 198
    .local v2, "restoreCount":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mIsLeftEdge:I

    if-nez v3, :cond_4

    .line 199
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v5, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 200
    const/high16 v3, -0x3d4c0000    # -90.0f

    invoke-virtual {p1, v3, v5, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 201
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->getWidth()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/EdgeEffect;->setSize(II)V

    .line 202
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    invoke-virtual {v3, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 203
    const/4 v1, 0x1

    .line 213
    :cond_2
    :goto_0
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 215
    if-eqz v1, :cond_3

    .line 216
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->invalidate()V

    .line 219
    .end local v2    # "restoreCount":I
    :cond_3
    return-void

    .line 205
    .restart local v2    # "restoreCount":I
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mIsLeftEdge:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 206
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 207
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {p1, v3, v5, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 208
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->getWidth()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/EdgeEffect;->setSize(II)V

    .line 209
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    invoke-virtual {v3, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 210
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected drawTalkbackBorder(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 222
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackIndex:I

    .line 223
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->invalidate()V

    .line 224
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x1

    .line 228
    invoke-super {p0, p1}, Landroid/widget/ImageButton;->onDraw(Landroid/graphics/Canvas;)V

    .line 230
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackIndex:I

    if-ne v0, v2, :cond_1

    .line 231
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBorderRect:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 232
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBorderRect:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 233
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBorderRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackBoderPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 235
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mTalkBackIndex:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 239
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mIsLeftEdge:I

    if-eq v0, v2, :cond_0

    .line 240
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->drawEdgeEffectsUnclipped(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected setEdgeEffect(Landroid/widget/EdgeEffect;I)V
    .locals 0
    .param p1, "edgeEffect"    # Landroid/widget/EdgeEffect;
    .param p2, "index"    # I

    .prologue
    .line 173
    packed-switch p2, :pswitch_data_0

    .line 183
    :goto_0
    :pswitch_0
    return-void

    .line 175
    :pswitch_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    goto :goto_0

    .line 179
    :pswitch_2
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    goto :goto_0

    .line 173
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
