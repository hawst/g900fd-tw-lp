.class public Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
.super Ljava/lang/Object;
.source "SpenPenPresetInfo.java"


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mFlag:Z

.field private final mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

.field private mPresetImage:Ljava/lang/String;

.field private mPresetIndex:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    .line 13
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mFlag:Z

    .line 15
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPresetImage:Ljava/lang/String;

    .line 10
    return-void
.end method


# virtual methods
.method protected close()V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    .line 171
    :cond_0
    return-void
.end method

.method public getAdvancedSetting()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    return v0
.end method

.method protected getFlag()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mFlag:Z

    return v0
.end method

.method public getPenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPenSize()F
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    return v0
.end method

.method public getPresetImageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPresetImage:Ljava/lang/String;

    return-object v0
.end method

.method public getPresetIndex()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPresetIndex:I

    return v0
.end method

.method protected setAdvancedSetting(Ljava/lang/String;)V
    .locals 1
    .param p1, "advancedSetting"    # Ljava/lang/String;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput-object p1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 149
    return-void
.end method

.method protected setBitmapSize(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 162
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    .line 164
    :cond_0
    return-void
.end method

.method protected setColor(I)V
    .locals 1
    .param p1, "paramInt"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput p1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 112
    return-void
.end method

.method protected setFlag(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mFlag:Z

    .line 45
    return-void
.end method

.method protected setPenName(Ljava/lang/String;)V
    .locals 1
    .param p1, "paramString"    # Ljava/lang/String;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput-object p1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 92
    return-void
.end method

.method protected setPenSize(F)V
    .locals 1
    .param p1, "size"    # F

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput p1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 73
    return-void
.end method

.method protected setPresetImageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "presetImage"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPresetImage:Ljava/lang/String;

    .line 102
    return-void
.end method

.method protected setPresetIndex(I)V
    .locals 0
    .param p1, "presetIndex"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPresetIndex:I

    .line 26
    return-void
.end method
